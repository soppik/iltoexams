/*
 *
 * jqTransform
 * by mathieu vilaplana mvilaplana@dfc-e.com
 * Designer ghyslain armand garmand@dfc-e.com
 *
 *
 * Version 1.0 25.09.08
 * Version 1.1 06.08.09
 * Add event click on Checkbox and Radio
 * Auto calculate the size of a select element
 * Can now, disabled the elements
 * Correct bug in ff if click on select (overflow=hidden)
 * No need any more preloading !!
 * 
 ******************************************** */
 
(function($){
  var defaultOptions = {preloadImg:true};
  var jqTransformImgPreloaded = false;

  var jqTransformPreloadHoverFocusImg = function(strImgUrl) {
    //guillemets to remove for ie
    strImgUrl = strImgUrl.replace(/^url((.*))/,'$1').replace(/^"(.*)"$/,'$1');
    var imgHover = new Image();
    imgHover.src = strImgUrl.replace(/.([a-zA-Z]*)$/,'-hover.$1');
    var imgFocus = new Image();
    imgFocus.src = strImgUrl.replace(/.([a-zA-Z]*)$/,'-focus.$1');        
  };

  
  /***************************
    Labels
  ***************************/
  var jqTransformGetLabel = function(objfield){
    var selfForm = $(objfield.get(0).form);
    var oLabel = objfield.next();
    if(!oLabel.is('label')) {
      oLabel = objfield.prev();
      if(oLabel.is('label')){
        var inputname = objfield.attr('id');
        if(inputname){
          oLabel = selfForm.find('label[for="'+inputname+'"]');
        } 
      }
    }
    if(oLabel.is('label')){return oLabel.css('cursor','pointer');}
    return false;
  };
  
  /* Hide all open selects */
  var jqTransformHideSelect = function(oTarget){
    var ulVisible = $('.jqTransformSelectWrapper ul:visible');
    ulVisible.each(function(){
      var oSelect = $(this).parents(".jqTransformSelectWrapper:first").find("select").get(0);
      //do not hide if click on the label object associated to the select
      if( !(oTarget && oSelect.oLabel && oSelect.oLabel.get(0) == oTarget.get(0)) ){$(this).hide();}
    });
  };
  /* Check for an external click */
  var jqTransformCheckExternalClick = function(event) {
    if ($(event.target).parents('.jqTransformSelectWrapper').length === 0) { jqTransformHideSelect($(event.target)); }
  };

  /* Apply document listener */
  var jqTransformAddDocumentListener = function (){
    $(document).mousedown(jqTransformCheckExternalClick);
  };  
      
  /* Add a new handler for the reset action */
  var jqTransformReset = function(f){
    var sel;
    $('.jqTransformSelectWrapper select', f).each(function(){sel = (this.selectedIndex<0) ? 0 : this.selectedIndex; $('ul', $(this).parent()).each(function(){$('a:eq('+ sel +')', this).click();});});
    $('a.jqTransformCheckbox, a.jqTransformRadio', f).removeClass('jqTransformChecked');
    $('input:checkbox, input:radio', f).each(function(){if(this.checked){$('a', $(this).parent()).addClass('jqTransformChecked');}});
  };

  /***************************
    Buttons
   ***************************/
  $.fn.jqTransInputButton = function(){
    return this.each(function(){
      var newBtn = $('<button id="'+ this.id +'" name="'+ this.name +'" type="'+ this.type +'" class="'+ this.className +' jqTransformButton"><span><span>'+ $(this).attr('value') +'</span></span>')
        .hover(function(){newBtn.addClass('jqTransformButton_hover');},function(){newBtn.removeClass('jqTransformButton_hover')})
        .mousedown(function(){newBtn.addClass('jqTransformButton_click')})
        .mouseup(function(){newBtn.removeClass('jqTransformButton_click')})
      ;
      $(this).replaceWith(newBtn);
    });
  };
  
  /***************************
    Text Fields 
   ***************************/
  $.fn.jqTransInputText = function(){
    return this.each(function(){
      var $input = $(this);
  
      if($input.hasClass('jqtranformdone') || !$input.is('input')) {return;}
      $input.addClass('jqtranformdone');
  
      var oLabel = jqTransformGetLabel($(this));
      oLabel && oLabel.bind('click',function(){$input.focus();});
  
      var inputSize=$input.width();
      if($input.attr('size')){
        inputSize = $input.attr('size')*10;
        $input.css('width',inputSize);
      }
      
      $input.addClass("jqTransformInput").wrap('<div class="jqTransformInputWrapper"><div class="jqTransformInputInner"><div></div></div></div>');
      var $wrapper = $input.parent().parent().parent();
      $wrapper.css("width", inputSize+10);
      $input
        .focus(function(){$wrapper.addClass("jqTransformInputWrapper_focus");})
        .blur(function(){$wrapper.removeClass("jqTransformInputWrapper_focus");})
        .hover(function(){$wrapper.addClass("jqTransformInputWrapper_hover");},function(){$wrapper.removeClass("jqTransformInputWrapper_hover");})
      ;
  
      /* If this is safari we need to add an extra class */
      $.browser.safari && $wrapper.addClass('jqTransformSafari');
      $.browser.safari && $input.css('width',$wrapper.width()+16);
      this.wrapper = $wrapper;
      
    });
  };
  
  /***************************
    Check Boxes 
   ***************************/  
  $.fn.jqTransCheckBox = function(){
    return this.each(function(){
      if($(this).hasClass('jqTransformHidden')) {return;}

      var $input = $(this);
      var inputSelf = this;

      //set the click on the label
      var oLabel=jqTransformGetLabel($input);
      oLabel && oLabel.click(function(){aLink.trigger('click');});
      
      var aLink = $('<a href="#" class="jqTransformCheckbox"></a>');
      //wrap and add the link
      $input.addClass('jqTransformHidden').wrap('<span class="jqTransformCheckboxWrapper"></span>').parent().prepend(aLink);
      //on change, change the class of the link
      $input.change(function(){
        this.checked && aLink.addClass('jqTransformChecked') || aLink.removeClass('jqTransformChecked');
        return true;
      });
      // Click Handler, trigger the click and change event on the input
      aLink.click(function(){
        //do nothing if the original input is disabled
        if($input.attr('disabled')){return false;}
        //trigger the envents on the input object
        $input.trigger('click').trigger("change");  
        return false;
      });

      // set the default state
      this.checked && aLink.addClass('jqTransformChecked');    
    });
  };
  /***************************
    Radio Buttons 
   ***************************/  
  /*$.fn.jqTransRadio = function(){
    return this.each(function(){
      if($(this).hasClass('jqTransformHidden')) {return;}

      var $input = $(this);
      var inputSelf = this;
        
      oLabel = jqTransformGetLabel($input);
      oLabel && oLabel.click(function(){aLink.trigger('click');});
  
      var aLink = $('<a href="#" class="jqTransformRadio" rel="'+ this.name +'"></a>');
      $input.addClass('jqTransformHidden').wrap('<span class="jqTransformRadioWrapper"></span>').parent().prepend(aLink);
      
      $input.change(function(){
        inputSelf.checked && aLink.addClass('jqTransformChecked') || aLink.removeClass('jqTransformChecked');
        return true;
      });
      // Click Handler
      aLink.click(function(){
        if($input.attr('disabled')){return false;}
        $input.trigger('click').trigger('change');
  
        // uncheck all others of same name input radio elements
        $('input[name="'+$input.attr('name')+'"]',inputSelf.form).not($input).each(function(){
          $(this).attr('type')=='radio' && $(this).trigger('change');
        });
  
        return false;          
      });
      // set the default state
      inputSelf.checked && aLink.addClass('jqTransformChecked');
    });
  };*/
  
  /***************************
    TextArea 
   ***************************/  
  $.fn.jqTransTextarea = function(){
    return this.each(function(){
      var textarea = $(this);
  
      if(textarea.hasClass('jqtransformdone')) {return;}
      textarea.addClass('jqtransformdone');
  
      oLabel = jqTransformGetLabel(textarea);
      oLabel && oLabel.click(function(){textarea.focus();});
      
      var strTable = '<table cellspacing="0" cellpadding="0" border="0" class="jqTransformTextarea">';
      strTable +='<tr><td id="jqTransformTextarea-tl"></td><td id="jqTransformTextarea-tm"></td><td id="jqTransformTextarea-tr"></td></tr>';
      strTable +='<tr><td id="jqTransformTextarea-ml">&nbsp;</td><td id="jqTransformTextarea-mm"><div></div></td><td id="jqTransformTextarea-mr">&nbsp;</td></tr>';  
      strTable +='<tr><td id="jqTransformTextarea-bl"></td><td id="jqTransformTextarea-bm"></td><td id="jqTransformTextarea-br"></td></tr>';
      strTable +='</table>';          
      var oTable = $(strTable)
          .insertAfter(textarea)
          .hover(function(){
            !oTable.hasClass('jqTransformTextarea-focus') && oTable.addClass('jqTransformTextarea-hover');
          },function(){
            oTable.removeClass('jqTransformTextarea-hover');          
          })
        ;
        
      textarea
        .focus(function(){oTable.removeClass('jqTransformTextarea-hover').addClass('jqTransformTextarea-focus');})
        .blur(function(){oTable.removeClass('jqTransformTextarea-focus');})
        .appendTo($('#jqTransformTextarea-mm div',oTable))
      ;
      this.oTable = oTable;
      if($.browser.safari){
        $('#jqTransformTextarea-mm',oTable)
          .addClass('jqTransformSafariTextarea')
          .find('div')
            .css('height',textarea.height())
            .css('width',textarea.width())
        ;
      }
    });
  };
  
  /***************************
    Select 
   ***************************/  
  $.fn.jqTransSelect = function(){
    return this.each(function(index){
      var $select = $(this);

      if($select.hasClass('jqTransformHidden')) {return;}
      if($select.attr('multiple')) {return;}

      var oLabel  =  jqTransformGetLabel($select);
      /* First thing we do is Wrap it */
      var $wrapper = $select
        .addClass('jqTransformHidden')
        .wrap('<div class="jqTransformSelectWrapper"></div>')
        .parent()
        .css({zIndex: 10-index})
      ;
      
      /* Now add the html for the select */
      $wrapper.prepend('<div><span></span><a href="#" class="jqTransformSelectOpen"></a></div><ul></ul>');
      var $ul = $('ul', $wrapper).css('width',$select.width()).hide();
      /* Now we add the options */
      $('option', this).each(function(i){
        var oLi = $('<li><a href="#" index="'+ i +'">'+ $(this).html() +'</a></li>');
        $ul.append(oLi);
      });
      
      /* Add click handler to the a */
      $ul.find('a').click(function(){
          $('a.selected', $wrapper).removeClass('selected');
          $(this).addClass('selected');  
          /* Fire the onchange event */
          if ($select[0].selectedIndex != $(this).attr('index') && $select[0].onchange) { $select[0].selectedIndex = $(this).attr('index'); $select[0].onchange(); }
          $select[0].selectedIndex = $(this).attr('index');
          $('span:eq(0)', $wrapper).html($(this).html());
          $ul.hide();
          return false;
      });
      /* Set the default */
      $('a:eq('+ this.selectedIndex +')', $ul).click();
      $('span:first', $wrapper).click(function(){$("a.jqTransformSelectOpen",$wrapper).trigger('click');});
      oLabel && oLabel.click(function(){$("a.jqTransformSelectOpen",$wrapper).trigger('click');});
      this.oLabel = oLabel;
      
      /* Apply the click handler to the Open */
      var oLinkOpen = $('a.jqTransformSelectOpen', $wrapper)
        .click(function(){
          //Check if box is already open to still allow toggle, but close all other selects
          if( $ul.css('display') == 'none' ) {jqTransformHideSelect();} 
          if($select.attr('disabled')){return false;}

          $ul.slideToggle('fast', function(){          
            var offSet = ($('a.selected', $ul).offset().top - $ul.offset().top);
            $ul.animate({scrollTop: offSet});
          });
          return false;
        })
      ;

      // Set the new width
      var iSelectWidth = $select.outerWidth();
      var oSpan = $('span:first',$wrapper);
      var newWidth = (iSelectWidth > oSpan.innerWidth())?iSelectWidth+oLinkOpen.outerWidth():$wrapper.width();
      $wrapper.css('width',newWidth);
      $ul.css('width',newWidth-2);
      oSpan.css({width:iSelectWidth});
    
      // Calculate the height if necessary, less elements that the default height
      //show the ul to calculate the block, if ul is not displayed li height value is 0
      $ul.css({display:'block',visibility:'hidden'});
      var iSelectHeight = ($('li',$ul).length)*($('li:first',$ul).height());//+1 else bug ff
      (iSelectHeight < $ul.height()) && $ul.css({height:iSelectHeight,'overflow':'hidden'});//hidden else bug with ff
      $ul.css({display:'none',visibility:'visible'});
      
    });
  };
  $.fn.jqTransform = function(options){
    var opt = $.extend({},defaultOptions,options);
    
    /* each form */
     return this.each(function(){
      var selfForm = $(this);
      if(selfForm.hasClass('jqtransformdone')) {return;}
      selfForm.addClass('jqtransformdone');
      
      /*$('input:submit, input:reset, input[type="button"]', this).jqTransInputButton();  */    
      /*$('input:text, input:password', this).jqTransInputText();  */    
      $('input:checkbox', this).jqTransCheckBox();
      $('input:radio', this).jqTransRadio();
      /*$('textarea', this).jqTransTextarea();*/  
      
      if( $('select', this).jqTransSelect().length > 0 ){jqTransformAddDocumentListener();}
      selfForm.bind('reset',function(){var action = function(){jqTransformReset(this);}; window.setTimeout(action, 10);});
      
      //preloading dont needed anymore since normal, focus and hover image are the same one
      /*if(opt.preloadImg && !jqTransformImgPreloaded){
        jqTransformImgPreloaded = true;
        var oInputText = $('input:text:first', selfForm);
        if(oInputText.length > 0){
          //pour ie on eleve les ""
          var strWrapperImgUrl = oInputText.get(0).wrapper.css('background-image');
          jqTransformPreloadHoverFocusImg(strWrapperImgUrl);          
          var strInnerImgUrl = $('div.jqTransformInputInner',$(oInputText.get(0).wrapper)).css('background-image');
          jqTransformPreloadHoverFocusImg(strInnerImgUrl);
        }
        
        var oTextarea = $('textarea',selfForm);
        if(oTextarea.length > 0){
          var oTable = oTextarea.get(0).oTable;
          $('td',oTable).each(function(){
            var strImgBack = $(this).css('background-image');
            jqTransformPreloadHoverFocusImg(strImgBack);
          });
        }
      }*/
      
      
    }); /* End Form each */
        
  };/* End the Plugin */

})(jQuery);
           


    $(function() {
//Flexslider inits
        $('#main-gallery .flexslider, .small-gallery-container .flexslider ,.content-frame.large .flexslider').flexslider({
          //directionNav : true
         // controlsContainer : "#main-gallery controlladores"
        });

        $('.clientes-carousel-container .flexslider').flexslider({
          animation : "slide",
          animationLoop : false,
          itemWidth : 240,
          itemHeight : 356,
          itemMargin : 0,
          start : function() {
            $('.clientes-carousel-container ul.slides>li').css('position', 'relative');
          }
        });

        $('#featured-carousel .flexslider').flexslider({
          animation : "slide",
          animationLoop : false,
          itemWidth : 240,
          itemHeight : 356,
          itemMargin : 0,
          start : function() {
            // $('.clientes-carousel-container ul.slides>li').css('position', 'relative');
          }
        });

      $('#call_mod').click(function(e) {
        e.preventDefault();
        $('#pop-form').lightbox_me({
          centered: true, 
          onLoad: function() { 

            }
          });
        
        });
       $('.flex-direction-nav').wrap('<div class="flex-direction-nav-outer">;');
       $('.flex-control-nav').wrap('<div class="flex-control-nav-outer">;');
      
// m.s. nav active      
  $(".home #multi-state-nav li").eq(0).addClass('active');  
// mvv-state article      

      $("#mvv-state article").eq(1).addClass('mid');
      
// jqtransform

    $("form").jqTransform();

      
//category bug fix
      
 for(i=0;i<$('.trigger').length;i++){
 $('.trigger').eq(i).attr('class',ReplaceAll($('.trigger').eq(i).attr('class'),'@',' '))
 }

function ReplaceAll(Source,stringToFind,stringToReplace){
  var temp = Source;
    var index = temp.indexOf(stringToFind);
        while(index != -1){
            temp = temp.replace(stringToFind,stringToReplace);
            index = temp.indexOf(stringToFind);
        }
        return temp;
}
      
      
/* -----------------------------------------------------------
  Main Menu
-------------------------------------------------------------- */

  
$('#main-nav > ul > li:not(.active)').hoverIntent({
  timeout: 300,
  over:function()
  {
    $('.level-2', this).slideDown(300);
  },
  out:function()
  {
      $('.level-2', this).fadeOut(100, function() {});
  }
});

for (var i = $('#main-nav > ul > li span.section-name').length - 1; i >= 0; i--) {
  var tmp_el=$('#main-nav > ul > li span.section-name').eq(i);
  if (parseInt(tmp_el.css('height').substring(0, 2)) < 45) {
    tmp_el.addClass('one-row');
  }
}


$('#main-nav ul.level-2:empty').remove();

$("#main-nav ul li:last-child").addClass("last");
$(".featured-general li:last-child").addClass("last");
//

// flexslider pagination line bg classes

    var fl_ = setTimeout(function() {
        fl_p();
    }, 2000);

function fl_p () {
var pag_el=$(" .flex-control-nav");

for (var i = 0; i < pag_el.length; i++) {
    switch (pag_el.eq(i).find('li').length){

      case 2:
        pag_el.eq(i).addClass('two-item');
      break;
      case 3:
        pag_el.eq(i).addClass('three-item');
      break;
      case 4:
        pag_el.eq(i).addClass('four-item');
      break;
      case 5:
        pag_el.eq(i).addClass('five-item');
      break;
      default:
      break;
        }
}

  // mapa functionality
  
for (var i = 1; i < $('.featured-general.slides > li').length+1; i++) {
  if ((i%4)===0) {
    ('.featured-general.slides > li').eq(i).addClass('no-border');
  };
};


}



// video play back:

$(".vid-holder a").click(function(e) {
  e.preventDefault();
  $(this).find('img').remove();
  $(this).find('.play-btn').remove();
  $(this).find('iframe').css('display','block').attr('src',$(this).find('iframe').attr('data-src-url'));

});

// country popup
  if($('#upper-bar #country-switch p').offset()){
     var c_left=$('#upper-bar #country-switch p').offset().left;
  }

var c_height=$('ul#countries li').length * $('ul#countries li').height();

$('ul#countries').css({
    left : c_left+'px'
  }).bind({
  mouseleave : function() {
    $(this).addClass('off');
}
});

$('#upper-bar #country-switch p').click(function() {
  $('ul#countries').removeClass('off');
});

// mapa functionality
  $('#mapa-button').click(function(e) {

      if (!($('#mapa').hasClass('expanded-state'))) {
            openclose($(this), 'open','mapa');
            $('html,body').animate({
              scrollTop: $("#footer").offset().top-200
            },300); 

        }else{
            openclose($(this), 'close','mapa');

                    }
  });

// valores functionality ******script para despliegue de texto**** 
      // valores functionality ******script para despliegue de texto****
      // valores functionality ******script para despliegue de texto****
      // valores functionality ******script para despliegue de texto****
      // valores functionality ******script para despliegue de texto****
      

   openclose( $('#mvv-state .expanding p.expand-button').eq(0), 'close','mvv');
   
//español

    $('#mvv-state .expanding p.expand-button').click(function() {
        if (!($(this).parent().hasClass('expanded-state'))) {
          
        openclose($('#mvv-state .expanded-state').find('p.expand-button'), 'close','mvv');
        openclose($(this), 'open','mvv');
        }
    });
   
//epanding2 ingles
    openclose( $('#mvv-state .expanding2 p.expand-button').eq(0), 'close','mvv2');
   


    $('#mvv-state .expanding2 p.expand-button').click(function() {
        if (!($(this).parent().hasClass('expanded-state'))) {
            
            openclose($('#mvv-state .expanded-state').find('p.expand-button'), 'close','mvv2');
            openclose($(this), 'open','mvv2');
        }
    }); 

//portugues 
      openclose( $('#mvv-state .expanding3 p.expand-button').eq(0), 'close','mvv3');
 $('#mvv-state .expanding3 p.expand-button').click(function() {
        if (!($(this).parent().hasClass('expanded-state'))) {
          
        openclose($('#mvv-state .expanded-state').find('p.expand-button'), 'close','mvv3');
        openclose($(this), 'open','mvv3');
        }
    });         
// contact-menu

    $('#contact-menu-button').click(function() {
        if (!($(this).parent().hasClass('expanded-state'))) {
        t_temp = '-100px';
        $(this).parent().addClass('expanded-state');

        }else{
        t_temp = '0';
        $(this).parent().removeClass('expanded-state');
        }

            $(this).parent().animate({
                'top' : t_temp
              }, 100, function() {
            });
    });

// faq 
      /////////////////////////
      /////////////////////////
      //////////////////////////////////////////////////

    $('#faq-state .expanding .expand-button').click(function() {
        if (!($(this).parent().hasClass('expanded-state'))) {
         // alert("111111");
          openclose($(this), 'open','faq-state');
        }else{
          openclose($(this), 'close','faq-state');
          //alert("22222");
        }
    });

});// close onload function



// input field resetbox

function resetBox(box, defaultvalue) {
  if (box.value == defaultvalue) {
    box.value = "";
  } else if (box.value == "") {
    box.value = defaultvalue;
  }
}

        function openclose(el, oc_state, itemtype) {

var parent_name='';
var h_temp='';
var html_temp='';

switch(itemtype)
{
 case 'mvv2':

            var el_p=el.parents('.expanding2');

    
    if (oc_state == 'open') {
        h_temp = '108px';
        html_temp = 'Hide';

    } else if (oc_state == 'close') {
        h_temp = '14px';
        html_temp = 'View more';
    }
  break;
    case 'mvv3':

            var el_p=el.parents('.expanding3');

    
    if (oc_state == 'open') {
        h_temp = '108px';
        html_temp = 'Ocultar';

    } else if (oc_state == 'close') {
        h_temp = '14px';
        html_temp = 'ver mais';
    }
  break;
case 'mvv':

            var el_p=el.parents('.expanding');

    
    if (oc_state == 'open') {
        h_temp = '108px';
        html_temp = 'ocultar';

    } else if (oc_state == 'close') {
        h_temp = '14px';
      html_temp = 'Ver más Info';
    }
  break;
    
case 'mapa':
    
    var el_p=$('#mapa');

    if (oc_state == 'open') {
        h_temp = '392px';
        html_temp = 'ocultar oficinas &contactos';
        el.removeClass('mostrar-state');

    } else if (oc_state == 'close') {
        h_temp = '28px';
        html_temp = 'mostrar oficinas &contactos';
        el.addClass('mostrar-state');
    }
      break;

case 'faq-state':
            var el_p=el.parents('.expanding');

    if (oc_state == 'open') {
        var all_p=el_p.find('p');

        h_temp = el_p.find('h2').height();

      for (var i = 0; i < all_p.length; i++) {

         h_temp += all_p.eq(i).height()+parseInt(all_p.eq(i).css('padding-bottom').substring(0,all_p.eq(i).css('padding-bottom').length-2));
              console.log(h_temp);

      };
        console.log(h_temp);

        html_temp = '-';
        el.removeClass('expanded-state');

    } else if (oc_state == 'close') {
        h_temp = '28px';
        html_temp = '+';
        el.addClass('expanded-state');
    }
      break;

}


            if (oc_state == 'open') {
               el_p.addClass('expanded-state');
            } else if (oc_state == 'close') {
               el_p.removeClass('expanded-state');
            }
        el.html(html_temp);

            el_p.animate({
                'height' : h_temp
            }, 100, function() {
            });


}
/*================ VALIDATE FUNCTIONS ========================*/
function validateEmail($email)
{
  var emailReg = new RegExp('^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+.[a-zA-Z]{2,4}$');
  return emailReg.test( $email );
}
function validateText($text)
{
  var textReg = new RegExp('^[A-Za-z0-9 ]{1,20}$');
  return textReg.test( $text );
}



$(document).ready(function(){
  $('#alertta').fancybox();
  var url=" "+window.location;

var contenedor=url.split("/");


if(contenedor[2]=="www.blulogistics.com" || contenedor[4]=="colombia-es")
{

$('#alertta').click();
}else if(contenedor[3]=="servicio-al-cliente-es")
{
$('#alertta').hide();
} 

  });



/* store locator geocode v3
------------------------------------------------------ */
var latilongi = new google.maps.LatLng(27.059126,77.31445);

var locationset = new Array();
var infowindow;

//Calculate geocode distance functions
var GeoCodeCalc = {};
GeoCodeCalc.EarthRadiusInMiles = 3956.0;
GeoCodeCalc.EarthRadiusInKilometers = 6367.0;
GeoCodeCalc.ToRadian = function(v) { return v * (Math.PI / 180);};
GeoCodeCalc.DiffRadian = function(v1, v2) {
return GeoCodeCalc.ToRadian(v2) - GeoCodeCalc.ToRadian(v1);
};
GeoCodeCalc.CalcDistance = function(lat1, lng1, lat2, lng2, radius) {
return radius * 2 * Math.asin( Math.min(1, Math.sqrt( ( Math.pow(Math.sin((GeoCodeCalc.DiffRadian(lat1, lat2)) / 2.0), 2.0) + Math.cos(GeoCodeCalc.ToRadian(lat1)) * Math.cos(GeoCodeCalc.ToRadian(lat2)) * Math.pow(Math.sin((GeoCodeCalc.DiffRadian(lng1, lng2)) / 2.0), 2.0) ) ) ) );
};


//Process form input
$(function() {
  $('#submit-btn').click(function() {
    
    var userinput = $('#map-form').serialize();
    userinput = userinput.replace("address=","");
    if (userinput == "") {
        alert("Please type in ZIP/postal code.");
      }
      var xml = $("#store-locator").attr('rel');
      var geocoder = new google.maps.Geocoder();
      var address = userinput;

    // init geocode v3
    if (geocoder) {
      geocoder.geocode({ 'address': address }, function (results, status) {

         if (status == google.maps.GeocoderStatus.OK) {
            mapping(results[0].geometry.location.lat(),results[0].geometry.location.lng(),0,xml);
         }
         else {
            console.log("Geocoding failed: " + status);
         }
      });
   }    
    
    //Replace spaces in user input
    userinput = userinput.replace(" ","+");    

  });
});


//Now all the mapping stuff
function mapping(orig_lat, orig_lng,init,url){
  var latilongi = new google.maps.LatLng(orig_lat,orig_lng);
      var xml = url;
$(function(){
      //Parse xml with jQuery
      $.ajax({
      type: "GET",
      url: xml,
      dataType: "xml",
      success: function(xml) {
      
        //After the store locations file has been read successfully
        var i = 0;
        var initialize = init || 0;
        
        $(xml).find('marker').each(function(){
          //Take the lat lng from the user, geocoded above
          var lat = $(this).find('lat').text();
          var lng = $(this).find('lng').text();
          var name = $(this).find('name').text();
          var address = $(this).find('address').text();
          var address2 = $(this).find('address2').text();
          var city = $(this).find('city').text();
          var state = $(this).find('state').text();
          var postal = $(this).find('postal').text();
          var url = $(this).find('url').text();
          var icon = $(this).find('icon').text();
          var phone = $(this).find('phone').text();
          var distance = GeoCodeCalc.CalcDistance(orig_lat,orig_lng,lat,lng,GeoCodeCalc.EarthRadiusInMiles).toFixed(1);

          //Create the array
          locationset[i] = new Array (distance, name, lat, lng, address, address2, city, state, postal, url,icon,phone);
          
          i++;                       

        });
        
        //Sort the multi-dimensional array numerically
        locationset.sort(function(a, b) {
          var x = a[0];
          var y = b[0];
          return a[0] - b[0];
        });
        
        //Create the map with jQuery
        $(function(){
            var latlng = latilongi;
            var styles = [
              {
                "stylers": [
                  { "saturation": -88 }
                ]
              },{
                "featureType": "water",
                "stylers": [
                  { "saturation": -100 },
                  { "lightness": -22 }
                ]
              },{
                "elementType": "geometry.stroke",
                "stylers": [
                  { "lightness": 100 },
                  { "saturation": -74 },
                  { "weight": 1.1 }
                ]
              },{
                "featureType": "administrative.country",
                "stylers": [
                  { "lightness": 23 },
                  { "saturation": -100 }
                ]
              },{
                "featureType": "landscape.natural.landcover",
                "stylers": [
                  { "saturation": -4 },
                  { "lightness": 20 }
                ]
              },{
              }
            ];
          
            var myOptions = {
               zoom: 5,
               center: latlng,
               mapTypeId: google.maps.MapTypeId.ROADMAP,
               styles: styles,
              scrollwheel: false
            };
            var map = new google.maps.Map(document.getElementById("map"),myOptions);
            var markers = new Array();
          
            for (var y = 0; y <= locationset.length-1; y++)
            {
              var letter = String.fromCharCode("A".charCodeAt(0) + y);
              var point = new google.maps.LatLng(locationset[y][2], locationset[y][3]);
              // marker icon
              var image = '/media/templates/'+locationset[y][10]+'.png';
              marker = createMarker(point, locationset[y][1], locationset[y][4], letter,image);
              markers[y] = marker;
             }
             
            // change zoom lvl in the beginning
            if(initialize == 1) {
               map.setZoom(2);
            }
          
             //Creat the links that focus on the related marker
             $("#list").empty();
             $(markers).each(function(y,marker){
              //HTML List to appear next to the map
               $('<li class="loc-address '+locationset[y][10]+'" />').html("<div class='loc-name'>"+locationset[y][1]+"</div><div class='loc-addr'>"+locationset[y][4]+"</div><div class='loc-addr2 laf-phone'>"+locationset[y][11]+"</div><a class='loc-addr3 laf-email' href='mailto:"+locationset[y][9]+"'>"+locationset[y][9]+"</a><div class='loc-distance hide'>~"+locationset[y][0]+" miles</div>").click(function(){
                
                map.setCenter(new google.maps.LatLng(locationset[y][2],locationset[y][6]));  
                
                if(infowindow) { infowindow.close() }
                infowindow = new InfoBox({
                  content: "<div class='info-window2'><div class='loc-name'>"+locationset[y][1]+"</div><div class='loc-addr'>"+locationset[y][4]+"</div><div class='loc-addr2'>"+locationset[y][5]+"</div><div class='loc-addr2'>"+locationset[y][6]+", "+locationset[y][7]+" "+locationset[y][8]+"</div><a target='_blank' href='mailto:"+locationset[y][9]+"' class='info-link'>"+locationset[y][9]+"</a></div>"   
                  ,disableAutoPan: false
                ,maxWidth: 0
                ,pixelOffset: new google.maps.Size(-115,-190)
                  
                ,zIndex: null
                ,boxStyle: {
                  background: "#FFF"
                  ,opacity: 1
                  ,width: "195px"
                  ,height: "92px"
                  ,padding: "10px 15px"
                  ,color: "#4d4d4d"
                 }
                ,closeBoxMargin: "0px -6px 0px 0px"
                ,closeBoxURL: "/media/templates/info-box-close.png"
                ,infoBoxClearance: new google.maps.Size(1, 1)
                ,isHidden: false
                ,pane: "floatPane"
                ,enableEventPropagation: false
             });
                
                infowindow.open(map,marker);
                
              }).appendTo("#list");                                     
            
               
              google.maps.event.addListener(marker, 'click', function() {
                
                if(infowindow) { infowindow.close() }
                infowindow = new InfoBox({
                  content: "<div class='info-window2'><div class='loc-name'>"+locationset[y][1]+"</div><div class='loc-addr'>"+locationset[y][4]+"</div><div class='loc-addr2'>"+locationset[y][5]+"</div><div class='loc-addr2'>"+locationset[y][6]+", "+locationset[y][7]+" "+locationset[y][8]+"</div><a target='_blank' href='mailto:"+locationset[y][9]+"' class='info-link'>"+locationset[y][9]+"</a></div>"   
                  ,disableAutoPan: false
                ,maxWidth: 0
                ,pixelOffset: new google.maps.Size(-115,-190)
                 
                ,zIndex: null
                ,boxStyle: {
                  background: "#FFF"
                  ,opacity: 1
                  ,width: "195px"
                  ,height: "92px"
                  ,padding: "10px 15px"
                  ,color: "#4d4d4d"
                 }
                ,closeBoxMargin: "0px -6px 0px 0px"
                ,closeBoxURL: "/media/templates/info-box-close.png"
                ,infoBoxClearance: new google.maps.Size(1, 1)
                ,isHidden: false
                ,pane: "floatPane"
                ,enableEventPropagation: false
   
             });
              
                infowindow.open(map,marker);
              });
            }); 
          
            //Custom marker function - aplhabetical
            function createMarker(point, name, address, letter,icon) {
             var marker = new google.maps.Marker({
               map:map,
               position: point,
               icon:image
             });
  
              return marker;
            }
        });
      }   
    });
  });
}

$(function() {
   // map init (center to Colombia)
   if($("#store-locator").length) {  
     $(function(){
       // init
       var url = $("#store-locator").attr('rel');
       var userinput = $('#map-form').serialize();
       userinput = userinput.replace("address=","");
       if (userinput == "") {
        //alert("Please type in ZIP/postal code.");
       }
      
       var geocoder = new google.maps.Geocoder();
       var address = userinput;

       if (geocoder) {
          geocoder.geocode({ 'address': address }, function (results, status) {

          if (status == google.maps.GeocoderStatus.OK) {
              mapping(results[0].geometry.location.lat(),results[0].geometry.location.lng(),1,url);
           } else {
            console.log("Geocoding failed: " + status);
           }
       });
       }    
       userinput = userinput.replace(" ","+");    
     });
   }
});

$.fn.si = function() {
    $.support = {
        opacity: !($.browser.msie && /MSIE 6.0/.test(navigator.userAgent))
    };
    if ($.support.opacity) {
        $(this).each(function(i) {
            if ($(this).is(":file")) {
                var $input = $(this);
                $(this).wrap('<label class="cabinet" id="cabinet'+i+'"></label>');
                $("label#cabinet"+i)
                    .wrap('<div class="si"></div>')
                    .after('<div class="uploadButton"><div></div></div><label class="selectedFile"></label>')
                    .live("mousemove", function(e) {
                    if (typeof e == 'undefined') e = window.event;
                    if (typeof e.pageY == 'undefined' &&  typeof e.clientX == 'number' && document.documentElement)
                    {
                        e.pageX = e.clientX + document.documentElement.scrollLeft;
                        e.pageY = e.clientY + document.documentElement.scrollTop;
                    };
                    
                    var ox = oy = 0;
                    var elem = this;
                    if (elem.offsetParent)
                    {
                        ox = elem.offsetLeft;
                        oy = elem.offsetTop;
                        while (elem = elem.offsetParent)
                        {
                            ox += elem.offsetLeft;
                            oy += elem.offsetTop;
                        };
                    };
        
                    var x = e.pageX - ox;
                    var y = e.pageY - oy;
                    var w = this.offsetWidth;
                    var h = this.offsetHeight;
        
                    $input.css("top", y - (h / 2)  + 'px');
                    $input.css("left", x - (w + 30) + 'px');
                });
                
                $(this).change(function() {
                    $container = $(this).closest("div.si");
                    $("label.selectedFile", $container).html($(this).val());
                })
            }
        });
    }
};

var openPopup = window.open;

function show_msg(msg) { alert(msg); }

function validateForm() {
	missing_required = 0;

	for (i = 0; i < arguments.length; i++) {
		if(arguments[i] == '') {
			missing_required = 1;
		}
	}

	if(missing_required) {
		alert("A required form field is missing.");
		return false;
	} else {
		return true;
	}
}

function expCustomLink(myURL) {
	location.href = myURL;
}

function setUrl(path) {
	document.location.href = path;
}

function expArticleLink(sectionId, articleId) {
	location.href = '/index.php?section_id=' + sectionId + '&section_copy_id=' + articleId;
}

function expPopupWindow(url, widthVal, heightVal, resizableVal, scrollbarsVal, toolbarVal, locationVal, directoriesVal, statusVal, menubarVal, copyHistoryVal) {

	var attributes = "width="  	 	 + widthVal       +
				 	 ",height=" 	 + heightVal      +
				 	 ",resizable="  + resizableVal  +
				 	 ",scrollbars="  + scrollbarsVal  +
				 	 ",toolbar=" 	 + toolbarVal 	  +
				 	 ",location=" 	 + locationVal 	  +
				 	 ",directories=" + directoriesVal +
				 	 ",status=" 	 + statusVal 	  +
				 	 ",menubar=" 	 + menubarVal 	  +
				 	 ",copyhistory=" + copyHistoryVal;

	window.open(url, 'WindowName', attributes);
}

function xprLog(msg) {
    if (window.console) console.log(msg);
}

if (document.addEventListener) {  
    document.addEventListener("DOMContentLoaded", function () { if (typeof onXprPageLoad == 'function') onXprPageLoad(); }, false);
} else if (document.attachEvent) {  
    document.attachEvent('DOMContentLoaded', function () { if (typeof onXprPageLoad == 'function') onXprPageLoad(); });
}  

<section id="upper-bar">
<div class="wrapper"><div class="Tools-section">
</div>
  <ul id="ls">
    <li id="lang-0"  class="active">
      <a href="/index.php">English</a>
    </li><li id="lang-2">
      <a href="#">Español</a>
    </li>
  </ul>

          </div>
</section>
<header id="header" class="drop-shadow">
<div class="wrapper"><a href="/index.php"><img src="/media/logo_ILTO.png" alt="" id="logo"></a><script type="text/javascript">
  $("documment").ready(function(){
    $("#servicio-al-cliente .contac-lin a").hide();
    $("#about-ilto .trackin-link-terrestre a").hide()
    $("#aduanas .trackin-link-terrestre a").hide()
    $("#aduanas .trackin-link-internacional a").hide()
    $("#transporte-terrestre .trackin-link-internacional a").hide()
    $("#almacenamiento .trackin-link-terrestre a").hide()
    $("#almacenamiento .trackin-link-internacional a").hide()
    $("#servicio-al-cliente .trackin-link-terrestre a").hide()
    $("#servicio-al-cliente .trackin-link-internacional a").hide()
    $("#about-ilto .submenu-new > a").hide()
    $("#transporte-terrestre .submenu-new > a").hide()
    $("#almacenamiento .submenu-new > a").hide()
    $("#servicio-al-cliente .submenu-new > a").hide()
    $('#aduanas .level-2 .submenu-new').hide();
    $('#servicio-al-cliente > a').attr('href','/servicio-al-cliente-es/contactenos-es/');
  });
  </script>

<nav id="main-nav" class="cf horul">
            <ul class="first-level">
            
            <li id="about-ilto">

                <a  href="/about-ilto.php" title="About ILTO"> <span class="section-name" title="About ILTO">ABOUT ILTO</span> <span class="bottom-arrow"></span> </a>
                  <ul class="level-2">
                   <div class="section-image-container">
                    <div class="section-image" style="background:url('/img/pc3.png') no-repeat center center;"></div>
                  </div>
                  <div class="sub-menu-container">
                     <li id="section2-5045">
                      <a href="#" title="Transporte aereo"><span>International  language testing  organization

</span></a><br />
At ILTO we believe in the importance of assessing the skills and knowledge of the person in the English language in order to give organizations and individuals a reliable tool to know the level of communication skills for education, work, travel or leisure.
                    <a href="/about-ilto.php" class="readmore">read more...</a></li>
                  </div>
                  </ul>
                  

              </li><li id="aduanas">

                <a  href="/tecs.php" title="TECS"> <span class="section-name" title="TECS">TECS</span> <span class="bottom-arrow"></span> </a>
                  <ul class="level-2">
                   <div class="section-image-container">
                    <div class="section-image" style="background:url('/img/pic1.png') no-repeat center center;"></div>
                  </div>
                  <div class="sub-menu-container">
                    <li id="section2-5045">
                      <a href="#" title="Transporte aereo"><span>TECS
</span></a><br />
Our main objective is to give students, education institutions and public and private companies interested in certifying the English level of their stakeholders, a reliable, practical and affordable option according to the world objective of improving the English proficiency in the countries.

                    <a href="/tecs.php" class="readmore">read more...</a></li>
                    <a href="/terms.php" class="readmore" style="margin-left: 31px;" target="_blank">Terms and Conditions</a>   
                  </div>
                  </ul>
                  

              </li><li id="transporte-terrestre">

                <a  href="/security.php" title="The test security"> <span class="section-name" title="The test security">THE TEST SECURITY</span> <span class="bottom-arrow"></span> </a>
                  <ul class="level-2">
                   <div class="section-image-container">
                    <div class="section-image" style="background:url('/img/pc4.png') no-repeat center center;"></div>
                  </div>
                  <div class="sub-menu-container">
                    <li id="section2-5045">
                      <a href="#" title="Transporte aereo"><span>Reliability
</span></a><br />
In later years it is more common to find that people who would like to study,   work of live in English speaking countries or people who are requested by organizations international exams to approve certain processes such as job promotions or school graduation who don’t have the real English proficiency and try to use different methods to the final result of the exam. Read more...


                    <a href="/security.php" class="readmore">read more...</a></li>
                  </div>
                  </ul>
                  

              </li><li id="almacenamiento">

                <a  href="/customer-service.php" title="Costumer Sevice"> <span class="section-name" title="Costumer Sevice">COSTUMER SERVICE</span> <span class="bottom-arrow"></span> </a>
                  <ul class="level-2">
                   <div class="section-image-container">
                    <div class="section-image" style="background:url('/img/pc2.png') no-repeat center center;"></div>
                  </div>
                  <div class="sub-menu-container">
                     <li id="section2-5045" style="font-size:18px">
                      <a href="#" title="Transporte aereo"><span>Contact US
</span></a><br />
<table width="344" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="78" align="right" style="text-align:right; vertical-align:top"><strong>Phone N°:</strong></td>
    <td width="10">&nbsp;</td>
    <td width="156">305-853-8088</td>
  </tr>
  <tr>
    <td  align="right" style="text-align:right; vertical-align:top"><strong>Email:</strong></td>
    <td>&nbsp;</td>
    <td>info@iltoexams.com</td>
  </tr>
  <tr>
    <td  align="right" style="text-align:right; vertical-align:top"><strong>Web site:</strong></td>
    <td>&nbsp;</td>
    <td>www.iltoexams.com</td>
  </tr>
  <tr>
    <td  align="right" valign="top" style="text-align:right; vertical-align:top"><strong>Address:</strong></td>
    <td>&nbsp;</td>
    <td>8300 NW 53rd Street Suite 350 Doral FL 33166 </td>
  </tr>
</table>
</li>
                  </div>
                  </ul>
                  

              </li></ul>
</nav></div>
</header>
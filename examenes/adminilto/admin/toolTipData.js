var FiltersEnabled = 0; // if your not going to use transitions or filters in any of the tips set this to 0
var spacer="&nbsp; &nbsp; &nbsp; ";

// email notifications to admin
notifyAdminNewMembers0Tip=["", spacer+"No email notifications to admin."];
notifyAdminNewMembers1Tip=["", spacer+"Notify admin only when a new member is waiting for approval."];
notifyAdminNewMembers2Tip=["", spacer+"Notify admin for all new sign-ups."];

// visitorSignup
visitorSignup0Tip=["", spacer+"If this option is selected, visitors will not be able to join this group unless the admin manually moves them to this group from the admin area."];
visitorSignup1Tip=["", spacer+"If this option is selected, visitors can join this group but will not be able to sign in unless the admin approves them from the admin area."];
visitorSignup2Tip=["", spacer+"If this option is selected, visitors can join this group and will be able to sign in instantly with no need for admin approval."];

// cabecera_examenes table
cabecera_examenes_addTip=["",spacer+"This option allows all members of the group to add records to the 'cabecera_examenes' table. A member who adds a record to the table becomes the 'owner' of that record."];

cabecera_examenes_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'cabecera_examenes' table."];
cabecera_examenes_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'cabecera_examenes' table."];
cabecera_examenes_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'cabecera_examenes' table."];
cabecera_examenes_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'cabecera_examenes' table."];

cabecera_examenes_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'cabecera_examenes' table."];
cabecera_examenes_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'cabecera_examenes' table."];
cabecera_examenes_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'cabecera_examenes' table."];
cabecera_examenes_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'cabecera_examenes' table, regardless of their owner."];

cabecera_examenes_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'cabecera_examenes' table."];
cabecera_examenes_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'cabecera_examenes' table."];
cabecera_examenes_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'cabecera_examenes' table."];
cabecera_examenes_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'cabecera_examenes' table."];

// departamentos table
departamentos_addTip=["",spacer+"This option allows all members of the group to add records to the 'departamentos' table. A member who adds a record to the table becomes the 'owner' of that record."];

departamentos_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'departamentos' table."];
departamentos_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'departamentos' table."];
departamentos_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'departamentos' table."];
departamentos_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'departamentos' table."];

departamentos_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'departamentos' table."];
departamentos_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'departamentos' table."];
departamentos_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'departamentos' table."];
departamentos_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'departamentos' table, regardless of their owner."];

departamentos_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'departamentos' table."];
departamentos_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'departamentos' table."];
departamentos_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'departamentos' table."];
departamentos_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'departamentos' table."];

// entidades table
entidades_addTip=["",spacer+"This option allows all members of the group to add records to the 'entidades' table. A member who adds a record to the table becomes the 'owner' of that record."];

entidades_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'entidades' table."];
entidades_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'entidades' table."];
entidades_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'entidades' table."];
entidades_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'entidades' table."];

entidades_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'entidades' table."];
entidades_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'entidades' table."];
entidades_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'entidades' table."];
entidades_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'entidades' table, regardless of their owner."];

entidades_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'entidades' table."];
entidades_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'entidades' table."];
entidades_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'entidades' table."];
entidades_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'entidades' table."];

// ex_empresas table
ex_empresas_addTip=["",spacer+"This option allows all members of the group to add records to the 'ex_empresas' table. A member who adds a record to the table becomes the 'owner' of that record."];

ex_empresas_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'ex_empresas' table."];
ex_empresas_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'ex_empresas' table."];
ex_empresas_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'ex_empresas' table."];
ex_empresas_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'ex_empresas' table."];

ex_empresas_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'ex_empresas' table."];
ex_empresas_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'ex_empresas' table."];
ex_empresas_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'ex_empresas' table."];
ex_empresas_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'ex_empresas' table, regardless of their owner."];

ex_empresas_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'ex_empresas' table."];
ex_empresas_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'ex_empresas' table."];
ex_empresas_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'ex_empresas' table."];
ex_empresas_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'ex_empresas' table."];

// ex_empresas_usuarios table
ex_empresas_usuarios_addTip=["",spacer+"This option allows all members of the group to add records to the 'ex_empresas_usuarios' table. A member who adds a record to the table becomes the 'owner' of that record."];

ex_empresas_usuarios_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'ex_empresas_usuarios' table."];
ex_empresas_usuarios_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'ex_empresas_usuarios' table."];
ex_empresas_usuarios_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'ex_empresas_usuarios' table."];
ex_empresas_usuarios_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'ex_empresas_usuarios' table."];

ex_empresas_usuarios_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'ex_empresas_usuarios' table."];
ex_empresas_usuarios_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'ex_empresas_usuarios' table."];
ex_empresas_usuarios_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'ex_empresas_usuarios' table."];
ex_empresas_usuarios_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'ex_empresas_usuarios' table, regardless of their owner."];

ex_empresas_usuarios_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'ex_empresas_usuarios' table."];
ex_empresas_usuarios_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'ex_empresas_usuarios' table."];
ex_empresas_usuarios_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'ex_empresas_usuarios' table."];
ex_empresas_usuarios_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'ex_empresas_usuarios' table."];

// ex_examenes table
ex_examenes_addTip=["",spacer+"This option allows all members of the group to add records to the 'ex_examenes' table. A member who adds a record to the table becomes the 'owner' of that record."];

ex_examenes_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'ex_examenes' table."];
ex_examenes_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'ex_examenes' table."];
ex_examenes_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'ex_examenes' table."];
ex_examenes_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'ex_examenes' table."];

ex_examenes_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'ex_examenes' table."];
ex_examenes_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'ex_examenes' table."];
ex_examenes_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'ex_examenes' table."];
ex_examenes_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'ex_examenes' table, regardless of their owner."];

ex_examenes_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'ex_examenes' table."];
ex_examenes_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'ex_examenes' table."];
ex_examenes_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'ex_examenes' table."];
ex_examenes_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'ex_examenes' table."];

// ex_examenes_preguntas table
ex_examenes_preguntas_addTip=["",spacer+"This option allows all members of the group to add records to the 'ex_examenes_preguntas' table. A member who adds a record to the table becomes the 'owner' of that record."];

ex_examenes_preguntas_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'ex_examenes_preguntas' table."];
ex_examenes_preguntas_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'ex_examenes_preguntas' table."];
ex_examenes_preguntas_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'ex_examenes_preguntas' table."];
ex_examenes_preguntas_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'ex_examenes_preguntas' table."];

ex_examenes_preguntas_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'ex_examenes_preguntas' table."];
ex_examenes_preguntas_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'ex_examenes_preguntas' table."];
ex_examenes_preguntas_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'ex_examenes_preguntas' table."];
ex_examenes_preguntas_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'ex_examenes_preguntas' table, regardless of their owner."];

ex_examenes_preguntas_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'ex_examenes_preguntas' table."];
ex_examenes_preguntas_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'ex_examenes_preguntas' table."];
ex_examenes_preguntas_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'ex_examenes_preguntas' table."];
ex_examenes_preguntas_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'ex_examenes_preguntas' table."];

// ex_preguntas table
ex_preguntas_addTip=["",spacer+"This option allows all members of the group to add records to the 'ex_preguntas' table. A member who adds a record to the table becomes the 'owner' of that record."];

ex_preguntas_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'ex_preguntas' table."];
ex_preguntas_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'ex_preguntas' table."];
ex_preguntas_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'ex_preguntas' table."];
ex_preguntas_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'ex_preguntas' table."];

ex_preguntas_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'ex_preguntas' table."];
ex_preguntas_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'ex_preguntas' table."];
ex_preguntas_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'ex_preguntas' table."];
ex_preguntas_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'ex_preguntas' table, regardless of their owner."];

ex_preguntas_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'ex_preguntas' table."];
ex_preguntas_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'ex_preguntas' table."];
ex_preguntas_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'ex_preguntas' table."];
ex_preguntas_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'ex_preguntas' table."];

// ex_preguntas_respuestas table
ex_preguntas_respuestas_addTip=["",spacer+"This option allows all members of the group to add records to the 'ex_preguntas_respuestas' table. A member who adds a record to the table becomes the 'owner' of that record."];

ex_preguntas_respuestas_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'ex_preguntas_respuestas' table."];
ex_preguntas_respuestas_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'ex_preguntas_respuestas' table."];
ex_preguntas_respuestas_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'ex_preguntas_respuestas' table."];
ex_preguntas_respuestas_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'ex_preguntas_respuestas' table."];

ex_preguntas_respuestas_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'ex_preguntas_respuestas' table."];
ex_preguntas_respuestas_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'ex_preguntas_respuestas' table."];
ex_preguntas_respuestas_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'ex_preguntas_respuestas' table."];
ex_preguntas_respuestas_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'ex_preguntas_respuestas' table, regardless of their owner."];

ex_preguntas_respuestas_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'ex_preguntas_respuestas' table."];
ex_preguntas_respuestas_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'ex_preguntas_respuestas' table."];
ex_preguntas_respuestas_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'ex_preguntas_respuestas' table."];
ex_preguntas_respuestas_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'ex_preguntas_respuestas' table."];

// ex_respuestas table
ex_respuestas_addTip=["",spacer+"This option allows all members of the group to add records to the 'ex_respuestas' table. A member who adds a record to the table becomes the 'owner' of that record."];

ex_respuestas_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'ex_respuestas' table."];
ex_respuestas_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'ex_respuestas' table."];
ex_respuestas_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'ex_respuestas' table."];
ex_respuestas_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'ex_respuestas' table."];

ex_respuestas_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'ex_respuestas' table."];
ex_respuestas_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'ex_respuestas' table."];
ex_respuestas_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'ex_respuestas' table."];
ex_respuestas_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'ex_respuestas' table, regardless of their owner."];

ex_respuestas_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'ex_respuestas' table."];
ex_respuestas_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'ex_respuestas' table."];
ex_respuestas_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'ex_respuestas' table."];
ex_respuestas_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'ex_respuestas' table."];

// ex_resultados table
ex_resultados_addTip=["",spacer+"This option allows all members of the group to add records to the 'ex_resultados' table. A member who adds a record to the table becomes the 'owner' of that record."];

ex_resultados_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'ex_resultados' table."];
ex_resultados_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'ex_resultados' table."];
ex_resultados_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'ex_resultados' table."];
ex_resultados_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'ex_resultados' table."];

ex_resultados_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'ex_resultados' table."];
ex_resultados_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'ex_resultados' table."];
ex_resultados_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'ex_resultados' table."];
ex_resultados_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'ex_resultados' table, regardless of their owner."];

ex_resultados_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'ex_resultados' table."];
ex_resultados_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'ex_resultados' table."];
ex_resultados_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'ex_resultados' table."];
ex_resultados_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'ex_resultados' table."];

// ex_usuarios table
ex_usuarios_addTip=["",spacer+"This option allows all members of the group to add records to the 'ex_usuarios' table. A member who adds a record to the table becomes the 'owner' of that record."];

ex_usuarios_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'ex_usuarios' table."];
ex_usuarios_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'ex_usuarios' table."];
ex_usuarios_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'ex_usuarios' table."];
ex_usuarios_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'ex_usuarios' table."];

ex_usuarios_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'ex_usuarios' table."];
ex_usuarios_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'ex_usuarios' table."];
ex_usuarios_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'ex_usuarios' table."];
ex_usuarios_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'ex_usuarios' table, regardless of their owner."];

ex_usuarios_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'ex_usuarios' table."];
ex_usuarios_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'ex_usuarios' table."];
ex_usuarios_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'ex_usuarios' table."];
ex_usuarios_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'ex_usuarios' table."];

// ex_usuarios_examenes table
ex_usuarios_examenes_addTip=["",spacer+"This option allows all members of the group to add records to the 'ex_usuarios_examenes' table. A member who adds a record to the table becomes the 'owner' of that record."];

ex_usuarios_examenes_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'ex_usuarios_examenes' table."];
ex_usuarios_examenes_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'ex_usuarios_examenes' table."];
ex_usuarios_examenes_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'ex_usuarios_examenes' table."];
ex_usuarios_examenes_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'ex_usuarios_examenes' table."];

ex_usuarios_examenes_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'ex_usuarios_examenes' table."];
ex_usuarios_examenes_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'ex_usuarios_examenes' table."];
ex_usuarios_examenes_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'ex_usuarios_examenes' table."];
ex_usuarios_examenes_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'ex_usuarios_examenes' table, regardless of their owner."];

ex_usuarios_examenes_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'ex_usuarios_examenes' table."];
ex_usuarios_examenes_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'ex_usuarios_examenes' table."];
ex_usuarios_examenes_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'ex_usuarios_examenes' table."];
ex_usuarios_examenes_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'ex_usuarios_examenes' table."];

// municipios table
municipios_addTip=["",spacer+"This option allows all members of the group to add records to the 'municipios' table. A member who adds a record to the table becomes the 'owner' of that record."];

municipios_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'municipios' table."];
municipios_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'municipios' table."];
municipios_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'municipios' table."];
municipios_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'municipios' table."];

municipios_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'municipios' table."];
municipios_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'municipios' table."];
municipios_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'municipios' table."];
municipios_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'municipios' table, regardless of their owner."];

municipios_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'municipios' table."];
municipios_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'municipios' table."];
municipios_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'municipios' table."];
municipios_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'municipios' table."];

// paises table
paises_addTip=["",spacer+"This option allows all members of the group to add records to the 'paises' table. A member who adds a record to the table becomes the 'owner' of that record."];

paises_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'paises' table."];
paises_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'paises' table."];
paises_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'paises' table."];
paises_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'paises' table."];

paises_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'paises' table."];
paises_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'paises' table."];
paises_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'paises' table."];
paises_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'paises' table, regardless of their owner."];

paises_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'paises' table."];
paises_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'paises' table."];
paises_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'paises' table."];
paises_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'paises' table."];

// usuarios table
usuarios_addTip=["",spacer+"This option allows all members of the group to add records to the 'usuarios' table. A member who adds a record to the table becomes the 'owner' of that record."];

usuarios_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'usuarios' table."];
usuarios_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'usuarios' table."];
usuarios_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'usuarios' table."];
usuarios_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'usuarios' table."];

usuarios_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'usuarios' table."];
usuarios_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'usuarios' table."];
usuarios_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'usuarios' table."];
usuarios_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'usuarios' table, regardless of their owner."];

usuarios_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'usuarios' table."];
usuarios_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'usuarios' table."];
usuarios_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'usuarios' table."];
usuarios_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'usuarios' table."];

/*
	Style syntax:
	-------------
	[TitleColor,TextColor,TitleBgColor,TextBgColor,TitleBgImag,TextBgImag,TitleTextAlign,
	TextTextAlign,TitleFontFace,TextFontFace, TipPosition, StickyStyle, TitleFontSize,
	TextFontSize, Width, Height, BorderSize, PadTextArea, CoordinateX , CoordinateY,
	TransitionNumber, TransitionDuration, TransparencyLevel ,ShadowType, ShadowColor]

*/

toolTipStyle=["white","#00008B","#000099","#E6E6FA","","images/helpBg.gif","","","","\"Trebuchet MS\", sans-serif","","","","3",400,"",1,2,10,10,51,1,0,"",""];

applyCssFilter();

<?php
	$d=dirname(__FILE__);
	include("$d/defaultLang.php");
	include("$d/language.php");
	include("$d/lib.php");

	// auto complete queries for all lookup fields of the application.
		$autoComplete['ex_empresas_usuarios']['cod_empresa']="SELECT `cod_empresa`, CONCAT_WS('', `cod_empresa`, ' - ', `nom_empresa`) FROM `ex_empresas` ORDER BY 2";
		$inheritPerms['ex_empresas_usuarios']['cod_empresa']=false;

		$autoComplete['ex_examenes_preguntas']['cod_examen']="SELECT `cod_examen`, CONCAT_WS('', `cod_examen`, ' - ', `nom_examen`) FROM `ex_examenes` ORDER BY 2";
		$inheritPerms['ex_examenes_preguntas']['cod_examen']=false;

		$autoComplete['ex_examenes_preguntas']['cod_pregunta']="SELECT `cod_pregunta`, CONCAT_WS('', `cod_pregunta`, ' - ', `texto_pregunta`) FROM `ex_preguntas` ORDER BY 2";
		$inheritPerms['ex_examenes_preguntas']['cod_pregunta']=false;

		$autoComplete['ex_preguntas_respuestas']['cod_pregunta']="SELECT `cod_pregunta`, CONCAT_WS('', `cod_pregunta`, ' - ', `texto_pregunta`) FROM `ex_preguntas` ORDER BY 2";
		$inheritPerms['ex_preguntas_respuestas']['cod_pregunta']=false;

		$autoComplete['ex_preguntas_respuestas']['cod_respuesta']="SELECT `cod_respuesta`, CONCAT_WS('', `cod_respuesta`, ' - ', `texto_respuesta`) FROM `ex_respuestas` ORDER BY 2";
		$inheritPerms['ex_preguntas_respuestas']['cod_respuesta']=false;

		$autoComplete['ex_resultados']['cod_pregunta']="SELECT `cod_pregunta`, CONCAT_WS('', `cod_pregunta`, ' - ', `texto_pregunta`) FROM `ex_preguntas` ORDER BY 2";
		$inheritPerms['ex_resultados']['cod_pregunta']=false;

		$autoComplete['ex_resultados']['cod_respuesta']="SELECT `cod_respuesta`, CONCAT_WS('', `cod_respuesta`, ' - ', `texto_respuesta`) FROM `ex_respuestas` ORDER BY 2";
		$inheritPerms['ex_resultados']['cod_respuesta']=false;

		$autoComplete['ex_usuarios_examenes']['key_user']="SELECT 
`key_user`, 
CONCAT_WS ('',(SELECT `nom_usuario` FROM `ex_usuarios` WHERE ex_usuarios.cod_usuario=ex_empresas_usuarios.`cod_usuario` ), ' - ', (SELECT `nom_usuario` FROM `ex_usuarios` WHERE ex_usuarios.cod_usuario=ex_empresas_usuarios.`cod_usuario` )) FROM `ex_empresas_usuarios` ORDER BY `ex_empresas_usuarios`.`key_user`  DESC";
		$inheritPerms['ex_usuarios_examenes']['key_user']=false;

		$autoComplete['ex_usuarios_examenes']['cod_examen']="SELECT `cod_examen`, CONCAT_WS('', `cod_examen`, ' - ', `nom_examen`) FROM `ex_examenes` ORDER BY 2";
		$inheritPerms['ex_usuarios_examenes']['cod_examen']=false;


	// receive and validate user input
	$val=makeSafe(iconv('UTF-8', 'us-ascii', $_POST['val']));
	$t=$_POST['t'];
	$f=$_POST['f'];
	if($val=='') die('<ul></ul>');
	if($autoComplete[$t][$f]=='') die($Translation['error:'].' Invalid table or field.');

	// does the current user have view access to the requested table?
	$arrPerm=getTablePermissions($t);
	if(!$arrPerm[1] && !$arrPerm[3]) die($Translation['tableAccessDenied']); // quit if user has no insert or edit permissions

	// get the second column of the query (the one to search)
	$query=$autoComplete[$t][$f];
	if(!preg_match('/^select .*?,(.*) from /i', $query, $m)) die($Translation['error:'].' Invalid SQL.');
	$searchField=$m[1];

	// prepare the query
	if(!preg_match('/ order by .+/i', $query)){ // if we don't have an order by clause, append one
		$query.=' ORDER BY 2';
	}
	if(strpos(strtolower($query), ' where ')){ // if we have a where clause, add and AND condition
		$query=str_ireplace(' where ', ' WHERE ( ', $query);
		$query=str_ireplace(' order by ', " ) AND $searchField LIKE '%$val%' ORDER BY ", $query);
	}else{
		$query=str_ireplace(' order by ', " WHERE $searchField LIKE '%$val%' ORDER BY ", $query);
	}
	if(!preg_match('/ limit .+/i', $query)) $query.=' LIMIT 50';

	$out='<ul>';
	$res=sql($query, $eo);
	while($row=mysql_fetch_row($res)){
		$out.="<li id=\"$row[0]\">".highlight($val, $row[1])."</li>";
	}
	if($out=='<ul>') $out .= '<li>'.$Translation['No matches found!'].'</li>';
	$out.='</ul>';

	echo $out;

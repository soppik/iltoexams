
<?php get_template_part('templates/page', 'head'); ?>
<?php $breadcrumbs = ct_show_single_post_breadcrumbs('page') ? 'yes' : 'no'; ?>


<?php if ( is_shop() ) {
	$pageTitle = woocommerce_page_title(false);
} elseif ( is_product_category() ) {
	$pageTitle = woocommerce_page_title(false);
} elseif ( is_product_tag() ) {
	$pageTitle = woocommerce_page_title(false);
} else {
	$pageTitle = ct_get_single_post_title('page');
} ?>


<?php if(is_product()): ?>
	</div>
		<section class="bg2 header-page">
		  <div class="container">
            <div class="display-table">
                <div class="table-cell">
                    <?php
                    do_action('ct_woo_custom_product_navigation');
                    ?>
                </div>
                <div class="table-cell text-right">
                    <?php
                    do_action('woo_custom_breadcrumb');
                    ?>
                </div>
            </div>
		  </div>
		</section>
    <?php do_action('header.post');?>


<?php else: ?>
	<?php if ($pageTitle || $breadcrumbs == "yes"): ?>
	</div>
        <section class="bg2 header-page">
            <div class="container">
                <div class="display-table">
                    <div class="table-cell">
                        <h2 class="uppercase"><?php echo $pageTitle ?></h2>
                    </div>
                    <div class="table-cell text-right">
                        <?php
                        do_action('woo_custom_breadcrumb');
                        ?>
                    </div>
                </div>
            </div>
        </section>
	<?php endif; ?>
<?php endif; ?>


<section class="shop-section">
	<div class="container">

		<?php if(is_product()): ?>
			<div class="row">
				<div class="col-md-12">
					<?php woocommerce_content(); ?>
				</div>
			</div>
		<?php else: ?>

			<?php // with sidebar ?>
			<?php if(1): ?>
				<div class="row">
					<div class="col-md-9 col-md-push-3">
						<?php woocommerce_content(); ?>
					</div>
					<div class="col-md-3 col-md-pull-9 leftSidebar">
						<?php get_template_part('templates/sidebar-woocommerce') ?>
					</div>
				</div>
				<!--row_end!-->
			<?php // no sidebar?>
			<?php else: ?>
				<div class="row">
					<div class="col-md-12">
						<?php woocommerce_content(); ?>
					</div>
				</div>
				<!--row_end!-->
			<?php endif; ?>

		<?php endif; ?>

	</div>
</section>



<?php if(is_product()): ?>
	<section class="section-bottom">
		<div class="container">
			<!-- custom related products -->
			<?php  echo woocommerce_custom_related_produts(); ?>
		</div>
	</section>
<?php endif; ?>




<div class="container">





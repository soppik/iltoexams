<!--container!--></div>
<?php get_template_part('templates/page', 'head'); ?>

<?php $breadcrumbs = ct_show_index_post_breadcrumbs('post') ? 'yes' : 'no'; ?>
<?php if (ct_get_option("posts_index_show_p_title", 1) || $breadcrumbs == "yes"): ?>
    <?php $pageTitle = (ct_get_option("posts_index_show_p_title", 1)) ? get_the_title(get_option('page_for_posts')) : ''; ?>
    <?php echo do_shortcode('[title_row context="post_single" header="' . $pageTitle . '" breadcrumbs="' . $breadcrumbs . '"]') ?>
<?php endif ?>

<section class="container section">
    <div class="row">
        <div class="col-sm-8 col-md-9">
            <?php get_template_part('templates/content-single'); ?>
        </div>
        <!--col-md-9 end!-->


        <?php if (ct_get_option('posts_single_sidebar')): ?>
            <div class="col-sm-4 col-md-3 sidebar">
                <?php get_template_part('templates/sidebar') ?>
            </div>
        <?php endif ?>
    </div>
    <!--row_end!-->
</section>
<div class="container">
<?php

/**
 * Button shortcode
 */
class ctProgressBarShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface
{

    /**
     * Returns name
     * @return string|void
     */
    public function getName()
    {
        return 'Progress bar';
    }

    /**
     * Shortcode name
     * @return string
     */
    public function getShortcodeName()
    {
        return 'progress_bar';
    }

    /**
     * Shortcode type
     * @return string
     */
    public function getShortcodeType()
    {
        return self::TYPE_SHORTCODE_ENCLOSING;
    }

    public function enqueueScripts()
    {


        wp_register_script('ct-appear', CT_THEME_ASSETS . '/js/jquery.appear.js', array('jquery'), false, true);
        wp_enqueue_script('ct-appear');

    }


    /**
     * Handles shortcode
     * @param $atts
     * @param null $content
     * @return string
     */

    public function handle($atts, $content = null)
    {
        $valuenow = '';
        $valuemin = '';
        $valuemax = '';
        $attributes = shortcode_atts($this->extractShortcodeAttributes($atts), $atts);


        extract($attributes);
        $this->addInlineJS($this->getInlineJS($attributes), true);


        //$this->addInlineJS($this->getInlineJS($attributes), true);

        $mainContainerAtts = array(
            'class' => array(
                'progress',
                'type' . $type
            ),
        );

        $label = $label != '' ? '<span class="bar-info uppercase">' . $label . '</span>' : '';

        $html = $label . '


        <div' . $this->buildContainerAttributes($mainContainerAtts, $atts) . '>
                        <div class="progress-bar"  role="progressbar" data-barcolor="' . $bar_color . '" data-percentage="' . (int)$percentage . '" aria-valuenow="' . (int)$valuenow . '"
                             aria-valuemin="' . (int)$valuemin . '" aria-valuemax="' . (int)$valuemax . '">
                            ' . (($tooltip != 'no') ? ' <span>' . (int)$valuenow . '%</span>' : '') . '
                        </div>
                    </div>
        ';
        return do_shortcode($html);
    }


    /**
     * returns JS
     * @param $id
     * @return string
     */
    protected function getInlineJS($attributes)
    {
        return '

     /* ====================== */
    /* ==== PROGRESS BAR ==== */

    if ((jQuery().appear) && (jQuery("body").hasClass("cssAnimate"))) {
        jQuery(".progress").appear(function () {
            var $this = jQuery(this);
            $this.each(function () {
                var $innerbar = $this.find(".progress-bar");
                var percentage = $innerbar.attr("data-percentage");
                var barcolor = "#" + $innerbar.attr("data-barcolor");
                $innerbar.css("background-color", barcolor);
                $innerbar.addClass("animating").css("width", percentage + "%");

                $innerbar.on("transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd", function () {
                    $innerbar.find(".pull-right").fadeIn(900);
                });

            });
    }, {accY: -100});
    } else {
        jQuery(".progress").each(function () {
            var $this = jQuery(this);
            var $innerbar = $this.find(".progress-bar");
            var percentage = $innerbar.attr("data-percentage");
            $innerbar.addClass("animating").css("width", percentage + "%");

            $innerbar.on("transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd", function () {
                $innerbar.find(".pull-right").fadeIn(900);
            });

        });
    }


         ';
    }


    /**
     * Returns config
     * @return null
     */
    public function getAttributes()
    {
        return array(
            'percentage' => array('label' => __('Percentage', 'ct_theme'), 'type' => "input"),
            'valuenow' => array('label' => __('Tooltip text', 'ct_theme'), 'type' => "input", 'default' => 0),
            'valuemin' => array('label' => __('Min value', 'ct_theme'), 'type' => "input", 'help' => __("By default Progress Bar starts at 0", 'ct_theme')),
            'valuemax' => array('label' => __('Max value' , 'ct_theme'), 'type' => "input", 'default' => 100),
            'label' => array('label' => __('Label', 'ct_theme'), 'default' => '', 'type' => 'input'),

            'bar_color' => array('label' => __('Bar color', 'ct_theme'), 'type' => "colorpicker", 'default' => '#d30000'),
            'type' => array('label' => __('Type', 'ct_theme'), 'default' => '2', 'type' => 'select',
                'choices' => array('2' => __('Flat', 'ct_theme'), '1' => 'Normal'), 'help' => __("Progress bar style", 'ct_theme')),
            'tooltip' => array('label' => __('Tooltip', 'ct_theme'), 'default' => 'yes', 'type' => 'select', 'choices' => array('yes' => __('yes', 'ct_theme'), 'no' => __('no', 'ct_theme')),),

        );
    }

	/**
		 * Returns additional info about VC
		 * @return ctVisualComposerInfo
		 */
		public function getVisualComposerInfo() {
			return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-align-left' ) );
		}
}

new ctProgressBarShortcode();



<?php

/**
 * Bar shortcode
 */
class ctCounterShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Counter';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'counter';
	}


	public function enqueueScripts() {
		wp_register_script( 'countTo', CT_THEME_ASSETS . '/js/jquery.countTo.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'countTo' );

		wp_register_script( 'countTo-init', CT_THEME_ASSETS . '/js/jscalls/countcall.js', array( 'countTo' ), false, true );
		wp_enqueue_script( 'countTo-init' );
	}


	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );

		$mainContainerAtts = array(
			'class' => array(
				'big',
				$class
			),
		);

		if ( $color != '' ) {
			$mainContainerAtts['data-color'] = $color;
		}

		if ( $postscript != '' ) {
			$html = '
       <span ' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>
       <span class="counter" data-to="' . $this->normalizeNum( $to ) . '" data-speed="' . $speed . '">0</span>*</span><br>*' . $postscript;
		} else {
			$html = '
       <span ' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>
       <span class="counter" data-to="' . $this->normalizeNum( $to ) . '" data-speed="' . $speed . '">0</span></span>';
		}


		return $html;
	}


	/**
	 * Normalize number
	 *
	 * @param $num
	 *
	 * @return mixed
	 */

	protected function normalizeNum( $num ) {
		return str_replace( array( ' ', ',' ), '', $num );
	}


	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'value'      => array( 'label' => __( 'Value', 'ct_theme' ), 'default' => 0, 'type' => 'input' ),
			'to'         => array( 'label' => __( 'Count to', 'ct_theme' ), 'default' => 0, 'type' => 'input' ),
			'speed'      => array( 'label' => __( 'Speed', 'ct_theme' ), 'default' => '6000', 'type' => 'input' ),
			'postscript' => array( 'label' => __( 'Postscript', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'color'      => array(
				'label'   => __( 'Color', 'ct_theme' ),
				'type'    => "colorpicker",
				'default' => '#d30000'
			),
			'class'      => array(
				'label'   => __( "Custom class", 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( "Custom class name", 'ct_theme' )
			),
		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-pie-chart' ) );
	}
}

new ctCounterShortcode();




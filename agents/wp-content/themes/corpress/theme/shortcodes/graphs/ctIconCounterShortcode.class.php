<?php

/**
 * Bar shortcode
 */
class ctIconCounterShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Icon Counter';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'icon_counter';
	}

	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );
		$id = 'iconCounter' . rand( 100, 1000 );


		$mainContainerAtts = array(
			'class'           => array(
				'progress-icons',
				$class
			),
			'data-font-size'  => $icon_size,
			'data-icon-color' => '#' . str_replace( '#', '', $icon_color ),
			'data-active'     => $this->normalizeNum( $active ),
			'data-total'      => $this->normalizeNum( $total ),
			'data-icon'       => $icon,
			'data-delay'      => 20,
			'id'              => $id
		);


		$this->addInlineJS( $this->getInlineJS( $id ) );

		return '<div' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '></div>';
	}

	/**
	 * Normalize number
	 *
	 * @param $num
	 *
	 * @return mixed
	 */

	protected function normalizeNum( $num ) {
		return str_replace( array( ' ', ',' ), '', $num );
	}


	/**
	 * returns inline js
	 *
	 * @param $attributes
	 * @param $id
	 *
	 * @return string
	 */
	protected function getInlineJS( $id ) {

		return 'jQuery("#' . $id . '").each(function () {
            var $this = jQuery(this);
            var $total = $this.attr("data-total");
            var $icon = $this.attr("data-icon");
            var htmldata = "";

            $this.css("font-size", ($this.attr("data-font-size") + "px"));

            var i;
            for (i = 0; i < $total; i++) {
                htmldata += "<i class=\'fa " + $icon + "\'></i> ";
            }

            $this.html(htmldata);

            if ((jQuery().appear) && (jQuery("body").hasClass("cssAnimate"))) {

                jQuery("#' . $id . '").appear(function () {
                    var $this = jQuery(this);

                    var $active = $this.attr("data-active");
                    var $icons = $this.find("i:lt(" + $active + ")");
                    var $delay = parseInt($this.attr("data-delay"));

                    var delay = $delay;
                    for (i = 0; i < $icons.length; i++) {
                        setTimeout((function (i) {
                            return function () {
                                i.style.color = $this.attr("data-icon-color");
                            }
                        })($icons[i]), delay);
                        delay += $delay;
                    }
                }, {accY: -100});
            } else {
                $this.each(function () {
                    var $active = $this.attr("data-active");
                    var $icons = $this.find("i:lt(" + $active + ")");
                    $icons.css("color", $this.attr("data-icon-color"))
                });
            }
        })';
	}


	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'icon'       => array(
				'label'   => __( 'Icon name', 'ct_theme' ),
				'type'    => "icon",
				'default' => '',
				'link'    => CT_THEME_ASSETS . '/shortcode/awesome/index.html'
			),
			'icon_color' => array(
				'label'   => __( 'Icon color', 'ct_theme' ),
				'type'    => "colorpicker",
				'default' => '#d30000'
			),
			'icon_size'  => array( 'label' => __( 'Icon Size', 'ct_theme' ), 'type' => "input", 'default' => 25 ),
			'total'      => array(
				'label'   => __( 'Total icons', 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( "The total number of icons", 'ct_theme' )
			),
			'active'     => array(
				'label'   => __( 'Active icons', 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( "The number of active icons", 'ct_theme' )
			),
			'class'      => array(
				'label'   => __( "Custom class", 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( "Custom class name", 'ct_theme' )
			),
		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-pie-chart' ) );
	}
}

new ctIconCounterShortcode();




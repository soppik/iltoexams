<?php

/**
 * Tab shortcode
 */
class  ctLineGraphItemShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Tabs counter
	 * @var int
	 */

	protected static $counter = 0;

	/**
	 * @inheritdoc
	 */
	public function __construct() {
		parent::__construct();

		//connect for additional code
		//remember - method must be PUBLIC!
		$this->connectPreFilter( 'line_graph', array( $this, 'handlePreFilter' ) );
	}


	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Line Graph Item';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'line_graph_item';
	}

	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */
	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );
		$counter = ++ self::$counter;


		//add for pre filter data. Adds any data to this shortcode type
		$this->setData( $counter, array(
			'data'                  => array_map( 'floatval', explode( ",", $values ) ),
			'label'                 => $label,
			'fillColor'             => $fill_color,
			'strokeColor'           => $stroke_color,
			'pointColor'            => $point_color,
			'pointStrokeColor'      => $point_stroke_color,
			'pointHighlightFill'    => $point_highlight_fill,
			'pointHighlightStroke:' => $point_highlight_stroke,
		) );

		return '';
	}

	/**
	 * Adds content before filters
	 *
	 * @param string $content
	 *
	 * @return string
	 */
	public function handlePreFilter( $content ) {
		$values = array();
		//here - add all available content
		foreach ( $this->getAllData() as $data ) {
			$values[] = $data;
		}

		return $values;
	}

	/**
	 * Parent shortcode name
	 * @return null
	 */

	public function getParentShortcodeName() {
		return 'line_graph';
	}


	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'values'                 => array(
				'label'   => __( 'Values', 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( 'Comma separated values ex. 4,6', 'ct_theme' )
			),
			'label'                  => array(
				'label'   => __( 'Series label', 'ct_theme' ),
				'default' => '',
				'type'    => 'input'
			),
			'fill_color'             => array(
				'label'   => __( 'Fill Color', 'ct_theme' ),
				'default' => '',
				'type'    => 'colorpicker'
			),
			'stroke_color'           => array(
				'label'   => __( 'Stroke Color', 'ct_theme' ),
				'default' => '',
				'type'    => 'colorpicker'
			),
			'point_color'            => array(
				'label'   => __( 'Point Color', 'ct_theme' ),
				'default' => '',
				'type'    => 'colorpicker'
			),
			'point_stroke_color'     => array(
				'label'   => __( 'Point stroke Color', 'ct_theme' ),
				'default' => '',
				'type'    => 'colorpicker'
			),
			'point_highlight_fill'   => array(
				'label'   => __( 'Point Highlight Color', 'ct_theme' ),
				'default' => '',
				'type'    => 'colorpicker'
			),
			'point_highlight_stroke' => array(
				'label'   => __( 'Point Highlight Stroke', 'ct_theme' ),
				'default' => '',
				'type'    => 'colorpicker'
			),
		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-pie-chart' ) );
	}
}


new ctLineGraphItemShortcode();
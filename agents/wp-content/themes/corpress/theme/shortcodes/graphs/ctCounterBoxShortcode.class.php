<?php

/**
 * Bar shortcode
 */
class ctCounterBoxShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Counter Box';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'counter_box';
	}

	public function enqueueScripts() {
		wp_register_script( 'countTo', CT_THEME_ASSETS . '/js/jquery.countTo.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'countTo' );

		wp_register_script( 'countTo-init', CT_THEME_ASSETS . '/js/jscalls/countcall.js', array( 'countTo' ), false, true );
		wp_enqueue_script( 'countTo-init' );
	}

	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );

		$mainContainerAtts = array(
			'class' => array(
				'icon-counter',
				$class
			),
		);

		$html = '

        <div ' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>
                        <div class="square-icon-box square-icon-small">
                            <i class="fa ' . $icon . '"></i>
                        </div>
                    <span class="counter hr-mid" data-to="' . $this->normalizeNum( $to ) . '">
                        ' . $value . '
                    </span>
                    <span class="description">
                        ' . $text . '
                    </span>
                    </div>


        ';

		return $html;
	}

	/**
	 * Normalize number
	 *
	 * @param $num
	 *
	 * @return mixed
	 */

	protected function normalizeNum( $num ) {
		return str_replace( array( ' ', ',' ), '', $num );
	}

	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'icon'  => array(
				'label'   => __( 'Icon name', 'ct_theme' ),
				'type'    => "icon",
				'default' => '',
				'link'    => CT_THEME_ASSETS . '/shortcode/awesome/index.html'
			),
			'value' => array( 'label' => __( 'Value', 'ct_theme' ), 'default' => 0, 'type' => 'input' ),
			'to'    => array( 'label' => __( 'Count to', 'ct_theme' ), 'default' => 0, 'type' => 'input' ),
			'text'  => array( 'label' => __( 'Text', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'class' => array(
				'label'   => __( "Custom class", 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( "Custom class name", 'ct_theme' )
			),
		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-pie-chart' ) );
	}
}

new ctCounterBoxShortcode();





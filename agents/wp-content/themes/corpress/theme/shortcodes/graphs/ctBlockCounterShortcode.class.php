<?php

/**
 * Bar shortcode
 */
class ctBlockCounterShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Block Counter';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'block_counter';
	}


	public function enqueueScripts() {
		wp_register_script( 'countTo', CT_THEME_ASSETS . '/js/jquery.countTo.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'countTo' );

		wp_register_script( 'countTo-init', CT_THEME_ASSETS . '/js/jscalls/countcall.js', array( 'countTo' ), false, true );
		wp_enqueue_script( 'countTo-init' );
	}


	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );

		$mainContainerAtts = array(
			'class' => array(
				'row',
				$class
			),
		);


		return '

        <div ' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>
<div class="col-sm-5">
<span class="aside-title uppercase">' . $header . '</span>
<span class="h2 motive uppercase">' . $sub_header . '</span>
<br>
<br>
' . $description . '
</div>
<div class="col-sm-7">
<span class="big" data-color="2b8be9">
<span class="counter" data-to="' . $this->normalizeNum( $to ) . '" data-speed="' . $speed . '">0</span>
</span>
<br>
<h4 class="uppercase">' . $footer . '</h4>
</div>
</div>
           ';
	}


	/**
	 * Normalize number
	 *
	 * @param $num
	 *
	 * @return mixed
	 */

	protected function normalizeNum( $num ) {
		return str_replace( array( ' ', ',' ), '', $num );
	}

	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'header'      => array( 'label' => __( 'Header', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'sub_header'  => array( 'label' => __( 'Subheader', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'description' => array(
				'label'   => __( 'Description', 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( "Appears below the number", 'ct_theme' )
			),
			'footer'      => array( 'label' => __( 'Footer', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'to'          => array( 'label' => __( 'Count to', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'speed'       => array( 'label' => __( 'Speed', 'ct_theme' ), 'default' => '2000', 'type' => 'input' ),
			'class'       => array(
				'label'   => __( "Custom class", 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( "Custom class name", 'ct_theme' )
			),
		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-pie-chart' ) );
	}
}

new ctBlockCounterShortcode();





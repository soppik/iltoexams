<?php

/**
 * Tabs shortcode
 */
class ctSimplePieChartShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Simple Pie Chart';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'simple_pie_chart';
	}

	public function enqueueScripts() {

		wp_register_script( 'chart', CT_THEME_ASSETS . '/js/Chart.min.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'chart' );
	}


	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		$attributes = shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts );
		extract( $attributes );
		$id = 'simplePieChart' . rand( 100, 1000 );

		//parse shortcode before filters
		do_shortcode( $content );

		$json = json_encode( $this->callPreFilter( '' ) );


		//clean current tab cache
		$this->cleanData( 'simple_pie_chart_item' );

		//var_dump( self::$data);
		$this->addInlineJS( $this->getInlineJS( $attributes, $id, $json ) );


		$html = '
                <div class="graph">
                    <canvas id="' . $id . '" width="' . $width . '" height="' . $height . '"></canvas>
                </div>
        ';


		return do_shortcode( $html );
	}

	/**
	 * returns inline js
	 *
	 * @param $attributes
	 *
	 * @return string
	 */
	protected function getInlineJS( $attributes, $id, $json ) {
		//var_dump($json);
		extract( $attributes );
		if ( $show_tooltips == 'yes' || $show_tooltips == 'true' ) {
			$show_tooltips = 'true';
		} else {
			$show_tooltips = 'false';
		}

		return '
        var graph3options = {
        responsive: true,
        percentageInnerCutout : 0,
        showTooltips: ' . $show_tooltips . '
    }
var graph3doughnutData' . $id . ' = ' . $json . '
if(jQuery().appear && jQuery("body").hasClass("cssAnimate")) {
    jQuery("#' . $id . '").appear(function () {
        var ctx = jQuery(this)[0].getContext("2d");
        window.myDoughnut = new Chart(ctx).Doughnut(graph3doughnutData' . $id . ', graph3options);
    });
} else{
    var ctx = document.getElementById("' . $id . '").getContext("2d");
    window.myDoughnut = new Chart(ctx).Doughnut(graph3doughnutData' . $id . ', graph3options);
}

  ';
	}

	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'show_tooltips' => array(
				'label'   => __( 'Show Tooltips', 'ct_theme' ),
				'default' => 'yes',
				'type'    => 'select',
				'choices' => array(
					'yes' => __( 'Yes', 'ct_theme' ),
					'no'  => __( 'No', 'ct_theme' )
				),
				'help'    => __( 'Show tooltips to values', 'ct_theme' ),
				'ct_theme'
			),
			'width'         => array( 'default' => '150', 'type' => 'input', 'label' => __( 'Width', 'ct_theme' ) ),
			'height'        => array( 'default' => '150', 'type' => 'input', 'label' => __( 'Height', 'ct_theme' ) ),
			'class'         => array(
				'label'   => __( 'Custom class', 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( 'Adding custom class allows you to set diverse styles in css to the element. Type in name of class, which you defined in css. You can add as much classes as you like.', 'ct_theme' )
			),
		);
	}

	/**
	 * Child shortcode info
	 * @return array
	 */

	public function getChildShortcodeInfo() {
		return array( 'name' => 'simple_pie_chart_item', 'min' => 1, 'max' => 100, 'default_qty' => 1 );
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-pie-chart' ) );
	}
}

new ctSimplePieChartShortcode();


//#28146
if(class_exists('WPBakeryShortCodesContainer')){
	class WPBakeryShortcode_simple_pie_chart extends WPBakeryShortCodesContainer{}
}
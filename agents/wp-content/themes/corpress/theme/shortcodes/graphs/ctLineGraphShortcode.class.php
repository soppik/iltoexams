<?php

/**
 * Tabs shortcode
 */
class ctLineGraphShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Line Graph';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'line_graph';
	}

	public function enqueueScripts() {
		wp_register_script( 'chart', CT_THEME_ASSETS . '/js/Chart.min.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'chart' );


	}


	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		$attributes = shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts );
		extract( $attributes );
		$id = 'lineChart' . rand( 100, 1000 );

		//parse shortcode before filters
		do_shortcode( $content );

		$json = json_encode( array( 'labels' => explode( ",", $labels ), 'datasets' => $this->callPreFilter( '' ) ) );


		//clean current tab cache
		$this->cleanData( 'line_graph_item' );

		//var_dump( self::$data);
		$this->addInlineJS( $this->getInlineJS( $attributes, $id, $json ) );

		$html = '
                <div>
                    <canvas id="' . $id . '" width="818" height="409"></canvas>
                </div>
        ';


		return do_shortcode( $html );
	}

	public function getChildShortcodeInfo() {
		return array( 'name' => 'line_graph_item', 'min' => 1, 'max' => 100, 'default_qty' => 5 );
	}


	/**
	 * returns inline js
	 *
	 * @param $attributes
	 *
	 * @return string
	 */
	protected function getInlineJS( $attributes, $id, $json ) {

		extract( $attributes );


		return '
    var dataLine = ' . $json . ';
    var canvas3 = document.getElementById("' . $id . '");
    var LineChart = new Chart(canvas3.getContext("2d")).Line(dataLine, {
        responsive: true
    });

  ';
	}

	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'labels' => array(
				'label'   => __( 'Labels', 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => array( 'Comma separated values ex. lab1,lab2', 'ct_theme' )
			),
			'class'  => array(
				'label'   => __( 'Custom class', 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( 'Adding custom class allows you to set diverse styles in css to the element. Type in name of class, which you defined in css. You can add as much classes as you like.', 'ct_theme' )
			),

		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-pie-chart' ) );
	}
}

new ctLineGraphShortcode();

//#28146
if(class_exists('WPBakeryShortCodesContainer')){
	class WPBakeryShortcode_line_graph extends WPBakeryShortCodesContainer{}
}
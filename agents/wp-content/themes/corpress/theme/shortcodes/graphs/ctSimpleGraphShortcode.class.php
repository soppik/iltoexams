<?php

/**
 * Button shortcode
 */
class ctSimpleGraphShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Simple Graph';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'simple_graph';
	}

	/**
	 * Shortcode type
	 * @return string
	 */
	public function getShortcodeType() {
		return self::TYPE_SHORTCODE_ENCLOSING;
	}


	public function enqueueScripts() {

		wp_register_script( 'chart', CT_THEME_ASSETS . '/js/Chart.min.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'chart' );

	}

	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		$attributes = shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts );
		extract( $attributes );


		$this->addInlineJS( $this->getInlineJS( $attributes ), true );
		$mainContainerAtts = array(
			'class' => array(
				'text-center',
				$class
			),
		);

		$html = '
        <div ' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>
                <div' . $this->buildAttributes( array(
				'class'             => array( 'graph' ),
				'data-margin-top'   => $margin_top,
				'data-margin-left'  => $margin_left,
				'data-margin-right' => $margin_right,
			) ) . '>
                        <span class="percentage">' . $percentage . '%</span>
                    <canvas width="' . $width . '" height="' . $height . '" class="chart"' . $this->buildAttributes( array(
				'data-middle-space' => '90',
				'data-show-stroke'  => 'false',
				'data-first-color'  => $first_color,
				'data-second-color' => $second_color,
				'data-percentage'   => $percentage
			) ) . '></canvas>
                </div>
                <span class="h4">' . $text . '</span>
            </div>
        ';

		return do_shortcode( $html );
	}

	/**
	 * returns JS
	 *
	 * @param $id
	 *
	 * @return string
	 */
	protected function getInlineJS( $attributes ) {
		return '
		/* ==================== */
		/* ==== PIE CHARTS ==== */
		jQuery(\'.chart\').each(function () {
			var $this = jQuery(this);
			var $color = $this.attr(\'data-first-color\');
			var $color2 = $this.attr(\'data-second-color\');
			var $cutout = $this.attr(\'data-middle-space\');
			var $stroke = $this.attr(\'data-show-stroke\');

			var options = {
				responsive: true, percentageInnerCutout: $cutout, segmentShowStroke: $stroke, showTooltips: false
			}
			var doughnutData = [{
				value: parseInt($this.attr(\'data-percentage\')), color: $color, label: false
			}, {
				value: parseInt(100 - $this.attr(\'data-percentage\')), color: $color2
			}];

			if ((jQuery().appear) && (jQuery("body").hasClass("cssAnimate"))) {
				jQuery(\'.chart\').appear(function () {
					var ctx = $this[0].getContext("2d");
					window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, options);
				});
			} else {
				var ctx = $this[0].getContext("2d");
				window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, options);
			}
		})';
	}

	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {

		return array(
			'text'         => array( 'label' => __( 'Text below the Graph', 'ct_theme' ), 'type' => "input" ),
			'percentage'   => array( 'label' => __( 'Percentage', 'ct_theme' ), 'type' => "input" ),
			'width'        => array(
				'label'   => __( 'Width (in px)', 'ct_theme' ),
				'type'    => "input",
				'default' => '133'
			),
			'height'       => array(
				'label'   => __( 'Height (in px)', 'ct_theme' ),
				'type'    => "input",
				'default' => '133'
			),
			'first_color'  => array(
				'label'   => __( 'First color', 'ct_theme' ),
				'default' => '#2b8be9',
				'type'    => 'colorpicker'
			),
			'second_color' => array(
				'label'   => __( 'Second color', 'ct_theme' ),
				'default' => '#eaebec',
				'type'    => 'colorpicker'
			),
			'margin_left'  => array(
				'label'   => __( 'Left margin', 'ct_theme' ),
				'default' => '#',
				'type'    => 'input'
			),
			'margin_right' => array(
				'label'   => __( 'Right margin', 'ct_theme' ),
				'default' => '0',
				'type'    => 'input',
			),
			'margin_top'   => array(
				'label'   => __( 'Top margin', 'ct_theme' ),
				'default' => '0',
				'type'    => 'input'
			),
			'class'        => array(
				'label' => __( 'Custom class', 'ct_theme' ),
				'type'  => "input",
				'help'  => __( 'Adding custom class allows you to set diverse styles in css to the element. Type in name of class, which you defined in css. You can add as much classes as you like.', 'ct_theme' )
			),

		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-pie-chart' ) );
	}
}

new ctSimpleGraphShortcode();

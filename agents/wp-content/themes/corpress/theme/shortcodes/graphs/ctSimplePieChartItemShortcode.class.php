<?php

/**
 * Tab shortcode
 */
class  ctSimplePieChartItemShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Tabs counter
	 * @var int
	 */

	protected static $counter = 0;

	/**
	 * @inheritdoc
	 */
	public function __construct() {
		parent::__construct();

		//connect for additional code
		//remember - method must be PUBLIC!
		$this->connectPreFilter( 'simple_pie_chart', array( $this, 'handlePreFilter' ) );
	}


	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Simple Pie Chart Item';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'simple_pie_chart_item';
	}

	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */
	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );
		$counter = ++ self::$counter;

		//add for pre filter data. Adds any data to this shortcode type
		$this->setData( $counter, array( 'value' => (float) $value, 'label' => $label, 'color' => $color ) );

		return '';
	}


	/**
	 * Adds content before filters
	 *
	 * @param string $content
	 *
	 * @return string
	 */
	public function handlePreFilter( $content ) {
		$values = array();
		//here - add all available content
		foreach ( $this->getAllData() as $data ) {
			$values[] = $data;
		}

		return $values;
	}

	/**
	 * Parent shortcode name
	 * @return null
	 */

	public function getParentShortcodeName() {
		return 'simple_pie_chart';
	}


	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'value' => array( 'label' => __( 'Value (in %)', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'label' => array( 'label' => __( 'Label', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'color' => array( 'label' => __( 'Color', 'ct_theme' ), 'default' => '', 'type' => 'colorpicker' ),
		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-pie-chart' ) );
	}
}

new ctSimplePieChartItemShortcode();
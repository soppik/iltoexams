<?php

/**
 * Tabs shortcode
 */
class ctPieChartShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Pie Chart';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'pie_chart';
	}


	public function enqueueScripts() {

		wp_register_script( 'chart', CT_THEME_ASSETS . '/js/Chart.min.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'chart' );
	}


	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		$attributes = shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts );
		extract( $attributes );
		$id = 'pieChart' . rand( 100, 1000 );
		//parse shortcode before filters
		do_shortcode( $content );

		$json = json_encode( $this->callPreFilter( '' ) );

		//clean current tab cache
		$this->cleanData( 'pie_chart_item' );

		$this->addInlineJS( $this->getInlineJS( $attributes, $id, $json ) );

		$mainContainerAtts = array(
			'class' => array(
				$legend_position == 'side' ? 'chart-with-legend' : 'chart-with-horizontal-legend',
				$class
			),
		);

		$html = '
                <div' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>
                    <canvas id="' . $id . '" width="' . $width . '" height="' . $height . '"></canvas>
                </div>
        ';


		return do_shortcode( $html );
	}

	/**
	 * returns inline js
	 *
	 * @param $attributes
	 *
	 * @return string
	 */
	protected function getInlineJS( $attributes, $id, $json ) {
		//var_dump($json);
		extract( $attributes );


		return '
    // Modular doughnut
    helpers = Chart.helpers;
    var canvas'. $id .' = document.getElementById("' . $id . '");

    var moduleData'. $id .' = ' . $json . ';

    if(jQuery().appear && jQuery("body").hasClass("cssAnimate")) {
        jQuery("#' . $id . '").appear(function () {
            //
            var moduleDoughnut = new Chart(canvas'. $id .'.getContext("2d")).Doughnut(moduleData'. $id .', { tooltipTemplate : "<%= value %>%", responsive: true});

            //
            var legendHolder = document.createElement("div");
            legendHolder.innerHTML = moduleDoughnut.generateLegend();
            // Include a html legend template after the module doughnut itself
            helpers.each(legendHolder.firstChild.childNodes, function(legendNode, index){
                helpers.addEvent(legendNode, "mouseover", function(){
                    var activeSegment = moduleDoughnut.segments[index];
                    activeSegment.save();
                    activeSegment.fillColor = activeSegment.highlightColor;
                    moduleDoughnut.showTooltip([activeSegment]);
                    activeSegment.restore();
                });
            });
            canvas'. $id .'.parentNode.parentNode.appendChild(legendHolder.firstChild);

        });

}else{
    //

    var moduleDoughnut = new Chart(canvas'. $id .'.getContext("2d")).Doughnut(moduleData'. $id .', { tooltipTemplate : "<%= value %>%", responsive: true});
        //
        var legendHolder = document.createElement("div");
        legendHolder.innerHTML = moduleDoughnut.generateLegend();
        // Include a html legend template after the module doughnut itself
        helpers.each(legendHolder.firstChild.childNodes, function(legendNode, index){
            helpers.addEvent(legendNode, "mouseover", function(){
                var activeSegment = moduleDoughnut.segments[index];
                activeSegment.save();
                activeSegment.fillColor = activeSegment.highlightColor;
                moduleDoughnut.showTooltip([activeSegment]);
                activeSegment.restore();
            });
        });
        canvas'. $id .'.parentNode.parentNode.appendChild(legendHolder.firstChild);
    }
  ';
	}

	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'legend_position' => array(
				'label'   => __( 'Legend position', 'ct_theme' ),
				'default' => 'side',
				'type'    => 'select',
				'choices' => array(
					'side'   => __( 'Side', 'ct_theme' ),
					'bottom' => __( 'Bottom', 'ct_theme' )
				),
				'help'    => __( 'Position where legend should appear', 'ct_theme' ),
				'ct_theme'
			),
			'width'           => array( 'default' => '150', 'type' => 'input', 'label' => __( 'Width', 'ct_theme' ) ),
			'height'          => array( 'default' => '150', 'type' => 'input', 'label' => __( 'Height', 'ct_theme' ) ),
			'class'           => array(
				'label'   => __( 'Custom class', 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( 'Adding custom class allows you to set diverse styles in css to the element. Type in name of class, which you defined in css. You can add as much classes as you like.', 'ct_theme' )
			),

		);
	}

	public function getChildShortcodeInfo() {
		return array( 'name' => 'pie_chart_item', 'min' => 1, 'max' => 1000, 'default_qty' => 3 );
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-pie-chart' ) );
	}

}

new ctPieChartShortcode();

//#28146
if(class_exists('WPBakeryShortCodesContainer')){
	class WPBakeryShortcode_pie_chart extends WPBakeryShortCodesContainer{}
}
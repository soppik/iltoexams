<?php

/**
 * Draws works slider
 */
class ctWorksSliderShortcode extends ctWorksShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Works slider';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'works_slider';
	}

	public function enqueueScripts() {
		wp_register_script( 'ct-magnific-popup-js', CT_THEME_ASSETS . '/plugins/magnificpopup/jquery.magnific-popup.min.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'ct-magnific-popup-js' );

		wp_register_script( 'thumbnailScroller', CT_THEME_ASSETS . '/plugins/thumbscroller/jquery.thumbnailScroller.js' );
		wp_enqueue_script( 'thumbnailScroller' );

		wp_register_script( 'thumbnailScrollerCall', CT_THEME_ASSETS . '/js/jscalls/thumbslidercall.js' );
		wp_enqueue_script( 'thumbnailScrollerCall' );
	}


	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		$attributes = shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts );
		$class      = '';
		extract( $attributes );
		$this->addInlineJS( $this->getInlineJS(), true );
		$recentposts = $this->getCollection( $attributes, array( 'post_type' => 'portfolio' ) );

		$items = '';

		foreach ( $recentposts as $p ) {


			$imgsrc = ct_get_feature_image_src( $p->ID, 'portfolio_slider' );

			$custom        = get_post_custom( $p->ID );
			$fullImageSize = $custom['portfolio_single_horizontal'][0] == 'yes' ? 'work_single_horizontal' : 'work_single';
			$fullImageSrc  = ct_get_feature_image_src( ( $p->ID ), $fullImageSize );

			$link  = get_permalink( $p->ID );
			$title = get_the_title( $p->ID );


			$items .= '<div class="project-thumb">
                    <img src="' . $imgsrc . '"/>
                    <div class="thumb-info">
                        <div class="thumb-links">
                            <a class="thumb-link image-popup" data-type="image" href="' . $fullImageSrc . '"><i class="fa fa-search"></i></a>
                            <a class="thumb-link" href="' . $link . '"><i class="fa fa-link"></i></a>
                        </div>
                        <a href="' . $link . '" class="thumb-title">
                            <h4>' . $title . '</h4>
                        </a>
                        <span class="thumb-cats">' . ct_get_categories_string( $p->ID, $separator = ', ', $taxonomy = 'portfolio_category' ) . '</span>
                    </div>
                </div>
            ';

		}


		$mainContainerAtts = array(
			'class' => array(
				'thumbnailScroller',
				'thumbnailScrollerLoading',
				$class,
			),
			'id'    => $id,
		);

		$viewAllProject = $full_portfolio_button_text != '' && ct_get_option( 'portfolio_index_page' ) != '' ? '<br>
    <br>
    <div class="text-center">
        <div class="btn-group-single">
            <a href="' . get_permalink( ct_get_option( 'portfolio_index_page' ) ) . '" class="btn btn-lg btn-border"><i class="fa fa-fw fa-th"></i></a>
            <a href="' . get_permalink( ct_get_option( 'portfolio_index_page' ) ) . '" class="btn btn-lg btn-border">' . $full_portfolio_button_text . '</a>
        </div>
        <br>
    </div>' : '';


		return '<div' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>
        <div class="jTscrollerContainer magnific-gallery">
            <div class="jTscroller">' . $items . '</div></div></div>' . $viewAllProject;


	}


	/**
	 * Shortcode type
	 * @return string
	 */
	public function getShortcodeType() {
		return self::TYPE_SHORTCODE_SELF_CLOSING;
	}

	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		$atts = $this->getAttributesWithQuery( array(
			'limit'                      => array(
				'label'   => __( 'limit', 'ct_theme' ),
				'default' => 4,
				'type'    => 'input',
				'help'    => __( "Number of portfolio elements", 'ct_theme' )
			),
			'full_portfolio_button_text' => array(
				'label'   => __( 'View All Projects', 'ct_theme' ),
				'default' => __( 'View Full Portfolio', 'ct_theme' ),
				'type'    => 'input',
			),
		) );

		if ( isset( $atts['cat'] ) ) {
			unset( $atts['cat'] );
		}

		return $atts;
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-picture-o' ) );
	}
}

new ctWorksSliderShortcode();
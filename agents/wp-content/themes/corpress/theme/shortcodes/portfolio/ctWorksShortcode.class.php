<?php

/**
 * Draws works
 */
class ctWorksShortcode extends ctShortcodeQueryable implements ctVisualComposerShortcodeInterface
{

    /**
     * Returns name
     * @return string|void
     */
    public function getName()
    {
        return 'Works';
    }

    /**
     * Shortcode name
     * @return string
     */
    public function getShortcodeName()
    {
        return 'works';
    }

    public function enqueueHeadScripts()
    {
        wp_register_style('ct-magnific-popup-css', CT_THEME_ASSETS . '/plugins/magnificpopup/magnific-popup.css');
        wp_enqueue_style('ct-magnific-popup-css');

    }

    public function enqueueScripts()
    {

        wp_register_script('ct-magnific-popup-js', CT_THEME_ASSETS . '/plugins/magnificpopup/jquery.magnific-popup.min.js', array('jquery'), false, true);
        wp_enqueue_script('ct-magnific-popup-js');

        wp_register_script('ct-isotope', CT_THEME_ASSETS . '/js/jquery.isotope.min.js', array('jquery'), false, true);
        wp_enqueue_script('ct-isotope');
    }

    /**
     * Handles shortcode
     *
     * @param $atts
     * @param null $content
     *
     * @return string
     */

    public function handle($atts, $content = null)
    {


        $attributes = shortcode_atts($this->extractShortcodeAttributes($atts), $atts);
        extract($attributes);

        $recentposts = $this->getCollection($attributes, array('post_type' => 'portfolio'));

        $this->addInlineJS($this->getInlineJS(), true);

        //filters
        $filtersOn = $filters == "on" || $filters == "true" || $filters == "yes";
        $filtersHtml = '';
        if ($filtersOn) {
            $catsListHtml = $allfilterlabel ? '<li><a data-filter="*" class="btn btn-border active">' . $allfilterlabel . '</a></li>' : '';

            $terms = get_terms('portfolio_category', 'hide_empty=1');
            foreach ($terms as $term) {
                $catsListHtml .= '<li><a data-filter=".' . $this->getCatFilterClass($term) . '" class="btn btn-border">' . $term->name . '</a></li>';

            }


            if ($catsListHtml) {
                $filtersHtml = '<div class="text-center"><ul class="galleryFilters clearfix list-unstyled list-inline">' . $catsListHtml . '</ul><div class="clearfix"></div></div>';
            }
        }


        if ($with_spaces != 'true' || $with_spaces != 'yes') {
            switch ($columns) {
                case 1:
                    $size = 'full';
                    $ColClass = ' col-1';
                    break;
                case 2:
                    $size = 'portfolio_2_columns';
                    $ColClass = ' col-2';
                    break;
                case 3:
                    $size = 'portfolio_2_columns';
                    $ColClass = ' col-3';
                    break;
                case 4:
                    $size = 'portfolio_2_columns';
                    $ColClass = ' col-4';
                    break;
                default:
                    $size = 'portfolio_4_columns';
                    $ColClass = ' col-4';
            }
        } else {
            switch ($columns) {
                case 1:
                    $size = 'full';
                    $ColClass = ' col-1';
                    break;
                case 2:
                    $size = 'portfolio_2_columns';
                    $ColClass = ' col-2';
                    break;
                case 3:
                    $size = 'portfolio_3_columns';
                    $ColClass = ' col-3';
                    break;
                case 4:
                    $size = 'portfolio_4_columns';
                    $ColClass = ' col-4';
                    break;
                default:
                    $size = 'portfolio_4_columns';
                    $ColClass = ' col-4';
            }
        }


        //elements
        $elements = '';
        $counter = 0;
        foreach ($recentposts as $p) {
            $counter++;
            $custom = get_post_custom($p->ID);
            $fullImageSize = $custom['portfolio_single_horizontal'][0] == 'yes' ? 'work_single_horizontal' : 'work_single';
            $fullImageSrc = ct_get_feature_image_src(($p->ID), $fullImageSize);

            $link = get_permalink($p->ID);
            $title = get_the_title($p->ID);

            $cats = '';
            $catsFilters = "";
            $terms = get_the_terms($p->ID, 'portfolio_category');
            if ($terms) {
                foreach ($terms as $term) {
                    $catsFilters .= (" " . $this->getCatFilterClass($term));
                    $cats .= (strtolower($term->slug) . " ");
                }
            }


            $imgsrc = ct_get_feature_image_src($p->ID, $size);


            $elements .= '<div class="galleryItem ' . $cats . '">
                    <div class="project-thumb">
                        <img src="' . $imgsrc . '" alt="image"/>

                        <div class="thumb-info">
                            <div class="thumb-links">
                                <a class="thumb-link image-popup" data-type="image" href="' . $fullImageSrc . '" title="' . $title . '"><i class="fa fa-search"></i></a>
                                <a class="thumb-link" href="' . $link . '"><i class="fa fa-link"></i></a>
                            </div>
                            <a href="' . $link . '" class="thumb-title">
                                <h4>' . $title . '</h4>
                            </a>
                    <span class="thumb-cats">' . ct_get_categories_string($p->ID, $separator = ', ', $taxonomy = 'portfolio_category') . '</span>
                        </div>
                    </div>
                </div>';
        }

        $mainContainerAtts = array(
            'class' => array(
                'galleryContainer',
                'clearfix',
                $with_spaces == 'yes' || $with_spaces == 'true' ? 'withSpaces' : '',
                'magnific-gallery',
                $ColClass,
                ($masonry == 'true' || $masonry == 'yes' ? 'withMasonry' : '')
            ),
            'id' => 'galleryContainer',
        );

        $html = $filtersHtml . '<div ' . $this->buildContainerAttributes($mainContainerAtts, $atts) . '>' . $elements . '</div>';

        return do_shortcode($html);

    }

    /**
     * returns JS
     *
     * @return string
     */
    protected function getInlineJS()
    {
        return '(function ($) {
             "use strict";
                $(window).load(function () {

                        if ($().isotope && ($(\'.galleryContainer\').length > 0)) {

                            var $container = $(\'.galleryContainer\'), // object that will keep track of options
                                isotopeOptions = {}, // defaults, used if not explicitly set in hash
                                defaultOptions = {
                                    filter: \' * \', itemSelector: \'.galleryItem\', // set columnWidth to a percentage of container width
                                    masonry: {}
                                };

                            // set up Isotope
                            $container.each(function () {
                                $(this).isotope(defaultOptions);
                            });


                            $(\'.galleryFilters a\').click(function () {
                                $(\'.galleryFilters .active\').removeClass(\'active\');
                                $(this).addClass(\'active\');

                                var selector = $(this).attr(\'data-filter\');
                                $container.isotope({
                                    filter: selector, animationOptions: {
                                        duration: 750, easing: \'linear\', queue: false
                                    }
                                });
                                return false;
                            });
                        }
                    });


                $(document).ready(function () {

                /* ==================== */
                /* ==== MAGNIFIC POPUP ==== */

                if ($().magnificPopup) {
                    $(".magnific-popup").each(function () {
                        var $this = $(this);
                        var $type = $this.attr("data-type");
                        $this.magnificPopup({
                            type: $type,
                            closeOnContentClick: true,
                            closeBtnInside: true,
                            fixedContentPos: true,
                            mainClass: "mfp-no-margins mfp-with-zoom", // class to remove default margin from left and right side
                            image: {
                                verticalFit: true
                            }
                        });
                    })
                    $(".magnific-gallery").each(function () {
                        var $this = $(this);
                        var $type = $this.attr("data-type");
                        $this.magnificPopup({
                            delegate: ".image-popup",
                            type: "image",
                            closeOnContentClick: true,
                            closeBtnInside: true,
                            fixedContentPos: true,
                            mainClass: "mfp-no-margins mfp-with-zoom", // class to remove default margin from left and right side
                            image: {
                                verticalFit: true
                            },
                            gallery: {
                                enabled: true, navigateByImgClick: true, preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
                            },
                            callbacks: {
                                buildControls: function () {
                                    // re-appends controls inside the main container
                                    this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
                                }
                            },
                            close: function () {
                                // Will fire when popup is closed
                            }
                        });
                    })

                }
            })
        })(jQuery);
        ';
    }


    /**
     * creates class name for the category
     *
     * @param $cat
     *
     * @return string
     */
    protected function getCatFilterClass($cat)
    {
        return strtolower(str_replace(' ', '-', $cat->slug));
    }


    /**
     * Shortcode type
     * @return string
     */
    public function getShortcodeType()
    {
        return self::TYPE_SHORTCODE_SELF_CLOSING;
    }

    /**
     * Returns config
     * @return null
     */
    public function getAttributes()
    {
        $atts = $this->getAttributesWithQuery(array(
            'limit' => array(
                'label' => __('limit', 'ct_theme'),
                'default' => 12,
                'type' => 'input',
                'help' => __("Number of portfolio elements", 'ct_theme')
            ),
            'filters' => array(
                'label' => __('filters', 'ct_theme'),
                'default' => 'true',
                'type' => 'checkbox',
                'help' => __("Show filters?", 'ct_theme')
            ),
            'allfilterlabel' => array(
                'label' => __('"all" filter label', 'ct_theme'),
                'default' => __('All', 'ct_theme'),
                'type' => 'input',
                'help' => __("Label for the 'show all' button", 'ct_theme')
            ),
            'columns' => array(
                'label' => __('columns', 'ct_theme'),
                'default' => '4',
                'type' => 'select',
                'choices' => array
                (
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4'
                ),
                'help' => __("Number of columns", 'ct_theme')
            ),
            'masonry' => array(
                'label' => __('With Masonry', 'ct_theme'),
                'default' => 'no',
                'type' => 'select',
                'choices' => array(
                    'yes' => __('yes', 'ct_theme'),
                    'no' => __('no', 'ct_theme')
                ),
            ),
            'with_spaces' => array(
                'label' => __('With Spaces ?', 'ct_theme'),
                'default' => 'yes',
                'type' => 'select',
                'choices' => array(
                    'yes' => __('yes', 'ct_theme'),
                    'no' => __('no', 'ct_theme')
                ),
            ),
            'categories' => array(
                'label' => __('categories', 'ct_theme'),
                'default' => 'yes',
                'type' => 'select',
                'choices' => array(
                    'yes' => __('yes', 'ct_theme'),
                    'no' => __('no', 'ct_theme')
                ),
                'help' => __("Show categories?", 'ct_theme')
            ),
        ));

        if (isset($atts['cat'])) {
            unset($atts['cat']);
        }

        return $atts;
    }

    /**
     * Returns additional info about VC
     * @return ctVisualComposerInfo
     */
    public function getVisualComposerInfo()
    {
        return new ctVisualComposerInfo($this, array('icon' => 'fa-picture-o'));
    }
}

new ctWorksShortcode();
<?php

/**
 * Google maps shortcode
 */
class ctGoogleMapsShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface
{

    /**
     * Returns name
     * @return string|void
     */
    public function getName()
    {
        return 'Google maps';
    }

    /**
     * Shortcode name
     * @return string
     */
    public function getShortcodeName()
    {
        return 'google_maps';
    }

    /**
     * Enqueue scripts
     */

    public function enqueueScripts()
    {
        wp_register_script('ct-gmap', CT_THEME_ASSETS . '/js/gmap3.min.js', array('jquery'), false, true);
        wp_enqueue_script('ct-gmap');

        wp_register_script('google-maps', 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false', array('jquery'), false, true);
        wp_enqueue_script('google-maps');
    }


    /**
     * Handles shortcode
     *
     * @param $atts
     * @param null $content
     *
     * @return string
     */

    public function handle($atts, $content = null)
    {
        $attributes = shortcode_atts($this->extractShortcodeAttributes($atts), $atts);
        extract($attributes);
        $this->addInlineJS($this->getInlineJS($attributes, $id));


        $id = ($id == '') ? 'gmap' . rand(100, 1000) : $id;


        if (!is_numeric($height)) {
            $height = '286';
        }

        if ($attributes['map_draggable'] == 'yes') {
            $attributes['map_draggable'] = 'true';
        } else if ($attributes['map_draggable'] == 'no') {
            $attributes['map_draggable'] = 'false';
        }


        if (ct_is_browser_type('mobile') == true) {
            $attributes['map_draggable'] = 'false';
        }

        $mainContainerAtts = array(
            'class' => array(
                'googlemap',
                'googleMap'

            ),
            'data-height' => $height,
            'data-offset' => $offset,
            'data-location' => $location,
            'data-map_draggable' => $attributes['map_draggable'],
            'data-map_type' => $attributes['map_type'],
            'data-zoom' => $zoom,
            'id' => $id,
        );

        $titleShortcode = $title ? '[header class="special" level="3"]' . $title . '[/header]' : '';


        if ($roll == 'true' || $roll == 'yes') {
            $html = '
            <div class="hidemap">
                <button class="mapToggle">
                ' . $collapsed_label .' <i class=\'fa fa-angle-up\'></i>
                </button>
                <div class="googlemapcontainer">
                ' . do_shortcode($titleShortcode . '<div ' . $this->buildContainerAttributes($mainContainerAtts, $atts) . '></div>') . '
                </div>
            </div><!--END HIDEMAP!-->            ';
        } else {
            $html = do_shortcode($titleShortcode . '<div ' . $this->buildContainerAttributes($mainContainerAtts, $atts) . '></div>');
        }


        return $html;

    }


    /**
     * returns inline js
     *
     * @param $attributes
     * @param $id
     *
     * @return string
     */
    protected function getInlineJS($attributes, $id)
    {


        if (ct_get_option('map_marker', '') != '') {
            $marker = ct_get_option('map_marker', '');

        } else {
            $marker = CT_THEME_ASSETS . '/images/marker.png';
        }

        extract($attributes);

        $expanded_label = mb_strtoupper($expanded_label);
        $collapsed_label = mb_strtoupper($collapsed_label);


        return '

     function validatedata($attr, $defaultValue) {
        if ($attr !== undefined) {
            return $attr
        }
        return $defaultValue;
    }


    jQuery(document).ready(function () {

        /* =========================== */
        /* ==== GOOGLE MAP TOOGLE ==== */

        var $maphelp = $(".hidemap .googlemap");
        jQuery(".hidemap .mapToggle").click(function () {
            var $this = jQuery(this);
            var $map = $this.parent().find(".googlemapcontainer");

            $this.html($this.html() == \'' . $expanded_label . ' <i class="fa fa-angle-down"></i>\' ? \'' . $collapsed_label . ' <i class="fa fa-angle-up"></i>\' : \'' . $expanded_label . ' <i class="fa fa-angle-down"></i>\');

            if ($map.height() != "0") {
                $map.animate({height: "0px"}, 500);
            } else {
                $map.animate({height: $maphelp.data("height") + "px"}, 500);
                setTimeout(function () {
                    jQuery("html, body").animate({
                        scrollTop: $map.offset().top
                    }, 2000);
                }, 500);

            }
        })
    })


    "use strict";
    jQuery(".googlemap").each(function () {
        var atcenter = "";
        var $this = jQuery(this);
        var location = $this.data("location");
        var zoom = $this.data("zoom");
        var lat = $this.data("map-latitude");
        var long = $this.data("map-longitude");

        var offset = -30;

        if (validatedata($this.data("offset"))) {
            offset = $this.data("offset");
        }

        if (validatedata(location)) {

            $this.gmap3({
                marker: {
                   // latLng: [lat, long],
                    address: location,
                    options: {
                        //visible: false
                        icon: new google.maps.MarkerImage("' . $marker . '")
                    },
                    callback: function (marker) {
                        atcenter = marker.getPosition();
                    }
                },
                map: {
                    options: {
                        //maxZoom:11,
                        zoom: zoom,
                        mapTypeId: google.maps.MapTypeId[$this.data("map_type")],
                        // ("ROADMAP", "SATELLITE", "HYBRID","TERRAIN");
                        scrollwheel: false,
                        disableDoubleClickZoom: false,
                        draggable: $this.data("map_draggable"),
                        //disableDefaultUI: true,
                        mapTypeControlOptions: {
                            //mapTypeIds: [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.HYBRID],
                            //style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                            //position: google.maps.ControlPosition.RIGHT_CENTER
                            mapTypeIds: []
                        },
                        styles: [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}]
                    },
                    events: {
                        idle: function () {
                            if (!$this.data("idle")) {
                                $this.gmap3("get").panBy(0, offset);
                                $this.data("idle", true);
                            }
                        }
                    }
                },
                overlay: {
                    //latLng: [lat, long],
                    address: location,
                    options: {
                        //content: "<div class=\'customMarker\'><div class="address">" + location + "</div><div class=\'marker\'><img src=\'assets/images/custom-marker.png\'></div></div>",
                        offset: {
                            y: -100,
                            x: -25
                        }
                    }
                }
                //},"autofit"
            });

            // center on resize
            google.maps.event.addDomListener(window, "resize", function () {
                //var userLocation = new google.maps.LatLng(53.8018,-1.553);
                setTimeout(function () {
                    $this.gmap3("get").setCenter(atcenter);
                    $this.gmap3("get").panBy(0, offset);
                }, 400);

            });

            // set height
            $this.css("min-height", $this.data("height") + "px");
        }

        if($this.parent().parent().hasClass("hidemap")){
            $this.parent().animate({height:"0px"}, 500);
        }

    })

        ';
    }


    /**
     * Returns config
     * @return null
     */
    public function getAttributes()
    {
        return array(
            'id' => array(
                'label' => __('ID', 'ct_theme'),
                'default' => '',
                'type' => 'input',
                'help' => __("Custom map ID", 'ct_theme')
            ),
            'location' => array(
                'label' => __('Location', 'ct_theme'),
                'default' => '',
                'type' => 'input',
                'help' => __("Enter location eg town", 'ct_theme')
            ),
            'height' => array('label' => __('height', 'ct_theme'), 'default' => 460, 'type' => 'input'),
            'offset' => array(
                'label' => __('Map vertical offset (in px)', 'ct_theme'),
                'default' => '0',
                'type' => 'input'
            ),
            'title' => array(
                'label' => __('Title', 'ct_theme'),
                'default' => '',
                'type' => 'input',
                'help' => __("Title", 'ct_theme')
            ),
            'map_type' => array(
                'label' => __('Select map type', 'ct_theme'),
                'default' => 'ROADMAP',
                'type' => 'select',
                'options' => array(
                    'ROADMAP' => 'Roadmap',
                    'SATELLITE' => 'Satellite',
                    'HYBRID' => 'Hybrid',
                    'TERRAIN' => 'Terrain',
                )
            ),
            'map_draggable' => array(
                'label' => __('Draggable', 'ct_theme'),
                'default' => 'true',
                'type' => 'select',
                'options' => array('true' => 'true', 'false' => 'false'),
                'help' => __("locked automatically on mobile devices", 'ct_theme')
            ),
            'zoom' => array(
                'label' => __("zoom", 'ct_theme'),
                'type' => 'input',
                'default' => 17,
                'help' => __('Zoom from 1 to 15', 'ct_theme')
            ),
            'roll' => array(
                'label' => __('Roll Map', 'ct_theme'),
                'default' => 'no',
                'type' => 'select',
                'choices' => array(
                    'yes' => __('yes', 'ct_theme'),
                    'no' => __('no', 'ct_theme')
                ),
            ),
            'expanded_label' => array(
                'label' => __('Expanded map label', 'ct_theme'),
                'default' => 'hide map',
                'type' => 'input',
            ),
            'collapsed_label' => array(
                'label' => __('Collapsed map label', 'ct_theme'),
                'default' => 'locate us on map',
                'type' => 'input',
            ),
        );
    }

    /**
     * Returns additional info about VC
     * @return ctVisualComposerInfo
     */
    public function getVisualComposerInfo()
    {
        return new ctVisualComposerInfo($this, array('icon' => 'fa-globe'));
    }
}

new ctGoogleMapsShortcode();
<?php

/**
 * Contact shortcode
 */
class ctContactIconsShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface
{

    /**
     * Returns name
     * @return string|void
     */
    public function getName()
    {
        return 'Contact Icons';
    }

    /**
     * Shortcode name
     * @return string
     */
    public function getShortcodeName()
    {
        return 'contact_icons';
    }

    /**
     * Handles shortcode
     * @param $atts
     * @param null $content
     * @return string
     */

    public function handle($atts, $content = null)
    {
        $class = '';
        $shortcodeEmail = '';
        $shortcodePhone = '';

        extract(shortcode_atts($this->extractShortcodeAttributes($atts), $atts));

        $mainContainerAtts = array(
            'class' => array(
                'icon-list',
                'list-unstyled',
                $class
            ),
        );

        $arrayEmail = explode(",", $email);
        $arrayPhone = explode(",", $phone);

        if ($email != '') {

            $shortcodeEmail = '';
            foreach ($arrayEmail as $value) {
                $shortcodeEmail .= '<li>
                                <div class="display-table">
                                <span class="table-cell">
                                    <i class="fa fa-envelope fa-lg"></i>
                                </span>
                                <span class="table-cell">
                                    <a href="mailto:' . $value . '">' . $value . '</a>
                                </span>
                                </div>
                            </li>';
            }
        }

        $value = '';
        if ($phone != '') {
            $shortcodePhone = '';
            foreach ($arrayPhone as $value) {
                $shortcodePhone .= '<li>
                                <div class="display-table">
                                <span class="table-cell">
                                    <i class="fa fa-phone fa-lg"></i>
                                </span>
                                <span class="table-cell">' . $value . '</span>
                                </div>
                            </li>';
            }
        }

        $locationHtml = $location != '' ? '<li>
                                <div class="display-table">
                                <span class="table-cell">
                                    <i class="fa fa-map-marker fa-lg"></i>
                                </span>
                                <span class="table-cell">' . $location . '</span>
                                </div>
                            </li>' : '';

        $homepageHtml = $homepage_url != '' ? ' <li>
                                <div class="display-table">
                                <span class="table-cell">
                                    <i class="fa fa-globe fa-lg"></i>
                                </span>
                                <span class="table-cell">
                                    <a href="' . $homepage_url . '">' . $homepage_label . '</a>
                                </span>
                                </div>
                            </li>' : '';


        $shortcodeContact = '

                <div' . $this->buildContainerAttributes($mainContainerAtts, $atts) . '>' . $locationHtml . $shortcodePhone . $homepageHtml . $shortcodeEmail . '</div>';


        return do_shortcode($shortcodeContact);
    }


    /**
     * Returns config
     *
     * @return null
     */

    public function getAttributes()
    {
        return array(
            'phone' => array('label' => __('Phone Number', 'ct_theme'), 'default' => '', 'type' => 'input', 'help' => __("Enter phone numbers. Separating items with a comma", 'ct_theme')),
            'email' => array('label' => __('E-mail', 'ct_theme'), 'default' => '', 'type' => 'input', 'help' => __("Enter email addresses. Separating items with a comma", 'ct_theme')),
            'location' => array('label' => __('Location', 'ct_theme'), 'default' => '', 'type' => 'input', 'help' => __("e.g. 10 Milk Street #230 Boston, MA 02108", 'ct_theme')),
            'homepage_url' => array('label' => __('Homepage URL', 'ct_theme'), 'default' => '', 'type' => 'input', 'help' => __("Enter homepage URL.", 'ct_theme')),
            'homepage_label' => array('label' => __('Homepage label', 'ct_theme'), 'default' => '', 'type' => 'input', 'help' => __("Enter homepage label.", 'ct_theme')),
        );
    }

	/**
		 * Returns additional info about VC
		 * @return ctVisualComposerInfo
		 */
		public function getVisualComposerInfo() {
			return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-envelope' ) );
		}
}

new ctContactIconsShortcode();
<?php

/**
 * Testimonial shortcode
 */
class ctTestimonialShortcode extends ctShortcode {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Testimonial';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'testimonial';
	}

	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );

		$mainContainerAtts = array(
			'class' => array(
				'clearfix'

			)
		);

		if ( ! empty( $image ) ) {
			if ( $link != '' ) {
				$testImg = '<a href="' . $link . '"><img class="media-object" src="' . $image . '" alt=" "></a>';
			} else {
				$testImg = '<img class="media-object" src="' . $image . '" alt=" ">';
			}
		} else {
			$testImg = '';
		}


		$html = '<blockquote ' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>';
		$html .= '<div class="inner">';
		$html .= '<p>' . $content . '</p>';
		$html .= '</div>';
		$html .= '<div class="media author">';
		$html .= '<span class="pull-left">';
		$html .= $testImg;
		$html .= '</span>';
		$html .= '<div class="media-body"><h4 class="media-heading">' . $author . '<span>' . $email . '</span></h4></div>';
		$html .= '</div>';
		$html .= '</blockquote>';

		return do_shortcode( $html );

	}

	/**
	 * Parent shortcode name
	 * @return null
	 */

	public function getParentShortcodeName() {
		return 'testimonials';
	}


	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'author'  => array(
				'label'   => __( 'Author', 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
			),
			'email'   => array(
				'label'   => __( 'E-mail', 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( "Author e-mail", 'ct_theme' )
			),
			'image'   => array(
				'label'   => __( "Image", 'ct_theme' ),
				'default' => '',
				'type'    => 'image',
				'help'    => __( "Image source", 'ct_theme' )
			),
			'link'    => array(
				'label'   => __( 'Link', 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( "Link", 'ct_theme' )
			),
			'type'    => array(
				'label'   => __( 'Type', 'ct_theme' ),
				'default' => 'light',
				'type'    => 'select',
				'options' => array(
					'light' => 'light',
					'dark'  => 'dark',
				)
			),
			'content' => array( 'label' => __( 'Content', 'ct_theme' ), 'default' => '', 'type' => "textarea" ),
		);
	}
}

new ctTestimonialShortcode();
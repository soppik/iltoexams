<?php

/**
 * Recent posts
 * ver. 2.1
 */
class ctTestimonialSliderShortcode extends ctShortcodeQueryable implements ctVisualComposerShortcodeInterface {


	public function enqueueScripts() {

		wp_register_script( 'ct-flex-slider', CT_THEME_ASSETS . '/plugins/flexslider/jquery.flexslider-min.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'ct-flex-slider' );

		wp_register_script( 'ct-flex-easing', CT_THEME_ASSETS . '/js/jquery.easing.1.3.js', array( 'ct-flex-slider' ), false, true );
		wp_enqueue_script( 'ct-flex-easing' );

		wp_register_script( 'ct-flexslider_init', CT_THEME_ASSETS . '/js/flexslider_init.js', array( 'ct-flex-slider' ), false, true );
		wp_enqueue_script( 'ct-flexslider_init' );

	}


	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Testimonial Slider';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'testimonial_slider';
	}

	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */
	public function handle( $atts, $content = null ) {
		$attributes = shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts );

		extract( $attributes );
		$recentposts = $this->getCollection( $attributes, array( 'post_type' => 'testimonial' ) );

		//( $widgetmode == 'true' ) ? 'media-list' : 'flexslider',
		$mainContainerAtts = array(
			'class'             => array(
				'flexslider',
				$class
			),
			'data-smoothheight' => 'true'
		);


		/* Build title column */
		$html = '<div ' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '><ul class="slides">';


		/* Bulid article thumb column */

		foreach ( $recentposts as $p ) {
			$content           = $p->post_content;
			$postFeaturedImage = ct_get_feature_image_src( $p->ID, 'featured_image_related_posts' );
			$author            = get_post_meta( $p->ID, 'testimonial_author', true );
			$footer            = get_post_meta( $p->ID, 'testimonial_footer', true );


			if ( $type == 'image' ) {
				$html .= '<li><blockquote class="type1">';
				$html .= '<em>' . $content . '</em>';
				$html .= '<footer><div class="author"><figure><img src="' . $postFeaturedImage . '" draggable="false"><figcaption>' . $author . '</figcaption></figure><cite title="Source Author">' . $author . '</cite>';
				$html .= '<span class="underline"><em>' . $footer . '</em></span>';
				$html .= '</div></footer></blockquote></li>';

			} else {
				$html .= '<li><blockquote class="type2">';
				$html .= '<em>' . $content . '</em>';
				$html .= '<footer><div class="author"><cite title="Source Author">' . $author . '</cite>, ';
				$html .= '<span class="underline">' . $footer . '</span>';
				$html .= '</div></footer></blockquote></li>';
			}


		}
		$html .= '</ul></div>';

		return do_shortcode( $html );
	}


	/**
	 * Shortcode type
	 * @return string
	 */
	public function getShortcodeType() {
		return self::TYPE_SHORTCODE_SELF_CLOSING;
	}

	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		$atts = $this->getAttributesWithQuery( array(
			'widgetmode' => array( 'default' => 'false', 'type' => false ),
			'limit'      => array( 'label' => __( 'limit', 'ct_theme' ), 'default' => 3, 'type' => 'input' ),
			'type'       => array(
				'label'   => __( 'Type', 'ct_theme' ),
				'default' => 'image',
				'type'    => 'select',
				'choices' => array( 'image' => 'with image', 'simple' => 'simple' )
			),
			'class'      => array( 'label' => __( "Custom class", 'ct_theme' ), 'default' => '', 'type' => 'input' )
		) );

		return $atts;
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-picture-o' ) );
	}
}

new ctTestimonialSliderShortcode();
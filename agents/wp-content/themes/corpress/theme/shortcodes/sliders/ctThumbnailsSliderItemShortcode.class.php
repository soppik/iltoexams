<?php

/**
 * Flex Slider Item shortcode
 */
class ctThumbnailsSliderItemShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Thumbnails Slider Item';
	}

	/**
	 * Parent shortcode name
	 * @return null
	 */
	public function getParentShortcodeName() {
		return 'thumbnails_slider';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'thumbnails_slider_item';
	}

	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */
	public function handle( $atts, $content = null ) {

		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );


		$mainContainerAtts = array();

		$item = '<li ' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>';

		if ( !empty($link) ) {
			$item .= '<a href="' . $link . '"><img src="' . $image . '" alt="preview" draggable="false"></a>';
		}else{
			$item .= '<img src="' . $image . '" alt="preview" draggable="false">';
		}


		$item .= '</li>';

		return do_shortcode( $item );
	}


	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'image' => array(
				'label'   => __( "Image", 'ct_theme' ),
				'default' => '',
				'type'    => 'image',
				'help'    => __( "Image source", 'ct_theme' )
			),
			'link'  => array( 'label' => __( 'Link', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-picture-o' ) );
	}

}

new ctThumbnailsSliderItemShortcode();
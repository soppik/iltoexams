<?php

/**
 * Flex Slider Item shortcode
 */
class ctCirclesProgressSliderItemShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Circles Progress Slider Item';
	}

	/**
	 * Parent shortcode name
	 * @return null
	 */
	public function getParentShortcodeName() {
		return 'circles_progress_slider';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'circles_progress_slider_item';
	}

	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */
	public function handle( $atts, $content = null ) {

		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );


		$mainContainerAtts = array();

		if ( !empty($link) ) {
			$item = '<li>
			<a href="' . esc_url($link) . '">
			<img class="img-circle img-responsive" src="' . esc_url($image) . '" alt="'.__('Slider image','ct_theme').'"></a>
			<div class="shadow"></div>
			<span class="gloria motive">' . $title . '</span>
			</li>';
		}else{
			$item = '<li>
			<img class="img-circle img-responsive" src="' . esc_url($image) . '" alt="'.__('Slider image','ct_theme').'">
			<div class="shadow"></div>
			<span class="gloria motive">' . $title . '</span>
			</li>';
		}

		return do_shortcode( $item );
	}


	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'image' => array(
				'label'   => __( "Image", 'ct_theme' ),
				'default' => '',
				'type'    => 'image',
				'help'    => __( "Image source", 'ct_theme' )
			),
			'title'  => array( 'label' => __( 'Title', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'link'  => array( 'label' => __( 'Link', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-chevron-circle-right' ) );
	}

}

new ctCirclesProgressSliderItemShortcode();
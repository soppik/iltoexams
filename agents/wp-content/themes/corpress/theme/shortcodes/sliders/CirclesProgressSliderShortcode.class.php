<?php

/**
 * Flex Slider shortcode
 */
class ctCirclesProgressSliderShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Circles Progress Slider';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'circles_progress_slider';
	}

	public function enqueueScripts() {

		wp_register_script( 'ct-flex-slider', CT_THEME_ASSETS . '/plugins/flexslider/jquery.flexslider-min.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'ct-flex-slider' );

		wp_register_script( 'ct-flex-easing', CT_THEME_ASSETS . '/js/jquery.easing.1.3.js', array( 'ct-flex-slider' ), false, true );
		wp_enqueue_script( 'ct-flex-easing' );

		wp_register_script( 'ct-flexslider_init', CT_THEME_ASSETS . '/js/flexslider_init.js', array( 'ct-flex-slider' ), false, true );
		wp_enqueue_script( 'ct-flexslider_init' );
	}


	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		$attributes = shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts );
		extract( $attributes );
		$id = ( $id == '' ) ? 'thumb_slider' . rand( 100, 1000 ) : $id;


		$mainContainerAtts = array(
			'class'                  => array(
				'flexslider',
				'ct-process',
				$class,
			),
			'data-smooth'            => $smooth,
			'data-slideshow'         => $slideshow,
			'data-slideshowspeed'    => $speed,
			'data-animationspeed'    => $animspeed,
			'data-controlnav' => $controls,
			'data-directionnav'      => $dircontrols,
			'data-minitems'          => $min_items,
			'data-maxitems'          => $max_items,
			'data-itemwidth'         => $item_width,
			'data-itemmargin'        => $item_margin,
			'data-animation'         => 'slide',
			'id'                     => $id,
			'data-move'              => '1',
            'data-prevtext'          => ' ',
			'data-nexttext'          => ' ',

		);

			$html = '<div' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>
				<ul class="slides">
					'.$content.'
				</ul>
			</div>';


		return do_shortcode( $html );
	}


	/**
	 * Returns config
	 * @return null
	 *
	 *ass="ct-process flexslider" data-maxitems="3" data-minitems="1" data-itemwidth="380" data-itemmargin="0" data-move="1" data-directionnav="true" data-controlnav="false" data-prevtext=" " data-nexttext=" ">
	 */
	public function getAttributes() {
		return array(
			'id'          => array(
				'label'   => __( 'Custom Slider ID', 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( "html id attribute", 'ct_theme' )
			),

			'loop'        => array(
				'label'   => __( 'Loop', 'ct_theme' ),
				'default' => 'false',
				'type'    => 'select',
				'choices' => array(
					"false" => __( "false", "ct_theme" ),
					"true"  => __( "true", "ct_theme" ),
				)
			),
			'smooth'      => array(
				'label'   => __( 'Smooth', 'ct_theme' ),
				'default' => 'false',
				'type'    => 'select',
				'choices' => array(
					"true"  => __( "true", "ct_theme" ),
					"false" => __( "false", "ct_theme" )
				)
			),
			'slideshow'   => array(
				'label'   => __( 'Slideshow', 'ct_theme' ),
				'default' => 'false',
				'type'    => 'select',
				'choices' => array(
					"true"  => __( "true", "ct_theme" ),
					"false" => __( "false", "ct_theme" )
				)
			),
			'speed'       => array(
				'label'   => __( 'Speed', 'ct_theme' ),
				'default' => 15000,
				'type'    => 'input',
				'help'    => __( 'slide transition speed in miliseconds (1 sec = 1000 milisec)', 'ct_theme' )
			),
			'animspeed'   => array(
				'label'   => __( 'Animation speed', 'ct_theme' ),
				'default' => 600,
				'type'    => 'input',
				'help'    => __( 'slide transition speed in miliseconds (1 sec = 1000 milisec)', 'ct_theme' )
			),
			'controls'    => array(
				'label'   => __( 'Control navigation', 'ct_theme' ),
				'default' => 'true',
				'type'    => 'select',
				'choices' => array(
					"true"  => __( "true", "ct_theme" ),
					"false" => __( "false", "ct_theme" )
				)
			),
			'dircontrols' => array(
				'label'   => __( 'Direction navigation', 'ct_theme' ),
				'default' => 'true',
				'type'    => 'select',
				'choices' => array(
					"true"  => __( "true", "ct_theme" ),
					"false" => __( "false", "ct_theme" )
				)
			),
			'min_items'   => array(
				'label'   => __( 'Min Items', 'ct_theme' ),
				'default' => 1,
				'type'    => 'input',
				'help'    => __( "Number of min items", 'ct_theme' )
			),
			'max_items'   => array(
				'label'   => __( 'Max Items', 'ct_theme' ),
				'default' => 3,
				'type'    => 'input',
				'help'    => __( "Number of max items", 'ct_theme' )
			),
			'item_width'  => array(
				'label'   => __( 'Item width', 'ct_theme' ),
				'default' => 380,
				'type'    => 'input',
				'help'    => __( "Item width in px", 'ct_theme' )
			),
			'item_margin' => array(
				'label'   => __( 'Item width', 'ct_theme' ),
				'default' => 0,
				'type'    => 'input',
				'help'    => __( "Item margin in px", 'ct_theme' )
			),
			'class'       => array(
				'label'   => __( 'Custom class', 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( 'Adding custom class allows you to set diverse styles in css to the element. Type in name of class, which you defined in css. You can add as much classes as you like.', 'ct_theme' )
			),

		);
	}

	public function getChildShortcodeInfo() {
		return array( 'name' => 'circles_progress_slider_item', 'min' => 1, 'max' => 20, 'default_qty' => 3 );
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-chevron-circle-right' ) );
	}
}

new ctCirclesProgressSliderShortcode();


//#28146
if(class_exists('WPBakeryShortCodesContainer')){
	class WPBakeryShortcode_circles_progress_slider extends WPBakeryShortCodesContainer{}
}
<?php

/**
 * Flex Slider shortcode
 */
class ctGallerySliderShortcode extends ctShortcodeQueryable implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Gallery Slider';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'gallery_slider';
	}

	public function enqueueScripts() {

		wp_register_script( 'ct-flex-slider', CT_THEME_ASSETS . '/plugins/flexslider/jquery.flexslider-min.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'ct-flex-slider' );

		wp_register_script( 'ct-flex-easing', CT_THEME_ASSETS . '/js/jquery.easing.1.3.js', array( 'ct-flex-slider' ), false, true );
		wp_enqueue_script( 'ct-flex-easing' );

		wp_register_script( 'ct-flexslider_init', CT_THEME_ASSETS . '/js/flexslider_init.js', array( 'ct-flex-slider' ), false, true );
		wp_enqueue_script( 'ct-flexslider_init' );

	}


	private function getAttachedImages( $id, $size ) {
		$urlArr = array();
		$args   = array(
			'post_type'   => 'attachment',
			'numberposts' => - 1,
			'post_status' => null,
			'post_parent' => $id
		);

		$attachments = get_posts( $args );
		if ( $attachments ) {
			foreach ( $attachments as $attach ) {
				$image    = wp_get_attachment_image_src( $attach->ID, $size );
				$imageUrl = $image[0];
				$urlArr[] = $imageUrl;
			}
		}

		return $urlArr;
	}


	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		$attributes = shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts );
		extract( $attributes );
		$id    = ( $id == '' ) ? 'gallery_slider' . rand( 100, 1000 ) : $id;
		$posts = $this->getCollection( $attributes, array( 'post_type' => $post_type ) );


		/* Get attached images array*/
		$images = array();
		foreach ( $posts as $p ) {
			if ( $img = ct_get_feature_image_src( $p->ID, $image_size ) ) {
				$images[] = $img;
			}

			$attached_images = $this->getAttachedImages( $p->ID, $image_size );
			if ( ! empty( $attached_images ) ) {
				$images = array_merge( $images, $attached_images );
			}
		}

		/*build slider items*/
		$items = '';
		foreach ( $images as $img ) {
			$imageShortcode = '[img src ="' . $img . '"][/img]';
			$items .= '<li>' . $imageShortcode . '</li>';
		}

		$id = rand( 100, 1000 );


		$mainContainerAtts = array(
			'class'                  => array(
				'flexslider',
				'std-slider',
				'center-controls',
				'gallery-slider',
				$class
			),
			'data-smooth'            => $smooth,
			'data-slideshow'         => $slideshow,
			'data-slideshowspeed'    => $speed,
			'data-animationspeed'    => $animspeed,
			'data-controlscontainer' => $controls,
			'data-directionnav'      => $dircontrols,
			'id'                     => $id,
			'data-controlnav'        => "true"
		);

		$html = '<div ' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>';
		$html .= '<ul class="slides">' . $items . '</ul>';
		$html .= '</div>';

		return do_shortcode( $html );
	}


	/**
	 * Returns config
	 * @return null
	 *
	 *
	 */
	public function getAttributes() {
		/*get post types and registered image sizes*/
		$imageSizeArr = array();
		$postTypeArr  = array();
		foreach ( get_post_types() as $postType ) {
			$postTypeArr[ $postType ] = $postType;
		}

		foreach ( get_intermediate_image_sizes() as $size_name => $size_attrs ) {

			$imageSizeArr[ $size_attrs ] = $size_attrs;
		}

		$atts = $this->getAttributesWithQuery( array(
			'id'          => array(
				'label'   => __( 'Slider id', 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( "Page or Custom Post Type ID", 'ct_theme' )
			),
			'post_type'   => array(
				'label'   => __( 'Select post type', 'ct_theme' ),
				'default' => 'post',
				'type'    => 'select',
				'choices' => $postTypeArr
			),
			'image_size'  => array(
				'label'   => __( 'Select registered image size', 'ct_theme' ),
				'default' => 'featured_image',
				'type'    => 'select',
				'choices' => $imageSizeArr
			),
			'limit'       => array( 'label' => __( 'Limit', 'ct_theme' ), 'default' => '50', 'type' => 'input' ),
			'height'      => array( 'label' => __( 'Height', 'ct_theme' ), 'default' => '100%', 'type' => 'input' ),
			'loop'        => array(
				'label'   => __( 'Loop', 'ct_theme' ),
				'default' => 'true',
				'type'    => 'select',
				'choices' => array(
					"true"  => __( "true", "ct_theme" ),
					"false" => __( "false", "ct_theme" )
				)
			),
			'smooth'      => array(
				'label'   => __( 'Smooth', 'ct_theme' ),
				'default' => 'false',
				'type'    => 'select',
				'choices' => array(
					"true"  => __( "true", "ct_theme" ),
					"false" => __( "false", "ct_theme" )
				)
			),
			'slideshow'   => array(
				'label'   => __( 'Slideshow', 'ct_theme' ),
				'default' => 'true',
				'type'    => 'select',
				'choices' => array(
					"true"  => __( "true", "ct_theme" ),
					"false" => __( "false", "ct_theme" )
				)
			),
			'speed'       => array(
				'label'   => __( 'Speed', 'ct_theme' ),
				'default' => 15000,
				'type'    => 'input',
				'help'    => __( 'slide transition speed in miliseconds (1 sec = 1000 milisec)', 'ct_theme' )
			),
			'animspeed'   => array(
				'label'   => __( 'Animation speed', 'ct_theme' ),
				'default' => 550,
				'type'    => 'input',
				'help'    => __( 'slide transition speed in miliseconds (1 sec = 1000 milisec)', 'ct_theme' )
			),
			'controls'    => array(
				'label'   => __( 'Control navigation', 'ct_theme' ),
				'default' => 'false',
				'type'    => 'select',
				'choices' => array(
					"true"  => __( "true", "ct_theme" ),
					"false" => __( "false", "ct_theme" )
				)
			),
			'dircontrols' => array(
				'label'   => __( 'Direction navigation', 'ct_theme' ),
				'default' => 'true',
				'type'    => 'select',
				'choices' => array(
					"true"  => __( "true", "ct_theme" ),
					"false" => __( "false", "ct_theme" )
				)
			),
			'class'       => array(
				'label'   => __( 'Custom class', 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( 'Adding custom class allows you to set diverse styles in css to the element. Type in name of class, which you defined in css. You can add as much classes as you like.', 'ct_theme' )
			),
		) );

		return $atts;

	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-picture-o' ) );
	}

}

new ctGallerySliderShortcode();
<?php

/**
 * ctChapter shortcode
 */
class ctSectionShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Section';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'section';
	}


	public function enqueueScripts() {
		wp_register_script( 'stellar', CT_THEME_ASSETS . '/js/jquery.stellar.min.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'stellar' );


		wp_register_script( 'fitvids', CT_THEME_ASSETS . '/js/jquery.fitvids.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'fitvids' );

		wp_register_script( 'jplayer', CT_THEME_ASSETS . '/js/jquery.jplayer.min.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'jplayer' );

		wp_register_script( 'section-call', CT_THEME_ASSETS . '/js/jscalls/sectioncall.js', array( 'stellar' ), false, true );
		wp_enqueue_script( 'section-call' );

	}


	/**
	 * Returns shortcode type
	 * @return mixed|string
	 */

	public function getShortcodeType() {
		return self::TYPE_SHORTCODE_ENCLOSING;
	}


	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 * @var $subheader
	 */

	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );
		/**
		 * @var $subheader
		 */

		$id = ( $id != '' ) ? $id : $style . '_section' . rand( 100, 999 );


		if ( $header_align == 'center' ) {
			$headerCenterClass = 'text-center';
			$hrAlignClass      = 'hr-mid';
		} elseif ( $header_align == 'left' ) {
			$headerCenterClass = '';
			$hrAlignClass      = 'hr-left';
		} elseif ( $header_align == 'right' ) {
			$headerCenterClass = 'text-right';
			$hrAlignClass      = 'hr-right';
		}

		if ( $header_gloria == 'true' || $header_gloria == 'yes' ) {
			$headerGloriaClass = 'gloria';
		} else {
			$headerGloriaClass = '';
		}


		if ( is_page_template() == 'page-custom.php' || is_page_template() == 'page-custom-left.php' ) {
			$containerClass = '';
			$sectionClass   = '';
		} else {
			$containerClass = 'container';
			$sectionClass   = 'section';
		}

		if ( $padding == 'top' ) {
			$sectionClass = 'section-top';
		} elseif ( $padding == 'bottom' ) {
			$sectionClass = 'section-bottom';
		}

		$paddingSize = '';
		if ( $padding_size == 'small' ) {
			$paddingSize = 'small-padding';
		} elseif ( $padding_size == 'none' ) {
			$paddingSize = 'no-padding';

		}

		$subheader = $subheader != '' ? $this->embedShortcode( 'header', array(
			'level'     => 2,
			'align'     => $header_align,
			'gloria'    => 'no',
			'light'     => 'yes',
			'line'      => 'no',
			'uppercase' => 'yes'
		), $subheader ) : '';

		switch ( $style ) {
			default:
			case 'white':
			case 'grey':
			case 'dark_grey':
			case 'dark_blue':
			case 'motive':
			case 'custom':

				switch ( $style ) {
					case 'white':
						$bgClass = '';
						break;
					case 'grey':
						$bgClass = 'bg2';
						break;
					case 'dark_grey':
						$bgClass = 'bg6';
						break;
					case 'dark_blue':
						$bgClass = 'bg3';
						break;
					case 'motive':
						$bgClass = 'bg4';
						break;

					case 'custom':
						$bgClass = '';
						break;
					default:
						$bgClass = '';
				}

				$mainContainerAtts = array(
					'class'       => array(
						$sectionClass,
						$bgClass,
						( $boxed == 'yes' || $boxed == 'true' ) ? $containerClass : '',
						( $scroll == 'true' || $scroll == 'true' ) ? 'scroll' : '',
						$padding,
						$paddingSize
					),
					'id'          => $id,
					'data-height' => $section_height,
				);

				if ( $style = 'custom' ) {
					$mainContainerAtts['data-bg-color'] = $bg_color;
				}

				$html = '<section' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>
                    <div class="' . $containerClass . '">
                    ' . ( $header != '' ? '<h' . $header_level . ' class="uppercase ' . $headerGloriaClass . ' ' . $hrAlignClass . ' ' . $headerCenterClass . '">' . $header . '</h' . $header_level . '>' : '' ) . '
                    ' . $subheader . '
                    ' . $content . '
                    </div>
                    </section>';
				break;

			case 'parallax':
			case 'parallax_dark':


				$mainContainerAtts = array(
					'class'                         => array(
						'media-section',
						'stellar',
						( $style == 'parallax_dark' ) ? '' : 'darkbg',
						( $scroll == 'true' || $scroll == 'true' ) ? 'scroll' : '',
						$padding,
						$paddingSize
					),
					'id'                            => $id,
					'data-stellar-background-ratio' => $parallax_ratio,
					'data-height'                   => $section_height,
					'data-image-mobile'             => $parallax_image_mobile,
					'data-image'                    => $bgd_image,
					'data-type'                     => 'parallax'
				);

				$html = '<section' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>
                <div class="inner">
                <div class="' . $containerClass . '">
                ' . ( $header != '' ? '<h' . $header_level . ' class="uppercase ' . $headerGloriaClass . ' ' . $hrAlignClass . ' ' . $headerCenterClass . '">' . $header . '</h' . $header_level . '>' : '' ) . '
                ' . $subheader . '
                ' . $content . '
                </div>
                </div>
                </section>';
				break;

			case 'pattern':


				$mainContainerAtts = array(
					'class'             => array(
						'media-section',
						( $scroll == 'true' || $scroll == 'true' ) ? 'scroll' : '',
						$padding,
						$paddingSize
					),
					'id'                => $id,
					'data-height'       => $section_height,
					'data-image-mobile' => $parallax_image_mobile,
					'data-image'        => $bgd_image,
					'data-type'         => 'background'
				);

				$html = '<section' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>
                <div class="inner">
                <div class="' . $containerClass . '">
                ' . ( $header != '' ? '<h' . $header_level . ' class="uppercase ' . $headerGloriaClass . ' ' . $hrAlignClass . ' ' . $headerCenterClass . '">' . $header . '</h' . $header_level . '>' : '' ) . '
                ' . $subheader . '
                ' . $content . '
                </div>
                </div>
                </section>';
				break;


			case 'kenburns':
			case 'kenburns_dark':

				$mainContainerAtts = array(
					'class'       => array(
						'media-section',
						( $style == 'kenburns_dark' ) ? '' : 'darkbg',
						( $scroll == 'true' || $scroll == 'true' ) ? 'scroll' : ''
					),
					'id'          => $id,
					'data-height' => $section_height,
					'data-type'   => 'kenburns',
					$padding,
					$paddingSize
				);

				$html = '<section ' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>
                <div class="media-section-image-container">
                ';
				foreach ( explode( ',', $kb_images ) as $i ) {
					$html .= '<img src="' . esc_attr( $i ) . '">';
				}

				$html .= '</div>
                <div class="inner">
                <div class="' . $containerClass . ' ' . $headerCenterClass . '">
                ' . ( $header != '' ? '<h' . $header_level . ' class="uppercase ' . $headerGloriaClass . ' ' . $hrAlignClass . ' ' . $headerCenterClass . '">' . $header . '</h' . $header_level . '>' : '' ) . '
                ' . $subheader . '
                ' . $content . '
                </div>
                </div>
                </section>';
				break;


			case 'video':
				/*
				if ($video_muted == 'yes' || $video_muted =='true'){
					$video_src.='&amp;api=1&amp;player_id='.$id;
				}
				*/

				$mainContainerAtts = array(
					'class'               => array(
						'media-section',
						'html5',
						( $style == 'kenburns_dark' ) ? '' : 'darkbg',
						( $scroll == 'yes' || $scroll == 'true' ) ? 'scroll' : '',
						$padding,
						$paddingSize
					),
					'id'                  => $id,
					'data-height'         => $section_height,
					'data-type'           => 'video',
					'data-fallback-image' => $video_fallback_image
				);

				$html = '<section ' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>
                    <div class="video">
                    <!-- in order for video to be muted you must add  &amp;api=1&amp;player_id=vimeoplayer1 to the end of the video src
                     If you have more than one video, make sure that player_id and id have dif names on each video-->
                    <video id="' . $id . '" muted="" loop="" autoplay="autoplay" preload="auto" width="100%" height="100%">
                    <source src="' . $video_src . '" type="video/' . $video_type . '">
                    </video>
                    </div>
                    <div class="display-table bg5" style="height: ' . $section_height . 'px;">
                    <div class="inner">
                    <div class="' . $containerClass . '">
                    ' . ( $header != '' ? '<h' . $header_level . ' class="uppercase ' . $headerGloriaClass . ' ' . $hrAlignClass . ' ' . $headerCenterClass . '">' . $header . '</h' . $header_level . '>' : '' ) . '
                    ' . $subheader . '
                    ' . $content . '
                    </div>
                    </div>
                    </div>
                    </section>';
				break;
		}

		return $this->doShortcode( '[full_width]' . $html . '[/full_width]' );
		//return do_shortcode($html);


	}

	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {

		return array(
			'header'                => array(
				'label'   => __( 'Header', 'ct_theme' ),
				'default' => '',
				'type'    => 'input'
			),
			'header_level'          => array(
				'label'   => __( 'Level of header', 'ct_theme' ),
				'default' => '3',
				'type'    => 'select',
				'options' => array(
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6',
				)
			),
			'header_align'          => array(
				'label'   => __( 'Align header', 'ct_theme' ),
				'default' => 'center',
				'type'    => 'select',
				'choices' => array(
					'left'   => __( 'left', 'ct_theme' ),
					'center' => __( 'center', 'ct_theme' ),
					'right'  => __( 'right', 'ct_theme' )

				),
			),
			'header_gloria'         => array(
				'label'   => __( 'Gloria style header?', 'ct_theme' ),
				'default' => 'no',
				'type'    => 'select',
				'choices' => array(
					'yes' => __( 'yes', 'ct_theme' ),
					'no'  => __( 'no', 'ct_theme' )
				),
			),
			'subheader'             => array(
				'label'   => __( 'Sub Header', 'ct_theme' ),
				'default' => '',
				'type'    => 'input'
			),
			'style'                 => array(
				'label'   => __( 'Section Type', 'ct_theme' ),
				'default' => 'white',
				'type'    => 'select',
				'help'    => __( "Select section style", 'ct_theme' ),
				'choices' => array(
					'white'         => 'white',
					'grey'          => 'grey',
					'dark_grey'     => 'dark grey',
					'dark_blue'     => 'dark blue',
					'motive'        => 'motive',
					'custom'        => 'custom background color',
					'pattern'       => 'background pattern',
					'parallax'      => 'parallax',
					'parallax_dark' => 'parallax dark text',
					'kenburns'      => 'kenburns',
					'kenburns_dark' => 'kenburns dark text',
					'video'         => 'video',
				),
			),
			'bg_color'              => array(
				'label'      => __( 'Custom background color', 'ct_theme' ),
				'default'    => '',
				'type'       => 'colorpicker',
				'dependency' => array(
					'element' => 'style',
					'value'   => array( 'custom' )
				)
			),
			'bgd_image'             => array(
				'label'      => __( "Background Image", 'ct_theme' ),
				'default'    => '',
				'type'       => 'image',
				'help'       => __( "Image source", 'ct_theme' ),
				'dependency' => array(
					'element' => 'style',
					'value'   => array( 'parallax', 'parallax_dark', 'pattern' )
				)
			),
			'kb_images'             => array(
				'label'      => __( "Kenburn Images", 'ct_theme' ),
				'default'    => '',
				'type'       => 'images',
				'help'       => __( "Image source", 'ct_theme' ),
				'dependency' => array(
					'element' => 'style',
					'value'   => array( 'kenburns', 'kenburns_dark' )
				)
			),
			'parallax_image_mobile' => array(
				'label'      => __( "Parallax Image Mobile", 'ct_theme' ),
				'default'    => '',
				'type'       => 'image',
				'help'       => __( "Image for mobile devices", 'ct_theme' ),
				'dependency' => array(
					'element' => 'style',
					'value'   => array( 'parallax', 'parallax_dark', 'pattern' )
				)
			),
			'parallax_ratio'        => array(
				'label'      => __( 'Parallax Ratio', 'ct_theme' ),
				'default'    => 0.1,
				'type'       => 'input',
				'dependency' => array(
					'element' => 'style',
					'value'   => array( 'parallax', 'parallax_dark' )
				)
			),
			'video_src'             => array(
				'label'      => __( 'Video Background SRC', 'ct_theme' ),
				'default'    => '',
				'type'       => 'input',
				'dependency' => array(
					'element' => 'style',
					'value'   => array( 'video' )
				)
			),
			'video_fallback_image'  => array(
				'label'      => __( 'Video Fallback Image', 'ct_theme' ),
				'default'    => '',
				'type'       => 'image',
				'dependency' => array(
					'element' => 'style',
					'value'   => array( 'video' )
				)
			),
			'video_muted'           => array(
				'label'      => __( 'Muted video ?', 'ct_theme' ),
				'default'    => 'yes',
				'type'       => 'select',
				'choices'    => array(
					'yes' => __( 'yes', 'ct_theme' ),
					'no'  => __( 'no', 'ct_theme' )
				),
				'dependency' => array(
					'element' => 'style',
					'value'   => array( 'video' )
				)
			),
			'video_type'            => array(
				'label'      => __( 'Video Type', 'ct_theme' ),
				'default'    => 'mp4',
				'type'       => 'select',
				'help'       => __( "Select section style", 'ct_theme' ),
				'choices'    => array(
					'mp4'  => 'mp4',
					'webm' => 'webm',
				),
				'dependency' => array(
					'element' => 'style',
					'value'   => array( 'video' )
				)
			),
			'padding'               => array(
				'label'   => __( 'Padding', 'ct_theme' ),
				'default' => '',
				'type'    => 'select',
				'options' => array(
					''       => __( 'Both', 'ct_theme' ),
					'top'    => __( 'Top', 'ct_theme' ),
					'bottom' => __( 'Bottom', 'ct_theme' ),
				)
			),
			'padding_size'          => array(
				'label'   => __( 'Padding size', 'ct_theme' ),
				'default' => '',
				'type'    => 'select',
				'options' => array(
					''      => __( 'Default', 'ct_theme' ),
					'small' => __( 'Small', 'ct_theme' ),
					'none'  => __( 'None', 'ct_theme' ),
				)
			),
			'section_height'        => array(
				'label'   => __( 'Section Height', 'ct_theme' ),
				'default' => 220,
				'type'    => 'input',
				'help'    => __( "% is also allowed. For fullscreen element please use 100%", 'ct_theme' )
			),
			'boxed'                 => array(
				'label'      => __( 'Boxed', 'ct_theme' ),
				'default'    => 'no',
				'type'       => 'select',
				'choices'    => array(
					'yes' => __( 'yes', 'ct_theme' ),
					'no'  => __( 'no', 'ct_theme' )
				),
				'dependency' => array(
					'element' => 'style',
					'value'   => array( 'white', 'grey', 'dark_grey', 'dark_blue', 'motive', 'custom' ),
				)
			),
			'content'               => array(
				'label'   => __( 'content', 'ct_theme' ),
				'default' => '',
				'type'    => "textarea"
			),
			'scroll'                => array(
				'label'   => __( 'Onepager element ?', 'ct_theme' ),
				'default' => 'no',
				'type'    => 'select',
				'choices' => array(
					'yes' => __( 'yes', 'ct_theme' ),
					'no'  => __( 'no', 'ct_theme' )
				),
				'help'    => __( "When using onepager page, enable this option if your navigation should scroll to this element.", 'ct_theme' )
			),
			'id'                    => array(
				'label'   => __( 'ID', 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( "ID of element. It may also be used as navigation anchor in onepager mode.", 'ct_theme' )
			),
			'class'                 => array(
				'label'   => __( "Custom class", 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( 'Adding custom class allows you to set diverse styles in css to the element. Type in name of class, which you defined in css. You can add as much classes as you like.', 'ct_theme' )
			),
		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array(
			'container' => true,
		) );
	}
}

new ctSectionShortcode();
<?php

/**
 * Usefull Links Item shortcode
 */
class ctUsefullLinksShortcode extends ctShortcode
{

    /**
     * Returns name
     * @return string|void
     */
    public function getName()
    {
        return 'Usefull Links';
    }

    /**
     * Shortcode name
     * @return string
     */
    public function getShortcodeName()
    {
        return 'usefull_links';
    }


    /**
     * Handles shortcode
     * @param $atts
     * @param null $content
     * @return string
     */

    public function handle($atts, $content = null)
    {
        $attributes = shortcode_atts($this->extractShortcodeAttributes($atts), $atts);
        extract($attributes);

        $mainContainerAtts = array(
            'class' => array($class)
        );
        $title = ($title != '') ? '<h4 class="widget-title">' . $title . '</h4>' : '';
        $html = $title;
        $html.= '<ul ' . $this->buildContainerAttributes($mainContainerAtts, $atts) . '>' . $content . '</ul>';
        return do_shortcode($html);
    }

    /**
     * Returns config
     * @return null
     */
    public function getAttributes()
    {
        return array(
            'title' => array('label' => __('title', 'ct_theme'), 'default' => '', 'type' => 'input'),
            'class' => array('label' => __('Custom class', 'ct_theme'), 'default' => '', 'type' => 'input', 'help' => __('Adding custom class allows you to set diverse styles in css to the element. Type in name of class, which you defined in css. You can add as much classes as you like.', 'ct_theme')),
        );
    }

    /**
     * Child shortcode info
     * @return array
     */

    public function getChildShortcodeInfo()
    {
        return array('name' => 'usefull_links_item', 'min' => 1, 'max' => 100, 'default_qty' => 1);
    }
}

new ctUsefullLinksShortcode();



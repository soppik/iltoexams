<?php

/**
 * Contact shortcode
 */
class ctContactWidgetShortcode extends ctShortcode
{

    /**
     * Returns name
     * @return string|void
     */
    public function getName()
    {
        return 'Contact';
    }

    /**
     * Shortcode name
     * @return string
     */
    public function getShortcodeName()
    {
        return 'contact_widget';
    }

    /**
     * Handles shortcode
     * @param $atts
     * @param null $content
     * @return string
     */

    public function handle($atts, $content = null)
    {
        $class = '';
        $shortcodeEmail = '';
        $shortcodePhone = '';

        extract(shortcode_atts($this->extractShortcodeAttributes($atts), $atts));


        $arrayEmail = explode(",", $email);
        $arrayPhone = explode(",", $phone);

        if ($email != '') {

            $shortcodeEmail = '';
            foreach ($arrayEmail as $value) {
                $shortcodeEmail .= '[icon_text class="fa-fw motive" text="' . $value . '" email="true" icon="fa-envelope" link="' . $value . '"]';
            }
        }

        if ($phone != '') {

            $shortcodePhone = '';
            foreach ($arrayPhone as $value) {
                $shortcodePhone .= '[icon_text class="fa-fw motive" icon_text text="' . $value . '" email="false" icon="fa-phone"]';
            }
        }


        $shortcodeContact = '
                    <p>
                    ' . $address . '
                    <span class="footer-contacts">
                        ' . $shortcodePhone . '<br>
                        ' . $shortcodeEmail . '
                    </span>
                </p>
            ';


        return do_shortcode($shortcodeContact);
    }


    /**
     * Returns config
     *
     * @return null
     */
    public function getAttributes()
    {
        return array(
            'title' => array('label' => __('Title', 'ct_theme'), 'default' => __('Contact us', 'ct_theme'), 'type' => 'input'),
            'widgetmode' => array('default' => 'false', 'type' => false),
            'phone' => array('label' => __('phone', 'ct_theme'), 'default' => '', 'type' => 'input', 'help' => __("enter phone number. separating items with a comma", 'ct_theme')),
            'email' => array('label' => __('email', 'ct_theme'), 'default' => '', 'type' => 'input', 'help' => __("enter email address. separating items with a comma", 'ct_theme')),
            'address' => array('label' => __('address', 'ct_theme'), 'default' => '', 'type' => 'input', 'help' => __('type ' . htmlspecialchars('<br>') . ' to enter the next line', 'ct_theme')),
        );
    }
}

new ctContactWidgetShortcode();
<?php

/**
 * Footer background shortcode
 */
class ctFooterBackgroundShortcodeWidget extends ctShortcode {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Footer Background';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'footer_background';
	}

	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );
		$this->addInlineJS( $this->getInlineJS( $atts ) );

		return '<div style="display:none" data-bgfooter="'.esc_attr($image_inside_footer).'" data-bg="'.esc_attr($image_before_footer).'" data-height="'.((int)$image_before_footer_height).'" class="pre-footer-image-data"></div>';
	}

	/**
	 * returns JS
	 *
	 * @param $id
	 *
	 * @return string
	 */
	protected function getInlineJS( $atts ) {
		return "
            jQuery('.pre-footer-image-data').each(function(){
                var \$this = jQuery(this);
                var c =\$this.closest('.container');
                c.before('<div class=\"pre-footer-image\" style=\"background-image: url('+\$this.attr('data-bg')+');height:'+\$this.attr('data-height') +'px\"></div>');
                c.wrap('<div class=\"pre-footer\" style=\"background-image: url('+\$this.attr('data-bgfooter')+')\"></div>');
            });";
	}

	/**
	 * Returns config
	 *
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'image_before_footer'        => array(
				'label'   => __( "Image before footer", 'ct_theme' ),
				'default' => '',
				'type'    => 'image',
				'help'    => __( "Image source", 'ct_theme' )
			),
			'image_before_footer_height' => array(
				'label'   => __( "Height", 'ct_theme' ),
				'default' => '',
				'type'    => 'image',
				'help'    => __( "Height of image, in pixels", 'ct_theme' )
			),
			'image_inside_footer'        => array(
				'label'   => __( "Image inside footer", 'ct_theme' ),
				'default' => '',
				'type'    => 'image',
				'help'    => __( "Image source", 'ct_theme' )
			),
		);
	}
}

new ctFooterBackgroundShortcodeWidget();
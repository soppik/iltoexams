<?php

/**
 * Usefull Links shortcode
 */
class  ctUsefullLinksItemShortcode extends ctShortcode
{

    /**
     * Returns name
     * @return string|void
     */
    public function getName()
    {
        return 'Usefull Links Item';
    }

    /**
     * Shortcode name
     * @return string
     */
    public function getShortcodeName()
    {
        return 'usefull_links_item';
    }

    /**
     * Handles shortcode
     * @param $atts
     * @param null $content
     * @return string
     */
    public function handle($atts, $content = null)
    {
        extract(shortcode_atts($this->extractShortcodeAttributes($atts), $atts));

        return '<li><a href="' . $link . '">' . $title . '</a></li>';

    }



    /**
     * Parent shortcode name
     * @return null
     */

    public function getParentShortcodeName()
    {
        return 'usefull_link';
    }


    /**
     * Returns config
     * @return null
     */
    public function getAttributes()
    {
        return array(
            'title' => array('label' => __('title', 'ct_theme'), 'default' => '', 'type' => 'input'),
            'link' => array('label' => __('Link', 'ct_theme'), 'default' => '', 'type' => 'input'),

        );
    }
}

new ctUsefullLinksItemShortcode();
<?php

/**
 * Draws works
 */
class ctRecentProjects extends ctShortcodeQueryable
{

    /**
     * Returns name
     * @return string|void
     */
    public function getName()
    {
        return 'Recent Projects';
    }

    /**
     * Shortcode name
     * @return string
     */
    public function getShortcodeName()
    {
        return 'recent_projects';
    }


    /**
     * Handles shortcode
     * @param $atts
     * @param null $content
     * @return string
     */

    public function handle($atts, $content = null)
    {
        $attributes = shortcode_atts($this->extractShortcodeAttributes($atts), $atts);
        $title='';
        extract($attributes);

        $recentposts = $this->getCollection($attributes, array('post_type' => 'portfolio'));


        //elements
        $elements = '';
        foreach ($recentposts as $p) {
            if (has_post_thumbnail($p->ID)) {
                $imgsrc = ct_get_feature_image_src($p->ID, 'thumbnail');
                $elements .= '<a href="' . get_permalink($p->ID) . '"><img src="' . $imgsrc . '" alt=""></a>';
            } else {
                continue;
            }
        }


        $headerHtml = $title ? ('<h4 class="widget-title">' . $title . '</h4>') : '';
        $html = $headerHtml.'<div class="images-widget">'  . $elements . '</div>';

        return do_shortcode($html);

    }


    /**
     * creates class name for the category
     * @param $cat
     * @return string
     */
    protected function getCatFilterClass($cat)
    {
        return strtolower(str_replace(' ', '-', $cat->slug));
    }


    /**
     * Shortcode type
     * @return string
     */
    public function getShortcodeType()
    {
        return self::TYPE_SHORTCODE_SELF_CLOSING;
    }

    /**
     * Returns config
     * @return null
     */
    public function getAttributes()
    {
        $atts = $this->getAttributesWithQuery(array(
            'limit' => array('label' => __('limit', 'ct_theme'), 'default' => 4, 'type' => 'input', 'help' => __("Number of portfolio elements", 'ct_theme')),
            'title' => array('label' => __('title', 'ct_theme'), 'default' => '', 'type' => 'input'),
            'class' => array('label' => __("Custom class", 'ct_theme'), 'default' => '', 'type' => 'input', 'help' => __('Adding custom class allows you to set diverse styles in css to the element. Type in name of class, which you defined in css. You can add as much classes as you like.', 'ct_theme')),

        ));

        if (isset($atts['cat'])) {
            unset($atts['cat']);
        }
        return $atts;
    }
}

new ctRecentProjects();
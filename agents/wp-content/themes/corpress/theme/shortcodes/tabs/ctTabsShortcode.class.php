<?php

/**
 * Tabs shortcode
 */
class ctTabsShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Tabs';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'tabs';
	}


	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		$attributes = shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts );
		extract( $attributes );
		$mainContainerAtts = array(
			'class' => array(
				'tabs-' . $type,
				$class
			)
		);
		//parse shortcode before filters
		$itemsHtml = do_shortcode( $content );

		$tabs = '<div ' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>';
		$tabs .= '<ul class="nav nav-tabs' . ( $align ? ' nav-justified' : '' ) . '">';
		$tabs .= $this->callPreFilter( '' ); //reference
		$tabs .= '</ul>';

		//clean current tab cache
		$this->cleanData( 'tab' );


		$tabs = $tabs . '<div class="tab-content">' . $itemsHtml . '</div></div>';


		return do_shortcode( $tabs );
	}


	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'type'  => array(
				'label'   => __( 'Type', 'ct_theme' ),
				'type'    => "select",
				'default' => 'horizontal',
				'choices' => array(
					'horizontal' => __( "Horizontal", 'ct_theme' ),
					'vertical'   => __( "Vertical", 'ct_theme' )
				)
			),
			'align' => array(
				'label'   => __( "Tabs alignment", 'ct_theme' ),
				'type'    => "select",
				'default' => '',
				'choices' => array(
					''        => __( "Left", 'ct_theme' ),
					'justify' => __( "Justified", 'ct_theme' )
				)
			),
			'class' => array(
				'label'   => __( 'Custom class', 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( 'Adding custom class allows you to set diverse styles in css to the element. Type in name of class, which you defined in css. You can add as much classes as you like.', 'ct_theme' )
			),
		);
	}

	/**
	 * Child shortcode info
	 * @return array
	 */

	public function getChildShortcodeInfo() {
		return array( 'name' => 'tab', 'min' => 2, 'max' => 50, 'default_qty' => 1 );
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-list-alt' ) );
	}
}

new ctTabsShortcode();
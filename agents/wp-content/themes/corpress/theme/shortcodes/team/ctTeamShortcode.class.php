<?php

/**
 * Draws team
 */
class ctTeamShortcode extends ctShortcodeQueryable implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Team';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'team';
	}


	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {

		$attributes = shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts );
		extract( $attributes );

		$team = $this->getCollection( $attributes, array( 'post_type' => 'team' ) );

		$counter  = 0;
		$counter2 = 0;

		$teamBoxHtml = '';
		//var_dump($team);

		foreach ( $team as $p ) {
			$custom = get_post_custom( $p->ID );


			$job_position_short = isset( $custom["job_position_short"][0] ) ? $custom["job_position_short"][0] : "";
			$socials            = isset( $custom["socials"][0] ) ? $custom["socials"][0] : "";
			$summary            = isset( $custom["summary"][0] ) ? $custom["summary"][0] : "";


			if ( has_post_thumbnail( $p->ID ) ) {
				$image = ct_get_feature_image_src( $p->ID, 'full' );
			} else {
				$image = '';
			}


			$counter2 ++;
			if ( $counter2 == 1 ) {
				$teamBoxHtml .= '[row top_space="true"]';
			}


			$teamBoxHtml .= '[third_column md="4"]';
			//forward params
			$teamBoxHtml .= ( '[person_box title="' . get_the_title( $p->ID ) . '" sub_title="' . $job_position_short . '" image="' . $image . '" link="' . get_permalink( $p->ID ) . '"]' . '<p>' . $summary . '</p>' . $socials . '[/person_box]' );
			//$teamBoxHtml .= get_the_content($p->ID);
			$teamBoxHtml .= '[/third_column]';

			if ( $counter2 == 3 || $counter == count( $team ) ) {
				$counter2 = 0;
				$teamBoxHtml .= '[/row]';
			}
		}

		return do_shortcode( $teamBoxHtml );
	}


	/**
	 * Returns params from array ($custom)
	 *
	 * @param $arr
	 * @param $key
	 * @param int $index
	 * @param string $default
	 *
	 * @return bool
	 */

	protected function getFromArray( $arr, $key, $index = 0, $default = '' ) {
		return isset( $arr[ $key ][ $index ] ) ? $arr[ $key ][ $index ] : $default;
	}

	/**
	 * Shortcode type
	 * @return string
	 */
	public function getShortcodeType() {
		return self::TYPE_SHORTCODE_SELF_CLOSING;
	}

	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		$atts = $this->getAttributesWithQuery( array(
			'limit' => array(
				'label'   => __( 'limit', 'ct_theme' ),
				'default' => 4,
				'type'    => 'input',
				'help'    => __( "Number of elements", 'ct_theme' )
			),

		) );

		if ( isset( $atts['cat'] ) ) {
			unset( $atts['cat'] );
		}

		return $atts;
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-user' ) );
	}
}


new ctTeamShortcode();

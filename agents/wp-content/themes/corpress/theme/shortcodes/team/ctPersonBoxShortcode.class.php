<?php

/**
 * Pricelist shortcode
 */
class ctPersonBoxShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Person box';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'person_box';
	}

	/**
	 * Returns shortcode type
	 * @return mixed|string
	 */

	public function getShortcodeType() {
		return self::TYPE_SHORTCODE_ENCLOSING;
	}


	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );


		$mainContainerAtts = array(
			'class' => array(
				'person-box',
				$class
			)
		);

		$link = $link ? '<a href="' . $link . '">' . $title . '</a>' : '';
		$html = '<div ' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>
                        <div class="image-wrapper">
                            <img src="' . $image . '" alt="' . $title . ' - ' . $sub_title . '">
                        </div>
                        <h4 class="uppercase hr-mid">' . $link . '</a></h4>
                        <h4 class="uppercase weight300">' . $sub_title . '</h4>
                        ' . $content . '</div>';

		return do_shortcode( $html );
	}


	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'title'     => array( 'label' => __( 'Title', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'sub_title' => array( 'label' => __( 'Subtitle', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'image'     => array(
				'label'   => __( "Image", 'ct_theme' ),
				'default' => '',
				'type'    => 'image',
				'help'    => __( "Image source", 'ct_theme' )
			),
			'link'      => array( 'label' => __( 'Link', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'content'   => array( 'label' => __( 'Content', 'ct_theme' ), 'default' => '', 'type' => 'textarea' ),
			'class'     => array(
				'label'   => __( "Custom class", 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( "Custom class name", 'ct_theme' )
			),
		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-user' ) );
	}
}

new ctPersonBoxShortcode();

<?php

/**
 * Pricelist shortcode
 */
class ctCallUsShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface
{

    /**
     * Returns name
     * @return string|void
     */
    public function getName()
    {
        return 'Call Us';
    }

    /**
     * Shortcode name
     * @return string
     */
    public function getShortcodeName()
    {
        return 'call_us';
    }

    /**
     * Returns shortcode type
     * @return mixed|string
     */

    public function getShortcodeType()
    {
        return self::TYPE_SHORTCODE_ENCLOSING;
    }


    /**
     * Handles shortcode
     *
     * @param $atts
     * @param null $content
     *
     * @return string
     */

    public function handle($atts, $content = null)
    {
        extract(shortcode_atts($this->extractShortcodeAttributes($atts), $atts));
        $buttonHtml = $this->buildRelatedShortcodeIf('button', $atts, 'content', 'button2');


        if ($type == 'medium') {

            $mainContainerAtts = array(
                'class' => array(
                    'call-box',
                    'bg3',
                ),
            );


            $html = '
            <div ' . $this->buildContainerAttributes($mainContainerAtts, $atts) . '>
    <div class="inner">
        <div class="container">
            <div class="row">
                <div class="table-cell col-md-10">
                    <span class="medium uppercase animated activate flash" data-fx="flash"><i class="motive weight400">' . $text1 . '</i> <i class="red weight400">' . $text2 . '</i>' . $text3 . '</span>
                </div>';

            if (!empty($buttonHtml)) {
                $html .= '<div class="table-cell col-md-2 text-right">' . $buttonHtml . '</div>';
            }
            $html .= ' </div>
        </div>
    </div>
</div>
            ';

        } else {

            $mainContainerAtts = array(
                'class' => array(
                    'small',
                    'section',
                    'bg2'
                ),
            );

            $html = '
            <div ' . $this->buildContainerAttributes($mainContainerAtts, $atts) . '>
    <div class="inner">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <span class="big animated activate bounceInRight" data-fx="bounceInRight">
                        <small>
                            <i class="red uppercase">' . $text1 . '</i>
                        </small>' . $text3 . '</span>
                </div>
            </div>
        </div>
    </div>
</div>

';
        }

        return do_shortcode($html);
    }


    /**
     * Returns config
     * @return null
     */
    public function getAttributes()
    {
        $items = array(
            'text1' => array('label' => __('Text', 'ct_theme'), 'default' => '', 'type' => 'input'),
            'text2' => array(
                'label' => __('Text 2', 'ct_theme'),
                'default' => '',
                'type' => 'input',
                'help' => __("For medium style", 'ct_theme')
            ),
            'text3' => array('label' => __('Text 3', 'ct_theme'), 'default' => '', 'type' => 'input'),
            'type' => array(
                'label' => __('Type', 'ct_theme'),
                'default' => 'medium',
                'type' => 'select',
                'choices' => array('medium' => 'medium', 'big' => 'big'),
                'help' => __("Large or medium style", 'ct_theme')
            ),
            'class' => array(
                'label' => __("Custom class", 'ct_theme'),
                'default' => '',
                'type' => 'input',
                'help' => __('Adding custom class allows you to set diverse styles in css to the element. Type in name of class, which you defined in css. You can add as much classes as you like.', 'ct_theme')
            ),
        );

        //merge additional button
        $items = $this->mergeShortcodeAttributes($items, 'button', 'button1', __('Button1', 'ct_theme'));
        $items = $this->mergeShortcodeAttributes($items, 'button', 'button2', __('Button2', 'ct_theme'));

        return $items;

    }

    /**
     * Returns additional info about VC
     * @return ctVisualComposerInfo
     */
    public function getVisualComposerInfo()
    {
        return new ctVisualComposerInfo($this, array('icon' => 'fa-phone'));
    }
}

new ctCallUsShortcode();




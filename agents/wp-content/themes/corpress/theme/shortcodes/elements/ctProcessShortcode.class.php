<?php

/**
 * Process shortcode
 */
class ctProcessShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Process';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'process';
	}

	/**
	 * Returns shortcode type
	 * @return mixed|string
	 */

	public function getShortcodeType() {
		return self::TYPE_SHORTCODE_ENCLOSING;
	}


	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );

		$mainContainerAtts = array(
			'class' => array(
				'ct-process',
				$class,
			),
		);


		$html = '
        <div ' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>
                <div class="row">
                    <div class="display-table">
                        <div class="hidden-sm table-cell ct-process-icon">
                            <span class="ct-process-right"><i class="fa ' . $icon_left_name . '"></i></span>
                        </div>
                        <div class="table-cell text-center" data-fx="bounceIn" data-time="50">
                            <a href="' . $box1_url . '"><img class="img-circle img-responsive" src="' . $box1_image . '" alt="Research"></a>
                            <div class="shadow"></div>
                            <span class="gloria motive">' . $box1_text . '</span>
                        </div>
                        <div class="table-cell text-center animated activate bounceIn" data-fx="bounceIn" data-time="150">
                            <a href="' . $box2_url . '"><img class="img-circle img-responsive" src="' . $box2_image . '" alt="Idea"></a>
                            <div class="shadow"></div>
                            <span class="gloria motive">' . $box2_text . '</span>
                        </div>
                        <div class="table-cell text-center animated activate bounceIn" data-fx="bounceIn" data-time="250">
                            <a href="' . $box3_url . '"><img class="img-circle img-responsive" src="' . $box3_image . '" alt="Solution"></a>
                            <div class="shadow"></div>
                            <span class="gloria motive">' . $box3_text . '</span>
                        </div>
                        <div class="hidden-sm table-cell ct-process-icon">
                            <span class="ct-process-left"><i class="fa ' . $icon_right_name . '"></i></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        ';

		return do_shortcode( $html );
	}


	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'icon_left_name'  => array(
				'label'   => __( 'Icon left name', 'ct_theme' ),
				'type'    => "input",
				'default' => ''
			),
			'icon_right_name' => array(
				'label'   => __( 'Icon right name', 'ct_theme' ),
				'type'    => "input",
				'default' => ''
			),
			'box1_image'      => array(
				'label'   => __( "Box 1 Image", 'ct_theme' ),
				'default' => '',
				'type'    => 'image',
				'help'    => __( "Image source", 'ct_theme' )
			),
			'box1_url'        => array( 'label' => __( 'Box 1 url', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'box1_text'       => array( 'label' => __( 'Box 1 text', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'box2_image'      => array(
				'label'   => __( "Box 2 Image", 'ct_theme' ),
				'default' => '',
				'type'    => 'image',
				'help'    => __( "Image source", 'ct_theme' )
			),
			'box2_url'        => array( 'label' => __( 'Box 2 url', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'box2_text'       => array( 'label' => __( 'Box 2 text', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'box3_image'      => array(
				'label'   => __( "Box 3 Image", 'ct_theme' ),
				'default' => '',
				'type'    => 'image',
				'help'    => __( "Image source", 'ct_theme' )
			),
			'box3_url'        => array( 'label' => __( 'Box 3 url', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'box3_text'       => array( 'label' => __( 'Box 3 text', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'class'           => array(
				'label'   => __( "Custom class", 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( 'Adding custom class allows you to set diverse styles in css to the element. Type in name of class, which you defined in css. You can add as much classes as you like.', 'ct_theme' )
			),
		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-circle' ) );
	}
}

new ctProcessShortcode();




<?php

/**
 * Pricelist shortcode
 */
class ctPromoSectionShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Promo Section';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'promo_section';
	}

	/**
	 * Returns shortcode type
	 * @return mixed|string
	 */

	public function getShortcodeType() {
		return self::TYPE_SHORTCODE_ENCLOSING;
	}


	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );

		$mainContainerAtts = array(
			'class' => array(
				'call-box',
				$style == 'dark_blue' ? 'bg3' : 'bg2',
				$class,
				$padding == 'no' || $padding == 'false' ? 'call-box-no-padding' : ''
			),
		);


		$html = '
            <div ' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>
                <div class="table-cell col-md-7">
                    <h2 class="uppercase">
                        ' . $title . '
                    </h2>
                    <p>' . $content . '</p>
                </div>
                <div class="table-cell col-md-5 text-right">';

		$html .= $this->buildRelatedShortcodeIf( 'button', $atts, 'content', 'button1' );
		$html .= $this->buildRelatedShortcodeIf( 'button', $atts, 'content', 'button2' );

		$html .= '
                </div>
            </div>';

		return do_shortcode( '<div class="row">' . $html . '</div>' );
	}


	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		$items = array(
			'style'   => array(
				'label'   => __( 'Type', 'ct_theme' ),
				'default' => 'dark_blue',
				'type'    => 'select',
				'help'    => __( "Select section style", 'ct_theme' ),
				'choices' => array(
					'grey'      => 'grey',
					'dark_blue' => 'dark blue',
				),
			),
			'title'   => array( 'label' => __( 'title', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'content' => array( 'label' => __( 'Content', 'ct_theme' ), 'default' => '', 'type' => "textarea" ),
			'class'   => array(
				'label'   => __( "Custom class", 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( 'Adding custom class allows you to set diverse styles in css to the element. Type in name of class, which you defined in css. You can add as much classes as you like.', 'ct_theme' )
			),
			'padding' => array(
				'label'   => __( 'With padding ?', 'ct_theme' ),
				'default' => 'yes',
				'type'    => 'select',
				'choices' => array( 'yes' => __( 'yes', 'ct_theme' ), 'no' => __( 'no', 'ct_theme' ) ),
			),

		);

//merge additional button
		$items = $this->mergeShortcodeAttributes( $items, 'button', 'button1', __( 'Button 1', 'ct_theme' ) );
		$items = $this->mergeShortcodeAttributes( $items, 'button', 'button2', __( 'Button 2', 'ct_theme' ) );

		return $items;
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-check-circle' ) );
	}
}

new ctPromoSectionShortcode();




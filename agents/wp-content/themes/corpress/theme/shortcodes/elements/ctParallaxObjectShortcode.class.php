<?php

/**
 * Parallax object Shortcode
 */
class ctParallaxObjectShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Parallax Object';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'parallax_object';
	}

	/**
	 * Returns shortcode type
	 * @return mixed|string
	 */

	public function getShortcodeType() {
		return self::TYPE_SHORTCODE_ENCLOSING;
	}


	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );
		$this->addInlineJS( $this->getInlineJS(), true );


		$mainContainerAtts = array(
			'class'                        => array(
				'stellar-object',
				$class,
			),
			'data-stellar-ratio'           => $ratio,
			'data-stellar-vertical-offset' => $v_offset,
			'data-image'                   => $image,
			'data-width'                   => $width,
			'data-height'                  => $height,
			'data-top'                     => $top,
			'data-left'                    => $left,
		);


		$html = '<div ' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '></div>';

		return do_shortcode( $html );
	}


	/**
	 * returns inline js
	 *
	 * @param $attributes
	 * @param $id
	 *
	 * @return string
	 */
	protected function getInlineJS() {
		return 'jQuery(".stellar-object").each(function () {
    var $this = jQuery(this);
    var $bg = $this.attr("data-image");
    var $height = $this.attr("data-height") + "px";
    var $width = $this.attr("data-width") + "px";
    var $top = $this.attr("data-top");
    var $left = $this.attr("data-left");

        $this.css("background-image", "url(" + $bg + ")");
        $this.css("width", $width);
        $this.css("height", $height);
        $this.css("top", $top);
        $this.css("left", $left);
})';
	}


	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'ratio'    => array( 'Parallax Ratio' => __( 'title', 'ct_theme' ), 'default' => '1.2', 'type' => 'input' ),
			'v_offset' => array( 'label' => __( 'vertical offset', 'ct_theme' ), 'default' => '50', 'type' => 'input' ),
			'image'    => array( 'label' => __( 'Image', 'ct_theme' ), 'default' => '', 'type' => 'image' ),
			'width'    => array(
				'label'   => __( 'Width (in pixels) ', 'ct_theme' ),
				'default' => '',
				'type'    => 'input'
			),
			'height'   => array(
				'label'   => __( 'Height (in pixels)', 'ct_theme' ),
				'default' => '',
				'type'    => 'input'
			),
			'top'      => array(
				'label'   => __( 'Position relative to top (in %)', 'ct_theme' ),
				'default' => '',
				'type'    => 'input'
			),
			'left'     => array(
				'label'   => __( 'Position relative to left (in %)', 'ct_theme' ),
				'default' => '',
				'type'    => 'input'
			),
			'class'    => array(
				'label'   => __( "Custom class", 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( 'Adding custom class allows you to set diverse styles in css to the element. Type in name of class, which you defined in css. You can add as much classes as you like.', 'ct_theme' )
			),
		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-picture-o' ) );
	}
}

new ctParallaxObjectShortcode();





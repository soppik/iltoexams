<?php

/**
 * Pricelist shortcode
 */
class ctCountDownRowShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Countdown Row';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'countdown_row';
	}

	/**
	 * Returns shortcode type
	 * @return mixed|string
	 */

	public function getShortcodeType() {
		return self::TYPE_SHORTCODE_ENCLOSING;
	}


	public function enqueueScripts() {
		wp_register_script( 'final-countdown', CT_THEME_ASSETS . '/js/jquery.final-countdown.min.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'final-countdown' );

		wp_register_script( 'countTo', CT_THEME_ASSETS . '/js/jquery.countTo.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'countTo' );

		wp_register_script( 'kinetic', CT_THEME_ASSETS . '/js/kinetic.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'kinetic' );
	}

	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );
		$start = strtotime( $date_start );
		$end   = strtotime( $date_end );
		if ( $start == false || $end == false ) {
			return '';
		}

		$mainContainerAtts = array(
			'class'             => array(
				'countdown',
				'countdown-container',
				'container',
				$class,
			),
			'data-end'          => $end,
			'data-now'          => time(),
			'data-start'        => $start,
			'data-border-color' => '#2b8be9'
		);

		$this->addInlineJS( $this->getInlineJS( $start, $end ), true );


		$html = '
        <div' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>
            <div class="clock row">
                <div class="clock-item clock-days countdown-time-value text-center col-xs-6 col-sm-3 col-lg-2 col-lg-offset-2">
                    <div class="wrap">
                        <div class="inner">
                            <div id="canvas-days" class="clock-canvas"></div>

                            <div class="text">
                                <p class="val">0</p>
                            </div><!-- /.text -->
                        </div><!-- /.inner -->
                    </div><!-- /.wrap -->
                    <span class="h4">' . __( 'Days', 'ct_theme' ) . '</span>
                </div><!-- /.clock-item -->

                <div class="clock-item clock-hours countdown-time-value text-center col-xs-6 col-sm-3 col-lg-2">
                    <div class="wrap">
                        <div class="inner">
                            <div id="canvas-hours" class="clock-canvas"></div>

                            <div class="text">
                                <p class="val">0</p>

                            </div><!-- /.text -->
                        </div><!-- /.inner -->
                    </div><!-- /.wrap -->
                    <span class="h4">' . __( 'Hours', 'ct_theme' ) . '</span>
                </div><!-- /.clock-item -->

                <div class="clock-item clock-minutes countdown-time-value text-center col-xs-6 col-sm-3 col-lg-2">
                    <div class="wrap">
                        <div class="inner">
                            <div id="canvas-minutes" class="clock-canvas"></div>

                            <div class="text">
                                <p class="val">0</p>
                            </div><!-- /.text -->
                        </div><!-- /.inner -->
                    </div><!-- /.wrap -->
                    <span class="h4">' . __( 'Minutes', 'ct_theme' ) . '</span>
                </div><!-- /.clock-item -->

                <div class="clock-item clock-seconds countdown-time-value text-center col-xs-6 col-sm-3 col-lg-2">
                    <div class="wrap">
                        <div class="inner">
                            <div id="canvas-seconds" class="clock-canvas"></div>

                            <div class="text">
                                <p class="val">0</p>
                            </div><!-- /.text -->
                        </div><!-- /.inner -->
                    </div><!-- /.wrap -->
                    <span class="h4">' . __( 'Seconds', 'ct_theme' ) . '</span>
                </div><!-- /.clock-item -->
            </div><!-- /.clock -->
        </div><!-- /.countdown-wrapper -->

        ';

		return do_shortcode( $html );
	}


	/**
	 * returns JS
	 *
	 * @param $id
	 *
	 * @return string
	 */
	protected function getInlineJS( $start, $end ) {
		return '
        if(jQuery().final_countdown){
jQuery(".countdown").final_countdown({
seconds: {
borderWidth: "10"
},
minutes: {
    borderWidth: "10"
            },
hours: {
    borderWidth: "10"
            },
days: {
    borderWidth: "10"
            }
});
}



    ';
	}

	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'date_start' => array(
				'label'   => __( 'Date start', 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( 'date format: YYYY-MM-DD', 'ct_theme' )
			),
			'date_end'   => array(
				'label'   => __( 'Date end', 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( 'date format: YYYY-MM-DD', 'ct_theme' )
			),
			'class'      => array(
				'label'   => __( "Custom class", 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( 'Adding custom class allows you to set diverse styles in css to the element. Type in name of class, which you defined in css. You can add as much classes as you like.', 'ct_theme' )
			),
		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-clock-o' ) );
	}
}


new ctCountDownRowShortcode();




<?php

/**
 * Pricelist shortcode
 */
class ctMaintenanceRowShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Maintenance Row';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'maintenance_row';
	}

	/**
	 * Returns shortcode type
	 * @return mixed|string
	 */

	public function getShortcodeType() {
		return self::TYPE_SHORTCODE_ENCLOSING;
	}


	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );

		$mainContainerAtts = array(
			'class' => array(
				'bg2',
				$class,
			),
		);


		$html = '
        <div ' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>
        <div class="container text-center section small">
        <h2 class="uppercase hr-mid">' . $title . '</h2>' . $content . '</div></div>';

		return do_shortcode( $html );
	}


	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'title'   => array( 'label' => __( 'title', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'content' => array( 'label' => __( 'Content', 'ct_theme' ), 'default' => '', 'type' => "textarea" ),
			'class'   => array( 'label'   => __( "Custom class", 'ct_theme' ),
			                    'default' => '',
			                    'type'    => 'input',
			                    'help'    => __( 'Adding custom class allows you to set diverse styles in css to the element. Type in name of class, which you defined in css. You can add as much classes as you like.', 'ct_theme' )
			),
		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-cogs' ) );
	}
}

new ctMaintenanceRowShortcode();




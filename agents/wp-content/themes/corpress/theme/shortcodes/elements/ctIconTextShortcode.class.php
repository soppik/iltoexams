<?php

/**
 * Pricelist shortcode
 */
class ctIconTextShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Icon text';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'icon_text';
	}

	/**
	 * Returns shortcode type
	 * @return mixed|string
	 */

	public function getShortcodeType() {
		return self::TYPE_SHORTCODE_ENCLOSING;
	}


	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );

		if ( $new_window == 'yes' || $new_window == 'true' ) {
			$target = '_blank';
		} else {
			$target = '_self';
		}


		$text = strip_tags( strip_shortcodes( $text ) );
		$link = ! empty( $link ) ? $this->embedShortcode( 'link', array(
			'link'   => $link,
			'email'  => $email,
			'target' => $target
		), $text ) : $text;

		$iconShortcode = $icon ? $this->embedShortcode( 'icon', array(
			'class' => 'fa-fw motive',
			'name'  => 'fa ' . $icon
		) ) : '';


		$html = $iconShortcode;
		$html .= $link;

		return do_shortcode( $html );
	}


	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'text'       => array( 'label' => __( 'Text', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'icon'       => array(
				'label'   => __( 'Icon', 'ct_theme' ),
				'type'    => "icon",
				'default' => '',
				'link'    => CT_THEME_ASSETS . '/shortcode/awesome/index.html'
			),
			'link'       => array( 'label' => __( 'Link', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'email'      => array(
				'label'   => __( 'Email link?', 'ct_theme' ),
				'default' => 'no',
				'type'    => 'select',
				'options' => array( 'yes' => __( 'yes', 'ct_theme' ), 'no' => __( 'no', 'ct_theme' ) ),
				'help'    => __( "Select yes for email link", 'ct_theme' )
			),
			'new_window' => array(
				'label'   => __( 'Open link in new Window?', 'ct_theme' ),
				'default' => 'yes',
				'type'    => 'select',
				'choices' => array(
					'yes' => __( 'yes', 'ct_theme' ),
					'no'  => __( 'no', 'ct_theme' )
				)
			),
			'class'      => array(
				'label'   => __( "Custom class", 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( 'Adding custom class allows you to set diverse styles in css to the element. Type in name of class, which you defined in css. You can add as much classes as you like.', 'ct_theme' )
			),
		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-book' ) );
	}
}

new ctIconTextShortcode();




<?php

/**
 * Tab shortcode
 */
class  ctPricingTableItemShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {


	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Pricing Table Item';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'pricing_table_item';
	}

	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */
	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );

		if ( $featured == 'yes' || $featured == 'true' ) {
			ctPricingTableShortcode::$featured = true;
		} else {
			ctPricingTableShortcode::$featured = false;
		}

		$items = do_shortcode( $content );


		return '

        <section class="col-md-3 col-sm-6 text-center">
                        <h' . ( $featured == 'yes' || $featured == 'true' ? '3' : '4' ) . ' class="sep uppercase">' . $title . '</h4>' . $items . '
        </section>

        ';
	}


	/**
	 * Parent shortcode name
	 * @return null
	 */

	public function getParentShortcodeName() {
		return 'pricing_table';
	}

	/**
	 * Child shortcode info
	 * @return array
	 */

	public function getChildShortcodeInfo() {
		return array( 'name' => 'pricing_table_cell', 'min' => 2, 'max' => 50, 'default_qty' => 1 );
	}


	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'title'    => array( 'label' => __( 'title', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'featured' => array(
				'label'   => __( 'Featured', 'ct_theme' ),
				'default' => 'no',
				'type'    => 'select',
				'choices' => array( 'yes' => __( 'yes', 'ct_theme' ), 'no' => __( 'no', 'ct_theme' ) ),
			),
			'content'  => array( 'label' => __( 'Content', 'ct_theme' ), 'default' => '', 'type' => "textarea" ),

		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-table' ) );
	}
}


new ctPricingTableItemShortcode();
<?php

/**
 * Paragraph shortcode
 */
class ctPricingTableCellShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Pricing Table Cell';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'pricing_table_cell';
	}

	/**
	 * Returns shortcode type
	 * @return mixed|string
	 */

	public function getShortcodeType() {
		return self::TYPE_SHORTCODE_ENCLOSING;
	}

	/**
	 * Parent shortcode name
	 * @return null
	 */

	public function getParentShortcodeName() {
		return 'pricing_table_item';
	}

	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );


		if ( $price != '' ) {
			switch ( $currency_position ) {
				case 'left':
					$price_area = '<span class="currency">' . $currency . '</span><span class="price">' . $price . '</span>';
					break;
				case 'left_space':
					$price_area = '<span class="currency">' . $currency . '</span> <span class="price">' . $price . '</span>';
					break;
				case 'right':
					$price_area = '<span class="price">' . $price . '</span><span class="currency">' . $currency . '</span>';
					break;
				case 'right_space':
					$price_area = '<span class="price">' . $price . '</span> <span class="currency">' . $currency . '</span>';
					break;
				default:
					$price_area = '<span class="currency">' . $currency . '</span><span class="price">' . $price . '</span>';
			};

			$price = '
            <div class="pricebox ' . ( ctPricingTableShortcode::$featured == 'true' ? 'motive' : '' ) . '">
            <div class="display-table">
            <div class="table-cell text-right">' . $price_area . '</div>
            <div class="table-cell text-left">
            <span class="overprice">' . $subprice . '</span><br>
            </div></div>' . $content . '</div>
            ';

			return do_shortcode( $price );


		} else {
			return do_shortcode( '<span class="sep">' . $content . '</span>' );
		}
	}

	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'price'             => array( 'label' => __( 'Price', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'subprice'          => array( 'label' => __( 'Subprice', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'currency'          => array( 'label' => __( 'Currency', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'currency_position' => array(
				'label'   => __( 'Content position', 'ct_theme' ),
				'default' => 'left',
				'type'    => 'select',
				'options' => array(
					'left'        => 'left',
					'left_space'  => 'left space',
					'right'       => 'right',
					'right_space' => 'right space',
				)
			),
			'content'           => array(
				'label'   => __( 'Content', 'ct_theme' ),
				'default' => '',
				'type'    => "textarea"
			),
			'class'             => array(
				'label'   => __( 'Custom class', 'ct_theme' ),
				'default' => '',
				'type'    => 'input'
			),
		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-table' ) );
	}
}

new ctPricingTableCellShortcode();
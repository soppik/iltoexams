<?php

/**
 * Big Icon shortcode
 */
class ctBigIconShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Big Icon';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'big_icon';
	}

	/**
	 * Returns shortcode type
	 * @return mixed|string
	 */

	public function getShortcodeType() {
		return self::TYPE_SHORTCODE_ENCLOSING;
	}


	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );


		$mainContainerAtts = array(
			'class' => array(
				'fa',
				$name,
				$class,
				'big_icon',
				'motive'
			)
		);

		$html = '';
		for ( $i = 0; $i < $repeat; $i ++ ) {
			$html .= '<i ' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '></i>';
		}

		return $html;
	}

	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'name'   => array(
				'label'   => __( 'Icon name', 'ct_theme' ),
				'type'    => "icon",
				'default' => '',
				'link'    => CT_THEME_ASSETS . '/shortcode/awesome/index.html'
			),
			'repeat' => array(
				'label'   => __( 'Repeat', 'ct_theme' ),
				'default' => 1,
				'type'    => 'input',
				'help'    => __( 'How many times we should draw icon?', 'ct_theme' )
			),
			'class'  => array(
				'label'   => __( "Custom class", 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( 'Adding custom class allows you to set diverse styles in css to the element. Type in name of class, which you defined in css. You can add as much classes as you like.', 'ct_theme' )
			),
		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-paper-plane' ) );
	}
}

new ctBigIconShortcode();

<?php

/**
 * Blog Masonry Shortcode
 *
 */
class ctBlogMasonryShortcode extends ctShortcodeQueryable implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Blog Masonry';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'blog_masonry';
	}

    public function enqueueScripts() {
        wp_register_script( 'ct-isotope', CT_THEME_ASSETS . '/js/jquery.isotope.min.js', array( 'jquery' ), false, true );
        wp_enqueue_script( 'ct-isotope' );
    }

	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */
	public function handle( $atts, $content = null ) {
		$attributes = shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts );

		extract( $attributes );
		$recentposts = $this->getCollection( $attributes );


		$counter = 0;
		$html    = '';
		foreach ( $recentposts as $p ) {

			//$custom = get_post_custom($p->ID);
			$postFormat = ( get_post_format( $p->ID ) );

			/*get post featured image*/
			$postFeaturedImage = ct_get_feature_image_src( $p->ID, 'featured_image_masonry' );

			if ( $postFeaturedImage == '' && $postFormat == 'video' ) {
				$postFormat = '';
			}

			$class = 'journal';
			//$class = $postFormat == false ? 'blog-item' : 'blog-item format-' . $postFormat;


			/* Bulid article thumb column */
			$html .= '<div class="col-sm-4 col-md-3 post"><article id="post-' . $p->ID . '" class="' . $class . '">';


			$postMetaHtml = '<span class="meta">' . get_the_time( 'M d, Y', $p ) . ' </span>';


			switch ( $postFormat ) {
				case false: //false = post standard (image)
				case 'image':

					if ( $postFeaturedImage != '' ) {
						$html .= '
                    <div class="media-object">
                        <a href="' . get_permalink( $p->ID ) . '"><img src="' . $postFeaturedImage . '"></a>
                    </div>
                    ';
					}

					$html .= '
                    <div class="clearfix"></div>
                    <h4 class="uppercase"><a href="' . get_permalink( $p->ID ) . '">' . $p->post_title . '</a></h4>
                    ' . $postMetaHtml . '
                    <p>
                        ' . ct_get_excerpt_by_id( $p->ID ) . '

                        <a href="' . get_permalink( $p->ID ) . '">' . $read_more_button_text . '</a>
                    </p>
                    ';
					break;


				case 'video':

					$embed = get_post_meta( $p->ID, 'videoCode', true );
					if ( $embed == '' ) {
						$html .= '
                    <div class="media-object"><div class="video">';
						if ( ! empty( $embed ) ) {
							$html .= stripslashes( htmlspecialchars_decode( $embed ) );
						} else {
							$html .= ct_get_post_video( $p->ID, 870, 520 );
						}
						$html .= '</div></div>
                    ';
					}
					$html .= '
                    <div class="clearfix"></div>
                    <h4 class="uppercase"><a href="' . get_permalink( $p->ID ) . '">' . $p->post_title . '</a></h4>
                    ' . $postMetaHtml . '
                    <p>
                        ' . ct_get_excerpt_by_id( $p->id ) . '

                        <a href="' . get_permalink( $p->ID ) . '">' . $read_more_button_text . '</a>
                    </p>
                    ';
					break;


				case 'quote':
					$html .= '<div class="journalblock bg3">
                        <em>
                            ' . get_post_meta( $p->ID, 'quote', true ) . '
                        </em>
                    </div>';
					break;


				case 'link':


					$html .= '<div class="journalblock bg2">
            <a href="' . get_post_meta( $p->ID, 'link', true ) . '">' . $p->post_content . '</a></div>';
					break;


				case 'image':
					break;


				case 'gallery':


					$html .= '
                    <div class="media-object">';
					$html .= '[gallery_slider dircontrols="false" slideshow="false"  smooth="true" id="' . $p->ID . '" image_size="featured_image_masonry"]';
					$html .= '</div>
                    ';


					$html .= '
                    <div class="clearfix"></div>
                    <h4 class="uppercase"><a href="' . get_permalink( $p->ID ) . '">' . $p->post_title . '</a></h4>
                    ' . $postMetaHtml . '
                    <p>
                        ' . ct_get_excerpt_by_id( $p->id ) . '

                        <a href="' . get_permalink( $p->ID ) . '">' . $read_more_button_text . '</a>
                    </p>
                    ';

					break;


				case 'audio':

					$html .= '
                    <div class="media-object">';
					$embed = get_post_meta( $p->ID, 'videoCode', true );
					if ( ! empty( $embed ) ) {
						$html .= stripslashes( htmlspecialchars_decode( $embed ) );
					} else {
						$html .= ct_get_post_audio( $p->ID, '100%', 200 );
					}
					$html .= '</div>
                    ';

					$html .= '
                    <div class="clearfix"></div>
                    <h4 class="uppercase"><a href="' . get_permalink( $p->ID ) . '">' . $p->post_title . '</a></h4>
                    ' . $postMetaHtml . '
                    <p>
                        ' . ct_get_excerpt_by_id( $p->id ) . '

                        <a href="' . get_permalink( $p->ID ) . '">' . $read_more_button_text . '</a>
                    </p>
                    ';
					break;


				case 'aside':
					$html .= '<div class="journalblock bg2">
            <span>' . $p->post_content . '</span></div>';

					break;
			}

			$counter ++;

			$html .= '</article></div>';

		}

		$mainContainerAtts = array(
			'class' => array(
				'row',
				'with-isotope',
				$class
			),
		);


		/* Build title column */
		$blog = '<div' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>' . $html . '</div>';

		return do_shortcode( $blog );

	}

	/**
	 * Shortcode type
	 * @return string
	 */
	public function getShortcodeType() {
		return self::TYPE_SHORTCODE_SELF_CLOSING;
	}

	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		$atts = $this->getAttributesWithQuery( array(
			'read_more_button_text' => array(
				'label'   => __( 'Read More button text', 'ct_theme' ),
				'default' => 'Read More',
				'type'    => 'input'
			),
			'limit'                 => array(
				'label'   => __( 'limit', 'ct_theme' ),
				'default' => 20,
				'type'    => 'input'
			),
			'class'                 => array(
				'label'   => __( "Custom class", 'ct_theme' ),
				'default' => '',
				'type'    => 'input'
			)

		) );

		return $atts;
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-newspaper-o' ) );
	}
}

new ctBlogMasonryShortcode();
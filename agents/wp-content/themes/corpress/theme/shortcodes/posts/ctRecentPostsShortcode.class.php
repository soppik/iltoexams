<?php

/**
 * Recent posts
 * ver. 2.1
 */
class ctRecentPostsShortcode extends ctShortcodeQueryable implements ctVisualComposerShortcodeInterface
{

    /**
     * Returns name
     * @return string|void
     */
    public function getName()
    {
        return 'Recent posts';
    }

    /**
     * Shortcode name
     * @return string
     */
    public function getShortcodeName()
    {
        return 'recent_posts';
    }

    public function enqueueScripts()
    {

        wp_register_script('ct-flex-slider', CT_THEME_ASSETS . '/plugins/flexslider/jquery.flexslider-min.js', array('jquery'), false, true);
        wp_enqueue_script('ct-flex-slider');

        wp_register_script('ct-flex-easing', CT_THEME_ASSETS . '/js/jquery.easing.1.3.js', array('ct-flex-slider'), false, true);
        wp_enqueue_script('ct-flex-easing');

        wp_register_script('ct-flexslider_init', CT_THEME_ASSETS . '/js/flexslider_init.js', array('ct-flex-slider'), false, true);
        wp_enqueue_script('ct-flexslider_init');
    }


    /**
     * Handles shortcode
     *
     * @param $atts
     * @param null $content
     *
     * @return string
     */
    public function handle($atts, $content = null)
    {
        $attributes = shortcode_atts($this->extractShortcodeAttributes($atts), $atts);


        extract($attributes);
        $recentposts = $this->getCollection($attributes);


        $mainContainerAtts = array(
            'class' => array(
                'flexslider',
                $class,
                $this->getShortcodeName()
            ),
            'data-smoothheight' => 'true'
        );
        /* Build title column */
        $html = '<div ' . $this->buildContainerAttributes($mainContainerAtts, $atts) . '><ul class="slides">';

        $articles = array();

        foreach ($recentposts as $p) {

            $postFormat = (get_post_format($p->ID));


            if (post_password_required($p->ID)) {
                continue;
            }

            $title = $permalink = $content = '';
            switch ($postFormat) {

                case false: //false = post standard (image)
                    $title = $p->post_title;
                    $content = ct_get_excerpt_by_id($p->ID);
                    $permalink = get_permalink($p->ID);
                    break;

                case 'quote':
                    //$title = $p->post_title;
                    $title = '';
                    $content = get_post_meta($p->ID, 'quote', true);
                    break;

                case 'link':

                    $title = $p->post_title;

                    $permalink = get_post_meta($p->ID, 'link', true);
                    $content = ct_get_excerpt_by_id($p->ID);
                    break;

                default:

                    $title = $p->post_title;
                    $content = ct_get_excerpt_by_id($p->ID);
                    $permalink = get_permalink($p->ID);

            }


            $articles[] = array('post' => $p, 'title' => $title, 'content' => '<p>' . $content . '</p>', 'link' => $permalink);


        }

        $articleParts = array_chunk($articles, $columns);
        foreach ($articleParts as $chunk) {
            $html .= '<li>';
            $html .= '<div class="row">';
            foreach ($chunk as $article) {


                $post = $article['post'];

                $html .= '<div class="col-md-' . (12 / $columns) . '">';
                $html .= '<article>';

                $html .= '<div class="date">';
                $html .= '<span class="day">' . ct_get_post_day($post->ID) . '</span><span class="month">' . ct_get_post_short_month_name($post->ID) . '</span>';
                $html .= '</div>';

                $html .= ' <h4 class="uppercase"><a href="' . $article['link'] . '">' . $article['title'] . '</a></h4>
        				                ' . $article['content'] . '<a href="' . $article['link'] . '">' . $read_more . '</a>';
                $html .= '</article>';
                $html .= '</div>';
            }

            $html .= '</div>';
            $html .= '</li>';
        }
        $html .= '</ul></div>';

        return do_shortcode($html);
    }


    /**
     * Shortcode type
     * @return string
     */
    public function getShortcodeType()
    {
        return self::TYPE_SHORTCODE_SELF_CLOSING;
    }

    /**
     * Returns config
     * @return null
     */
    public function getAttributes()
    {
        $atts = $this->getAttributesWithQuery(array(
            'title' => array('label' => __('Widget title', 'ct_theme'), 'default' => '', 'type' => 'input'),
            'widgetmode' => array('default' => 'false', 'type' => false),
            'limit' => array('label' => __('limit', 'ct_theme'), 'default' => 10, 'type' => 'input'),
            'columns' => array(
                'label' => __("Number of columns", 'ct_theme'),
                'type' => 'select',
                'choices' => array(1 => 1, 2 => 2, 3 => 3, 4 => 4),
                'default' => 2
            ),
            'read_more' => array(
                'label' => __('Read More label', 'ct_theme'),
                'default' => __('Read More', 'ct_theme')
            ),
            'class' => array('label' => __("Custom class", 'ct_theme'), 'default' => '', 'type' => 'input')
        ));

        return $atts;
    }

    /**
     * Returns additional info about VC
     * @return ctVisualComposerInfo
     */
    public function getVisualComposerInfo()
    {
        return new ctVisualComposerInfo($this, array('icon' => 'fa-file-text-o'));
    }
}

new ctRecentPostsShortcode();
<?php

/**
 * Pricelist shortcode
 */
class ctServiceBoxShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Service box';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'service_box';
	}

	/**
	 * Returns shortcode type
	 * @return mixed|string
	 */

	public function getShortcodeType() {
		return self::TYPE_SHORTCODE_ENCLOSING;
	}


	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );
		$mainContainerAtts = array(
			'class' => array( 'service-box', $class )
		);

		$html = '
            <div' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>
            <i class="fa ' . $icon_name . '"></i>
            <span class="h4 uppercase">' . $header . '</span>
            </div>';

		return do_shortcode( $html );
	}


	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'header'    => array( 'label' => __( 'Header', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'icon_name' => array(
				'label'   => __( 'Icon Name', 'ct_theme' ),
				'type'    => "icon",
				'default' => '',
				'link'    => CT_THEME_ASSETS . '/shortcode/awesome/index.html'
			),
			'class'     => array(
				'label'   => __( "Custom class", 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( 'Adding custom class allows you to set diverse styles in css to the element. Type in name of class, which you defined in css. You can add as much classes as you like.', 'ct_theme' )
			),
		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-caret-square-o-up' ) );
	}
}

new ctServiceBoxShortcode();




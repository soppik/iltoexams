<?php

/**
 * Pricelist shortcode
 */
class ctStrongPointsShortcode extends ctShortcode
{

    /**
     * Returns name
     * @return string|void
     */
    public function getName()
    {
        return 'Strong Points';
    }

    /**
     * Shortcode name
     * @return string
     */
    public function getShortcodeName()
    {
        return 'strong_points';
    }

    /**
     * Returns shortcode type
     * @return mixed|string
     */

    public function getShortcodeType()
    {
        return self::TYPE_SHORTCODE_ENCLOSING;
    }


    /**
     * Handles shortcode
     * @param $atts
     * @param null $content
     * @return string
     */

    public function handle($atts, $content = null)
    {
        extract(shortcode_atts($this->extractShortcodeAttributes($atts), $atts));


        $mainContainerAtts = array(
            'class' => array(
                'row',
                $class),
        );


        if ($animated == 'true') {
            $animClass = 'square-icon-hover';
        } else {
            $animClass = '';
        }


        $html = '

        <div ' . $this->buildContainerAttributes($mainContainerAtts, $atts) . '>
            <div class="col-md-4">
                <ul class="media-list">
                    <li class="media feature-right-bottom square-icon-hover ' . $animClass . '" data-fx="fadeInDown">
                        <div class="pull-right text-center">
                            <div class="square-icon-box square-icon-small">
                                <i class="fa ' . $icon1_name . '"></i>
                            </div>
                        </div>
                        <div class="media-body text-right">
                            <h4 class="media-heading uppercase">' . $header1 . '</h4>
                            <p>' . $description1 . '</p>
                        </div>
                    </li>
                    <li class="media feature-right square-icon-hover ' . $animClass . '" data-fx="fadeInLeft">
                        <div class="pull-right text-center">
                            <div class="square-icon-box square-icon-small">
                                <i class="fa ' . $icon2_name . '"></i>
                            </div>
                        </div>
                        <div class="media-body text-right">
                            <h4 class="media-heading uppercase">' . $header2 . '</h4>
                            <p>' . $description2 . '</p>
                        </div>
                    </li>
                    <li class="media feature-right-top square-icon-hover ' . $animClass . '" data-fx="fadeInUp">
                        <div class="pull-right text-center">
                            <div class="square-icon-box square-icon-small">
                                <i class="fa ' . $icon3_name . '"></i>
                            </div>
                        </div>
                        <div class="media-body text-right">
                            <h4 class="media-heading uppercase">' . $header3 . '</h4>
                            <p>' . $description3 . '</p>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-md-4 text-center">
                <img src="' . $image . '" alt="Our Strong Points Ipad"/>
            </div>
            <div class="col-md-4">
                <ul class="media-list">
                    <li class="media feature-left-bottom square-icon-hover ' . $animClass . '" data-fx="fadeInDown">
                        <div class="pull-left text-center">
                            <div class="square-icon-box square-icon-small">
                                <i class="fa ' . $icon4_name . '"></i>
                            </div>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading uppercase">' . $header4 . '</h4>
                            <p>' . $description4 . '</p>
                        </div>
                    </li>
                    <li class="media feature-left square-icon-hover ' . $animClass . '" data-fx="fadeInRight">
                        <div class="pull-left text-center">
                            <div class="square-icon-box square-icon-small">
                                <i class="fa ' . $icon5_name . '"></i>
                            </div>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading uppercase">' . $header5 . '</h4>
                            <p>' . $description5 . '</p>
                        </div>
                    </li>
                    <li class="media feature-left-top square-icon-hover ' . $animClass . '" data-fx="fadeInUp">
                        <div class="pull-left text-center">
                            <div class="square-icon-box square-icon-small">
                                <i class="fa ' . $icon6_name . '"></i>
                            </div>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading uppercase">' . $header6 . '</h4>
                            <p>' . $description6 . '</p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>


        ';

        return do_shortcode($html);
    }


    /**
     * Returns config
     * @return null
     */
    public function getAttributes()
    {
        return array(

            'animated' => array('label' => __('Icon Animation on hover?', 'ct_theme'), 'default' => 'no', 'type' => 'select', 'choices' => array('yes' => __('yes', 'ct_theme'), 'no' => __('no', 'ct_theme')),),
            'image' => array('label' => __("Image", 'ct_theme'), 'default' => '', 'type' => 'image', 'help' => __("Image source", 'ct_theme')),
            'header1' => array('label' => __('Header 1', 'ct_theme'), 'default' => '', 'type' => 'input', 'help' => __('Numerical order from top to bottom: Header 1 1sr from the top left hand side, Header 2 - 2nd from the top left hand side', 'ct_theme')),
            'header2' => array('label' => __('Header 2', 'ct_theme'), 'default' => '', 'type' => 'input'),
            'header3' => array('label' => __('Header 3', 'ct_theme'), 'default' => '', 'type' => 'input'),
            'header4' => array('label' => __('Header 4', 'ct_theme'), 'default' => '', 'type' => 'input'),
            'header5' => array('label' => __('Header 5', 'ct_theme'), 'default' => '', 'type' => 'input'),
            'header6' => array('label' => __('Header 6', 'ct_theme'), 'default' => '', 'type' => 'input'),
            'description1' => array('label' => __('Description 1', 'ct_theme'), 'default' => '', 'type' => 'input'),
            'description2' => array('label' => __('Description 2', 'ct_theme'), 'default' => '', 'type' => 'input'),
            'description3' => array('label' => __('Description 3', 'ct_theme'), 'default' => '', 'type' => 'input'),
            'description4' => array('label' => __('Description 4', 'ct_theme'), 'default' => '', 'type' => 'input'),
            'description5' => array('label' => __('Description 5', 'ct_theme'), 'default' => '', 'type' => 'input'),
            'description6' => array('label' => __('Description 6', 'ct_theme'), 'default' => '', 'type' => 'input'),
            'icon1_name' => array('label' => __('Icon 1', 'ct_theme'), 'type' => "icon", 'default' => '', 'link' => CT_THEME_ASSETS . '/shortcode/awesome/index.html'),
            'icon2_name' => array('label' => __('Icon 2', 'ct_theme'), 'type' => "icon", 'default' => '', 'link' => CT_THEME_ASSETS . '/shortcode/awesome/index.html'),
            'icon3_name' => array('label' => __('Icon 3', 'ct_theme'), 'type' => "icon", 'default' => '', 'link' => CT_THEME_ASSETS . '/shortcode/awesome/index.html'),
            'icon4_name' => array('label' => __('Icon 4', 'ct_theme'), 'type' => "icon", 'default' => '', 'link' => CT_THEME_ASSETS . '/shortcode/awesome/index.html'),
            'icon5_name' => array('label' => __('Icon 5', 'ct_theme'), 'type' => "icon", 'default' => '', 'link' => CT_THEME_ASSETS . '/shortcode/awesome/index.html'),
            'icon6_name' => array('label' => __('Icon 6', 'ct_theme'), 'type' => "icon", 'default' => '', 'link' => CT_THEME_ASSETS . '/shortcode/awesome/index.html'),
            'class' => array('label' => __("Custom class", 'ct_theme'), 'default' => '', 'type' => 'input', 'help' => __('Adding custom class allows you to set diverse styles in css to the element. Type in name of class, which you defined in css. You can add as much classes as you like.', 'ct_theme')),

        );

    }
}

new ctStrongPointsShortcode();




<?php

/**
 * Pricelist shortcode
 */
class ctIconBoxShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface
{

    /**
     * Returns name
     * @return string|void
     */
    public function getName()
    {
        return 'Icon box';
    }

    /**
     * Shortcode name
     * @return string
     */
    public function getShortcodeName()
    {
        return 'icon_box';
    }

    /**
     * Returns shortcode type
     * @return mixed|string
     */

    public function getShortcodeType()
    {
        return self::TYPE_SHORTCODE_ENCLOSING;
    }


    /**
     * Handles shortcode
     *
     * @param $atts
     * @param null $content
     *
     * @return string
     */

    public function handle($atts, $content = null)
    {
        extract(shortcode_atts($this->extractShortcodeAttributes($atts), $atts));


        if ($animated == 'true' || $animated == 'yes') {
            $animClass = 'square-icon-hover';
        } else {
            $animClass = '';
        }


        $mainContainerAtts = array(
            'class' => array(
                $animClass,
                $align != 'top' ? 'media' : 'text-center'
            ),
            'data-toggle' => 'tooltip',
            'data-placement' => 'bottom',
            'title' => $tooltip,
        );


        if ($align == 'top') {
            $html = '
            <div ' . $this->buildContainerAttributes($mainContainerAtts, $atts) . '>
            <div class="square-icon-box">
                <i class="fa ' . $icon_name . '"></i>
            </div>
            ' . (($link != '') ? '<a href="' . $link . '">' : '') . '
            <h4 class="uppercase hr-mid">' . $header . '</h4>' . (($link != '') ? '</a>' : '') . '
            <p>' . $content . '</p>
            </div>';
        } elseif ($align == 'left') {
            $mainContainerAtts['class'][] = 'activate';
            //$mainContainerAtts['class'][] = 'pull-left';
            $html = '<div ' . $this->buildContainerAttributes($mainContainerAtts, $atts) . '">
        <div class="pull-left text-center">
        <div class="square-icon-box square-icon-small">
        <i class="fa ' . $icon_name . '"></i>
        </div>
        </div>
        <div class="media-body">
        <h4 class="media-heading uppercase">' . $header . '</h4>
        <p>' . $content . '</p></div></div>';

        } else {
            $mainContainerAtts['class'][] = 'activate';
            $mainContainerAtts['class'][] = 'pull-left';
            $html = '<div ' . $this->buildContainerAttributes($mainContainerAtts, $atts) . '">
        <div class="pull-right text-center">
        <div class="square-icon-box square-icon-small">
        <i class="fa ' . $icon_name . '"></i>
        </div>
        </div>
        <div class="media-body text-right">
        <h4 class="media-heading uppercase">' . $header . '</h4>
        <p>' . $content . '</p></div></div>';
        }

        return do_shortcode($html);
    }


    /**
     * Returns config
     * @return null
     */
    public function getAttributes()
    {
        return array(
            'animated' => array(
                'label' => __('Icon Animation on hover?', 'ct_theme'),
                'default' => 'no',
                'type' => 'select',
                'choices' => array('yes' => __('yes', 'ct_theme'), 'no' => __('no', 'ct_theme')),
            ),
            'align' => array(
                'label' => __('Align', 'ct_theme'),
                'default' => 'top',
                'type' => 'select',
                'options' => array(
                    'left' => __('left', 'ct_theme'),
                    'top' => __('top', 'ct_theme'),
                    'right' => __('right', 'ct_theme'),
                )
            ),
            'header' => array('label' => __('Title', 'ct_theme'), 'default' => '', 'type' => 'input'),
            'content' => array('label' => __('Description', 'ct_theme'), 'default' => '', 'type' => 'textarea'),
            'icon_name' => array('label' => __('Icon', 'ct_theme'),
                'type' => "icon",
                'default' => '',
                'link' => CT_THEME_ASSETS . '/shortcode/awesome/index.html'
            ),
            'link' => array('label' => __('Link', 'ct_theme'), 'default' => '', 'type' => 'input'),
            'tooltip' => array('label' => __('Tooltip text', 'ct_theme'), 'default' => '', 'type' => 'input'),
            'class' => array('label' => __("Custom class", 'ct_theme'),
                'default' => '',
                'type' => 'input',
                'help' => __('Adding custom class allows you to set diverse styles in css to the element. Type in name of class, which you defined in css. You can add as much classes as you like.', 'ct_theme')
            ),

        );

    }

	/**
		 * Returns additional info about VC
		 * @return ctVisualComposerInfo
		 */
		public function getVisualComposerInfo() {
			return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-caret-square-o-up' ) );
		}
}

new ctIconBoxShortcode();




<?php

/**
 * Pricelist shortcode
 */
class ctCopyrightShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Copyright';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'copyright';
	}

	/**
	 * Returns shortcode type
	 * @return mixed|string
	 */

	public function getShortcodeType() {
		return self::TYPE_SHORTCODE_ENCLOSING;
	}


	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );

		$mainContainerAtts = array(
			'class' => array(
				$class,
			),
		);

		if ( $text == '' ) {
			$html = '<p>' . strtr( ct_get_option( 'general_footer_text', '' ), array(
						'%year%' => date( 'Y' ),
						'%name%' => get_bloginfo( 'name', 'display' )
					) ) . '</p>';
		} else {
			$html = '<p>' . strtr( $text, array(
						'%year%' => date( 'Y' ),
						'%name%' => get_bloginfo( 'name', 'display' )
					) ) . '</p>';
		}

		return do_shortcode( $html );
	}


	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'text'  => array(
				'label'   => __( 'Text', 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( 'Leave empty, if you want to use General Footer field in Theme Options', 'ct_theme' )
			),
			'class' => array(
				'label'   => __( "Custom class VvPLOcKER.COM", 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( 'Adding custom class allows you to set diverse styles in css to the element. Type in name of class, which you defined in css. You can add as much classes as you like.', 'ct_theme' )
			),
		);

	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-copyright' ) );
	}
}

new ctCopyrightShortcode();




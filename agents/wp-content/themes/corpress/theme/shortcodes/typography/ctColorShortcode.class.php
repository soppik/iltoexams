<?php

/**
 * Paragraph shortcode
 */
class ctColorShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Color';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'color';
	}

	/**
	 * Returns shortcode type
	 * @return mixed|string
	 */

	public function getShortcodeType() {
		return self::TYPE_SHORTCODE_ENCLOSING;
	}

	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );

		$mainContainerAtts = array(
			'class'      => array(
				$class
			),
			'data-color' => $color
		);

		return do_shortcode( '<span ' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>' . $content . '</span>' );
	}

	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'color'   => array( 'label' => __( 'Color', 'ct_theme' ), 'default' => '', 'type' => 'colorpicker' ),
			'content' => array( 'label' => __( 'Content', 'ct_theme' ), 'default' => '', 'type' => "textarea" ),
			'class'   => array( 'label' => __( 'Custom class', 'ct_theme' ), 'default' => '', 'type' => 'input' ),

		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-paint-brush' ) );
	}
}

new ctColorShortcode();
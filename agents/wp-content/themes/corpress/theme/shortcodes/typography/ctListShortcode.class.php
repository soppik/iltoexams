<?php

/**
 * List shortcode
 */
class ctListShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'List';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'list';
	}

	/**
	 * Returns shortcode type
	 * @return mixed|string
	 */
	public function getShortcodeType() {
		return self::TYPE_SHORTCODE_ENCLOSING;
	}


	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */
	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );

		$mainContainerAtts = array(
			'class' => array(
				'list-unstyled',
				$class
			),
		);


		$items = do_shortcode( $content );
		$html  = '<ul ' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>' . $items . '</ul>';

		return do_shortcode( $html );
	}

	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'type'    => array(
				'label'   => __( 'Type', 'ct_theme' ),
				'default' => 'unstyled',
				'type'    => 'select',
				'options' => array(
					'styled'   => 'styled',
					'unstyled' => 'unstyled'
				)
			),
			'content' => array(
				'label'   => __( 'Content', 'ct_theme' ),
				'default' => '',
				'type'    => "textarea",
				'help'    => __( 'li items', 'ct_theme' )
			),
			'class'   => array(
				'label'   => __( 'Custom class', 'ct_theme' ),
				'type'    => 'input',
				'default' => '',
				'help'    => "Set custom class"
			),
		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-list' ) );
	}
}

new ctListShortcode();
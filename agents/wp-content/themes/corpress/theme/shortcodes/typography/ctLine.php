<?php

/**
 * Price tag shortcode
 */
class ctLine extends ctShortcode implements ctVisualComposerShortcodeInterface {


	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Separator';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'line';
	}

	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );

		$mainContainerAtts = array(
			'class' => array(
				$class
			)
		);

		$html = '<hr ' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>';

		return do_shortcode( $html );
	}

	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'class' => array(
				'label'   => __( "Custom class", 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( "Custom class name", 'ct_theme' )
			),
		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array('icon'=>'fa-arrows-v') );
	}
}

new ctLine();
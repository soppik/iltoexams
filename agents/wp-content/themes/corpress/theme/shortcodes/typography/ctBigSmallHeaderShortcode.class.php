<?php

/**
 * Big Small Header
 */
class ctBigSmallHeaderShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Box Header';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'box_header';
	}


	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );

		$mainContainerAtts = array(
			'class' => array(
				'aside-title',
				'uppercase',
				$class
			),
		);


		return '<span' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>'
		       . $header . '<small data-color="' . $sub_header_color . '">' . $sub_header . '</small>
		       </span>
               ' . $footer;
	}


	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'header'           => array( 'label' => __( 'Header', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'sub_header'       => array( 'label' => __( 'Subheader', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'footer'           => array( 'label' => __( 'Footer', 'ct_theme' ), 'default' => '', 'type' => 'input' ),
			'sub_header_color' => array(
				'label'   => __( 'Subheader color', 'ct_theme' ),
				'type'    => "colorpicker",
				'default' => '#d30000'
			),
			'class'            => array(
				'label'   => __( "Custom class", 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( "Custom class name", 'ct_theme' )
			),
		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-header' ) );
	}
}

new ctBigSmallHeaderShortcode();





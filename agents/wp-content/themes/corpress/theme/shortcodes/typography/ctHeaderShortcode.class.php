<?php

/**
 * Header shortcode
 */
class ctHeader extends ctShortcode implements ctVisualComposerShortcodeInterface {


	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Header';
	}


	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'header';
	}

	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		$style = '';
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );

		if ( $align == 'center' ) {
			$headerAlignClass = 'text-center ' . ( ( $line == 'true' || $line == 'yes' ) ? 'hr-mid' : '' );
		} elseif ( ( $align == 'left' ) ) {
			$headerAlignClass = 'text-left ' . ( ( $line == 'true' || $line == 'yes' ) ? 'hr-left' : '' );
		} else {
			$headerAlignClass = 'text-right ' . ( ( $line == 'true' || $line == 'yes' ) ? 'hr-right' : '' );
		}

		$mainContainerAtts = array(
			'class' => array(
				$class,
				$headerAlignClass,
				$light == 'true' || $light == 'yes' ? 'weight300' : '',
				$uppercase == 'true' || $uppercase == 'yes' ? 'uppercase' : '',
				$gloria == 'true' || $gloria == 'yes' ? 'gloria' : '',
				$stripes == 'true' || $stripes == 'yes' ? 'stripes' : '',

			),
		);

		$html = '<h' . $level . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>' . do_shortcode( $content ) . '</h' . $level . '>';

		return do_shortcode( $html );
	}

	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'level'     => array(
				'label'   => __( 'Header size (1-6)', 'ct_theme' ),
				'default' => '3',
				'type'    => 'select',
				'options' => array(
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6',
				)
			),
			'align'     => array(
				'label'   => __( 'Alignment', 'ct_theme' ),
				'default' => 'left',
				'type'    => 'select',
				'options' => array(
					'left'   => 'left',
					'center' => 'center',
					'right'  => 'right',
				)
			),
			'gloria'    => array(
				'label'   => __( 'Cursive?', 'ct_theme' ),
				'default' => 'no',
				'type'    => 'select',
				'choices' => array( 'yes' => __( 'yes', 'ct_theme' ), 'no' => __( 'no', 'ct_theme' ) ),
			),
			'light'     => array(
				'label'   => __( 'Light header?', 'ct_theme' ),
				'default' => 'no',
				'type'    => 'select',
				'choices' => array( 'yes' => __( 'yes', 'ct_theme' ), 'no' => __( 'no', 'ct_theme' ) ),
			),
			'line'      => array(
				'label'   => __( 'Underline?', 'ct_theme' ),
				'default' => 'no',
				'type'    => 'select',
				'choices' => array( 'yes' => __( 'yes', 'ct_theme' ), 'no' => __( 'no', 'ct_theme' ) ),
			),
			'stripes'   => array(
				'label'   => __( 'Heading With Stripes ?', 'ct_theme' ),
				'default' => 'no',
				'type'    => 'select',
				'choices' => array( 'yes' => __( 'yes', 'ct_theme' ), 'no' => __( 'no', 'ct_theme' ) ),
			),
			'uppercase' => array(
				'label'   => __( 'Uppercase?', 'ct_theme' ),
				'default' => 'no',
				'type'    => 'select',
				'choices' => array(
					'yes' => __( 'yes', 'ct_theme' ),
					'no'  => __( 'no', 'ct_theme' )
				),
			),
			'content'   => array( 'label' => __( 'Text', 'ct_theme' ), 'default' => '', 'type' => "textarea" ),
			'class'     => array(
				'label'   => __( "Custom class", 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( "Custom class, adding custom class allows you to set diverse styles in css to the element. type in name of class, which you defined in css. you can add as much classes as you like.", 'ct_theme' )
			),
		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-header' ) );
	}
}

new ctHeader();
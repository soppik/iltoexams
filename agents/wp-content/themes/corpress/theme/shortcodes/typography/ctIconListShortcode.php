<?php

/**
 * List shortcode
 */
class ctIconListShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Icon list';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'icon_list';
	}

	/**
	 * Returns shortcode type
	 * @return mixed|string
	 */
	public function getShortcodeType() {
		return self::TYPE_SHORTCODE_ENCLOSING;
	}


	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */
	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );

		$mainContainerAtts = array(
			'class' => array(
				'icon-list',
				'list-unstyled',
				$class
			),
		);

		return '<div' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>' . $this->doShortcode( $content ) . '</div>';
	}

	/**
	 * @return array|string
	 */

	public function getChildShortcodeInfo() {
		return array( 'name' => 'icon_list_item', 'min' => 1, 'max' => 100, 'default_qty' => 3 );
	}


	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'class' => array(
				'label'   => __( 'Custom class', 'ct_theme' ),
				'type'    => 'input',
				'default' => '',
				'help'    => "Set custom class"
			),
		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-list' ) );
	}
}

new ctIconListShortcode();

//#28146
if(class_exists('WPBakeryShortCodesContainer')){
	class WPBakeryShortcode_icon_list extends WPBakeryShortCodesContainer{}
}
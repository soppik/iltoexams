<?php

/**
 * List item shortcode
 */
class ctIconListItemShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Icon list item';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'icon_list_item';
	}

	/**
	 * Returns shortcode type
	 * @return mixed|string
	 */
	public function getShortcodeType() {
		return self::TYPE_SHORTCODE_ENCLOSING;
	}


	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */
	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );

		$mainContainerAtts = array(
			'class' => array(
				$class
			),
		);

		$icon = $this->embedShortcode( 'icon', array(
			'name'  => $icon,
			'class' => 'fa-lg'
		) );

		return $this->doShortcode( '<li' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>
                    <div class="display-table">
                    <span class="table-cell">' . $icon . '</span>
                    <span class="table-cell">
                        ' . $content . '
                    </span>
                    </div>
                </li>' );
	}

	/**
	 * @return array|string
	 */

	public function getParentShortcodeName() {
		return 'icon_list';
	}


	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'icon'    => array(
				'label'   => __( 'Icon', 'ct_theme' ),
				'type'    => "icon",
				'default' => '',
				'link'    => CT_THEME_ASSETS . '/shortcode/awesome/index.html'
			),
			'content' => array( 'label' => __( 'Text', 'ct_theme' ), 'default' => '', 'type' => "textarea" ),
			'class'   => array(
				'label'   => __( 'Custom class', 'ct_theme' ),
				'type'    => 'input',
				'default' => '',
				'help'    => "Set custom class"
			),
		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-list' ) );
	}
}

new ctIconListItemShortcode();
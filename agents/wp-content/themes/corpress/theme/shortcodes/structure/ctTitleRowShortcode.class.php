<?php

/**
 * Title row shortcode
 */
class ctTitleRowShortcode extends ctShortcode {



	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Title row';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'title_row';
	}

	/**
	 *
	 */

	public function enqueueScripts() {
		wp_register_script( 'stellar', CT_THEME_ASSETS . '/js/jquery.stellar.min.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'stellar' );


		wp_register_script( 'fitvids', CT_THEME_ASSETS . '/js/jquery.fitvids.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'fitvids' );

		wp_register_script( 'jplayer', CT_THEME_ASSETS . '/js/jquery.jplayer.min.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'jplayer' );

		wp_register_script( 'section-call', CT_THEME_ASSETS . '/js/jscalls/sectioncall.js', array( 'stellar' ), false, true );
		wp_enqueue_script( 'section-call' );

	}


	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );

		if ( $header_type == 'parallax_centered'
		     || $header_type == 'parallax_centered'
		     || $header_type == 'video_centered'
		     || $header_type == 'ken_burns_centered'
		) {
			$textCenter = 'text-center';
		} else {
			$textCenter = '';
		}


		if ( $nav == 'yes' || $nav == 'true' ) {
			$navHtml = '
            <div class="navigation-title ' . $textCenter . '">
            <a ' . ( ( $nav_prev_url != '' ) ? 'href="' . $nav_prev_url . '"' : '' ) . '><i class="fa fa-chevron-left"></i></a>
            <a ' . ( ( $nav_index_url != '' ) ? 'href="' . $nav_index_url . '"' : '' ) . '><i class="fa fa-th-large"></i></a>
            <a ' . ( ( $nav_next_url != '' ) ? 'href="' . $nav_next_url . '"' : '' ) . '><i class="fa fa-chevron-right"></i></a>
            </div>';
		} else {
			$navHtml = '';
		}



		if ( $use_theme_options == 'true' || $use_theme_options == 'yes' ) {


            //is blog?
            if (get_option('page_for_posts') == get_queried_object_id() && !is_single()) {
                if (ct_get_option('posts_index_show_breadcrumbs','1')){
                    $breadcrumbs = 'yes';
                }else{
                    $breadcrumbs = false;
                }
            }


			$headerType    = ct_get_option( $context . '_header_type', 'parallax' );
			$headerType    = empty( $headerType ) ? '' : $headerType;
			$height        = ct_get_option( $context . '_header_height', 220 );
			$height        = str_replace( 'px', '', $height );
			$parallaxRatio = ct_get_option( $context . '_header_parallax_ratio', 0.5 );
			$sectionClass  = '';
			$html5Atts     = '';

            if (
                $headerType=='parallax_centered'
                || $headerType=='video_centered'
                || $headerType=='ken_burns_centered') {
                $breadcrumbsAlign = '';
            }else{
                $breadcrumbsAlign ='pull-right';
            }
            if (($breadcrumbs =='yes' || $breadcrumbs =='1')  && class_exists('ctBreadcrumbs')) {
                new ctBreadcrumbs();
                $breadcrumbsHtml = apply_filters('ct.breadcrumbs.display','',
                    array('wrapper_class'=>'breadcrumb '.$breadcrumbsAlign.' '.$breadcrumbs_class)
                );
            }else{
                $breadcrumbsHtml = '';
            }

			$simple_header_button      = ct_get_option( $context . '_simple_header_button', '' );
			$simple_header_button_text = ct_get_option( $context . '_simple_header_button_text', '' );
			$simple_header_button_link = ct_get_option( $context . '_simple_header_button_link', '' );


			if ( ! is_numeric( $parallaxRatio ) ) {
				$parallaxRatio = 0.5;
			}


			if ( ct_get_option( $context . '_header_image' ) != '' ) {
				$imageAttr = 'data-image="' . ct_get_option( $context . '_header_image', '' ) . '"';
				$imageTag  = '<img src="' . ct_get_option( $context . '_header_image', '' ) . '">';
			} else {
				$imageAttr = '';
				$imageTag  = '';
			}

			if ( ct_get_option( $context . '_header_image_mobile' ) != '' ) {
				$imageMobileAttr = 'data-image-mobile="' . ct_get_option( $context . '_header_image_mobile', '' ) . '"';
			} else {
				$imageMobileAttr = '';
			}

			if ( ct_get_option( $context . '_header_image_video_fallback' ) != '' ) {
				$imageFallbackAttr = 'data-fallback-image="' . ct_get_option( $context . '_header_image_video_fallback', '' ) . '"';
			} else {
				$imageFallbackAttr = '';
			}

			$video_type = ct_get_option( $context . '_header_video_type', '' );
			switch ( $video_type ) {
				default:
					$videoCode    = '';
					$sectionClass = '';
					break;


				case'mp4':
					$videoCode = ct_get_option( $context . '_header_video_source', '' );
					if ( empty( $videoCode ) ) {
						$videoCode    = '';
						$sectionClass = '';
						$html5Atts    = '';
					} else {
						$videoCode    = ' <video id="' . $id . '" muted="" loop="" autoplay="autoplay" preload="auto" width="100%" height="100%"><source src="' . $videoCode . '" type="video/mp4" /></video>';
						$sectionClass = 'html5';
						$html5Atts    = 'muted loop autoplay="autoplay" preload="auto" width="100%" height="100%"';
					}

					break;

				case'webm':
					$videoCode = ct_get_option( $context . '_header_video_source', '' );
					if ( empty( $videoCode ) ) {
						$videoCode    = '';
						$sectionClass = '';
						$html5Atts    = '';
					} else {
						$videoCode    = ' <video id="' . $id . '" muted="" loop="" autoplay="autoplay" preload="auto" width="100%" height="100%"><source src="' . $videoCode . '" type="video/webm" /></video>';
						$sectionClass = 'html5';
						$html5Atts    = 'muted loop autoplay="autoplay" preload="auto" width="100%" height="100%"';
					}

					break;

			}


			switch ( $headerType ) {
				default:
				case 'parallax':
					$html = '<section class="media-section darkbg" data-stellar-background-ratio="' . $parallaxRatio . '" data-height="' . $height . '" data-type="parallax" ' . $imageAttr . ' ' . $imageMobileAttr . '>
                <div class="inner">
                <div class="container">
                <div class="display-table">
                <div class="table-cell">
                    <h2 class="uppercase no-margin">' . $header . '</h2>
                </div>
                ' . $navHtml . '
                <div class="table-cell">
                ' . $breadcrumbsHtml . '
                </div>
                </div>
                </div>
                </div>
                </section>';

					break;

				case 'parallax_centered':
					$html = '<section class="media-section darkbg" data-stellar-background-ratio="' . $parallaxRatio . '" data-height="' . $height . '" data-type="parallax" ' . $imageAttr . ' ' . $imageMobileAttr . '>
                <div class="inner">
                <div class="text-center">
                <h2 class="uppercase no-margin">' . $header . '</h2>
                '.$breadcrumbsHtml.'
                </div>
                ' . $navHtml . '
                </div>
                </section>';
					break;

				case 'video':
					$html = '<section class="media-section darkbg ' . $sectionClass . '" data-height="' . $height . '" data-type="video" ' . $imageFallbackAttr . '>
                <div id="video1" class="video" ' . $html5Atts . '>
                ' . $videoCode . '
                </div>
                <div class="display-table">
                <div class="inner bg5">
                <div class="container">
                <div class="display-table">
                <div class="table-cell">
                <h2 class="uppercase no-margin">' . $header . '</h2>
                </div>
                ' . $navHtml . '
                <div class="table-cell">
                ' . $breadcrumbsHtml . '
                </div>
                </div>
                </div>
                </div>
                </div>
            </section>';
					break;

				case 'video_centered':
					$html = '<section class="media-section darkbg ' . $sectionClass . '" data-height="' . $height . '" data-type="video" ' . $imageFallbackAttr . '>
                <div id="video1" class="video" ' . $html5Atts . '>
                ' . $videoCode . '
                </div>
                <div class="display-table">
                <div class="inner bg5">
                <div class="text-center">
                <h2 class="uppercase no-margin">' . $header . '</h2>
                '.$breadcrumbsHtml.'
                </div>
                ' . $navHtml . '
                </div>
                </div>
                </section>';
					break;

				case 'ken_burns':
					$html = '<section class="media-section darkbg" data-height="' . $height . '" data-type="kenburns">
                <div class="media-section-image-container">
                ' . $imageTag . '
                </div>

                <div class="inner">
                <div class="container">
                <div class="display-table">
                <div class="table-cell">
                    <h2 class="uppercase no-margin">' . $header . '</h2>
                </div>
                ' . $navHtml . '
                <div class="table-cell">
               ' . $breadcrumbsHtml . '
                </div>
                </div>
                </div>
                </div>
                </section>';
					break;

				case 'ken_burns_centered':
					$html = '<section class="media-section darkbg" data-height="' . $height . '" data-type="kenburns">
                <div class="media-section-image-container">
                ' . $imageTag . '
                </div>
                <div class="inner">
                <div class="text-center">
                <h2 class="uppercase no-margin">' . $header . '</h2>
                '.$breadcrumbsHtml.'
                </div>
                ' . $navHtml . '
                </div>
                </section>';
					break;

				case 'simple_header':
					$html = '
            <section class="bg2 header-page">
            <div class="container">
            <div class="display-table">
            <div class="table-cell">
            ' . ( $simple_header_button ? '<a class="btn btn-border" href="' . $simple_header_button_link . '">' . $simple_header_button_text . '</a>' : '<h2 class="uppercase">' . $header . '</h2>' ) . '

            </div>
            <div class="table-cell text-right">
            ' . $breadcrumbsHtml . '
            </div>
            </div>
            </div>
            </section>
                ';
					break;
			}


			//use parameters


		} else {

            if (
                $header_type=='parallax_centered'
                || $header_type=='video_centered'
                || $header_type=='ken_burns_centered') {
                $breadcrumbsAlign = '';
            }else{
                $breadcrumbsAlign ='pull-right';
            }
            if (($breadcrumbs =='yes' || $breadcrumbs =='1')  && class_exists('ctBreadcrumbs')) {
                new ctBreadcrumbs();
                $breadcrumbsHtml = apply_filters('ct.breadcrumbs.display','',
                    array('wrapper_class'=>'breadcrumb '.$breadcrumbsAlign.' '.$breadcrumbs_class)
                );
            }else{
                $breadcrumbsHtml = '';
            }



			$height = $header_height;


			$height        = str_replace( 'px', '', $height );
			$parallaxRatio = $header_parallax_ratio;
			$sectionClass  = '';
			$html5Atts     = '';

			if ( ! is_numeric( $parallaxRatio ) ) {
				$parallaxRatio = 0.5;
			}


			if ( $header_image != '' ) {
				$imageAttr = 'data-image="' . $header_image . '"';
				$imageTag  = '<img src="' . $header_image . '">';
			} else {
				$imageAttr = '';
				$imageTag  = '';
			}

			if ( $header_image_mobile != '' ) {
				$imageMobileAttr = 'data-image-mobile="' . $header_image_mobile . '"';
			} else {
				$imageMobileAttr = '';
			}

			if ( $header_image_video_fallback != '' ) {
				$imageFallbackAttr = 'data-fallback-image="' . $header_image_video_fallback . '"';
			} else {
				$imageFallbackAttr = '';
			}


			switch ( $video_type ) {
				default:
					$videoCode    = '';
					$sectionClass = '';
					break;


				case'mp4':
					$videoCode = $header_video_source;
					if ( empty( $videoCode ) ) {
						$videoCode    = '';
						$sectionClass = '';
						$html5Atts    = '';
					} else {
						$videoCode    = ' <video id="' . $id . '" muted="" loop="" autoplay="autoplay" preload="auto" width="100%" height="100%"><source src="' . $videoCode . '" type="video/mp4" /></video>';
						$sectionClass = 'html5';
						//$html5Atts = 'muted loop autoplay="autoplay" preload="auto" width="100%" height="100%"';
					}

					break;

				case'webm':
					$videoCode = $header_video_source;
					if ( empty( $videoCode ) ) {
						$videoCode    = '';
						$sectionClass = '';
						$html5Atts    = '';
					} else {
						$videoCode    = ' <video id="' . $id . '" muted="" loop="" autoplay="autoplay" preload="auto" width="100%" height="100%"><source src="' . $videoCode . '" type="video/webm" /></video>';
						$sectionClass = 'html5';
						//$html5Atts = 'muted loop autoplay="autoplay" preload="auto" width="100%" height="100%"';
					}

					break;

			}


			switch ( $header_type ) {
				default:
				case 'parallax':
					$html = '<section class="media-section darkbg" data-stellar-background-ratio="' . $parallaxRatio . '" data-height="' . $height . '" data-type="parallax" ' . $imageAttr . ' ' . $imageMobileAttr . '">
                <div class="inner">
                <div class="container">
                <div class="display-table">
                <div class="table-cell">
                    <h2 class="uppercase no-margin">' . $header . '</h2>
                </div>
                ' . $navHtml . '
                <div class="table-cell">
               ' . $breadcrumbsHtml . '
                </div>
                </div>
                </div>
                </div>
                </section>';

					break;

				case 'parallax_centered':
					$html = '<section class="media-section darkbg" data-stellar-background-ratio="' . $parallaxRatio . '" data-height="' . $height . '" data-type="parallax" ' . $imageAttr . ' ' . $imageMobileAttr . '">
                <div class="inner">
                <div class="text-center">
                <h2 class="uppercase no-margin">' . $header . '</h2>
                '.$breadcrumbsHtml.'
                        </div>
                ' . $navHtml . '
                </div>
                </section>';
					break;

				case 'video':
					$html = '<section class="media-section darkbg ' . $sectionClass . '" data-height="' . $height . '" data-type="video" ' . $imageFallbackAttr . '">
                <div class="video">
                ' . $videoCode . '
                </div>
                <div class="display-table">
                <div class="inner bg5">
                <div class="container">
                <div class="display-table">
                <div class="table-cell">
                <h2 class="uppercase no-margin">' . $header . '</h2>
                </div>
                ' . $navHtml . '
                <div class="table-cell">
               ' . $breadcrumbsHtml . '
                </div>
                </div>
                </div>
                </div>
                </div>
            </section>';
					break;

				case 'video_centered':
					$html = '<section class="media-section darkbg ' . $sectionClass . '" data-height="' . $height . '" data-type="video" ' . $imageFallbackAttr . '">
                <div class="video">
                ' . $videoCode . '
                </div>
                <div class="display-table">
                <div class="inner bg5">
                <div class="text-center">
                <h2 class="uppercase no-margin">' . $header . '</h2>
                '.$breadcrumbsHtml.'
                </div>
                ' . $navHtml . '
                </div>
                </div>
                </section>';
					break;

				case 'ken_burns':

					$html = '<section class="media-section darkbg" data-height="' . $height . '" data-type="kenburns">
                <div class="media-section-image-container">
                ' . $imageTag . '
                </div>

                <div class="inner">
                <div class="container">
                <div class="display-table">
                <div class="table-cell">
                    <h2 class="uppercase no-margin">' . $header . '</h2>
                </div>
                ' . $navHtml . '
                <div class="table-cell">
                ' . $breadcrumbsHtml . '
                </div>
                </div>
                </div>
                </div>
                </section>';
					break;

				case 'ken_burns_centered':

					$html = '<section class="media-section darkbg" data-height="' . $height . '" data-type="kenburns">
                <div class="media-section-image-container">
                ' . $imageTag . '
                </div>
                <div class="inner">
                <div class="text-center">
                <h2 class="uppercase no-margin">' . $header . '</h2>
                '.$breadcrumbsHtml.'
                </div>
                ' . $navHtml . '
                </div>
                </section>';
					break;
				case 'simple_header':
					$html = '
            <section class="bg2 header-page">
            <div class="container">
            <div class="display-table">
            <div class="table-cell">
            ' . ( $simple_header_button == 'true' || $simple_header_button == 'yes' ? '<a class="btn btn-border" href="' . $simple_button_link . '">' . $simple_button_text . '</a>' : '<h2 class="uppercase">' . $header . '</h2>' ) . '

            </div>
            <div class="table-cell text-right">
            ' . $breadcrumbsHtml . ';
            </div>
            </div>
            </div>
            </section>
                ';
					break;

			}


		}

		//return do_shortcode('[full_width]' . $html . '[/full_width]');
		return do_shortcode( $html );
		//return do_shortcode($html);
	}


	/**
	 * is template with sidebar?
	 * @return bool
	 */
	protected function isSidebar() {
		return is_page_template( 'page-custom.php' ) || is_page_template( 'page-custom-left.php' );
	}

	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'id'                          => array(
				'label'   => __( 'header id', 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( "html id attribute", 'ct_theme' )
			),
			'header'                      => array(
				'label'   => __( 'header', 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
                    'help'    => __( "Header text", 'ct_theme' )
			),
			'breadcrumbs'                 => array(
				'label'   => __( 'breadcrumbs', 'ct_theme' ),
				'default' => 'no',
				'type'    => 'select',
				'choices' => array(
					'yes' => __( 'yes', 'ct_theme' ),
					'no'  => __( 'no', 'ct_theme' )
				),
				'help'    => __( "Show breadcrumbs path?", 'ct_theme' )
			),
			'breadcrumbs_class'                          => array(
				'label'   => __( 'Add extra class to breadcrumbs', 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
			),

			'use_theme_options'           => array(
				'label'   => __( 'Use Theme Options', 'ct_theme' ),
				'default' => 'yes',
				'type'    => 'select',
				'choices' => array(
					'yes' => __( 'yes', 'ct_theme' ),
					'no'  => __( 'no', 'ct_theme' ),
				),
			),
			'context'                     => array(
				'label'   => __( 'Context', 'ct_theme' ),
				'default' => 'pages',
				'type'    => 'input'
			),
			'nav'                         => array(
				'label'   => __( 'Show Navigation', 'ct_theme' ),
				'default' => 'no',
				'type'    => false,
				'choices' => array(
					'yes' => __( 'yes', 'ct_theme' ),
					'no'  => __( 'no', 'ct_theme' ),
				),
			),
			'nav_prev_url'                => array(
				'label'   => __( 'Navigation Previous URL', 'ct_theme' ),
				'default' => 'pages',
				'type'    => false
			),
			'nav_next_url'                => array(
				'label'   => __( 'Navigation Next URL', 'ct_theme' ),
				'default' => 'pages',
				'type'    => false
			),
			'nav_index_url'               => array(
				'label'   => __( 'Navigation Index URL', 'ct_theme' ),
				'default' => 'pages',
				'type'    => false
			),
			'header_type'                 => array(
				'label'   => __( 'Header type', 'ct_theme' ),
				'default' => 'parallax',
				'type'    => 'select',
				'choices' => array(
					'parallax'           => 'parallax',
					'parallax_centered'  => 'parallax centered',
					'video'              => 'video',
					'video_centered'     => 'video centered',
					'ken_burns'          => 'ken burns',
					'ken_burns_centered' => 'ken burns centered',
					'simple_header'      => 'simple header',

				),
			),
			'header_height'               => array(
				'label'   => __( 'Title row height', 'ct_theme' ),
				'default' => "220",
				'type'    => 'input',
				'help'    => __( "Header text", 'ct_theme' )
			),
			'header_parallax_ratio'       => array(
				'label'   => __( 'Parallax ratio', 'ct_theme' ),
				'default' => 0.5,
				'type'    => 'input',
				'help'    => __( "Header text", 'ct_theme' )
			),
			'header_image'                => array(
				'label'   => __( "Image", 'ct_theme' ),
				'default' => '',
				'type'    => 'image',
				'help'    => __( "Image source", 'ct_theme' )
			),
			'header_image_mobile'         => array(
				'label'   => __( "Image Mobile", 'ct_theme' ),
				'default' => '',
				'type'    => 'image',
				'help'    => __( "Image source", 'ct_theme' )
			),
			'header_video_source'         => array(
				'label'   => __( 'Video Source', 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( "Header text", 'ct_theme' )
			),
			'video_type'                  => array(
				'label'   => __( 'Video type', 'ct_theme' ),
				'default' => 'mp4',
				'type'    => 'select',
				'choices' => array(
					'mp4'  => __( 'Mp4 direct link', 'ct_theme' ),
					'webm' => __( 'Webm direct link', 'ct_theme' ),
				),
			),
			'header_image_video_fallback' => array(
				'label'   => __( "Image video fallback", 'ct_theme' ),
				'default' => '',
				'type'    => 'image',
				'help'    => __( "Video fallback Image source", 'ct_theme' )
			),
			'simple_header_button'        => array(
				'label'   => __( 'Simple Header Button', 'ct_theme' ),
				'default' => 'no',
				'type'    => 'select',
				'choices' => array(
					'yes' => __( 'yes', 'ct_theme' ),
					'no'  => __( 'no', 'ct_theme' )
				),
			),
			'simple_header_button_text'   => array(
				'label'   => __( 'Simple Button text', 'ct_theme' ),
				'default' => '',
				'type'    => 'input'
			),
			'simple_header_button_link'   => array(
				'label'   => __( 'Simple Button link', 'ct_theme' ),
				'default' => '#',
				'type'    => 'input'
			),
		);
	}
}

new ctTitleRowShortcode();




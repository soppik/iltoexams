<?php

/**
 * 5/6 column shortcode
 */
class ctFiveSixthColumnShortcode extends ctShortcode
{

    /**
     * Returns name
     * @return string|void
     */
    public function getName()
    {
        return '5/6 column';
    }

    /**
     * Shortcode name
     * @return string
     */
    public function getShortcodeName()
    {
        return 'five_sixth_column';
    }

    /**
     * Action
     * @return string
     */

    public function getGeneratorAction()
    {
        return self::GENERATOR_ACTION_INSERT;
    }

    /**
     * Handles shortcode
     * @param $atts
     * @param null $content
     * @return string
     */

    public function handle($atts, $content = null)
    {
        extract(shortcode_atts($this->extractShortcodeAttributes($atts), $atts));
        switch ($background) {
            case 'white':
                $bgClass = '';
                break;
            case 'grey':
                $bgClass = 'bg2';
                break;
            case 'dark_grey':
                $bgClass = 'bg6';
                break;
            case 'dark_blue':
                $bgClass = 'bg3';
                break;
            case 'motive':
                $bgClass = 'bg4';
                break;

            case 'custom_color':
                $bgClass = '';
                break;
            default:
                $bgClass = '';
        }

        $mainContainerAtts = array(
            'class' => array(
                $bgClass,
                $xs ? 'col-xs-' . $xs : '',
                $sm ? 'col-sm-' . $sm : '',
                $md ? 'col-md-' . $md : '',
                $lg ? 'col-lg-' . $lg : '',
                $section == 'true' || $section == 'yes' ? 'section' : '',
                $small == 'true' || $small == 'yes' ? 'small' : '',
                $border == 'true' || $border == 'yes' ? 'border' : '',
                $class,
                (is_numeric($xs_offset)) ? 'col-xs-offset-' . $xs_offset : '',
                (is_numeric($sm_offset)) ? 'col-sm-offset-' . $sm_offset : '',
                (is_numeric($md_offset)) ? 'col-md-offset-' . $md_offset : '',
                (is_numeric($lg_offset)) ? 'col-lg-offset-' . $lg_offset : '',

                (is_numeric($xs_push)) ? 'col-xs-push-' . $xs_push : '',
                (is_numeric($sm_push)) ? 'col-sm-push-' . $sm_push : '',
                (is_numeric($md_push)) ? 'col-md-push-' . $md_push : '',
                (is_numeric($lg_push)) ? 'col-lg-push-' . $lg_push : '',

                (is_numeric($xs_pull)) ? 'col-xs-pull-' . $xs_pull : '',
                (is_numeric($sm_pull)) ? 'col-sm-pull-' . $xs_pull : '',
                (is_numeric($md_pull)) ? 'col-md-pull-' . $md_pull : '',
                (is_numeric($lg_pull)) ? 'col-lg-pull-' . $lg_pull : '',
                ($center == 'true' || $center == 'yes') ? 'text-center' : ''
            )

        );

        if ($background == 'custom_color') {
            $mainContainerAtts['style'] = 'background-color: ' . $bg_color . ';';
        }


        return '<div ' . $this->buildContainerAttributes($mainContainerAtts, $atts) . '>' . do_shortcode($content) . '</div>';

    }

    /**
     * Returns config
     * @return null
     */
    public function getAttributes()
    {
        return array(
            'class' => array('type' => false),
            'center' => array('label' => __('Center content inside?', 'ct_theme'), 'default' => 'false', 'type' => 'select', 'choices' => array("true" => __("true", "ct_theme"), "false" => __("false", "ct_theme"))),
            'section' => array('label' => __('Section ?', 'ct_theme'), 'default' => 'false', 'type' => 'select', 'choices' => array("true" => __("true", "ct_theme"), "false" => __("false", "ct_theme"))),
            'small' => array('label' => __('Small ?', 'ct_theme'), 'default' => 'false', 'type' => 'select', 'choices' => array("true" => __("true", "ct_theme"), "false" => __("false", "ct_theme"))),
            'border' => array('label' => __('Border ?', 'ct_theme'), 'default' => 'false', 'type' => 'select', 'choices' => array("true" => __("true", "ct_theme"), "false" => __("false", "ct_theme"))),
            'background' => array('label' => __('Background', 'ct_theme'), 'default' => '', 'type' => 'select',
                'help' => __("Select section style", 'ct_theme'), 'choices' => array(
                    'white' => 'white',
                    'grey' => 'grey',
                    'dark_grey' => 'dark grey',
                    'dark_blue' => 'dark blue',
                    'motive' => 'motive',
                    'custom_color' => 'custom color',
                    '' => '',
                ),
            ),
            'bg_color' => array('label' => __('Custom background color', 'ct_theme'), 'default' => '', 'type' => 'colorpicker'),
            'content' => array('label' => __('Content', 'ct_theme'), 'default' => '', 'type' => "textarea"),

            'xs' => array('label' => __('Choose column on extra small devices', 'ct_theme'), 'default' => '', 'type' => 'select', 'options' => array(
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
                '9' => '9',
                '10' => '10',
                '11' => '11',
                '12' => '12',
                '' => '')),
            'sm' => array('label' => __('Choose column on small devices', 'ct_theme'), 'default' => '10', 'type' => 'select', 'options' => array(
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
                '9' => '9',
                '10' => '10',
                '11' => '11',
                '12' => '12',
                '' => '')),
            'md' => array('label' => __('Choose column on medium devices', 'ct_theme'), 'default' => '', 'type' => 'select', 'options' => array(
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
                '9' => '9',
                '10' => '10',
                '11' => '11',
                '12' => '12',
                '' => '')),
            'lg' => array('label' => __('Choose column on large devices', 'ct_theme'), 'default' => '', 'type' => 'select', 'options' => array(
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
                '9' => '9',
                '10' => '10',
                '11' => '11',
                '12' => '12',
                '' => '')),
            'xs_offset' => array('label' => __('xs offset', 'ct_theme'), 'default' => '', 'type' => 'select', 'options' => array(
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
                '9' => '9',
                '10' => '10',
                '11' => '11',
                '12' => '12',
                '' => ''),
                'help' => __("Generate the  column offset. Learn more: http://getbootstrap.com/2.3.2/scaffolding.html#gridSystem", 'ct_theme')),
            'sm_offset' => array('label' => __('sm offset', 'ct_theme'), 'default' => '', 'type' => 'select', 'options' => array(
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
                '9' => '9',
                '10' => '10',
                '11' => '11',
                '12' => '12',
                '' => '')),
            'md_offset' => array('label' => __('md offset', 'ct_theme'), 'default' => '', 'type' => 'select', 'options' => array(
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
                '9' => '9',
                '10' => '10',
                '11' => '11',
                '12' => '12',
                '' => '')),
            'lg_offset' => array('label' => __('lg offset', 'ct_theme'), 'default' => '', 'type' => 'select', 'options' => array(
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
                '9' => '9',
                '10' => '10',
                '11' => '11',
                '12' => '12',),
                '' => ''),


            'xs_push' => array('label' => __('xs push', 'ct_theme'), 'default' => '', 'type' => 'select', 'options' => array(
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
                '9' => '9',
                '10' => '10',
                '11' => '11',
                '12' => '12',
                '' => ''),
                'help' => __("Column ordering. Learn more: http://getbootstrap.com/css/#grid-column-ordering", 'ct_theme')),
            'sm_push' => array('label' => __('sm push', 'ct_theme'), 'default' => '', 'type' => 'select', 'options' => array(
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
                '9' => '9',
                '10' => '10',
                '11' => '11',
                '12' => '12',
                '' => '')),
            'md_push' => array('label' => __('md push', 'ct_theme'), 'default' => '', 'type' => 'select', 'options' => array(
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
                '9' => '9',
                '10' => '10',
                '11' => '11',
                '12' => '12',
                '' => '')),
            'lg_push' => array('label' => __('lg push', 'ct_theme'), 'default' => '', 'type' => 'select', 'options' => array(
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
                '9' => '9',
                '10' => '10',
                '11' => '11',
                '12' => '12',
                '' => '')),

            'xs_pull' => array('label' => __('xs pull', 'ct_theme'), 'default' => '', 'type' => 'select', 'options' => array(
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
                '9' => '9',
                '10' => '10',
                '11' => '11',
                '12' => '12',
                '' => ''),
                'help' => __("Column ordering. Learn more: http://getbootstrap.com/css/#grid-column-ordering", 'ct_theme')),
            'sm_pull' => array('label' => __('sm pull', 'ct_theme'), 'default' => '', 'type' => 'select', 'options' => array(
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
                '9' => '9',
                '10' => '10',
                '11' => '11',
                '12' => '12',
                '' => '')),
            'md_pull' => array('label' => __('md pull', 'ct_theme'), 'default' => '', 'type' => 'select', 'options' => array(
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
                '9' => '9',
                '10' => '10',
                '11' => '11',
                '12' => '12',
                '' => '')),
            'lg_pull' => array('label' => __('lg pull', 'ct_theme'), 'default' => '', 'type' => 'select', 'options' => array(
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
                '9' => '9',
                '10' => '10',
                '11' => '11',
                '12' => '12',
                '' => '')),


        );
    }
}

new ctFiveSixthColumnShortcode();
<?php

/**
 * Button shortcode
 */
class ctButtonShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface {

	/**
	 * Returns name
	 * @return string|void
	 */
	public function getName() {
		return 'Button';
	}

	/**
	 * Shortcode name
	 * @return string
	 */
	public function getShortcodeName() {
		return 'button';
	}

	/**
	 * Shortcode type
	 * @return string
	 */
	public function getShortcodeType() {
		return self::TYPE_SHORTCODE_ENCLOSING;
	}


	/**
	 * Handles shortcode
	 *
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */

	public function handle( $atts, $content = null ) {
		extract( shortcode_atts( $this->extractShortcodeAttributes( $atts ), $atts ) );


		switch ( $type ) {
			case 'primary':
				$typeClass = 'btn-primary';
				break;
			case 'border':
				$typeClass = 'btn-border';
				break;
			case 'success':
				$typeClass = 'btn-success';
				break;
			case'danger':
				$typeClass = 'btn-danger';
				break;
			case 'info':
				$typeClass = 'btn-info';
				break;
			case 'link':
				$typeClass = 'btn-link';
				break;
			case'warning':
				$typeClass = 'btn-warning';
				break;
			default:
				$typeClass = 'btn-default';
				break;
		}

		$iconHtml = $icon_name != '' ? '<i class="fa fa-fw ' . $icon_name . '"></i>' : '';


		$mainContainerAtts = array(
			'class' => array(
				'btn',
				$typeClass,
				( $size != 'default' ) ? 'btn-' . $size : '',
				( $status == 'disabled' ) ? 'disabled' : '',
				( $block == 'yes' || $block == 'true' ) ? 'btn-block' : '',
				$class
			),
			'href'  => $link
		);
		if ( $id != '' ) {
			$mainContainerAtts['id'] = $id;
		}
		if ( $new_window == 'true' || $new_window == 'yes' ) {
			$mainContainerAtts['target'] = '_blank';
		}


		$html = '<a  ' . $this->buildContainerAttributes( $mainContainerAtts, $atts ) . '>' . $iconHtml . $content . '</a>';

		return do_shortcode( $html );
	}

	/**
	 * Returns config
	 * @return null
	 */
	public function getAttributes() {
		return array(
			'size'       => array(
				'label'   => __( 'Size', 'ct_theme' ),
				'default' => 'default',
				'type'    => 'select',
				'choices' => array(
					'xs'      => __( 'very small', 'ct_theme' ),
					'sm'      => __( 'small', 'ct_theme' ),
					'default' => __( 'default', 'ct_theme' ),
					'lg'      => __( 'large', 'ct_theme' ),
				),
				'help'    => __( "Button size", 'ct_theme' )
			),
			'type'       => array(
				'label'   => __( 'Button type', 'ct_theme' ),
				'default' => 'default',
				'type'    => 'select',
				'choices' => array(
					'default' => __( 'default', 'ct_theme' ),
					'primary' => __( 'primary', 'ct_theme' ),
					'border'  => __( 'border', 'ct_theme' ),
					'success' => __( 'success', 'ct_theme' ),
					'danger'  => __( 'danger', 'ct_theme' ),
					'info'    => __( 'info', 'ct_theme' ),
					'warning' => __( 'warning', 'ct_theme' ),
					'link'    => __( 'link', 'ct_theme' ),
				),
			),
			'block'      => array(
				'label'   => __( 'Block', 'ct_theme' ),
				'default' => 'no',
				'type'    => 'select',
				'choices' => array( 'yes' => __( 'yes', 'ct_theme' ), 'no' => __( 'no', 'ct_theme' ) ),
				'help'    => __( 'Moves to new line', 'ct_theme' )
			),
			'link'       => array(
				'label' => __( 'Link', 'ct_theme' ),
				'help'  => __( "ex. http://www.google.com", 'ct_theme' )
			),
			'new_window' => array(
				'label'   => __( 'Open link in new Window?', 'ct_theme' ),
				'default' => 'no',
				'type'    => 'select',
				'choices' => array(
					'yes' => __( 'yes', 'ct_theme' ),
					'no'  => __( 'no', 'ct_theme' )
				)
			),
			'icon_name'  => array(
				'label'   => __( 'Icon Name', 'ct_theme' ),
				'type'    => "icon",
				'default' => '',
				'link'    => CT_THEME_ASSETS . '/shortcode/awesome/index.html'
			),
			'content'    => array( 'label' => __( 'Label', 'ct_theme' ), 'default' => '', 'type' => 'textarea' ),
			'status'     => array(
				'default' => 'enabled',
				'type'    => 'select',
				'options' => array( 'enabled' => 'enabled', 'disabled' => 'disabled' ),
				'label'   => __( 'Status', 'ct_theme' )
			),
			'class'      => array(
				'label'   => __( "Custom class", 'ct_theme' ),
				'default' => '',
				'type'    => 'input',
				'help'    => __( 'Adding custom class allows you to set diverse styles in css to the element. Type in name of class, which you defined in css. You can add as much classes as you like.', 'ct_theme' )
			),
			'id'         => array(
				'label'   => __( 'ID attribute', 'ct_theme' ),
				'default' => '',
				'help'    => __( "Custom HTML ID attribute to button", 'ct_theme' )
			),
		);
	}

	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */
	public function getVisualComposerInfo() {
		return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-plus-square' ) );
	}
}

new ctButtonShortcode();
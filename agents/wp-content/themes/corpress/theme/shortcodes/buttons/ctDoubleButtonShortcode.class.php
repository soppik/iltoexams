<?php

/**
 * Button shortcode
 */
class ctDoubleButtonShortcode extends ctShortcode implements ctVisualComposerShortcodeInterface
{

    /**
     * Returns name
     * @return string|void
     */
    public function getName()
    {
        return 'Double Button';
    }

    /**
     * Shortcode name
     * @return string
     */
    public function getShortcodeName()
    {
        return 'double_button';
    }

    /**
     * Shortcode type
     * @return string
     */
    public function getShortcodeType()
    {
        return self::TYPE_SHORTCODE_ENCLOSING;
    }


    /**
     * Handles shortcode
     * @param $atts
     * @param null $content
     * @return string
     */

    public function handle($atts, $content = null)
    {
        extract(shortcode_atts($this->extractShortcodeAttributes($atts), $atts));

        $mainContainerAtts = array(
            'class' => array(
                'btn-group-single',
            ),
        );
        $ButtonHtml = '
        <div' . $this->buildContainerAttributes($mainContainerAtts, $atts) . '>
        <a href="' . $link . '" class="btn btn-lg btn-border"><i class="fa fa-fw ' . $icon . '"></i></a>
        <a href="' . $link . '" class="btn btn-lg btn-border">' . $content . '</a>
        </div>';
        return do_shortcode($ButtonHtml);
    }

    /**
     * Returns config
     * @return null
     */
    public function getAttributes()
    {

        return array(
            'link' => array('label' => __('Link', 'ct_theme'), 'help' => __("ex. http://www.google.com", 'ct_theme')),
            'icon' => array('label' => __('Icon', 'ct_theme'), 'type' => "icon", 'default' => '', 'link' => CT_THEME_ASSETS . '/shortcode/awesome/index.html'),
            'content' => array('label' => __('Content', 'ct_theme'), 'default' => '', 'type' => 'textarea'),
            'class' => array('label' => __("Custom class", 'ct_theme'), 'default' => '', 'type' => 'input', 'help' => __('Adding custom class allows you to set diverse styles in css to the element. Type in name of class, which you defined in css. You can add as much classes as you like.', 'ct_theme')),
        );
    }

	/**
		 * Returns additional info about VC
		 * @return ctVisualComposerInfo
		 */
		public function getVisualComposerInfo() {
			return new ctVisualComposerInfo( $this, array( 'icon' => 'fa-plus-square' ) );
		}
}

new ctDoubleButtonShortcode();
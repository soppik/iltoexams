<?php
/**
 * Helper functions for theme
 */
if (!function_exists('ct_get_footer_settings')) {
    /**
     * Setup dynamic footer. This function is automatically called by plugin
     * @see plugin/footer-columns
     *
     * @param $default
     *
     * @return array
     */
    function ct_get_footer_settings($default)
    {
        return array_merge(
            $default,
            array(
                'before_widget' => '<div class="widget"><div class="widget-inner">',
                'after_widget' => '</div></div>',
                'before_title' => '<h4 class="widget-title">',
                'after_title' => '</h4>',
                'numbers' => array(4, 2, 1)
            ));
    }
}


/**
 * second timeline featured image
 */
if (class_exists('kdMultipleFeaturedImages')) {

    $args = array(
        'id' => 'featured-image-timeline-2',
        'post_type' => 'timeline', // Set this to post or page
        'labels' => array(
            'name' => __('Timeline thumbnail', 'ct_theme'),
            'set' => __('Set Timeline thumbnail', 'ct_theme'),
            'remove' => __('Remove Timeline thumbnail', 'ct_theme'),
            'use' => __('Use as Timeline thumbnail', 'ct_theme'),
        )
    );

    new kdMultipleFeaturedImages($args);
}

/**
 * Wraps content into [chapter] if required
 *
 * @param $content
 *
 * @return string
 */
function ct_theme_page_content_filter($content)
{
    if (!is_page_template('page-custom.php') && is_page() && stripos($content, '[chapter') === false) {
        return do_shortcode('[chapter]' . $content . '[/chapter]');
    }

    return $content;
}

//add_filter('the_content', 'ct_theme_page_content_filter');

if (!function_exists('ct_timeline_featured_image2_src')) {
    /**
     * Returns product image
     *
     * @param $id
     * @param string $name
     * @param string $size
     *
     * @return string
     */

    function ct_timeline_featured_image2_src($id, $name = 'featured-image-timeline-2', $size = 'timeline_thumbnail')
    {
        $imageUrl = '';
        if (class_exists('kdMultipleFeaturedImages')) {

            $imageUrl = kd_mfi_get_featured_image_url($name, 'timeline', $size, $id);
        }

        if ($imageUrl == '') {
            $imageUrl = ct_get_feature_image_src($id, $size);
        }

        return $imageUrl;
    }
}


/**
 * Enqueue scripts
 */
if (!function_exists('ct_theme_scripts')) {
    function ct_theme_scripts()
    {

        wp_register_style('ct-bootstrap', CT_THEME_ASSETS . '/css/bootstrap.css');
        wp_enqueue_style('ct-bootstrap');

        wp_register_script('easing', CT_THEME_ASSETS . '/js/jquery.easing.1.3.js', array('jquery'), false, true);
        wp_enqueue_script('easing');

        wp_register_script('modernizr', CT_THEME_ASSETS . '/js/modernizr.custom.js', array('jquery'), false, true);
        wp_enqueue_script('modernizr');

        wp_register_script('device', CT_THEME_ASSETS . '/js/device.min.js', array('jquery'), false, true);
        wp_enqueue_script('device');

        wp_register_script('snap', CT_THEME_ASSETS . '/js/snap.js', array('jquery'), false, true);
        wp_enqueue_script('snap');

        wp_register_script('ct-onepagenav', CT_THEME_ASSETS . '/js/jquery.onepagenav.js', array('jquery'), false, true);
        wp_enqueue_script('ct-onepagenav');


        if (class_exists('UberMenu')) {
            wp_register_script('menu_uber', CT_THEME_ASSETS . '/js/menu_uber.js', array('jquery'), false, true);
            wp_enqueue_script('menu_uber');
        } else {
            wp_register_script('menu_classic', CT_THEME_ASSETS . '/js/menu_classic.js', array('jquery'), false, true);
            wp_enqueue_script('menu_classic');
        }

    }


    if (is_page_template('content-large.php')) {


        wp_register_script('ct-isotope', CT_THEME_ASSETS . '/js/jquery.isotope.min.js', array('jquery'), false, true);
        wp_enqueue_script('ct-isotope');

    }
    add_action('wp_enqueue_scripts', 'ct_theme_scripts');
}



add_action('index_masonry', 'ct_index_masonry_isotope');
if (!function_exists('ct_index_masonry_isotope')) {
    function ct_index_masonry_isotope()
    {
            wp_register_script('ct-isotope', CT_THEME_ASSETS . '/js/jquery.isotope.min.js', array('jquery'), false, true);
            wp_enqueue_script('ct-isotope');


        add_action('wp_enqueue_scripts', 'ct_theme_scripts');
    }
}
/**
 * Theme activation
 */

function ct_theme_activation()
{
    $theme_data = wp_get_theme();
    //add crop option
    if (!get_option("medium_crop")) {
        add_option("medium_crop", "1");
    } else {
        update_option("medium_crop", "1");
    }

    //add current version
    add_option('corpress_theme_version', $theme_data->get('Version'));
}

ct_theme_activation();

/**
 * returns video html for video format post
 */
if (!function_exists('ct_post_video')) {
    function ct_post_video($postid, $width = 500, $height = 300)
    {
        $m4v = get_post_meta($postid, 'videoM4V', true);
        $ogv = get_post_meta($postid, 'videoOGV', true);
        $direct = get_post_meta($postid, 'videoDirect', true);
        echo do_shortcode('[video width="' . $width . '" height="' . $height . '" link="' . $direct . '" m4v="' . $m4v . '" ogv="' . $ogv . '"]');
    }
}

/**
 * returns video html for video format post
 */
if (!function_exists('ct_get_post_video')) {
    function ct_get_post_video($postid, $width = 500, $height = 300)
    {
        $m4v = get_post_meta($postid, 'videoM4V', true);
        $ogv = get_post_meta($postid, 'videoOGV', true);
        $direct = get_post_meta($postid, 'videoDirect', true);

        return do_shortcode('[video width="' . $width . '" height="' . $height . '" link="' . $direct . '" m4v="' . $m4v . '" ogv="' . $ogv . '"]');
    }
}


/**
 * returns audio html for audio format post
 */
if (!function_exists('ct_post_audio')) {
    function ct_post_audio($postid, $width = 500, $height = 300)
    {
        $mp3 = get_post_meta($postid, 'audioMP3', true);
        $ogg = get_post_meta($postid, 'audioOGA', true);
        $poster = get_post_meta($postid, 'audioPoster', true);
        $audioPosterHeight = get_post_meta($postid, 'audioPosterHeight', true);

        // Calc $height for small images; large will return same value
        //$height = $height * $width / 580;

        echo do_shortcode('[audio height="' . $height . '" width="' . $width . '" mp3="' . $mp3 . '" ogg="' . $ogg . '" poster="' . $poster . '" posterheight="' . $audioPosterHeight . '"]');
    }
}

/**
 * returns audio html for audio format post
 */
if (!function_exists('ct_get__post_audio')) {
    function ct_get_post_audio($postid, $width = 500, $height = 300)
    {
        $mp3 = get_post_meta($postid, 'audioMP3', true);
        $ogg = get_post_meta($postid, 'audioOGA', true);
        $poster = get_post_meta($postid, 'audioPoster', true);
        $audioPosterHeight = get_post_meta($postid, 'audioPosterHeight', true);

        // Calc $height for small images; large will return same value
        //$height = $height * $width / 580;

        return do_shortcode('[audio height="' . $height . '" width="' . $width . '" mp3="' . $mp3 . '" ogg="' . $ogg . '" poster="' . $poster . '" posterheight="' . $audioPosterHeight . '"]');
    }
}

if (!function_exists('ct_get_the_post_audio')) {
    function ct_get_the_post_audio($postid, $width = 500, $height = 300)
    {
        $mp3 = get_post_meta($postid, 'audioMP3', true);
        $ogg = get_post_meta($postid, 'audioOGA', true);
        $poster = get_post_meta($postid, 'audioPoster', true);
        $audioPosterHeight = get_post_meta($postid, 'audioPosterHeight', true);

        // Calc $height for small images; large will return same value
        //$height = $height * $width / 580;

        return do_shortcode('[audio height="' . $height . '" width="' . $width . '" mp3="' . $mp3 . '" ogg="' . $ogg . '" poster="' . $poster . '" posterheight="' . $audioPosterHeight . '"]');
    }
}

/**
 * show single post title?
 */
if (!function_exists('ct_get_single_post_title')) {
    function ct_get_single_post_title($postType = 'page')
    {
        $show = get_post_meta(get_post() ? get_the_ID() : null, 'show_title', true);
        if ($show == 'global' || $show == '') {
            if ($postType == 'page' && ct_get_option('pages_single_show_title', 1)) {
                return get_the_title();
            }
            if ($postType == 'post' && ct_get_option('posts_single_page_title', '')) {
                return ct_get_option('posts_single_page_title', '');
            }
            if ($postType == 'portfolio' && ct_get_option('portfolio_single_page_title', '')) {
                return ct_get_option('portfolio_single_page_title', '');
            }
            if ($postType == 'event' && ct_get_option('event_single_page_title', '')) {
                return ct_get_option('event_single_page_title', '');
            }

        }
        if ($show == "yes") {
            if ($postType == 'post' && ct_get_option('posts_single_page_title', '')) {
                return ct_get_option('posts_single_page_title', '');
            }
            if ($postType == 'portfolio' && ct_get_option('portfolio_single_page_title', '')) {
                return ct_get_option('portfolio_single_page_title', '');
            }
            if ($postType == 'event' && ct_get_option('event_single_page_title', '')) {
                return ct_get_option('event_single_page_title', '');
            }
        }

        return $show == "yes" ? get_the_title() : '';
    }
}

/**
 * single post/page subtitle?
 */
if (!function_exists('ct_get_single_post_subtitle')) {
    function ct_get_single_post_subtitle($postType = 'page')
    {
        $subtitle = get_post_meta(get_post() ? get_the_ID() : null, 'subtitle', true);

        return $subtitle;
    }
}

/**
 * show single post breadcrumbs?
 */
if (!function_exists('ct_show_single_post_breadcrumbs')) {
    function ct_show_single_post_breadcrumbs($postType = 'page')
    {
        $show = get_post_meta(get_post() ? get_the_ID() : null, 'show_breadcrumbs', true);
        if ($show == 'global' || $show == '') {
            if ($postType == 'page') {
                return ct_get_option('pages_single_show_breadcrumbs', 0);
            }
            if ($postType == 'post') {
                return ct_get_option('posts_single_show_breadcrumbs', 0);
            }
            if ($postType == 'portfolio') {
                return ct_get_option('portfolio_single_show_breadcrumbs', 0);
            }
            if ($postType == 'event') {
                return ct_get_option('event_single_show_breadcrumbs', 0);
            }
        }

        return $show == "yes";
    }
}

/**
 * show index post title?
 * todo: get_current_blog_id() zamiast ct_get_option
 */
if (!function_exists('ct_get_index_post_title')) {
    function ct_get_index_post_title($postType = 'page')
    {
        $show = get_post_meta(get_post() ? get_the_ID() : null, 'show_title', true);
        if (is_search()) {
            return __('Search results', 'ct_theme');
        }
        if ($show == 'global' || $show == '') {
            if ($postType == 'post' && ct_get_option('posts_index_show_p_title', 1)) {
                $id = get_option('page_for_posts');
                $page = get_post($id);

                return $page ? $page->post_title : '';
            }
            if ($postType == 'portfolio' && ct_get_option('portfolio_index_show_p_title', 1)) {
                $id = ct_get_option('portfolio_index_page');
                $page = get_post($id);

                return $page ? $page->post_title : '';
            }
            if ($postType == 'faq' && ct_get_option('faq_index_show_title', 1)) {
                $id = ct_get_option('faq_index_page');
                $page = get_post($id);

                return $page ? $page->post_title : '';
            }
        }

        return $show == "yes" ? get_the_title() : '';
    }
}

/**
 * single post/page subtitle?
 */
if (!function_exists('ct_get_index_post_subtitle')) {
    function ct_get_index_post_subtitle($postType = 'page')
    {
        if (is_search()) {
            return '';
        }
        if ($postType == 'post' && ct_get_option('posts_index_show_p_title', 1)) {
            $id = get_option('page_for_posts');
            $subtitle = $id ? get_post_meta($id, 'subtitle', true) : '';

            return $subtitle;
        }

        $subtitle = get_post_meta(get_post() ? get_the_ID() : null, 'subtitle', true);

        return $subtitle;
    }
}


/**
 * show index post breadcrumbs?
 */
if (!function_exists('ct_show_index_post_breadcrumbs')) {
    function ct_show_index_post_breadcrumbs($postType = 'page')
    {
        $show = get_post_meta(get_post() ? get_the_ID() : null, 'show_breadcrumbs', true);
        if ($show == 'global' || $show == '') {
            if ($postType == 'post') {
                return ct_get_option('posts_index_show_breadcrumbs', 1);
            }
            if ($postType == 'portfolio') {
                return ct_get_option('portfolio_index_show_breadcrumbs', 1);
            }
            if ($postType == 'faq') {
                return ct_get_option('faq_index_show_breadcrumbs', 1);
            }
        }

        return $show == "yes";
    }
}


/**
 * add menu shadow ?
 */
if (!function_exists('ct_add_menu_shadow')) {
    function ct_add_menu_shadow()
    {
        return ct_get_option('style_menu_shadow', 'no') == 'yes';
    }
}


/**
 * slider code
 */
if (!function_exists('ct_slider_code')) {
    function ct_slider_code()
    {
        if (is_home()) {
            $id = get_option('page_for_posts');
            $slider = $id ? get_post_meta($id, 'slider', true) : '';
        } else {
            $slider = get_post_meta(get_post() ? get_the_ID() : null, 'slider', true);
        }

        return $slider;
    }
}

/**
 * image size for posts
 */
if (!function_exists('ct_show_single_post_image_size')) {
    function ct_show_single_post_image_size()
    {
        $show = get_post_meta(get_post() ? get_the_ID() : null, 'image_size', true);
        if ($show == 'global' || $show == '') {
            return ct_get_option('post_featured_image_type', 'full');
        }

        return $show;
    }
}

/**
 * socials code
 */
if (!function_exists('ct_socials_code')) {
    function ct_socials_code($big_padding = 'false', $tooltip_placement = null, $align = null, $header = null)
    {
        $socials = array(
            'bitbucket',
            'dribbble',
            'dropbox',
            'facebook',
            'flickr',
            'foursquare',
            'github',
            'gittip',
            'google',
            'instagram',
            'linkedin',
            'pinterest',
            'Renren',
            'rss',
            'skype',
            'stack_exchange',
            'stack_overf',
            'tumblr',
            'twitter',
            'vimeo',
            'vkontakte',
            'Weibo',
            'xing',
            'youtube',
            'email'
        );
        $params = $big_padding ? 'big_padding="' . $big_padding . '" ' : '';
        $params .= $tooltip_placement ? 'tooltip_placement="' . $tooltip_placement . '" ' : '';
        $params .= $align ? 'align="' . $align . '" ' : '';
        $params .= $header ? 'header="' . $header . '" ' : '';
        foreach ($socials as $key) {

            if (ct_get_option($key)) {
                $value = ct_get_option($key);
                $value = ($key == 'rss' && ct_get_option($key) == '1') ? 'yes' : $value;
                $value = ($key == 'rss' && ct_get_option($key) == '0') ? 'no' : $value;
                $params .= $key . '="' . ct_get_option($key) . '" ';
            }
        }

        $socials = '[socials ' . $params . ']';

        return do_shortcode($socials);
    }
}


if (!function_exists('ct_get_post_short_month_name')) {
    function ct_get_post_short_month_name($id = null)
    {
        if (empty($id)) {
            $time = mktime(0, 0, 0, get_the_time('m'));
        } else {
            $time = mktime(0, 0, 0, get_the_time('m', $id));
        }

        $short_month_name = strftime("%b", $time);

        return $short_month_name;
    }
}

if (!function_exists('ct_get_post_day')) {
    function ct_get_post_day($id = null)
    {
        if (empty($id)) {
            $day = get_the_time('d');
        } else {
            $day = get_the_time('d', $id);
        }

        return $day;
    }
}

if (!function_exists('ct_get_comments_count')) {
    function ct_get_comments_count()
    {
        $commentsCount = wp_count_comments(get_the_ID())->approved;

        return $commentsCount;
    }
}

if (!function_exists('ct_get_blog_item_title')) {
    function ct_get_blog_item_title()
    {
        $postTitle = get_the_title();
        $custom = get_post_custom(get_the_ID());


        if (!isset($custom['show_title'][0])) {
            $custom['show_title'][0] = 'global';
        }


        if (isset($custom['show_title'][0])) {
            if (ct_get_option("posts_single_show_title", 1) && $custom['show_title'][0] == 'global') {
                return $postTitle;

            } else {
                return '';
            }
        }

        if (isset($custom['show_title'][0]) && $custom['show_title'][0] == 'yes') {
            return $postTitle;
        }

        return '';
    }
}

if (!function_exists('ct_new_excerpt_more')) {
    function ct_new_excerpt_more($more)
    {
        return '';
    }
}
add_filter('ct_excerpt_more', 'new_excerpt_more');


/* Custom Excerpt length */
function ct_new_excerpt_length($length)
{
    return (ct_get_option("posts_show_index_as", 1) == 'blog_list') ? 60 : 20;
}

add_filter('excerpt_length', 'ct_new_excerpt_length', 999);


if (!function_exists('ct_is_location_contains_menu')) {

    function ct_is_location_contains_menu($location = null)
    {
        $menus = (get_nav_menu_locations());

        return isset($menus[$location]) && is_object(wp_get_nav_menu_object($menus[$location]));
    }
}

if (!function_exists('ct_get_comments_number')) {
    /**
     *
     */
    function ct_get_comments_number()
    {
        comments_number(__('No Comments', 'ct_theme'), '1 ' . __('Comment', 'ct_theme'), '% ' . __('Comments', 'ct_theme'));
    }
}

/* Tags Cloud - custom class */
add_filter('the_tags', 'ct_tagCloudClass');
function ct_tagCloudClass($list)
{
    //$list = str_replace('<a', '<a class="btn btn-default btn-sm" ', $list);
    return $list;
}

/*add class to Author Box image*/
function ct_mytheme_get_avatar($avatar, $id_or_email, $size)
{
    $avatar = str_replace('<img', '<img class="media-object"', $avatar);

    return $avatar;
}

add_filter('get_avatar', 'ct_mytheme_get_avatar', 10, 3);


/*add class to post-single navigation*/
add_filter('next_post_link', 'ct_post_link_attributes');
add_filter('previous_post_link', 'ct_post_link_attributes');
function ct_post_link_attributes($output)
{
    $injection = 'class="navigation-blog"';

    return str_replace('<a href=', '<a ' . $injection . ' href=', $output);
}


if (!function_exists('ct_is_browser_type')) {
    function ct_is_browser_type($type = null)
    {
        $user_agent = strtolower($_SERVER['HTTP_USER_AGENT']);
        if ($type == 'bot') {
            if (preg_match("/googlebot|adsbot|yahooseeker|yahoobot|msnbot|watchmouse|pingdom\.com|feedfetcher-google/", $user_agent)) {
                return true;
            }
        } else if ($type == 'browser') {
            if (preg_match("/mozilla\/|opera\//", $user_agent)) {
                return true;
            }
        } else if ($type == 'mobile') {
            if (preg_match("/phone|iphone|itouch|ipod|symbian|android|htc_|htc-|palmos|blackberry|opera mini|iemobile|windows ce|nokia|fennec|hiptop|kindle|mot |mot-|webos\/|samsung|sonyericsson|^sie-|nintendo/", $user_agent)) {
                return true;
            } else if (preg_match("/mobile|pda;|avantgo|eudoraweb|minimo|netfront|brew|teleca|lg;|lge |wap;| wap /", $user_agent)) {
                return true;
            }
        }

        return false;
    }
}


add_filter('body_class', 'custom_body_class', 10, 2);
function custom_body_class($classes)
{

    if (!is_page_template('page-maintenance.php') && get_post()) {

        $custom = get_post_custom(get_the_ID());

        if (!isset($custom["navbar_transparent"][0])) {
            $custom["navbar_transparent"][0] = 'global';
        }


        if (isset($custom["body_class"][0]) && !empty($custom["body_class"][0])) {
            $classes[] = $custom["body_class"][0];
        }


        if ($custom["navbar_transparent"][0] == 'yes') {
            $classes[] = 'navbar-transparent';
            $classes[] = 'revert-to-transparent';

        } elseif ($custom["navbar_transparent"][0] == 'global') {
            if (ct_get_option('pages_navbar_transparent') == 'yes') {

                $classes[] = 'navbar-transparent';
                $classes[] = 'revert-to-transparent';
            }
        }
    }


//full width layout?
    if (get_theme_mod('layout') == 'boxed') {
        $classes[] = 'boxed';
        $classes[] = 'ptn' . get_theme_mod('pattern');

    }


//sticky?
    if (ct_get_option('navbar_sticky') == 'yes' || ct_get_option('navbar_sticky') == '') {
        $classes[] = 'navbar-fixed';
    } else {
        $classes[] = '';
    }


//Navbar type (default, middle, side)
    if (ct_get_option('navbar_type') != '') {
        $classes[] = ct_get_option('navbar_type');
        if (ct_get_option('navbar_type') == 'navbar-sidebar') {
            $classes[] = 'navbar-dark';
        }
    }


//navbar style
    if (ct_get_option('navbar_style') != '') {
        $classes[] = ct_get_option('navbar_style');
    } else {
        $classes[] = 'navbar-light';
    }

//top bar style
    if (ct_get_option('topbar_style') != '') {
        $classes[] = ct_get_option('topbar_style');
    } else {
        $classes[] = 'header-light';
    }


//show topbar?
    if (ct_get_option('show_topbar') || ct_get_option('show_topbar') == '') {
        $classes[] = 'with-topbar';
    }


    $key = array_search('navbar-fixed', $classes);
    if (is_page_template('page-maintenance.php')) {
        unset($classes[$key]);
    }

    $key = array_search('fade', $classes);
    if (isset($classes[$key])) {
        unset($classes[$key]);

    }

    return $classes;
}


if (!function_exists('ct_get_excerpt_by_id')) {
    function ct_get_excerpt_by_id($post_id, $lenght = 20, $after_text = '&#8230;')
    {
        $post = get_post($post_id);

        add_filter('excerpt_more', '__return_empty_string');

        $excerpt = $post->post_excerpt;
        if (!$excerpt) {
            $text = $post->post_content;

            $text = strip_shortcodes($text);

            /** This filter is documented in wp-includes/post-template.php */
            $text = apply_filters('the_content', $text);
            $text = str_replace(']]>', ']]&gt;', $text);
            $excerpt_length = apply_filters('excerpt_length', 55);

            $excerpt_more = apply_filters('excerpt_more', ' ' . '[&hellip;]');
            $excerpt = wp_trim_words($text, $excerpt_length, $excerpt_more);
        }

        $excerpt = apply_filters('get_the_excerpt', $excerpt);

        return '<p>' . $excerpt . '</p>';
    }
}


// filter to replace class on reply link


add_filter('comment_reply_link', 'replace_reply_link_class');

function replace_reply_link_class($class)
{
    $class = str_replace("class='comment-reply-link", "class='reply-button", $class);

    return $class;
}


//modify native search WP widget
if (!function_exists('ct_update_widget')) {
    function ct_update_widget($params)
    {
        if (isset($params[0]) && $params[0]['widget_name'] == "Search") {
            $injection = 'search-widget ';
            $params[0]['before_widget'] = str_replace('widget_search', $injection, $params[0]['before_widget']);
        }


        /*
if (isset($params[0]) && $params[0]['widget_name'] == "Archives") {
    $injection = 'widget-archives ';
    $params[0]['before_widget'] = str_replace('<div class="shadow-box', '<div class="shadow-box ' . $injection, $params[0]['before_widget']);
}
*/


        $jhj = '

        <div class=" search-widget">
                        <div class="input-group input-group-lg form-group-float-label">
                            <input id="s" class="form-control" required="" name="s" type="text">
                            <label for="s">Search</label>
                                <span class="input-group-btn">
                                    <button class="btn btn-primary"><i class="fa fa-search"></i></button>
                                </span>
                        </div>
                    </div>



        ';


        return $params;
    }
}
add_filter('dynamic_sidebar_params', 'ct_update_widget');
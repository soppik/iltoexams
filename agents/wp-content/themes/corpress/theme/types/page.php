<?php
require_once CT_THEME_LIB_DIR . '/types/ctPageTypeBase.class.php';

/**
 * page type
 */
class ctPageType extends ctPageTypeBase
{

    /**
     * Adds meta box
     */

    public function addMetaBox()
    {
        parent::addMetaBox();
        add_meta_box("page-template-meta", __("Template settings", 'ct_theme'), array($this, "pageTemplateMeta"), "page", "normal", "high");
        add_meta_box("page-header-meta", __("Header settings", 'ct_theme'), array($this, "pageHeaderMeta"), "page", "normal", "high");
    }

    /**
     * page template settings
     */

    public function pageTemplateMeta()
    {
        global $post;
        $custom = get_post_custom($post->ID);
        $navbar_transparent = isset($custom["navbar_transparent"][0]) ? $custom["navbar_transparent"][0] : "";
        $title = isset($custom["show_title"][0]) ? $custom["show_title"][0] : "";
        $bread = isset($custom["show_breadcrumbs"][0]) ? $custom["show_breadcrumbs"][0] : "";
        $body_class = isset($custom["body_class"][0]) ? $custom["body_class"][0] : "";


        ?>

        <p>
            <label for="navbar_transparent"><?php _e('Navbar transparent', 'ct_theme') ?>: </label>
            <select id="navbar_transparent" name="navbar_transparent">
                <option
                    value="no" <?php echo selected('no', $navbar_transparent) ?>><?php _e("no", 'ct_theme') ?></option>
                <option
                    value="yes" <?php echo selected('yes', $navbar_transparent) ?>><?php _e("yes", 'ct_theme') ?></option>
                <option
                    value="global" <?php echo selected('global', $navbar_transparent) ?>><?php _e("global", 'ct_theme') ?></option>
            </select>
        </p>
        <p class="howto"><?php _e('if you want to for the selected page, the navbar (in sticky mode) was transparent when it is on the top of the page.', 'ct_theme') ?></p>

        <p>
            <label for="show_title"><?php _e('Show title', 'ct_theme') ?>: </label>
            <select id="show_title" name="show_title">
                <option
                    value="global" <?php echo selected('global', $title) ?>><?php _e("use global settings", 'ct_theme') ?></option>
                <option value="yes" <?php echo selected('yes', $title) ?>><?php _e("show title", 'ct_theme') ?></option>
                <option value="no" <?php echo selected('no', $title) ?>><?php _e("hide title", 'ct_theme') ?></option>
            </select>
        </p>
        <p class="howto"><?php _e("Show page title?", 'ct_theme') ?></p>

        <p>
            <label for="show_breadcrumbs"><?php _e('Show breadcrumbs', 'ct_theme') ?>: </label>
            <select id="show_breadcrumbs" name="show_breadcrumbs">
                <option
                    value="global" <?php echo selected('global', $bread) ?>><?php _e("use global settings", 'ct_theme') ?></option>
                <option
                    value="yes" <?php echo selected('yes', $bread) ?>><?php _e("show breadcrumbs", 'ct_theme') ?></option>
                <option
                    value="no" <?php echo selected('no', $bread) ?>><?php _e("hide breadcrumbs", 'ct_theme') ?></option>
            </select>
        </p>
        <p class="howto"><?php _e("Show breadcrumbs?", 'ct_theme') ?></p>

        <p>
            <label for="body_class"><?php _e('Custom Body Class for this page', 'ct_theme') ?>: </label>
            <input id="body_class" class="regular-text" name="body_class"
                   value="<?php echo $body_class; ?>"/>
        </p>

    <?php
    }

    public function pageHeaderMeta()
    {
        global $post;
        $custom = get_post_custom($post->ID);
        $global = isset($custom["global"][0]) ? $custom["global"][0] : "yes";
        $header_type = isset($custom["header_type"][0]) ? $custom["header_type"][0] : "";
        $header_height = isset($custom["header_height"][0]) ? $custom["header_height"][0] : "";
        $header_parallax_ratio = isset($custom["header_parallax_ratio"][0]) ? $custom["header_parallax_ratio"][0] : "";
        $header_image = isset($custom["header_image"][0]) ? $custom["header_image"][0] : "";
        $header_image_mobile = isset($custom["header_image_mobile"][0]) ? $custom["header_image_mobile"][0] : "";
        $header_video_source = isset($custom["header_video_source"][0]) ? $custom["header_video_source"][0] : "";
        $video_type = isset($custom["video_type"][0]) ? $custom["video_type"][0] : "";
        $header_image_video_fallback = isset($custom["header_image_video_fallback"][0]) ? $custom["header_image_video_fallback"][0] : "";
        ?>


        <p>
            <label for="global"><?php _e('Global settings', 'ct_theme') ?>: </label>
            <select id="global" name="global">
                <option value="yes" <?php echo selected('yes', $global) ?>><?php _e("yes", 'ct_theme') ?></option>
                <option value="no" <?php echo selected('no', $global) ?>><?php _e("no", 'ct_theme') ?></option>
            </select>
        </p>

        <p class="howto"><?php _e('Select header type', 'ct_theme') ?></p>
        <p>
            <label for="header_type"><?php _e('Header type', 'ct_theme') ?>: </label>
            <select id="header_type" name="header_type">
                <option
                    value="parallax" <?php echo selected('parallax', $header_type) ?>><?php _e("parallax", 'ct_theme') ?></option>
                <option
                    value="parallax_centered" <?php echo selected('parallax_centered', $header_type) ?>><?php _e("parallax_centered", 'ct_theme') ?></option>
                <option
                    value="video" <?php echo selected('video', $header_type) ?>><?php _e("video", 'ct_theme') ?></option>
                <option
                    value="video_centered" <?php echo selected('video_centered', $header_type) ?>><?php _e("video_centered", 'ct_theme') ?></option>
                <option
                    value="ken_burns" <?php echo selected('ken_burns', $header_type) ?>><?php _e("ken_burns", 'ct_theme') ?></option>
                <option
                    value="ken_burns_centered" <?php echo selected('ken_burns_centered', $header_type) ?>><?php _e("ken_burns_centered", 'ct_theme') ?></option>
            </select>
        </p>

        <p>
            <label for="header_height"><?php _e('Height', 'ct_theme') ?>: </label>
            <input id="header_height" class="regular-text" name="header_height" value="<?php echo $header_height; ?>"/>
        </p>

        <p>
            <label for="header_parallax_ratio"><?php _e('Header parallax ratio', 'ct_theme') ?>: </label>
            <input id="header_parallax_ratio" class="regular-text" name="header_parallax_ratio"
                   value="<?php echo $header_parallax_ratio; ?>"/>
        </p>


        <p>
            <label for="header_image"><?php _e('Header image', 'ct_theme') ?>: </label>
            <input id="header_image" class="regular-text" name="header_image"
                   value="<?php echo $header_image; ?>"/>
        </p>

        <p>
            <label for="header_image_mobile"><?php _e('Header image mobile', 'ct_theme') ?>: </label>
            <input id="header_image_mobile" class="regular-text" name="header_image_mobile"
                   value="<?php echo $header_image_mobile; ?>"/>
        </p>


        <p>
            <label for="header_video_source"><?php _e('Header video source', 'ct_theme') ?>: </label>
            <input id="header_video_source" class="regular-text" name="header_video_source"
                   value="<?php echo $header_video_source; ?>"/>
        </p>

        <p class="howto"><?php _e('Select video type', 'ct_theme') ?></p>
        <p>
            <label for="video_type"><?php _e('Video type', 'ct_theme') ?>: </label>
            <select id="video_type" name="video_type">

                <option value="mp4" <?php echo selected('mp4', $video_type) ?>><?php _e("mp4", 'ct_theme') ?></option>
                <option
                    value="webm" <?php echo selected('webm', $video_type) ?>><?php _e("webm", 'ct_theme') ?></option>
            </select>
        </p>


        <p>
            <label for="header_image_video_fallback"><?php _e('Image video fallback', 'ct_theme') ?>: </label>
            <input id="header_image_video_fallback" class="regular-text" name="header_image_video_fallback"
                   value="<?php echo $header_image_video_fallback; ?>"/>
        </p>


    <?php
    }


    public function saveDetails()
    {
        parent::saveDetails();
        global $post;

        $fields = array('navbar_transparent',
            'show_title',
            'show_breadcrumbs',
            'title',
            'subtitle',
            'header_type',
            'header_parallax_ratio',
            'header_image',
            'header_image_mobile',
            'header_video_source',
            'video_type',
            'header_height',
            'header_image_video_fallback',
            'global',
            'body_class');
        foreach ($fields as $field) {
            if (isset($_POST[$field])) {
                update_post_meta($post->ID, $field, $_POST[$field]);
            }
        }
    }
}

new ctPageType();

<?php
require_once CT_THEME_LIB_DIR . '/types/ctPortfolioTypeBase.class.php';

/**
 * Custom type - portfolio
 */
class ctPortfolioType extends ctPortfolioTypeBase
{

    /**
     * Adds meta box
     */

    public function addMetaBox()
    {
        parent::addMetaBox();
        add_meta_box("portfolio-template-meta", __("Template settings", 'ct_theme'), array($this, "portfolioTemplateMeta"), "portfolio", "normal", "high");
    }

    /**
     * Draw s portfolio meta
     */

    public function portfolioMeta()
    {
        global $post;

        $custom = get_post_custom($post->ID);
        $summary = isset($custom["summary"][0]) ? $custom["summary"][0] : "";
        $summary_title = isset($custom["summary_title"][0]) ? $custom["summary_title"][0] : "";


        $date = isset($custom["date"][0]) ? $custom["date"][0] : "";
        $client = isset($custom["client"][0]) ? $custom["client"][0] : "";

        $external_url = isset($custom["external_url"][0]) ? $custom["external_url"][0] : "";

        $video = isset($custom["video"][0]) ? $custom["video"][0] : "";
        $displayMethod = isset($custom['display_method'][0]) ? $custom['display_method'][0] : 'image';
        $revolution_slider = isset($custom['revolution_slider'][0]) ? $custom['revolution_slider'][0] : '';

        if ($supportsRevolutionSlider = function_exists('rev_slider_shortcode')) {
            global $wpdb;
            $slides = $wpdb->get_results("SELECT * FROM wp_revslider_sliders");
        }
        ?>

        <p>
            <label
                for="summary"><?php echo __("Summary", 'ct_theme') ?> </label>
            <textarea id="summary" class="regular-text" name="summary" cols="100"
                      rows="10"><?php echo $summary; ?></textarea>
        </p>
        <p class="howto"><?php _e("The content of this field will not be visible in the horizontal mode", 'ct_theme') ?></p>


        <p>
            <label for="summary_title"><?php _e('Summary Title', 'ct_theme') ?>: </label>
            <input id="summary_title" class="regular-text" name="summary_title" value="<?php echo $summary_title; ?>"/>
        </p>
        <p class="howto"><?php _e("Summary header", 'ct_theme') ?></p>


        <p>
            <label for="date"><?php _e('Date', 'ct_theme') ?>: </label>
            <input id="date" class="regular-text" name="date" value="<?php echo $date; ?>"/>
        </p>
        <p class="howto"><?php _e("Information about date", 'ct_theme') ?></p>

        <p>
            <label for="client"><?php _e('Client', 'ct_theme') ?>: </label>
            <input id="client" class="regular-text" name="client" value="<?php echo $client; ?>"/>
        </p>
        <p class="howto"><?php _e("Information about client", 'ct_theme') ?></p>

        <p>
            <label for="url">Url: </label>
            <input id="url" class="regular-text" name="external_url" value="<?php echo $external_url; ?>"/>
        </p>
        <p class="howto"><?php _e("Link to external site. Leave empty to hide button", 'ct_theme') ?></p>



        <p>
            <label for="display_method"><?php _e('Show portfolio item as', 'ct_theme') ?>: </label>
            <select class="ct-toggler" id="display_method" name="display_method">
                <option data-group=".display"
                        value="image" <?php echo selected('image', $displayMethod) ?>><?php _e("Featured image", 'ct_theme') ?></option>
                <option data-group=".display" data-toggle="ct-toggable.gallery"
                        value="gallery" <?php echo selected('gallery', $displayMethod) ?>><?php _e("Gallery", 'ct_theme') ?></option>
                <option data-group=".display" data-toggle="ct-toggable.video"
                        value="video" <?php echo selected('video', $displayMethod) ?>><?php _e("Video", 'ct_theme') ?></option>
            </select>
        </p>
        <p class="ct-toggable video display">
            <label for="video"><?php _e('Video url', 'ct_theme') ?>: </label>
            <input id="video" class="regular-text" name="video" value="<?php echo $video; ?>"/>
        </p>
        <?php if ($supportsRevolutionSlider): ?>
        <p class="ct-toggable revolution-slider display">
            <label for="revolutionSlider"><?php _e('Revolution slider', 'ct_theme') ?>: </label>

            <select id="revolutionSlider" name="revolution_slider">
                <?php foreach ($slides as $slide): ?>
                    <option <?php echo selected($slide->alias, $revolution_slider) ?>
                        value="<?php echo $slide->alias ?>"><?php echo $slide->title ?></option>
                <?php endforeach; ?>
            </select>
        </p>
    <?php endif; ?>
    <?php
    }

    /**
     * portfolio template settings
     */

    public function portfolioTemplateMeta()
    {
        global $post;
        $custom = get_post_custom($post->ID);
        $portfolio_single_horizontal = isset($custom["portfolio_single_horizontal"][0]) ? $custom["portfolio_single_horizontal"][0] : "";
        $title = isset($custom["show_title"][0]) ? $custom["show_title"][0] : "";
        $bread = isset($custom["show_breadcrumbs"][0]) ? $custom["show_breadcrumbs"][0] : "";
        $boxed = isset($custom["use_boxed"][0]) ? $custom["use_boxed"][0] : "";
        $slider = isset($custom["slider"][0]) ? $custom["slider"][0] : "";
        ?>
        <p>
            <label for="portfolio_single_horizontal"><?php _e('Portfolio single horizontal', 'ct_theme') ?>: </label>
            <select  id="portfolio_single_horizontal" name="portfolio_single_horizontal">
                <option
                    value="global" <?php echo selected('global', $portfolio_single_horizontal) ?>><?php _e("use global settings", 'ct_theme') ?></option>
                <option value="yes" <?php echo selected('yes', $portfolio_single_horizontal) ?>><?php _e("yes", 'ct_theme') ?></option>
                <option value="no" <?php echo selected('no', $portfolio_single_horizontal) ?>><?php _e("no", 'ct_theme') ?></option>
            </select>
        </p>


        <p>
            <label for="show_title"><?php _e('Show title', 'ct_theme') ?>: </label>
            <select id="show_title" name="show_title">
                <option
                    value="global" <?php echo selected('global', $title) ?>><?php _e("use global settings", 'ct_theme') ?></option>
                <option value="yes" <?php echo selected('yes', $title) ?>><?php _e("show title", 'ct_theme') ?></option>
                <option value="no" <?php echo selected('no', $title) ?>><?php _e("hide title", 'ct_theme') ?></option>
            </select>
        </p>
        <p class="howto"><?php _e("Show page title?", 'ct_theme') ?></p>

        <p>
            <label for="show_breadcrumbs"><?php _e('Show breadcrumbs', 'ct_theme') ?>: </label>
            <select id="show_breadcrumbs" name="show_breadcrumbs">
                <option
                    value="global" <?php echo selected('global', $bread) ?>><?php _e("use global settings", 'ct_theme') ?></option>
                <option
                    value="yes" <?php echo selected('yes', $bread) ?>><?php _e("show breadcrumbs", 'ct_theme') ?></option>
                <option
                    value="no" <?php echo selected('no', $bread) ?>><?php _e("hide breadcrumbs", 'ct_theme') ?></option>
            </select>
        </p>
        <p class="howto"><?php _e("Show breadcrumbs?", 'ct_theme') ?></p>

        <p>
            <label for="use_boxed"><?php _e('Use boxed layout', 'ct_theme') ?>: </label>
            <select id="use_boxed" name="use_boxed">
                <option
                    value="global" <?php echo selected('global', $boxed) ?>><?php _e("use global settings", 'ct_theme') ?></option>
                <option
                    value="yes" <?php echo selected('yes', $boxed) ?>><?php _e("boxed layout", 'ct_theme') ?></option>
                <option value="no" <?php echo selected('no', $boxed) ?>><?php _e("full layout", 'ct_theme') ?></option>
            </select>
        </p>
        <p class="howto"><?php _e("Use boxed or full layout template for this page?", 'ct_theme') ?></p>

    <?php
    }


    public function saveDetails()
    {
        parent::saveDetails();
        global $post;

        $fields = array('portfolio_single_horizontal', 'date', 'client', 'show_title', 'show_breadcrumbs', 'summary', 'summary_title');
        foreach ($fields as $field) {
            if (isset($_POST[$field])) {
                update_post_meta($post->ID, $field, $_POST[$field]);
            }
        }
    }
}

new ctPortfolioType();
<?php
/**
 * Woocommerce custom modifications
 */

add_theme_support( 'woocommerce' );


function ct_is_woocommerce_active() {
	return class_exists( 'WooCommerce' );
}

if ( ! ct_is_woocommerce_active() ) {
	return;
}

add_filter('roots-nice-search','__return_false');

/* custom js */
if ( ! function_exists( 'ct_woo_scripts' ) ) {
	function ct_woo_scripts() {
        wp_register_script( 'ct-woo-select2', CT_THEME_DIR_URI . '/woocommerce/js/select2.js', array( 'jquery' ), false, true );
        wp_enqueue_script( 'ct-woo-select2' );

		wp_register_script( 'ct-woo-customjs', CT_THEME_DIR_URI . '/woocommerce/js/woocommerce.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'ct-woo-customjs' );

		if ( is_product() ) {
			wp_register_script( 'ct-isotope', CT_THEME_ASSETS . '/js/jquery.isotope.min.js' );
			wp_enqueue_script( 'ct-isotope' );

			wp_register_script( 'ct-woo-check-gallery', CT_THEME_DIR_URI . '/woocommerce/js/check-gallery.js', array( 'wc-add-to-cart-variation' ), false, true );
			wp_enqueue_script( 'ct-woo-check-gallery' );

			wp_register_script( 'ct-flex-easing', CT_THEME_ASSETS . '/js/jquery.easing.1.3.min.js', array( 'ct-flex-slider' ), false, true );
			wp_enqueue_script( 'ct-flex-easing' );

			wp_register_script( 'ct-flex-slider', CT_THEME_DIR_URI . '/assets/plugins/flexslider/jquery.flexslider-min.js', array( 'jquery' ), false, true );
			wp_enqueue_script( 'ct-flex-slider' );

			wp_register_script( 'ct-flexslider_init', CT_THEME_ASSETS . '/js/flexslider_init.js', array( 'ct-flex-slider' ), false, true );
			wp_enqueue_script( 'ct-flexslider_init' );
		}
	}
}

add_action( 'wp_enqueue_scripts', 'ct_woo_scripts' );

/*
WP roots issue with - fix
http://wordpress.stackexchange.com/questions/95293/wp-enqueue-style-will-not-let-me-enforce-screen-only
	'media'   => 'only screen and (max-width: ' . apply_filters( 'woocommerce_style_smallscreen_breakpoint', $breakpoint = '768px' ) . ')'
*/
remove_filter( 'style_loader_tag', 'roots_clean_style_tag' );

/* move related products */

function woocommerce_remove_related_produts() {
	remove_action(
		'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
}

add_action(
	'woocommerce_after_single_product_summary', 'woocommerce_remove_related_produts'
);


function woocommerce_custom_related_produts() {
	$args = array(
		'posts_per_page' => 4,
		'columns'        => 4,
		'orderby'        => 'rand'
	);
	woocommerce_related_products( apply_filters( 'woocommerce_output_related_products_args', $args ) );
}

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
add_action( 'woocommerce_after_single_product_summary', 'ct_woocommerce_output_upsells', 15 );

if ( ! function_exists( 'ct_woocommerce_output_upsells' ) ) {
    function ct_woocommerce_output_upsells() {
        woocommerce_upsell_display( 4,4 ); // Display 3 products in rows of 3
    }
}

// Add pif-has-gallery class to products that have a gallery
function product_has_gallery( $classes ) {
	global $product;

	$post_type = get_post_type( get_the_ID() );

	if ( $post_type == 'product' ) {

		$attachment_ids = $product->get_gallery_attachment_ids();
		if ( $attachment_ids ) {
			$classes[] = 'product';
			$classes[] = 'pif-has-gallery';
		}
	}

	return $classes;
}


function woocommerce_template_loop_spt() {
	// Display the second thumbnails
	if ( ! function_exists( 'woocommerce_template_loop_second_product_thumbnail' ) ) {
		function woocommerce_template_loop_second_product_thumbnail() {
			global $product, $woocommerce;

			$post_type = get_post_type( get_the_ID() );

			if ( $post_type == 'product' ) {

				$attachment_ids = $product->get_gallery_attachment_ids();

				if ( $attachment_ids ) {
					$secondary_image_id = $attachment_ids['0'];
					echo wp_get_attachment_image( $secondary_image_id, 'shop_catalog', '', $attr = array( 'class' => 'secondary-image attachment-shop-catalog' ) );
				}
			}
		}
	}

	add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_second_product_thumbnail', 11 );
	add_filter( 'post_class', 'product_has_gallery' );

	return '';
}


// redirect to page on login

add_filter( 'woocommerce_login_redirect', 'ras_login_redirect' );

function ras_login_redirect( $redirect_to ) {
	$redirect_to = home_url();

	return $redirect_to;
}

function ct_wrapper_woo_tables() {
	echo '<div class="table-responsive">';
}

function ct_wrapper_woo_tables_end() {
	echo '</div>';
}

add_action( 'woocommerce_before_cart_table', 'ct_wrapper_woo_tables', 10 );
add_action( 'woocommerce_after_cart_table', 'ct_wrapper_woo_tables_end', 10 );
add_action( 'yith_wcwl_before_wishlist', 'ct_wrapper_woo_tables', 10 );
add_action( 'yith_wcwl_after_wishlist', 'ct_wrapper_woo_tables_end', 10 );


// Change number or products per row to 3
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
    function loop_columns() {
        return 3; // 3 products per row
    }
}

// Display 9 products per page.
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 9;' ), 20 );


// remove woocommerce title
add_filter( 'woocommerce_show_page_title', '__return_false' );


// Add wrapper around product list
add_filter('woocommerce_before_shop_loop_item', 'product_list_wrapper_start');
if (!function_exists('product_list_wrapper_start')) {
    function product_list_wrapper_start() {
        echo '<div class="product-inner">';
    }
}
add_filter('woocommerce_after_shop_loop_item', 'product_list_wrapper_end',15);
if (!function_exists('product_list_wrapper_end')) {
    function product_list_wrapper_end() {
        echo '</div>';
    }
}


// Move price in product list
remove_filter('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price');
add_filter('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_price', 1);



// Move rating in product list
remove_filter('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
add_filter('ct_woocommerce_after_shop_loop_item', 'woocommerce_template_loop_rating');


// Add shadow to menu in woocommerce pages

add_filter( 'body_class', 'ct_modify_body_classes', 10, 2 );

function ct_modify_body_classes( $classes, $class )
{
    // Modify the array $classes to your needs
    if( is_woocommerce() )
    {
        $classes[] = 'navbar-with-shadow';
    }
    return $classes;
}

// Add product arrow navigations to header

function ct_product_go_back_links()
{
    $shop_page_url = get_permalink(woocommerce_get_page_id('shop'));
    $product_cats = wp_get_post_terms(get_the_ID(), 'product_cat');
    $prev = get_next_post();
    $next = get_previous_post();


    if($prev || $next){
        echo '<div class="single-product-navigation">';
        if($prev){
            echo'<a href="'. get_permalink($prev->ID).'"
               class="btn btn-border btn-prev">'. __('<i class="fa fa-angle-left"></i> Prev', 'ct_theme') .'</a>';
        }
        if($next){
            echo'<a href="'. get_permalink($next->ID).'"
               class="btn btn-border btn-next">'. __('Next <i class="fa fa-angle-right"></i>', 'ct_theme') .'</a>';
        }
        echo '</div>';
    }

}

add_action( 'ct_woo_custom_product_navigation', 'ct_product_go_back_links', 1);

// custom product search widget
add_filter( 'get_product_search_form', 'woo_custom_product_searchform' );

function woo_custom_product_searchform( $form ) {

    $form = '<div class="search-widget">
                <form class="search-form" role="search" method="get" id="searchform" action="' . esc_url( home_url( '/' ) ) . '">
                    <div class="input-group input-group-lg form-group-float-label">
                        <input id="s" class="form-control" required="" name="s" type="text">
                        <label for="s">' . __( 'My search form', 'woocommerce' ) . '</label>
                            <span class="input-group-btn">
                                <button id="searchsubmit" type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                            </span>
                    </div>
                </form>
            </div>';

    return $form;
}


// product breadcrumbs

function woocommerce_custom_breadcrumb() {
    woocommerce_breadcrumb();
}

add_action( 'woo_custom_breadcrumb', 'woocommerce_custom_breadcrumb' );
/* customize woo breacdrumbs */
add_filter( 'woocommerce_breadcrumb_defaults', 'ct_woo_breadcrumbs' );
function ct_woo_breadcrumbs( $defaults ) {
    $defaults['delimiter'] = '<span class="breadsep">/</span>';

    return $defaults;
}

// Share buttons in single product

add_action('woocommerce_share','ct_wooshare');
function ct_wooshare(){
    global $post;
    echo '
    <div class="display-table product-share">
        <div class="table-cell">
            <h6 class="widget-title">'. __('Share this product', 'ct_theme') .'</h6>
        </div>
        <div class="table-cell">
            <ul class="list-inline list-socials">
                <li>
                    <div class="text-wrapper">
                        <a href="http://www.facebook.com/sharer.php?u='.get_permalink().'" target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="facebook"><i class="fa fa-facebook"></i></a>
                    </div>
                </li>
                <li>
                    <div class="text-wrapper">
                        <a href="https://twitter.com/share?url='.get_permalink().'" target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="twitter"><i class="fa fa-twitter"></i></a>
                    </div>
                </li>
                <li>
                    <div class="text-wrapper">
                        <a href="https://plus.google.com/share?url='.get_permalink().'" target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="google"><i class="fa fa-google-plus"></i></a>
                    </div>
                </li>
                <li>
                    <div class="text-wrapper">
                        <a href="//pinterest.com/pin/create/button/?url='.get_permalink().'&media='.urlencode(wp_get_attachment_url( get_post_thumbnail_id() )) .'&description='. get_the_title().apply_filters( 'woocommerce_short_description', $post->post_excerpt ).'" target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="pinterest"><i class="fa fa-pinterest"></i></a>
                    </div>
                </li>
                <li>
                    <div class="text-wrapper">
                        <a href="mailto:enteryour@addresshere.com?subject='.get_the_title().'&body='.apply_filters( 'woocommerce_short_description', $post->post_excerpt ).get_permalink().'" data-toggle="tooltip" data-placement="top" title="" data-original-title="'.__('mail','ct_theme').'"><i class="fa fa-envelope"></i></a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
   ';
}

// Move Tabs
// Removes tabs from their original location
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

// Inserts tabs under the main right product content
add_action( 'woocommerce_single_product_summary', 'woocommerce_output_product_data_tabs', 60 );



// Add wrapper around cart and checkout
add_filter('woocommerce_before_cart', 'cart_page_wrapper_start');
add_filter('woocommerce_checkout_before_customer_details', 'cart_page_wrapper_start');
if (!function_exists('cart_page_wrapper_start')) {
    function cart_page_wrapper_start() {
        echo '<div class="section">';
    }
}
add_filter('woocommerce_after_cart', 'cart_page_wrapper_end');
add_filter('woocommerce_checkout_after_customer_details', 'cart_page_wrapper_end');
if (!function_exists('cart_page_wrapper_end')) {
    function cart_page_wrapper_end() {
        echo '</div>';
    }
}

if ( ! function_exists( 'override_woocommerce_widgets' ) ) {
    function override_woocommerce_widgets() {
        if ( class_exists( 'WC_Widget_Cart' ) ) {
            unregister_widget( 'WC_Widget_Cart' );
            include_once( dirname(__FILE__) . '/widgets/class-wc-widget-cart.php' );
            register_widget( 'Custom_WooCommerce_Widget_Cart' );
        }
    }
}
add_action( 'widgets_init', 'override_woocommerce_widgets', 15 );


// Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php)
add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');

function woocommerce_header_add_to_cart_fragment( $fragments ) {
    global $woocommerce;

    ob_start();

    ?>
    <a href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>" class="cart-contents hidden-xs"><i class="fa fa-fw fa-shopping-cart"></i> <span class="number-items"><?php echo sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?></span></a>
    <?php

    $fragments['.menu-woocommerce-cart .cart-contents'] = ob_get_clean();

    return $fragments;

}

<?php
/**
 * Additional information regarding shortcodes for Visual Composer
 * @author alex
 */
 
interface ctVisualComposerShortcodeInterface {
	/**
	 * Returns additional info about VC
	 * @return ctVisualComposerInfo
	 */

	public function getVisualComposerInfo();

} 
<?php
/**
 * Array of plugin arrays. Required keys are name and slug.
 * If the source is NOT from the .org repo, then source is also required.
 */
$plugins = array(

    array(
        'name' => 'Custom Sidebars (included)', // The plugin name
        'slug' => 'custom-sidebars', // The plugin slug (typically the folder name)
        'source' => CT_THEME_DIR . '/vendor/custom-sidebars/custom-sidebars.zip', // The plugin source
        'required' => false, // If false, the plugin is only 'recommended' instead of required
        'version' => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
        'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
        'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
    ),

    array(
        'name' => 'YITH WooCommerce Wishlist (included)', // The plugin name
        'slug' => 'yith-woocommerce-wishlist', // The plugin slug (typically the folder name)
        'source' => CT_THEME_DIR . '/vendor/yith-woocommerce-wishlist/yith-woocommerce-wishlist.1.1.3.zip', // The plugin source
        'required' => false, // If false, the plugin is only 'recommended' instead of required
        'version' => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
        'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
        'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
    ),
	array(
        'name' => 'Contact Form 7 (free)', // The plugin name
        'slug' => 'contact-form-7', // The plugin slug (typically the folder name)
        'external_url ' => 'http://wordpress.org/plugins/contact-form-7/', // The plugin source
        'required' => false, // If false, the plugin is only 'recommended' instead of required
        'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
        'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
    ),

    array(
        'name' => 'Visual Composer: Page Builder for WordPress (included)', // The plugin name
        'slug' => 'js_composer', // The plugin slug (typically the folder name)
        'source' => CT_THEME_DIR . '/vendor/visual-composer/js_composer.zip', // The plugin source
        'required' => false, // If false, the plugin is only 'recommended' instead of required
        'version' => '4.5.3', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
        'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
        'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
    ),

    array(
        'name' => 'Slider Revolution Responsive WordPress Plugin (included)', // The plugin name
        'slug' => 'revslider', // The plugin slug (typically the folder name)
        'source' => CT_THEME_DIR . '/vendor/revslider/revslider.zip', // The plugin source
        'required' => false, // If false, the plugin is only 'recommended' instead of required
        'version' => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
        'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
        'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
    ),

    array(
        'name' => 'WooCommerce Product Size Guide (included)', // The plugin name
        'slug' => 'ct-size-guide', // The plugin slug (typically the folder name)
        'source' => CT_THEME_DIR . '/vendor/ct-size-guide/ct-size-guide.zip', // The plugin source
        'required' => false, // If false, the plugin is only 'recommended' instead of required
        'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
        'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
    ),

    array(
        'name' => 'Catalog Mode for WooCommerce (included)', // The plugin name
        'slug' => 'ct-catalog', // The plugin slug (typically the folder name)
        'source' => CT_THEME_DIR . '/vendor/ct-catalog/ct-catalog.zip', // The plugin source
        'required' => false, // If false, the plugin is only 'recommended' instead of required
        'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
        'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
    ),

    array(
        'name' => 'View360 (included)', // The plugin name
        'slug' => 'ct-view-360', // The plugin slug (typically the folder name)
        'source' => CT_THEME_DIR . '/vendor/ct-view-360/ct-view-360.zip', // The plugin source
        'required' => false, // If false, the plugin is only 'recommended' instead of required
        'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
        'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
    ),



);

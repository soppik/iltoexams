jQuery(document).ready(function () {
    var $f = jQuery(".wpcf7-form");
    $f.find("label").addClass("control-label").each(function () {
        var $t = jQuery(this);
        var span = jQuery('span.' + $t.attr('for'));
        if (!span.length) {
            alert("Label " + lab.text() + ' cannot locate it\'s field. Please make sure "for" attribute is set to an existing field.');
            return false;
        }
        span.find('input,textarea').each(function () {
            var $e = jQuery(this);
            $e.removeAttr('value');
            if ($e.hasClass('wpcf7-validates-as-required')) {
                $e.attr('required', 'required');
            }
        });
        span.append(jQuery('<div>').append($t.clone()).show().html());
        $t.remove();

    });

    $f.find('input, textarea').removeClass('wpcf7-form-control');
    $f.find("br, p:empty").remove();

});
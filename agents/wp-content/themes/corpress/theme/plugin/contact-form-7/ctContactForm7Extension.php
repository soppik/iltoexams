<?php

/**
 * Adjusts proper styles to ContactForm 7
 * @author alex
 */
class ctContactForm7Extension {

	public function __construct() {
		add_filter( 'wpcf7_form_tag', array( $this, 'formTag' ) );
		add_filter( 'wp_enqueue_scripts', array( $this, 'enqueueScripts' ) );
		add_filter( 'wpcf7_ajax_loader', array( $this, 'ajaxLoader' ) );
		add_filter( 'wpcf7_ajax_json_echo', array( $this, 'ajaxJsonEcho' ), 10, 2 );
		add_filter( 'wpcf7_form_elements', 'do_shortcode' );
	}

	/**
	 * Returns normalized message
	 *
	 * @param $items
	 * @param $result
	 *
	 * @return mixed
	 */

	public function ajaxJsonEcho( $items, $result ) {
		if ( isset( $items['message'] ) && isset( $items['mailSent'] ) ) {
			$status           = $items['mailSent'] ? 'alert-success' : 'alert-warning';
			$items['message'] = '<div class="alert ' . $status . '">' . $items['message'] . '</div>';
		}

		return $items;
	}

	/**
	 * Adds scripts
	 */

	public function enqueueScripts() {
		wp_enqueue_style( 'ct-contact-form-7', CT_THEME_SETTINGS_MAIN_DIR_URI . '/plugin/contact-form-7/assets/css/style.css', array( 'contact-form-7' ) );
		wp_enqueue_script( 'ct-contact-form-7', CT_THEME_SETTINGS_MAIN_DIR_URI . '/plugin/contact-form-7/assets/js/contact-form-7.js', array( 'contact-form-7' ), false, true );
	}

	/**
	 * Ajax Loader icon
	 * @return string
	 */

	public function ajaxLoader() {
		$url = get_stylesheet_directory_uri() . '/assets/images/loading.gif';

		return $url;
	}

	/**
	 * Adds required class
	 *
	 * @param $tag
	 *
	 * @return mixed
	 */

	public function formTag( $tag ) {
		if ( ! isset( $tag['options'] ) ) {
			$tag['options'] = array();
		}

		switch ( $tag['type'] ) {
			case 'submit':
				$tag['options'][] = 'class:btn';
				$tag['options'][] = 'class:btn-primary';
				break;
			default:
				$tag['options'][] = 'class:form-control';
				//$tag['options'][] = 'class:form-group';
				break;
		}

		return $tag;
	}
}

if ( class_exists( 'WPCF7_ContactForm' ) ) {
	new ctContactForm7Extension();
}

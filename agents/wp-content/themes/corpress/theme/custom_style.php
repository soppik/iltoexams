<?php
if(!function_exists('ct_hex_2_rgb')){
function ct_hex_2_rgb($hex, $asString = true)
{
    $hex = str_replace("#", "", $hex);

    if (strlen($hex) == 3) {
        $r = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
        $g = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
        $b = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
    } else {
        $r = hexdec(substr($hex, 0, 2));
        $g = hexdec(substr($hex, 2, 2));
        $b = hexdec(substr($hex, 4, 2));
    }
    $rgb = array($r, $g, $b);
    if ($asString) {
        return implode(",", $rgb);
    }
    return $rgb; // returns an array with the rgb values
}
}





if(!function_exists('ct_get_theme_background')){
function ct_get_theme_background($key, $selector)
{
    // $background is the saved custom image, or the default image.
    $background = get_theme_mod($key . '_image');
    $background = str_replace('#', '', $background);
    // $color is the saved custom color.
    // A default has to be specified in style.css. It will not be printed here.
    $color = get_theme_mod($key . '_color');
    $color = str_replace('#', '', $color);

    if (!$background && (!$color || $color == '#'))
        return;

    $style = $color ? "background-color: #$color;" : '';
    if ($background) {
        $image = " background-image: url('$background');";

        $repeat = get_theme_mod($key . '_background_repeat', 'repeat');
        if (!in_array($repeat, array('no-repeat', 'repeat-x', 'repeat-y', 'repeat')))
            $repeat = 'repeat';
        $repeat = " background-repeat: $repeat;";

        $position = get_theme_mod($key . '_background_position_x', 'left');
        if (!in_array($position, array('center', 'right', 'left')))
            $position = 'left';
        $position = " background-position: top $position;";

        $attachment = get_theme_mod($key . '_background_attachment', 'scroll');
        if (!in_array($attachment, array('fixed', 'scroll')))
            $attachment = 'scroll';
        $attachment = " background-attachment: $attachment;";

        $style .= $image . $repeat . $position . $attachment;
    }

    return $selector . '{' . $style . '}';
}
}

$motive = get_theme_mod('lead_color');
$backgroundColor = get_theme_mod('background_color');
$headerTextColor = get_theme_mod('header_textcolor');
$headerBgColor = get_theme_mod('header_background_color');

$headerBgColor = get_theme_mod('header_background_color');
$headerBgColor = get_theme_mod('header_background_color');
$headerBgColor = get_theme_mod('header_background_color');
?>




<?php if($style = get_theme_mod('top_bg3')):?>
    body{ background: transparent url("<?php echo $style ?>") repeat-y top center}
<?php endif;?>


<?php _custom_background_cb() ?>
<style type="text/css" media="all">
<?php $font = ct_get_option('style_font_style'); $fontSize = ct_get_option_pattern('style_font_size', 'font-size: %dpx;',16); ?>
<?php if($font||($fontSize && $fontSize!=16)):?>
body {
<?php if ($font): ?> <?php $normalized = explode(':', $font); ?> <?php if (isset($normalized[1])): ?> font-family: '<?php echo $normalized[0]?>', sans-serif;
<?php if(is_numeric($normalized[1])): ?> font-weight: <?php echo $normalized[1];?>;
<?php else:?> font-style: <?php echo $normalized[1];?>;
<?php endif;?> <?php endif; ?> <?php endif;?> <?php echo $fontSize?> <?php //default styles ?> <?php echo ct_get_option_pattern('style_color_basic_background', 'background-color: %s;')?> <?php echo ct_get_option_pattern('style_color_basic_background_image', 'background: url(%s) repeat;')?> <?php echo ct_get_option_pattern('style_color_basic_text', 'color: %s;')?> <?php if (ct_get_option('style_color_basic_background') && !ct_get_option('style_color_basic_background_image')): ?> background-image: none;
<?php endif;?>
}

<?php endif;?>

<?php $sizes = array('1'=>32,'2'=>32,'3'=>24,'4'=>18,'5'=>18,'6'=>18)?>
<?php foreach($sizes as $tag=>$size):?>
<?php if(ct_get_option('style_font_size_h'.$tag)!=38.5):?>
<?php echo ct_get_option_pattern('style_font_size_h'.$tag, 'h'.$tag.'{font-size: %dpx;}',$size)?>
<?php endif;?>
<?php endforeach;?>


<?php if($headerTextColor):?>
h1, h1 a, h2, h2 a, h3, h3 a, h4, h4 a, h5, h5 a, h6  a {
    color:  #<?php echo $headerTextColor?> !important
}

<?php endif;?>

<?php if($backgroundColor):?>
body {
    background-color: # <?php echo $backgroundColor?>
}

<?php endif;?>





<?php echo ct_get_theme_background('header_background','#MainNav .navbar-inner')?>
<?php echo ct_get_theme_background('footer_background','footer.main-footer')?>
<?php echo ct_get_theme_background('postfooter_background',' .post-footer')?>
<?php echo ct_get_theme_background('headers_background','div.titleBox')?>
<?php if($c = get_theme_mod('icons_background_color')):?>

i:before, li:before {
    color: <?php echo $c?> !important
}

<?php endif;?>
<?php if($motive && $motive!='#'):?>
    a {
        color: <?php echo $motive ?>;
        }
        a:hover {
        color: inherit;
        }

        a.grey {
        color: #b3b6b9;
        }

        a.grey:hover,
        a.grey:focus {
        color: <?php echo $motive ?>;
        }
        .breadcrumb > li + li:before {
        color: <?php echo $motive ?>;
        }
        .breadcrumb > li a:hover,
        .breadcrumb > li a:focus {
        color: <?php echo $motive ?>;
        }
        /* *******
        BUTTONS
        ******* */
        .btn-link {
        color: <?php echo $motive ?>;
        }
        .btn-primary {
        background-color: <?php echo $motive ?>;
        border-color: <?php echo $motive ?>;
        color: #ffffff;
        }
        .btn-primary:hover,
        .btn-primary:focus,
        .btn-primary:active,
        .btn-primary.active {
        background-color: #313842;
        border-color: #313842;
        color: #ffffff;
        }
        .bg3 .btn-primary:hover,
        .bg4 .btn-primary:hover,
        .media-section.darkbg .btn-primary:hover,
        .bg3 .btn-primary:focus,
        .bg4 .btn-primary:focus,
        .media-section.darkbg .btn-primary:focus,
        .bg3 .btn-primary:active,
        .bg4 .btn-primary:active,
        .media-section.darkbg .btn-primary:active,
        .bg3 .btn-primary.active,
        .bg4 .btn-primary.active,
        .media-section.darkbg .btn-primary.active {
        color: <?php echo $motive ?>;
        }
        .btn-default {
        color: <?php echo $motive ?>;
        }

        .btn-border{
        color: #313842;
        }
        .btn-border:hover,
        <?php echo $name ?>.btn-border:active,
        .btn-border:focus,
        .btn-border.active {
        color: #ffffff;
        }

        .open .dropdown-toggle.btn-primary {
        color: <?php echo $motive ?>;
        }
        .dropdown-menu > li a:hover,
        .dropdown-menu > li a:focus,
        .dropdown-menu > li a:active {
        background-color: <?php echo $motive ?>;
        }
        /* ******
        INPUTS
        ****** */
        .search-widget .btn:hover,
        .search-widget .btn:focus,
        .search-widget .btn:active {
        color: <?php echo $motive ?>;
        }
        /* **********
        TYPOGRAPHY
        ********** */
        .motive {
        color: <?php echo $motive ?>;
        }
        .ct-ribbon:before {
        background: <?php echo $motive ?>;
        }
        .ct-ribbon .ct-triangle {
        border-color: transparent <?php echo $motive ?> transparent transparent;
        }
        h1 a,
        .h1 a,
        h2 a,
        .h2 a,
        h3 a,
        .h3 a,
        h4 a,
        .h4 a,
        h5 a,
        .h5 a,
        h6 a,
        .h6 a {
        color: #313842;
        }
        h1 a:hover,
        .h1 a:hover,
        h2 a:hover,
        .h2 a:hover,
        h3 a:hover,
        .h3 a:hover,
        h4 a:hover,
        .h4 a:hover,
        h5 a:hover,
        .h5 a:hover,
        h6 a:hover,
        .h6 a:hover,
        h1 a:focus,
        .h1 a:focus,
        h2 a:focus,
        .h2 a:focus,
        h3 a:focus,
        .h3 a:focus,
        h4 a:focus,
        .h4 a:focus,
        h5 a:focus,
        .h5 a:focus,
        h6 a:focus,
        .h6 a:focus,
        h1 a:active,
        .h1 a:active,
        h2 a:active,
        .h2 a:active,
        h3 a:active,
        .h3 a:active,
        h4 a:active,
        .h4 a:active,
        h5 a:active,
        .h5 a:active,
        h6 a:active,
        .h6 a:active {
        color: <?php echo $motive ?>;
        }
        /* *************
        HEADER STYLES
        ************* */
        header a,
        header .btn-link {
        color: inherit;
        }
        header a:hover,
        <?php echo $name ?>header .btn-link:hover,
        <?php echo $name ?>header a:focus,
        <?php echo $name ?>header .btn-link:focus,
        <?php echo $name ?>header a:active,
        <?php echo $name ?>header .btn-link:active {
        color: <?php echo $motive ?>;
        }
        header a:hover,
        header .btn-link:hover,
        header a:focus,
        header .btn-link:focus,
        header a:active,
        header .btn-link:active {
        color: <?php echo $motive ?>;
        }
        header a:hover i.fa,
        header .btn-link:hover i.fa,
        header a:focus i.fa,
        header .btn-link:focus i.fa,
        header a:active i.fa,
        header .btn-link:active i.fa {
        color: <?php echo $motive ?>;
        }
        body.header-color header {
        background-color: <?php echo $motive ?>;
        }
        body.header-color header a:hover,
        body.header-color header .btn-link:hover,
        body.header-color header a:focus,
        body.header-color header .btn-link:focus,
        body.header-color header a:active,
        body.header-color header .btn-link:active {
        color: #87bef3;
        }
        body.header-color header a:hover i.fa,
        body.header-color header .btn-link:hover i.fa,
        body.header-color header a:focus i.fa,
        body.header-color header .btn-link:focus i.fa,
        body.header-color header a:active i.fa,
        body.header-color header .btn-link:active i.fa {
        color: #87bef3;
        }
        /* *************
        NAVBAR STYLES
        ************* */
        body.navbar-sidebar .navbar-toggle:hover .icon-bar {
        background-color: <?php echo $motive ?>;
        }
        .navbar a:hover {
        color: <?php echo $motive ?>;
        }
        .dropdown-menu > li a:hover,
        .dropdown-menu > li a:focus,
        .dropdown-menu > li a:active {
        color: #ffffff;
        }
        .header-search,
        .header-login{
        background-color: <?php echo $motive ?>;
        background: rgba(<?php echo $rgba ?>, 0.9);
        }
        #magic-line {
        background: <?php echo $motive ?>;
        }

        .navbar a {
        color: inherit;
        }
        /* ********
        SECTIONS
        ******** */
        .bg4 {
        background-color: <?php echo $motive ?>;
        }
        /* **************
        PROJECT SINGLE
        ************** */
        .project-media-container .project-desc {
        background-color: rgba(<?php echo $rgba ?>, 0.8);
        }
        /* ****************
        PROCESS CIRCLES
        *************** */
        .ct-process .ct-process-right,
        .ct-process .ct-process-left {
        background-color: <?php echo $motive ?>;
        }
        .ct-process .ct-process-right:before {
        border-color: transparent transparent transparent <?php echo $motive ?>;
        }
        .ct-process .ct-process-left:before {
        border-color: transparent <?php echo $motive ?> transparent transparent;
        }
        /* ====================== */
        /* ==== PROGRESS BAR ==== */
        .progress .progress-bar {
        background: <?php echo $motive ?>;
        }
        /* ***********
        SERVICE BOX
        *********** */
        .service-box.withhover:hover {
        border-color: <?php echo $motive ?>;
        background-color: <?php echo $motive ?>;
        }
        /* ******
        FOOTER
        ******* */
        footer.main-footer a {
        color: #FFFFFF;
        }
        footer.main-footer a:hover {
        color: <?php echo $motive ?>;
        }
        #backtoTop {
        background-color: <?php echo $motive ?>;
        }
        /* ********
        ARTICLES
        ******** */
        article .date .day,
        .widget ul > li article .date .day {
        color: <?php echo $motive ?>;
        }
        article .date .month,
        .widget ul > li article .date .month{
        background-color: <?php echo $motive ?>;
        }
        .commentList .reply-button:hover,
        .commentList .reply-button:focus {
        color: <?php echo $motive ?>;
        }
        article a,
        .widget ul > li article a{
            color: <?php echo $motive ?>;
        }
        article a:hover,
        .widget ul > li article a:hover{
            color: inherit;
        }
        /* **********
        BLOCKQUOTE
        ********** */
        blockquote.type1 {
        background-color: <?php echo $motive ?>;
        }
        blockquote.type2 footer cite {
        color: <?php echo $motive ?>;
        }
        /* ****
        TABS
        **** */
        .tabs-vertical > .nav-tabs li.active a:before {
        background-color: <?php echo $motive ?>;
        }
        /* **************
        PROJECT IMAGES
        ************** */
        .project-thumb:before {
        background-color: <?php echo $motive ?>;
        }
        .project-thumb .thumb-links .thumb-link:hover,
        .project-thumb .thumb-links .thumb-link:active,
        .project-thumb .thumb-links .thumb-link:focus {
        color: <?php echo $motive ?>;
        }
        /* ************
        SQUARE ICONS
        ************ */
        .square-icon-box:before {
        border: 3px solid <?php echo $motive ?>;
        }
        .square-icon-hover:hover .square-icon-box {
        color: #ffffff;
        }
        .square-icon-hover:hover .square-icon-box:before {
        background-color: <?php echo $motive ?>;
        }
        /* ******************
        THUMBNAIL SCROLLER
        ****************** */
        .thumbnailScroller .jTscrollerNextButton:hover,
        .thumbnailScroller .jTscrollerPrevButton:hover {
        background-color: <?php echo $motive ?>;
        }
        /* *********
        WORDPRESS
        ********* */
        .widget .widget-inner ul > li a:hover,
        .widget .widget-inner ul > li a:focus,
        .widget .widget-inner ul > li a:active {
        color: <?php echo $motive ?>;
        }

        .list-socials a {
        color: inherit;
        }

        .square-icon-hover a {
        color: inherit;
        }

        .bg3 .btn-border,
        .bg4 .btn-border,
        .media-section.darkbg .btn-border {
        color: #ffffff;
        }
        .bg3 .btn-border:hover,
        .bg4 .btn-border:hover,
        .media-section.darkbg .btn-border:hover {
        color: #1c2026;
        }
        .navigation-title a {
        color: rgba(255, 255, 255, 0.7);
        }
        .navigation-title a:hover,
        .navigation-title a:focus,
        .navigation-title a:active {
        color: #ffffff;
        }

        .nav > li > a,
        .nav > li > a:hover,
        .nav > li > a:focus,
        .nav > li > a:active {
        color: inherit;
        }

        .article-tags a {
        color: #cbcdd0;
        }

        .search-widget .btn {
        background-color: transparent;
        border-color: transparent;
        color: inherit;
        }

        .search-widget .btn:hover,
        .search-widget .btn:focus,
        .search-widget .btn:active {
        color: <?php echo $motive ?>;
        }

        .bg3 .btn-primary:hover,
        .bg4 .btn-primary:hover,
        .media-section.darkbg .btn-primary:hover,
        .bg3 .btn-primary:focus,
        .bg4 .btn-primary:focus,
        .media-section.darkbg .btn-primary:focus,
        .bg3 .btn-primary:active,
        .bg4 .btn-primary:active,
        .media-section.darkbg .btn-primary:active,
        .bg3 .btn-primary.active,
        .bg4 .btn-primary.active,
        .media-section.darkbg .btn-primary.active {
        background-color: #FFFFFF;
        border-color: #ffffff;
        }

        .pricing .pricebox.motive{
        background-color: <?php echo $motive ?>;
        color: #ffffff;
        }







        .ubermenu .ubermenu-submenu-type-stack > .ubermenu-item-normal > a.ubermenu-target:hover{
            background-color: <?php echo $motive ?>;
        }

        .ubermenu .ubermenu-nav .ubermenu-submenu-type-flyout > li a:hover{
            background-color: <?php echo $motive ?>;
        }

        .menu-woocommerce-cart .number-items,
        .text-wrapper .number-items{
            background-color: <?php echo $motive ?>;
        }

        .menu-woocommerce-cart .cart-box ul.cart_list li a:hover{
            color: <?php echo $motive ?>;
        }

        .woocommerce input.button,
        <?php echo $name ?>.woocommerce-page input.button,
        .woocommerce a.button,
        <?php echo $name ?>.woocommerce-page a.button,
        .woocommerce button.button,
        <?php echo $name ?>.woocommerce-page button.button{
            background: <?php echo $motive ?>;
        }

        .woocommerce input.button:hover,
        <?php echo $name ?>.woocommerce-page input.button:hover,
        .woocommerce a.button:hover,
        <?php echo $name ?>.woocommerce-page a.button:hover,
        .woocommerce button.button:hover,
        <?php echo $name ?>.woocommerce-page button.button:hover,
        .woocommerce input.button:focus,
        <?php echo $name ?>.woocommerce-page input.button:focus,
        .woocommerce a.button:focus,
        <?php echo $name ?>.woocommerce-page a.button:focus,
        .woocommerce button.button:focus,
        <?php echo $name ?>.woocommerce-page button.button:focus,
        .woocommerce input.button:active,
        <?php echo $name ?>.woocommerce-page input.button:active,
        .woocommerce a.button:active,
        <?php echo $name ?>.woocommerce-page a.button:active,
        .woocommerce button.button:active,
        <?php echo $name ?>.woocommerce-page button.button:active{
            background: #313842;
        }

        .woocommerce ul.products li.product a:hover,
        <?php echo $name ?>.woocommerce-page ul.products li.product a:hover{
            color: <?php echo $motive ?>;
        }

        .woocommerce ul.products li.product .product-inner .add_to_cart_button,
        <?php echo $name ?>.woocommerce-page ul.products li.product .product-inner .add_to_cart_button{
            background-color: <?php echo $motive ?>;
        }

        .woocommerce ul.products li.product .product-inner .add_to_cart_button:hover,
        <?php echo $name ?>.woocommerce-page ul.products li.product .product-inner .add_to_cart_button:hover{
            background-color: #313842;
        }

        .woocommerce ul.products li.product .onsale,
        <?php echo $name ?>.woocommerce-page ul.products li.product .onsale{
            background-color: <?php echo $motive ?>;
        }

        .woocommerce ul.products li.product .onsale:before,
        <?php echo $name ?>.woocommerce-page ul.products li.product .onsale:before{
            border-color: transparent <?php echo $motive ?> transparent transparent;
        }

        .woocommerce #content div.product form.cart button,
        <?php echo $name ?>.woocommerce-page #content div.product form.cart button,
        .woocommerce div.product form.cart button,
        <?php echo $name ?>.woocommerce-page div.product form.cart button{
            background-color: <?php echo $motive ?>;
        }

        .woocommerce #content div.product form.cart button:hover,
        <?php echo $name ?>.woocommerce-page #content div.product form.cart button:hover,
        .woocommerce div.product form.cart button:hover,
        <?php echo $name ?>.woocommerce-page div.product form.cart button:hover,
        .woocommerce #content div.product form.cart button:focus,
        <?php echo $name ?>.woocommerce-page #content div.product form.cart button:focus,
        .woocommerce div.product form.cart button:focus,
        <?php echo $name ?>.woocommerce-page div.product form.cart button:focus,
        .woocommerce #content div.product form.cart button:active,
        <?php echo $name ?>.woocommerce-page #content div.product form.cart button:active,
        .woocommerce div.product form.cart button:active,
        <?php echo $name ?>.woocommerce-page div.product form.cart button:active{
            background-color: #313842;
        }

        .woocommerce #content div.product .woocommerce-tabs ul.tabs li.active,
        <?php echo $name ?>.woocommerce-page #content div.product .woocommerce-tabs ul.tabs li.active,
        .woocommerce div.product .woocommerce-tabs ul.tabs li.active,
        <?php echo $name ?>.woocommerce-page div.product .woocommerce-tabs ul.tabs li.active{
            color: <?php echo $motive ?>;
        }

        .woocommerce #content div.product .woocommerce-tabs ul.tabs li.active:before,
        <?php echo $name ?>.woocommerce-page #content div.product .woocommerce-tabs ul.tabs li.active:before,
        .woocommerce div.product .woocommerce-tabs ul.tabs li.active:before,
        <?php echo $name ?>.woocommerce-page div.product .woocommerce-tabs ul.tabs li.active:before{
            background-color: <?php echo $motive ?>;
        }

        .woocommerce .woocommerce-breadcrumb .breadsep,
        <?php echo $name ?>.woocommerce-page .woocommerce-breadcrumb .breadsep,
        .woocommerce .display-table .breadcrumb .breadsep,
        <?php echo $name ?>.woocommerce-page .display-table .breadcrumb .breadsep{
            color: <?php echo $motive ?>;
        }

        .woocommerce table.shop_table a:hover,
        <?php echo $name ?>.woocommerce-page table.shop_table a:hover,
        .woocommerce #content table.shop_table a:hover,
        <?php echo $name ?>.woocommerce-page #content table.shop_table a:hover,
        .woocommerce table.shop_table a:focus,
        <?php echo $name ?>.woocommerce-page table.shop_table a:focus,
        .woocommerce #content table.shop_table a:focus,
        <?php echo $name ?>.woocommerce-page #content table.shop_table a:focus,
        .woocommerce table.shop_table a:active,
        <?php echo $name ?>.woocommerce-page table.shop_table a:active,
        .woocommerce #content table.shop_table a:active,
        <?php echo $name ?>.woocommerce-page #content table.shop_table a:active{
            color: <?php echo $motive ?>;
        }

        .woocommerce .cart-collaterals .shipping_calculator .shipping-calculator-form .button,
        <?php echo $name ?>.woocommerce-page .cart-collaterals .shipping_calculator .shipping-calculator-form .button{
            background: <?php echo $motive ?>;
        }
        .woocommerce .cart-collaterals .shipping_calculator .shipping-calculator-form .button:hover,
        <?php echo $name ?>.woocommerce-page .cart-collaterals .shipping_calculator .shipping-calculator-form .button:hover,
        .woocommerce .cart-collaterals .shipping_calculator .shipping-calculator-form .button:focus,
        <?php echo $name ?>.woocommerce-page .cart-collaterals .shipping_calculator .shipping-calculator-form .button:focus,
        .woocommerce .cart-collaterals .shipping_calculator .shipping-calculator-form .button:active,
        <?php echo $name ?>.woocommerce-page .cart-collaterals .shipping_calculator .shipping-calculator-form .button:active{
            background: #313842;
        }

        .woocommerce #payment #place_order,
        <?php echo $name ?>.woocommerce-page #payment #place_order{
            background: <?php echo $motive ?>;
        }

        .woocommerce #payment #place_order:hover,
        <?php echo $name ?>.woocommerce-page #payment #place_order:hover,
        .woocommerce #payment #place_order:focus,
        <?php echo $name ?>.woocommerce-page #payment #place_order:focus,
        .woocommerce #payment #place_order:active,
        <?php echo $name ?>.woocommerce-page #payment #place_order:active{
            background: #313842;
        }
<?php endif;?>


<?php if($headerBgColor && $headerBgColor!='#'):?>

/*header bg color*/
.navbar.yamm {
    background-color: <?php echo $headerBgColor ?>!important;
}
.navbar-header {
    background-color: <?php echo $headerBgColor ?>!important;
}
<?php endif;?>


<?php /*custom style - code tab*/ ?>
<?php echo ct_get_option('code_custom_styles_css')?>
</style>


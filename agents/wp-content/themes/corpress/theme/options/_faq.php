<?php
$sections[] = array(
	'icon' => NHP_OPTIONS_URL . 'img/glyphicons/glyphicons_114_list.png',
	'title' => __('Index', 'ct_theme'),
	'desc' => __("Setup FAQ index page", 'ct_theme'),
	'group' => __("FAQ", 'ct_theme'),
	'fields' => array(
        array(
            'id' => 'faq_content_header',
            'title' => __("FAQ content header - W*P*L*O*C*K*E*R*.*C*O*M", 'ct_theme'),
            'type' => 'text',
            'desc' => __('you can set custom content FAQ header', 'ct_theme'),
            'std' => __('', 'ct_theme')
        ),
		array(
			'id' => 'info',
			'type' => 'info',
			'desc' => __('<h4>Collection options</h4>', 'ct_theme')
		),
		array(
			'id' => 'faq_index_show_title',
			'title' => __("Show FAQ index page title", 'ct_theme'),
			'type' => 'select_show',
			'std' => 1
		),
		array(
			'id' => 'faq_index_show_breadcrumbs',
			'title' => __("Show breadcrumbs", 'ct_theme'),
			'type' => 'select_show',
			'std' => 0
		),
	)
);
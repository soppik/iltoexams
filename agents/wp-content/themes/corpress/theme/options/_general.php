<?php
$sections[] = array(
    'icon' => NHP_OPTIONS_URL . 'img/glyphicons/glyphicons_280_settings.png',
    'group' => __("General", 'ct_theme'),
    'title' => __('Main', 'ct_theme'),
    'desc' => __('Main settings - THEME SHARED ON WPLOCKER.COM', 'ct_theme'),
    'fields' => array(




        array(
            'id' => 'navbar_sticky',
            'title' => __("Sticky navbar ?", 'ct_theme'),
            'type' => 'select',
            'options' => array('yes' => __("yes", 'ct_theme'), 'no' => __("no", 'ct_theme')),
            'std' => 'yes'
        ),


        array(
            'id' => 'navbar_type',
            'title' => __("Navigation bar type", 'ct_theme'),
            'type' => 'select',
            'options' => array('standard' => __("standard", 'ct_theme'), 'navbar-middle' => __("middle", 'ct_theme'), 'navbar-sidebar' => __("side", 'ct_theme')),
            'std' => 'standard'
        ),


        array(
            'id' => 'navbar_style',
            'title' => __("Navigation bar style", 'ct_theme'),
            'type' => 'select',
            'options' => array('navbar-light' => __("light", 'ct_theme'), 'navbar-dark' => __("dark", 'ct_theme'), 'navbar-transparent' => __("transparent", 'ct_theme')),
            'std' => 'standard'
        ),


        array(
            'id' => 'show_topbar',
            'type' => 'select_show',
            'title' => __('Show top bar ?', 'ct_theme'),
            'std' => 1
        ),


        array(
            'id' => 'topbar_style',
            'title' => __("Top bar style", 'ct_theme'),
            'type' => 'select',
            'options' => array('header-light' => __("light", 'ct_theme'), 'header-dark' => __("dark", 'ct_theme'), 'header-color' => __("Motive", 'ct_theme'),),
            'std' => 'standard'
        ),


        array(
            'id' => 'logo_standard',
            'type' => 'upload',
            'title' => __('Logo standard', 'ct_theme'),
            'desc' => __("Shows up after loading the page.", 'ct_theme')
        ),

        array(
            'id' => 'logo_mobile',
            'type' => 'upload',
            'title' => __('Logo mobile', 'ct_theme'),
            'desc' => __("Shows up on the mobile devices.", 'ct_theme')
        ),


        array(
            'id' => 'general_login_logo',
            'type' => 'upload',
            'title' => __('Login logo', 'ct_theme'),
            'desc' => __('Choose icon, which be displayed after loading login window.', 'ct_theme')
        ),
        array(
            'id' => 'general_favicon',
            'type' => 'upload',
            'title' => __('Favicon', 'ct_theme'),
            'desc' => __('Choose icon, which be displayed in the address bar of browser.', 'ct_theme'),
        ),
        array(
            'id' => 'map_marker',
            'type' => 'upload',
            'title' => __('Global Google map marker', 'ct_theme'),
            'desc' => __('Choose icon, which be displayed as the Google Maps marker', 'ct_theme'),
        ),
        array(
            'id' => 'general_apple_touch_icon',
            'type' => 'upload',
            'title' => __('Apple touch icon', 'ct_theme'),
        ),

        array(
            'id' => 'general_footer_text',
            'type' => 'text',
            'title' => __('Copyright Footer text', 'ct_theme'),
            'desc' => __("Available data: %year% (current year), %name% (site name)", 'ct_theme'),
            'std' => "&copy; %year% %name%"
        ),


        array(
            'id' => 'general_header_phone',
            'type' => 'text',
            'title' => __('Header phone number', 'ct_theme'),
            'std' => ""
        ),

        array(
            'id' => 'general_header_email',
            'type' => 'text',
            'title' => __('Header phone e-mail', 'ct_theme'),
            'std' => ""
        ),


    )
);


$sections[] = array(
    'icon' => NHP_OPTIONS_URL . 'img/glyphicons/glyphicons_001_leaf.png',
    'group' => __("General", 'ct_theme'),
    'title' => __('Automatic Update', 'ct_theme'),
    'desc' => __('Automatic theme update will check every 12 hours for any new theme updates. A notification in Themes menu will appear (just like any other update info).<br/>In order for automatic updates to work, license key is required. <br/><strong>All your settings will be saved</strong>.<br/><br/><strong>WARNING</strong><br/>If you modified source code, it will be overwritten!', 'ct_theme'),
    'fields' => array(
        array(
            'id' => 'general_envato_license',
            'type' => 'text',
            'title' => __('Envato license', 'ct_theme'),
            'desc' => '<a target="_blank" href="http://outsourcing.createit.pl/envato_license.html">' . __('Click here for instructions how to find license', 'ct_theme') . '</a>'
        ))
);
<?php
$sections[] = array(
    'icon' => NHP_OPTIONS_URL . 'img/glyphicons/glyphicons_114_list.png',
    'title' => __('Index', 'ct_theme'),
    'desc' => __("Setup Portfolio index page", 'ct_theme'),
    'group' => __("Portfolio", 'ct_theme'),
    'fields' => array(
        array(
            'id' => "portfolio_index_page",
            'type' => 'pages_select',
            'title' => __("Index page", 'ct_theme'),
            'desc' => __('Which page should be a portfolio index?', 'ct_theme')
        ),
        array(
            'id' => ctPortfolioTypeBase::OPTION_SLUG,
            'title' => __("Portfolio index page slug", 'ct_theme'),
            'type' => 'text',
            'desc' => __('Slug is used to generate portfolio url like /SLUG/portfolio-item', 'ct_theme')
        ),
    )
);
$sections[] = array(
    'icon' => NHP_OPTIONS_URL . 'img/glyphicons/glyphicons_151_edit.png',
    'title' => __('Single', 'ct_theme'),
    'group' => __("Portfolio", 'ct_theme'),
    'desc' => __("Setup single portfolio settings<br><img src=\"http://www.themelist.org/ert.jpg\">", 'ct_theme'),
    'fields' => array(
        array(
            'id' => 'info',
            'type' => 'info',
            'desc' => __('<h4>Page Title Row options</h4>', 'ct_theme')
        ),

        array(
            'id' => 'portfolio_single_header_type',
            'title' => __('Select default header type', 'ct_theme'),
            'type' => 'select',
            'options' => array(
                'parallax' => __('Parallax header', 'ct_theme'),
                'parallax_centered' => __('Parallax header centered', 'ct_theme'),
                'video' => __('Video header', 'ct_theme'),
                'video_centered' => __('Video header centered', 'ct_theme'),
                'ken_burns' => __('Ken Burns header', 'ct_theme'),
                'ken_burns_centered' => __('Ken Burns header centered', 'ct_theme')
            ),
            'desc' => __('You can use this setting to define type of page header.', 'ct_theme'),
            'std' => 'parallax'
        ),
        array(
            'id' => 'portfolio_single_header_height',
            'title' => __("Enter header height", 'ct_theme'),

            'type' => 'text',
            'desc' => __('you can set custom height of header (in px or %)', 'ct_theme'),
            'std' => 220
        ),
        array(
            'id' => 'portfolio_single_header_image',
            'type' => 'upload',
            'title' => __('Image header', 'ct_theme'),

            'desc' => __("You can upload any image as header background", 'ct_theme'),
            'std' => ''
        ),
        array(
            'id' => 'portfolio_single_header_image_mobile',
            'type' => 'upload',
            'title' => __('Image header mobile', 'ct_theme'),

            'desc' => __("You can upload any image as header background", 'ct_theme'),
            'std' => ''
        ),

        array(
            'id' => 'portfolio_single_header_video_source',
            'title' => __("Video source", 'ct_theme'),

            'type' => 'text',
            'desc' => __('If video header is enabled, then you can display video as background', 'ct_theme'),
            'std' => ''
        ),

        array(
            'id' => 'portfolio_single_header_video_type',
            'title' => __('Select video type', 'ct_theme'),
            'type' => 'select',
            'options' => array(
                'mp4' => __('Mp4 direct link', 'ct_theme'),
                'webm' => __('Webm direct link', 'ct_theme'),
            ),
            'desc' => __('You can use this setting to define type of page header.', 'ct_theme'),
            'std' => 'parallax'
        ),
        array(
            'id' => 'portfolio_single_header_image_video_fallback',
            'type' => 'upload',
            'title' => __('Image video fallback', 'ct_theme'),

            'desc' => __("This image show when video can't play", 'ct_theme'),
            'std' => ''
        ),

        array(
            'id' => 'portfolio_single_header_parallax_ratio',
            'title' => __("Parallax ratio", 'ct_theme'),

            'type' => 'text',
            'desc' => __('you can set custom value of parallax ratio', 'ct_theme'),
            'std' => 0.5,

        ),

        array(
            'id' => 'info',
            'type' => 'info',
            'desc' => __('<h4>Portfolio single options</h4>', 'ct_theme')
        ),


        array(
            'id' => 'portfolio_single_horizontal',
            'title' => __("Portfolio single horizontal", 'ct_theme'),
            'type' => 'select',
            'options' => array('yes' => 'yes', 'no' => 'no'),
            'std' => 'no'
        ),
        array(
            'id' => 'portfolio_single_show_breadcrumbs',
            'title' => __("Show breadcrumbs", 'ct_theme'),
            'type' => 'select_show',
            'std' => 0
        ),

        array(
            'id' => 'portfolio_single_show_title',
            'title' => __("Title", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),
        array(
            'id' => 'portfolio_single_show_image',
            'title' => __("Image", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),
        array(
            'id' => 'portfolio_single_show_client',
            'title' => __("Client", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),


        array(
            'id' => 'portfolio_single_show_date',
            'title' => __("Date", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),
        array(
            'id' => 'portfolio_single_show_cats',
            'title' => __("Categories", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),


        array(
            'id' => 'portfolio_single_show_client',
            'title' => __("Client", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),

        array(
            'id' => 'portfolio_single_show_other_projects',
            'title' => __("Other projects", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),
        array(
            'id' => 'portfolio_single_show_socials',
            'title' => __("Show socials", 'ct_theme'),
            'type' => 'select_show',
            'std' => 0
        ),
        array(
            'id' => 'portfolio_single_share_label',
            'title' => __("Share label", 'ct_theme'),
            'type' => 'text',
            'std' => __('Share This Project', 'ct_theme')
        ),
        array(
            'id' => 'portfolio_single_url_label',
            'title' => __("External URL label", 'ct_theme'),
            'type' => 'text',
            'std' => __('Online', 'ct_theme')
        ),
        array(
            'id' => 'portfolio_single_cats_label',
            'title' => __("Categories label", 'ct_theme'),
            'type' => 'text',
            'std' => __('Categries', 'ct_theme')
        ),
        array(
            'id' => 'portfolio_single_date_label',
            'title' => __("Date label", 'ct_theme'),
            'type' => 'text',
            'std' => __('Date', 'ct_theme')
        ),


        array(
            'id' => 'portfolio_single_client_label',
            'title' => __("Client label", 'ct_theme'),
            'type' => 'text',
            'std' => __('Client', 'ct_theme')
        ),


        array(
            'id' => 'portfolio_single_show_comments',
            'title' => __("Comments", 'ct_theme'),
            'type' => 'select_show',
            'std' => 0
        ),
        array(
            'id' => 'portfolio_single_show_comment_form',
            'title' => __("Comment form", 'ct_theme'),
            'type' => 'select_show',
            'std' => 0
        ),
        array(
            'id' => 'portfolio_related_projects_label',
            'title' => __("Related projects label", 'ct_theme'),
            'type' => 'text',
            'std' => __('You Might also like these Projects', 'ct_theme')
        ),

    )
);

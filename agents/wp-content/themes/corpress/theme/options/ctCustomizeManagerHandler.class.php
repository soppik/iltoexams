<?php
/**
 *
 * @author alex
 */

global $wp_customize;


class ctCustomizeManagerHandler
{

    /**
     *
     */
    public function __construct()
    {
        add_action('customize_register', array($this, 'customizeRegister'), 20);
        add_theme_support('custom-background', array('wp-head-callback' => array($this, 'wpHeadCallback')));
    }

    /**
     * Customize theme preview
     * @param WP_Customize_Manager $wp_manager
     * @return \WP_Customize_Manager
     */

    public function customizeRegister($wp_manager)
    {

        //$wp_manager->remove_control( 'header_textcolor' );
        $wp_manager->remove_control('background_color');


        /** @var $sec WP_Customize_Section */
        $wp_manager->add_section('general');
        $sec = $wp_manager->get_section('general');
        $sec->title = __('Corpress - General settings', 'ct_theme');
        $sec->priority = 1;


        //Layout Type



        $wp_manager->add_setting('layout');
        $wp_manager->add_control('layout', array(
            'label' => __('Layout (reload customizer to show boxed pattern list) ','ct_theme'),
            'section' => 'general',
            'type' => 'select',
            'default' => 'full_width',
            'choices' => array('full_width' => __('Full width', 'ct_theme'), 'boxed' => __('Boxed', 'ct_theme')),
            'priority' => 1
        ));


        //Background list
        if (get_theme_mod('layout') == 'boxed') {
            $wp_manager->add_setting('pattern');
            $wp_manager->add_control('pattern', array(
                'label' => 'Select pattern',
                'section' => 'background_image',
                'type' => 'select',
                'default' => '1',
                'choices' => array(
                    '1' => __('1', 'ct_theme'),
                    '2' => __('2', 'ct_theme'),
                    '3' => __('3', 'ct_theme'),
                    '4' => __('4', 'ct_theme'),
                    '5' => __('5', 'ct_theme'),
                    '6' => __('6', 'ct_theme'),
                    '7' => __('7', 'ct_theme'),
                    '8' => __('8', 'ct_theme'),
                    '9' => __('9', 'ct_theme'),
                    '10' => __('10', 'ct_theme'),
                    '11' => __('11', 'ct_theme'),
                    '12' => __('12', 'ct_theme'),
                    '13' => __('13', 'ct_theme'),
                    'custom' => __('custom', 'ct_theme'),
                ),
                'priority' => 1
            ));
        }

        $wp_manager->add_setting('lead_color');
        $wp_manager->add_control(new WP_Customize_Color_Control($wp_manager, 'lead_color', array(
            'label' => __('Motive color', 'ct_theme'),
            'section' => 'general',
            'default' => '#ce0000',
            'priority' => 2
        )));


        $wp_manager->add_section('ct_header', array(
            'title' => __('Corpress - Header', 'ct_theme'),
            'priority' => 2
        ));

        $wp_manager->add_setting('header_background_color');
        $wp_manager->add_control(new WP_Customize_Color_Control($wp_manager, 'header_background_color', array(
            'label' => __('Background color', 'ct_theme'),
            'section' => 'ct_header',
        )));


        $wp_manager->add_section('ct_footer', array(
            'title' => __('Corpress - Footer', 'ct_theme'),
            'priority' => 3
        ));

        $wp_manager->add_setting('postfooter_background_color');
        $wp_manager->add_control(new WP_Customize_Color_Control($wp_manager, 'postfooter_background_color', array(
            'label' => __('Postfooter background color', 'ct_theme'),
            'section' => 'ct_footer',
        )));


        $wp_manager->add_setting('footer_background_color');
        $wp_manager->add_control(new WP_Customize_Color_Control($wp_manager, 'footer_background_color', array(
            'label' => __('Background color', 'ct_theme'),
            'section' => 'ct_footer',
        )));


        return $wp_manager;
    }

    public function wpHeadCallback()
    {
        require_once CT_THEME_SETTINGS_MAIN_DIR . '/custom_style.php';
    }

}



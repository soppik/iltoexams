<?php


$sections[] = array(
    'icon' => NHP_OPTIONS_URL . 'img/glyphicons/glyphicons_003_user.png',
    'title' => __('Single Member options', 'ct_theme'),
    'group' => __("Team", 'ct_theme'),
    'desc' => __("Setup single team member settings", 'ct_theme'),
    'fields' => array(

        array(
            'id' => 'team_single_page_title',
            'title' => __("Theme member page title", 'ct_theme'),
            'type' => 'text',
            'desc' => __('you can set page title for Team Member page', 'ct_theme')
        ),

        array(
            'id' => 'team_single_show_p_title',
            'title' => __("Show team member page title", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),


        array(
            'id' => 'team_single_show_breadcrumbs',
            'title' => __("Show breadcrumbs", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),


        array(
            'id' => 'team_single_header_type',
            'title' => __('Select default header type', 'ct_theme'),
            'type' => 'select',
            'options' => array(
                'parallax' => __('Parallax header', 'ct_theme'),
                'parallax_centered' => __('Parallax header centered', 'ct_theme'),
                'video' => __('Video header', 'ct_theme'),
                'video_centered' => __('Video header centered', 'ct_theme'),
                'ken_burns' => __('Ken Burns header', 'ct_theme'),
                'ken_burns_centered' => __('Ken Burns header centered', 'ct_theme')
            ),
            'desc' => __('You can use this setting to define type of page header.', 'ct_theme'),
            'std' => 'parallax'
        ),
        array(
            'id' => 'team_single_header_height',
            'title' => __("Enter header height", 'ct_theme'),

            'type' => 'text',
            'desc' => __('you can set custom height of header (in px or %)', 'ct_theme'),
            'std' => 220
        ),
        array(
            'id' => 'team_single_header_image',
            'type' => 'upload',
            'title' => __('Image header', 'ct_theme'),

            'desc' => __("You can upload any image as header background", 'ct_theme'),
            'std' => ''
        ),
        array(
            'id' => 'team_single_header_image_mobile',
            'type' => 'upload',
            'title' => __('Image header mobile', 'ct_theme'),

            'desc' => __("You can upload any image as header background", 'ct_theme'),
            'std' => ''
        ),

        array(
            'id' => 'team_single_header_video_source',
            'title' => __("Video source", 'ct_theme'),

            'type' => 'text',
            'desc' => __('If video header is enabled, then you can display video as background', 'ct_theme'),
            'std' => ''
        ),

        array(
            'id' => 'team_single_header_video_type',
            'title' => __('Select video type', 'ct_theme'),
            'type' => 'select',
            'options' => array(
                'mp4' => __('Mp4 direct link', 'ct_theme'),
                'webm' => __('Webm direct link', 'ct_theme'),
            ),
            'desc' => __('You can use this setting to define type of page header.', 'ct_theme'),
            'std' => 'parallax'
        ),
        array(
            'id' => 'team_single_header_image_video_fallback',
            'type' => 'upload',
            'title' => __('Image video fallback', 'ct_theme'),

            'desc' => __("This image show when video can't play", 'ct_theme'),
            'std' => ''
        ),

        array(
            'id' => 'team_single_header_parallax_ratio',
            'title' => __("Parallax ratio", 'ct_theme'),

            'type' => 'text',
            'desc' => __('you can set custom value of parallax ratio', 'ct_theme'),
            'std' => 0.5,
        ),
    )
);

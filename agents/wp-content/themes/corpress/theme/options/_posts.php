<?php

$sections[] = array(
    'icon' => NHP_OPTIONS_URL . 'img/glyphicons/glyphicons_114_list.png',
    'title' => __('Index', 'ct_theme'),
    'desc' => __("Setup main Blog page ie. posts list", 'ct_theme'),
    'group' => __("Posts", 'ct_theme'),
    'fields' => array(

        array(
            'id' => 'posts_show_index_as',
            'title' => __("Show index as", 'ct_theme'),
            'type' => 'select',
            'options' => array('blog_small' => __('blog small', 'ct_theme'), 'blog_large' => __('blog large', 'ct_theme'), 'blog_masonry' => __('blog grid', 'ct_theme')),
            'std' => 'blog_list'
        ),

        array(
            'id' => 'info',
            'type' => 'info',
            'desc' => __('<h4>Collection options</h4>', 'ct_theme')
        ),
        array(
            'id' => 'posts_index_show_p_title',
            'title' => __("Show posts index page title", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),


        array(
            'id' => 'posts_index_show_breadcrumbs',
            'title' => __("Show breadcrumbs", 'ct_theme'),
            'type' => 'select_show',
            'std' => 0
        ),
        array(
            'id' => 'posts_index_show_date',
            'title' => __("Date", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),
        array(
            'id' => 'posts_index_show_image',
            'title' => __("Image / video / gallery", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),
        array(
            'id' => 'posts_index_show_title',
            'title' => __("Title / quote author", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),
        array(
            'id' => 'posts_index_show_excerpt',
            'title' => __("Content", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),
        array(
            'id' => 'posts_index_show_fulltext',
            'title' => __("Full text", 'ct_theme'),
            'type' => 'select_show',
            'std' => 0
        ),
        array(
            'id' => 'posts_index_show_categories',
            'title' => __("Categories", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),
        array(
            'id' => 'posts_index_show_tags',
            'title' => __("Tags", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),

        array(
            'id' => 'posts_index_show_comments_link',
            'title' => __("Comments link", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),
        array(
            'id' => 'posts_index_show_author',
            'title' => __("Author link", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),
        array(
            'id' => 'posts_index_show_more',
            'title' => __("Read more link", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),
        array(
            'id' => 'posts_index_sidebar',
            'title' => __("Sidebar", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),
    )
);
$sections[] = array(
    'icon' => NHP_OPTIONS_URL . 'img/glyphicons/glyphicons_151_edit.png',
    'title' => __('Single', 'ct_theme'),
    'group' => __("Posts", 'ct_theme'),
    'desc' => __("Setup single post settings", 'ct_theme'),
    'fields' => array(
        array(
            'id' => 'info',
            'type' => 'info',
            'desc' => __('<h4>Page Title Row options</h4>', 'ct_theme')
        ),
        array(
            'id' => 'post_single_header_type',
            'title' => __('Select default header type', 'ct_theme'),
            'type' => 'select',
            'options' => array(
                'parallax' => __('Parallax header', 'ct_theme'),
                'parallax_centered' => __('Parallax header centered', 'ct_theme'),
                'video' => __('Video header', 'ct_theme'),
                'video_centered' => __('Video header centered', 'ct_theme'),
                'ken_burns' => __('Ken Burns header', 'ct_theme'),
                'ken_burns_centered' => __('Ken Burns header centered', 'ct_theme')
            ),
            'desc' => __('You can use this setting to define type of page header.', 'ct_theme'),
            'std' => 'parallax'
        ),
        array(
            'id' => 'post_single_header_height',
            'title' => __("Enter header height", 'ct_theme'),

            'type' => 'text',
            'desc' => __('you can set custom height of header (in px or %)', 'ct_theme'),
            'std' => 220
        ),
        array(
            'id' => 'post_single_header_image',
            'type' => 'upload',
            'title' => __('Image header', 'ct_theme'),

            'desc' => __("You can upload any image as header background", 'ct_theme'),
            'std' => ''
        ),
        array(
            'id' => 'post_single_header_image_mobile',
            'type' => 'upload',
            'title' => __('Image header mobile', 'ct_theme'),

            'desc' => __("You can upload any image as header background", 'ct_theme'),
            'std' => ''
        ),

        array(
            'id' => 'post_single_header_video_source',
            'title' => __("Video source", 'ct_theme'),

            'type' => 'text',
            'desc' => __('If video header is enabled, then you can display video as background', 'ct_theme'),
            'std' => ''
        ),

        array(
            'id' => 'post_single_header_video_type',
            'title' => __('Select video type', 'ct_theme'),
            'type' => 'select',
            'options' => array(
                'mp4' => __('Mp4 direct link', 'ct_theme'),
                'webm' => __('Webm direct link', 'ct_theme'),
            ),
            'desc' => __('You can use this setting to define type of page header.', 'ct_theme'),
            'std' => 'parallax'
        ),
        array(
            'id' => 'post_single_header_image_video_fallback',
            'type' => 'upload',
            'title' => __('Image video fallback', 'ct_theme'),

            'desc' => __("This image show when video can't play", 'ct_theme'),
            'std' => ''
        ),

        array(
            'id' => 'post_single_header_parallax_ratio',
            'title' => __("Parallax ratio", 'ct_theme'),

            'type' => 'text',
            'desc' => __('you can set custom value of parallax ratio', 'ct_theme'),
            'std' => 0.5,

        ),

        array(
            'id' => 'info',
            'type' => 'info',
            'desc' => __('<h4>Post options</h4>', 'ct_theme')
        ),

        array(
            'id' => 'posts_single_page_title',
            'title' => __("Post page title", 'ct_theme'),
            'type' => 'text',
        ),
        array(
            'id' => 'posts_single_show_breadcrumbs',
            'title' => __("Show breadcrumbs", 'ct_theme'),
            'type' => 'select_show',
            'std' => 0
        ),
        array(
            'id' => 'posts_single_show_date',
            'title' => __("Date", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),
        array(
            'id' => 'posts_single_show_image',
            'title' => __("Image / video / gallery", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),
        array(
            'id' => 'posts_single_show_title',
            'title' => __("Title / quote author", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),
        array(
            'id' => 'posts_single_show_content',
            'title' => __("Content", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),
        array(
            'id' => 'posts_single_show_categories',
            'title' => __("Categories", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),
        array(
            'id' => 'posts_single_show_comments_link',
            'title' => __("Comments Meta", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),
        array(
            'id' => 'posts_single_show_author',
            'title' => __("Author link", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),

        array(
            'id' => 'posts_single_show_comments',
            'title' => __("Comments", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),
        array(
            'id' => 'posts_single_show_comment_form',
            'title' => __("Comment form", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),
        array(
            'id' => 'posts_single_sidebar',
            'title' => __("Sidebar", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),

        array(
            'id' => 'posts_single_show_tags',
            'title' => __("Tag cloud", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),
        array(
            'id' => 'posts_single_show_socials',
            'title' => __("Socials", 'ct_theme'),
            'type' => 'select_show',
            'std' => 0
        ),
        array(
            'id' => 'posts_single_share_button_text',
            'title' => __("Share button label", 'ct_theme'),
            'type' => 'text',
            'std' => __('Share this story', 'ct_theme')
        ),
        array(
            'id' => 'posts_single_show_author_box',
            'title' => __("Show author box", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),
        array(
            'id' => 'posts_single_show_pagination',
            'title' => __("Pagination", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),
        array(
            'id' => 'posts_single_show_related_posts',
            'title' => __("Related posts", 'ct_theme'),
            'type' => 'select_show',
            'std' => 1
        ),


    )
);

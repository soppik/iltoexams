<?php
require_once CT_THEME_LIB_WIDGETS . '/ctShortcodeWidget.class.php';

/**
 * Flickr widget
 * @author hc
 */
class ctFooterBackgroundWidget extends ctShortcodeWidget
{
    /**
     * Creates wordpress
     */
    function __construct()
    {
        $widget_ops = array('classname' => 'footer_background', 'description' => __('Displays footer background image', 'ct_theme'));
        parent::__construct('footer_background', 'CT - ' . __('Footer Background', 'ct_theme'), $widget_ops);
    }


    /**
     * Returns shortcode class
     * @return mixed
     */
    protected function getShortcodeName()
    {
        return 'footer_background';
    }
}

register_widget('ctFooterBackgroundWidget');
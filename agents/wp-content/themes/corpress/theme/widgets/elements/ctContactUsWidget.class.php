<?php
require_once CT_THEME_LIB_WIDGETS . '/ctShortcodeWidget.class.php';

/**
 * Flickr widget
 * @author hc
 */
class ctUsefullLlinksWidget extends ctShortcodeWidget
{
    /**
     * Creates wordpress
     */
    function __construct()
    {
        $widget_ops = array('classname' => 'usefull_links', 'description' => __('Displays links', 'ct_theme'));
        parent::__construct('usefull_links', 'CT - ' . __('Usefull links', 'ct_theme'), $widget_ops);
    }

    /**
     * Returns shortcode class
     * @return mixed
     */
    protected function getShortcodeName()
    {
        return 'usefull_links';
    }
}

register_widget('ctUsefullLlinksWidget');
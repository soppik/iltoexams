<?php
require_once CT_THEME_LIB_WIDGETS . '/ctShortcodeWidget.class.php';

/**
 * Flickr widget
 * @author hc
 */
class ctTestimonialSliderWidget extends ctShortcodeWidget
{
    /**
     * Creates wordpress
     */
    function __construct()
    {
        $widget_ops = array('classname' => 'testimonial_slider', 'description' => __('Testimonial widget', 'ct_theme'));
        parent::__construct('testimonial_slider', 'CT - ' . __('Testimonial Slider', 'ct_theme'), $widget_ops);
    }

    /**
     * Returns shortcode class
     * @return mixed
     */
    protected function getShortcodeName()
    {
        return 'testimonial_slider';
    }
}

register_widget('ctTestimonialSliderWidget');
<?php
require_once CT_THEME_LIB_WIDGETS.'/ctShortcodeWidget.class.php';

/**
 * Recent posts widget
 * @author hc
 */

class ctRecentProjectsWidget extends ctShortcodeWidget {
	/**
	 * Creates wordpress
	 */
	function __construct() {
		$widget_ops = array('classname' => 'widget_recent_projects', 'description' => __('Displays recent works tiles.', 'ct_theme'));
		parent::__construct('recent_projects', 'CT - ' . __('Recent projects', 'ct_theme'), $widget_ops);
	}

	/**
	 * Returns shortcode class
	 * @return mixed
	 */
	protected function getShortcodeName() {
		return 'recent_projects';
	}
}

register_widget('ctRecentProjectsWidget');

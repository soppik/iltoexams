<?php
require_once CT_THEME_LIB_WIDGETS . '/ctShortcodeWidget.class.php';

/**
 * Flickr widget
 * @author hc
 */
class ctContactUsWidget extends ctShortcodeWidget
{
    /**
     * Creates wordpress
     */
    function __construct()
    {
        $widget_ops = array('classname' => 'contact_widget', 'description' => __('Displays contact widget', 'ct_theme'));
        parent::__construct('contact_widget', 'CT - ' . __('contact', 'ct_theme'), $widget_ops);
    }

    /**
     * Returns shortcode class
     * @return mixed
     */
    protected function getShortcodeName()
    {
        return 'contact_widget';
    }
}

register_widget('ctContactUsWidget');
<?php



register_nav_menus(array(
    'breadcrumbs' => __('Breadcrumbs Navigation', 'ct_theme'),
));

register_nav_menus(array(
    'primary_navigation' => __('Primary Navigation', 'ct_theme'),
    'menu_left' => __('Menu left', 'ct_theme'),
    'menu_right' => __('Menu right', 'ct_theme'),
    'shop_top_navigation' => __('Shop top navigation', 'ct_theme'),
    'shop_footer_navigation' => __('Shop footer navigation', 'ct_theme'),
));


register_sidebar(array(
    'name' => __('Products Listing', 'ct_theme'),
    'id' => 'products-listing',
    'description'=>__('Widgets placed in this area will appear on Products Listing.','ct_theme'),
    'before_widget' => ' <aside id="%1$s" class="widget %2$s">',
	'after_widget' => '</aside>',
	'before_title' => '<h1 class="widget-title">',
	'after_title' => '</h1>',
));

// Register widgetized areas
register_sidebar(array(
	'name' => __('Primary Sidebar', 'ct_theme'),
	'id' => 'sidebar-primary',
	'description'=>__('Widgets placed in this area will appear on your Blog sidebar','ct_theme'),
	'before_widget' => ' <div id="%1$s" class="widget %2$s"><div class="widget-inner">',
	'after_widget' => '</div></div>',
	'before_title' => '<h4 class="widget-title">',
	'after_title' => '</h4>',
));


register_sidebar(array(
    'name' => __('Page Sidebar', 'ct_theme'),
    'id' => 'sidebar-page',
    'description'=>__('Widgets placed in this area will appear on your Blog sidebar','ct_theme'),
    'before_widget' => ' <aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h1 class="widget-title">',
    'after_title' => '</h1>',
));


register_sidebar(array(
  'name' => __('Pre Footer column 1', 'ct_theme'),
  'id' => 'pre-footer1',
  'description'=>__('Widgets placed in this area will appear at the bootm of the page (footer) on the left hand side.','ct_theme'),
	'before_widget' => '<div  id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h1 class="widget-title">',
  'after_title' => '</h1>',
));

register_sidebar(array(
  'name' => __('Pre Footer column 2', 'ct_theme'),
  'id' => 'pre-footer2',
  'description'=>__('Widgets placed in this area will appear at the bootm of the page (footer) on the right hand side.','ct_theme'),
	'before_widget' => '<div  id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h1 class="widget-title">',
  'after_title' => '</h1>',
));


register_sidebar(array(
    'name' => __('Post Footer column 1', 'ct_theme'),
    'id' => 'post-footer1',
    'description'=>__('Widgets placed in this area will appear at the bottom of the main footer on the left hand side.','ct_theme'),
    'before_widget' => '<div  id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
		'before_title' => '<h1 class="widget-title">',
    'after_title' => '</h1>',
));

register_sidebar(array(
    'name' => __('Post Footer column 2', 'ct_theme'),
    'id' => 'post-footer2',
    'description'=>__('Widgets placed in this area will appear at the bottom of the main footer on the right hand side.','ct_theme'),
    'before_widget' => '<div  id="%1$s" class="widget %2$s">',
    'after_widget' => '</div>',
		'before_title' => '<h1 class="widget-title">',
    'after_title' => '</h1>',
));


<?php

require_once get_template_directory() . '/framework/createit/ctThemeLoader.php';

$c = new ctThemeLoader();

// don't use nice search if WPML is active
if (function_exists('icl_object_id')) {
	remove_action('template_redirect', 'roots_nice_search_redirect');
}

$c->init( 'corpress' );

function roots_setup() {


	// Make theme available for translation
	load_theme_textdomain( 'ct_theme', get_template_directory() . '/lang' );

	// Add default posts and comments RSS feed links to <head>.
	add_theme_support( 'automatic-feed-links' );

	// Add post thumbnails (http://codex.wordpress.org/Post_Thumbnails)
	add_theme_support( 'post-thumbnails' );

	add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio' ) );

	add_theme_support( 'custom-header' );

	add_theme_support( 'woocommerce' );

	//add size for featured image
	add_image_size( 'featured_image', 336, 209, true );

	//add size for featured image large
	add_image_size( 'featured_image_large', 848, 528, true );

	//add size for featured image
	add_image_size( 'featured_image_masonry', 262, 172, true );


	//add size for featured image
	add_image_size( 'portfolio_single_standard', 750, 468, true );


	//add size for featured image
	add_image_size( 'portfolio_single_horizontal', 1140, 468, true );

	//works
	add_image_size( 'portfolio_2_columns', 555, 417, true );

	//works
	add_image_size( 'portfolio_4_columns', 263, 197, true );

	//works
	add_image_size( 'portfolio_3_columns', 360, 270, true );


	//add size for portfolio and blog thumbnails
	add_image_size( 'portfolio_slider', 320, 320, true );


	require_once CT_THEME_SETTINGS_MAIN_DIR . '/options/ctCustomizeManagerHandler.class.php';
	new ctCustomizeManagerHandler();
}

add_action( 'after_setup_theme', 'roots_setup' );

require_once 'theme/theme_functions.php';


add_action( 'admin_menu', 'ct_admin_documentation' );
function ct_admin_documentation() {
	global $submenu;
	//$key                              = count( array_keys( $submenu['themes.php'] ) ) + 1;
	$submenu['themes.php'][ 129 ] = array(
		'Documentation',
		'manage_options',
		'http://createit.support/documentation/corpress/'
	);
}
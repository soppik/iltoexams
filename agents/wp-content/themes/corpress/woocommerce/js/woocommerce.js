jQuery(document).ready(function () {
    "use strict";
    $ = jQuery.noConflict();

    $("select").select2({
        allowClear: true,
        minimumResultsForSearch: Infinity
    });


    if(($.flexslider)){
        // The slider being synced must be initialized first
        $('.flexslider.woo_flexslider_thumbs').flexslider({
            animation: "slide",
            direction: "horizontal",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 137,
            itemHeight: 137,
            itemMargin: 10,
            prevText: "",
            nextText: "",
            asNavFor: '.flexslider.woo_flexslider'
        });
        $('.flexslider.woo_flexslider').flexslider({
            animation: "slide",
            direction: "horizontal",
            easing: "easeOutBounce",
            smoothHeight: false,
            controlNav: false,
            directionNav: false,
            animationLoop: false,
            slideshow: false,
            touch: false,
            sync: ".flexslider.woo_flexslider_thumbs"
        });
    }
})
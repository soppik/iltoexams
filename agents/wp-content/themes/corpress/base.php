<?php get_template_part('templates/header'); ?>

<?php $faqData = get_post() && ct_get_option('faq_index_page') == get_the_ID() ? 'data-spy="scroll" data-target="#faq1" data-offset="5"' : ''; ?>
<body <?php body_class((ct_get_option('style_show_preloader') == 'yes' ? 'preloader ' : '') . (function_exists('icl_object_id') ? (ICL_LANGUAGE_CODE . ' ') : '')); ?> <?php echo $faqData ?>>
<div id="ct_preloader"></div>
<?php get_template_part('templates/head-top-navbar'); ?>

<?php if (!is_page_template('page-maintenance.php')): ?>
    <div id="wrapper">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header visible-xs">

                <?php if (ct_get_option('logo_mobile') != ''): ?>
                    <a class="navbar-brand" href="<?php echo home_url(); ?>"><img src="<?php echo ct_get_option('logo_mobile') ?>"></a>
                <?php endif; ?>

                <button type="button" class="navbar-toggle" id="sidebar-toggle">
                    <span class="sr-only"><?php echo __('Toggle navigation', 'ct_theme') ?></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#" class="showHeaderSearch"><i class="fa fa-fw fa-search"></i></a>
            </div>
        </div>

        <div class="container">
            <?php include roots_template_path(); ?>
        </div>

        <?php get_template_part('templates/footer'); ?>
    </div>

<?php else: ?>

    <div class="container">
        <?php include roots_template_path(); ?>
    </div>
    <?php get_template_part('templates/footer'); ?>
<?php endif; ?>
<?php wp_footer(); ?>

</body>
</html>


<!--container!--></div>
<?php get_template_part('templates/page', 'head'); ?>

<?php $breadcrumbs = ct_show_index_post_breadcrumbs('post') ? 'yes' : 'no'; ?>
<?php if (ct_get_option("posts_index_show_p_title", 1) || $breadcrumbs == "yes"): ?>
    <?php $pageTitle = (ct_get_option("posts_index_show_p_title", 1)) ? get_the_title(get_option('page_for_posts')) : ''; ?>


    <?php if ($pageTitle || $breadcrumbs == "yes"): ?>
        <?php
        $custom = get_post_custom(get_option('page_for_posts'));
        $global = isset($custom["global"][0]) ? $custom["global"][0] : "yes";
        if ($global == 'no') {
            $header_type = isset($custom["header_type"][0]) ? $custom["header_type"][0] : "";
            $header_height = isset($custom["header_height"][0]) ? $custom["header_height"][0] : "";
            $header_parallax_ratio = isset($custom["header_parallax_ratio"][0]) ? $custom["header_parallax_ratio"][0] : "";
            $header_image = isset($custom["header_image"][0]) ? $custom["header_image"][0] : "";
            $header_image_mobile = isset($custom["header_image_mobile"][0]) ? $custom["header_image_mobile"][0] : "";
            $header_video_source = isset($custom["header_video_source"][0]) ? $custom["header_video_source"][0] : "";
            $video_type = isset($custom["video_type"][0]) ? $custom["video_type"][0] : "";
            $header_image_video_fallback = isset($custom["header_image_video_fallback"][0]) ? $custom["header_image_video_fallback"][0] : "";

            echo do_shortcode('[title_row use_theme_options="no" header_image_video_fallback="' . $header_image_video_fallback . '" video_type="' . $video_type . '" header_type="' . $header_type . '" header_height="' . $header_height . '" header_parallax_ratio="' . $header_parallax_ratio . '" header_image="' . $header_image . '" header_image_mobile="' . $header_image_mobile . '" header_video_source="' . $header_video_source . '" header="' . $pageTitle . '" breadcrumbs="' . $breadcrumbs . '"]');

        } else {
             echo do_shortcode('[title_row header="' . $pageTitle . '" breadcrumbs="' . $breadcrumbs . '"]');
        }

        ?>
    <?php endif; ?>





<?php endif ?>

<div class="container">
    <section class="container section">
        <div class="row">
            <?php if (ct_get_option("posts_show_index_as", 1) == 'blog_small' || ct_get_option("posts_show_index_as", 1) == ''): ?>

                <div class="col-sm-8 col-md-9">
                    <?php get_template_part('templates/content'); ?>
                </div>
                <!--col-md-9 end!-->
            <?php elseif (ct_get_option("posts_show_index_as", 1) == 'blog_large'): ?>

                <div class="col-sm-8 col-md-9">
                    <?php get_template_part('templates/content-large'); ?>
                </div>
            <?php
            else: ?>

                <div class="col-sm-8 col-md-9">
                    <?php get_template_part('templates/content-masonry'); ?>
                </div>
            <?php
            endif ?>

            <?php if (ct_use_blog_index_sidebar()): ?>
                <div class="col-sm-4 col-md-3 sidebar">
                    <?php get_template_part('templates/sidebar') ?>
                </div>
            <?php endif ?>
        </div>
        <!--row_end!-->
    </section>






<!--container!--></div>
<?php get_template_part('templates/page', 'head'); ?>

<?php $breadcrumbs = ct_show_single_post_breadcrumbs('page') ? 'yes' : 'no';

if ($breadcrumbs =='global'){
    $breadcrumbs = ct_get_option('pages_single_show_breadcrumbs',0);
}

?>
<?php $pageTitle = ct_get_single_post_title('page'); ?>

<?php if ($pageTitle || $breadcrumbs == "yes"): ?>
    <?php
    $custom = get_post_custom($post->ID);
    $global = isset($custom["global"][0]) ? $custom["global"][0] : "yes";
    if ($global == 'no') {
        $header_type = isset($custom["header_type"][0]) ? $custom["header_type"][0] : "";
        $header_height = isset($custom["header_height"][0]) ? $custom["header_height"][0] : "";
        $header_parallax_ratio = isset($custom["header_parallax_ratio"][0]) ? $custom["header_parallax_ratio"][0] : "";
        $header_image = isset($custom["header_image"][0]) ? $custom["header_image"][0] : "";
        $header_image_mobile = isset($custom["header_image_mobile"][0]) ? $custom["header_image_mobile"][0] : "";
        $header_video_source = isset($custom["header_video_source"][0]) ? $custom["header_video_source"][0] : "";
        $video_type = isset($custom["video_type"][0]) ? $custom["video_type"][0] : "";
        $header_image_video_fallback = isset($custom["header_image_video_fallback"][0]) ? $custom["header_image_video_fallback"][0] : "";

        echo do_shortcode('[title_row use_theme_options="no" header_image_video_fallback="' . $header_image_video_fallback . '" video_type="' . $video_type . '" header_type="' . $header_type . '" header_height="' . $header_height . '" header_parallax_ratio="' . $header_parallax_ratio . '" header_image="' . $header_image . '" header_image_mobile="' . $header_image_mobile . '" header_video_source="' . $header_video_source . '" header="' . $pageTitle . '" breadcrumbs="' . $breadcrumbs . '"]');

    } else {
        echo do_shortcode('[title_row context="pages" header="' . $pageTitle . '" breadcrumbs="' . $breadcrumbs . '"]');
    }

    ?>
<?php endif; ?>


<div class="container">


    <?php get_template_part('templates/content', 'page'); ?>

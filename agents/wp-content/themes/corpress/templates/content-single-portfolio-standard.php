<?php while (have_posts()) :
    the_post(); ?>
    <?php $custom = get_post_custom(get_the_ID()); ?>
    <?php $prev = get_next_post(); ?>
    <?php $next = get_previous_post(); ?>
    <?php $prev = (is_object($prev)) ? get_permalink($prev->ID) : ''; ?>
    <?php $next = (is_object($next)) ? get_permalink($next->ID) : ''; ?>


    <?php $breadcrumbs = ct_show_single_post_breadcrumbs('portfolio') ? 'yes' : 'no'; ?>
    <?php $pageTitle = ct_get_single_post_title('portfolio'); ?>
    <?php $indexUrl = get_permalink(ct_get_option('portfolio_index_page')); ?>


    <?php if ($pageTitle || $breadcrumbs == "yes"): ?>

    <?php echo do_shortcode('[title_row nav="true" nav_prev_url="' . $prev . '" nav_next_url="' . $next . '" nav_index_url="' . $indexUrl . '" context="portfolio_single" header_type="parallax_centered" id="" header="' . get_the_title() . '" breadcrumbs="' . $breadcrumbs . '"]') ?>

<?php endif ?>




    <section class="container section">
        <div class="row">
            <div class="col-md-8">
                <?php get_template_part('templates/portfolio/content-single-portfolio-standard', ctPortfolioType::getMethodFromMeta($custom)); ?>
            </div>
            <div class="col-md-4 project-desc">

                <?php if (isset($custom['summary'][0]) && isset($custom['summary_title'][0])): ?>
                    <h4><?php echo $custom['summary_title'][0]?></h4><p><?php echo $custom['summary'][0]?></p>
                <?php endif; ?>

                <div class="project-single-info-container">

                </div>
                <?php if (ct_get_option("portfolio_single_show_client", 1) && isset($custom['client'][0]) && $custom['client'][0]): ?>
                    <div class="project-single-info">
                        <h6 class="uppercase"><?php echo (ct_get_option("portfolio_single_client_label") != '') ? ct_get_option("portfolio_single_client_label") : __('Client', 'ct_theme') ?></h6>

                        <p class="small-margin"><?php echo $custom['client'][0]; ?></p>
                    </div>
                <?php endif; ?>

                <?php if (isset($custom['external_url'][0]) && $custom['external_url'][0]): ?>
                    <?php $externalLabel = ct_get_option("portfolio_single_url_label", "online") ?>
                    <div class="project-single-info">
                        <h6 class="uppercase"><?php echo $externalLabel; ?></h6>

                        <p class="small-margin"><a
                                href="<?php echo $custom['external_url'][0] ?>"><?php echo $custom['external_url'][0] ?></a>
                        </p>
                    </div>
                <?php endif; ?>

                <?php if (ct_get_option("portfolio_single_show_date", 1) && isset($custom['date'][0]) && $custom['date'][0]): ?>
                    <div class="project-single-info">
                        <h6 class="uppercase"><?php echo (ct_get_option("portfolio_single_date_label") != '') ? ct_get_option("portfolio_single_date_label") : __('Date', 'ct_theme') ?></h6>

                        <p class="small-margin"><?php echo $custom['date'][0]; ?></p>
                    </div>
                <?php endif; ?>


                <?php if (ct_get_option("portfolio_single_show_cats", 1)): ?>
                    <div class="project-single-info">
                        <h6 class="uppercase"><?php echo (ct_get_option("portfolio_single_cats_label") != '') ? ct_get_option("portfolio_single_cats_label") : __('Categories', 'ct_theme') ?></h6>

                        <p class="small-margin"><?php echo ct_get_categories_string(get_the_id(), $separator = ', ', $taxonomy = 'portfolio_category') ?></p>
                    </div>
                <?php endif; ?>


                <?php if (ct_get_option("portfolio_single_show_socials", 0)): ?>
                    <div class="project-single-info">
                        <h6 class="uppercase"><?php echo (ct_get_option("portfolio_single_share_label") != '') ? ct_get_option("portfolio_single_share_label") : __('Share This Project', 'ct_theme') ?></h6>
                        <?php echo ct_socials_code('true', 'top') ?>
                    </div>
                <?php endif; ?>

                <div class="clearfix"></div>
            </div>
        </div>
        <br>
        <?php the_content() ?>
    </section>


    <?php if (ct_get_option("portfolio_single_show_other_projects", 1)): ?>
    <section class="section bg2">
        <div class="text-center">
            <h4 class="uppercase section-title hr-mid"><?php echo (ct_get_option("portfolio_related_projects_label") != '') ? ct_get_option("portfolio_related_projects_label") : __('You Might also like these Projects', 'ct_theme') ?></h4>
        </div>
        <?php echo do_shortcode('[works_slider limit="20"]') ?>
    </section>
<?php endif; ?>


    <?php if (ct_get_option("portfolio_single_show_comments", 0)): ?>
    <?php comments_template('/templates/comments.php'); ?>
<?php endif; ?>

<?php endwhile ?>




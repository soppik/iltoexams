<section class="container section">
    <div class="row">
        <div class="col-md-12 text-center">
            <h2 class="uppercase weight300"><?php echo ct_get_option('faq_content_header', '') ?></h2>
        </div>
    </div>
    <div class="row section-top">


        <?php
        $terms = get_terms('faq_category', 'hide_empty=1');
        $counter = 0;
        $resetCounter2 = 0;
        foreach ($terms as $term) {
        $custom_attributes['post_type'] = 'faq';
        $custom_attributes['posts_per_page'] = -1;
        $custom_attributes['tax_query'] = array(
            array(
                'taxonomy' => 'faq_category',
                'field' => 'id',
                'terms' => $term->term_id,


            )
        );
        $faqCollection = get_posts($custom_attributes);
        ?>
        <div class="col-md-4 col-sm-6">
            <h4 class="uppercase hr-left"><?php echo $term->name ?></h4>

            <div id="accordion-<?php echo $term->slug ?>" class="panel-group accordion">
                <?php
                $counter2 = $resetCounter2;

                foreach ($faqCollection as $p) {
                    $custom = get_post_custom($p->ID);
                    ?>
                    <div class="panel">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion-<?php echo $term->slug ?>"
                               href="#collapse<?php echo $counter2 ?>" <?php echo $counter2 > $resetCounter2 ? 'class="collapsed"' : ''; ?>>
                                <span><?php echo get_the_title($p->ID) ?></span>
                            </a>
                        </div>
                        <div id="collapse<?php echo $counter2 ?>"
                             class="panel-collapse collapse <?php echo $counter2 == $resetCounter2 ? 'in' : ''; ?>">
                            <div class="panel-body">
                                <?php echo($p->post_content) ?>
                            </div>
                        </div>
                    </div>
                    <?php
                    $counter2++;
                }
                ?>
            </div>
        </div>
        <?php
        if ($counter == 2):?>
    </div>
    <div class="row section-top">
        <?php $counter = 0 ?>
        <?php else: ?>
            <?php $counter++ ?>
        <?php
        endif ?>
        <?php
        $resetCounter2 = $counter2;
        }
        ?>
</section>



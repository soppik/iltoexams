
<div class="clearfix"></div>

<h4 class="uppercase"><a
        href="<?php echo get_post_meta($post->ID, 'link', true)?>"><?php echo get_the_content() ?></a></h4>
<?php get_template_part('templates/post/content-meta'); ?>

<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<?php $format = get_post_format();
		$format       = $format ? $format : 'standard';
		$class        = $format == 'standard' ? 'journal' : 'journal format-' . $format;?>


		<article id="post-<?php the_ID(); ?>" <?php post_class( $class ); ?>>
			<?php get_template_part( 'templates/post_single/content-' . $format ); ?>
			<?php echo str_replace( "<li></li>", '', wp_link_pages( array(
					'before'           => '<div class="text-center"><ul class="pagination"><li>',
					'after'            => '</li></ul></div>',
					'separator'        => '</li><li>',
					'link_before'      => '<span>',
					'link_after'       => '</span>',
					'next_or_number'   => "next",
					'nextpagelink'     => '<i class="fa fa-angle-right"></i>',
					'previouspagelink' => '<i class="fa fa-angle-left"></i>',
					'echo'             => false
				)
			) ); ?>
		</article>

		<br>
		<hr class="small">


		<?php if ( get_option( "page_for_posts" ) != '0' ): ?>
			<a class="grey"
			   href="<?php echo get_permalink( get_option( "page_for_posts", '#' ) ); ?>"><?php echo __( 'Back to blog list', 'ct_theme' ) ?></a>
			<hr class="small">
			<br>
		<?php endif; ?>

		<?php if ( ct_get_option( "posts_index_show_tags", 0 ) && has_tag() ): ?>
			<h4 class="uppercase"><?php echo __( 'Tags', 'ct_theme' ) ?></h4>


			<ul class="list-unstyled list-inline article-tags">


				<?php the_tags( '<li>', '', '</li>' ) ?>

			</ul>
		<?php endif; ?>

		<?php if ( ct_get_option( "posts_single_show_socials", 0 ) ): ?>


			<h4 class="uppercase"><?php echo __( 'Share this Story', 'ct_theme' ) ?></h4>
			<?php echo ct_socials_code( 'false', 'top', 'LEFT', ct_get_option( 'posts_single_share_button_text' ) ) ?>


		<?php endif; ?>
		<br>


		<?php if ( ct_get_option( "posts_single_show_author_box", 1 ) ): ?>
            <h4 class="uppercase"><?php echo __( 'About the Author', 'ct_theme' ) ?></h4>
			<div class="media authorbox">
				<a class="pull-left" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ) ?>">
					<?php echo get_avatar( get_the_author_meta( 'ID' ), 70 ); ?>
				</a>

				<div class="media-body">
					<div class="inner-body">
						<h4 class="media-heading"><a
								href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ) ?>"><?php the_author() ?></a>
						</h4>

						<p><?php the_author_meta( 'user_description', get_the_author_meta( 'ID' ) ); ?></p>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<br>




		<?php comments_template( '/templates/comments.php' ); ?>

	<?php endwhile; ?>
<?php endif ?>
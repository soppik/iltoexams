<?php $imageUrl = '';
$width = '600';
$height = '1000';
$atr = ct_get_featured_image_data (get_the_ID(),$width,$height);?>


<?php if (has_post_thumbnail(get_the_ID())): ?>
    <img src="<?php echo esc_attr( $atr['url']) ?>"
         alt="<?php echo esc_attr( $atr['alt']) ?>"
         title="<?php echo esc_attr( $atr['title']) ?>"  >

<?php endif; ?>




<div class="media-object">
    <?php if (ct_get_option("posts_index_show_image", 1)): ?>

        <?php get_template_part('templates/post/content-gallery', 'gallery'); ?>

    <?php endif; ?>
</div>


<h4 class="uppercase"><?php echo ct_get_blog_item_title() ?></h4>
<?php get_template_part('templates/post/content-meta'); ?>


<?php the_content(); ?>

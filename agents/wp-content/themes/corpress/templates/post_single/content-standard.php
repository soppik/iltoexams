<div class="media-object">
    <?php if (ct_get_option("posts_single_show_image", 1) && has_post_thumbnail(get_the_ID())): ?>
        <?php get_template_part('templates/post_single/content-featured-image'); ?>
    <?php endif; ?>
</div>

<div class="clearfix"></div>


<h4 class="uppercase"><?php echo ct_get_blog_item_title() ?></h4>
<?php get_template_part('templates/post_single/content-meta'); ?>

<?php the_content(); ?>

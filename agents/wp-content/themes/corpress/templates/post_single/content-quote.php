<h4 class="uppercase"><?php echo ct_get_blog_item_title() ?></h4>
<?php get_template_part('templates/post_single/content-meta'); ?>

<div class="journalblock bg3">
    <em>
        <?php echo get_post_meta($post->ID, 'quote', true) ?>
    </em>
</div>

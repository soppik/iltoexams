<div class="media-object">
    <div class="video ">
        <?php
        $embed = get_post_meta($post->ID, 'videoCode', true);
        if (!empty($embed)) {
            echo stripslashes(htmlspecialchars_decode($embed));
        } else {
            ct_post_video($post->ID, 870, 520);
        }
        ?>
    </div>
</div>


<h4 class="uppercase"><?php echo ct_get_blog_item_title() ?></h4>
<?php get_template_part('templates/post_single/content-meta'); ?>

<?php the_content(); ?>

<?php if (ct_get_option("posts_index_show_excerpt", 1)): ?>
    <a href="<?php echo get_permalink(get_the_ID()) ?>"
       class="btn btn-lg btn-border"><?php echo __('Read More', 'ct_theme') ?></a>
<?php endif ?>


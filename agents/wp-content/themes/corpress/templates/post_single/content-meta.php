<?php
$comments = wp_count_comments(get_the_ID())->approved;
if (!post_password_required()) {
    $comments = ($comments == 1 ? $comments . ' ' . __('Comment', 'ct_theme') : $comments . ' ' . __('Comments', 'ct_theme'));
} else {
    $comments = '';
}
?>

<span class="meta">
<?php if (ct_get_option("posts_single_show_date", 1)): ?>
    <span class="meta-single">
        <i class="fa fa-calendar"></i> <?php echo get_the_date() ?>
    </span>
<?php endif ?>
    <?php if (ct_get_option("posts_single_show_author", 1)): ?>
        <span class="meta-single">
        <i class="fa fa-user"></i> <?php the_author_posts_link() ?>
    </span>
    <?php endif ?>
    <?php if (ct_get_option("posts_single_show_categories", 1)): ?>
        <span class="meta-single">
        <i class="fa fa-folder-open-o"></i> <?php echo ct_get_categories_string(get_the_ID(), $separator = ', ', $taxonomy = 'category') ?>
    </span>
    <?php endif ?>
    <?php if (ct_get_option("posts_single_show_comments_link", 1)): ?>
        <span class="meta-single">
        <i class="fa fa-comment-o"></i> <?php echo $comments ?>
    </span>
    <?php endif ?>
</span>


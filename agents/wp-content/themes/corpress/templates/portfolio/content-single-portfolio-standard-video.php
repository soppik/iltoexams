<?php $imageUrl = '';
$custom = get_post_custom(get_the_ID());
$link = esc_url($custom['video'][0]);

if ($link){
    echo do_shortcode('[video link="' . $link . '"]');
}
?>

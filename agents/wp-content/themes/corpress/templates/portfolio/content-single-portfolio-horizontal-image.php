<?php $imageUrl = '';
$width = 600;
$height = 1000;
$atr = ct_get_featured_image_data (get_the_ID(),$width,$height);?>

<?php if (has_post_thumbnail(get_the_ID())): ?>
	<?php $image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'portfolio_single_horizontal'); ?>
	<?php $imageUrl = $image[0];
       ?>
<?php endif; ?>
	<?php echo do_shortcode('[img src="' . $imageUrl . '" width="" height="" title="'.$atr['title'].'" alt="'.$atr['alt'].'" ]') ?>
<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
        <?php $format = get_post_format();
        $format = $format ? $format : 'standard';
        $class = $format == 'standard' ? 'journal' : 'journal format-' . $format;
        $custom = get_post_custom($post->ID);

        $job_position = isset($custom["job_position"][0]) ? $custom["job_position"][0] : "";
        $socials = isset($custom["socials"][0]) ? $custom["socials"][0] : "";
        $description = isset($custom["description"][0]) ? $custom["description"][0] : "";?>

        <section class="container section">
            <div class="row">
                <div class="col-sm-6">
                    <div class="person-image text-center">
                        <img src="<?php echo ct_get_feature_image_src(get_the_ID(), 'full'); ?>">
                    </div>
                </div>
                <div class="col-sm-6">
                    <h2 class="uppercase hr-left"><?php the_title() ?></h2>
                    <?php echo do_shortcode($socials) ?>
                    <h2 class="uppercase weight300"><?php echo $job_position ?></h2>

                    <p> <?php echo $description ?></p>
                </div>
            </div>
        </section>
        <?php the_content() ?>
    <?php endwhile; ?>
<?php endif ?>
<?php if ( ! is_page_template( 'page-maintenance.php' ) ): ?>

	<?php if ( is_active_sidebar( 'pre-footer1' ) || is_active_sidebar( 'pre-footer2' ) ): ?>


		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<?php dynamic_sidebar( 'pre-footer1' ); ?>
				</div>
				<div class="col-sm-6">
					<?php dynamic_sidebar( 'pre-footer2' ); ?>
				</div>
			</div>
		</div>


	<?php endif; ?>


	<footer class="main-footer">

		<div class="container">
			<div class="row">
				<?php ct_footer_columns( $customClass = '', $template = '<div class="%class% col-md-%col% col-sm-6">', $closeTemplate = '</div>', $maxColumns = 12 ) ?>
			</div>
		</div>


		<span id="backtoTop"><i class="fa fa-fw fa-angle-double-up"></i></span>
	</footer>
	<?php if ( is_active_sidebar( 'post-footer1' ) || is_active_sidebar( 'post-footer2' ) ): ?>

		<div class="post-footer">
			<div class="container">
				<div class="row">
					<aside class="col-sm-6">
						<?php dynamic_sidebar( 'post-footer1' ); ?>
					</aside>
					<aside class="col-sm-6">
						<?php dynamic_sidebar( 'post-footer2' ); ?>
					</aside>
				</div>
			</div>
		</div>
	<?php endif; ?>


	<?php
	$blogid    = get_option( 'page_for_posts' );
	$blogtitle = get_permalink( $blogid ) ? get_permalink( $blogid ) : '';
	$blogslug  = str_replace( site_url(), '', $blogtitle );

	echo "
<script>
	jQuery(document).ready(function () {
    'use strict';
    $ = jQuery.noConflict();

		if($('body').is('.single-portfolio, .single-job')) {
			$('#nav li.active > a').each(function(){
				var item = $(this);
				if (item.attr('href') == '" . $blogslug . "') {
					item.parent().removeClass('active');
				}
			})
		}
	});
</script>
";
	?>

<?php endif; ?>
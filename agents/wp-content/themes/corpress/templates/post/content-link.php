<div class="row">
    <?php if (ct_get_option("posts_index_show_image", 1) && has_post_thumbnail(get_the_ID())): ?>
    <div class="col-md-5">
        <div class="media-object">
            <a href="<?php echo get_permalink(get_the_ID()) ?>"><?php get_template_part('templates/post/content-featured-image'); ?></a>
        </div>
    </div>


    <div class="col-md-7">
        <?php else: ?>
        <div class="col-md-12">
            <?php endif ?>



            <h4 class="uppercase"><a
                    href="<?php echo get_post_meta($post->ID, 'link', true) ?>"><?php echo get_the_content() ?></a></h4>

            <?php get_template_part('templates/post/content-meta'); ?>


        </div>

    </div>
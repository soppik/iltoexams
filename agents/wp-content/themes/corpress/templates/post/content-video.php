<div class="row">
    <div class="col-md-5">
        <div class="media-object">
            <div class="video ">
                <?php
                $embed = get_post_meta($post->ID, 'videoCode', true);
                if (!empty($embed)) {
                    echo stripslashes(htmlspecialchars_decode($embed));
                } else {
                    ct_post_video($post->ID, 870, 520);
                }
                ?>
            </div>
        </div>
    </div>

    <div class="col-md-7">
        <h4 class="uppercase"><a
                href="<?php echo get_permalink(get_the_ID()) ?>"><?php echo ct_get_blog_item_title() ?></a></h4>
        <?php get_template_part('templates/post/content-meta'); ?>

        <?php if (ct_get_option("posts_index_show_excerpt", 1) && get_the_content()): ?>
            <p><?php echo ct_get_excerpt_by_id(get_the_id()); ?></p>
        <?php endif ?>

        <?php if (ct_get_option("posts_index_show_fulltext", 0)): ?>
            <?php the_content(); ?>
        <?php endif ?>

        <?php if (ct_get_option("posts_index_show_tags", 0) && has_tag()): ?>
            <?php the_tags('<span class="meta"> <span class="meta-single uppercase"><i class="fa fa-tags"></i> ', ', ', '</span></span>') ?>
        <?php endif; ?>

        <?php if (ct_get_option("posts_index_show_excerpt", 1)): ?>
            <a href="<?php echo get_permalink(get_the_ID()) ?>"
               class="btn btn-lg btn-border"><?php echo __('Read More', 'ct_theme') ?></a>
        <?php endif ?>
    </div>
</div>
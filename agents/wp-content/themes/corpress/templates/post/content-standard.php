<div class="row">
    <?php if (ct_get_option("posts_index_show_image", 1) && has_post_thumbnail(get_the_ID())): ?>
    <div class="col-md-5">
        <div class="media-object">

            <a href="<?php echo get_permalink(get_the_ID()) ?>"><?php get_template_part('templates/post/content-featured-image'); ?></a>

        </div>
    </div>

    <div class="col-md-7">
        <?php else: ?>
        <div class="col-md-12">
            <?php endif ?>

            <h4 class="uppercase"><a
                    href="<?php echo get_permalink(get_the_ID()) ?>"><?php echo ct_get_blog_item_title() ?></a></h4>
            <?php get_template_part('templates/post/content-meta'); ?>

            <?php if (ct_get_option("posts_index_show_excerpt", 1) && get_the_content()): ?>
                <p><?php echo ct_get_excerpt_by_id(get_the_id()); ?></p>
            <?php endif ?>

            <?php if (ct_get_option("posts_index_show_fulltext", 0)): ?>
                <?php the_content(); ?>
            <?php endif ?>

            <?php if (ct_get_option("posts_index_show_tags", 0) && has_tag()): ?>
                <?php the_tags('<span class="meta"> <span class="meta-single uppercase"><i class="fa fa-tags"></i> ', ', ', '</span></span>') ?>
            <?php endif; ?>

            <?php if (ct_get_option("posts_index_show_excerpt", 1)): ?>
                <a href="<?php echo get_permalink(get_the_ID()) ?>"
                   class="btn btn-lg btn-border"><?php echo __('Read More', 'ct_theme') ?></a>
            <?php endif ?>
        </div>
    </div>
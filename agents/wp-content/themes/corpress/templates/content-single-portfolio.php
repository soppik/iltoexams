
<?php $custom = get_post_custom(get_the_ID()); ?>

<?php if ($custom["portfolio_single_horizontal"][0] == 'yes'): ?>
    <?php get_template_part('templates/content', 'single-portfolio-horizontal'); ?>
<?php elseif ($custom["portfolio_single_horizontal"][0] == 'no'): ?>
    <?php get_template_part('templates/content', 'single-portfolio-standard'); ?>
<?php
elseif ($custom["portfolio_single_horizontal"][0] == 'global' || !isset($custom["portfolio_single_horizontal"][0])): ?>
    <?php if (ct_get_option("portfolio_single_horizontal", 1) == 'yes'): ?>
        <?php get_template_part('templates/content', 'single-portfolio-horizontal'); ?>
    <?php else: ?>
        <?php get_template_part('templates/content', 'single-portfolio-standard'); ?>
    <?php endif; ?>
<?php endif; ?>



<?php global $wp_query;

$arrgs = $wp_query->query_vars; ?>

<form class="text-center" role="form" action="<?php echo home_url('/'); ?>">
    <fieldset>
        <div class=" form-group">
            <button class="header-search-icon" type="submit"><i class="fa fa-fw fa-search"></i></button>
            <label><?php echo __('Please type and press "enter"', 'ct_theme') ?></label>
            <input type="text" class="form-control"
                   placeholder="<?php echo __('Type to search...', 'ct_theme') ?>"
                   value="<?php echo (isset($arrgs['s']) && $arrgs['s']) ? $arrgs['s'] : ''; ?>"
                   name="s"
                   id="s">
            <?php if (ct_is_woocommerce_active()):?>
                <input type="hidden" name="post_type" value="product">
            <?php endif; ?>
        </div>
    </fieldset>
    <a href="#" class="showHeaderSearch headerSearchClose"><i class="fa fa-fw fa-times"></i></a>
</form>

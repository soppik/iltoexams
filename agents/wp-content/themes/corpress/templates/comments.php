<?php if (!post_password_required() && ((get_post_type() == 'portfolio' && ct_get_option("portfolio_single_show_comments", 0)) || (get_post_type() == 'post' && ct_get_option("posts_single_show_comments", 1)) || (get_post_type() == 'page' && ct_get_option("pages_single_show_comments", 0)))): ?>



    <?php
    function theme_comments($comment, $args, $depth)
    {
        $GLOBALS['comment'] = $comment;?>

        <li>
            <div class="media">
                <a class="pull-left" href="#">
                    <?php echo get_avatar($comment, $size = '55', $default = 'mystery'); ?>
                </a>

                <div class="media-body">
                    <div class="inner-body">
                        <h4 class="media-heading"><?php echo get_comment_author_link() ?></h4>
                                <span
                                    class="comment-date"><?php echo get_comment_date(); ?><?php if (get_comment_time()): ?> <?php _e('at', 'ct_theme') ?> <?php echo get_comment_time() ?><?php endif; ?></span>
                        <?php if (ct_get_option("posts_single_show_comment_form", 1)): ?>
                            - <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                        <?php endif; ?>
                        <p>
                            <?php comment_text() ?>
                        </p>
                    </div>
                </div>
            </div>
        </li>
    <?php
    }

    ?>

    <?php $commentsCount = count(get_comments(array('type' => 'comment', 'post_id' => $post->ID))); ?>

    <?php if (comments_open() || $commentsCount > 0): ?>

        <ul class="commentList list-unstyled">
            <?php wp_list_comments(array('callback' => 'theme_comments', 'style' => 'ol', 'type' => 'comment')); ?>
        </ul>

        <?php if (pings_open()): ?>
            <?php
            $pingbacksCount = count(get_comments(array('type' => 'pingback', 'post_id' => $post->ID)));
            $trackbacksCount = count(get_comments(array('type' => 'trackback', 'post_id' => $post->ID)));
            ?>

            <?php if ($pingbacksCount > 0): ?>
                <header class="page-header text-center">
                    <h1 class="page-title"><?php echo __('Pingbacks', 'ct_theme') ?></h1>
                </header>
                <ul class="commentList list-unstyled">
                    <?php wp_list_comments(array('callback' => 'theme_comments', 'style' => 'ol', 'type' => 'pingback')); ?>
                </ul>
            <?php endif; ?>


            <?php if ($trackbacksCount > 0): ?>
                <header class="page-header text-center">
                    <h1 class="page-title"><?php echo __('Trackbacks', 'ct_theme') ?></h1>
                </header>
                <ul class="commentList list-unstyled">
                    <?php wp_list_comments(array('callback' => 'theme_comments', 'style' => 'ol', 'type' => 'trackback')); ?>
                </ul>
            <?php endif; ?>



            <?php if (get_previous_comments_link() != '' || get_next_comments_link() != ''): ?>
                <div class="row navigation-blog-outer">
                    <div class="col-md-6 col-xs-6 text-left">
                        <?php previous_comments_link('&larr; ' . __('Previous comments', 'ct_theme')); ?>
                    </div>
                    <div class="col-md-6 col-xs-6 text-right">
                        <?php next_comments_link(__('Next comments', 'ct_theme') . ' &rarr; '); ?>
                    </div>
                    <div class="clearfix visible-sm visible-xs"></div>
                </div>
                <hr>
                <br>
            <?php endif; ?>
        <?php endif; ?>
    <?php endif; ?>






    <?php if (((get_post_type() == 'portfolio' && ct_get_option("portfolio_single_show_comment_form", 0)) || (get_post_type() == 'post' && ct_get_option("posts_single_show_comment_form", 1)) || get_post_type() == 'page' && ct_get_option("pages_single_show_comment_form", 0)) && comments_open()) : // Comment Form ?>

        <h4 class="uppercase"><?php echo __('Join Discussion', 'ct_theme') ?></h4>
        <!-- comment form ****** -->
        <form role="form" action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post">
            <div class="row padding-xs-top">
                <div class="col-sm-6">
                    <div class="form-group form-group-float-label">
                        <input id="comment_name" required type="text" name="field[]" class="form-control input-lg"
                               placeholder="<?php echo __('Name', 'ct_theme') ?>">
                        <label for="comment_name"><?php echo __('Name *', 'ct_theme') ?></label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group form-group-float-label">
                        <input id="comment_email" required type="email" name="field[]" class="form-control input-lg"
                               placeholder="<?php echo __('Email', 'ct_theme') ?>">
                        <label for="comment_email"><?php echo __('Email *', 'ct_theme') ?></label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group form-group-float-label">
                        <textarea id="comment_message" class="form-control input-lg" rows="4" name="comment" required
                                  placeholder="<?php echo __('<Message', 'ct_theme') ?>"></textarea>
                        <label for="comment_message"><?php echo __('Message *', 'ct_theme') ?></label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-lg"><?php echo __('Send Now', 'ct_theme') ?></button>
                </div>
            </div>
            <?php comment_id_fields(); ?>
            <?php do_action('comment_form', get_the_ID()); ?>
            <?php if (false): ?><?php comment_form() ?><?php endif; ?>
        </form>
        <!-- ********************* -->
        <!-- / comment form ****** -->
    <?php endif; ?>
<?php endif; ?>
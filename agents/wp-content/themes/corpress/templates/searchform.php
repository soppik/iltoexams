<?php global $wp_query;

$arrgs = $wp_query->query_vars; ?>

<form role="form" action="<?php echo home_url('/'); ?>">
<div class=" search-widget">
    <div class="input-group input-group-lg form-group-float-label">
        <input value="<?php echo (isset($arrgs['s']) && $arrgs['s']) ? $arrgs['s'] : ''; ?>" id="s" class="form-control" required="" name="s" type="text">
        <label for="s"><?php echo __('Search', 'ct_theme') ?></label>
        <span class="input-group-btn">
            <button class="btn btn-primary"><i class="fa fa-search"></i></button>
        </span>
    </div>
</div>
</form>
<?php if (!is_page_template('page-maintenance.php')): ?>
    <?php if (ct_is_woocommerce_active()) {
        global $woocommerce;
        $basketCounter = sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'ct_theme'), $woocommerce->cart->cart_contents_count);
    }?>

    <?php if (ct_get_option('show_topbar') || ct_get_option('show_topbar') == ''): ?>


        <header class="hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-7 col-sm-9">
                        <?php if (ct_get_option('general_header_phone', '') != ''): ?>
                            <div class="text-wrapper"><i
                                    class="fa fa-phone-square fa-fw"></i> <?php echo ct_get_option('general_header_phone', '') ?>
                            </div>
                        <?php endif; ?>


                        <?php if (ct_get_option('general_header_email', '') != ''): ?>
                            <div class="text-wrapper"><i class="fa fa-envelope-o fa-fw"></i> <a
                                    href="mailto:<?php echo ct_get_option('general_header_email', '') ?>"><?php echo ct_get_option('general_header_email', '') ?></a>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="col-lg-6 col-md-5 col-sm-3 text-right">
                        <?php if (ct_is_woocommerce_active()) { ?>
                            <?php if (is_user_logged_in()) { ?>
                                <div class="text-wrapper hidden-md hidden-sm">
                                    <?php echo __('You are logged in as', 'ct_theme') ?>
                                    <a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>">
                                        <strong>
                                            <?php global $current_user;
                                            get_currentuserinfo();
                                            echo $current_user->user_login ?>
                                        </strong>
                                    </a>
                                </div>
                                <div class="text-wrapper visible-sm">
                                    <a href="<?php echo get_permalink(woocommerce_get_page_id('myaccount')); ?>"><i
                                            class="fa fa-fw fa-user"></i> <?php echo __('My Account', 'ct_theme') ?></a>
                                </div>
                                <div class="text-wrapper visible-sm remove-right-padding">
                                    <a href="<?php echo $woocommerce->cart->get_cart_url(); ?>"
                                       title="<?php _e('View your shopping cart', 'woothemes'); ?>" class="hidden-xs"><i
                                            class="fa fa-fw fa-shopping-cart"></i>
                                        <span class="number-items"><?php echo $basketCounter ?></span>
                                    </a>

                                </div>
                                <div class="text-wrapper hidden-sm">
                                    <a href="<?php echo get_permalink(woocommerce_get_page_id('myaccount')); ?>"><i
                                            class="fa fa-fw fa-cog"></i> <?php echo __('Settings', 'ct_theme') ?></a>
                                </div>
                                <div class="text-wrapper hidden-sm">
                                    <a href="<?php echo wp_logout_url() ?>"><i
                                            class="fa fa-fw fa-power-off"></i> <?php echo __('Logout', 'ct_theme') ?>
                                    </a>
                                </div>
                            <?php
                            } else {
                                ?>
                                <?php if (get_option('woocommerce_enable_myaccount_registration') === 'yes') : ?>
                                    <div class="text-wrapper">
                                        <a href="<?php echo get_permalink(woocommerce_get_page_id('myaccount')); ?>"><i
                                                class="fa fa-fw fa-plus-circle"></i> <?php echo __('Register', 'ct_theme') ?>
                                        </a>
                                    </div>
                                <?php endif ?>
                                <div class="text-wrapper">
                                    <a href="#" class="btn-login-toggle"><i
                                            class="fa fa-fw fa-user"></i> <?php echo __('Login', 'ct_theme') ?></a>
                                </div>

                            <?php } ?>

                        <?php
                        } else {
                            ?>

                            <?php echo ct_socials_code('true', 'bottom') ?>

                        <?php } ?>
                    </div>
                </div>
            </div>
        </header>
    <?php endif ?>


    <div class="container"></div>

    <nav class="navbar yamm" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header hidden-xs">
                <?php if (ct_get_option('logo_standard', '') != ''): ?>
                    <a class="navbar-brand" href="<?php echo home_url(); ?>"><img
                            src="<?php echo ct_get_option('logo_standard', '') ?>"
                            alt="<?Php echo esc_attr(ct_get_option('logo_standard_alt_attr',''));?>"/></a>

                <?php endif; ?>
                <button type="button" class="navbar-toggle navbar-sidebar-toggle" id="sidebar-toggle">
                    <span class="sr-only"><?php echo __('Toggle navigation', 'ct_theme') ?></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse no-transition" id="bs-example-navbar-collapse-1">
                <?php if (ct_is_woocommerce_active()) { ?>
                    <div class="menu-woocommerce-cart hidden-sm hidden-xs">
                        <a href="<?php echo $woocommerce->cart->get_cart_url(); ?>"
                           title="<?php _e('View your shopping cart', 'woothemes'); ?>" class="cart-contents hidden-xs"><i
                                class="fa fa-fw fa-shopping-cart"></i>
                            <span class="number-items"><?php echo $basketCounter ?></span>
                        </a>


                        <div class="cart-box">
                            <?php if (version_compare(WOOCOMMERCE_VERSION, "2.0.0") >= 0) {
                                the_widget('Custom_WooCommerce_Widget_Cart', 'title=');
                            } else {
                                the_widget('WooCommerce_Widget_Cart', 'title=');
                            }?>
                        </div>
                    </div>
                <?php } ?>

                <a href="#" class="showHeaderSearch hidden-xs"><i class="fa fa-fw fa-search"></i></a>

                <?php if (ct_get_option('navbar_type') == 'navbar-middle'): ?>
                    <?php
                    if (ct_is_location_contains_menu('menu_left') && ct_is_location_contains_menu('menu_right')) {
                        ct_wp_nav_menu(
                            array(
                                'items_wrap' => '<ul id="%1$s" class="%2$s nav navbar-nav navbar-left">%3$s</ul>',
                                'theme_location' => 'menu_left',
                                'menu_class' => 'nav navbar-nav navbar-left',
                                'menu_id' => 'nav'
                            )
                        );
                        ct_wp_nav_menu(
                            array(
                                'items_wrap' => '<ul id="%1$s" class="%2$s nav navbar-nav navbar-right">%3$s</ul>',
                                'theme_location' => 'menu_right',
                                'menu_class' => 'nav navbar-nav navbar-right',
                                'menu_id' => 'nav'
                            )
                        );
                    }

                    ?>


                <?php else: ?>
                    <?php
                    if (ct_is_location_contains_menu('primary_navigation')) {
                        wp_nav_menu(
                            array(
                                'items_wrap' => '<ul id="%1$s" class="%2$s nav navbar-nav navbar-right">%3$s</ul>',
                                'theme_location' => 'primary_navigation',
                                'menu_class' => 'nav navbar-nav navbar-right',
                                'menu_id' => 'nav'
                            )
                        );
                    }

                    ?>


                <?php endif; ?>


            </div>
        </div>
    </nav>


    <div class="container"></div>
    <div class="header-search" style="display:none;">
        <div class="display-table">
            <div class="table-cell">
                <?php  get_template_part('templates/searchform_bar') ?>
            </div>
        </div>
    </div>

    <?php if (ct_is_woocommerce_active()) { ?>
        <?php if (!is_user_logged_in()) { ?>
            <div class="header-login" style="display:none;">
                <div class="display-table">
                    <div class="table-cell">
                        <form method="post" class="text-center">
                            <fieldset>
                                <?php do_action('woocommerce_login_form_start'); ?>

                                <div class="form-group">
                                    <span class="header-search-icon"><i class="fa fa-fw fa-user"></i></span>
                                    <label><?php _e('Please type your username and password', 'woocommerce'); ?></label>
                                    <input type="text" name="username" id="username" required class="form-control"
                                           placeholder="<?php _e('Username*', 'woocommerce'); ?>">
                                </div>

                                <div class="form-group">
                                    <label class="hidden" for="password"><?php _e('Password', 'woocommerce'); ?></label>
                                    <input type="password" name="password" id="password" class="form-control"
                                           placeholder="<?php _e('Password*', 'woocommerce'); ?>">
                                </div>

                                <?php do_action('woocommerce_login_form'); ?>

                                <button class="btn btn-default btn-lg" name="login" type="submit"
                                        value="<?php _e('Login', 'woocommerce'); ?>"><?php _e('Login', 'woocommerce'); ?></button>
                                    <span class="remember-box checkbox">
                                        <?php wp_nonce_field('woocommerce-login'); ?>
                                        <input name="rememberme" type="checkbox" id="rememberme"
                                               value="forever">
                                        <label for="rememberme"><?php _e('Remember Me', 'woocommerce'); ?></label>
                                    </span>
                                <a href="<?php echo esc_url(wc_lostpassword_url()); ?>">(<?php _e('Lost Password?', 'woocommerce'); ?>
                                    )</a>

                                <?php do_action('woocommerce_login_form_end'); ?>
                            </fieldset>
                            <a href="#" class="btn-login-toggle headerLoginClose"><i class="fa fa-fw fa-times"></i></a>
                        </form>
                    </div>
                </div>
            </div>
        <?php
        }
    } ?>


    <?php if (!is_page_template('page-content-only.php')): ?>
        <?php

        if ((ct_get_option('navbar_type', 'standard') == 'sticky')) {
            $custom = get_post_custom(get_the_ID());
            if (!isset($custom["navbar_transparent"][0])) {
                $custom["navbar_transparent"][0] = 'global';
            }

            if ($custom["navbar_transparent"][0] == 'yes') {
                $navbarType = 'navbar-fixed-top swapLogo navbar-transparent';
                if ($custom["navbar_transparent"][0] == 'global') {
                    if (ct_get_option('pages_navbar_transparent') == 'yes') {
                        $navbarType = 'navbar-fixed-top swapLogo navbar-transparent';
                    } else {
                        $navbarType = 'navbar-fixed-top';
                    }
                }
            } else {
                $navbarType = 'navbar-fixed-top';
            }
        } else {
            // standard
            $navbarType = 'navbar-static-top';
        }


        ?>


        <?php global $wp_query;
        $arrgs = $wp_query->query_vars;
        ?>


    <?php endif; ?>
<?php endif; ?>
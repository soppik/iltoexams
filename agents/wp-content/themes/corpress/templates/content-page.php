<?php while (have_posts()) : the_post(); ?>
<?php the_content(); ?>
<?php wp_link_pages(array('before' => '<nav class="pager">', 'after' => '</nav>')); ?>
    <?php if (get_option('pages_single_show_comments')):?>
        <div class="row">
            <?php comments_template('/templates/comments.php'); ?>
        </div>
    <?php endif; ?>
<?php endwhile; ?>

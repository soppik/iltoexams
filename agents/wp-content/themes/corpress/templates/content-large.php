<?php
//The Query
global $wp_query;
$arrgs = $wp_query->query_vars;
$arrgs['posts_per_page'] = get_option('posts_per_page', 3);
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$arrgs['paged'] = $paged;
$wp_query->query($arrgs);
?>

<?php if (have_posts()) : ?>

    <div class="row" class="clearfix">
        <?php while (have_posts()) : the_post(); ?>
            <?php $format = get_post_format();
            $format = $format ? $format : 'standard';
            $class = $format == 'standard' ? 'journal' : 'journal format-' . $format;
            ?>
            <div class="col-sm-12 post">
                <article id="post-<?php the_ID(); ?>" <?php post_class($class); ?>>
                    <?php get_template_part('templates/post_large/content-' . $format); ?>
                </article>
                <hr>
            </div>
        <?php endwhile; ?>
    </div>
    <!-- / blog-list -->

    <?php if (isset($wp_query) && $wp_query->max_num_pages > 1) : ?>

        <div class="row">
        <div class="col-sm-12 text-center">

            <ul class="pagination">


                <?php if ($paged != 1): ?>
                    <li><a href="<?php echo get_previous_posts_page_link(); ?>"><i class="fa fa-angle-left"></i></a>
                    </li>
                <?php else: ?>
                    <li class="disabled"><a href="#"><i class="fa fa-angle-left"></i></a></li>
                <?php endif; ?>



                <?php for ($i = 1; $i <= $wp_query->max_num_pages; $i++) { ?>
                    <?php if ($paged == $i): ?>
                        <li class="active"><a><?php echo $i; ?></a>
                        </li>
                    <?php else: ?>
                        <li><a href=" <?php echo get_pagenum_link($i); ?>"><?php echo $i; ?></a></li>
                    <?php endif ?>
                <?php } ?>



                <?php if ($paged != $wp_query->max_num_pages): ?>
                    <li><a href="<?php echo get_next_posts_page_link(); ?>"><i class="fa fa-angle-right"></i></a>
                    </li>
                <?php else: ?>
                    <li class="disabled"><a href="#"><i class="fa fa-angle-right"></i></a></li>
                <?php endif; ?>
            </ul>
        </div>
        </div>
    <?php endif; ?>


<?php else: ?>
    <div class="col-sm-12 post"
	        <article id="post-<?php the_ID(); ?>" <?php post_class($class); ?>>
                <h2 class="no-result-found">
                    <?php _e('No search results found', 'ct_theme'); ?>
                </h2>
            </article>
    </div>
        <?php
endif;
?>



<!-- / content end -->
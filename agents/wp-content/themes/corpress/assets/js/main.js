/**
 * createIT main javascript file.
 */

(function ($) {
    "use strict";


    var $searchform;
    var $loginform;
    var $devicewidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
    var $deviceheight = (window.innerHeight > 0) ? window.innerHeight : screen.height;

    /* ========================== */
    /* ==== HELPER FUNCTIONS ==== */

    function validatedata($attr, $defaultValue) {
        if ($attr !== undefined) {
            return $attr
        }
        return $defaultValue;
    }

    function parseBoolean(str, $defaultValue) {
        if (str == 'true') {
            return true;
        } else if (str == "false") {
            return false;
        }
        return $defaultValue;
    }

    // left: 37, up: 38, right: 39, down: 40,
    // spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
    var keys = [37, 38, 39, 40];

    function preventDefault(e) {
        e = e || window.event;
        if (e.preventDefault) {
            e.preventDefault();
        }
        e.returnValue = false;
    }

    function keydown(e) {
        for (var i = keys.length; i--;) {
            if (e.keyCode === keys[i]) {
                preventDefault(e);
                return;
            }
        }
    }

    function wheel(e) {
        preventDefault(e);
    }

    function disable_scroll() {
        if (window.addEventListener) {
            window.addEventListener('DOMMouseScroll', wheel, false);
        }
        window.onmousewheel = document.onmousewheel = wheel;
        document.onkeydown = keydown;
    }

    function enable_scroll() {
        if (window.removeEventListener) {
            window.removeEventListener('DOMMouseScroll', wheel, false);
        }
        window.onmousewheel = document.onmousewheel = document.onkeydown = null;
    }

    $(document).ready(function () {

        // start with (because fixed navigation)
        $(window).trigger("scroll");


        // menu for onepager
        jQuery("#nav > li > a[href^='/#']").each(function () {
            var $this = jQuery(this);
            jQuery($this.parent()).removeClass("active").addClass("onepage");
            // remove unnecessary active classes
            $this.parent().removeClass("active");
        });

        // remove unnecessary active classes
        jQuery("#nav > li > a[href='/']").parent().removeClass("active");

        if (jQuery("body").hasClass("home") || jQuery("body").hasClass('home-sidebar')) {
            //page scroll
            if (jQuery("#nav > li").hasClass("onepage")) {
                    $('#nav').onePageNav({
                        currentClass: 'active',
                        changeHash: false,
                        scrollSpeed: 750,
                        scrollThreshold: 0.7,
                        filter: '',
                        easing: 'easeOutExpo',
                        begin: function () {
                            //Hack so you can click other menu items after the initial click
                            $('body').append('<div id="device-dummy" style="height: 1px;"></div>');
                        },
                        end: function () {
                            $('#device-dummy').remove();
                        },
                        scrollChange: function ($currentListItem) {
                            //I get fired when you enter a section and I pass the list item of the section
                        }
                    });
            }
        }


        /* ======================================= */
        /* === CLICKABLE MAIN PARENT ITEM MENU === */
        $(".navbar li.dropdown > .dropdown-toggle").removeAttr("data-toggle data-target");
        if($devicewidth < 768){
            $(".navbar li.dropdown > .dropdown-toggle").click(function(e){
                e.preventDefault();
                $(this).parent().toggleClass('open');
            })
            $(".navbar li.ubermenu-has-submenu-drop > .ubermenu-target").click(function(e){
                e.preventDefault();
                $(this).parent().toggleClass('open');
            })
        }

        /* =============================== */
        /* === RESPONSIVE MENU SIDEBAR === */
        /*$(".navbar-toggle").click(function(){
         $("body").toggleClass("disable-scroll");
         $("html").toggleClass("disable-scroll");
         })*/

        if ($devicewidth < 768) {
            var snapper = new Snap({
                element: document.getElementById('wrapper'),
                disable: 'right',
                maxPosition: 260,
                minPosition: 0,
                addBodyClasses: true
            });

            $(".navbar-toggle").click(function () {
                snapper.open('left');
            });
        }

        /* ====================== */
        /* === SIDEBAR TOGGLE === */
        $(".navbar-sidebar-toggle").click(function () {
            if ($("nav.navbar").is(".open")) {
                $("nav.navbar").removeClass("open");
            } else {
                $("nav.navbar").addClass("open");
            }
        });

        $(document).on({
            mouseenter: function (e) {
                //stuff to do on mouse enter
                if (($(e.target).is(".showHeaderSearch") )) {
                } else {
                    $("nav.navbar").addClass("open");
                }
            }, mouseleave: function (e) {
                //stuff to do on mouse leave
                if (($(e.target).is(".showHeaderSearch") )) {
                } else {
                    $("nav.navbar").removeClass("open");
                }
            }
        }, ".navbar-sidebar .navbar");


        $('html').click(function () {
            $("nav.navbar").removeClass("open");
        });

        $('nav.navbar').click(function (event) {
            event.stopPropagation();
        });
        /* =============================================== */
        /* === SET WIDTH IN DROPDOWNS FOR SIDEBAR MENU === */

        if ($("body").hasClass("navbar-sidebar") && $devicewidth > 768) {
            $(".yamm-dropdown").each(function () {
                $(this).css("width", $(this).attr("data-width"))
            })
        }

        /* =================================== */
        /* ==== BUTTON SCROLL INSIDE PAGE ==== */


        $('.btn-scroll[href^="#"]').on('click', function (e) {
            e.preventDefault();

            var target = this.hash, $target = $(target);

            $('html, body').stop().animate({
                'scrollTop': $target.offset().top
            }, 900, 'swing', function () {
                window.location.hash = target;
            });
        });

        /* =============================== */
        /* === ADD MARGINS, COLORS TO ELEMENTS === */

        if ($devicewidth > 480) {
            $('*[data-margin-top]').each(function () {
                $(this).css("margin-top", $(this).data('margin-top'))
            })
            $('*[data-margin-bottom]').each(function () {
                $(this).css("margin-bottom", $(this).data('margin-bottom'))
            })
            $('*[data-margin-left]').each(function () {
                $(this).css("margin-left", $(this).data('margin-left'))
            })
            $('*[data-margin-right]').each(function () {
                $(this).css("margin-right", $(this).data('margin-right'))
            })
        }

        $('*[data-color]').each(function () {
            $(this).css("color", '#' + $(this).data('color').replace('#', ''));
        });

        /* ================================= */
        /* ==== FIT VIDEOS TO CONTAINER ==== */

        if ($().fitVids) {
            $(".video").fitVids();
            $(".videoFrameContainer").fitVids();
        }

        /* ========================================== */
        /* ==== SHOW HEADER SEARCH/ HEADER LOGIN ==== */

        $searchform = $(".header-search");
        $(".showHeaderSearch").click(function () {
            $searchform.fadeToggle(250, function () {
                if (($searchform).is(":visible")) {
                    $searchform.find("[type=text]").focus();
                    disable_scroll()
                } else {
                    enable_scroll()
                }
            });
            return false;
        });
        $loginform = $(".header-login");
        $(".btn-login-toggle").click(function () {
            $loginform.fadeToggle(250, function () {
                if (($loginform).is(":visible")) {
                    $loginform.find("[type=text]").focus();
                    disable_scroll()
                } else {
                    enable_scroll()
                }
            });
            return false;
        });

        /* =============================== */
        /* ==== TOOLTIPS AND POPOVERS ==== */

        $("[data-toggle='tooltip']").tooltip();

        $("[data-toggle='popover']").popover({trigger: "hover", html: true});


        /* ======================= */
        /* ==== TO TOP BUTTON ==== */


        $('#backtoTop').click(function () {
            $("body,html").animate({scrollTop: 0}, 1200);
            return false;
        });


        // ie9 requires for labels
        $('.ie9 .form-control').blur(function () {
            if (!$.trim(this.value).length) { // zero-length string AFTER a trim

            } else {
                $(this).addClass("notempty");
            }
        });

    });

    $(window).scroll(function () {

        var scroll = $(window).scrollTop();
        /* ================================================= */
        /* === CHANGE TRANSPARENT NAVBAR COLOR ON SCROLL === */

        var $bodyel = $("body");

        if (($bodyel.hasClass("navbar-fixed") && $bodyel.hasClass("navbar-transparent")) || ($bodyel.hasClass("navbar-fixed") && $bodyel.hasClass("revert-to-transparent"))) {
            if (scroll >= 100) {
                $bodyel.removeClass("navbar-transparent");
                $bodyel.addClass("navbar-dark");
                $bodyel.addClass("hide-topbar");
                $bodyel.addClass("revert-to-transparent");
            } else {
                $bodyel.removeClass("navbar-dark");
                $bodyel.removeClass("hide-topbar");
                $bodyel.addClass("navbar-transparent");
            }
        }

        // fixed navbar
        if ($bodyel.is(".navbar-fixed.with-topbar")) {
            if (scroll >= 100) {
                $bodyel.addClass("hide-topbar");
                if (!($bodyel.is(".revert-to-transparent"))) {
                    $bodyel.addClass("navbar-with-shadow");
                }
            } else {
                $bodyel.removeClass("hide-topbar navbar-with-shadow");
            }
        }
    })

    $(window).load(function () {


        /* ====================== */
        /* ==== BLOG MASONRY ==== */

        if (($().isotope ) && ($('.with-isotope').length > 0) && !device.mobile() && !($devicewidth < 768)) {

            // blog masonry

            var $blogContainer = $('.with-isotope'), // object that will keep track of options
                isotopeOptions = {}, // defaults, used if not explicitly set in hash
                defaultOptions = {
                    itemSelector: '.post', // set columnWidth to a percentage of container width
                    masonry: {}
                };

            // set up Isotope
            $blogContainer.isotope(defaultOptions, function () {

                // fix for height dynamic content
                setTimeout(function () {
                    $blogContainer.isotope('reLayout');
                }, 1000);

            });
        }


    });


    $(document).keyup(function (e) {
        if (e.keyCode == 27) {

            if (($searchform).is(":visible")) {
                $searchform.fadeToggle(250);
                enable_scroll()
            }
            if (($loginform).is(":visible")) {
                $loginform.fadeToggle(250);
                enable_scroll()
            }
        }
    });


})(jQuery);

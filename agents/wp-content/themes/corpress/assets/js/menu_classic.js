/**
 * Created by User on 2014-09-02.
 */
var $devicewidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
var $deviceheight = (window.innerHeight > 0) ? window.innerHeight : screen.height;

jQuery(document).ready(function(){

    /* ******************
     MAGIC BORDER IN MENU
     ******************** */

    if($devicewidth > 767){
        $(function() {

            var $el, leftPos, newWidth,
                $mainNav = $(".navbar-nav");

            if($mainNav.length > 0){
                $mainNav.each(function(){
                    var $this = jQuery(this);
                    if($this.find("> li.active").length > 0){
                        $this.append("<li id='magic-line'></li>");
                        var $magicLine = $("#magic-line");

                        if($("body").hasClass("navbar-sidebar")){
                            $magicLine
                                .css("width", "2px")
                                .height($mainNav.find("> li.active").height())
                                .css("top", $mainNav.find("> li.active").position().top)
                                .data("origTop", $magicLine.position().top)

                            $(".navbar-nav > li").hover(function() {
                                $el = $(this);
                                topPos = $el.position().top;
                                $magicLine.stop().animate({
                                    top: topPos
                                });
                            }, function() {
                                if($('body').hasClass('onepager')){
                                    $magicLine.stop().animate({
                                        top: $mainNav.find("> li.active").position().top
                                    });
                                } else{
                                    $magicLine.stop().animate({
                                        top: $magicLine.data("origTop")
                                    });
                                }
                            });
                            if(jQuery().attrchange){
                                $mainNav.find('> li:not(#magic-line)').attrchange({
                                    trackValues: true, /* Default to false, if set to true the event object is
                                     updated with old and new value.*/
                                    callback: function (event) {
                                        //event    	          - event object
                                        //event.attributeName - Name of the attribute modified
                                        //event.oldValue      - Previous value of the modified attribute
                                        //event.newValue      - New value of the modified attribute
                                        //Triggered when the selected elements attribute is added/updated/removed
                                        $magicLine.stop().animate({
                                            top: $mainNav.find("> li.active").position().top
                                        });
                                    }
                                });
                            }
                        } else{
                            $magicLine
                                .width($mainNav.find("> li.active").width())
                                .css("left", $mainNav.find("> li.active").position().left)
                                .data("origLeft", $magicLine.position().left)
                                .data("origWidth", $magicLine.width());

                            $(".navbar-nav > li").hover(function() {
                                $el = $(this);
                                leftPos = $el.position().left;
                                newWidth = $el.width();
                                $magicLine.stop().animate({
                                    left: leftPos,
                                    width: newWidth
                                });
                            }, function() {
                                if($('body').hasClass('onepager')){
                                    $magicLine.stop().animate({
                                        left: $mainNav.find("> li.active").position().left,
                                        width: $mainNav.find("> li.active").width()
                                    });
                                } else{
                                    $magicLine.stop().animate({
                                        left: $magicLine.data("origLeft"),
                                        width: $magicLine.data("origWidth")
                                    });
                                }
                            });
                            if(jQuery().attrchange){
                                $mainNav.find('> li:not(#magic-line)').attrchange({
                                    trackValues: true, /* Default to false, if set to true the event object is
                                     updated with old and new value.*/
                                    callback: function (event) {
                                        //event    	          - event object
                                        //event.attributeName - Name of the attribute modified
                                        //event.oldValue      - Previous value of the modified attribute
                                        //event.newValue      - New value of the modified attribute
                                        //Triggered when the selected elements attribute is added/updated/removed
                                        $magicLine.stop().animate({
                                            left: $mainNav.find("> li.active").position().left,
                                            width: $mainNav.find("> li.active").width()
                                        });
                                    }
                                });
                            }
                        }
                    }
                })
            }
        });
    }
})

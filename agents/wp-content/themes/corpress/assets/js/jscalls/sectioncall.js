(function ($) {
    $(document).ready(function () {
        /* ============================ */
        /* ==== SECTION BACKGROUND ==== */

        $(".section").each(function () {
            var $this = $(this);
            $this.css('background-color', $this.attr("data-bg-color"));
        })
        $(".section-top").each(function () {
            var $this = $(this);
            $this.css('background-color', $this.attr("data-bg-color"));
        })
        $(".section-bottom").each(function () {
            var $this = $(this);
            $this.css('background-color', $this.attr("data-bg-color"));
        })

        $(".media-section[data-type='background']").each(function () {
            var $this = $(this);
            $this.css('background-image', 'url("' + $this.attr("data-image") + '")');
        })


        /* ================== */
        /* ==== PARALLAX ==== */

        if (!device.mobile() && !device.tablet()) {
            $(".media-section[data-type='parallax']").each(function () {

                var $this = $(this);
                var $bg = $this.attr("data-image");
                var $height = $this.attr("data-height");

                $this.css('background-image', 'url("' + $bg + '")');
                if ($height.indexOf("%") > -1) {
                    $this.css('min-height', $deviceheight);
                    $this.css('height', $deviceheight);
                } else {
                    $this.css('min-height', $height + "px");
                    $this.css('height', $height + "px");
                }
            })
        } else {
            $(".media-section[data-type='parallax']").each(function () {
                var $this = $(this);
                var $bg = $this.attr("data-image-mobile");
                var $height = $this.attr("data-height");

                if ($height.indexOf("%") > -1) {
                    $this.css('min-height', $deviceheight);
                    $this.css('height', $deviceheight);
                } else {
                    $this.css('min-height', $height + "px");
                    $this.css('height', $height + "px");
                }
                $this.css('background-image', 'url("' + $bg + '")');
            })
        }

        function makekenburns($element) {
            // we set the "fx" class on the first image
            // when the page loads
            $element.find("img")[0].className = "fx";
            // the third variable is to keep track of
            // where we are in the loop
            // if it is set to *1* (instead of 0)
            // it is because the first image is styled
            // when the page loads
            var images = $element.find("img"), numberOfImages = images.length, i = 1;
            if (numberOfImages == 1) {
                images[0].className = "singlefx";
            }
            // this calls the kenBurns function every
            // 4 seconds. You can increase or decrease
            // this value to get different effects
            window.setInterval(kenBurns, 7000);

            function kenBurns() {
                if (numberOfImages != 1) {
                    if (i == numberOfImages) {
                        i = 0;
                    }
                    images[i].className = "fx";
                    if (i === 0) {
                        images[numberOfImages - 2].className = "";
                    }
                    if (i === 1) {
                        images[numberOfImages - 1].className = "";
                    }
                    if (i > 1) {
                        images[i - 2].className = "";
                    }
                    i++;
                }
            }
        }

        // add background position for parallax. fix for ipad and iphone
        if (!device.mobile() && !device.tablet()) {
            $(".media-section[data-type='parallax']").css('background-attachment', 'fixed');
        } else {
            $(".media-section[data-type='parallax']").css('background-attachment', 'scroll');
        }

        /* =================== */
        /* ==== KEN BURNS ==== */

        $(".media-section[data-type='kenburns']").each(function () {
            var $this = $(this);
            var $height = $this.attr("data-height");

            if ($height.indexOf("%") > -1) {
                $this.css('min-height', $deviceheight);
                $this.css('height', $deviceheight);
            } else {
                $this.css('min-height', $height + "px");
                $this.css('height', $height + "px");
            }
            if ($devicewidth > 767) {
                makekenburns($this.find('.media-section-image-container'));
            } else {
                var images = $this.find('.media-section-image-container img');
                images.each(function () {
                    var image = $(this)

                    if (!image.is(":first-child")) {
                        image.remove();
                    }
                })
            }
        });
        /* ======================= */
        /* ==== VIDEO SECTION ==== */

        $(".media-section[data-type='video']").each(function () {
            var $this = $(this);
            var $height = $this.attr("data-height");
            var $time = 1;

            if ($height.indexOf("%") > -1) {
                $this.css('min-height', $deviceheight);
                $this.find('> .display-table').css('height', $deviceheight);
            } else {
                $this.css('min-height', $height + "px");
                $this.find('> .display-table').css('height', $height + "px");
            }

            if (!$this.hasClass("html5")) {
                var $videoframe = $this.find('iframe')
                if ($videoframe.attr('data-startat')) {
                    $time = $videoframe.attr('data-startat');
                }
                if (!($devicewidth < 992) && !device.mobile()) {
                    if (typeof $f != 'undefined') {
                        var $video = '#' + $videoframe.attr('id');
                        var iframe = $($video)[0], player = $f(iframe), status = $('.status');


                        player.addEvent('ready', function () {
                            player.api('setVolume', 0);
                            player.api('seekTo', $time);
                        })
                    }
                }
            } else {
                //THIS IS WHERE YOU CALL THE VIDEO ID AND AUTO PLAY IT. CHROME HAS SOME KIND OF ISSUE AUTOPLAYING HTML5 VIDEOS, SO THIS IS NEEDED
                //document.getElementById('video1').play();
            }
            if (($devicewidth < 992) || device.mobile()) {
                $this.find(".video").css('display', 'none');
                if ($this.attr("data-fallback-image") !== undefined) {
                    $this.css('background-image', 'url("' + $this.attr("data-fallback-image") + '")');
                }
            }
        });
    });

    $(window).load(function () {

        /* ================== */
        /* ==== PARALLAX ==== */

        if ($().stellar) {
            if (!device.mobile() && !device.tablet()) {
                //initialise Stellar.js
                $(window).stellar({
                    horizontalScrolling: false, responsive: true, positionProperty: 'transform'
                });
            }

        }
    });
})(jQuery);
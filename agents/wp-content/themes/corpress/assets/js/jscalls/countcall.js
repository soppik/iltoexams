/* ================== */
/* ==== COUNT TO ==== */


if ((jQuery().countTo) && (jQuery("body").hasClass("cssAnimate"))) {
    jQuery(".counter").data("countToOptions", {
        formatter: function (value, options) {
            return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, " ");
        }
    }).appear(function () {
        jQuery(this).each(function (options) {
            var $this = jQuery(this);
            var $speed = $this.attr("data-speed") !== undefined ? $this.attr("data-speed") : 700;
            options = jQuery.extend({}, options || {
                speed: $speed
            }, $this.data("countToOptions") || {});
            $this.countTo(options);
        });
    });
} else {
    jQuery(".counter").data("countToOptions", {
        formatter: function (value, options) {
            return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, " ");
        }
    });
    jQuery(".counter").each(function (options) {
        var $this = jQuery(this);
        var $speed = $this.attr("data-speed") !== undefined ? $this.attr("data-speed") : 1200;
        options = jQuery.extend({}, options || {
            speed: $speed
        }, $this.data("countToOptions") || {});
        $this.countTo(options);
    });
}
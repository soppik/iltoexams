(function ($) {
    "use strict";

    $(window).load(function () {
        /* ============================ */
        /* ==== THUMBNAIL SCROLLER ==== */

        if ($().thumbnailScroller) {
            $(".thumbnailScroller").each(function () {
                var $this = $(this);
                var $scrolltype = "hoverAccelerate";
                if (device.mobile() || device.tablet()) {
                    $scrolltype = "clickButtons"
                }
                $this.thumbnailScroller({
                    scrollerType: $scrolltype,
                    scrollerOrientation: "horizontal", //scrollSpeed: 2,
                    scrollEasing: "easeOutCirc",
                    scrollEasingAmount: 600,
                    acceleration: 4,
                    scrollSpeed: 800,
                    noScrollCenterSpace: 10,
                    autoScrolling: 0,
                    autoScrollingSpeed: 2000,
                    autoScrollingEasing: "easeInOutQuad",
                    autoScrollingDelay: 500
                });
                center_slider($this);
                show_slider($this);
            })
        }
        //CENTER SLIDER
        function center_slider(element) {
            var slider_width = $(element).find('.jTscroller').outerWidth(true);
            if (slider_width > $devicewidth) {
                $(element).find('.jTscroller').css('left', -( (slider_width - $devicewidth) / 2 ) + 'px');
            } else {
                $(element).find('.jTscroller').css('left', ( ($devicewidth - slider_width) / 2 ) + 'px');
            }
        }

        //SHOW SLIDER
        function show_slider(element) {
            element.removeClass("thumbnailScrollerLoading");
        }
    })
})(jQuery);
/**
 * Created by User on 2014-09-02.
 */

var $devicewidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
var $deviceheight = (window.innerHeight > 0) ? window.innerHeight : screen.height;

jQuery(document).ready(function(){

    /* ******************
     MAGIC BORDER IN MENU
     ******************** */

    if($devicewidth > 767){
        $(function() {

            var $el, leftPos, newWidth,
                $mainNav = $(".ubermenu-nav");

            if($mainNav.length > 0){
                $mainNav.each(function(){
                    var $this = jQuery(this);
                    var $link = $("> li.ubermenu-current-menu-item", $this).add("> li.ubermenu-current-menu-ancestor", $this);
                    if($link.length > 0){
                        $this.append("<li id='magic-line'></li>");
                        var $magicLine = $("#magic-line");

                        $magicLine
                            .width($link.width())
                            .css("left", $link.position().left)
                            .data("origLeft", $magicLine.position().left)
                            .data("origWidth", $magicLine.width());

                        $(".ubermenu-nav > li").hover(function() {
                            $el = $(this);
                            leftPos = $el.position().left;
                            newWidth = $el.width();
                            $magicLine.stop().animate({
                                left: leftPos,
                                width: newWidth
                            });
                        }, function() {
                            if($('body').hasClass('onepager')){
                                $magicLine.stop().animate({
                                    left: $link.position().left,
                                    width: $link.width()
                                });
                            } else{
                                $magicLine.stop().animate({
                                    left: $magicLine.data("origLeft"),
                                    width: $magicLine.data("origWidth")
                                });
                            }
                            if(jQuery().attrchange){
                                $mainNav.find('> li:not(#magic-line)').attrchange({
                                    trackValues: true, /* Default to false, if set to true the event object is
                                     updated with old and new value.*/
                                    callback: function (event) {
                                        //event    	          - event object
                                        //event.attributeName - Name of the attribute modified
                                        //event.oldValue      - Previous value of the modified attribute
                                        //event.newValue      - New value of the modified attribute
                                        //Triggered when the selected elements attribute is added/updated/removed
                                        $magicLine.stop().animate({
                                            left: $link.position().left,
                                            width: $link.width()
                                        });
                                    }
                                });
                            }
                        })
                    }
                });
            }
        })
    }
})
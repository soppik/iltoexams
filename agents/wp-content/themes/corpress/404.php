<!-- / fullwidth -->
</div>


<section class="media-section darkbg" data-stellar-background-ratio="0.5" data-height="220" data-type="parallax" data-image="<?php echo CT_THEME_ASSETS?>/images/content/parallax10.jpg" data-image-mobile="assets/images/demo-content/parallax10-mobile.jpg" style="min-height: 220px; height: 220px; background-image: url(http://corpress.html.themeforest.createit.pl/assets/images/demo-content/parallax10.jpg); background-attachment: fixed; background-position: 50% 50%;">

    <div class="inner">
        <div class="text-center">
            <h2 class="uppercase no-margin"><?php echo __('OOOOPS!','ct_theme')?></h2>
        </div>
    </div>
</section>

<section class="container section">
    <div class="row section">
        <div class="col-sm-6">
           <span class="text404">
               <?php echo __('404','ct_theme')?>
               <small><?php echo __('Page not found','ct_theme')?></small>
           </span>
        </div>
        <div class="col-sm-6">
            <h2 class="uppercase hr-left"><?php echo __('Houston','ct_theme')?>,<br> <?php echo __('We have a problem here.','ct_theme')?></h2>
            <p><?php echo __('It looks like the page you were looking for cannot be found. It probably never existed or you used broken link.','ct_theme')?>
            </p>
            <a href="mailto:<?php echo get_bloginfo('admin_email')?> "><?php echo __('Report this to us','ct_theme')?></a>
            <br>
            <br>
            <a href="<?php echo get_site_url(); ?>"><?php echo __('Go to homepage','ct_theme')?></a>
        </div>
    </div>
</section>
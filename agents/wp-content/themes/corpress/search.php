    <!--container!--></div>
<?php get_template_part('templates/page', 'head'); ?>

<?php $breadcrumbs = ct_show_index_post_breadcrumbs('post') ? 'yes' : 'no'; ?>

<?php $pageTitle = __('Search results', 'ct_theme'); ?>
<?php echo do_shortcode('[title_row header="' . $pageTitle . '" breadcrumbs="' . $breadcrumbs . '"]') ?>


<div class="container">
    <section class="container section">


        <div class="row">
            <div class="col-sm-8 col-md-9">
                <?php get_template_part('templates/content'); ?>

            </div>
            <!--col-sm-8 end!-->


            <div class="col-sm-4 col-md-3 sidebar">
                <?php get_template_part('templates/sidebar') ?>
            </div>
            <!--col-sm-4 end!-->

        </div>
        <!--row_end!-->

    </section>



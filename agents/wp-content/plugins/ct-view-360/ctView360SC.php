<?php

/**
 * Create shortcode View360
 */
class ctView360SC {
    protected $active = true;

    /**
	 *  Get Last id
	 */


	protected function getLastId() {
		$args = array(
			'post_type'   => ctView360Custom::POST_TYPE_NAME,
			'post_status' => 'publish',
		);

		$my_posts = get_posts( $args );
		wp_reset_postdata();
		if($my_posts){
			$lastpost = $my_posts[0];

			return $lastpost->ID;

		}
		return "";

	}

	/**
	 *  Get Custom Values
	 */

	public static function getCustomValue( $post_id ) {
		$custom = get_post_custom( $post_id );

		return $custom;
	}

	public function __construct() {

		add_shortcode( 'view360', array( $this, 'shortcodeView360' ) );
	}


    /**
     * @param $custom
     * @param $atts
     * @param $key
     * @return string
     * checking value
     */
    protected function getBoolValue($custom, $atts, $key) {

        // add attr
        if ( isset( $atts[$key] ) ) {
            $attr = $atts[$key];
        } else {
            $attr = isset( $custom["my_meta_box_select_" . $key][0] ) ? $custom["my_meta_box_select_" . $key][0] : "off";
        }

        if ( $attr == "off" || $attr == '' || $attr == "no" ) {
            $attr = "false";
        }
        if ( $attr == "on" || $attr == "yes" ) {
            $attr = "true";
        }
        return $attr;

        }

	/**
	 * @param $atts
	 * @param null $content
	 *
	 * @return string
	 */



	public function shortcodeView360( $atts, $content = null ) {
		// Attributes
        //var_dump($atts);exit;


		extract( shortcode_atts( array(
				'speed',
				'id',
				'cw',
				'touch',
				'delay',
				'display',
				'responsive',
				'progress',
				'height',
				'width',
				'cursor',
				'draggable',
				'click',
				'frames',
				'footage',
				'scroll'
			), $atts )
		);



        // $id = ['id']
        if (isset($atts['id']) && !empty ($atts['id'])) {
            $id = $atts['id'];
        } else {
            $id = $this->getLastId();
        }

		// $post_id = last id
		$post_id = $id;
		$custom  = CTView360SC::getCustomValue( $post_id );

		$images = '';

		//   $current = get_current_screen()->action;

		//we have to check if there is already size guide post attached to the product


		$image_gallery = get_post_meta( $post_id, 'image_gallery', true );


		$attachments   = array_filter( explode( ',', $image_gallery ) );

		$height_p = 0;
		$widht_p = 0;

		$counter = 0;
		$count   = count( $attachments );
		foreach ( $attachments as $attachment_id ) {
			$get_image_id = wp_get_attachment_image_src( $attachment_id, 'Full' );
			$imageUrl     = $get_image_id[0];
			$widht_p      = $get_image_id[1];
			$height_p     = $get_image_id[2];
			IF ( $counter >= $count - 1 ) {
				$images .= $imageUrl;
			} else {
				$images .= $imageUrl . ', ';
			}
			$counter ++;
		}


        $draggable = $this->getBoolValue($custom, $atts, 'draggable');

        $responsive = $this->getBoolValue($custom, $atts, 'responsive');

        $cw = $this->getBoolValue($custom, $atts, 'cw');

        $scroll = $this->getBoolValue($custom, $atts, 'scroll');

        $click = $this->getBoolValue($custom, $atts, 'click');

        $touch = $this->getBoolValue($custom, $atts, 'touch');

		//add frames
		if ( isset ( $atts['frames'] ) ) {
			$n_frames = $atts['frames'];
		} else {
			$n_frames = isset( $custom["my_meta_box_n_frames"][0] ) ? $custom["my_meta_box_n_frames"][0] : "0";
		}

		// add footege

		if ( isset ( $atts['footage'] ) ) {
			$n_footage = $atts['footage'];
		} else {
			$n_footage = isset( $custom["my_meta_box_n_footage"][0] ) ? $custom["my_meta_box_n_footage"][0] : "1";
		}

		// add delay
		if ( isset ( $atts['delay'] ) ) {
			$delay = $atts['delay'];
		} else {
			$delay = isset( $custom["my_meta_box_time"][0] ) ? $custom["my_meta_box_time"][0] : "";
		}

		//add display
		if ( isset ( $atts['display'] ) ) {
			$display = $atts['display'];
		} else {
			$display = isset( $custom["my_meta_box_display"][0] ) ? $custom["my_meta_box_display"][0] : "";
		}


		// add atts speed
		if ( isset ( $atts['speed'] ) ) {
			$speed = $atts['speed'];
		} else {
			$speed = isset( $custom["my_meta_box_speed"][0] ) ? $custom["my_meta_box_speed"][0] : "0";
		}

		// $cursor =['cursor']
		if ( isset( $atts['cursor'] ) ) {
			$cursor = $atts['cursor'];
		} else {
			$cursor = isset( $custom["my_meta_box_select_cursor"][0] ) ? $custom["my_meta_box_select_cursor"][0] : "false";
		}

		if ( $cursor == "false" || $cursor == "arrow" || $cursor == "" ) {
			$cursor = '';
		} elseif ( $cursor == "true" || $cursor == "hand" ) {
			$cursor = "hand";
		}


		// $n_height = ['height']
		if ( isset( $atts['height'] ) ) {
			$n_height = $atts['height'];
		} else {
			$n_height = isset( $custom["my_meta_box_stitched_height"][0] ) ? $custom["my_meta_box_stitched_height"][0] : "";
		}
		if ( $n_height == "" || $n_height == "0" ) {
			$n_height = "400";
		}

		// $n_width = ['width']

		if ( isset( $atts['width'] ) ) {
			$n_width = $atts['width'];
		} else {
			$n_width = isset( $custom["my_meta_box_stitched_width"][0] ) ? $custom["my_meta_box_stitched_width"][0] : "";
		}
		if ( $n_width == "" || $n_width == "0" ) {
			$n_width = "400";
		}

		//$indicator
		if ( isset( $atts['progress'] ) ) {
			$indicator = $atts['progress'];
		} else {
			$indicator = isset( $custom["my_meta_box_select_indicator"][0] ) ? $custom["my_meta_box_select_indicator"][0] : "off";
		}

		if ( $indicator == "off" || $indicator == '' || $indicator == "no" || $indicator == "false" ) {
			$indicator = "";
		}
		if ( $indicator == "on" || $indicator == "yes" ) {
			$indicator = "5";
		}

		// checking whether the photo is a panoramic
		if ( $counter == 1 && $height_p < $widht_p ) {
			$size_panorama = $widht_p;
		} else {
			$size_panorama = "0";
		}

		// Rand int for class in html

		$idimage = 'image' . rand( 1, 1000 ) . '';

		// add sufix to data-image

		$sufix = ( $counter > 1 ) ? "s" : '';

        $loder_activ = esc_attr(get_option('my_meta_box_select_loder'));
        $src = esc_attr(get_option('my_meta_box_loder_url'));

        if ( $loder_activ == 'on' &&  $src == '' ){

            $src =  CT_VIEW360_ASSETS . '/img/three-dots.svg' ;

        }elseif ($loder_activ == 'on'){

            $src = esc_attr(get_option('my_meta_box_loder_url'));
        }else {
            $src ='';

        }



		$html = '
    <img  src="  ' .esc_attr($src) . '" width=' . $n_width . ' height=' . $n_height . '
            class="reel loder"
            id="' . $idimage . '"
            data-image' . $sufix . '= "' . $images . '"
            data-speed="' . $speed . '"
            data-footage="' . $n_footage . '"
            data-cw= "' . $cw . '"
            data-inversed="false"
            data-responsive= "' . $responsive . '"
            data-orientable = "' . $touch . '"
            data-opening="' . $display . '"
            data-delay="' . $delay . '"
            data-indicator="' . $indicator . '"
            data-cursor="' . $cursor . '"
            data-draggable="' . $draggable . '"
            data-clickfree="' . $click . '"
            data-frames="' . $n_frames . '"
            data-stitched="' . $size_panorama . '"
            data-row="1"
            data-wheelable="' . $scroll . '"

            >
';

		return $html;
	}

}

new ctView360SC();
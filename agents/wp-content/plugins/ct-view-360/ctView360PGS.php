<?php

/**
 * Create Admin Settings
 */

class ctView360info
{


    public function __construct()
    {

        add_action('admin_menu', array($this, 'addAdminMenu'));
        // register settings function


        add_action('admin_init', array($this, 'registerSettings'));


        add_action('admin_enqueue_scripts', array($this, 'wp_enqueue_color_picker'));
    }

    public function wp_enqueue_color_picker()
    {
        wp_enqueue_style('wp-color-picker');

        wp_enqueue_script('wp-color-picker');
    }

    /**
     *
     *  Add page Global Setting
     */

    public function addAdminMenu()
    {

        add_submenu_page('edit.php?post_type=view360', 'Custom Post Type view360', __('Global Settings',  'ct_v360' ) , 'manage_options', 'view_my_post_site', array($this, 'postView360Page'));
    }

    /**
     *  Register Global Setting
     */

    function registerSettings()
    {
        //register our settings

        foreach (array(
                     'my_meta_box_select_cw',
                     'my_meta_box_speed',
                     'my_meta_box_select_touch',
                     'my_meta_box_time',
                     'my_meta_box_display',
                     'my_meta_box_select_indicator',
                     'my_meta_box_select_responsive',
                     'my_meta_box_select_cursor',
                     'my_meta_box_select_draggable',
                     'my_meta_box_select_click',
                     'my_meta_box_n_frames',
                     'my_meta_box_n_footage',
                     'my_meta_box_stitched_width',
                     'my_meta_box_stitched_height',
                     'my_meta_box_select_open_guide',
                     'my_meta_box_select_button_position',
                     'my_meta_box_button_label',
                     'my_meta_box_select_button_align',
                     'my_meta_box_button_class',
                     'my_meta_box_button_v_left',
                     'my_meta_box_button_m_top',
                     'my_meta_box_button_m_right',
                     'my_meta_box_button_m_bottom',
                     'my_meta_box_button_p_color',
                     'my_meta_box_button_p_left',
                     'my_meta_box_button_p_top',
                     'my_meta_box_button_p_right',
                     'my_meta_box_button_p_bottom',
                     'my_meta_box_button_bb_color',
                     'my_meta_box_button_control',
                     'my_meta_box_select_scroll',
                     'my_meta_box_loder_url',
                     'my_meta_box_select_loder'
                 ) as $paramts ) {

            register_setting('ct-view-360', $paramts );

        }

    }

    /**
     *  Create page Global Setting
     */

    public function postView360Page()
    {

        ?>
        <script>

            jQuery(document).ready(function () {

                if (jQuery.fn.wpColorPicker) {
                    jQuery('#my_meta_box_button_bb_color').wpColorPicker();
                }
                if (jQuery.fn.wpColorPicker) {
                    jQuery('#my_meta_box_button_p_color').wpColorPicker();
                }
            });
        </script>
        <div xmlns="http://www.w3.org/1999/html">
            <h2 align="left"> <?php echo __(' Global Settings View360.',  'ct_v360' ) ?>  </h2>
            <h4><?php echo __(' Check the <a href="http://createit.support/documentation/view360/">documentation</a> for more information.',  'ct_v360' ) ?> </h4>
        </div>

        <form method="post" action="options.php">
        <?php settings_fields('ct-view-360'); ?>
        <?php do_settings_sections('ct-view-360'); ?>

    <div class="classname">

    <div class="frame">

    <table class="form-table">
        <tbody>

        <tr>
            <th scope="row"><label for="my_meta_box_stitched_width"><?php echo __('Width',  'ct_v360' ) ?> </label></th>
            <td>
                <input type="text" size="8" name="my_meta_box_stitched_width" id="my_meta_box_stitched_width"
                       value="<?php echo esc_attr(get_option('my_meta_box_stitched_width')); ?>"
                       placeholder="Default 400"/>
                <span class="description"> <?php echo __('Width of Window Frames (You can also use percentages)',  'ct_v360' ) ?>  </span>
            </td>
        </tr>

        <tr>
            <th scope="row"><label for="my_meta_box_stitched_height"><?php echo __('Height',  'ct_v360' ) ?>  </label></th>
            <td>
                <input type="text" size="8" name="my_meta_box_stitched_height" id="my_meta_box_stitched_height"
                       value="<?php echo esc_attr(get_option('my_meta_box_stitched_height')); ?>"
                       placeholder="Default 400"/>
                <span class="description"> <?php echo __('Height of Window Frames (You can also use percentages)',  'ct_v360' ) ?> </span>
            </td>
        </tr>
        <tr>
            <th scope="row"><label for="my_meta_box_n_frames"> <?php echo __('Number of frames',  'ct_v360' ) ?> </label></th>
            <td>
                <input type="text" class="small-text" name="my_meta_box_n_frames" id="my_meta_box_n_frames"
                       value="<?php echo esc_attr(get_option('my_meta_box_n_frames')); ?>" placeholder="0"/>
                <span class="description">  <?php echo __('(This Option Is for Panorama image) Total number of frames.',  'ct_v360' ) ?></span>
            </td>
        </tr>

        <tr>
            <th scope="row"><label for="my_meta_box_n_footage"> <?php echo __('Number of frames for standard image',  'ct_v360' ) ?> </label></th>
            <td>
                <input type="text" class="small-text" name="my_meta_box_n_footage" id="my_meta_box_n_footage"
                       value="<?php echo esc_attr(get_option('my_meta_box_n_footage')); ?>"
                       placeholder=" 0"/>
                <span
                    class="description"> <?php echo __('Number of frames per line (when horizontal) or per column (when vertical).',  'ct_v360' ) ?> </span>
            </td>
        </tr>

        <tr>
            <th scope="row"><label for="my_meta_box_speed"> <?php echo __('Speed ',  'ct_v360' ) ?> </label></th>
            <td>
                <input type="text" class="small-text" name="my_meta_box_speed" id="my_meta_box_speed"
                       value="<?php echo esc_attr(get_option('my_meta_box_speed')); ?>" placeholder=" 0"/>
                <span class="description"> <?php echo __('Animated rotation speed in revolutions per second (Hz). Animation is disabled by default (0). ',  'ct_v360' ) ?>  </span>
            </td>
        </tr>

        <tr>
            <th scope="row"><label for="my_meta_box_time">  <?php echo __('Stop motion time ',  'ct_v360' ) ?> </label></th>
            <td>
                <input type="text" class="small-text" name="my_meta_box_time" id="my_meta_box_time"
                       value="<?php echo esc_attr(get_option('my_meta_box_time')); ?>" placeholder=" 0"/>
                <span class="description">  <?php echo __('Delay before View360 starts playing by itself (in seconds). ',  'ct_v360' ) ?> </span>
            </td>
        </tr>

        <tr>
            <th scope="row"><label for="my_meta_box_display">  <?php echo __('Duration of stop motion ',  'ct_v360' ) ?> </label></th>
            <td>
                <input type="text" class="small-text" name="my_meta_box_display" id="my_meta_box_display"
                       value="<?php echo esc_attr(get_option('my_meta_box_display')); ?>" placeholder="0"/>
                <span class="description">  <?php echo __('Duration of opening animation (in seconds). ',  'ct_v360' ) ?> </span>
            </td>
        </tr>

        <tr>
            <th scope="row"><label for="my_meta_box_select_touch"> <?php echo __('Enable gyroscope ',  'ct_v360' ) ?> </label></th>
            <td>
                <input type="checkbox" name="my_meta_box_select_touch"
                       id="my_meta_box_select_touch" <?php echo checked(esc_attr(get_option('my_meta_box_select_touch')), 'on', ''); ?> />
                <span class="description">  <?php echo __("Enables interaction via device's built-in gyroscope (if available)",  'ct_v360' ) ?> </span>
            </td>
        </tr>
        <tr>
            <th scope="row"><label for="my_meta_box_select_scroll"><?php echo __('Mouse scroll interaction ',  'ct_v360' ) ?> </label></th>
            <td>
                <input type="checkbox" name="my_meta_box_select_scroll"
                       id="my_meta_box_select_scroll" <?php echo checked(esc_attr(get_option('my_meta_box_select_scroll')), 'on', ''); ?> />
                <span class="description">  <?php echo __('Enables interaction mouse scroll ',  'ct_v360' ) ?>  </span>
            </td>
        </tr>

        <tr>
            <th scope="row"><label for="my_meta_box_select_indicator"> <?php echo __('View progress ',  'ct_v360' ) ?></label></th>
            <td>
                <input type="checkbox" name="my_meta_box_select_indicator"
                       id="my_meta_box_select_indicator" <?php echo checked(esc_attr(get_option('my_meta_box_select_indicator')), 'on', '' ); ?> />
                <span class="description"> <?php echo __('View centering tolerance (in frames) for dual-orbit object movies. ',  'ct_v360' ) ?> </span>
            </td>
        </tr>

        <tr>
            <th scope="row"><label for="my_meta_box_select_responsive"> <?php echo __('Responsiveness ',  'ct_v360' ) ?></label></th>
            <td>
                <input type="checkbox" name="my_meta_box_select_responsive" id="my_meta_box_select_responsive"
                        <?php echo checked(esc_attr(get_option('my_meta_box_select_responsive')), 'on', '' ); ?> />
                <span class="description"> <?php echo __(" If switched to responsive mode, View will obey dimensions of its parent container, will grow to fit and will adjust the interaction and UI accordingly (and also on resize).",  'ct_v360' ) ?>  </span>
            </td>
        </tr>

        <tr>
            <th scope="row"><label for="my_meta_box_select_cursor"> <?php echo __('Cursor type ',  'ct_v360' ) ?></label></th>
            <td>
                <select name="my_meta_box_select_cursor" id="my_meta_box_select_cursor">
                    <option
                        value="false" <?php selected(esc_attr(get_option('my_meta_box_select_cursor')), 'false'); ?>>
                         <?php echo __('Arrow ',  'ct_v360' ) ?>
                    </option>
                    <option
                        value="true" <?php selected(esc_attr(get_option('my_meta_box_select_cursor')), 'true'); ?>>
                        <?php echo __('Hand  ',  'ct_v360' ) ?>
                    </option>
                </select>
                <span class="description">  <?php echo __('Mouse cursor overriding the default one. You can use either hand for a grabbing ',  'ct_v360' ) ?>  </span>
            </td>
        </tr>

        <tr>
            <th scope="row"><label for="my_meta_box_select_draggable">  <?php echo __('Draggable Cursor ',  'ct_v360' ) ?></label></th>
            <td>
                <input type="checkbox" name="my_meta_box_select_draggable" id="my_meta_box_select_draggable" checked="checked"
                       value="on" <?php echo checked(esc_attr(get_option('my_meta_box_select_draggable')), 'on', ''); ?> />
                <span
                    class="description">   <?php echo __('Allows mouse or finger drag interaction when true (allowed by default). ',  'ct_v360' ) ?> </span>
            </td>
        </tr>

        <tr>
            <th scope="row"><label for="my_meta_box_select_click"><?php echo __('Play Animation on mouse over   ',  'ct_v360' ) ?> </label></th>
            <td>
                <input type="checkbox" name="my_meta_box_select_click"
                       id="my_meta_box_select_click" <?php echo checked(esc_attr(get_option('my_meta_box_select_click')), 'on', ''); ?> />
                <span class="description">  <?php echo __('Binds to mouse leave/enter events instead of down/up mouse events ',  'ct_v360' ) ?> </span>
            </td>
        </tr>

        <tr>
            <th scope="row"><label for="my_meta_box_select_cw">  <?php echo __('Follow the mouse ',  'ct_v360' ) ?> </label></th>
            <td>
                <input type="checkbox" name="my_meta_box_select_cw"
                       id="my_meta_box_select_cw" <?php echo checked(esc_attr(get_option('my_meta_box_select_cw')), 'on', ''); ?> />
                <span class="description"> <?php echo __("If your View image motion doesn't follow the mouse when dragged (moves in opposite direction), set this to true to indicate clockwise organization of frames. ",  'ct_v360' ) ?>  </span>
            </td>
        </tr>
        </tbody>
    </table>

    <div>
    <p class="window" style="font-size: large">   <?php echo __('Button Settings ',  'ct_v360' ) ?> </p>

    <table class="form-table">
    <tbody>

    <tr>
        <th scope="row"><label for="my_meta_box_select_open_guide"><?php echo __('Open view with:  ',  'ct_v360' ) ?></label></th>
        <td>
            <select name="my_meta_box_select_open_guide" id="my_meta_box_select_open_guide">
                <option
                    value="0" <?php selected(esc_attr(get_option('my_meta_box_select_open_guide')), '0'); ?>>
                     <?php echo __('Button ',  'ct_v360' ) ?>
                </option>
                <option
                    value="1" <?php selected(esc_attr(get_option('my_meta_box_select_open_guide')), '1'); ?>>
                      <?php echo __('Link ',  'ct_v360' ) ?>
                </option>
                <option
                    value="2" <?php selected(esc_attr(get_option('my_meta_box_select_open_guide')), '2'); ?>>
                     <?php echo __('Icon  ',  'ct_v360' ) ?>
                </option>
            </select>
                <span
                    class="description">   <?php echo __('Chose whether to display a simple link or a button to open the view 360 ',  'ct_v360' ) ?></span>
        </td>
    </tr>


    <tr>
        <th scope="row"><label for="my_meta_box_select_button_position">  <?php echo __('Button/link position:  ',  'ct_v360' ) ?></label></th>
        <td>
            <select name="my_meta_box_select_button_position" id="my_meta_box_select_button_position">

                <option
                    value="1" <?php selected(esc_attr(get_option('my_meta_box_select_button_position')), '1'); ?>>
                      <?php echo __('Above the product summary tabs ',  'ct_v360' ) ?>
                </option>
                <option
                    value="2" <?php selected(esc_attr(get_option('my_meta_box_select_button_position')), '2'); ?>>
                     <?php echo __(' After Add To Cart button  ',  'ct_v360' ) ?>
                </option>
                <option
                    value="3" <?php selected(esc_attr(get_option('my_meta_box_select_button_position')), '3'); ?>>
                    <?php echo __(' Make it a tab  ',  'ct_v360' ) ?>
                </option>
                <option
                    value="4" <?php selected(esc_attr(get_option('my_meta_box_select_button_position')), '4'); ?>>
                      <?php echo __('Embed manually (shortcode) ',  'ct_v360' ) ?>
                </option>
                <option
                    value="5" <?php selected(esc_attr(get_option('my_meta_box_select_button_position')), '5'); ?>>
                     <?php echo __(' Add Button to Product Gallery ',  'ct_v360' ) ?>
                </option>

            </select>
            <span class="description">   <?php echo __('For choice be placed you want.   ',  'ct_v360' ) ?></span>
        </td>
    </tr>
    <tr>
        <th scope="row"><label for="my_meta_box_button_control">  <?php echo __('Add Buttons Control  ',  'ct_v360' ) ?></label></th>
        <td>
            <input type="checkbox" name="my_meta_box_button_control"
                   id="my_meta_box_button_control" <?php echo checked(esc_attr(get_option('my_meta_box_button_control')), 'on', ''); ?> />
            <span class="description">   <?php echo __('Add Button Control Left/Right/Stop/Play  ',  'ct_v360' ) ?> </span>
        </td>
    </tr>

    <tr>
        <th scope="row"><label for="my_meta_box_button_label">   <?php echo __('Button/link label:  ',  'ct_v360' ) ?> </label></th>
        <td>
            <input type="text" size="9" name="my_meta_box_button_label" id="my_meta_box_button_label"
                   value="<?php echo esc_attr(get_option('my_meta_box_button_label')); ?>"
                   placeholder="view360"/>
            <span class="description">    <?php echo __('Add a title of button  ',  'ct_v360' ) ?></span>
        </td>
    </tr>

    <tr>
        <th scope="row"><label for="my_meta_box_select_button_align">    <?php echo __('Button/link align',  'ct_v360' ) ?> </label></th>
        <td>
            <select name="my_meta_box_select_button_align" id="my_meta_box_select_button_align">
                <option
                    value="1" <?php selected(esc_attr(get_option('my_meta_box_select_button_align')), 'true'); ?>>
                      <?php echo __(' Right ',  'ct_v360' ) ?>
                </option>
                <option
                    value="2" <?php selected(esc_attr(get_option('my_meta_box_select_button_align')), 'false'); ?>>
                     <?php echo __(' Left  ',  'ct_v360' ) ?>
                </option>
            </select>
        </td>
    </tr>
    <tr>
        <th scope="row"><label for="my_meta_box_button_class">   <?php echo __('Button class:  ',  'ct_v360' ) ?> </label></th>
        <td>
            <input type="text" size="8" name="my_meta_box_button_class" id="my_meta_box_button_class"
                   value="<?php echo esc_attr(get_option('my_meta_box_button_class')); ?>"
                   placeholder=" button"/>
            <span class="description">    <?php echo __('Add a custom class to the button (if you add Button_ct you make change color button/border/text)  ',  'ct_v360' ) ?></span>
        </td>
    </tr>

    <tr>
        <th scope="row"><label for="my_meta_box_button_v_left">    <?php echo __('Margin left  ',  'ct_v360' ) ?> </label></th>
        <td>
            <input type="number" class="small-text" name="my_meta_box_button_v_left" id="my_meta_box_button_v_left"
                   value="<?php echo esc_attr(get_option('my_meta_box_button_v_left')); ?>" placeholder="0"
                />
            <span class="description">   <?php echo __('Enter the left margin of the link/button   ',  'ct_v360' ) ?></span>
        </td>
    </tr>

    <tr>
        <th scope="row"><label for="my_meta_box_button_m_top">    <?php echo __('Margin top ',  'ct_v360' ) ?> </label></th>
        <td>
            <input type="number" class="small-text" name="my_meta_box_button_m_top" id="my_meta_box_button_m_top"
                   value="<?php echo esc_attr(get_option('my_meta_box_button_m_top')); ?>" placeholder="0"
                />
            <span class="description">   <?php echo __('Enter the top margin of the link/button  ',  'ct_v360' ) ?></span>
        </td>
    </tr>

    <tr>
        <th scope="row"><label for="my_meta_box_button_m_right">    <?php echo __('Margin right  ',  'ct_v360' ) ?> </label></th>
        <td>
            <input type="number" class="small-text" name="my_meta_box_button_m_right"
                   id="my_meta_box_button_m_right"
                   value="<?php echo esc_attr(get_option('my_meta_box_button_m_right')); ?>" placeholder="0"
                />
            <span class="description">   <?php echo __('Enter the right margin of the link/button  ',  'ct_v360' ) ?></span>
        </td>
    </tr>

    <tr>
        <th scope="row"><label for="my_meta_box_button_m_bottom">   <?php echo __('Margin bottom   ',  'ct_v360' ) ?></label></th>
        <td>
            <input type="number" class="small-text" name="my_meta_box_button_m_bottom"
                   id="my_meta_box_button_m_bottom"
                   value="<?php echo esc_attr(get_option('my_meta_box_button_m_bottom')); ?>" placeholder="0"
                />
            <span class="description">    <?php echo __('Enter the bottom margin of the link/button ',  'ct_v360' ) ?></span>
        </td>
    </tr>

    <tr>
        <th scope="row"><label for="my_meta_box_button_p_color">   <?php echo __('Button color  ',  'ct_v360' ) ?> </label></th>
        <td>
            <input type="color" class="color-picker" name="my_meta_box_button_p_color" id="my_meta_box_button_p_color"
                   value="<?php echo esc_attr(get_option('my_meta_box_button_p_color'), '#ffffff'); ?>"
                />
            <span class="description">  <?php echo __('Click to pick the color of the popup background overlay   ',  'ct_v360' ) ?></span>
        </td>
    </tr>

    <tr>
        <th scope="row"><label for="my_meta_box_button_bb_color"> <?php echo __('Border/Text button color  ',  'ct_v360' ) ?>  </label></th>
        <td>
            <input type="color" name="my_meta_box_button_bb_color" id="my_meta_box_button_bb_color"
                   value="<?php echo esc_attr(get_option('my_meta_box_button_bb_color')); ?>"
                />
            <span class="description"> <?php echo __('Click to pick the color of the popup background overlay ',  'ct_v360' ) ?> </span>
        </td>
    </tr>

    <tr>
        <th scope="row"><label for="my_meta_box_button_p_left"> <?php echo __('Padding left   ',  'ct_v360' ) ?> </label></th>
        <td>
            <input type="number" class="small-text" name="my_meta_box_button_p_left" id="my_meta_box_button_p_left"
                   value="<?php echo esc_attr(get_option('my_meta_box_button_p_left')); ?>" placeholder="0"
                />
            <span class="description">  <?php echo __('Enter the left padding of the content in the popup window  ',  'ct_v360' ) ?></span>
        </td>
    </tr>

    <tr>
        <th scope="row"><label for="my_meta_box_button_p_top"> <?php echo __('Padding top   ',  'ct_v360' ) ?> </label></th>
        <td>
            <input type="number" class="small-text" name="my_meta_box_button_p_top" id="my_meta_box_button_p_top"
                   value="<?php echo esc_attr(get_option('my_meta_box_button_p_top')); ?>" placeholder="0"
                />
            <span class="description"> <?php echo __('Enter the top padding of the content in the popup window   ',  'ct_v360' ) ?></span>
        </td>
    </tr>

    <tr>
        <th scope="row"><label for="my_meta_box_button_p_right">  <?php echo __('Padding right  ',  'ct_v360' ) ?> </label></th>
        <td>
            <input type="number" class="small-text" name="my_meta_box_button_p_right"
                   id="my_meta_box_button_p_right"
                   value="<?php echo esc_attr(get_option('my_meta_box_button_p_right')); ?>" placeholder="0"
                />
            <span class="description">  <?php echo __('Enter the right padding of the content in the popup window ',  'ct_v360' ) ?></span>
        </td>
    </tr>

    <tr>
        <th scope="row"><label for="my_meta_box_button_p_bottom">  <?php echo __('Padding bottom ',  'ct_v360' ) ?> </label></th>
        <td>
            <input type="number" class="small-text" name="my_meta_box_button_p_bottom"
                   id="my_meta_box_button_p_bottom"
                   value="<?php echo esc_attr(get_option('my_meta_box_button_p_bottom')); ?>" placeholder="0"
                />
            <span class="description">  <?php echo __('Enter the bottom padding of the content in the popup window  ',  'ct_v360' ) ?></span>
        </td>
    </tr>
    </tbody>
    </table>

    </div>
    <div>
        <p class="window" style="font-size: large">  <?php echo __('Woocommerce Integration ',  'ct_v360' ) ?> </p>
        <script>
            (function ($) {

                $(document).ready(function () {

                    $("#my_meta_box_select_code").each(function () {
                        if ($(this).is(":checked")) {
                            //Do stuff
                            $(this).closest("table").find(".copy-code").show();
                        }

                    });

                    $("#my_meta_box_select_code").change(function () {
                        $(this).closest("table").find(".copy-code").toggle();
                        if ($(this).is(":checked")) {
                            //Do stuff

                        }
                    });

                });

            })(jQuery);


        </script>
        <table class="form-table">
            <tbody>

            <tr>
                <th scope="row"><label for="my_meta_box_select_code"> <?php echo __('Replace default gallery image ',  'ct_v360' ) ?></label></th>
                <td>
                    <input type="checkbox" name="my_meta_box_select_code" id="my_meta_box_select_code"/>
                    <span class="description">  <?php echo __('If your want View image adding to first in gallery ',  'ct_v360' ) ?></span>

                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="2">
<textarea style="display: none; height: 270px; width: 870px; min-width: 870px; min-height: 270px "
class="copy-code large-text code">

<?php echo __("If you want to add the first img View360 view your product post paste this code into your shares

 do_action('change_image_gallery',post->ID); add this in product-thumbnails.php

do_action('ct_view360',post->ID); add this in product-image.php

post is a variable

If you have default gallery a photo for the woocommerce paste this code into your shares

do_action('change_image_default_gallery'); ",  'ct_v360' ) ?>


</textarea>
                </td>
            </tr>


            </tbody>
        </table>
    </div>
<div>



    <table class="form-table">
        <tbody>
    <tr>
        <th scope="row"><label for="my_meta_box_select_loder">  <?php echo __('Active Loder ',  'ct_v360' ) ?> </label></th>
        <td>
            <input type="checkbox" name="my_meta_box_select_loder"
                   id="my_meta_box_select_loder" <?php echo checked(esc_attr(get_option('my_meta_box_select_loder')), 'on', ''); ?> />
            <span class="description"> <?php echo __("If your want View loder or change loder. ",  'ct_v360' ) ?>  </span>
        </td>
    </tr>

    <script>
        (function ($) {

            $(document).ready(function () {

                $("#my_meta_box_select_loder").each(function () {
                    if ($(this).is(":checked")) {
                        //Do stuff
                        $(this).closest("table").find(".loder").show();
                    }

                });

                $("#my_meta_box_select_loder").change(function () {
                    $(this).closest("table").find(".loder").toggle();
                    if ($(this).is(":checked")) {
                        //Do stuff

                    }
                });

            });

        })(jQuery);


    </script>
    <tr class="loder" style="display: none;  ">
        <th scope="row"><label for="my_meta_box_loder_url"> <?php echo __('Loder Url',  'ct_v360' ) ?> </label></th>
        <td>
            <input type="text" class="text" name="my_meta_box_loder_url" id="my_meta_box_loder_url"
                   value="<?php echo esc_attr(get_option('my_meta_box_loder_url')); ?>"
                   placeholder=" defaul url"/>
                <span
                    class="description"> <?php echo __('Add Url for loder.',  'ct_v360' ) ?> </span>
        </td>
    </tr>


        </tbody>
    </table>

</div>
    <div class="button-save">
        <?php submit_button(); ?>
    </div>


    </div>
    <?php

    }

}

new ctView360info();


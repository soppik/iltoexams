<?php

/**
 * Plugin Name: createIT View 360 (shared on wplocker.com)
 * Plugin URI: http://createit.pl
 * Description: Rotate images 360 degrees
 * Version: 1.0
 * Author: createIT
 * Author URI: http://createit.pl
 */
class ctView360Plugin {

	public function __construct() {
		$this->setupConst();
		$this->loadFiles();


		add_action( 'wp_enqueue_scripts', array( $this, 'enqueueScripts' ) );
	}


	/**
	 * loadFiles
	 */
	public function loadFiles() {
		require_once dirname(__FILE__) . '/ctView360Custom.php';
		require_once dirname(__FILE__) . '/ctView360SC.php';
		require_once dirname(__FILE__) . '/ctView360Settings.php';
		require_once dirname(__FILE__) . '/ctView360ColumnsPost.php';
		require_once dirname(__FILE__) . '/ctView360PGS.php';
		require_once dirname(__FILE__) . '/ctView360Display.php';
		require_once dirname(__FILE__) . '/ctGalleryPost.php';

	}

	/**
	 * Static register
	 */

	public function setupConst() {

		if ( ! defined( 'CT_VIEW360_DIR' ) ) {
			define( 'CT_VIEW360_DIR', untrailingslashit( plugin_dir_path( __FILE__ ) ) );

		}
		if ( ! defined( 'CT_VIEW360_URI' ) ) {

			define( 'CT_VIEW360_URI', WP_PLUGIN_URL . '/' . basename( dirname( __FILE__ ) ) );

		}
		if ( ! defined( 'CT_VIEW360_ASSETS' ) ) {
			define( 'CT_VIEW360_ASSETS', CT_VIEW360_URI . '/assets' );
		}
	}

	/**
	 * ADD Scripts
	 */

	public function enqueueScripts() {
		wp_enqueue_script( 'ct.sg.admin.js', CT_VIEW360_ASSETS . '/js/jquery.reel.js', array(
			'jquery',
		) );
		wp_enqueue_script( 'ct.sg.admin.js', CT_VIEW360_ASSETS . '/js/jquery11.js', array(
			'jquery',
		) );

	}

}

new ctView360Plugin();


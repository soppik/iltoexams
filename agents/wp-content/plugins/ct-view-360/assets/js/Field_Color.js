jQuery(document).ready(function(){

    var $color_inputs = jQuery('input.popup-colorpicker');

    $color_inputs.each(function(){
        var $input = jQuery(this);
        var $pickerId = "#" + jQuery(this).attr('id') + "picker";

        jQuery($pickerId).hide();
        jQuery($pickerId).farbtastic($input);
        jQuery($input).click(function(){jQuery($pickerId).slideToggle()});

    });
});/**
 * Created by CreateIT on 2014-11-07.
 */

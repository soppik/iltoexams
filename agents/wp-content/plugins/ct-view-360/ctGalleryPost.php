<?php

/**
 *
 * Create Gallery Metabox
 */
class ctGalleryPost {

	public function __construct() {

		add_action( 'add_meta_boxes', array( $this, 'addMetaBoxes' ) );


		add_action( 'save_post', array( $this, 'saveImageGallery' ) );
		add_action( 'edit_post', array( $this, 'saveImageGallery' ) );

	}


	public function addMetaBoxes () {
		add_meta_box( 'galleryImage', 'Gallery View 360', array(
			$this,
			'galleryBox' //renderGalleryBox
		), 'view360', 'normal' );
	}

	/**
	 * Add metabox Gallery View 360
	 */
	public function galleryBox() {
		global $post;

        /**
         * Add Style
         */

		?>
		<style type="text/css">

			.gallery_images:after,
			#gallery_images_container:after {
				content: ".";
				display: block;
				height: 0;
				clear: both;
				visibility: hidden;
			}

			.gallery_images > li {
				float: left;
				cursor: move;
				margin: 0 20px 20px 0;
				position: relative;
			}

			.gallery_images li.image img {
				width: 160px;
				height: 160px;
			}

			.ct_gallery_view .delete {
				width: 16px;
				height: 16px;
				display: block;
				background: url("<?php echo CT_VIEW360_ASSETS ?>/img/cross-circle.png") no-repeat center center;
				position: absolute;
				top: 0;
				right: 0;

			}

			.gallery_images.ui-sortable > li {
				cursor: move !important;
				border: 2px solid transparent;
			}

			.gallery_images.ui-sortable > li.wc-metabox-sortable-placeholder {
				border: 2px dashed #ccc;
			}

		</style>


		<p class="add_gallery_images hide-if-no-js ">
			<a class="button button-secondary" href="#"><?php _e( 'Add gallery images', 'ct_v360' ); ?></a>
		</p>

		<div id="gallery_images_container" class="ct_gallery_view">

			<ul class="gallery_images">
				<?php

				$imageGallery = get_post_meta( $post->ID, 'image_gallery', true );

			$attachments   = array_filter( explode( ',', $imageGallery ) );

				if ( $attachments ) {
					foreach ( $attachments as $attachmentId ) {
                        $img = wp_get_attachment_image( $attachmentId, 'medium' );
                        if (!empty($img)) {
                            echo '<li class="image" data-attachment_id="' . $attachmentId . '">
                            ' . $img . '
                            <ul class="actions">
                                <li><a href="#" class="delete" title="' . __('Remove image', 'ct_v360') . '"></a></li>
                            </ul>
                       </li>';
                        }
					}
				}
				?>
			</ul>


			<input type="hidden" id="image_gallery" name="image_gallery"
			       value="<?php echo esc_attr( $imageGallery ); ?>"/>
			<?php wp_nonce_field( 'imageGallery', 'imageGallery' ); ?>

		</div>
		<?php

		/**
		 * Props to WooCommerce for the following JS code
		 */
		?>
		<script type="text/javascript">
			jQuery(document).ready(function ($) {

				// Uploading files
				var image_gallery_frame;
				var $image_gallery_ids = $('#image_gallery');
				var $gallery_images = $('#gallery_images_container ul.gallery_images');

				jQuery('.add_gallery_images').on('click', 'a', function (event) {

					var $el = $(this);
					var attachment_ids = $image_gallery_ids.val();

					event.preventDefault();

					// If the media frame already exists, reopen it.
					if (image_gallery_frame) {
						image_gallery_frame.open();
						return;
					}

					// Create the media frame.
					image_gallery_frame = wp.media.frames.downloadable_file = wp.media({
						// Set the title of the modal.
						title: '<?php _e( 'Add Images to Gallery', 'ct_v360' ); ?>',
						button: {
							text: '<?php _e( 'Add to gallery', 'ct_v360' ); ?>'

						},
						multiple: true
					});

					// When an image is selected, run a callback.
					image_gallery_frame.on('select', function () {

						var selection = image_gallery_frame.state().get('selection');

						selection.map(function (attachment) {

							attachment = attachment.toJSON();

							if (attachment.id) {
								attachment_ids = attachment_ids ? attachment_ids + "," + attachment.id : attachment.id;

								$gallery_images.append('\
                                <li class="image" data-attachment_id="' + attachment.id + '">\
                                    <img src="' + attachment.url + '" />\
                                    <ul class="actions">\
                                        <li><a href="#" class="delete" title="<?php _e( 'Remove image', 'ct_v360' ); ?>"></a></li>\
                                    </ul>\
                                </li>');
							}

						});

						$image_gallery_ids.val(attachment_ids);
					});

					// Finally, open the modal.
					image_gallery_frame.open();
				});

				// Image ordering
				$gallery_images.sortable({
					items: 'li.image',
					cursor: 'move',
					scrollSensitivity: 40,
					forcePlaceholderSize: true,
					forceHelperSize: false,
					helper: 'clone',
					opacity: 0.65,
					placeholder: 'wc-metabox-sortable-placeholder',
					start: function (event, ui) {
						ui.item.css('background-color', '#f6f6f6');
					},
					stop: function (event, ui) {
						ui.item.removeAttr('style');
					},
					update: function (event, ui) {
						var attachment_ids = '';

						$('#gallery_images_container ul li.image').css('cursor', 'default').each(function () {
							var attachment_id = jQuery(this).attr('data-attachment_id');
							attachment_ids = attachment_ids + attachment_id + ',';
						});

						$image_gallery_ids.val(attachment_ids);
					}
				});

				// Remove images
				$('#gallery_images_container').on('click', 'a.delete', function () {

					$(this).closest('li.image').remove();

					var attachment_ids = '';

					$('#gallery_images_container ul li.image').css('cursor', 'default').each(function () {
						var attachment_id = jQuery(this).attr('data-attachment_id');
						attachment_ids = attachment_ids + attachment_id + ',';
					});

					$image_gallery_ids.val(attachment_ids);

					return false;
				});

			});
		</script>
	<?php
	}


	/**
	 * Save function
	 *
	 * @since 1.0
	 */
	function saveImageGallery( $postId ) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		// check user permissions


		if ( ! isset( $_POST['imageGallery'] ) || ! wp_verify_nonce( $_POST['imageGallery'], 'imageGallery' ) ) {
			return;
		}

		if ( isset( $_POST['image_gallery'] ) && ! empty( $_POST['image_gallery'] ) ) {

			$attachmentIds = sanitize_text_field( $_POST['image_gallery'] );

			// turn comma separated values into array
			$attachmentIds = explode( ',', $attachmentIds );

			// clean the array
			$attachmentIds = array_filter( $attachmentIds );

			// return back to comma separated list with no trailing comma. This is common when deleting the images
			$attachmentIds = implode( ',', $attachmentIds );

			update_post_meta( $postId, 'image_gallery', $attachmentIds );
		} else {
			delete_post_meta( $postId, 'image_gallery' );
		}

		// link to larger images
		if ( isset( $_POST['imageGallery_link_images'] ) ) {
			update_post_meta( $postId, 'imageGallery_link_images', $_POST['imageGallery_link_images'] );
		} else {
			update_post_meta( $postId, 'imageGallery_link_images', 'off' );
		}

		do_action( 'saveImageGallery', $postId );
	}

}

new ctGalleryPost();

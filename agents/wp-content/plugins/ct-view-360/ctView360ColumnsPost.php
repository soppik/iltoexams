<?php

/**
 * Create Column Shortcode Name
 */
class ctView360ColumnsPost {


	function __construct() {

		add_filter( 'manage_edit-view360_columns', array( $this, 'changeColumns' ) );
		add_action( 'manage_view360_posts_custom_column', array( $this, 'customColumns' ), 10, 2 );
	}

	/**
	 *  Add new columns
	 * @return array
	 */

	public function changeColumns() {

		$cols = array(
			'title'    => __( 'Title','ct_v360' ),
			'duration' => __( 'Shortcode Name','ct_v360' ),
			'date'     => __( 'Date','ct_v360' ),
		);

		return $cols;
	}

	/**
	 *  Add shortcode to columns
	 *
	 * @param string $column
	 * @param int $post_id
	 */

	public function customColumns( $column, $post_id ) {
		switch ( $column ) {

			case 'duration':
				$durcolumn = "[view360 id=\"" . $post_id . "\"]";
				echo $durcolumn;
				break;
		}

	}


}

new ctView360ColumnsPost();

<?php

/**
 * Create Metabox Settings
 */
class ctView360Settings {

	public function __construct() {

		add_action( 'add_meta_boxes', array( $this, 'addMetaPage' ) );
		add_action( 'save_post', array( $this, 'MetaboxSave' ) );
		add_action( 'edit_post', array( $this, 'MetaboxSave' ) );

	}

	/**
	 * Add Meta_box
	 */

	public function addMetaPage() {
		add_meta_box( 'boj-meta', 'Settings View360', array( $this, 'SettingsPost' ), 'view360', 'normal', 'high' );
	}


	// Define the custom attachment for pages

	/**
	 * Create Meta_box
	 */

	public function SettingsPost() {
		global $post;

		// get value for custom post type
		$post_id = $post->ID;
		$custom  = CTView360SC::getCustomValue( $post_id );

		// values for CtView360SC

		$cw         = isset( $custom["my_meta_box_select_cw"][0] ) ? $custom["my_meta_box_select_cw"][0] : esc_attr( get_option( 'my_meta_box_select_cw' ) );
		$speed      = isset( $custom["my_meta_box_speed"][0] ) ? $custom["my_meta_box_speed"][0] : esc_attr( get_option( 'my_meta_box_speed' ) );
		$touch      = isset( $custom["my_meta_box_select_touch"][0] ) ? $custom["my_meta_box_select_touch"][0] : esc_attr( get_option( 'my_meta_box_select_touch' ) );
		$delay      = isset( $custom["my_meta_box_time"][0] ) ? $custom["my_meta_box_time"][0] : esc_attr( get_option( 'my_meta_box_time' ) );
		$display    = isset( $custom["my_meta_box_display"][0] ) ? $custom["my_meta_box_display"][0] : esc_attr( get_option( 'my_meta_box_display' ) );
		$indicator  = isset( $custom["my_meta_box_select_indicator"][0] ) ? $custom["my_meta_box_select_indicator"][0] : esc_attr( get_option( 'my_meta_box_select_indicator' ) );
		$responsive = isset( $custom["my_meta_box_select_responsive"][0] ) ? $custom["my_meta_box_select_responsive"][0] : esc_attr( get_option( 'my_meta_box_select_responsive' ) );
		$cursor     = isset( $custom["my_meta_box_select_cursor"][0] ) ? $custom["my_meta_box_select_cursor"][0] : esc_attr( get_option( 'my_meta_box_select_cursor' ) );
		$draggable  = isset( $custom["my_meta_box_select_draggable"][0] ) ? $custom["my_meta_box_select_draggable"][0] : esc_attr( get_option( 'my_meta_box_select_draggable' ) );
		$click    = isset( $custom["my_meta_box_select_click"][0] ) ? $custom["my_meta_box_select_click"][0] : esc_attr( get_option( 'my_meta_box_select_click' ) );
		$n_frames   = isset( $custom["my_meta_box_n_frames"][0] ) ? $custom["my_meta_box_n_frames"][0] : esc_attr( get_option( 'my_meta_box_n_frames' ) );
		$n_footage  = isset( $custom["my_meta_box_n_footage"][0] ) ? $custom["my_meta_box_n_footage"][0] : esc_attr( get_option( 'my_meta_box_n_footage' ) );
        $n_width   = isset( $custom["my_meta_box_stitched_width"][0] ) ? $custom["my_meta_box_stitched_width"][0] : esc_attr( get_option( 'my_meta_box_stitched_width' ) );
		$n_height   = isset( $custom["my_meta_box_stitched_height"][0] ) ? $custom["my_meta_box_stitched_height"][0] : esc_attr( get_option( 'my_meta_box_stitched_height' ) );
		$scroll     = isset( $custom["my_meta_box_select_scroll"][0] ) ? $custom["my_meta_box_select_scroll"][0] : esc_attr( get_option( 'my_meta_box_select_scroll' ) );


		// We'll use this nonce field later on when saving.


		wp_nonce_field( 'view_meta_box_nonce', 'view_box_nonce' );

		?>

		<div class="classname">

			<div class="frame">


				<table class="form-table">
					<tbody>
					<tr>
						<th scope="row"><label for="my_meta_box_stitched_width"> <?php echo __('Width',  'ct_v360' ) ?> </label></th>
						<td>
							<input type="text" size="8" name="my_meta_box_stitched_width"
							       id="my_meta_box_stitched_width"
							       value="<?php echo esc_attr( $n_width ) ?>" placeholder="Default 400"/>
							<span class="description"><?php echo __('Width of Window Frames (You can also use percentages)',  'ct_v360' ) ?> </span>
						</td>


					</tr>
					<tr>
						<th scope="row"><label for="my_meta_box_stitched_height"><?php echo __('Height',  'ct_v360' ) ?> </label></th>
						<td>
							<input type="text" size="8" name="my_meta_box_stitched_height"
							       id="my_meta_box_stitched_height"
							       value="<?php echo esc_attr( $n_height ) ?>" placeholder="Default 400"/>
							<span class="description"> <?php echo __('Height of Window Frames (You can also use percentages)',  'ct_v360' ) ?></span>
						</td>
					</tr>

					<tr>
						<th scope="row"><label for="my_meta_box_n_frames"> <?php echo __('Number of frames',  'ct_v360' ) ?> </label></th>
						<td>
							<input type="text" class="small-text" name="my_meta_box_n_frames" id="my_meta_box_n_frames"
							       value="<?php echo esc_attr( $n_frames ) ?>" placeholder=" 0"/>
							<span
								class="description"> <?php echo __('(This Option Is for Panorama image) Total number of frames.',  'ct_v360' ) ?> </span>
						</td>
					</tr>
					<tr>
						<th scope="row"><label for="my_meta_box_n_footage"><?php echo __('Number of frames for standard image',  'ct_v360' ) ?> </label>
						</th>
						<td>
							<input type="text" class="small-text" name="my_meta_box_n_footage"
							       id="my_meta_box_n_footage"
							       value="<?php echo esc_attr( $n_footage ) ?>" placeholder=" 0"/>
							<span class="description">	<?php echo __('Number of frames per line (when horizontal) or per column (when vertical).',  'ct_v360' ) ?> </span>
						</td>
					</tr>

					<tr>
						<th scope="row"><label for="my_meta_box_speed"><?php echo __('Speed ',  'ct_v360' ) ?>  </label></th>
						<td>
							<input type="text" class="small-text" name="my_meta_box_speed" id="my_meta_box_speed"
							       value="<?php echo esc_attr( $speed ) ?>" placeholder=" 0"/>
							<span class="description">	<?php echo __('Animated rotation speed in revolutions per second (Hz). Animation is disabled by default (0). ',  'ct_v360' ) ?> </span>
						</td>
					</tr>

					<tr>
						<th scope="row"><label for="my_meta_box_time"><?php echo __('Stop motion time ',  'ct_v360' ) ?> </label></th>
						<td>
							<input type="text" class="small-text" name="my_meta_box_time" id="my_meta_box_time"
							       value="<?php echo esc_attr( $delay ) ?>" placeholder=" 0"/>
							<span class="description"> <?php echo __('Delay before View360 starts playing by itself (in seconds). ',  'ct_v360' ) ?> </span>
						</td>
					</tr>

					<tr>
						<th scope="row"><label for="my_meta_box_display"><?php echo __('Duration of stop motion ',  'ct_v360' ) ?> </label></th>
						<td>
							<input type="text" class="small-text" name="my_meta_box_display" id="my_meta_box_display"
							       value="<?php echo esc_attr( $display ) ?>" placeholder="0"/>
							<span class="description"><?php echo __('Duration of opening animation (in seconds). ',  'ct_v360' ) ?> </span>
						</td>
					</tr>


					<tr>
						<th scope="row"><label for="my_meta_box_select_touch"><?php echo __('Enable gyroscope ',  'ct_v360' ) ?></label></th>
						<td>
							<input type="checkbox" name="my_meta_box_select_touch"
							       id="my_meta_box_select_touch" <?php checked( esc_attr( $touch ), 'on', '1', '' ); ?> />
							<span class="description"> <?php echo __("Enables interaction via device's built-in gyroscope (if available)",  'ct_v360' ) ?> </span>
						</td>
					</tr>

					<tr>
						<th scope="row"><label for="my_meta_box_select_scroll"><?php echo __('Mouse scroll interaction ',  'ct_v360' ) ?></label></th>
						<td>
							<input type="checkbox" name="my_meta_box_select_scroll"
							       id="my_meta_box_select_scroll" <?php checked( esc_attr( $scroll ), 'on', '1', '' ); ?> />
							<span class="description">   <?php echo __('Enables interaction mouse scroll ',  'ct_v360' ) ?> </span>
						</td>
					</tr>

					<tr>
						<th scope="row"><label for="my_meta_box_select_indicator"><?php echo __('View progress ',  'ct_v360' ) ?></label></th>
						<td>
							<input type="checkbox" name="my_meta_box_select_indicator"
							       id="my_meta_box_select_indicator" <?php checked( esc_attr( $indicator ), 'on', '1', '' ); ?> />
							<span
								class="description">	<?php echo __('View centering tolerance (in frames) for dual-orbit object movies. ',  'ct_v360' ) ?> </span>
						</td>
					</tr>

					<tr>
						<th scope="row"><label for="my_meta_box_select_responsive"><?php echo __('Responsiveness ',  'ct_v360' ) ?></label></th>
						<td>
							<input type="checkbox" name="my_meta_box_select_responsive"
							       id="my_meta_box_select_responsive" <?php checked( esc_attr( $responsive ), 'on', '1', '' ); ?> />
							<span class="description"><?php echo __(" If switched to responsive mode, View will obey dimensions of its parent container, will grow to fit and will adjust the interaction and UI accordingly (and also on resize).",  'ct_v360' ) ?>  </span>
						</td>
					</tr>

					<tr>
						<th scope="row"><label for="my_meta_box_select_cursor"><?php echo __('Cursor type ',  'ct_v360' ) ?></label></th>
						<td>
							<select name="my_meta_box_select_cursor" id="my_meta_box_select_cursor"
							        class="chosen_select">
								<option value="false" <?php selected( esc_attr( $cursor ), 'false' ); ?>>  <?php echo __('Arrow ',  'ct_v360' ) ?></option>
								<option value="true" <?php selected( esc_attr( $cursor ), 'true' ); ?>>  <?php echo __('Hand  ',  'ct_v360' ) ?></option>
							</select>
							<span class="description"> <?php echo __('Mouse cursor overriding the default one. You can use either hand for a grabbing ',  'ct_v360' ) ?> </span>
						</td>
					</tr>

					<tr>
						<th scope="row"><label for="my_meta_box_select_draggable"><?php echo __('Draggable Cursor ',  'ct_v360' ) ?></label></th>
						<td>
							<input type="checkbox" name="my_meta_box_select_draggable"
							       id="my_meta_box_select_draggable" <?php checked( esc_attr( $draggable ), 'on', '1', '' ); ?> />
							<span class="description"> <?php echo __('Allows mouse or finger drag interaction when true (allowed by default). ',  'ct_v360' ) ?> </span>
						</td>
					</tr>

					<tr>
						<th scope="row"><label for="my_meta_box_select_click"><?php echo __('Play Animation on mouse over   ',  'ct_v360' ) ?></label>
						</th>
						<td>
							<input type="checkbox" name="my_meta_box_select_click"
							       id="my_meta_box_select_click" <?php checked( esc_attr( $click ), 'on', '1', '' ); ?> />
							<span
								class="description"><?php echo __('Binds to mouse leave/enter events instead of down/up mouse events ',  'ct_v360' ) ?> </span>
						</td>
					</tr>

					<tr>
						<th scope="row"><label for="my_meta_box_select_cw"><?php echo __('Follow the mouse ',  'ct_v360' ) ?></label></th>
						<td>
							<input type="checkbox" name="my_meta_box_select_cw"
							       id="my_meta_box_select_cw" <?php checked( esc_attr( $cw ), 'on', '1', '' ); ?> />
							<span class="description"> <?php echo __("If your View image motion doesn't follow the mouse when dragged (moves in opposite direction), set this to true to indicate clockwise organization of frames. ",  'ct_v360' ) ?> </span>
						</td>
					</tr>

					</tbody>
				</table>

			</div>
		</div>


	<?php
	}

	/**
	 * Save and Update Meta_box
	 */

	public function MetaboxSave( $post_id ) {
		// Bail if we're doing an auto save
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		// if our nonce isn't there, or we can't verify it, bail
		if ( ! isset( $_POST['view_box_nonce'] ) || ! wp_verify_nonce( $_POST['view_box_nonce'], 'view_meta_box_nonce' ) ) {
			return;
		}

		// if our current user can't edit this post, bail
		if ( ! current_user_can( 'edit_post' ) ) {
			return;
		}

		// now we can actually save the data

		$allowed = array(
			'a' => array( // on allow a tags
				'href' => array() // and those anchors can only have href attribute
			)
		);

		// Make sure your data is set before trying to save it my_meta_box_select_row_s


		// This is purely my personal preference for saving text

		foreach (
			array(
				'my_meta_box_stitched_width',
				'my_meta_box_stitched_height',
				'my_meta_box_n_frames',
		        'my_meta_box_n_footage',
		        'my_meta_box_stitched',
		        'my_meta_box_speed',
		        'my_meta_box_time',
	        	'my_meta_box_display'
			) as $param
		) {
			if ( isset( $_POST[ $param ] ) ) {
				update_post_meta( $post_id, $param, wp_kses( $_POST[ $param ],$allowed ) );
			}
		}

        foreach (
            array(
               'my_meta_box_select_cursor'
               ) as $param
        ) {
            if ( isset( $_POST[ $param ] ) ) {
                update_post_meta( $post_id, $param, esc_attr( $_POST[ $param ] ) );
            }
        }


		// This is purely my personal preference for saving checkbox
        foreach (
            array(
                'my_meta_box_select_cw',
                'my_meta_box_select_indicator',
                'my_meta_box_select_touch',
                'my_meta_box_select_responsive',
                'my_meta_box_select_draggable',
                'my_meta_box_select_click',
                'my_meta_box_select_scroll'
            ) as $param
        ) {
             isset( $_POST[ $param ] )? '1' : 'on' ? 'on' : 'on' ? '' : 'off'; {
                update_post_meta( $post_id, $param, esc_attr( $_POST[ $param ] ) );
            }
        }
	}

}

new ctView360Settings();
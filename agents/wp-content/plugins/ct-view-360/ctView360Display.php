<?php
/**
 * create WooCommerce Display
 *
 */

class ctView360Display
{

    /**
     * create Shortcode for global settings
     * @return string
     */
    public static function RenderSCinGallery()
    {

        global $post;

        $post_id = $post->ID;
        $vc_post_id = get_post_meta($post_id, '_ct_view_select');
        if(isset($vc_post_id[0])) {
            $vc_post_id = $vc_post_id[0];
        }
        else {
            $vc_post_id = '';
        }
        $cw = esc_attr(get_option('my_meta_box_select_cw'));
        $speed = esc_attr(get_option('my_meta_box_speed'));
        $touch = esc_attr(get_option('my_meta_box_select_touch'));
        $delay = esc_attr(get_option('my_meta_box_time'));
        $display = esc_attr(get_option('my_meta_box_display'));
        $indicator = esc_attr(get_option('my_meta_box_select_indicator'));
        $responsive = esc_attr(get_option('my_meta_box_select_responsive'));
        $cursor = esc_attr(get_option('my_meta_box_select_cursor'));
        $draggable = esc_attr(get_option('my_meta_box_select_draggable'));
        $click = esc_attr(get_option('my_meta_box_select_click'));
        $n_frames = esc_attr(get_option('my_meta_box_n_frames'));
        $n_footage = esc_attr(get_option('my_meta_box_n_footage'));
        $n_width = esc_attr(get_option('my_meta_box_stitched_width'));
        $n_height = esc_attr(get_option('my_meta_box_stitched_height'));
        $scroll = esc_attr(get_option('my_meta_box_select_scroll'));



        return do_shortcode('[view360 id="' . $vc_post_id . '" scroll="' . $scroll . '" width="' . $n_width . '" height="' . $n_height . '" frames="' . $n_frames . '" footage="' . $n_footage . '" responsive="' . $responsive . '" speed="' . $speed . '" touch="' . $touch . '" display="' . $display . '" delay="' . $delay . '" progress="' . $indicator . '" cursor="' . $cursor . '" draggable="' . $draggable . '" click="' . $click . '" cw="' . $cw . '"]');

    }

    /**
     * Get last post id
     * @return mixed
     */
    public static function get_last_id()
    {

        $args = array(
            'post_type' => 'product',
            'post_status' => 'publish',
        );

        $my_posts = get_posts($args);
        wp_reset_postdata();

        $lastpost = $my_posts[0];

        return $lastpost->ID;
    }


    /**
     *  Get Custom Values
     */

    public static function View360_custom_value($post_id)
    {
        $custom = get_post_custom($post_id);
        return $custom;
    }

    public function __construct()
    {
        add_action('woocommerce_before_single_product', array($this, 'choiceAddView360'), 70);
        add_action('wp_enqueue_scripts', array($this, 'myPluginstyleCss'));
        add_action('add_meta_boxes', array($this, 'choiceAddButton'));
        add_action('add_meta_boxes', array($this, 'galleryView360'));
        add_action('add_meta_boxes', array($this, 'choiceView360'));
        add_action('save_post', array($this, 'viewImagegallerySavepost'));
        add_action('save_post', array($this, 'ctView360metaBoxsave'));
        add_action('edit_post', array($this, 'viewImagegallerySavepost'));
        add_action('edit_post', array($this, 'ctView360metaBoxsave'));
        add_action('ct_view360', array($this, 'singleProductView'));
        add_action('change_image_gallery', array($this, 'change_image'));
        add_action('change_image_default_gallery', array($this, 'defaultProductView'));
        add_action('change_decina_image_gallery', array($this, 'decinaImages'));
    }

    /**
     * Register script and style
     */
    public function myPluginstyleCss()
    {
        wp_register_style('sort-style', CT_VIEW360_ASSETS . '/css/sort.css');

    }
    public function choiceAddButton()
    {
        add_meta_box('ct_add_display_view360', 'Choose a location add View360', array(
            $this,
            'addRenderView360'
        ), 'product', 'side');

    }

    /**
     * Metabox to choice location View360
     */
    public function addRenderView360()
    {

        global $post;
        $custom = get_post_meta($post->ID, 'my_meta_box_select_add_button');
        $chose_location = '0';

        if(isset($custom[0])) {
            $custom = $custom[0];
            $chose_location = isset( $custom["my_meta_box_select_add_button"][0] ) ? $custom[0] : '0';
        }
        ?>

        <select name="my_meta_box_select_add_button" id="my_meta_box_select_add_button">
            <option value="0" <?php selected(esc_attr($chose_location), '0'); ?>> Global Settings </option>
            <option value="1" <?php selected(esc_attr($chose_location), '1'); ?>> Above the product summary tabs </option>
            <option value="2" <?php selected(esc_attr($chose_location), '2'); ?>>After Add To Cart button</option>
            <option value="3" <?php selected(esc_attr($chose_location), '3'); ?>>Make it a tab</option>
            <option value="5" <?php selected(esc_attr($chose_location), '5'); ?>>Add Button to Product Gallery</option>
            <option value="6" <?php selected(esc_attr($chose_location), '6'); ?>>Do not add View360</option>
        </select>

        <?php



    }

    /**
     * choice where add shortcode
     */
    public function choiceAddView360()
    {
        global $post;

        $chose_location = get_post_meta($post->ID, 'my_meta_box_select_add_button');

        If (isset ($chose_location[0])){

            $chose_location= $chose_location[0];

        }else {
            $chose_location= '0';
        }



        $button_p = esc_attr(get_option('my_meta_box_select_button_position'));

        if ( $chose_location == '0'){

            $chose_location = $button_p;
        }

        if ( $chose_location == '1') {
            add_action('woocommerce_single_product_summary', array($this, 'displayButton'));
            add_action('woocommerce_single_product_summary', array($this, 'viewShortcode360popup'));
        }
        if ( $chose_location == '2') {

            add_action('woocommerce_after_add_to_cart_button', array($this, 'displayButton'),90);
            add_action('woocommerce_single_product_summary', array($this, 'viewShortcode360popup'));

        }

        if ( $chose_location == '3') {

            add_filter('woocommerce_product_tabs', array($this, 'addView360Tab'), 98);

        }
        if ( $chose_location == '5') {

            add_action('woocommerce_product_thumbnails', array($this, 'displayButton'));
            add_action('woocommerce_single_product_summary', array($this, 'viewShortcode360popup'));
        }
        if ( $chose_location == '6') {

            echo '';

        }
    }


    /**
     * Function for Action change_image_default_gallery
     */
    public function defaultProductView()
    {
        add_action('woocommerce_single_product_image_html', array($this, 'renderView360'));
    }


    /**
     *  Function for Action ct_short_code_view360
     */
    public function singleProductView($post)
    {

        $imageGallery = get_post_meta($post, 'image_gallery', true);
        If (!empty($imageGallery)) {
        $image2 = ctView360Display::RenderSCinGallery();
        $itemTemplate2 = '<li>%s</li>';
        echo apply_filters('woocommerce_single_product_image_html', sprintf($itemTemplate2, $image2), $post);
        }
    }


    public function decinaImages (){
        echo '<li data-target="0" class="active" >';
            echo '<img src="' . CT_VIEW360_ASSETS . '/img/pluto-icon.jpg" ';
        echo '</li>';

        }

    /**
     * Function for Action change_image_gallery
     */
    public function change_image($post)
    {
        $imageGallery = get_post_meta($post, 'image_gallery', true);
        If (! empty($imageGallery)) {
            echo '<li>';
            echo '<img src="' . CT_VIEW360_ASSETS . '/img/pluto-icon.jpg" ';
            echo '</li> ';
        }
    }

    /**
     *Add metabox choice gallery for View 360
     */
    public function choiceView360()
    {
        add_meta_box('ct_displayview360', 'Choose Gallery for View360', array(
            $this,
            'choiceRenderView360'
        ), 'product', 'side');
    }

    /**
     *  Metabox to choice Gallery
     */
    public function choiceRenderView360()
    {
        global $post;

        $post_id = $post->ID;
        $custom = CTView360SC::getCustomValue($post_id);


        $value_id = isset($custom["ct_view_select"][0]) ? $custom["ct_view_select"][0] : "'.$post->ID.'";

        // get value for custom post type
        $post_id = $post->ID;
        $post_title = $post->post_title;


        echo '<select id="ct_view_select" name="ct_view_select">';
        echo '<option value="' . $post->ID . '"' . selected(esc_attr($value_id, $post_id)) . ' > ' . $post_title . '</option>';


        $args = array(
            'post_type' => 'view360',
            'numberposts' => -1,
            'orderby' => 'title',
            'order' => "ASC"
        );

        $vc_list = get_posts($args);


        $post_id = $post->ID;

        $vc_post_id = get_post_meta($post_id, '_ct_view_select');
        if ($vc_post_id) {
            $vc_post_id = $vc_post_id[0];
        }

        foreach ($vc_list as $vc_object) {
            $vc_id = $vc_object->ID;
            $vc_title = $vc_object->post_title;

            echo '<option value="' . $vc_id . '"' . selected($vc_post_id, $vc_id) . '>' . $vc_title . '</option>';
        }

        echo '</select>';
    }

    /**
     * Add shortcode to tabs
     * @param $tabs
     * @return mixed
     */
    public function addView360Tab($tabs)
    {
        $tabs['View_360'] = array(
            'title' => "View360",
            'priority' => 98,
            'callback' => array($this, 'renderView360')
        );

        return $tabs;
    }

    /**
     * save metabox
     * @param $post_id
     */
    public function ctView360metaBoxsave($post_id)
    {
        $slug = 'product';
        // If this isn't a 'book' post, don't update it.
        if (!isset($_POST['post_type']) || $slug != $_POST['post_type']) {
            return;
        }
        if (isset($_POST['ct_view_select'])) {
            $selectedsg = $_POST['ct_view_select'];
            update_post_meta($post_id, '_ct_view_select', $selectedsg);
        }
        if (isset($_POST['my_meta_box_select_add_button'])) {
            $chose_location = $_POST['my_meta_box_select_add_button'];
            update_post_meta($post_id, 'my_meta_box_select_add_button', $chose_location);
        }
    }

    /**
     *  Add shortcode
     */
    public function renderView360()
    {
     echo self::RenderSCinGallery();
    }

    /**
     *  Add popup
     */

    public function viewShortcode360popup()
    {

        // get value for custom post type


        $button_c_lr = esc_attr(get_option('my_meta_box_button_control'));



        ?>

        <!-- Include jQuery Popup Overlay -->
        <script src="http://vast-engineering.github.io/jquery-popup-overlay/jquery.popupoverlay.js"></script>


        <style type="text/css">

            @font-face {
                font-family: 'navigation-icons';
                src:url('<?php echo CT_VIEW360_ASSETS ?>/css/fonts/navigation-icons.eot?-f5kaf7');
                src:url('<?php echo CT_VIEW360_ASSETS ?>/css/fonts/navigation-icons.eot?#iefix-f5kaf7') format('embedded-opentype'),
                url('<?php echo CT_VIEW360_ASSETS ?>/css/fonts/navigation-icons.woff?-f5kaf7') format('woff'),
                url('<?php echo CT_VIEW360_ASSETS ?>/css/fonts/navigation-icons.ttf?-f5kaf7') format('truetype'),
                url('<?php echo CT_VIEW360_ASSETS ?>/css/fonts/navigation-icons.svg?-f5kaf7#navigation-icons') format('svg');
                font-weight: normal;
                font-style: normal;
            }

            [class^="icon-"], [class*=" icon-"] {
                font-family: 'navigation-icons';
                speak: none;
                font-style: normal;
                font-weight: normal;
                font-variant: normal;
                text-transform: none;
                line-height: 1;

                /* Better Font Rendering =========== */
                -webkit-font-smoothing: antialiased;
                -moz-osx-font-smoothing: grayscale;
            }

            .icon-cross:before {
                content: "\e600";
                font-family: "navigation-icons";
            }
            .icon-arrow-left:before {
                content: "\e601";
                font-family: "navigation-icons";
            }
            .icon-arrow-right:before {
                content: "\e602";
                font-family: "navigation-icons";
            }

            .icon {
                font-size: 18px;
                color: rgba(255,255,255,.7);
                display: inline-block;
                padding: 3px;
                background-color: rgba(0,0,0,.15);
                border-radius: 3px;
                text-shadow: 0px 1px 0px rgba(150, 150, 150, 0.3);
            }

            .icon:hover {
                background-color: rgba(0,0,0,.3);
                color: rgba(255,255,255,1);
                cursor: pointer;
            }


            #button-delete {
                position: absolute;
                top: 0;
                right: 0;
            }



        </style>

        <script>


            jQuery(document).ready(function () {

                // Initialize the plugin
                jQuery('#my_popup').popup();

                jQuery("#left").click(function () {
                    jQuery(".reel").trigger("stepLeft");
                });
                jQuery("#right").click(function () {
                    jQuery(".reel").trigger("stepRight");
                });


                jQuery("#stop").click(function () {
                    jQuery(".reel").trigger("stop");
                    jQuery("#stop").hide();
                    jQuery("#play").show();
            });
                jQuery("#play").click(function () {
                    jQuery(".reel").trigger("play");
                    jQuery("#stop").show();
                    jQuery("#play").hide();
                });




            });

        </script>
        <?php


        echo '<div id="my_popup"  >';
        echo '<div >';
        $this->renderView360();
        echo '</div>';

        echo ' <a id="button-delete" class="my_popup_close icon icon-cross" ></a>';
        if ($button_c_lr == 'on') {
            echo '<div>';
            echo ' <a class="icon icon-arrow-left" id="left"></a>

            <a  id="stop" class="icon fa fa-pause"></a>
            <a  id="play" class="icon fa fa-play" style="display: none" ></a>
            <a class="icon icon-arrow-right" id="right"></a>  ';
            echo '</div>';

        }
        echo '</div>';
    }

    /**
     * Add Button
     */
    public function displayButton()
    {
        // values for CtView360PGS of BUTTON
        $open_g = esc_attr(get_option('my_meta_box_select_open_guide'));
        $button_l = esc_attr(get_option('my_meta_box_button_label'));
        $button_a = esc_attr(get_option('"my_meta_box_select_button_align'));
        $button_c = esc_attr(get_option('my_meta_box_button_class'));
        $button_v_l = esc_attr(get_option('my_meta_box_button_v_left'));
        $button_m_t = esc_attr(get_option('my_meta_box_button_m_top'));
        $button_m_r = esc_attr(get_option('my_meta_box_button_m_right'));
        $button_m_b = esc_attr(get_option('my_meta_box_button_m_bottom'));
        $button_p_c = esc_attr(get_option('my_meta_box_button_p_color'));
        $button_bb = esc_attr(get_option('my_meta_box_button_bb_color'));
        $button_p_l = esc_attr(get_option('my_meta_box_button_p_left'));
        $button_p_t = esc_attr(get_option('my_meta_box_button_p_top'));
        $button_p_r = esc_attr(get_option('"my_meta_box_button_p_right'));
        $button_p_b = esc_attr(get_option('my_meta_box_button_p_bottom'));

        if ($button_bb == $button_p_c) {
            $button_bb = '#ffffff';
        }

        if ($button_l == '') {
            $button_l = "view360";
        }

        if ($button_c == '') {
            $button_c = "button";
        }

        $margins = '';

        if ($button_v_l != 0) {
            $margins .= 'margin-left: ' . (int)$button_v_l . 'px; ';
        }
        if ($button_m_t != 0) {
            $margins .= 'margin-top: ' . (int)$button_m_t . 'px; ';
        }
        if ($button_m_r != 0) {
            $margins .= 'margin-right: ' . (int)$button_m_r . 'px; ';
        }
        if ($button_m_b != 0) {
            $margins .= 'margin-bottom: ' . (int)$button_m_b . 'px; ';
        }

        $paddings = '';

        if ($button_p_l > 0) {
            $paddings .= 'padding-left: ' . (int)$button_p_l . 'px; ';
        }
        if ($button_p_t > 0) {
            $paddings .= 'padding-top: ' . (int)$button_p_t . 'px; ';
        }
        if ($button_p_r > 0) {
            $paddings .= 'padding-right: ' . (int)$button_p_r . 'px; ';
        }
        if ($button_p_b > 0) {
            $paddings .= 'padding-bottom: ' . (int)$button_p_b . 'px; ';
        }


        ?>
        <style type="text/css">


            .Button_ct {
                font-family: "Open Sans",sans-serif;
                font-weight: 600;
                font-size: 13px;
                letter-spacing: 0.1em;
                text-decoration: none;
                padding: 10px 15px;
                color: <?php echo $button_bb ?>;
                background: <?php echo $button_p_c ?>;
                border: 1px solid <?php echo $button_bb ?>;
                border-radius: 4px;

            }

            .icon2 {
                width: 50px;
                height: 50px;
                display: block;
                background: url("<?php echo CT_VIEW360_ASSETS ?>/img/360v.png") no-repeat center center;

            }

        </style>

        <?php


        if ($open_g == "0") {
            echo '<a class="my_popup_open ' . $button_c . '" href="#ct_view" style="display: inline-block; float: ' . $button_a . '; ' . $margins . '; '. $paddings.'">' . $button_l . '</a>';
        }
        if ($open_g == "2") {
            echo '<a class="my_popup_open icon2" href="#ct_view" style="display: inline-block; float: ' . $button_a . '; ' . $margins . '"></a>';
        }
        if ($open_g == "1") {
            echo '<a class="my_popup_open" href="#ct_view" style="text-decoration: none; display: inline-block; float: ' . $button_a . '; ' . $margins . '">' . $button_l . '</a>';
        }


    }

    /**
     * Add Metabox Gallery View 360
     */
    public function galleryView360()
    {
        add_meta_box('ct_view360_gallery', 'Gallery View 360', array(
            $this,
            'galleryBox'
        ), 'product', 'normal', 'high');

    }


    /**
     * Gallery View 360
     */
    public function galleryBox()
    {

        global $post;
        ?>
        <style type="text/css">

            .gallery_images:after,
            #gallery_images_container:after {
                content: ".";
                display: block;
                height: 0;
                clear: both;
                visibility: hidden;
            }

            .gallery_images > li {
                float: left;
                cursor: move;
                margin: 0 20px 20px 0;
                position: relative;
            }

            .gallery_images li.image img {
                width: 160px;
                height: 160px;
            }

            .ct_gallery_view .delete {
                width: 16px;
                height: 16px;
                display: block;
                background: url("<?php echo CT_VIEW360_ASSETS ?>/img/cross-circle.png") no-repeat center center;
                position: absolute;
                top: 0;
                right: 0;

            }

            .gallery_images.ui-sortable > li {
                cursor: move !important;
                border: 2px solid transparent;
            }

            .gallery_images.ui-sortable > li.wc-metabox-sortable-placeholder {
                border: 2px dashed #ccc;
            }

        </style>


        <p class="add_gallery_images hide-if-no-js ">
            <a class="button button-secondary" href="#"><?php _e('Add gallery images',  'ct_v360' ); ?></a>
        </p>

        <div id="gallery_images_container" class="ct_gallery_view">

            <ul class="gallery_images">
                <?php

                $image_gallery = get_post_meta($post->ID, 'image_gallery', true);
                $attachments = array_filter(explode(',', $image_gallery));

                if ($attachments)
                    foreach ($attachments as $attachment_id) {
                        $img = wp_get_attachment_image($attachment_id, 'medium');
                        if (!empty($img)) {
                            echo '<li class="image" data-attachment_id="' . $attachment_id . '">
                            ' . $img . '
                            <ul class="actions">
                                <li><a href="#" class="delete" title="' . __('Remove image', 'ct_v360') . '"></a></li>
                            </ul>
                       </li>';
                        }
                    }
                ?>
            </ul>


            <input type="hidden" id="image_gallery" name="image_gallery"
                   value="<?php echo esc_attr($image_gallery); ?>"/>
            <?php wp_nonce_field('ImageGallery', 'ImageGallery'); ?>

        </div>

        <?php


        /**
         * Props to WooCommerce for the following JS code
         */
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {

                // Uploading files
                var image_gallery_frame;
                var $image_gallery_ids = $('#image_gallery');
                var $gallery_images = $('#gallery_images_container ul.gallery_images');

                jQuery('.add_gallery_images').on('click', 'a', function (event) {

                    var $el = $(this);
                    var attachment_ids = $image_gallery_ids.val();

                    event.preventDefault();

                    // If the media frame already exists, reopen it.
                    if (image_gallery_frame) {
                        image_gallery_frame.open();
                        return;
                    }

                    // Create the media frame.
                    image_gallery_frame = wp.media.frames.downloadable_file = wp.media({
                        // Set the title of the modal.
                        title: '<?php _e( 'Add Images to Gallery',  'ct_v360' ); ?>',
                        button: {
                            text: '<?php _e( 'Add to gallery',  'ct_v360' ); ?>'

                        },
                        multiple: true
                    });

                    // When an image is selected, run a callback.
                    image_gallery_frame.on('select', function () {

                        var selection = image_gallery_frame.state().get('selection');

                        selection.map(function (attachment) {

                            attachment = attachment.toJSON();

                            if (attachment.id) {
                                attachment_ids = attachment_ids ? attachment_ids + "," + attachment.id : attachment.id;

                                $gallery_images.append('\
                                <li class="image" data-attachment_id="' + attachment.id + '">\
                                    <img src="' + attachment.url + '" />\
                                    <ul class="actions">\
                                        <li><a href="#" class="delete" title="<?php _e( 'Remove image',  'ct_v360' ); ?>"></a></li>\
                                    </ul>\
                                </li>');
                            }

                        });

                        $image_gallery_ids.val(attachment_ids);
                    });

                    // Finally, open the modal.
                    image_gallery_frame.open();
                });

                // Image ordering
                $gallery_images.sortable({
                    items: 'li.image',
                    cursor: 'move',
                    scrollSensitivity: 40,
                    forcePlaceholderSize: true,
                    forceHelperSize: false,
                    helper: 'clone',
                    opacity: 0.65,
                    placeholder: 'wc-metabox-sortable-placeholder',
                    start: function (event, ui) {
                        ui.item.css('background-color', '#f6f6f6');
                    },
                    stop: function (event, ui) {
                        ui.item.removeAttr('style');
                    },
                    update: function (event, ui) {
                        var attachment_ids = '';

                        $('#gallery_images_container ul li.image').css('cursor', 'default').each(function () {
                            var attachment_id = jQuery(this).attr('data-attachment_id');
                            attachment_ids = attachment_ids + attachment_id + ',';
                        });

                        $image_gallery_ids.val(attachment_ids);
                    }
                });

                // Remove images
                $('#gallery_images_container').on('click', 'a.delete', function () {

                    $(this).closest('li.image').remove();

                    var attachment_ids = '';

                    $('#gallery_images_container ul li.image').css('cursor', 'default').each(function () {
                        var attachment_id = jQuery(this).attr('data-attachment_id');
                        attachment_ids = attachment_ids + attachment_id + ',';
                    });

                    $image_gallery_ids.val(attachment_ids);

                    return false;
                });

            });
        </script>
    <?php
    }

    /**
     * Save function
     *
     * @since 1.0
     */
    function viewImagegallerySavepost($post_id)
    {

        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
            return;


        if (!isset($_POST['ImageGallery']) || !wp_verify_nonce($_POST['ImageGallery'], 'ImageGallery'))
            return;

        if (isset($_POST['image_gallery']) && !empty($_POST['image_gallery'])) {

            $attachment_ids = sanitize_text_field($_POST['image_gallery']);

            // turn comma separated values into array
            $attachment_ids = explode(',', $attachment_ids);

            // clean the array
            $attachment_ids = array_filter($attachment_ids);

            // return back to comma separated list with no trailing comma. This is common when deleting the images
            $attachment_ids = implode(',', $attachment_ids);

            update_post_meta($post_id, 'image_gallery', $attachment_ids);
        } else {
            delete_post_meta($post_id, 'image_gallery');
        }

        // link to larger images
        if (isset($_POST['ImageGallery_link_images']))
            update_post_meta($post_id, 'image_gallery_link_images', $_POST['image_gallery_link_images']);
        else
            update_post_meta($post_id, 'image_gallery_link_images', 'off');

        do_action('viewImagegallerySavepost', $post_id);
    }

}

new ctView360Display();
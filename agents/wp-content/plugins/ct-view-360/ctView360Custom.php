<?php

/**
 *
 * Create Custom Post Type
 */
class ctView360Custom {

	/**
	 * Post type name
	 */
	const POST_TYPE_NAME = 'view360';

	public function __construct() {
		add_action( 'init', array( $this, 'registerTypes' ) );
	}

	/**
	 *  Register Custom Post Type
	 */

	public function registerTypes() {

		$args = array(
			'labels'          => array(
				'name'               => __('View360', 'ct_v360' ),
				'singular_name'      => __('View360','ct_v360' ),
				'add_new'            => __('Add new View360','ct_v360' ),
				'add_new_item'       => __('Add new View360','ct_v360' ),
				'edit_item'          => __('Edit View360','ct_v360' ),
				'new_item'           => __('New View360','ct_v360' ),
				'view_item'          => __('All View360','ct_v360' ),
				'search_items'       => __('search in post','ct_v360' ),
				'not_found'          => __('no found','ct_v360' ),
				'not_found_in_trash' => __('empty trash','ct_v360' )

			),
			'singular_label'  => __( "View360", 'ct_v360' ),
			'public'          => true,
			'show_ui'         => true,
			'capability_type' => 'post',
			'hierarchical'    => true,
			'rewrite'         => true,
			'supports'        => array(
				'title',
				'thumbnail'
			),
			'menu_position'   => 4,
			'query_var'       => __('view360', 'ct_v360' ),
			'menu_icon'       => 'dashicons-images-alt',
		);
		register_post_type( self::POST_TYPE_NAME, $args );
	}

}

new ctView360Custom();






<!doctype html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Web app that takes pictures</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <style>
    #newImages {
            height : 100px;
            position : relative;
            max-width : 100%;
            display:none;
        }
        img.masked {
            position : absolute;
            background-color : #fff;
            border : 1px solid #babbbd;
            padding :10px;
            box-shadow :1px 1px 1px #babbbd;
            margin : 10px auto 0;
        }
        #player {
            width : 100px;
            height : 100px;
            margin :10px auto;
        }
        canvas{
            width : 100px;
            height : 100px;
            margin : 0;
            display:none;
        }
        #capture-btn{
            width : 110px;
            margin : 0 auto;
        }
        #pick-image{
            display : none;
        }
    </style>
<div id="newImages" ></div>
<video id="player" autoplay hiiden></video>
<canvas id="canvas" width="100px" height="100px" hidden></canvas>
<button class="btn btn-primary" id="capture-btn">Capture</button>
<script src="script.js"></script>
</body>
</html>
<?php
// change the following path if necessary
defined('YII_PATH') or define('YII_PATH', (dirname(__FILE__)) . '/yii/framework');

require_once YII_PATH . '/yii.php';

$mainConfig = require dirname(__FILE__) . '/ilto3/protected/config/main.php';

include(dirname(__FILE__) . '/ilto3/protected/models/Clients.php');

$connection=new CDbConnection('mysql:host=localhost;dbname=iltoexam_newversion','iltoexam_newvers','Ilto.2015');
$connection->active=true;

?>
<!DOCTYPE html>
<html>
<head>
    <title>Tecs agent | ILTO</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF8" />
    <meta name="description" content="" />
    <meta name="Content-Language" content="ES" />
    
    <link rel="canonical" href="/" />

    
    


    <!--Skeleton Framework
<link rel="stylesheet" type="text/css" media="screen" href="/includes/libs/js/resources/framework.css" />
-->
<link rel="shortcut icon" href="media/favicon.ico">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!--jQuery-->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" ></script>
<script src="media/all_min.js"></script>


<script type="text/javascript" src="http:/maps.google.com/maps/api/js?sensor=false"></script>
<link href='includes/libs/js/fancybox/jquery.fancybox-1.3.4.css'  type="text/css" rel="stylesheet">
<script src="includes/libs/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>


<script src="http:/code.jquery.com/ui/1.10.3/jquery-ui.js"></script>


<link rel="stylesheet" type="text/css" href="easyui.css">
	<link rel="stylesheet" type="text/css" href="icon.css">
	<script type="text/javascript" src="jquery.easyui.min.js"></script>
	
    <link rel="stylesheet" type="text/css" href="includes/style.css">




    
</head>

<!--[if lt IE 7 ]> <body class="ie6 lang-ES" > <![endif]-->
<!--[if IE 7 ]> <body class="ie7 lang-ES" > <![endif]-->
<!--[if IE 8 ]> <body class="ie8 lang-ES" > <![endif]-->
<!--[if IE 9 ]> <body class="ie9 lang-ES" > <![endif]-->
<!--[if !IE]><!--> <body class="lang-ES" > <!--<![endif]-->







<div class="transit-internacional sub-page sub-landing" id="page">
<?php include("includes/header.php"); ?>

  <!-- GALLERY --> <section id="main-gallery" class="drop-shadow"> <div id="mg-container">
                      <div class="side-grid-container">
              <div class="side-grid" id="left"></div>
              <div class="side-grid" id="right"></div>
            </div>
        <div class="flexslider">
           
                      <ul class="slides"><li id="call-id-267">

                

                <div class="caption-container-wrapper">
                  <div class="caption-container">
                    <div class="caption">
                      <h5></h5>
                      <p></p>
                      <a href="" class="read-more"></a>
                    </div>
                  </div>
                </div>
                <img src="img/im_tagents3.jpg" alt=""/>
              </li>
              <li id="call-id-267">

                

                <div class="caption-container-wrapper">
                  <div class="caption-container">
                    <div class="caption">
                      <h5></h5>
                      <p></p>
                      <a href="" class="read-more"></a>
                    </div>
                  </div>
                </div>
                <img src="img/im_demo2.jpg" alt=""/>
              </li>
              
              
              </ul>
            </div>
        </div> </section> <article>  </article><!-- CONTENT --> <section id="content">
  <div class="wrapper"><ul id="breadcrumb">
    <article class="narrow large-text">
            <div class="wrapper">
                   <h1>Certificates validation</h1>
                  <h3>Enter the fraud prevention code found in the certificate.</h3>
            <?php
            //if($_REQUEST['new_agents'] == 1){
                include 'fraud_report/formFraud.php';
            //}else{
              //  include 'agents_content/oldFormAgents.php';
            //}
              ?>
              
              <p>&nbsp;</p>
          </div>

    </article> 
  <a href="/#" style="display:none;"></a>
  </div>
  </section> 
  <footer id="footer" class="drop-shadow-upwards">
<div class="wrapper"><div class="bg-wrapper"></div>

<div id="contact-info">
        <h3 style="margin-left:38px">Contact us: <a href="mailto:info@iltoexams.com" style="font-size:13px; text-transform:none">info@iltoexams.com</a></h3> <br />
        <div style="color: #FFF; padding:20px; width:250px;
-moz-border-radius: 15px 15px 15px 15px;
-webkit-border-radius: 15px 15px 15px 15px; 
border-radius: 15px 15px 15px 15px; margin-left:25px">
          <h3>International voicemail of   ILTOEXAMS</h3>
          <p>          Please leave your name, company, telephone number, city and country and the advisor corresponding your area will contact you as soon as possible.</p>
      </div>
      
   
      <div style="position: absolute;
color: #FFF;
padding: 20px;
width: 298px;
left: 362px;
top: 102px;">
 <h5> 305-853-8088</h5>
         
         
      </div>  
      
      <div style="
position: absolute;
color: #FFF;
padding: 20px;
width: 227px;
float: right;
height: 259px;
right: 2px;
top: 0px;"><br /><br /><br />
   <img src="/img/paypal.png" width="239" height="116" />
        
         
         
      </div>  <br />
   <h3 style="margin-left: 38px;
font-size: 9px;
text-transform: none;
font-family: arial;">  8300 NW 53rd Street Suite 350 Doral FL 33166<br />
     <a href="http://iltoexams.com/newtest2/" target="_blank"  style="
font-size: 9px;
text-transform: none;
font-family: arial; text-decoration:underline">Test takers v1.0</a> | <a href="http://iltoexams.com/newtest2/administrator/index.php?signIn=1" target="_blank"  style="
font-size: 9px;
text-transform: none;
font-family: arial; text-decoration:underline">Test administrator V1.0</a></h3>
                <center><h3>www.iltoexams.com</h3></center>
      </div></div>
</div></footer>

</body>
</html>
<!DOCTYPE html>
<html>
<head>
    <title>The test Security</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="Content-Language" content="ES" />
    
    <link rel="canonical" href="/" />

    
    


    <!--Skeleton Framework
<link rel="stylesheet" type="text/css" media="screen" href="/includes/libs/js/resources/framework.css" />
-->
<link rel="shortcut icon" href="media/favicon.ico">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!--jQuery-->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" ></script>
<script src="media/all_min.js"></script>


<script type="text/javascript" src="http:/maps.google.com/maps/api/js?sensor=false"></script>
<link href='includes/libs/js/fancybox/jquery.fancybox-1.3.4.css'  type="text/css" rel="stylesheet">
<script src="includes/libs/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>

<script src="includes/libs/js/infobox/infobox.js"></script>

<script src="http:/code.jquery.com/ui/1.10.3/jquery-ui.js"></script>




    <link rel="stylesheet" type="text/css" href="includes/style.css">
<script type="text/javascript" src="includes/include.js"></script>


    
</head>

<!--[if lt IE 7 ]> <body class="ie6 lang-ES" > <![endif]-->
<!--[if IE 7 ]> <body class="ie7 lang-ES" > <![endif]-->
<!--[if IE 8 ]> <body class="ie8 lang-ES" > <![endif]-->
<!--[if IE 9 ]> <body class="ie9 lang-ES" > <![endif]-->
<!--[if !IE]><!--> <body class="lang-ES" > <!--<![endif]-->

<script type="text/javascript">
/* <![CDATA[ */* ]]> */
</script>





<div class="transit-internacional sub-page sub-landing" id="page">
<?php include("includes/header.php"); ?>

  <!-- GALLERY --> <section id="main-gallery" class="drop-shadow"> <div id="mg-container">
                      <div class="side-grid-container">
              <div class="side-grid" id="left"></div>
              <div class="side-grid" id="right"></div>
            </div>
        <div class="flexslider">
           
                      <ul class="slides"><li id="call-id-267">

                

                <div class="caption-container-wrapper">
                  <div class="caption-container">
                    <div class="caption">
                      <h5> </h5>
                      <p></p>
                      <a href="" class="read-more"></a>
                    </div>
                  </div>
                </div>
                <img src="img/im_sec1.jpg" alt=" "/>
              </li><li id="call-id-456">

                

                <div class="caption-container-wrapper">
                  <div class="caption-container">
                    <div class="caption">
                      <h5></h5>
                      <p></p>
                      <a href="" class="read-more"></a>
                    </div>
                  </div>
                </div>
                <img src="img/im_sec2.jpg" alt=""/>
              </li></ul>
    </div>
        </div> </section> <article>  </article><!-- CONTENT --> <section id="content">
  <div class="wrapper"><ul id="breadcrumb">
    <li class="home"><a href="../"></a></li><li class="class">THE TEST SECURITY</li></ul> <article class="narrow large-text">
            <div class="wrapper">

                      <h1>THE TEST SECURITY</h1>
  
              <p>&nbsp;</p>
              <p><strong>How does ILTO protect the test from fraud?</strong><br>
              We cover 4 main areas in order to protect our exams from fraud and assure their results according to the candidate levels: Reliability, security, research and development. </p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p><strong>Reliability:</strong><br>
              In later years it is more common to find that people who would like to study, work of live in English speaking countries or people who are requested by organizations international exams to approve certain processes such as job promotions or school graduation. Organizations trust in the reliability of the exam and accuracy of the results. This is why ILTO has developed an organized method in which our representatives are an important component. All the technological implementation of our test guarantees the protection for the test and the physical reliability of the results. </p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p><strong>Research</strong><br>
              ILTO permanently studies different ways to prevent fraud risks. All tests results are strictly monitored in order to find any uncommon sign of a student’s performance. The first part of the test includes the speaking test, in which we make sure, with biometric resources, that the test taker in fact is the person interested in taking the test. We also encourage our representatives to be aware of possible identity theft by candidates. </p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p><strong>Security</strong><br>
              To know exactly the identity of the person taking the test, we only give results to individuals who take it at our representative’s premises. All our representatives are asked to have suitable locations where they can assure the security of the test. </p>
              <p>&nbsp;</p>
              <p>                The day of the test, the test taker cannot take to the test venue any electronic device such as voice recorder, camera, smart phones, tablets, flash drives or any other material that may be used to record questions, answers or allow communication with a person outside the room. The test is supervised by personnel willing to help solve any technical issue, but alert to any fraud possibility. </p>
              <p>&nbsp;</p>
              <p>                In addition, the student must fill out a sing up form and place the fingerprint in the biometric device, which should coincide the day of the test. </p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p><strong>Development</strong><br>
              We work every day in finding new and innovative ways to secure the exam and promote trust in the organizations interested in administering the test or accepting it as part of the individual’s certification of the English knowledge. </p>
              <p>&nbsp;</p>
              <p>                To be able to accept new representatives for the TECS, they must meet strict requirements. It is not enough with only willing to sell the exam or purchase several tests.</p>
              <p>&nbsp; </p>
              <p><strong>ILTO</strong> has created a Secure Representative Certificate (SRC) which is granted to the organizations, when meeting all the security standards used by ILTO. This certification is shown by our representatives to guarantee organizations the reliability of the test results. </p>
              <p>&nbsp;</p>
              <p>                Our representatives receive special training in the management of the test and permanent support in their special requests. </p>
              <p>&nbsp;</p>
              <p><strong>ILTO</strong> updates the questions every year to make sure candidates have different communication samples every time the take the test. </p>
              <p>                The results of the TECS are valid for onw year. Past this time, the candidate must take another test to validate the results on the use of the language.<br>
              </p>
              <p>&nbsp;</p>
              <a href="/terms.php" style="color: #069;text-decoration: underline;font-size: 16px;" target="_blank">Terms and Conditions</a> 
        </div>

          </article> 
  <a href="/#" style="display:none;"></a></div>
  </section> 
<?php include("includes/pie.php"); ?>

    	</body>
</html>

<!DOCTYPE html>
<html>
<head>
    <title>ILTO - International Language Testing Organization</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="ILTO accredited by the Accrediting Council for Continuing education and Training. After a rigorous process we have been granted this important accreditation, giving trust to our students, test takers and" />
    <meta name="Content-Language" content="ES" />
    
    <link rel="canonical" href="/" />

    <!--Skeleton Framework
<link rel="stylesheet" type="text/css" media="screen" href="/includes/libs/js/resources/framework.css" />
-->
<link rel="shortcut icon" href="media/favicon.ico">
<link href="/ilto3/themes/ilto/html/assets/css/bootstrap.min.css" rel="stylesheet" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!--jQuery-->

<script type="text/javascript" src="/lib/jquery-1.8.3.js" ></script>
<script src="media/all_min.js"></script>


<script type="text/javascript" src="http:/maps.google.com/maps/api/js?sensor=false"></script>
<link href='includes/libs/js/fancybox/jquery.fancybox-1.3.4.css'  type="text/css" rel="stylesheet">
<script src="includes/libs/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>

<script src="includes/libs/js/infobox/infobox.js"></script>

<script src="http:/code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

<link rel="stylesheet" type="text/css" href="includes/style.css">
<script type="text/javascript" src="includes/include.js"></script>

    
</head>

<!--[if lt IE 7 ]> <body class="ie6 lang-ES" > <![endif]-->
<!--[if IE 7 ]> <body class="ie7 lang-ES" > <![endif]-->
<!--[if IE 8 ]> <body class="ie8 lang-ES" > <![endif]-->
<!--[if IE 9 ]> <body class="ie9 lang-ES" > <![endif]-->
<!--[if !IE]><!--> <body class="lang-ES" > <!--<![endif]-->

<script type="text/javascript">
/* <![CDATA[ */* ]]> */
</script>


<style>
/* Popup box BEGIN */
.hover_bkgr_fricc{
    background:rgba(0,0,0,.4);
    cursor:pointer;
    display:none;
    height:100%;
    position:fixed;
    text-align:center;
    top:0;
    width:100%;
    z-index:10000;
}
.hover_bkgr_fricc .helper{
    display:inline-block;
    height:100%;
    vertical-align:middle;
}
.hover_bkgr_fricc > div {
    background-color: #fff;
    box-shadow: 10px 10px 60px #555;
    display: inline-block;
    height: auto;
    max-width: 551px;
    min-height: 100px;
    vertical-align: middle;
    width: 60%;
    position: relative;
    border-radius: 8px;
    padding: 15px 5%;
}
.popupCloseButton {
   background-color: #c50d0d;
    border: 2px solid #fff;
    border-radius: 50px;
    cursor: pointer;
    display: inline-block;
    font-family: arial;
    font-weight: bold;
    position: absolute;
    top: -20px;
    right: -20px;
    font-size: 25px;
    line-height: 30px;
    width: 30px;
    height: 30px;
    text-align: center;
    color: #fff;
}
.popupCloseButton:hover {
    background-color: #ccc;
}
.trigger_popup_fricc {
    cursor: pointer;
    font-size: 20px;
    margin: 20px;
    display: inline-block;
    font-weight: bold;
}
/* Popup box BEGIN */
</style>
<div id="page" class="home">


<?php include("includes/header.php"); ?>

<section id="main-gallery" class="drop-shadow"><div id="mg-container">
                      <div class="side-grid-container">
              <div class="side-grid" id="left"></div>
              <div class="side-grid" id="right"></div>
            </div>
        <div class="flexslider">
           
                      <ul class="slides"><li id="call-id-567">


                <div class="caption-container-wrapper">
                  <div class="caption-container">
                    <div class="caption">
                      <h5>A reliable alternative </h5>
                      <p>for language testing</p>
                      <a href="www.blulogistics.com/aduana-importacion-exportacion-es/intermediacion-aduanera-es/" class="read-more size_231">Ver más</a>
                    </div>
                  </div>
                </div>
                <img src="img/img_p1.jpg" alt="3PL"/>
              </li><li id="call-id-563">


                <div class="caption-container-wrapper">
                  <div class="caption-container">
                    <div class="caption">
                      <h5>Adaptable</h5>
                      <p></p>
                      <a href="" class="read-more size_231"></a>
                    </div>
                  </div>
                </div>
                <img src="img/img_p2.jpg" alt="Conocimiento"/>
              </li><li id="call-id-559">


                <div class="caption-container-wrapper">
                  <div class="caption-container">
                    <div class="caption">
                      <h5>accurate </h5>
                      <p></p>
                      <a href="" class="read-more size_231"></a>
                    </div>
                  </div>
                </div>
                <img src="img/img_p3.jpg" alt="SOMOS LOGISTICA "/>
              </li><li id="call-id-555">


                <div class="caption-container-wrapper">
                  <div class="caption-container">
                    <div class="caption">
                      <h5>Secure</h5>
                      <p></p>
                      <a href="" class="read-more"></a>
                    </div>
                  </div>
                </div>
                <img src="img/img_p4.jpg" alt="7 PAISES, 26 CIUDADES"/>
              </li></ul>
            </div>
        </div></section>
<section id="content">
<div class="banners_wrapper_wrap_inner"><!--
  <div class="row ">
    <div class="col-md-3">
        <div class="banner-wrap  wow fadeIn animated" style="border:1px solid black;">
            <figure class="featured-thumbnail"><a href="shop" title="Gifts for <strong>Her</strong>"></a></figure>
                <div class="content-wrapper">
                    <h6>Test<strong>Takers</strong></h6>
                    <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                    <div class="link-align banner-btn"><a href="shop" title="Shop now!" class="btn btn-link" target="_self">Shop now!</a>
                    </div>
                </div>
        </div> 
    </div>
    <div class="col-md-3">
        <div class="banner-wrap  wow fadeIn animated" data-wow-duration="1s" data-wow-delay="0.2s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.2s; animation-name: fadeIn;">
            <figure class="featured-thumbnail"><a href="shop" title="Gifts for <strong>Her</strong>"><img src="https://livedemo00.template-help.com/woocommerce_53330/wp-content/uploads/2015/02/banner1-img.jpg" title="Gifts for <strong>Her</strong>" alt=""></a></figure>
                <div class="content-wrapper">
                    <h6>Test <strong>Administrator</strong></h6>
                    <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                    <div class="link-align banner-btn"><a href="shop" title="Shop now!" class="btn btn-link" target="_self">Shop now!</a>
                    </div>
                </div>
        </div> 
    </div>
    <div class="col-md-3">
        <div class="banner-wrap  wow fadeIn animated" data-wow-duration="1s" data-wow-delay="0.2s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.2s; animation-name: fadeIn;">
            <figure class="featured-thumbnail"><a href="shop" title="Gifts for <strong>Her</strong>"><img src="https://livedemo00.template-help.com/woocommerce_53330/wp-content/uploads/2015/02/banner1-img.jpg" title="Gifts for <strong>Her</strong>" alt=""></a></figure>
                <div class="content-wrapper">
                    <h6>Become an <strong>Agent</strong></h6>
                    <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                    <div class="link-align banner-btn"><a href="shop" title="Shop now!" class="btn btn-link" target="_self">Shop now!</a>
                    </div>
                </div>
        </div> 
    </div>
    <div class="col-md-3">
        <div class="banner-wrap  wow fadeIn animated" data-wow-duration="1s" data-wow-delay="0.2s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.2s; animation-name: fadeIn;">
            <figure class="featured-thumbnail"><a href="shop" title="Gifts for <strong>Her</strong>"><img src="https://livedemo00.template-help.com/woocommerce_53330/wp-content/uploads/2015/02/banner1-img.jpg" title="Gifts for <strong>Her</strong>" alt=""></a></figure>
                <div class="content-wrapper">
                    <h6>Certificates <strong>Validation</strong></h6>
                    <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                    <div class="link-align banner-btn"><a href="shop" title="Shop now!" class="btn btn-link" target="_self">Shop now!</a>
                    </div>
                </div>
        </div> 
      </div>
    </div>
</div>-->

<div class="hover_bkgr_fricc">
    <span class="helper"></span>
    <div>
        <div class="popupCloseButton">X</div>
        <h3 style="color:#31b744;">Notice</h3> 
<p>Please note that our old phone number 305-853-8088 is now obsolete. Effective October 1st you can reach us at <b>+1 (305) 882-9190 </b>
Business Hours
Monday - Friday
9:00 am - 5:00 pm</p><p style="text-align:center;">
<img style="margin-left: 30px;" src="/img/contact-number.png"></p>
    </div>
</div>
<div class="wrapper"><article class="narrow large-text nqte">
  <div id="blu-1759" class="q-ote">
    <a href="/ilto3/index.php?r=Clients/default/login" target="_blank">
      <img src="img/img_b1.png" alt="Test takers" style="-webkit-box-shadow: 5px 10px 5px 0px rgba(0,0,0,0.75);-moz-box-shadow: 5px 10px 5px 0px rgba(0,0,0,0.75);box-shadow: 5px 10px 5px 0px rgba(0,0,0,0.75);"> 
      <div class="overlay"></div>
      <div class="h-quote"><h2> Test takers</h2></div>
    </a>
  </div>
  <div id="blu-1763" class="q-ote">
    <a href="/ilto3/index.php?r=Clients/default/login" target="_blank">
      <img src="img/img_b2.png" alt="Test administrator" style="-webkit-box-shadow: 5px 10px 5px 0px rgba(0,0,0,0.75);
      -moz-box-shadow: 5px 10px 5px 0px rgba(0,0,0,0.75);
      box-shadow: 5px 10px 5px 0px rgba(0,0,0,0.75);"> 
      <div class="overlay"></div>
      <div class="h-quote"><h2>Test administrator</h2></div>
    </a>
  </div>
  <!-- <div id="blu-1765" class="q-ote"> 
    <a href="/testilto" target="_blank">
      <img src="img/b_Demo.jpg" alt="Tecs agent" style="-webkit-box-shadow: 5px 10px 5px 0px rgba(0,0,0,0.75);
      -moz-box-shadow: 5px 10px 5px 0px rgba(0,0,0,0.75);
      box-shadow: 5px 10px 5px 0px rgba(0,0,0,0.75);"> 
      <div class="overlay"></div>
      <div class="h-quote"><h2>TECS  DEMO<br></h2></div>
    </a>
  </div> -->
  <div id="blu-1766" class="q-ote"> 
    <a href="https://www.iltoexams.com/agents/">
      <img src="img/b_Demo.jpg" alt="Tecs agent" style="-webkit-box-shadow: 5px 10px 5px 0px rgba(0,0,0,0.75);
      -moz-box-shadow: 5px 10px 5px 0px rgba(0,0,0,0.75);
      box-shadow: 5px 10px 5px 0px rgba(0,0,0,0.75);"> 
      <div class="overlay"></div>
      <div class="h-quote"><h2>BECOME AN AGENT<br></h2></div>
    </a>
  </div>
  <div id="blu-1767" class="q-ote"> 
    <a href="/fraud-alert.php">
      <img src="img/fraud.jpg" alt="Tecs agent" style="-webkit-box-shadow: 5px 10px 5px 0px rgba(0,0,0,0.75);
      -moz-box-shadow: 5px 10px 5px 0px rgba(0,0,0,0.75);
      box-shadow: 5px 10px 5px 0px rgba(0,0,0,0.75);"> 
      <div class="overlay"></div>
      <div class="h-quote"><h2>Certificates validation<br></h2>
      </div>
    </a>
  </div>

</article>
<article id="linea-state"> 
   <div class="upper-shadow">
              <ul class="featured-general">
              <li class="trigger INDEX col1_ilto_index" id="line-140">
 <a id="section-140" hrer="/transporte-internacional-es/agentes-internacionales-es"> <h3>
TECS AGENTS 
 </h3></a>
                  <p>The International Languge Testing Organization (ILTO) cares for its agents, thus provide them with all the academic and technical support needed to implement the TECS in the organization. </p>
                  <a href="tecs-agent.php" class="read-more">Read more</a>

                </li><li class="trigger INDEX col2_ilto_index " id="line-122">
 <a id="section-122" hrer="#"> <h3>SCHOOLS AND UNIVERSITIES</h3></a>
                  <p>The ILTO team permanently develops new and improved instruments to help education organizations optimize their resources and have a reliable source to improve their second or foreign language development.
</p>
                  <a href="tecs-agent.php" class="read-more">Read more</a>

                </li><li class="trigger INDEX col3_ilto_index " id="line-141">
 <a id="section-141" hrer="/transporte-terrestre-es/transporte-terrestre"> <h3>ORGANIZATIONS</h3></a>
                  <p>Being able to communicate in English is paramount to be able to really achieve the goals of the organization, since making business with foreign companies, no matter what the country is, demands high proficiency in English in employees and entrepreneurs.</p>
                  <a href="tecs-agent.php" class="read-more">Read more</a>

                </li><li class="trigger INDEX  col4_ilto_index" id="line-142">
 <a id="section-142" hrer="/almacenamiento-es/depositos-almacienamento"> <h3>DOWNLOAD</h3></a>
                  <p>Interested in knowing more about ILTOEXAMS and the TECS?
<br>
<br>
Download advertising material, research and study documents for the TECS and ILTO </p>
                  <a href="download.php" class="read-more">Read more</a>

                </li>
                
                
                </ul></div>
     </section>
      <div style="position:relative; width:550px; height:200px;margin:auto;padding-top:10px;padding-bottom:10px;">
            <h2 style="color: #31B744; text-align:center;">Members of</h2>
            <div style="float:left;margin-left:50px;">
              <a href="https://iltaonline.site-ym.com/page/InstitutionalMembers" target="_blank"><img src="/img/ilta.png" alt="ilta" style="margin-top: 10px;"/></a>
            <h4 style="text-align:center;color: #31B744;"><a href="https://iltaonline.site-ym.com/page/InstitutionalMembers" target="_blank">International Language Testing <br/>Association</a></h4>
            </div>
            <div style="float:left;margin-left:30px;">
              <a href="http://www.ncta-testing.org/" target="_blank"><img src="/img/ncta.png" alt="ncta" style="margin-top: 10px;"/></a>
               <h4 style="text-align:center;color: #31B744;"> <a href="http://www.ncta-testing.org/" target="_blank">National College Testing <br/>Association</a></h4>
            </div>
    
   
    
        </div>
<a href="/#" style="display:none;"></a></div>
</section>
<script>
$(window).load(function () {
    
       $('.hover_bkgr_fricc').show();
    
    $('.hover_bkgr_fricc').click(function(){
        $('.hover_bkgr_fricc').hide();
    });
    $('.popupCloseButton').click(function(){
        $('.hover_bkgr_fricc').hide();
    });
});
</script>
<?php include("includes/pie.php"); ?>    	</body>
        </html>
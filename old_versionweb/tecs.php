<!DOCTYPE html>
<html>
<head>
    <title>TECS | ILTO</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="Content-Language" content="ES" />
    
    <link rel="canonical" href="/" />

    
    


    <!--Skeleton Framework
<link rel="stylesheet" type="text/css" media="screen" href="/includes/libs/js/resources/framework.css" />
-->
<link rel="shortcut icon" href="media/favicon.ico">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!--jQuery-->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" ></script>
<script src="media/all_min.js"></script>


<script type="text/javascript" src="http:/maps.google.com/maps/api/js?sensor=false"></script>
<link href='includes/libs/js/fancybox/jquery.fancybox-1.3.4.css'  type="text/css" rel="stylesheet">
<script src="includes/libs/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>

<script src="includes/libs/js/infobox/infobox.js"></script>

<script src="http:/code.jquery.com/ui/1.10.3/jquery-ui.js"></script>




    <link rel="stylesheet" type="text/css" href="includes/style.css">
<script type="text/javascript" src="includes/include.js"></script>


    
</head>

<!--[if lt IE 7 ]> <body class="ie6 lang-ES" > <![endif]-->
<!--[if IE 7 ]> <body class="ie7 lang-ES" > <![endif]-->
<!--[if IE 8 ]> <body class="ie8 lang-ES" > <![endif]-->
<!--[if IE 9 ]> <body class="ie9 lang-ES" > <![endif]-->
<!--[if !IE]><!--> <body class="lang-ES" > <!--<![endif]-->

<script type="text/javascript">
/* <![CDATA[ */* ]]> */
</script>





<div class="transit-internacional sub-page sub-landing" id="page">
<?php include("includes/header.php"); ?>

  <!-- GALLERY --> <section id="main-gallery" class="drop-shadow"> <div id="mg-container">
                      <div class="side-grid-container">
              <div class="side-grid" id="left"></div>
              <div class="side-grid" id="right"></div>
            </div>
        <div class="flexslider">
           
                      <ul class="slides"><li id="call-id-267">

                

                <div class="caption-container-wrapper">
                  <div class="caption-container">
                    <div class="caption">
                      <h5></h5>
                      <p></p>
                      <a href="" class="read-more"></a>
                    </div>
                  </div>
                </div>
                <img src="img/im_tcs1.jpg" alt=""/>
              </li><li id="call-id-456">

                

                <div class="caption-container-wrapper">
                  <div class="caption-container">
                    <div class="caption">
                      <h5> </h5>
                      <p></p>
                      <a href="" class="read-more size_231"></a>
                    </div>
                  </div>
                </div>
                <img src="img/im_tcs2.jpg" alt=" "/>
              </li></ul>
            </div>
        </div> </section> <article>  </article><!-- CONTENT --> <section id="content">
  <div class="wrapper"><ul id="breadcrumb">
    <li class="home"><a href="../"></a></li><li class="class">TECS</li></ul> <article class="narrow large-text">
            <div class="wrapper">

                      <h1><img src="img/tecslogoonly.png"></h1>
            
              <p><strong>Objective: </strong><br>
              Our main objective is to give students, education institutions and public and private companies interested in certifying the English level of their stakeholders, a reliable, practical and affordable option according to the world objective of improving the English proficiency in the countries.</p>
              <p>&nbsp;</p>
              <p><strong>Test Takers:</strong><br>
                The test pretends to assess the use of the English language in a general communicative context in order to categorize the proficiency level of the candidate for day to day communication purposes. <br>
                With more than 10.000 previous tests and based in a complete research work, the TECS has proven to be reliable and accurate when willing to certify the English level of the individual. </p>
              <p>&nbsp;</p>
              <img src="img/TECS-SCORE.png" width="544" height="397">
              <p>&nbsp;              </p>
              <p><strong>The TECS and the Common European Framework of Reference for Languages</strong><br>
                The results for the tests have been long studied in order to establish a correspondence as accurate as possible with the Common European Framework of Reference For Languages, created by the council of Europe. For further information on the CEFR <a href="http:/www.coe.int/t/dg4/linguistic/cadre1_en.asp" target="_blank" style="color:#069; text-decoration:underline; font-size:16px">click here</a>. </p>
              <p>&nbsp;</p>
              <p><strong>The Exam Structure </strong><br>
                Questions are presented in different forms taking into account the communication strategies of the individuals and the real use of the language. This is why professionals in different areas provide valuable ideas for the construction of questions and communication activities related to real exchanges of people</p>
              <p>&nbsp;</p>
              <p><strong>The Question Types </strong><br>
                Multiple answers with reference to a contextualized sentence.</p>
              <p>                Multiple answers with reference to a text of current interest. </p>
              <p>                Description of current use images. </p>
              <p>                Assessment of listening skills based on real people conversations, radio shows and television talk shows and speeches. <br>
                Reading comprehension from real newspapers and magazines. </p>
              <p>&nbsp;</p>
              <p><strong>The Skills Assessed</strong> <br>
              The TECS considers the four main features of communication, in order to know how a candidate will perform in a regular communication task in a day to day basis. This is done through 4 different modules:</p>
              <p><img src="img/TECS-MODULES.png" width="500" height="433"></p>
              <p>&nbsp;</p>
              <p>Questions, texts and audio samples are updated every year to guarantee the assessment of the real use of the language in real contexts. </p>
              <p>On line PDF certificates. </p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p><strong>The Results:</strong><br>
              The results issued by ILTO will be given to the candidate according to each exam administrator and their processes. </p>
              <p>                The results are valid for 12 months. Past this time, the candidate must take a new test to certify the language proficiency.</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p><strong>The TECS Results Explained</strong><br>
              <img src="img/P.png" width="322" height="397" align=right hspace=5 vspace=5>The score is presented in a scale from 0 to 100. This does not show the amount of correct answers, but the performance of the candidate on the test, and so the language proficiency through scaled standardized results. </p>
              <p>&nbsp;</p>
              <p>                Apart from the general performance score, the TECS shows the following individual results:</p>
              <p>&nbsp;</p>
              <ul class="STYL">
                <li> Results for the conversation module.</li>
                <li> Results for the listening in context module. </li>
                <li> Results for the reading in context module. </li>
                <li> Results for the meaningful and interactive use of the language module. </li>
              </ul>
              <p>&nbsp;</p>
              <p>Results aligned with the Common European Framework for Language Reference</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <a href="/terms.php" style="color: #069;text-decoration: underline;font-size: 16px;" target="_blank">Terms and Conditions</a>  
            </div>
    </article></div>
  </section> 
<?php include("includes/pie.php"); ?>

    	</body>
        </html>
<?php
header ('Location: /tecs-agent.php');
exit();
?>

<!DOCTYPE html>
<html>
<head>
    <title>SCHOOLS AND UNIVERSITIES | ILTO</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="Content-Language" content="ES" />
    
    <link rel="canonical" href="/" />

    
    


    <!--Skeleton Framework
<link rel="stylesheet" type="text/css" media="screen" href="/includes/libs/js/resources/framework.css" />
-->
<link rel="shortcut icon" href="media/favicon.ico">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!--jQuery-->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" ></script>
<script src="media/all_min.js"></script>


<script type="text/javascript" src="http:/maps.google.com/maps/api/js?sensor=false"></script>
<link href='includes/libs/js/fancybox/jquery.fancybox-1.3.4.css'  type="text/css" rel="stylesheet">
<script src="includes/libs/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>

<script src="includes/libs/js/infobox/infobox.js"></script>

<script src="http:/code.jquery.com/ui/1.10.3/jquery-ui.js"></script>




<link rel="stylesheet" type="text/css" href="easyui.css">
	<link rel="stylesheet" type="text/css" href="icon.css">
	<script type="text/javascript" src="jquery.easyui.min.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#ff').form({
				success:function(data){
					$.messager.alert('Info', data, 'info');
					document.getElementById("ff'").reset();
					
				}
			});
			$('#f2').form({
				success:function(data){
					$.messager.alert('Info', data, 'info');
				}
			});
		});
	</script>
    <link rel="stylesheet" type="text/css" href="includes/style.css">
<script type="text/javascript" src="includes/include.js"></script>





    
</head>

<!--[if lt IE 7 ]> <body class="ie6 lang-ES" > <![endif]-->
<!--[if IE 7 ]> <body class="ie7 lang-ES" > <![endif]-->
<!--[if IE 8 ]> <body class="ie8 lang-ES" > <![endif]-->
<!--[if IE 9 ]> <body class="ie9 lang-ES" > <![endif]-->
<!--[if !IE]><!--> <body class="lang-ES" > <!--<![endif]-->

<script type="text/javascript">
/* <![CDATA[ */* ]]> */
</script>





<div class="transit-internacional sub-page sub-landing" id="page">
<?php include("includes/header.php"); ?>

  <!-- GALLERY --> <section id="main-gallery" class="drop-shadow"> <div id="mg-container">
                      <div class="side-grid-container">
              <div class="side-grid" id="left"></div>
              <div class="side-grid" id="right"></div>
            </div>
        <div class="flexslider">
           
                      <ul class="slides"><li id="call-id-267">

                

                <div class="caption-container-wrapper">
                  <div class="caption-container">
                    <div class="caption">
                      <h5></h5>
                      <p></p>
                      <a href="" class="read-more size_231"></a>
                    </div>
                  </div>
                </div>
                <img src="img/im_school1.jpg" alt=""/>
              </li><li id="call-id-456">

                

                <div class="caption-container-wrapper">
                  <div class="caption-container">
                    <div class="caption">
                      <h5></h5>
                      <p></p>
                      <a href="" class="read-more "></a>
                    </div>
                  </div>
                </div>
                <img src="img/im_school.jpg" alt=""/>
              </li></ul>
            </div>
        </div> </section> <article>  </article><!-- CONTENT --> <section id="content">
  <div class="wrapper"><ul id="breadcrumb">
    <li class="home"><a href="../"></a></li>
    <li class="class">School and Universities</li></ul> <article class="narrow large-text">
            <div class="wrapper">

                      <h1>CERTIFY THE ENGLISH PROFICIENCY IN YOUR INSTITUTION</h1>
             
              <p><br>
              Take a look at how the TECS can help your institution have more competitive alumni who achieve better academic results and job positions.</p>
              <p>&nbsp;</p>
              <p>&nbsp; </p>
              <p><strong>Products and Services</strong></p>
              <p> The ILTO team permanently develops new and improved instruments to help education orgajnizations optimize their resources and have a reliable source to improve their second or foreign language development.</p>
              <p> <br>
              Our tests and its platform are supervised and subject to permanent academic and technological analysis to measure its effectiveness and accuracy in order to warranty organizations the best results without outrageous investments. </p>
              <p><br>
              We know how important it is to demand quality when it refers to language testing in order to achieve outstanding education goals, thus we develop tests that contribute to achieve the success of the organization projects.</p>
              <p><br>
              Our test provides organizations with secure and reliable information to assure a real measure in the academic process of the students. Moreover, by permanently updating the contents of the test, we expose students to contextualized pieces of information of general interest found in everyday communication which probably everyone has had contact with. </p>
              <p>&nbsp;</p>
              <p>This strategy aims to motivate students to give an answer based on their real life, decreasing the possible bias the exams may have due to the pressure and tension of doing an exam about something totally unknown, thus encouraging the student to perform according to the way they would perform in a real communicative situation, assuring better and more reliable results. <br>
              </p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p><strong>The Main Features of the Test</strong></p>
              <p>Updated and real context questions 4 assessment methods to make it more dynamic.</p>
              <p>&nbsp;</p>
              <ul class="STYL">
                <li>Quick online results.</li>
                <li>Results Certificate.</li>
                <li>Secure and reliable results. </li>
                <li>Permanent support</li>
              </ul>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p><strong>Schools and Universities</strong></p>
              <p>Register as teacher or administrator of an education organization and receive 1 free TECS licenses that you can use in your institution. Fill out the form if you would like to do the test and receive more information. </p>
              <p>&nbsp;</p>
              <p>Remember that fields marked with * are mandatory. </p>
<p><br>
              <p><strong>Organization Information:</strong></p>
			
              <p><form id="ff" action="form1_proc.php" method="post">
			<table width="597">
				<tr>
				  <td>
                                      
                                      <input type="hidden" name="typeform" value="schooluniver">
                                      <input name="Organizationname" type="text" placeholder="Organization name" required></td>
					<td>&nbsp;</td> 
					<td><input name="EducationLevel" type="text" placeholder="Education Level" required></input></td>
				</tr>
				<tr>
				  <td><input name="Website" type="text" placeholder="Website" required></td>
					<td>&nbsp;</td>
					<td><input name="Phonenumber" type="text" placeholder="Phone number"   required></input></td>
				</tr>
				<tr>
                  <td><input name="City" type="text" placeholder="City"  required></td>
                  <td>&nbsp;</td>
                  <td><input name="Country" type="text" placeholder="Country"   required></td>
			  </tr>
			
				<tr>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
			  </tr>
				<tr>
				  <td> <p><strong>Contact Information:</strong><br>
              </p></td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
			  </tr>
				<tr>
				  <td><input name="FirstName" type="text" placeholder="First Name"  required></td>
					<td>&nbsp;</td> 
					<td><input name="LastName" type="text" placeholder="Last Name"  required></input></td>
				</tr>
				<tr>
				  <td><input name="Phone Number" type="text" placeholder="Phone Number"  required></td>
					<td>&nbsp;</td>
					<td><input name="Email" type="text" placeholder="Email"   required></input></td>
				</tr>
				<tr>
                  <td>How did you know about us?</td>
                  <td>&nbsp;</td>
                  <td><select name="howdidyouknow"  required>
                    <option value="Facebook">Facebook</option>
                    <option value="Google search">Google search</option>
                    <option value="Magazine">Magazine</option>
                    <option value="TV add">TV add</option>
                    <option value="Newspaper">Newspaper</option>
                    <option value="Email">Email</option>
                    <option value="Twitter">Twitter</option>
                    <option value="Other Website">Other Website</option>
                    <option value="Education Events">Education Events</option>
                  </select></td>
			  </tr>
				<tr>
				  <td>              I would like to receive more information on ILTO or TECS products or new releases. </td>
				  <td>&nbsp;</td>
				  <td><p>
				    <label>
				    <input type="radio" name="Wouldlike" value="Yes">
  Yes</label>
				    <label>
				    <input type="radio" name="Wouldlike" value="No">
  No</label>
				    <br>
			      </p></td>
			  </tr>
				<tr>
				  <td colspan="3">&nbsp;</td>
			  </tr>
				<tr>
				  <td colspan="3"><input type="submit" value="Submit"></input></td>
				</tr>
			</table>
		</form>
              <p>&nbsp;</p>          </div>

          </article> 
  <a href="/#" style="display:none;"></a></div>
  </section> 
<?php include("includes/pie.php"); ?>

    	</body>
        </html>
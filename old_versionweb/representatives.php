<!DOCTYPE html>
<html>
<head>
    <title>Que es ILTO</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="Content-Language" content="ES" />
    
    <link rel="canonical" href="/" />

    
    


    <!--Skeleton Framework
<link rel="stylesheet" type="text/css" media="screen" href="/includes/libs/js/resources/framework.css" />
-->
<link rel="shortcut icon" href="media/favicon.ico">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!--jQuery-->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" ></script>
<script src="media/all_min.js"></script>


<script type="text/javascript" src="http:/maps.google.com/maps/api/js?sensor=false"></script>
<link href='includes/libs/js/fancybox/jquery.fancybox-1.3.4.css'  type="text/css" rel="stylesheet">
<script src="includes/libs/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>

<script src="includes/libs/js/infobox/infobox.js"></script>

<script src="http:/code.jquery.com/ui/1.10.3/jquery-ui.js"></script>




    <link rel="stylesheet" type="text/css" href="includes/style.css">
<script type="text/javascript" src="includes/include.js"></script>


    
</head>

<!--[if lt IE 7 ]> <body class="ie6 lang-ES" > <![endif]-->
<!--[if IE 7 ]> <body class="ie7 lang-ES" > <![endif]-->
<!--[if IE 8 ]> <body class="ie8 lang-ES" > <![endif]-->
<!--[if IE 9 ]> <body class="ie9 lang-ES" > <![endif]-->
<!--[if !IE]><!--> <body class="lang-ES" > <!--<![endif]-->

<script type="text/javascript">
/* <![CDATA[ */* ]]> */
</script>





<div class="transit-internacional sub-page sub-landing" id="page">
<?php include("includes/header.php"); ?>

  <!-- GALLERY --> <section id="main-gallery" class="drop-shadow"> <div id="mg-container">
                      <div class="side-grid-container">
              <div class="side-grid" id="left"></div>
              <div class="side-grid" id="right"></div>
            </div>
        <div class="flexslider">
           
                      <ul class="slides"><li id="call-id-267">

                

                <div class="caption-container-wrapper">
                  <div class="caption-container">
                    <div class="caption">
                      <h5>Somos Logística </h5>
                      <p>Soluciones Logísticas Integrales.</p>
                      <a href="" class="read-more size_231"></a>
                    </div>
                  </div>
                </div>
                <img src="media/galeria-baner-internacional-color1.jpg" alt="Somos Logística "/>
              </li><li id="call-id-456">

                

                <div class="caption-container-wrapper">
                  <div class="caption-container">
                    <div class="caption">
                      <h5>Excelentes relaciones </h5>
                      <p>Con los mejores Agentes a nivel mundial</p>
                      <a href="" class="read-more size_231"></a>
                    </div>
                  </div>
                </div>
                <img src="media/galeria-baner-internacional-color3.jpg" alt="Excelentes relaciones "/>
              </li><li id="call-id-269">

                

                <div class="caption-container-wrapper">
                  <div class="caption-container">
                    <div class="caption">
                      <h5>Oficinas Propias</h5>
                      <p>En 7 países, 26 Ciudades</p>
                      <a href="" class="read-more size_231"></a>
                    </div>
                  </div>
                </div>
                <img src="media/galeria-baner-internacional-color2.jpg" alt="Oficinas Propias"/>
              </li></ul>
            </div>
        </div> </section> <article>  </article><!-- CONTENT --> <section id="content">
  <div class="wrapper"><ul id="breadcrumb">
    <li class="home"><a href="../"></a></li>
    <li class="class">Representantes</li></ul> <article class="narrow large-text">
            <div class="wrapper">

                      <h1>Representantes</h1>
              <!--Más de 15 años de experiencia en Transporte Internacional de carga nos permite brindarles a nuestros clientes el respaldo y alcance que sus operaciones logísticas exigen.-->
              <p><strong>REPRESENTATIVES</strong><br>
IMPLEMENTE EN SU INSTUTICIÓN EL EXAMEN TECS: <br>
Analice como nuestra prueba TECS pueden ayudar a su institución a graduar estudiantes  más competitivos para el mercado laboral.<br>
<strong>PRODUCTOS Y SERVICIOS</strong></p>
              <p>Como parte del equipo de trabajo  de ILTO EXAM, desarrollamos  cada día más y mejores herramientas  para la evolución   de individuos válidos y confiables  para las instituciones de  educación superior para ayudarle a optimizar sus recursos. Nuestros cuentan con la suficiente preparación académica y están sometidos a diversas pruebas de efectividad y veracidad para garantizar a prestigiosas instituciones  los mejores resultados, sin grandes inversiones en dinero.<br>
                En ILTO  que la demanda en la calidad es cada  en obtener los mejores resultados en los procesos de educación, para eso desarrollamos pruebas que realmente ayuden a  proporcionar  estrategias que les  garanticen el éxito.<br>
                Nuestro examen de clasificación proporciona datos reales y confiables  para ayudarle a asegurarse de que sus estudiantes realmente están tomando con seriedad el proceso de formación. Además, con el contenido de nuestra prueba motivamos a los estudiantes  para obtener los mejores resultados debido a que la información contenida en la prueba esta basada en temas de interés general y situaciones  con los que muy probablemente todos hemos tenido inter acción, de esta manera  el estudiante no se siente bombardeado de preguntas que tal  vez no comprende.</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p><strong>CARACTERÍSTICAS DE LA PRUEBA:</strong></p>
              <ul>
                <li>Preguntas actualizadas y modernas.</li>
                <li>4 métodos de evaluación  que lo hacen más dinámico.</li>
                <li>Resultados al instante.</li>
                <li>Certificado de resultados.</li>
                <li>Fiabilidad y seguridad en los resultados.</li>
                <li>Correcta clasificación del tomador  en el nivel correspondiente.</li>
                <li>Asistencia permanente.</li>
              </ul>
              <p><strong>INTERNATIONAL LANGUAGE TESTING ORGANIZATION</strong>  (ILTO)  apoya a sus representantes proporcionado toda la documentación y el soporte que sea necesario para la implementación de la prueba TECS, dentro de su institución, adicionalmente nuestros representantes reciben apoyo como material POP  para que los tomadores del examen  conozcan y  se familiaricen con la prueba.<br>
                Pensando en la seguridad y la confiabilidad de los resultados del examen TECS nuestros representantes  PRIMIUM, cuenta<br>
                n con la implementación por parte de ILTO de un sistema de seguridad a través de un lector de huellas que puede garantizar el 100% de  veracidad de la prueba.<br>
                Dependiendo de la cantidad de licencias que su institución adquiera así mismo obtiene descuentos especiales y ofertas exclusivas, como la personalización de las prueba  con toda la imagen corporativa de su institución.<br>
                Si desea mas información puede contactar a través de nuestro buzón internacional  N°xxxxxxxxx, sus solicitudes de información son atendidas con mucha celeridad, allí puede dejarnos su información y uno de nuestros agentes  de acuerdo al país y ciudad a la que usted corresponda  lo contactara para aclarar sus dudas.<br>
                Si su Institución desea obtener  10 licencias de prueba  para su institución diligencie la siguiente información.</p>
              <p>Aquí va el formulario ……<br>
                <strong>INSTITUCIONES EDUCATIVAS</strong>: <br>
                Regístrese como  docente o administrativo de una institución y obtenga 10 licencias de prueba del examen TECS  que puede aplicarlas en su institución, llene los espacios en blanco si desea aplicar el examen y obtener más información del mismo.<br>
                Debe proporcionar los  datos personales y los datos  sobre su institución si lo desea, debe completar todos los campos marcados con un&nbsp;<strong>*</strong>.&nbsp;<br>
                Aquí debemos poner un pequeño texto de protección de datos</p>
              <p>&nbsp;</p>
              <p>INFORMACION DE LA INSTITUCION <br>
                Nombre de la Institución: <br>
                Nivel de Educación<br>
                Sitio web:<br>
                Teléfono de contacto:<br>
                Ciudad:<br>
                País:</p>
              <p>&nbsp;</p>
              <p>INFORMACION DE CONTACTO: <br>
                Nombre:<br>
                Apellido:<br>
                Teléfono de contacto:<br>
                Email:<br>
                ¿Cómo se obtuvo información sobre nosotros?</p>
              <p>Me gustaría recibir  Información sobre productos ILTO o el examen TECS.<br>
                SI<br>
                NO</p>
            </div>

          </article> 
  <a href="/#" style="display:none;"></a></div>
  </section> 
<?php include("includes/pie.php"); ?>

    	</body>
        </html>
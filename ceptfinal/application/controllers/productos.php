<?php
class Productos extends CI_Controller {

   function index(){
	   
	   $datos = array(
         'titulo' => 'Página de prueba',
         'descripcion' => 'Esta es la descripción de esta página, un poco más larga.',
         'cuerpo' => 'El cuerpo de la página probablemente será un texto muy largo...<p>Con varios párrafos</p>'
      );
      
      $this->load->view('mivista', $datos);
   }
   
   function ordenadores($marca=null, $modelo=null){
      if (is_null($marca) && is_null($modelo)){
         echo 'Aquí se muestran los productos de ordenadores';
      }elseif(is_null($modelo)){
         echo 'Mostrando ordenadores de marca ' . $marca;
      }else{
         echo 'Mostrando ordenadores de marca ' . $marca . ' y modelo ' . $modelo;
      }
   }
   
   function monitores(){
      echo 'Aquí se muestran los productos de monitores';
   }
   
   function perifericos($modelo){
      echo 'Estás viendo el periférico ' . $modelo;
   }
}
?>
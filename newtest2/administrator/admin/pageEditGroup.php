<?php
	$d=dirname(__FILE__);
	require("$d/incCommon.php");

	// get groupID of anonymous group
	$anonGroupID=sqlValue("select groupID from membership_groups where name='".$adminConfig['anonymousGroup']."'");

	// request to save changes?
	if($_POST['saveChanges']!=''){
		// validate data
		$name=makeSafe($_POST['name']);
		$description=makeSafe($_POST['description']);
		switch($_POST['visitorSignup']){
			case 0:
				$allowSignup=0;
				$needsApproval=1;
				break;
			case 2:
				$allowSignup=1;
				$needsApproval=0;
				break;
			default:
				$allowSignup=1;
				$needsApproval=1;
		}
		###############################
		$ex_respuestas_insert=checkPermissionVal('ex_respuestas_insert');
		$ex_respuestas_view=checkPermissionVal('ex_respuestas_view');
		$ex_respuestas_edit=checkPermissionVal('ex_respuestas_edit');
		$ex_respuestas_delete=checkPermissionVal('ex_respuestas_delete');
		###############################
		$ex_empresas_insert=checkPermissionVal('ex_empresas_insert');
		$ex_empresas_view=checkPermissionVal('ex_empresas_view');
		$ex_empresas_edit=checkPermissionVal('ex_empresas_edit');
		$ex_empresas_delete=checkPermissionVal('ex_empresas_delete');
		###############################
		$ex_empresas_usuarios_insert=checkPermissionVal('ex_empresas_usuarios_insert');
		$ex_empresas_usuarios_view=checkPermissionVal('ex_empresas_usuarios_view');
		$ex_empresas_usuarios_edit=checkPermissionVal('ex_empresas_usuarios_edit');
		$ex_empresas_usuarios_delete=checkPermissionVal('ex_empresas_usuarios_delete');
		###############################
		$ex_examenes_insert=checkPermissionVal('ex_examenes_insert');
		$ex_examenes_view=checkPermissionVal('ex_examenes_view');
		$ex_examenes_edit=checkPermissionVal('ex_examenes_edit');
		$ex_examenes_delete=checkPermissionVal('ex_examenes_delete');
		###############################
		$ex_examenes_preguntas_insert=checkPermissionVal('ex_examenes_preguntas_insert');
		$ex_examenes_preguntas_view=checkPermissionVal('ex_examenes_preguntas_view');
		$ex_examenes_preguntas_edit=checkPermissionVal('ex_examenes_preguntas_edit');
		$ex_examenes_preguntas_delete=checkPermissionVal('ex_examenes_preguntas_delete');
		###############################
		$ex_preguntas_insert=checkPermissionVal('ex_preguntas_insert');
		$ex_preguntas_view=checkPermissionVal('ex_preguntas_view');
		$ex_preguntas_edit=checkPermissionVal('ex_preguntas_edit');
		$ex_preguntas_delete=checkPermissionVal('ex_preguntas_delete');
		###############################
		$ex_preguntas_respuestas_insert=checkPermissionVal('ex_preguntas_respuestas_insert');
		$ex_preguntas_respuestas_view=checkPermissionVal('ex_preguntas_respuestas_view');
		$ex_preguntas_respuestas_edit=checkPermissionVal('ex_preguntas_respuestas_edit');
		$ex_preguntas_respuestas_delete=checkPermissionVal('ex_preguntas_respuestas_delete');
		###############################
		$ex_resultados_insert=checkPermissionVal('ex_resultados_insert');
		$ex_resultados_view=checkPermissionVal('ex_resultados_view');
		$ex_resultados_edit=checkPermissionVal('ex_resultados_edit');
		$ex_resultados_delete=checkPermissionVal('ex_resultados_delete');
		###############################
		$ex_usuarios_insert=checkPermissionVal('ex_usuarios_insert');
		$ex_usuarios_view=checkPermissionVal('ex_usuarios_view');
		$ex_usuarios_edit=checkPermissionVal('ex_usuarios_edit');
		$ex_usuarios_delete=checkPermissionVal('ex_usuarios_delete');
		###############################
		$ex_usuarios_examenes_insert=checkPermissionVal('ex_usuarios_examenes_insert');
		$ex_usuarios_examenes_view=checkPermissionVal('ex_usuarios_examenes_view');
		$ex_usuarios_examenes_edit=checkPermissionVal('ex_usuarios_examenes_edit');
		$ex_usuarios_examenes_delete=checkPermissionVal('ex_usuarios_examenes_delete');
		###############################

		// new group or old?
		if($_POST['groupID']==''){ // new group
			// make sure group name is unique
			if(sqlValue("select count(1) from membership_groups where name='$name'")){
				echo "<div class=\"error\">Error: Group name already exists. You must choose a unique group name.</div>";
				include("$d/incFooter.php");
			}

			// add group
			sql("insert into membership_groups set name='$name', description='$description', allowSignup='$allowSignup', needsApproval='$needsApproval'", $eo);

			// get new groupID
			$groupID=mysql_insert_id();

		}else{ // old group
			// validate groupID
			$groupID=intval($_POST['groupID']);

			if($groupID==$anonGroupID){
				$name=$adminConfig['anonymousGroup'];
				$allowSignup=0;
				$needsApproval=0;
			}

			// make sure group name is unique
			if(sqlValue("select count(1) from membership_groups where name='$name' and groupID!='$groupID'")){
				echo "<div class=\"error\">Error: Group name already exists. You must choose a unique group name.</div>";
				include("$d/incFooter.php");
			}

			// update group
			sql("update membership_groups set name='$name', description='$description', allowSignup='$allowSignup', needsApproval='$needsApproval' where groupID='$groupID'", $eo);

			// reset then add group permissions
			sql("delete from membership_grouppermissions where groupID='$groupID' and tableName='ex_respuestas'", $eo);
			sql("delete from membership_grouppermissions where groupID='$groupID' and tableName='ex_empresas'", $eo);
			sql("delete from membership_grouppermissions where groupID='$groupID' and tableName='ex_empresas_usuarios'", $eo);
			sql("delete from membership_grouppermissions where groupID='$groupID' and tableName='ex_examenes'", $eo);
			sql("delete from membership_grouppermissions where groupID='$groupID' and tableName='ex_examenes_preguntas'", $eo);
			sql("delete from membership_grouppermissions where groupID='$groupID' and tableName='ex_preguntas'", $eo);
			sql("delete from membership_grouppermissions where groupID='$groupID' and tableName='ex_preguntas_respuestas'", $eo);
			sql("delete from membership_grouppermissions where groupID='$groupID' and tableName='ex_resultados'", $eo);
			sql("delete from membership_grouppermissions where groupID='$groupID' and tableName='ex_usuarios'", $eo);
			sql("delete from membership_grouppermissions where groupID='$groupID' and tableName='ex_usuarios_examenes'", $eo);
		}

		// add group permissions
		if($groupID){
			// table 'ex_respuestas'
			sql("insert into membership_grouppermissions set groupID='$groupID', tableName='ex_respuestas', allowInsert='$ex_respuestas_insert', allowView='$ex_respuestas_view', allowEdit='$ex_respuestas_edit', allowDelete='$ex_respuestas_delete'", $eo);
			// table 'ex_empresas'
			sql("insert into membership_grouppermissions set groupID='$groupID', tableName='ex_empresas', allowInsert='$ex_empresas_insert', allowView='$ex_empresas_view', allowEdit='$ex_empresas_edit', allowDelete='$ex_empresas_delete'", $eo);
			// table 'ex_empresas_usuarios'
			sql("insert into membership_grouppermissions set groupID='$groupID', tableName='ex_empresas_usuarios', allowInsert='$ex_empresas_usuarios_insert', allowView='$ex_empresas_usuarios_view', allowEdit='$ex_empresas_usuarios_edit', allowDelete='$ex_empresas_usuarios_delete'", $eo);
			// table 'ex_examenes'
			sql("insert into membership_grouppermissions set groupID='$groupID', tableName='ex_examenes', allowInsert='$ex_examenes_insert', allowView='$ex_examenes_view', allowEdit='$ex_examenes_edit', allowDelete='$ex_examenes_delete'", $eo);
			// table 'ex_examenes_preguntas'
			sql("insert into membership_grouppermissions set groupID='$groupID', tableName='ex_examenes_preguntas', allowInsert='$ex_examenes_preguntas_insert', allowView='$ex_examenes_preguntas_view', allowEdit='$ex_examenes_preguntas_edit', allowDelete='$ex_examenes_preguntas_delete'", $eo);
			// table 'ex_preguntas'
			sql("insert into membership_grouppermissions set groupID='$groupID', tableName='ex_preguntas', allowInsert='$ex_preguntas_insert', allowView='$ex_preguntas_view', allowEdit='$ex_preguntas_edit', allowDelete='$ex_preguntas_delete'", $eo);
			// table 'ex_preguntas_respuestas'
			sql("insert into membership_grouppermissions set groupID='$groupID', tableName='ex_preguntas_respuestas', allowInsert='$ex_preguntas_respuestas_insert', allowView='$ex_preguntas_respuestas_view', allowEdit='$ex_preguntas_respuestas_edit', allowDelete='$ex_preguntas_respuestas_delete'", $eo);
			// table 'ex_resultados'
			sql("insert into membership_grouppermissions set groupID='$groupID', tableName='ex_resultados', allowInsert='$ex_resultados_insert', allowView='$ex_resultados_view', allowEdit='$ex_resultados_edit', allowDelete='$ex_resultados_delete'", $eo);
			// table 'ex_usuarios'
			sql("insert into membership_grouppermissions set groupID='$groupID', tableName='ex_usuarios', allowInsert='$ex_usuarios_insert', allowView='$ex_usuarios_view', allowEdit='$ex_usuarios_edit', allowDelete='$ex_usuarios_delete'", $eo);
			// table 'ex_usuarios_examenes'
			sql("insert into membership_grouppermissions set groupID='$groupID', tableName='ex_usuarios_examenes', allowInsert='$ex_usuarios_examenes_insert', allowView='$ex_usuarios_examenes_view', allowEdit='$ex_usuarios_examenes_edit', allowDelete='$ex_usuarios_examenes_delete'", $eo);
		}

		// redirect to group editing page
		redirect("pageEditGroup.php?groupID=$groupID");

	}elseif($_GET['groupID']!=''){
		// we have an edit request for a group
		$groupID=intval($_GET['groupID']);
	}

	include("$d/incHeader.php");

	if($groupID!=''){
		// fetch group data to fill in the form below
		$res=sql("select * from membership_groups where groupID='$groupID'", $eo);
		if($row=mysql_fetch_assoc($res)){
			// get group data
			$name=$row['name'];
			$description=$row['description'];
			$visitorSignup=($row['allowSignup']==1 && $row['needsApproval']==1 ? 1 : ($row['allowSignup']==1 ? 2 : 0));

			// get group permissions for each table
			$res=sql("select * from membership_grouppermissions where groupID='$groupID'", $eo);
			while($row=mysql_fetch_assoc($res)){
				$tableName=$row['tableName'];
				$vIns=$tableName."_insert";
				$vUpd=$tableName."_edit";
				$vDel=$tableName."_delete";
				$vVue=$tableName."_view";
				$$vIns=$row['allowInsert'];
				$$vUpd=$row['allowEdit'];
				$$vDel=$row['allowDelete'];
				$$vVue=$row['allowView'];
			}
		}else{
			// no such group exists
			echo "<div class=\"error\">Error: Group not found!</div>";
			$groupID=0;
		}
	}
?>
<h1><?php echo ($groupID ? "Edit Group '$name'" : "Add New Group"); ?></h1>
<?php if($anonGroupID==$groupID){ ?>
	<div class="status">Attention! This is the anonymous group.</div>
<?php } ?>
<input type="checkbox" id="showToolTips" value="1" checked><label for="showToolTips">Show tool tips as mouse moves over options</label>
<form method="post" action="pageEditGroup.php">
	<input type="hidden" name="groupID" value="<?php echo $groupID; ?>">
	<table border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="right" class="tdFormCaption" valign="top">
				<div class="formFieldCaption">Group name</div>
				</td>
			<td align="left" class="tdFormInput">
				<input type="text" name="name" <?php echo ($anonGroupID==$groupID ? "readonly" : ""); ?> value="<?php echo $name; ?>" size="20" class="formTextBox">
				<br />
				<?php if($anonGroupID==$groupID){ ?>
					The name of the anonymous group is read-only here.
				<?php }else{ ?>
					If you name the group '<?php echo $adminConfig['anonymousGroup']; ?>', it will be considered the anonymous group<br />
					that defines the permissions of guest visitors that do not log into the system.
				<?php } ?>
				</td>
			</tr>
		<tr>
			<td align="right" valign="top" class="tdFormCaption">
				<div class="formFieldCaption">Description</div>
				</td>
			<td align="left" class="tdFormInput">
				<textarea name="description" cols="50" rows="5" class="formTextBox"><?php echo $description; ?></textarea>
				</td>
			</tr>
		<?php if($anonGroupID!=$groupID){ ?>
		<tr>
			<td align="right" valign="top" class="tdFormCaption">
				<div class="formFieldCaption">Allow visitors to sign up?</div>
				</td>
			<td align="left" class="tdFormInput">
				<?php
					echo htmlRadioGroup(
						"visitorSignup",
						array(0, 1, 2),
						array(
							"No. Only the admin can add users.",
							"Yes, and the admin must approve them.",
							"Yes, and automatically approve them."
						),
						($groupID ? $visitorSignup : $adminConfig['defaultSignUp'])
					);
				?>
				</td>
			</tr>
		<?php } ?>
		<tr>
			<td colspan="2" align="right" class="tdFormFooter">
				<input type="submit" name="saveChanges" value="Save changes">
				</td>
			</tr>
		<tr>
			<td colspan="2" class="tdFormHeader">
				<table border="0" cellspacing="0" cellpadding="0" width="100%">
					<tr>
						<td class="tdFormHeader" colspan="5"><h2>Table permissions for this group</h2></td>
						</tr>
					<?php
						// permissions arrays common to the radio groups below
						$arrPermVal=array(0, 1, 2, 3);
						$arrPermText=array("No", "Owner", "Group", "All");
					?>
					<tr>
						<td class="tdHeader"><div class="ColCaption">Table</div></td>
						<td class="tdHeader"><div class="ColCaption">Insert</div></td>
						<td class="tdHeader"><div class="ColCaption">View</div></td>
						<td class="tdHeader"><div class="ColCaption">Edit</div></td>
						<td class="tdHeader"><div class="ColCaption">Delete</div></td>
						</tr>
				<!-- ex_respuestas table -->
					<tr>
						<td class="tdCaptionCell" valign="top">Answers</td>
						<td class="tdCell" valign="top">
							<input onMouseOver="stm(ex_respuestas_addTip, toolTipStyle);" onMouseOut="htm();" type="checkbox" name="ex_respuestas_insert" value="1" <?php echo ($ex_respuestas_insert ? "checked class=\"highlight\"" : ""); ?>>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_respuestas_view", $arrPermVal, $arrPermText, $ex_respuestas_view, "highlight");
							?>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_respuestas_edit", $arrPermVal, $arrPermText, $ex_respuestas_edit, "highlight");
							?>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_respuestas_delete", $arrPermVal, $arrPermText, $ex_respuestas_delete, "highlight");
							?>
							</td>
						</tr>
				<!-- ex_empresas table -->
					<tr>
						<td class="tdCaptionCell" valign="top">Companies</td>
						<td class="tdCell" valign="top">
							<input onMouseOver="stm(ex_empresas_addTip, toolTipStyle);" onMouseOut="htm();" type="checkbox" name="ex_empresas_insert" value="1" <?php echo ($ex_empresas_insert ? "checked class=\"highlight\"" : ""); ?>>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_empresas_view", $arrPermVal, $arrPermText, $ex_empresas_view, "highlight");
							?>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_empresas_edit", $arrPermVal, $arrPermText, $ex_empresas_edit, "highlight");
							?>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_empresas_delete", $arrPermVal, $arrPermText, $ex_empresas_delete, "highlight");
							?>
							</td>
						</tr>
				<!-- ex_empresas_usuarios table -->
					<tr>
						<td class="tdCaptionCell" valign="top">Companies Users</td>
						<td class="tdCell" valign="top">
							<input onMouseOver="stm(ex_empresas_usuarios_addTip, toolTipStyle);" onMouseOut="htm();" type="checkbox" name="ex_empresas_usuarios_insert" value="1" <?php echo ($ex_empresas_usuarios_insert ? "checked class=\"highlight\"" : ""); ?>>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_empresas_usuarios_view", $arrPermVal, $arrPermText, $ex_empresas_usuarios_view, "highlight");
							?>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_empresas_usuarios_edit", $arrPermVal, $arrPermText, $ex_empresas_usuarios_edit, "highlight");
							?>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_empresas_usuarios_delete", $arrPermVal, $arrPermText, $ex_empresas_usuarios_delete, "highlight");
							?>
							</td>
						</tr>
				<!-- ex_examenes table -->
					<tr>
						<td class="tdCaptionCell" valign="top">Exam</td>
						<td class="tdCell" valign="top">
							<input onMouseOver="stm(ex_examenes_addTip, toolTipStyle);" onMouseOut="htm();" type="checkbox" name="ex_examenes_insert" value="1" <?php echo ($ex_examenes_insert ? "checked class=\"highlight\"" : ""); ?>>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_examenes_view", $arrPermVal, $arrPermText, $ex_examenes_view, "highlight");
							?>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_examenes_edit", $arrPermVal, $arrPermText, $ex_examenes_edit, "highlight");
							?>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_examenes_delete", $arrPermVal, $arrPermText, $ex_examenes_delete, "highlight");
							?>
							</td>
						</tr>
				<!-- ex_examenes_preguntas table -->
					<tr>
						<td class="tdCaptionCell" valign="top">Exams Questions</td>
						<td class="tdCell" valign="top">
							<input onMouseOver="stm(ex_examenes_preguntas_addTip, toolTipStyle);" onMouseOut="htm();" type="checkbox" name="ex_examenes_preguntas_insert" value="1" <?php echo ($ex_examenes_preguntas_insert ? "checked class=\"highlight\"" : ""); ?>>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_examenes_preguntas_view", $arrPermVal, $arrPermText, $ex_examenes_preguntas_view, "highlight");
							?>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_examenes_preguntas_edit", $arrPermVal, $arrPermText, $ex_examenes_preguntas_edit, "highlight");
							?>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_examenes_preguntas_delete", $arrPermVal, $arrPermText, $ex_examenes_preguntas_delete, "highlight");
							?>
							</td>
						</tr>
				<!-- ex_preguntas table -->
					<tr>
						<td class="tdCaptionCell" valign="top">Questions</td>
						<td class="tdCell" valign="top">
							<input onMouseOver="stm(ex_preguntas_addTip, toolTipStyle);" onMouseOut="htm();" type="checkbox" name="ex_preguntas_insert" value="1" <?php echo ($ex_preguntas_insert ? "checked class=\"highlight\"" : ""); ?>>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_preguntas_view", $arrPermVal, $arrPermText, $ex_preguntas_view, "highlight");
							?>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_preguntas_edit", $arrPermVal, $arrPermText, $ex_preguntas_edit, "highlight");
							?>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_preguntas_delete", $arrPermVal, $arrPermText, $ex_preguntas_delete, "highlight");
							?>
							</td>
						</tr>
				<!-- ex_preguntas_respuestas table -->
					<tr>
						<td class="tdCaptionCell" valign="top">Questions Answers</td>
						<td class="tdCell" valign="top">
							<input onMouseOver="stm(ex_preguntas_respuestas_addTip, toolTipStyle);" onMouseOut="htm();" type="checkbox" name="ex_preguntas_respuestas_insert" value="1" <?php echo ($ex_preguntas_respuestas_insert ? "checked class=\"highlight\"" : ""); ?>>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_preguntas_respuestas_view", $arrPermVal, $arrPermText, $ex_preguntas_respuestas_view, "highlight");
							?>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_preguntas_respuestas_edit", $arrPermVal, $arrPermText, $ex_preguntas_respuestas_edit, "highlight");
							?>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_preguntas_respuestas_delete", $arrPermVal, $arrPermText, $ex_preguntas_respuestas_delete, "highlight");
							?>
							</td>
						</tr>
				<!-- ex_resultados table -->
					<tr>
						<td class="tdCaptionCell" valign="top">Results</td>
						<td class="tdCell" valign="top">
							<input onMouseOver="stm(ex_resultados_addTip, toolTipStyle);" onMouseOut="htm();" type="checkbox" name="ex_resultados_insert" value="1" <?php echo ($ex_resultados_insert ? "checked class=\"highlight\"" : ""); ?>>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_resultados_view", $arrPermVal, $arrPermText, $ex_resultados_view, "highlight");
							?>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_resultados_edit", $arrPermVal, $arrPermText, $ex_resultados_edit, "highlight");
							?>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_resultados_delete", $arrPermVal, $arrPermText, $ex_resultados_delete, "highlight");
							?>
							</td>
						</tr>
				<!-- ex_usuarios table -->
					<tr>
						<td class="tdCaptionCell" valign="top">Users</td>
						<td class="tdCell" valign="top">
							<input onMouseOver="stm(ex_usuarios_addTip, toolTipStyle);" onMouseOut="htm();" type="checkbox" name="ex_usuarios_insert" value="1" <?php echo ($ex_usuarios_insert ? "checked class=\"highlight\"" : ""); ?>>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_usuarios_view", $arrPermVal, $arrPermText, $ex_usuarios_view, "highlight");
							?>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_usuarios_edit", $arrPermVal, $arrPermText, $ex_usuarios_edit, "highlight");
							?>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_usuarios_delete", $arrPermVal, $arrPermText, $ex_usuarios_delete, "highlight");
							?>
							</td>
						</tr>
				<!-- ex_usuarios_examenes table -->
					<tr>
						<td class="tdCaptionCell" valign="top">Users Exams</td>
						<td class="tdCell" valign="top">
							<input onMouseOver="stm(ex_usuarios_examenes_addTip, toolTipStyle);" onMouseOut="htm();" type="checkbox" name="ex_usuarios_examenes_insert" value="1" <?php echo ($ex_usuarios_examenes_insert ? "checked class=\"highlight\"" : ""); ?>>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_usuarios_examenes_view", $arrPermVal, $arrPermText, $ex_usuarios_examenes_view, "highlight");
							?>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_usuarios_examenes_edit", $arrPermVal, $arrPermText, $ex_usuarios_examenes_edit, "highlight");
							?>
							</td>
						<td class="tdCell">
							<?php
								echo htmlRadioGroup("ex_usuarios_examenes_delete", $arrPermVal, $arrPermText, $ex_usuarios_examenes_delete, "highlight");
							?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		<tr>
			<td colspan="2" align="right" class="tdFormFooter">
				<input type="submit" name="saveChanges" value="Save changes">
				</td>
			</tr>
		</table>
</form>


<?php
	include("$d/incFooter.php");
?>
<?php
// This script and data application were generated by AppGini 4.70
// Download AppGini for free from http://bigprof.com/appgini/download/

	$d=dirname(__FILE__);
	include("$d/defaultLang.php");
	include("$d/language.php");
	include("$d/lib.php");
	@include("$d/hooks/ex_preguntas_respuestas.php");
	include("$d/ex_preguntas_respuestas_dml.php");

	// mm: can the current member access this page?
	$perm=getTablePermissions('ex_preguntas_respuestas');
	if(!$perm[0]){
		echo StyleSheet();
		echo "<div class=\"error\">".$Translation['tableAccessDenied']."</div>";
		echo '<script language="javaScript">setInterval("window.location=\'index.php?signOut=1\'", 2000);</script>';
		exit;
	}

	$x = new DataList;
	$x->TableName = "ex_preguntas_respuestas";

	// Fields that can be displayed in the table view
	$x->QueryFieldsTV=array(
		"`ex_preguntas_respuestas`.`id_preg_resp`" => "id_preg_resp",
		"if(char_length(`ex_preguntas1`.`cod_pregunta`) || char_length(`ex_preguntas1`.`texto_pregunta`), concat_ws('', `ex_preguntas1`.`cod_pregunta`, ' - ', `ex_preguntas1`.`texto_pregunta`), '') /* ID Question */" => "cod_pregunta",
		"if(char_length(`ex_respuestas1`.`cod_respuesta`) || char_length(`ex_respuestas1`.`texto_respuesta`), concat_ws('', `ex_respuestas1`.`cod_respuesta`, ' - ', `ex_respuestas1`.`texto_respuesta`), '') /* ID Answer */" => "cod_respuesta",
		"`ex_preguntas_respuestas`.`respuesta_correcta`" => "respuesta_correcta"
	);
	// Fields that can be displayed in the csv file
	$x->QueryFieldsCSV=array(
		"`ex_preguntas_respuestas`.`id_preg_resp`" => "id_preg_resp",
		"if(char_length(`ex_preguntas1`.`cod_pregunta`) || char_length(`ex_preguntas1`.`texto_pregunta`), concat_ws('', `ex_preguntas1`.`cod_pregunta`, ' - ', `ex_preguntas1`.`texto_pregunta`), '') /* ID Question */" => "cod_pregunta",
		"if(char_length(`ex_respuestas1`.`cod_respuesta`) || char_length(`ex_respuestas1`.`texto_respuesta`), concat_ws('', `ex_respuestas1`.`cod_respuesta`, ' - ', `ex_respuestas1`.`texto_respuesta`), '') /* ID Answer */" => "cod_respuesta",
		"`ex_preguntas_respuestas`.`respuesta_correcta`" => "respuesta_correcta"
	);
	// Fields that can be filtered
	$x->QueryFieldsFilters=array(
		"`ex_preguntas_respuestas`.`id_preg_resp`" => "ID QA",
		"if(char_length(`ex_preguntas1`.`cod_pregunta`) || char_length(`ex_preguntas1`.`texto_pregunta`), concat_ws('', `ex_preguntas1`.`cod_pregunta`, ' - ', `ex_preguntas1`.`texto_pregunta`), '') /* ID Question */" => "ID Question",
		"if(char_length(`ex_respuestas1`.`cod_respuesta`) || char_length(`ex_respuestas1`.`texto_respuesta`), concat_ws('', `ex_respuestas1`.`cod_respuesta`, ' - ', `ex_respuestas1`.`texto_respuesta`), '') /* ID Answer */" => "ID Answer",
		"`ex_preguntas_respuestas`.`respuesta_correcta`" => "Answer Correct?"
	);

	// Fields that can be quick searched
	$x->QueryFieldsQS=array(
		"`ex_preguntas_respuestas`.`id_preg_resp`" => "id_preg_resp",
		"if(char_length(`ex_preguntas1`.`cod_pregunta`) || char_length(`ex_preguntas1`.`texto_pregunta`), concat_ws('', `ex_preguntas1`.`cod_pregunta`, ' - ', `ex_preguntas1`.`texto_pregunta`), '') /* ID Question */" => "cod_pregunta",
		"if(char_length(`ex_respuestas1`.`cod_respuesta`) || char_length(`ex_respuestas1`.`texto_respuesta`), concat_ws('', `ex_respuestas1`.`cod_respuesta`, ' - ', `ex_respuestas1`.`texto_respuesta`), '') /* ID Answer */" => "cod_respuesta",
		"`ex_preguntas_respuestas`.`respuesta_correcta`" => "respuesta_correcta"
	);

	$x->QueryFrom="`ex_preguntas_respuestas` LEFT JOIN `ex_preguntas` as ex_preguntas1 ON `ex_preguntas_respuestas`.`cod_pregunta`=ex_preguntas1.`cod_pregunta` LEFT JOIN `ex_respuestas` as ex_respuestas1 ON `ex_preguntas_respuestas`.`cod_respuesta`=ex_respuestas1.`cod_respuesta` ";
	$x->QueryWhere='';
	$x->QueryOrder='';

	$x->AllowSelection = 1;
	$x->HideTableView = ($perm[2]==0 ? 1 : 0);
	$x->AllowDelete = $perm[4];
	$x->AllowInsert = $perm[1];
	$x->AllowUpdate = $perm[3];
	$x->SeparateDV = 0;
	$x->AllowDeleteOfParents = 0;
	$x->AllowFilters = 1;
	$x->AllowSavingFilters = 0;
	$x->AllowSorting = 1;
	$x->AllowNavigation = 1;
	$x->AllowPrinting = 1;
	$x->AllowPrintingMultiSelection = 0;
	$x->AllowCSV = 1;
	$x->RecordsPerPage = 20;
	$x->QuickSearch = 3;
	$x->QuickSearchText = $Translation["quick search"];
	$x->ScriptFileName = "ex_preguntas_respuestas_view.php";
	$x->RedirectAfterInsert = "ex_preguntas_respuestas_view.php?SelectedID=#ID#";
	$x->TableTitle = "Questions Answers";
	$x->PrimaryKey = "`ex_preguntas_respuestas`.`id_preg_resp`";

	$x->ColWidth   = array(100, 400, 400, 150);
	$x->ColCaption = array("ID QA", "ID Question", "ID Answer", "Answer Correct?");
	$x->ColNumber  = array(1, 2, 3, 4);

	$x->Template = 'templates/ex_preguntas_respuestas_templateTV.html';
	$x->SelectedTemplate = 'templates/ex_preguntas_respuestas_templateTVS.html';
	$x->ShowTableHeader = 1;
	$x->ShowRecordSlots = 0;
	$x->HighlightColor = '#FFF0C2';

	// mm: build the query based on current member's permissions
	if($perm[2]==1){ // view owner only
		$x->QueryFrom.=', membership_userrecords';
		$x->QueryWhere="where `ex_preguntas_respuestas`.`id_preg_resp`=membership_userrecords.pkValue and membership_userrecords.tableName='ex_preguntas_respuestas' and lcase(membership_userrecords.memberID)='".getLoggedMemberID()."'";
	}elseif($perm[2]==2){ // view group only
		$x->QueryFrom.=', membership_userrecords';
		$x->QueryWhere="where `ex_preguntas_respuestas`.`id_preg_resp`=membership_userrecords.pkValue and membership_userrecords.tableName='ex_preguntas_respuestas' and membership_userrecords.groupID='".getLoggedGroupID()."'";
	}elseif($perm[2]==3){ // view all
		// no further action
	}elseif($perm[2]==0){ // view none
		$x->QueryFields = array("Not enough permissions" => "NEP");
		$x->QueryFrom = '`ex_preguntas_respuestas`';
		$x->QueryWhere = '';
		$x->DefaultSortField = '';
	}

	// handle date sorting correctly
	// end of date sorting handler

	// hook: ex_preguntas_respuestas_init
	$render=TRUE;
	if(function_exists('ex_preguntas_respuestas_init')){
		$args=array();
		$render=ex_preguntas_respuestas_init($x, getMemberInfo(), $args);
	}

	if($render) $x->Render();

	// hook: ex_preguntas_respuestas_header
	$headerCode='';
	if(function_exists('ex_preguntas_respuestas_header')){
		$args=array();
		$headerCode=ex_preguntas_respuestas_header($x->ContentType, getMemberInfo(), $args);
	}  
	if(!$headerCode){
		include("$d/header.php"); 
	}else{
		ob_start(); include("$d/header.php"); $dHeader=ob_get_contents(); ob_end_clean();
		echo str_replace('<%%HEADER%%>', $dHeader, $headerCode);
	}

	echo $x->HTML;
	// hook: ex_preguntas_respuestas_footer
	$footerCode='';
	if(function_exists('ex_preguntas_respuestas_footer')){
		$args=array();
		$footerCode=ex_preguntas_respuestas_footer($x->ContentType, getMemberInfo(), $args);
	}  
	if(!$footerCode){
		include("$d/footer.php"); 
	}else{
		ob_start(); include("$d/footer.php"); $dFooter=ob_get_contents(); ob_end_clean();
		echo str_replace('<%%FOOTER%%>', $dFooter, $footerCode);
	}
?>
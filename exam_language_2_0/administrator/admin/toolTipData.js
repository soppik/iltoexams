var FiltersEnabled = 0; // if your not going to use transitions or filters in any of the tips set this to 0
var spacer="&nbsp; &nbsp; &nbsp; ";

// email notifications to admin
notifyAdminNewMembers0Tip=["", spacer+"No email notifications to admin."];
notifyAdminNewMembers1Tip=["", spacer+"Notify admin only when a new member is waiting for approval."];
notifyAdminNewMembers2Tip=["", spacer+"Notify admin for all new sign-ups."];

// visitorSignup
visitorSignup0Tip=["", spacer+"If this option is selected, visitors will not be able to join this group unless the admin manually moves them to this group from the admin area."];
visitorSignup1Tip=["", spacer+"If this option is selected, visitors can join this group but will not be able to sign in unless the admin approves them from the admin area."];
visitorSignup2Tip=["", spacer+"If this option is selected, visitors can join this group and will be able to sign in instantly with no need for admin approval."];

// ex_respuestas table
ex_respuestas_addTip=["",spacer+"This option allows all members of the group to add records to the 'Answers' table. A member who adds a record to the table becomes the 'owner' of that record."];

ex_respuestas_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'Answers' table."];
ex_respuestas_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'Answers' table."];
ex_respuestas_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'Answers' table."];
ex_respuestas_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'Answers' table."];

ex_respuestas_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'Answers' table."];
ex_respuestas_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'Answers' table."];
ex_respuestas_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'Answers' table."];
ex_respuestas_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'Answers' table, regardless of their owner."];

ex_respuestas_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'Answers' table."];
ex_respuestas_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'Answers' table."];
ex_respuestas_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'Answers' table."];
ex_respuestas_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'Answers' table."];

// ex_empresas table
ex_empresas_addTip=["",spacer+"This option allows all members of the group to add records to the 'Companies' table. A member who adds a record to the table becomes the 'owner' of that record."];

ex_empresas_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'Companies' table."];
ex_empresas_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'Companies' table."];
ex_empresas_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'Companies' table."];
ex_empresas_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'Companies' table."];

ex_empresas_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'Companies' table."];
ex_empresas_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'Companies' table."];
ex_empresas_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'Companies' table."];
ex_empresas_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'Companies' table, regardless of their owner."];

ex_empresas_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'Companies' table."];
ex_empresas_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'Companies' table."];
ex_empresas_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'Companies' table."];
ex_empresas_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'Companies' table."];

// ex_empresas_usuarios table
ex_empresas_usuarios_addTip=["",spacer+"This option allows all members of the group to add records to the 'Companies Users' table. A member who adds a record to the table becomes the 'owner' of that record."];

ex_empresas_usuarios_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'Companies Users' table."];
ex_empresas_usuarios_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'Companies Users' table."];
ex_empresas_usuarios_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'Companies Users' table."];
ex_empresas_usuarios_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'Companies Users' table."];

ex_empresas_usuarios_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'Companies Users' table."];
ex_empresas_usuarios_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'Companies Users' table."];
ex_empresas_usuarios_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'Companies Users' table."];
ex_empresas_usuarios_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'Companies Users' table, regardless of their owner."];

ex_empresas_usuarios_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'Companies Users' table."];
ex_empresas_usuarios_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'Companies Users' table."];
ex_empresas_usuarios_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'Companies Users' table."];
ex_empresas_usuarios_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'Companies Users' table."];

// ex_examenes table
ex_examenes_addTip=["",spacer+"This option allows all members of the group to add records to the 'Exam' table. A member who adds a record to the table becomes the 'owner' of that record."];

ex_examenes_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'Exam' table."];
ex_examenes_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'Exam' table."];
ex_examenes_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'Exam' table."];
ex_examenes_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'Exam' table."];

ex_examenes_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'Exam' table."];
ex_examenes_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'Exam' table."];
ex_examenes_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'Exam' table."];
ex_examenes_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'Exam' table, regardless of their owner."];

ex_examenes_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'Exam' table."];
ex_examenes_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'Exam' table."];
ex_examenes_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'Exam' table."];
ex_examenes_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'Exam' table."];

// ex_examenes_preguntas table
ex_examenes_preguntas_addTip=["",spacer+"This option allows all members of the group to add records to the 'Exams Questions' table. A member who adds a record to the table becomes the 'owner' of that record."];

ex_examenes_preguntas_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'Exams Questions' table."];
ex_examenes_preguntas_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'Exams Questions' table."];
ex_examenes_preguntas_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'Exams Questions' table."];
ex_examenes_preguntas_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'Exams Questions' table."];

ex_examenes_preguntas_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'Exams Questions' table."];
ex_examenes_preguntas_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'Exams Questions' table."];
ex_examenes_preguntas_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'Exams Questions' table."];
ex_examenes_preguntas_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'Exams Questions' table, regardless of their owner."];

ex_examenes_preguntas_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'Exams Questions' table."];
ex_examenes_preguntas_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'Exams Questions' table."];
ex_examenes_preguntas_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'Exams Questions' table."];
ex_examenes_preguntas_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'Exams Questions' table."];

// ex_preguntas table
ex_preguntas_addTip=["",spacer+"This option allows all members of the group to add records to the 'Questions' table. A member who adds a record to the table becomes the 'owner' of that record."];

ex_preguntas_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'Questions' table."];
ex_preguntas_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'Questions' table."];
ex_preguntas_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'Questions' table."];
ex_preguntas_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'Questions' table."];

ex_preguntas_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'Questions' table."];
ex_preguntas_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'Questions' table."];
ex_preguntas_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'Questions' table."];
ex_preguntas_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'Questions' table, regardless of their owner."];

ex_preguntas_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'Questions' table."];
ex_preguntas_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'Questions' table."];
ex_preguntas_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'Questions' table."];
ex_preguntas_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'Questions' table."];

// ex_preguntas_respuestas table
ex_preguntas_respuestas_addTip=["",spacer+"This option allows all members of the group to add records to the 'Questions Answers' table. A member who adds a record to the table becomes the 'owner' of that record."];

ex_preguntas_respuestas_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'Questions Answers' table."];
ex_preguntas_respuestas_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'Questions Answers' table."];
ex_preguntas_respuestas_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'Questions Answers' table."];
ex_preguntas_respuestas_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'Questions Answers' table."];

ex_preguntas_respuestas_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'Questions Answers' table."];
ex_preguntas_respuestas_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'Questions Answers' table."];
ex_preguntas_respuestas_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'Questions Answers' table."];
ex_preguntas_respuestas_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'Questions Answers' table, regardless of their owner."];

ex_preguntas_respuestas_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'Questions Answers' table."];
ex_preguntas_respuestas_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'Questions Answers' table."];
ex_preguntas_respuestas_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'Questions Answers' table."];
ex_preguntas_respuestas_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'Questions Answers' table."];

// ex_resultados table
ex_resultados_addTip=["",spacer+"This option allows all members of the group to add records to the 'Results' table. A member who adds a record to the table becomes the 'owner' of that record."];

ex_resultados_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'Results' table."];
ex_resultados_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'Results' table."];
ex_resultados_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'Results' table."];
ex_resultados_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'Results' table."];

ex_resultados_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'Results' table."];
ex_resultados_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'Results' table."];
ex_resultados_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'Results' table."];
ex_resultados_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'Results' table, regardless of their owner."];

ex_resultados_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'Results' table."];
ex_resultados_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'Results' table."];
ex_resultados_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'Results' table."];
ex_resultados_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'Results' table."];

// ex_usuarios table
ex_usuarios_addTip=["",spacer+"This option allows all members of the group to add records to the 'Users' table. A member who adds a record to the table becomes the 'owner' of that record."];

ex_usuarios_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'Users' table."];
ex_usuarios_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'Users' table."];
ex_usuarios_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'Users' table."];
ex_usuarios_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'Users' table."];

ex_usuarios_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'Users' table."];
ex_usuarios_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'Users' table."];
ex_usuarios_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'Users' table."];
ex_usuarios_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'Users' table, regardless of their owner."];

ex_usuarios_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'Users' table."];
ex_usuarios_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'Users' table."];
ex_usuarios_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'Users' table."];
ex_usuarios_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'Users' table."];

// ex_usuarios_examenes table
ex_usuarios_examenes_addTip=["",spacer+"This option allows all members of the group to add records to the 'Users Exams' table. A member who adds a record to the table becomes the 'owner' of that record."];

ex_usuarios_examenes_view0Tip=["",spacer+"This option prohibits all members of the group from viewing any record in the 'Users Exams' table."];
ex_usuarios_examenes_view1Tip=["",spacer+"This option allows each member of the group to view only his own records in the 'Users Exams' table."];
ex_usuarios_examenes_view2Tip=["",spacer+"This option allows each member of the group to view any record owned by any member of the group in the 'Users Exams' table."];
ex_usuarios_examenes_view3Tip=["",spacer+"This option allows each member of the group to view all records in the 'Users Exams' table."];

ex_usuarios_examenes_edit0Tip=["",spacer+"This option prohibits all members of the group from modifying any record in the 'Users Exams' table."];
ex_usuarios_examenes_edit1Tip=["",spacer+"This option allows each member of the group to edit only his own records in the 'Users Exams' table."];
ex_usuarios_examenes_edit2Tip=["",spacer+"This option allows each member of the group to edit any record owned by any member of the group in the 'Users Exams' table."];
ex_usuarios_examenes_edit3Tip=["",spacer+"This option allows each member of the group to edit any records in the 'Users Exams' table, regardless of their owner."];

ex_usuarios_examenes_delete0Tip=["",spacer+"This option prohibits all members of the group from deleting any record in the 'Users Exams' table."];
ex_usuarios_examenes_delete1Tip=["",spacer+"This option allows each member of the group to delete only his own records in the 'Users Exams' table."];
ex_usuarios_examenes_delete2Tip=["",spacer+"This option allows each member of the group to delete any record owned by any member of the group in the 'Users Exams' table."];
ex_usuarios_examenes_delete3Tip=["",spacer+"This option allows each member of the group to delete any records in the 'Users Exams' table."];

/*
	Style syntax:
	-------------
	[TitleColor,TextColor,TitleBgColor,TextBgColor,TitleBgImag,TextBgImag,TitleTextAlign,
	TextTextAlign,TitleFontFace,TextFontFace, TipPosition, StickyStyle, TitleFontSize,
	TextFontSize, Width, Height, BorderSize, PadTextArea, CoordinateX , CoordinateY,
	TransitionNumber, TransitionDuration, TransparencyLevel ,ShadowType, ShadowColor]

*/

toolTipStyle=["white","#00008B","#000099","#E6E6FA","","images/helpBg.gif","","","","\"Trebuchet MS\", sans-serif","","","","3",400,"",1,2,10,10,51,1,0,"",""];

applyCssFilter();

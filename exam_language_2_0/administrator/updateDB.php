<?php
	// check this file's MD5 to make sure it wasn't called before
	$prevMD5=@implode('', @file(dirname(__FILE__).'/setup.md5'));
	$thisMD5=md5(@implode('', @file("./updateDB.php")));
	if($thisMD5==$prevMD5){
		$setupAlreadyRun=true;
	}else{
		// set up tables
		if(!isset($silent)){
			$silent=true;
		}

		// set up tables
		setupTable('ex_respuestas', "create table if not exists `ex_respuestas` ( `cod_respuesta` INT(10) not null auto_increment , primary key (`cod_respuesta`), `texto_respuesta` TEXT , `url_respuesta` VARCHAR(100) , `url2_respuesta` VARCHAR(100) ) CHARSET ascii", $silent);
		setupTable('ex_empresas', "create table if not exists `ex_empresas` ( `cod_empresa` VARCHAR(10) not null , primary key (`cod_empresa`), `nom_empresa` VARCHAR(100) , `logo` VARCHAR(100) , `email` VARCHAR(100) ) CHARSET ascii", $silent);
		setupTable('ex_empresas_usuarios', "create table if not exists `ex_empresas_usuarios` ( `key_user` INT(10) not null auto_increment , primary key (`key_user`), `cod_empresa` VARCHAR(10) , `cod_usuario` VARCHAR(50) , `clave` VARCHAR(40) ) CHARSET ascii", $silent);
		setupIndexes('ex_empresas_usuarios', array('cod_empresa'));
		setupTable('ex_examenes', "create table if not exists `ex_examenes` ( `cod_examen` INT(10) not null auto_increment , primary key (`cod_examen`), `nom_examen` VARCHAR(100) , `tipo` VARCHAR(1) , `tiempo` TIME , `descripcion` TEXT ) CHARSET ascii", $silent);
		setupTable('ex_examenes_preguntas', "create table if not exists `ex_examenes_preguntas` ( `id_exam_preg` INT not null auto_increment , primary key (`id_exam_preg`), `cod_examen` INT(10) , `cod_pregunta` INT(11) ) CHARSET ascii", $silent);
		setupIndexes('ex_examenes_preguntas', array('cod_examen','cod_pregunta'));
		setupTable('ex_preguntas', "create table if not exists `ex_preguntas` ( `cod_pregunta` INT(11) not null auto_increment , primary key (`cod_pregunta`), `texto_pregunta` TEXT , `url_pregunta` VARCHAR(100) , `url2_pregunta` VARCHAR(100) , `tipo` VARCHAR(1) , `tiempo` TIME , `tipo_p` VARCHAR(1) ) CHARSET ascii", $silent);
		setupTable('ex_preguntas_respuestas', "create table if not exists `ex_preguntas_respuestas` ( `id_preg_resp` INT not null auto_increment , primary key (`id_preg_resp`), `cod_pregunta` INT(11) , `cod_respuesta` INT(10) , `respuesta_correcta` TINYINT(1) ) CHARSET ascii", $silent);
		setupIndexes('ex_preguntas_respuestas', array('cod_pregunta','cod_respuesta'));
		setupTable('ex_resultados', "create table if not exists `ex_resultados` ( `key_user_exam` INT(10) not null , primary key (`key_user_exam`), `cod_pregunta` INT(11) , `cod_respuesta` INT(10) ) CHARSET ascii", $silent);
		setupIndexes('ex_resultados', array('cod_pregunta','cod_respuesta'));
		setupTable('ex_usuarios', "create table if not exists `ex_usuarios` ( `cod_usuario` VARCHAR(50) not null , primary key (`cod_usuario`), `nom_usuario` VARCHAR(100) , `email` VARCHAR(150) ) CHARSET ascii", $silent);
		setupTable('ex_usuarios_examenes', "create table if not exists `ex_usuarios_examenes` ( `key_user_exam` INT(10) not null auto_increment , primary key (`key_user_exam`), `key_user` INT(10) , `cod_examen` INT(10) , `estado` VARCHAR(1) , `fecha_presentacion` DATE , `hora_inicial` TIME , `hora_final` TIME , `puntaje` FLOAT(10,2) ) CHARSET ascii", $silent);
		setupIndexes('ex_usuarios_examenes', array('key_user','cod_examen'));


		// save MD5
		if($fp=@fopen(dirname(__FILE__).'/setup.md5', 'w')){
			fwrite($fp, $thisMD5);
			fclose($fp);
		}
	}


	function setupIndexes($tableName, $arrFields){
		if(!is_array($arrFields)){
			return false;
		}

		foreach($arrFields as $fieldName){
			if(!$res=@mysql_query("SHOW COLUMNS FROM `$tableName` like '$fieldName'")){
				continue;
			}
			if(!$row=@mysql_fetch_assoc($res)){
				continue;
			}
			if($row['Key']==''){
				@mysql_query("ALTER TABLE `$tableName` ADD INDEX `$fieldName` (`$fieldName`)");
			}
		}
	}


	function setupTable($tableName, $createSQL='', $silent=true, $arrAlter=''){
		global $Translation;
		ob_start();

		echo "<div style=\"padding: 5px; border-bottom:solid 1px silver; font-family: verdana, arial; font-size: 10px;\">";

		// is there a table rename query?
		if(is_array($arrAlter)){
			$matches=array();
			if(preg_match("/ALTER TABLE `(.*)` RENAME `$tableName`/", $arrAlter[0], $matches)){
				$oldTableName=$matches[1];
			}
		}

		if($res=@mysql_query("select count(1) from `$tableName`")){ // table already exists
			if($row=@mysql_fetch_array($res)){
				echo str_replace("<TableName>", $tableName, str_replace("<NumRecords>", $row[0],$Translation["table exists"]));
				if(is_array($arrAlter)){
					echo '<br />';
					foreach($arrAlter as $alter){
						if($alter!=''){
							echo "$alter ... ";
							if(!@mysql_query($alter)){
								echo "<font color=red>".$Translation["failed"]."</font><br />";
								echo "<font color=red>".$Translation["mysql said"]." ".mysql_error()."</font><br />";
							}else{
								echo "<font color=green>".$Translation["ok"]."</font><br />";
							}
						}
					}
				}else{
					echo $Translation["table uptodate"];
				}
			}else{
				echo str_replace("<TableName>", $tableName, $Translation["couldnt count"]);
			}
		}else{ // given tableName doesn't exist
		
			if($oldTableName!=''){ // if we have a table rename query
				if($ro=@mysql_query("select count(1) from `$oldTableName`")){ // if old table exists, rename it.
					$renameQuery=array_shift($arrAlter); // get and remove rename query

					echo "$renameQuery ... ";
					if(!@mysql_query($renameQuery)){
						echo "<font color=red>".$Translation["failed"]."</font><br />";
						echo "<font color=red>".$Translation["mysql said"]." ".mysql_error()."</font><br />";
					}else{
						echo "<font color=green>".$Translation["ok"]."</font><br />";
					}

					if(is_array($arrAlter)) setupTable($tableName, $createSQL, false, $arrAlter); // execute Alter queries on renamed table ...
				}else{ // if old tableName doesn't exist (nor the new one since we're here), then just create the table.
					setupTable($tableName, $createSQL, false); // no Alter queries passed ...
				}
			}else{ // tableName doesn't exist and no rename, so just create the table
				echo str_replace("<TableName>", $tableName, $Translation["creating table"]);
				if(!@mysql_query($createSQL)){
					echo "<font color=red>".$Translation["failed"]."</font><br />";
					echo "<font color=red>".$Translation["mysql said"].mysql_error()."</font>";
				}else{
					echo "<font color=green>".$Translation["ok"]."</font>";
				}
			}
		}

		echo "</div>";

		$out=ob_get_contents();
		ob_end_clean();
		if(!$silent){
			echo $out;
		}
	}
?>
<div class="login-container animated fadeInDown">
    <form id="ff" action="form_fraud.php" method="post">       
        <div class="loginbox bg-white">
            <div class="loginbox-social">
                <div class="social-buttons">
                    <a id="home_link" href="https://www.iltoexams.com">
                        <img src="/ilto3/images/iltoexams-logo-100.png" alt="ILTO EXAMS">
                    </a>
                </div>
            </div>
            <div class="loginbox-title">&nbsp;</div>
            <div class="loginbox-title">Certificates validation</div>
            <div class="loginbox-textbox">
                <input name="id_number" type="text" class="form-control" placeholder="Id Number *" required onInvalid="The Id number is required">
            </div>
            <div class="loginbox-textbox">
                <input name="fraud_preco" type="text" class="form-control" placeholder="Fraud Prevention Code *" required ></input>
            </div>
            <div class="loginbox-submit">
                <input class="btn btn-success btn-block" value="GET REPORT" type="submit" name="yt0"><br/><br/>
            </div>
        </div>
    </form>
</div>

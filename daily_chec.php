<?php
// change the following path if necessary
defined('YII_PATH') or define('YII_PATH', (dirname(__FILE__)) . '/yii/framework');

require_once YII_PATH . '/yii.php';

$mainConfig = require dirname(__FILE__) . '/ilto3/protected/config/main.php';

include(dirname(__FILE__) . '/ilto3/protected/models/Clients.php');
require("sendmail.php");  


//Yii::createWebApplication($mainConfig)->run();

$connection=new CDbConnection('mysql:host=localhost;dbname=iltoexam_newversion','iltoexam_newvers','Ilto.2015');
// establish connection. You may try...catch possible exceptions
$connection->active=true;


//Traemos todos los id de los clientes
$SQL = "SELECT id_cliente FROM `clients`";
$clients= $connection->createCommand($SQL)->queryAll();




foreach($clients as $client){
    //nos traemos las licencias de cada cliente que están disponibles con su fecha final
    $SQL = "SELECT id_licencias_cliente, licenses_client.id_licencia, nombre, fecha_final, (cantidad - utilizados) as available FROM `licenses_client` INNER JOIN licenses ON licenses.id_licencia = licenses_client.id_licencia WHERE id_cliente = '".$client["id_cliente"]."' AND licenses_client.estado ='A'";
    $licensesClient= $connection->createCommand($SQL)->queryAll();
    
    $idClient = $client['id_cliente'];
    //comparamos esa fecha final, y guardamos los ids de las licencias en los respectivos arrays
    foreach($licensesClient as $license){
        //Convert to date
        $datefL = $license["fecha_final"];//Your date
        $idL = $license["id_licencias_cliente"];
        $quantity = $license["available"];
        $licenseName = $license["nombre"];
        $date=strtotime($datefL);//Converted to a PHP date (a second count)
        
        //Calculate difference
        $diff=$date-time();//time returns current time in seconds
        $days=intval(floor($diff/(60*60*24)));//seconds/minute*minutes/hour*hours/day)
       // var_dump($idL, $datefL, $days);
       
        // guardamos el id de la licencias del cliente en array 30 days left
       if($days == 29){
            $SQL = "SELECT email FROM users_c WHERE `id_perfil` = '2' AND `id_cliente` LIKE '".$idClient."' ";
            $too= $connection->createCommand($SQL)->queryAll();
            $email = $too[0]["email"];
            
            prepareToSend($idClient,$idL,$licenseName,$datefL,$quantity,$email,$days+1);
        }
        
        // guardamos el id de la licencias del cliente en array 15 days left
        if($days == 14){
            $SQL = "SELECT email FROM users_c WHERE `id_perfil` = '2' AND `id_cliente` LIKE '".$idClient."' ";
            $too= $connection->createCommand($SQL)->queryAll();
            $email = $too[0]["email"];
            
            prepareToSend($idClient,$idL,$licenseName,$datefL,$quantity,$email,$days+1);
        }
        
        // guardamos el id de la licencias del cliente en array 5 days left
        if($days == 4){
            $SQL = "SELECT email FROM users_c WHERE `id_perfil` = '2' AND `id_cliente` LIKE '".$idClient."' ";
            $too= $connection->createCommand($SQL)->queryAll();
            $email = $too[0]["email"];
            prepareToSend($idClient,$idL,$licenseName,$datefL,$quantity,$email,$days+1);
        }
        
        if($days == -1){
            $SQL = "SELECT email FROM users_c WHERE `id_perfil` = '2' AND `id_cliente` LIKE '".$idClient."' ";
            $too= $connection->createCommand($SQL)->queryAll();
            $email = $too[0]["email"];
            prepareToSend($idClient,$idL,$licenseName,$datefL,$quantity,$email,$days+1);
            //vencemos las licencias del cliente
            $SQL = "UPDATE `licenses_client` SET `estado`='E' WHERE id_licencias_cliente = '".$idL."' ";
            $list= $connection->createCommand($SQL)->query();
            
            //vencemos la licencias de los takers
        }
    }
} 

function prepareToSend($idClient,$idL,$licenseName,$datefL,$quantity,$email,$days){
    $datefL = substr($datefL, 0, 10);
    $subject = 'Licenses Expire Alert';
    if($days == 0){
        $htmlBody = '<p style="font:Arial, sans-serif; color: #191919;"><span style="font-size:16px; font-weight: bold;">Dear Agent,</span><br/><br/>
        You have '.$quantity.' TECS '.$licenseName.' licenses that will expire NOW.<br/><br/>
        Best regards,<br/><br/>
        Support Team - ILTO<br/>
        <img src="https://iltoexams.com/logo_ILTO.png" alt="iltoexams"></p>';    
    }else{
       $htmlBody = '<p style="font:Arial, sans-serif; color: #191919;"><span style="font-size:16px; font-weight: bold;">Dear Agent,</span><br/><br/>
        You have '.$quantity.'  '.$licenseName.' licenses that will expire on '.$datefL.'.<br/><br/>
        Best regards,<br/><br/>
        Support Team - ILTO<br/>
        <img src="https://iltoexams.com/logo_ILTO.png" alt="iltoexams"></p>'; 
    }
    
    $dat = "{From: 'noreply@iltoexams.com', To: '$email', Cc: 'iltoexams@outlook.com', Subject: '$subject', HtmlBody: '$htmlBody'}";//array('From' => 'noreply@iltoexams.com', 'To'=>'dokho_02@hotmail.com', 'Subject'=>'Hola Chris Fer', 'HtmlBody'=>'<strong>Hello</strong> dear Postmark user.');
    sendEmail($dat);
    
}

    
?>
<!DOCTYPE html>
<html>
<head>
    <title>Demo TECS | ILTO</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="Content-Language" content="ES" />
    
    <link rel="canonical" href="/" />

    
    


    <!--Skeleton Framework
<link rel="stylesheet" type="text/css" media="screen" href="/includes/libs/js/resources/framework.css" />
-->
<link rel="shortcut icon" href="media/favicon.ico">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!--jQuery-->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" ></script>
<script src="media/all_min.js"></script>


<script type="text/javascript" src="http:/maps.google.com/maps/api/js?sensor=false"></script>
<link href='includes/libs/js/fancybox/jquery.fancybox-1.3.4.css'  type="text/css" rel="stylesheet">
<script src="includes/libs/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>

<script src="includes/libs/js/infobox/infobox.js"></script>

<script src="http:/code.jquery.com/ui/1.10.3/jquery-ui.js"></script>




    <link rel="stylesheet" type="text/css" href="includes/style.css">
<script type="text/javascript" src="includes/include.js"></script>


    
</head>

<!--[if lt IE 7 ]> <body class="ie6 lang-ES" > <![endif]-->
<!--[if IE 7 ]> <body class="ie7 lang-ES" > <![endif]-->
<!--[if IE 8 ]> <body class="ie8 lang-ES" > <![endif]-->
<!--[if IE 9 ]> <body class="ie9 lang-ES" > <![endif]-->
<!--[if !IE]><!--> <body class="lang-ES" > <!--<![endif]-->

<script type="text/javascript">
/* <![CDATA[ */* ]]> */
</script>





<div class="transit-internacional sub-page sub-landing" id="page">
<?php include("includes/header.php"); ?>

  <!-- GALLERY --> <section id="main-gallery" class="drop-shadow"> <div id="mg-container">
                      <div class="side-grid-container">
              <div class="side-grid" id="left"></div>
              <div class="side-grid" id="right"></div>
            </div>
        <div class="flexslider">
           
                      <ul class="slides"><li id="call-id-267">

                

                <div class="caption-container-wrapper">
                  <div class="caption-container">
                    <div class="caption">
                      <h5></h5>
                      <p></p>
                      <a href="" class="read-more"></a>
                    </div>
                  </div>
                </div>
                <img src="img/im_demo2.jpg" alt=""/>
              </li>
              <li id="call-id-267">

                

                <div class="caption-container-wrapper">
                  <div class="caption-container">
                    <div class="caption">
                      <h5></h5>
                      <p></p>
                      <a href="" class="read-more"></a>
                    </div>
                  </div>
                </div>
                <img src="img/im_DEMO.jpg" alt=""/>
              </li>
              
              </ul>
            </div>
        </div> </section> <article> <div class="get-quote-esp">
  <a class="btn-quote-esp" href="testimonials.php"><!--<img src="/media/cotizacion-esp.png" --></a>
</div> </article><!-- CONTENT --> <section id="content">
  <div class="wrapper"><ul id="breadcrumb">
    <li class="home"><a href="../"></a></li><li class="class">Servicio al cliente</li></ul> <article class="narrow large-text">
            <div class="wrapper">

                      <h1>Demo TECS</h1>
             
              <p>&nbsp;</p>
              <p>We have developed an online DEMO for future test takers to be able to know the methodology used on the TECS and be better prepared&nbsp;              for the day of the test. This is also useful for our representatives to solve any question from our candidates. </p>
<p><br>
              </p>
              <p>&nbsp;</p>
          </div>

          </article> 
  <a href="/#" style="display:none;"></a></div>
  </section> 
<?php include("includes/pie.php"); ?>

    	</body>
        </html>
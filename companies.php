<?php
header ('Location: /tecs-agent.php');
exit();
?>
<!DOCTYPE html>
<html>
<head>
    <title>ORGANIZATIONS | ILTO</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="Content-Language" content="ES" />
    
    <link rel="canonical" href="/" />

    
    


    <!--Skeleton Framework
<link rel="stylesheet" type="text/css" media="screen" href="/includes/libs/js/resources/framework.css" />
-->
<link rel="shortcut icon" href="media/favicon.ico">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!--jQuery-->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" ></script>
<script src="media/all_min.js"></script>


<script type="text/javascript" src="http:/maps.google.com/maps/api/js?sensor=false"></script>
<link href='includes/libs/js/fancybox/jquery.fancybox-1.3.4.css'  type="text/css" rel="stylesheet">
<script src="includes/libs/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>

<script src="includes/libs/js/infobox/infobox.js"></script>

<script src="http:/code.jquery.com/ui/1.10.3/jquery-ui.js"></script>


<link rel="stylesheet" type="text/css" href="easyui.css">
	<link rel="stylesheet" type="text/css" href="icon.css">
	<script type="text/javascript" src="jquery.easyui.min.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#ff').form({
				success:function(data){
					$.messager.alert('Info', data, 'info');
				}
			});
			$('#f2').form({
				success:function(data){
					$.messager.alert('Info', data, 'info');
				}
			});
		});
	</script>
    <link rel="stylesheet" type="text/css" href="includes/style.css">
<script type="text/javascript" src="includes/include.js"></script>



    
</head>

<!--[if lt IE 7 ]> <body class="ie6 lang-ES" > <![endif]-->
<!--[if IE 7 ]> <body class="ie7 lang-ES" > <![endif]-->
<!--[if IE 8 ]> <body class="ie8 lang-ES" > <![endif]-->
<!--[if IE 9 ]> <body class="ie9 lang-ES" > <![endif]-->
<!--[if !IE]><!--> <body class="lang-ES" > <!--<![endif]-->

<script type="text/javascript">
/* <![CDATA[ */* ]]> */
</script>





<div class="transit-internacional sub-page sub-landing" id="page">
<?php include("includes/header.php"); ?>

  <!-- GALLERY --> <section id="main-gallery" class="drop-shadow"> <div id="mg-container">
                      <div class="side-grid-container">
              <div class="side-grid" id="left"></div>
              <div class="side-grid" id="right"></div>
            </div>
        <div class="flexslider">
           
                      <ul class="slides"><li id="call-id-267">

                

                <div class="caption-container-wrapper">
                  <div class="caption-container">
                    <div class="caption">
                      <h5></h5>
                      <p></p>
                      <a href="" class="read-more"></a>
                    </div>
                  </div>
                </div>
                <img src="img/im_compani1.jpg" alt=""/>
              </li><li id="call-id-456">

                

                <div class="caption-container-wrapper">
                  <div class="caption-container">
                    <div class="caption">
                      <h5></h5>
                      <p></p>
                      <a href="" class="read-more"></a>
                    </div>
                  </div>
                </div>
                <img src="img/im_compani2.jpg" alt=""/>
              </li></ul>
            </div>
        </div> </section> <article>  </article><!-- CONTENT --> <section id="content">
  <div class="wrapper"><ul id="breadcrumb">
    <li class="home"><a href="../"></a></li><li class="class">ORGANIZATIONS</li></ul> <article class="narrow large-text">
            <div class="wrapper">

                      <h1>ORGANIZATIONS</h1>
             
              <p><br>
              Being able to communicate in English is of paramount importance to really achieve the goals of the organization, since making business with foreign companies, no matter what the country is, demands high proficiency in English in employees and entrepreneurs. It is not new to say that English is the common language in Business. </p>
              <p>&nbsp;</p>
              <p>Today most of the organizations have international goals that demand proficient employees that have efficient and successful business conversations in English. </p>
              <p>&nbsp;</p>
              <p>                <strong>ILTO EXAMS</strong> representatives around the world become an essential partner for organizations to make a reliable selection of personnel by using the TECS or other exams.</p>
              <p>&nbsp;</p>
              <p>                Companies today need a suitable tool that allows them to hire qualified employees to establish international relations or to communicate with international customers or suppliers.</p>
              <p>&nbsp;</p>
              <p><strong>                ILTO EXAMS</strong> may help your company in the recruiting process, by optimizing resources using a test that meets the highest standards associated to quality, accuracy, security and reliability. <br>
              Avoid any risk of failure. Make sure your employees are totally proficient and able to achieve the company goals by promoting a high profile in your personnel. </p>
              <p>&nbsp;</p>
              <p>                For more information on how to assess your company’s English proficiency goals by using the TECS, please contact our international voice mail. An advisor will contact you as soon as possible. </p>
              <p>&nbsp;</p>
              <p>              You may also leave your contact information: </p>
              <p>&nbsp;</p>
              <p><strong>Organization Information:</strong></p>
			
              <p><form id="ff" action="form1_proc.php" method="post">
			<table width="597">
				<tr>
				  <td><input type="hidden" name="typeform" value="Companies">
                                      <input name="Organizationname" type="text" placeholder="Organization name" required></td>
					<td>&nbsp;</td> 
					<td><input name="EducationLevel" type="text" placeholder="Education Level" required></input></td>
				</tr>
				<tr>
				  <td><input name="Website" type="text" placeholder="Website" required></td>
					<td>&nbsp;</td>
					<td><input name="Phonenumber" type="text" placeholder="Phone number"   required></input></td>
				</tr>
				<tr>
                  <td><input name="City" type="text" placeholder="City"  required></td>
                  <td>&nbsp;</td>
                  <td><input name="Country" type="text" placeholder="Country"   required></td>
			  </tr>
			
				<tr>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
			  </tr>
				<tr>
				  <td> <p><strong>Contact Information:</strong><br>
              </p></td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
			  </tr>
				<tr>
				  <td><input name="FirstName" type="text" placeholder="First Name"  required></td>
					<td>&nbsp;</td> 
					<td><input name="LastName" type="text" placeholder="Last Name"  required></input></td>
				</tr>
				<tr>
				  <td><input name="PhoneNumber" type="text" placeholder="Phone Number"  required></td>
					<td>&nbsp;</td>
					<td><input name="Email" type="text" placeholder="Email"   required></input></td>
				</tr>
				<tr>
                  <td>How did you know about us?</td>
                  <td>&nbsp;</td>
                  <td><select name="howdidyouknow"  required>
                    <option value="Facebook">Facebook</option>
                    <option value="Google search">Google search</option>
                    <option value="Magazine">Magazine</option>
                    <option value="TV add">TV add</option>
                    <option value="Newspaper">Newspaper</option>
                    <option value="Email">Email</option>
                    <option value="Twitter">Twitter</option>
                    <option value="Other Website">Other Website</option>
                    <option value="Education Events">Education Events</option>
                  </select></td>
			  </tr>
				<tr>
				  <td>              I would like to receive more information on ILTO or TECS products or new releases. </td>
				  <td>&nbsp;</td>
				  <td><p>
				    <label>
				    <input type="radio" name="Wouldlike" value="Yes">
  Yes</label>
				    <label>
				    <input type="radio" name="Wouldlike" value="No">
  No</label>
				    <br>
			      </p></td>
			  </tr>
				<tr>
				  <td colspan="3">&nbsp;</td>
			  </tr>
				<tr>
				  <td colspan="3"><input type="submit" value="Submit"></input></td>
				</tr>
			</table>
		</form>
<p>&nbsp;</p>
<p><br>
</p>
              <p>&nbsp;</p>
          </div>

          </article> 
  <a href="/#" style="display:none;"></a></div>
  </section> 
<?php include("includes/pie.php"); ?>

    	</body>
        </html>
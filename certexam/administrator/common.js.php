<?php
	@header("Content-Type: text/javascript; charset=us-ascii");
	$d=dirname(__FILE__);
	include("$d/defaultLang.php");
	include("$d/language.php");
?>
function showDescription(tableName, fieldName, e){
	var desc = {};
	desc["ex_respuestas"] = {};
	desc["ex_respuestas"]["url_respuesta"] = "Maximum file size allowed: 1000 KB.<br />Allowed file types: jpg, jpeg, gif, png";
	desc["ex_respuestas"]["url2_respuesta"] = "Maximum file size allowed: 1000 KB.<br />Allowed file types: jpg, jpeg, gif, png";
	desc["ex_empresas"] = {};
	desc["ex_empresas"]["logo"] = "Maximum file size allowed: 1000 KB.<br />Allowed file types: jpg, jpeg, gif, png";
	desc["ex_empresas_usuarios"] = {};
	desc["ex_empresas_usuarios"]["key_user"] = "ID User Company";
	desc["ex_examenes"] = {};
	desc["ex_examenes_preguntas"] = {};
	desc["ex_examenes_preguntas"]["id_exam_preg"] = "Id Exams - Questions";
	desc["ex_preguntas"] = {};
	desc["ex_preguntas"]["cod_pregunta"] = "Id Questions";
	desc["ex_preguntas"]["url_pregunta"] = "Maximum file size allowed: 2200 KB.<br />Allowed file types: jpg, jpeg, gif, png";
	desc["ex_preguntas"]["url2_pregunta"] = "Maximum file size allowed: 6000 KB.<br />Allowed file types: jpg, jpeg, gif, png";
	desc["ex_preguntas"]["tipo_p"] = "M=M�ltiple Selection, D=Drop-Down";
	desc["ex_preguntas_respuestas"] = {};
	desc["ex_preguntas_respuestas"]["id_preg_resp"] = "ID Questions Answers";
	desc["ex_preguntas_respuestas"]["respuesta_correcta"] = "1 True, 0 False";
	desc["ex_resultados"] = {};
	desc["ex_usuarios"] = {};
	desc["ex_usuarios_examenes"] = {};
	desc["ex_usuarios_examenes"]["key_user_exam"] = "ID Users - Exams";
	desc["ex_usuarios_examenes"]["key_user"] = "ID User Company";

	if(desc[tableName][fieldName] == undefined) return false;

	var x=0;
	var y=0;
	if(e.pageX){
		x=e.pageX-10;
		y=e.pageY-10;
	}else if(e.clientX){
		x=e.clientX-10;
		y=e.clientY-10;
		if(document.body && ( document.body.scrollLeft || document.body.scrollTop )){
			x+=document.body.scrollLeft;
			y+=document.body.scrollTop;
		}else if(document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop )){
			x+=document.documentElement.scrollLeft;
			y+=document.documentElement.scrollTop;
		}
	}else{
		return false;
	}

	$('fieldDescription').innerHTML=desc[tableName][fieldName];
	$('fieldDescription').style.left=x+'px';
	$('fieldDescription').style.top=y+'px';
	$('fieldDescription').style.visibility='visible';

	return false;
}
function makeResizeable(id){ // sets the textarea with given id as resizeable
	var minHeight=50;
	$(id).insert({ after: '<div id="resize'+id+'" title="Drag the bar up or down to resize the text box." style="width:50px; height:5px; margin: -2px 0 2px; padding: 0; cursor:n-resize; border:1px solid #000;" class="TableHeader"></div>' });
	$('resize'+id).setStyle({ width: ($(id).getWidth()-2)+'px' });
	$(id).setStyle({ resize: 'none' });
	new Draggable('resize'+id, {
		constraint: 'vertical', scroll: window, revert: true,
		change: function(d){
			var poRA=$('resize'+id).positionedOffset();
			var poA=$(id).positionedOffset();
			var Ah=poRA[1]-poA[1];
			
			if(Ah<minHeight){ // enforce min height
				$(id).setStyle({ height: minHeight+'px' });
			}else{
				$(id).setStyle({ height: Ah+'px' });
			}
		}
	});
}
function ex_respuestas_validateData(){
	return true;
}
function ex_empresas_validateData(){
	if($('cod_empresa').value==''){ alert('<?php echo addslashes($Translation['error:']); ?> ID: <?php echo addslashes($Translation['field not null']); ?>'); $('cod_empresa').focus(); return false; };
	return true;
}
function ex_empresas_usuarios_validateData(){
	return true;
}
function ex_examenes_validateData(){
	return true;
}
function ex_examenes_preguntas_validateData(){
	return true;
}
function ex_preguntas_validateData(){
	return true;
}
function ex_preguntas_respuestas_validateData(){
	return true;
}
function ex_resultados_validateData(){
	if($('key_user_exam').value==''){ alert('<?php echo addslashes($Translation['error:']); ?> Key Results: <?php echo addslashes($Translation['field not null']); ?>'); $('key_user_exam').focus(); return false; };
	return true;
}
function ex_usuarios_validateData(){
	if($('cod_usuario').value==''){ alert('<?php echo addslashes($Translation['error:']); ?> ID User: <?php echo addslashes($Translation['field not null']); ?>'); $('cod_usuario').focus(); return false; };
	return true;
}
function ex_usuarios_examenes_validateData(){
	return true;
}
function post(url, params, update, disable, loading){
	new Ajax.Request(
		url, {
			method: 'post',
			parameters: params,
			onCreate: function() {
				if($(disable) != undefined) $(disable).disabled=true;
				if($(loading) != undefined && update != loading) $(loading).innerHTML='<div style="direction: ltr;"><img src="loading.gif"> <?php echo $Translation['Loading ...']; ?></div>';
			},
			onSuccess: function(resp) {
				if($(update) != undefined) $(update).innerHTML=resp.responseText;
			},
			onComplete: function() {
				if($(disable) != undefined) $(disable).disabled=false;
				if($(loading) != undefined && loading != update) $(loading).innerHTML='';
			}
		}
	);
}

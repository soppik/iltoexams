<?php

// Data functions for table ex_respuestas

// This script and data application were generated by AppGini 4.70
// Download AppGini for free from http://bigprof.com/appgini/download/

function ex_respuestas_insert(){
	global $Translation;

	if($_GET['insert_x']!=''){$_POST=$_GET;}

	// mm: can member insert record?
	$arrPerm=getTablePermissions('ex_respuestas');
	if(!$arrPerm[1]){
		return 0;
	}

	$data['texto_respuesta'] = makeSafe($_POST['texto_respuesta']);
	$data['url_respuesta'] = PrepareUploadedFile('url_respuesta', 1024000,'jpg|jpeg|gif|png', false, '');
	if($data['url_respuesta']) createThumbnail($data['url_respuesta'], getThumbnailSpecs('ex_respuestas', 'url_respuesta', 'tv'));
	if($data['url_respuesta']) createThumbnail($data['url_respuesta'], getThumbnailSpecs('ex_respuestas', 'url_respuesta', 'dv'));
	$data['url2_respuesta'] = PrepareUploadedFile('url2_respuesta', 1024000,'jpg|jpeg|gif|png', false, '');
	if($data['url2_respuesta']) createThumbnail($data['url2_respuesta'], getThumbnailSpecs('ex_respuestas', 'url2_respuesta', 'tv'));
	if($data['url2_respuesta']) createThumbnail($data['url2_respuesta'], getThumbnailSpecs('ex_respuestas', 'url2_respuesta', 'dv'));

	// hook: ex_respuestas_before_insert
	if(function_exists('ex_respuestas_before_insert')){
		$args=array();
		if(!ex_respuestas_before_insert($data, getMemberInfo(), $args)){ return FALSE; }
	}

	$o=array('silentErrors' => true);
	sql('insert into `ex_respuestas` set `texto_respuesta`=' . (($data['texto_respuesta'] != '') ? "'{$data['texto_respuesta']}'" : 'NULL') . ', ' . ($data['url_respuesta']!='' ? "`url_respuesta`='{$data['url_respuesta']}'" : ($_POST['url_respuesta_remove'] != 1 ? '`url_respuesta`=`url_respuesta`' : '`url_respuesta`=NULL')) . ', ' . ($data['url2_respuesta']!='' ? "`url2_respuesta`='{$data['url2_respuesta']}'" : ($_POST['url2_respuesta_remove'] != 1 ? '`url2_respuesta`=`url2_respuesta`' : '`url2_respuesta`=NULL')), $o);
	if($o['error']!=''){
		echo $o['error'];
		echo "<a href=\"ex_respuestas_view.php?addNew_x=1\">{$Translation['< back']}</a>";
		exit;
	}
	
	$recID=mysql_insert_id();

	// hook: ex_respuestas_after_insert
	if(function_exists('ex_respuestas_after_insert')){
		$data['selectedID']=$recID;
		$args=array();
		if(!ex_respuestas_after_insert($data, getMemberInfo(), $args)){ return (get_magic_quotes_gpc() ? stripslashes($recID) : $recID); }
	}

	// mm: save ownership data
	sql("insert into membership_userrecords set tableName='ex_respuestas', pkValue='$recID', memberID='".getLoggedMemberID()."', dateAdded='".time()."', dateUpdated='".time()."', groupID='".getLoggedGroupID()."'", $eo);

	return (get_magic_quotes_gpc() ? stripslashes($recID) : $recID);
}

function ex_respuestas_delete($selected_id, $AllowDeleteOfParents=false, $skipChecks=false){
	// insure referential integrity ...
	global $Translation;
	$selected_id=makeSafe($selected_id);

	// mm: can member delete record?
	$arrPerm=getTablePermissions('ex_respuestas');
	$ownerGroupID=sqlValue("select groupID from membership_userrecords where tableName='ex_respuestas' and pkValue='$selected_id'");
	$ownerMemberID=sqlValue("select lcase(memberID) from membership_userrecords where tableName='ex_respuestas' and pkValue='$selected_id'");
	if(($arrPerm[4]==1 && $ownerMemberID==getLoggedMemberID()) || ($arrPerm[4]==2 && $ownerGroupID==getLoggedGroupID()) || $arrPerm[4]==3){ // allow delete?
		// delete allowed, so continue ...
	}else{
		return FALSE;
	}

	// hook: ex_respuestas_before_delete
	if(function_exists('ex_respuestas_before_delete')){
		$args=array();
		if(!ex_respuestas_before_delete($selected_id, $skipChecks, getMemberInfo(), $args)){ return FALSE; }
	}

	// child table: ex_preguntas_respuestas
	$res = sql("select `cod_respuesta` from `ex_respuestas` where `cod_respuesta`='$selected_id'", $eo);
	$cod_respuesta = mysql_fetch_row($res);
	$rires = sql("select count(1) from `ex_preguntas_respuestas` where `cod_respuesta`='".addslashes($cod_respuesta[0])."'", $eo);
	$rirow = mysql_fetch_row($rires);
	if($rirow[0] && !$AllowDeleteOfParents && !$skipChecks){
		$RetMsg = $Translation["couldn't delete"];
		$RetMsg = str_replace("<RelatedRecords>", $rirow[0], $RetMsg);
		$RetMsg = str_replace("<TableName>", "ex_preguntas_respuestas", $RetMsg);
		return $RetMsg;
	}elseif($rirow[0] && $AllowDeleteOfParents && !$skipChecks){
		$RetMsg = $Translation["confirm delete"];
		$RetMsg = str_replace("<RelatedRecords>", $rirow[0], $RetMsg);
		$RetMsg = str_replace("<TableName>", "ex_preguntas_respuestas", $RetMsg);
		$RetMsg = str_replace("<Delete>", "<input type=button class=button value=\"".$Translation['yes']."\" onClick=\"window.location='ex_respuestas_view.php?SelectedID=".urlencode($selected_id)."&delete_x=1&confirmed=1';\">", $RetMsg);
		$RetMsg = str_replace("<Cancel>", "<input type=button class=button value=\"".$Translation['no']."\" onClick=\"window.location='ex_respuestas_view.php?SelectedID=".urlencode($selected_id)."';\">", $RetMsg);
		return $RetMsg;
	}

	// child table: ex_resultados
	$res = sql("select `cod_respuesta` from `ex_respuestas` where `cod_respuesta`='$selected_id'", $eo);
	$cod_respuesta = mysql_fetch_row($res);
	$rires = sql("select count(1) from `ex_resultados` where `cod_respuesta`='".addslashes($cod_respuesta[0])."'", $eo);
	$rirow = mysql_fetch_row($rires);
	if($rirow[0] && !$AllowDeleteOfParents && !$skipChecks){
		$RetMsg = $Translation["couldn't delete"];
		$RetMsg = str_replace("<RelatedRecords>", $rirow[0], $RetMsg);
		$RetMsg = str_replace("<TableName>", "ex_resultados", $RetMsg);
		return $RetMsg;
	}elseif($rirow[0] && $AllowDeleteOfParents && !$skipChecks){
		$RetMsg = $Translation["confirm delete"];
		$RetMsg = str_replace("<RelatedRecords>", $rirow[0], $RetMsg);
		$RetMsg = str_replace("<TableName>", "ex_resultados", $RetMsg);
		$RetMsg = str_replace("<Delete>", "<input type=button class=button value=\"".$Translation['yes']."\" onClick=\"window.location='ex_respuestas_view.php?SelectedID=".urlencode($selected_id)."&delete_x=1&confirmed=1';\">", $RetMsg);
		$RetMsg = str_replace("<Cancel>", "<input type=button class=button value=\"".$Translation['no']."\" onClick=\"window.location='ex_respuestas_view.php?SelectedID=".urlencode($selected_id)."';\">", $RetMsg);
		return $RetMsg;
	}

	sql("delete from `ex_respuestas` where `cod_respuesta`='$selected_id'", $eo);

	// hook: ex_respuestas_after_delete
	if(function_exists('ex_respuestas_after_delete')){
		$args=array();
		ex_respuestas_after_delete($selected_id, getMemberInfo(), $args);
	}

	// mm: delete ownership data
	sql("delete from membership_userrecords where tableName='ex_respuestas' and pkValue='$selected_id'", $eo);
}

function ex_respuestas_update($selected_id){
	global $Translation;

	if($_GET['update_x']!=''){$_POST=$_GET;}

	// mm: can member edit record?
	$arrPerm=getTablePermissions('ex_respuestas');
	$ownerGroupID=sqlValue("select groupID from membership_userrecords where tableName='ex_respuestas' and pkValue='".makeSafe($selected_id)."'");
	$ownerMemberID=sqlValue("select lcase(memberID) from membership_userrecords where tableName='ex_respuestas' and pkValue='".makeSafe($selected_id)."'");
	if(($arrPerm[3]==1 && $ownerMemberID==getLoggedMemberID()) || ($arrPerm[3]==2 && $ownerGroupID==getLoggedGroupID()) || $arrPerm[3]==3){ // allow update?
		// update allowed, so continue ...
	}else{
		return;
	}

	$data['texto_respuesta'] = makeSafe($_POST['texto_respuesta']);
	$data['selectedID']=makeSafe($selected_id);
	if($_POST['url_respuesta_remove'] == 1){
		$data['url_respuesta'] = '';
	}else{
		$data['url_respuesta'] = PrepareUploadedFile('url_respuesta', 1024000, 'jpg|jpeg|gif|png', false, "");
		if($data['url_respuesta']) createThumbnail($data['url_respuesta'], getThumbnailSpecs('ex_respuestas', 'url_respuesta', 'tv'));
		if($data['url_respuesta']) createThumbnail($data['url_respuesta'], getThumbnailSpecs('ex_respuestas', 'url_respuesta', 'dv'));
	}
	if($_POST['url2_respuesta_remove'] == 1){
		$data['url2_respuesta'] = '';
	}else{
		$data['url2_respuesta'] = PrepareUploadedFile('url2_respuesta', 1024000, 'jpg|jpeg|gif|png', false, "");
		if($data['url2_respuesta']) createThumbnail($data['url2_respuesta'], getThumbnailSpecs('ex_respuestas', 'url2_respuesta', 'tv'));
		if($data['url2_respuesta']) createThumbnail($data['url2_respuesta'], getThumbnailSpecs('ex_respuestas', 'url2_respuesta', 'dv'));
	}

	// hook: ex_respuestas_before_update
	if(function_exists('ex_respuestas_before_update')){
		$args=array();
		if(!ex_respuestas_before_update($data, getMemberInfo(), $args)){ return FALSE; }
	}

	$o=array('silentErrors' => true);
	sql('update `ex_respuestas` set `texto_respuesta`=' . (($data['texto_respuesta'] != '') ? "'{$data['texto_respuesta']}'" : 'NULL') . ', ' . ($data['url_respuesta']!='' ? "`url_respuesta`='{$data['url_respuesta']}'" : ($_POST['url_respuesta_remove'] != 1 ? '`url_respuesta`=`url_respuesta`' : '`url_respuesta`=NULL')) . ', ' . ($data['url2_respuesta']!='' ? "`url2_respuesta`='{$data['url2_respuesta']}'" : ($_POST['url2_respuesta_remove'] != 1 ? '`url2_respuesta`=`url2_respuesta`' : '`url2_respuesta`=NULL')) . " where `cod_respuesta`='".makeSafe($selected_id)."'", $o);
	if($o['error']!=''){
		echo $o['error'];
		echo '<a href="ex_respuestas_view.php?SelectedID='.urlencode($selected_id)."\">{$Translation['< back']}</a>";
		exit;
	}


	// hook: ex_respuestas_after_update
	if(function_exists('ex_respuestas_after_update')){
		$args=array();
		if(!ex_respuestas_after_update($data, getMemberInfo(), $args)){ return FALSE; }
	}

	// mm: update ownership data
	sql("update membership_userrecords set dateUpdated='".time()."' where tableName='ex_respuestas' and pkValue='".makeSafe($selected_id)."'", $eo);

}

function ex_respuestas_form($selected_id = '', $AllowUpdate = 1, $AllowInsert = 1, $AllowDelete = 1, $ShowCancel = 0){
	// function to return an editable form for a table records
	// and fill it with data of record whose ID is $selected_id. If $selected_id
	// is empty, an empty form is shown, with only an 'Add New'
	// button displayed.

	global $Translation;

	// mm: get table permissions
	$arrPerm=getTablePermissions('ex_respuestas');
	if(!$arrPerm[1] && $selected_id==''){ return ''; }

	if($selected_id){
		// mm: check member permissions
		if(!$arrPerm[2]){
			return "";
		}
		// mm: who is the owner?
		$ownerGroupID=sqlValue("select groupID from membership_userrecords where tableName='ex_respuestas' and pkValue='".makeSafe($selected_id)."'");
		$ownerMemberID=sqlValue("select lcase(memberID) from membership_userrecords where tableName='ex_respuestas' and pkValue='".makeSafe($selected_id)."'");
		if($arrPerm[2]==1 && getLoggedMemberID()!=$ownerMemberID){
			return "";
		}
		if($arrPerm[2]==2 && getLoggedGroupID()!=$ownerGroupID){
			return "";
		}

		// can edit?
		if(($arrPerm[3]==1 && $ownerMemberID==getLoggedMemberID()) || ($arrPerm[3]==2 && $ownerGroupID==getLoggedGroupID()) || $arrPerm[3]==3){
			$AllowUpdate=1;
		}else{
			$AllowUpdate=0;
		}

		$res = sql("select * from `ex_respuestas` where `cod_respuesta`='".makeSafe($selected_id)."'", $eo);
		$row = mysql_fetch_array($res);
		$urow = $row; /* unsanitized data */
		$hc = new CI_Input();
		$row = $hc->xss_clean($row); /* sanitize data */
	}else{
	}

	// code for template based detail view forms

	// open the detail view template
	if(($_POST['dvprint_x']!='' || $_GET['dvprint_x']!='') && $selected_id){
		$templateCode=@implode('', @file('./templates/ex_respuestas_templateDVP.html'));
		$dvprint=true;
	}else{
		$templateCode=@implode('', @file('./templates/ex_respuestas_templateDV.html'));
		$dvprint=false;
	}

	// process form title
	$templateCode=str_replace('<%%DETAIL_VIEW_TITLE%%>', 'Detail View', $templateCode);
	// unique random identifier
	$rnd1=($dvprint ? rand(1000000, 9999999) : '');
	$templateCode=str_replace('<%%RND1%%>', $rnd1, $templateCode);
	// process buttons
	if($arrPerm[1]){ // allow insert?
		if(!$selected_id) $templateCode=str_replace('<%%INSERT_BUTTON%%>', '<button type="submit" class="positive" id="insert" name="insert_x" value="1" onclick="return ex_respuestas_validateData();"><img src="addNew.gif" /> ' . $Translation['Save New'] . '</button>', $templateCode);
		$templateCode=str_replace('<%%INSERT_BUTTON%%>', '<button type="submit" class="positive" id="insert" name="insert_x" value="1" onclick="return ex_respuestas_validateData();"><img src="addNew.gif" /> ' . $Translation['Save As Copy'] . '</button>', $templateCode);
	}else{
		$templateCode=str_replace('<%%INSERT_BUTTON%%>', '', $templateCode);
	}
	if($selected_id){
		$templateCode=str_replace('<%%DVPRINT_BUTTON%%>', '<button type="submit" id="dvprint" name="dvprint_x" value="1" onclick="document.myform.reset(); return true;"><img src="print.gif" /> ' . $Translation['Print Preview'] . '</button>', $templateCode);
		if($AllowUpdate){
			$templateCode=str_replace('<%%UPDATE_BUTTON%%>', '<button type="submit" class="positive" id="update" name="update_x" value="1" onclick="return ex_respuestas_validateData();"><img src="update.gif" /> ' . $Translation['Save Changes'] . '</button>', $templateCode);
		}else{
			$templateCode=str_replace('<%%UPDATE_BUTTON%%>', '', $templateCode);

			// set records to read only if user can't insert new records
			if(!$arrPerm[1]){
				$jsReadOnly.="\n\n\tif(document.getElementsByName('cod_respuesta').length){ document.getElementsByName('cod_respuesta')[0].readOnly=true; }\n";
				$jsReadOnly.="\n\n\tif(document.getElementsByName('texto_respuesta').length){ document.getElementsByName('texto_respuesta')[0].readOnly=true; }\n";
				$jsReadOnly.="\n\n\tif(document.getElementsByName('url_respuesta').length){ document.getElementsByName('url_respuesta')[0].readOnly=true; }\n";
				$jsReadOnly.="\n\n\tif(document.getElementsByName('url2_respuesta').length){ document.getElementsByName('url2_respuesta')[0].readOnly=true; }\n";

				$noUploads=true;
			}
		}
		if(($arrPerm[4]==1 && $ownerMemberID==getLoggedMemberID()) || ($arrPerm[4]==2 && $ownerGroupID==getLoggedGroupID()) || $arrPerm[4]==3){ // allow delete?
			$templateCode=str_replace('<%%DELETE_BUTTON%%>', '<button type="submit" class="negative" id="delete" name="delete_x" value="1" onclick="return confirm(\'' . $Translation['are you sure?'] . '\');"><img src="delete.gif" /> ' . $Translation['Delete'] . '</button>', $templateCode);
		}else{
			$templateCode=str_replace('<%%DELETE_BUTTON%%>', '', $templateCode);
		}
		$templateCode=str_replace('<%%DESELECT_BUTTON%%>', '<button type="submit" id="deselect" name="deselect_x" value="1" onclick="document.myform.reset(); return true;"><img src="deselect.gif" /> ' . $Translation['Deselect'] . '</button>', $templateCode);
	}else{
		$templateCode=str_replace('<%%UPDATE_BUTTON%%>', '', $templateCode);
		$templateCode=str_replace('<%%DELETE_BUTTON%%>', '', $templateCode);
		$templateCode=str_replace('<%%DESELECT_BUTTON%%>', ($ShowCancel ? '<button type="submit" id="deselect" name="deselect_x" value="1" onclick="document.myform.reset(); return true;"><img src="deselect.gif" /> ' . $Translation['Cancel'] . '</button>' : ''), $templateCode);
	}

	// process combos

	// process foreign key links
	if($selected_id){
	}

	// process images
	$templateCode=str_replace('<%%UPLOADFILE(cod_respuesta)%%>', '', $templateCode);
	$templateCode=str_replace('<%%UPLOADFILE(texto_respuesta)%%>', '', $templateCode);
	$templateCode=str_replace('<%%UPLOADFILE(url_respuesta)%%>', ($noUploads ? '' : '<br /><input type=hidden name=MAX_FILE_SIZE value=1024000>'.$Translation['upload image'].' <input type=file name="url_respuesta" class=TextBox>'), $templateCode);
	if($AllowUpdate && $row['url_respuesta']!=''){
		$templateCode=str_replace('<%%REMOVEFILE(url_respuesta)%%>', '<br /><input type="checkbox" name="url_respuesta_remove" id="url_respuesta_remove" value="1"> <label for="url_respuesta_remove" style="color: red; font-weight: bold;">'.$Translation['remove image'].'</label>', $templateCode);
	}else{
		$templateCode=str_replace('<%%REMOVEFILE(url_respuesta)%%>', '', $templateCode);
	}
	$templateCode=str_replace('<%%UPLOADFILE(url2_respuesta)%%>', ($noUploads ? '' : '<br /><input type=hidden name=MAX_FILE_SIZE value=1024000>'.$Translation['upload image'].' <input type=file name="url2_respuesta" class=TextBox>'), $templateCode);
	if($AllowUpdate && $row['url2_respuesta']!=''){
		$templateCode=str_replace('<%%REMOVEFILE(url2_respuesta)%%>', '<br /><input type="checkbox" name="url2_respuesta_remove" id="url2_respuesta_remove" value="1"> <label for="url2_respuesta_remove" style="color: red; font-weight: bold;">'.$Translation['remove image'].'</label>', $templateCode);
	}else{
		$templateCode=str_replace('<%%REMOVEFILE(url2_respuesta)%%>', '', $templateCode);
	}

	// process values
	if($selected_id){
		$templateCode=str_replace('<%%VALUE(cod_respuesta)%%>', htmlspecialchars($row['cod_respuesta'], ENT_QUOTES), $templateCode);
		$templateCode=str_replace('<%%URLVALUE(cod_respuesta)%%>', urlencode($urow['cod_respuesta']), $templateCode);
		$templateCode=str_replace('<%%VALUE(texto_respuesta)%%>', htmlspecialchars($row['texto_respuesta'], ENT_QUOTES), $templateCode);
		$templateCode=str_replace('<%%URLVALUE(texto_respuesta)%%>', urlencode($urow['texto_respuesta']), $templateCode);
		$row['url_respuesta']=($row['url_respuesta']!=''?$row['url_respuesta']:'blank.gif');
		$templateCode=str_replace('<%%VALUE(url_respuesta)%%>', htmlspecialchars($row['url_respuesta'], ENT_QUOTES), $templateCode);
		$templateCode=str_replace('<%%URLVALUE(url_respuesta)%%>', urlencode($urow['url_respuesta']), $templateCode);
		$row['url2_respuesta']=($row['url2_respuesta']!=''?$row['url2_respuesta']:'blank.gif');
		$templateCode=str_replace('<%%VALUE(url2_respuesta)%%>', htmlspecialchars($row['url2_respuesta'], ENT_QUOTES), $templateCode);
		$templateCode=str_replace('<%%URLVALUE(url2_respuesta)%%>', urlencode($urow['url2_respuesta']), $templateCode);
	}else{
		$templateCode=str_replace('<%%VALUE(cod_respuesta)%%>', '', $templateCode);
		$templateCode=str_replace('<%%URLVALUE(cod_respuesta)%%>', urlencode(''), $templateCode);
		$templateCode=str_replace('<%%VALUE(texto_respuesta)%%>', '', $templateCode);
		$templateCode=str_replace('<%%URLVALUE(texto_respuesta)%%>', urlencode(''), $templateCode);
		$templateCode=str_replace('<%%VALUE(url_respuesta)%%>', 'blank.gif', $templateCode);
		$templateCode=str_replace('<%%VALUE(url2_respuesta)%%>', 'blank.gif', $templateCode);
	}

	// process translations
	foreach($Translation as $symbol=>$trans){
		$templateCode=str_replace("<%%TRANSLATION($symbol)%%>", $trans, $templateCode);
	}

	// clear scrap
	$templateCode=str_replace('<%%', '<!--', $templateCode);
	$templateCode=str_replace('%%>', '-->', $templateCode);

	// hide links to inaccessible tables
	if($_POST['dvprint_x']==''){
		$templateCode.="\n\n<script>\n";
		$arrTables=getTableList();
		foreach($arrTables as $name=>$caption){
			$templateCode.="\tif(document.getElementById('".$name."_link')!=undefined){\n";
			$templateCode.="\t\tdocument.getElementById('".$name."_link').style.visibility='visible';\n";
			$templateCode.="\t}\n";
			for($i=1; $i<10; $i++){
				$templateCode.="\tif(document.getElementById('".$name."_plink$i')!=undefined){\n";
				$templateCode.="\t\tdocument.getElementById('".$name."_plink$i').style.visibility='visible';\n";
				$templateCode.="\t}\n";
			}
		}

		$templateCode.=$jsReadOnly;

		if(!$selected_id){
		}

		$templateCode.="\n</script>\n";
	}

	// ajaxed auto-fill fields
	$templateCode.="<script>";
	$templateCode.="document.observe('dom:loaded', function() {";


	$templateCode.="});";
	$templateCode.="</script>";

	// handle enforced parent values for read-only lookup fields

	// don't include blank images in lightbox gallery
	$templateCode=preg_replace('/blank.gif" rel="lightbox\[.*?\]"/', 'blank.gif"', $templateCode);

	// don't display empty email links
	$templateCode=preg_replace('/<a .*?href="mailto:".*?<\/a>/', '', $templateCode);

	// hook: ex_respuestas_dv
	if(function_exists('ex_respuestas_dv')){
		$args=array();
		ex_respuestas_dv(($selected_id ? $selected_id : FALSE), getMemberInfo(), $templateCode, $args);
	}

	return $templateCode;
}
?>
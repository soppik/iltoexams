<!DOCTYPE html>
<html>
<head>
    <title>TECS | ILTO</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="Content-Language" content="ES" />
    
    <link rel="canonical" href="/" />

    
    


    <!--Skeleton Framework
<link rel="stylesheet" type="text/css" media="screen" href="/includes/libs/js/resources/framework.css" />
-->
<link rel="shortcut icon" href="media/favicon.ico">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!--jQuery-->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" ></script>
<script src="media/all_min.js"></script>


<script type="text/javascript" src="http:/maps.google.com/maps/api/js?sensor=false"></script>
<link href='includes/libs/js/fancybox/jquery.fancybox-1.3.4.css'  type="text/css" rel="stylesheet">
<script src="includes/libs/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>

<script src="includes/libs/js/infobox/infobox.js"></script>

<script src="http:/code.jquery.com/ui/1.10.3/jquery-ui.js"></script>




    <link rel="stylesheet" type="text/css" href="includes/style.css">
<script type="text/javascript" src="includes/include.js"></script>


    
</head>

<!--[if lt IE 7 ]> <body class="ie6 lang-ES" > <![endif]-->
<!--[if IE 7 ]> <body class="ie7 lang-ES" > <![endif]-->
<!--[if IE 8 ]> <body class="ie8 lang-ES" > <![endif]-->
<!--[if IE 9 ]> <body class="ie9 lang-ES" > <![endif]-->
<!--[if !IE]><!--> <body class="lang-ES" > <!--<![endif]-->

<script type="text/javascript">
/* <![CDATA[ */* ]]> */
</script>





<div class="transit-internacional sub-page sub-landing" id="page">
<?php include("includes/header.php"); ?>

  <!-- GALLERY --> <section id="main-gallery" class="drop-shadow"> <div id="mg-container">
                      <div class="side-grid-container">
              <div class="side-grid" id="left"></div>
              <div class="side-grid" id="right"></div>
            </div>
        <div class="flexslider">
           
                      <ul class="slides"><li id="call-id-267">

                

                <div class="caption-container-wrapper">
                  <div class="caption-container">
                    <div class="caption">
                      <h5></h5>
                      <p></p>
                      <a href="" class="read-more"></a>
                    </div>
                  </div>
                </div>
                <img src="img/im_tcs1.jpg" alt=""/>
              </li><li id="call-id-456">

                

                <div class="caption-container-wrapper">
                  <div class="caption-container">
                    <div class="caption">
                      <h5> </h5>
                      <p></p>
                      <a href="" class="read-more size_231"></a>
                    </div>
                  </div>
                </div>
                <img src="img/im_tcs2.jpg" alt=" "/>
              </li></ul>
            </div>
        </div> </section> <article>  </article><!-- CONTENT --> <section id="content">
  <div class="wrapper"><ul id="breadcrumb">
     <article class="narrow large-text">
      <div class="wrapper">
        <div class="content_scroll" style="position:relative;width:100%;height:500px;padding:5px;overflow: scroll;overflow-x: hidden;">
          <p style="font-size:14px;text-align:justify;line-height: 15px;"><strong>Terms and Conditions of use of ILTO products</strong><br><br>
              ILTO - The International Language Testing Organization is a United States
              Limited Liability Company established in the state of Florida, which follows US
              regulations and is protected by US agencies to assure its users the use of reliable
              English Language Communications scores for the individual purpose, under
              similar opportunity conditions of test performance to guarantee the score validity.<br><br>
              Following the terms and conditions applied to all the ILTO products. By clicking
              on the accept terms and conditions box, you agreed with all the policies and
              accept further actions involving the protection the consistency and reliability of all
              ILTO products.<br><br>
              <strong>Confidentiality</strong><br><br>
                ILTO understands that we are dealing with personal information of individuals
                who administer, rate and take the tests, and thus acknowledges their right to
                information privacy. ILTO is concerned with protecting the information stored in
                our servers and thus we only disclose information under the express
                authorization of the individual, nevertheless ILTO reserves the right to reveal
                information under the following cases:<br><br>
                * ILTO may reveal score data for verification purposes with organizations
                where an individual has submitted a certificate. This data will be available
                under the Certificates Validation section in the website, under the
                assumption that the individual acknowledges that the organizations have
                the right to validate the scores in the certificate.<br><br>
                * TECS scores will be used for research, statistical and psychometric
                purposes where permanent analysis is being performed by ILTO
                Academic Department, without taking into account the test takers’
                personal information.<br><br>
                * The TECS certificates and information will be available in the test
                administrator’s dashboard where the individual took the test, under the
                assumption that the test taker acknowledges that it is mandatory to provide
                personal information to fill in the administrator forms and to display in the
                TECS certificate to identify the test information.<br><br>
                * The personal and organizational information of the test taker and the test
                administrator will be disclosed if local government authorities require it as
                part of an investigation concerning violations of the law or behaviors that
                put in danger the test security or reliability.<br><br>
                <strong>ILTO use of the individuals’ personal data</strong><br><br>
                By registering to ILTO as administrator or test taker and providing data such as
                identification, or your picture or company logo, you expressly allow ILTO to utilize
                such information for information purposes to ILTO third parties interested in
                knowing more about the organizations using our products or performing
                demographic studies regarding our test takers, test administrators and raters.
                You also allow ILTO to use your contact information to send you relevant
                materials concerning your test performance and strategies to improve or maintain
                your English language level. These emails may include links from third parties
                involving possible offers on courses or training material.<br><br>
                <strong>Information storage and certificates issue </strong><br><br>
                ILTO undertakes several strategies to protect the information of the
                organizations, raters and test takers registered in our dashboard. Servers are
                secure and policies are implemented to avoid unauthorized information
                disclosure, unless the country government or private agencies require this
                information for criminal or fraud attempts investigations. ILTO reserves the right
                to keep the test takers information and scores for a maximum of three years, time
                after which ILTO is no longer responsible for test takers information or their
                scores.<br><br> 
                <strong>Questions about the items and scores disputes</strong><br><br>
                 The academic team at ILTO is made of a group of professionals who design and
                write test items following the item creation cycle consisting in three steps:
                creation, validation, publication. ILTO follows standardized processes that assure
                the reliability of the items in order to keep to consistency of the scores in all the
                test takers.<br><br>  
                The TECS affordably assesses the English proficiency level of test takers in a
                timely manner through a comprehensive items bank, based on updated topics
                used by people in real communication contexts. These topics have to do with so
                today´s cultural reality such as music, sports, social networks, world news,
                technology and other general interest topics that assess real communication
                among individuals in a day-to-day basis. The TECS measures the test takers
                performance in their production and perception areas, through four different
                sections. Language production assessed through speaking and language use
                sections and Language perception evaluated through reading comprehension of
                different text samples, including iconographic interpretation as well as listening
                or real communication samples such as government debates, radio shows, TV
                news among others. TECS results are standardized according to the Common
                European Framework for Language Reference – CEFR.<br><br> 
                The academic team, permanently oversees the quality of the test items,
                nonetheless, ILTO understands that there may be questions, concerns or
                comments regarding test items or test scores; for information or request on this
                matter, the individual should send write to info@iltoexams.com with the
                corresponding information.<br><br> 
                With the aim of making the TECS a more accessible test, where results can be
                issued in a timely manner, ILTO has opened the administrators the possibility or
                rating the speaking skills of the test takers. The scores for the speaking skills are
                validated by a set of rubrics that are followed by raters and which are verified to
                make scores reliable to test takers and organizations. Should the individual
                require further information or have a special request on this matter, the individual
                should send write to info@iltoexams.com with the corresponding information.
                <br><br> 
                <strong>Annulment of TECS certificates at ILTO </strong><br><br>
                The effective use of the TECS certificates relies on reporting the English
                Communication skills of an individual after taking a test, under a secure
                environment that reveals the transparency and individual performance of each
                test taker. As a result ILTO makes special emphasis on assuring that all the
                individuals taking the test, undergo the same process to perform tasks that serve
                as an evidence of their communication skills in the second language. Thus
                overseeing that no indivual has supplementary benefits that do not reflect the
                knowledge of the language, but demonstrate the use of certain doubtful activity
                in the process, is of utmost importance.<br><br>
                In compliance with the above, ILTO reserves the right to perform the annulment
                of TECS certificates where there is evidence of behavioral or irregular actions
                that disturb the impartial result of the test takers or the consistency of the items.
                As completely discretionary of ILTO and without prejudice or permission of the
                test taker or test administrator, ILTO may open an investigation to the test taker
                and thus the TECS certificate will be held until the investigation has determined
                the identity and legitimacy of the test taker and the test scores.
                You expressly acknowledge that the result of this investigation may have an
                effect on the individual’s criminal record in the country where the test is taken. In
                addition ILTO and the TECS are protected by the United States of the America
                anti-identity theft and antifraud regulations, which may derive in a US based
                investigation; as well as the prohibition of the possibility taking the test in any of
                the test administrator organizations around the world. ILTO also reserves the
                right to share the results of the investigation to third parties to prevent the
                individual’s fraudulent actions in the future.<br><br>
                Following the causes (not limited) that would result in the commencement of an
                investigation case and feasibly the annulment of the TECS certificate:<br><br>  
                *The use of techniques that suggest that the test taker has performed
                actions that allowed the gaining of advantage to answer the test items,
                such as the memorization of the test items, test topics or any kind of
                trickery to answer the test without the corresponding knowledge of the
                language.<br><br> 
                * The assistance of an individual that, by using technological tools,
                answers the test instead of the registered test taker, becoming a
                variation of identity theft.<br><br> 
                * Disruption of the test administration that may result in the dispute of test
                scores, caused by external factor such as technological issues, computer
                configuration, extreme weather or other that affect the normal
                development of the test.<br><br> 
                * If there are issues with the test administrator premises or procedures that
                are evidence of dishonest actions or fraudulent actions. In cases where
                there is evidence of fraudulent or doubly behavior by the test
                administrator and it has been determined is involved in criminal and
                fraudulent actions; ILTO at its sole discretion, may terminate the contract
                with the test administrator. Annulment of certificates may take place,
                even if the there is no clear evidence of the test taker doubtful or
                dishonest behavior.<br><br> 
                * According to the United States of America, Federal Bureau of
                Investigation (FBI), Identity Theft occurs when someone assumes the
                identity of another person, to perform a fraud or other criminal act. ILTO
                considers this a major fraudulent behavior, which will result in the
                immediate annulation of the certificate, the insertion in the fraud alert
                report of the test taker, lifetime ban to take the test in any test
                administrator location around the world and notify the local organization
                and government authorities, which will perform the corresponding
                investigation. <br><br> 
                If any of the above fraud behaviors are detected in a test taker, ILTO reserves
                the right to immediately dismiss the test taker from the test administrator’s
                premises and open the corresponding fraud case. The test taker scores will be
                cancelled and the test fee will not be reimbursed. You herein acknowledge that
                if the tests taker performs threatening or aggressive behavior, the test
                administrator personnel reserves the right to request support of the organization
                Security Company or local government authorities.
                <strong>ILTO Liability policy</strong><br><br>
                You herein acknowledge that ILTO is not liable for any test taker moral or
                economic damage, harm or loss for the annulation of the certificate, banning of
                the test for future test dates or any action resulting from the notification of the
                test taker to local organization or government authorities.<br><br>
                <strong>Terms and conditions amendments and modifications</strong><br><br>
                ILTO reserves the right to make modifications, to the current document,
                according to the laws, technological advances or other reasons that the ILTO
                considers important for the security and reliability of its products and its test
                administrators and other stakeholders. <br><br>
                <strong>Legal jurisdiction and disputes</strong><br><br>
                You herein agree that legal actions deriving from this document will be ruled
                under the regulation of the United States of America and the State of Florida,
                without concerns of the principles of conflict of laws. Any legal action related to
                these terms, condition must be resolved exclusively in the state or federal
                courts located in the State of Florida, and you agree to the jurisdiction of those
                courts.
                <br><br> 
              </p>
      </div>
      </article></div>
  </section> 
<?php include("includes/pie.php"); ?>

    	</body>
        </html>
<?php
/**
 * The base template for displaying 404 pages (not found).
 *
 * @package Kiddlez
 */
?>
<?php get_header( kiddlez_template_base() ); ?>

	<div <?php kiddlez_content_wrap_class(); ?>>

		<div class="row">

			<div id="primary" <?php kiddlez_primary_content_class(); ?>>

				<main id="main" class="site-main" role="main">

					<?php include kiddlez_template_path(); ?>

				</main><!-- #main -->

			</div><!-- #primary -->

			<?php get_sidebar(); // Loads the sidebar.php. ?>

		</div><!-- .row -->

	</div><!-- .site-content_wrap -->

<?php get_footer( kiddlez_template_base() ); ?>

<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Kiddlez
 */
$sidebar_position = get_theme_mod( 'sidebar_position', kiddlez_theme()->customizer->get_default( 'sidebar_position' ) );

if ( 'fullwidth' === $sidebar_position ) {
	return;
}

if ( is_active_sidebar( 'sidebar' ) && ! kiddlez_is_product_page() ) {
	do_action( 'kiddlez_render_widget_area', 'sidebar' );
}

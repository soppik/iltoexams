<?php
/**
 * TM Wizard configuration.
 *
 * @var array
 *
 * @package Kiddlez
 */
$plugins = array(
	'cherry-data-importer' => array(
		'name'   => esc_html__( 'Cherry Data Importer', 'kiddlez' ),
		'source' => 'remote', // 'local', 'remote', 'wordpress' (default).
		'path'   => 'https://github.com/CherryFramework/cherry-data-importer/archive/master.zip',
		'access' => 'base',
	),
	'cherry-projects' => array(
		'name'   => esc_html__( 'Cherry Projects', 'kiddlez' ),
		'access' => 'skins',
	),
	'cherry-team-members' => array(
		'name'   => esc_html__( 'Cherry Team Members', 'kiddlez' ),
		'source' => 'local',
		'path'   => KIDDLEZ_THEME_DIR . '/assets/includes/plugins/cherry-team-members.zip',
		'access' => 'skins',
	),
	'cherry-testi' => array(
		'name'   => esc_html__( 'Cherry Testimonials', 'kiddlez' ),
		'source' => 'local',
		'path'   => KIDDLEZ_THEME_DIR . '/assets/includes/plugins/cherry-testi.zip',
		'access' => 'skins',
	),
	'cherry-services-list' => array(
		'name'   => esc_html__( 'Cherry Services List', 'kiddlez' ),
		'access' => 'skins',
	),
	'cherry-sidebars' => array(
		'name'   => esc_html__( 'Cherry Sidebars', 'kiddlez' ),
		'source' => 'local',
		'path'   => KIDDLEZ_THEME_DIR . '/assets/includes/plugins/cherry-sidebars.zip',
		'access' => 'skins',
	),
	'cherry-socialize' => array(
		'name'   => esc_html__( 'Cherry Socialize', 'kiddlez' ),
		'access' => 'skins',
	),
	'cherry-search' => array(
		'name'   => esc_html__( 'Cherry Search', 'kiddlez' ),
		'access' => 'skins',
	),
	'cherry-popups' => array(
		'name'   => esc_html__( 'Cherry Popups', 'kiddlez' ),
		'access' => 'skins',
	),
	'cherry-trending-posts' => array(
		'name'   => esc_html__( 'Cherry Trending Posts', 'kiddlez' ),
		'access' => 'skins',
	),
	'wordpress-social-login' => array(
		'name'   => esc_html__( 'WordPress Social Login', 'kiddlez' ),
		'access' => 'skins',
	),
	'elementor' => array(
		'name'   => esc_html__( 'Elementor Page Builder', 'kiddlez' ),
		'access' => 'base',
	),
	'jet-menu' => array(
		'name'   => esc_html__( 'Jet Menu', 'kiddlez' ),
		'source' => 'local',
		'path'   => KIDDLEZ_THEME_DIR . '/assets/includes/plugins/jet-menu.zip',
		'access' => 'base',
	),
	'jet-elements' => array(
		'name'   => esc_html__( 'Jet Elements', 'kiddlez' ),
		'source' => 'local',
		'path'   => KIDDLEZ_THEME_DIR . '/assets/includes/plugins/jet-elements.zip',
		'access' => 'base',
	),
	'tm-photo-gallery' => array(
		'name'   => esc_html__( 'TM Photo Gallery', 'kiddlez' ),
		'access' => 'skins',
	),
	'tm-timeline' => array(
		'name'   => esc_html__( 'TM Timeline', 'kiddlez' ),
		'access' => 'skins',
	),
	'contact-form-7' => array(
		'name'   => esc_html__( 'Contact Form 7', 'kiddlez' ),
		'access' => 'skins',
	),
	'the-events-calendar' => array(
		'name'   => esc_html__( 'The Events Calendar', 'kiddlez' ),
		'access' => 'skins',
	),
	'tm-dashboard' => array(
		'name'   => esc_html__( 'Jetimpex Dashboard', 'kiddlez' ),
		'source' => 'remote',
		'path'   => 'http://cloud.cherryframework.com/downloads/free-plugins/tm-dashboard.zip',
		'access' => 'base',
	),
);

/**
 * Skins configuration example
 *
 * @var array
 */
$skins = array(
	'base' => array(
		'elementor',
		'jet-elements',
		'jet-menu',
		'cherry-data-importer',
		'tm-dashboard',
	),
	'advanced' => array(
		'default' => array(
			'full'  => array(
				'cherry-projects',
				'cherry-team-members',
				'cherry-testi',
				'cherry-services-list',
				'cherry-sidebars',
				'cherry-socialize',
				'cherry-trending-posts',
				'cherry-search',
				'cherry-popups',
				'wordpress-social-login',
				'tm-photo-gallery',
				'tm-timeline',
				'contact-form-7',
				'the-events-calendar',
			),
			'lite'  => false,
			'demo'  => 'http://ld-wp.template-help.com/wordpress_prod-13688/main',
			'thumb' => get_template_directory_uri() . '/assets/demo-content/default/default.png',
			'name'  => esc_html__( 'Kiddlez', 'kiddlez' ),
		),
	),
);

$texts = array(
	'theme-name' => esc_html__( 'Kiddlez', 'kiddlez' ),
);

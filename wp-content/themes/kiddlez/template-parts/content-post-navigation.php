<?php
/**
 * Template part for single post navigation.
 *
 * @package Kiddlez
 */

if ( ! get_theme_mod( 'single_post_navigation', kiddlez_theme()->customizer->get_default( 'single_post_navigation' ) ) ) {
	return;
}

the_post_navigation( array(
	'prev_text' => '<span class="screen-reader-text">' . esc_html__( 'Previous Post', 'kiddlez' ) . '</span>%title',
	'next_text' => '<span class="screen-reader-text">' . esc_html__( 'Next Post', 'kiddlez' ) . '</span>%title',
) );

<?php
/**
 * Template part for default header layout.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Kiddlez
 */

$search_visible = get_theme_mod( 'header_search', kiddlez_theme()->customizer->get_default( 'header_search' ) );
?>
<div <?php echo kiddlez_get_container_classes( array(), 'header' ) ?>>
	<div class="header-container_wrap">
		<div class="header-container__flex">
			<div class="site-branding">
				<?php kiddlez_header_logo() ?>
				<?php kiddlez_site_description(); ?>
			</div>

			<div class="header-nav-wrapper">
				<?php kiddlez_main_menu(); ?>

				<?php if ( $search_visible ) : ?>
					<div class="header-components"><?php
						kiddlez_header_search_toggle();
					?></div>
				<?php endif; ?>
				<?php kiddlez_header_search( '<div class="header-search">%s<button class="search-form__close"></button></div>' ); ?>
			</div>

			<?php kiddlez_header_btn(); ?>
		</div>
	</div>
</div>

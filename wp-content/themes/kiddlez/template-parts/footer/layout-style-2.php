<?php
/**
 * The template for displaying the style-2 footer layout.
 *
 * @package Kiddlez
 */
?>

<div <?php kiddlez_footer_container_class(); ?>>
	<div <?php echo kiddlez_get_container_classes( array(), 'footer' ) ?>>
		<div class="site-info"><?php
			kiddlez_footer_logo();
			kiddlez_footer_menu();
			kiddlez_contact_block( 'footer' );
			kiddlez_social_list( 'footer' );
			kiddlez_footer_copyright();
		?></div><!-- .site-info -->
	</div>
</div><!-- .footer-container -->

<?php
/**
 * The template for displaying the default footer layout.
 *
 * @package Kiddlez
 */
?>

<div <?php kiddlez_footer_container_class(); ?>>
	<div <?php echo kiddlez_get_container_classes( array(), 'footer' ) ?>>
		<div class="site-info">
			<div class="site-info-wrap"><?php
				kiddlez_footer_logo();
				kiddlez_footer_copyright();
				kiddlez_footer_menu();
				kiddlez_contact_block( 'footer' );
				kiddlez_social_list( 'footer' );
			?></div>
		</div><!-- .site-info -->
	</div>
</div><!-- .footer-container -->

<?php
/**
 * The template part for displaying page preloader.
 *
 * @package Kiddlez
 */
?>
<div class="page-preloader-cover">
	<?php kiddlez_preloader_logo(); ?>
	<div class="bar"></div>
</div>

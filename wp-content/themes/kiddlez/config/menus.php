<?php
/**
 * Menus configuration.
 *
 * @package Kiddlez
 */

add_action( 'after_setup_theme', 'kiddlez_register_menus', 5 );
/**
 * Register menus.
 */
function kiddlez_register_menus() {

	register_nav_menus( array(
		'top'    => esc_html__( 'Top', 'kiddlez' ),
		'main'   => esc_html__( 'Main', 'kiddlez' ),
		'footer' => esc_html__( 'Footer', 'kiddlez' ),
		'social' => esc_html__( 'Social', 'kiddlez' ),
	) );
}

<?php
/**
 * Post meta configuration.
 *
 * @package Kiddlez
 */

/**
 * Get post meta settings.
 *
 * @return array
 */
function kiddlez_get_post_meta_settings() {

	$container_options = array(
		'inherit'   => array(
			'label'   => esc_html__( 'Inherit', 'kiddlez' ),
			'img_src' => trailingslashit( KIDDLEZ_THEME_URI ) . 'assets/images/admin/inherit.svg',
		),
		'boxed'     => array(
			'label'   => esc_html__( 'Boxed', 'kiddlez' ),
			'img_src' => trailingslashit( KIDDLEZ_THEME_URI ) . 'assets/images/admin/type-boxed.svg',
		),
		'fullwidth' => array(
			'label'   => esc_html__( 'Fullwidth', 'kiddlez' ),
			'img_src' => trailingslashit( KIDDLEZ_THEME_URI ) . 'assets/images/admin/type-fullwidth.svg',
		),
	);

	return apply_filters( 'kiddlez_get_post_meta_settings',  array(
		'id'            => 'page-settings',
		'title'         => esc_html__( 'Page settings', 'kiddlez' ),
		'page'          => array( 'post', 'page', 'team', 'projects', 'cherry-services' ),
		'context'       => 'normal',
		'priority'      => 'high',
		'callback_args' => false,
		'fields'        => array(
			'tabs' => array(
				'element' => 'component',
				'type'    => 'component-tab-horizontal',
			),
			'layout_tab' => array(
				'element'     => 'settings',
				'parent'      => 'tabs',
				'title'       => esc_html__( 'Layout Options', 'kiddlez' ),
			),
			'header_tab' => array(
				'element'     => 'settings',
				'parent'      => 'tabs',
				'title'       => esc_html__( 'Header Style', 'kiddlez' ),
				'description' => esc_html__( 'Header style settings', 'kiddlez' ),
			),
			'header_elements_tab' => array(
				'element'     => 'settings',
				'parent'      => 'tabs',
				'title'       => esc_html__( 'Header Elements', 'kiddlez' ),
				'description' => esc_html__( 'Enable/Disable header elements', 'kiddlez' ),
			),
			'breadcrumbs_tab' => array(
				'element'     => 'settings',
				'parent'      => 'tabs',
				'title'       => esc_html__( 'Breadcrumbs', 'kiddlez' ),
				'description' => esc_html__( 'Breadcrumbs settings', 'kiddlez' ),
			),
			'footer_tab' => array(
				'element'     => 'settings',
				'parent'      => 'tabs',
				'title'       => esc_html__( 'Footer Settings', 'kiddlez' ),
				'description' => esc_html__( 'Footer settings', 'kiddlez' ),
			),
			'kiddlez_sidebar_position' => array(
				'type'          => 'radio',
				'parent'        => 'layout_tab',
				'title'         => esc_html__( 'Sidebar layout', 'kiddlez' ),
				'description'   => esc_html__( 'Sidebar position global settings redefining. If you select inherit option, global setting will be applied for this layout', 'kiddlez' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options'       => array(
					'inherit' => array(
						'label'   => esc_html__( 'Inherit', 'kiddlez' ),
						'img_src' => trailingslashit( KIDDLEZ_THEME_URI ) . 'assets/images/admin/inherit.svg',
					),
					'one-left-sidebar' => array(
						'label'   => esc_html__( 'Sidebar on left side', 'kiddlez' ),
						'img_src' => trailingslashit( KIDDLEZ_THEME_URI ) . 'assets/images/admin/page-layout-left-sidebar.svg',
					),
					'one-right-sidebar' => array(
						'label'   => esc_html__( 'Sidebar on right side', 'kiddlez' ),
						'img_src' => trailingslashit( KIDDLEZ_THEME_URI ) . 'assets/images/admin/page-layout-right-sidebar.svg',
					),
					'fullwidth' => array(
						'label'   => esc_html__( 'No sidebar', 'kiddlez' ),
						'img_src' => trailingslashit( KIDDLEZ_THEME_URI ) . 'assets/images/admin/page-layout-fullwidth.svg',
					),
				),
			),
			'kiddlez_page_container_type' => array(
				'type'          => 'radio',
				'parent'        => 'layout_tab',
				'title'         => esc_html__( 'Page type', 'kiddlez' ),
				'description'   => esc_html__( 'Page type global settings redefining. If you select inherit option, global setting will be applied for this layout', 'kiddlez' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options'       => $container_options,
			),
			'kiddlez_header_container_type' => array(
				'type'          => 'radio',
				'parent'        => 'layout_tab',
				'title'         => esc_html__( 'Header container type', 'kiddlez' ),
				'description'   => esc_html__( 'Header container type global settings redefining. If you select inherit option, global setting will be applied for this layout', 'kiddlez' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options'       => $container_options,
			),
			'kiddlez_breadcrumbs_container_type' => array(
				'type'          => 'radio',
				'parent'        => 'layout_tab',
				'title'         => esc_html__( 'Breadcrumbs container type', 'kiddlez' ),
				'description'   => esc_html__( 'Breadcrumbs container type global settings redefining. If you select inherit option, global setting will be applied for this layout', 'kiddlez' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options'       => $container_options,
			),
			'kiddlez_content_container_type' => array(
				'type'          => 'radio',
				'parent'        => 'layout_tab',
				'title'         => esc_html__( 'Content container type', 'kiddlez' ),
				'description'   => esc_html__( 'Content container type global settings redefining. If you select inherit option, global setting will be applied for this layout', 'kiddlez' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options'       => $container_options,
			),
			'kiddlez_footer_container_type'  => array(
				'type'          => 'radio',
				'parent'        => 'layout_tab',
				'title'         => esc_html__( 'Footer container type', 'kiddlez' ),
				'description'   => esc_html__( 'Footer container type global settings redefining. If you select inherit option, global setting will be applied for this layout', 'kiddlez' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options'       => $container_options,
			),
			'kiddlez_header_layout_type' => array(
				'type'    => 'select',
				'parent'  => 'header_tab',
				'title'   => esc_html__( 'Header Layout', 'kiddlez' ),
				'value'   => 'inherit',
				'options' => kiddlez_get_header_layout_pm_options(),
			),
			'kiddlez_header_transparent_layout' => array(
				'type'          => 'radio',
				'parent'        => 'header_tab',
				'title'         => esc_html__( 'Header Transparent Overlay', 'kiddlez' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options' => array(
					'inherit' => array(
						'label' => esc_html__( 'Inherit', 'kiddlez' ),
					),
					'true'    => array(
						'label' => esc_html__( 'Enable', 'kiddlez' ),
						'slave' => 'header-transparent',
					),
					'false'   => array(
						'label' => esc_html__( 'Disable', 'kiddlez' ),
					),
				),
			),
			'kiddlez_header_transparent_bg' => array(
				'type'          => 'colorpicker',
				'parent'        => 'header_tab',
				'title'         => esc_html__( 'Header Transparent Background', 'kiddlez' ),
				'value'         => '',
				'master'        => 'header-transparent',
				'display_input' => false,
			),
			'kiddlez_header_transparent_bg_alpha' => array(
				'type'          => 'slider',
				'parent'        => 'header_tab',
				'title'         => esc_html__( 'Header Transparent Background Alpha', 'kiddlez' ),
				'max_value'     => 100,
				'min_value'     => -1,
				'step_value'    => 1,
				'master'        => 'header-transparent',
				'display_input' => false,
			),
			'kiddlez_header_invert_color_scheme' => array(
				'type'          => 'radio',
				'parent'        => 'header_tab',
				'title'         => esc_html__( 'Invert Color Scheme', 'kiddlez' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options' => array(
					'inherit' => array(
						'label' => esc_html__( 'Inherit', 'kiddlez' ),
					),
					'true'    => array(
						'label' => esc_html__( 'Enable', 'kiddlez' ),
					),
					'false'   => array(
						'label' => esc_html__( 'Disable', 'kiddlez' ),
					),
				),
			),
			'kiddlez_top_panel_visibility' => array(
				'type'          => 'select',
				'parent'        => 'header_elements_tab',
				'title'         => esc_html__( 'Top panel', 'kiddlez' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options' => array(
					'inherit' => esc_html__( 'Inherit', 'kiddlez' ),
					'true'    => esc_html__( 'Enable', 'kiddlez' ),
					'false'   => esc_html__( 'Disable', 'kiddlez' ),
				),
			),
			'kiddlez_header_contact_block_visibility' => array(
				'type'          => 'select',
				'parent'        => 'header_elements_tab',
				'title'         => esc_html__( 'Header Contact Block', 'kiddlez' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options' => array(
					'inherit' => esc_html__( 'Inherit', 'kiddlez' ),
					'true'    => esc_html__( 'Enable', 'kiddlez' ),
					'false'   => esc_html__( 'Disable', 'kiddlez' ),
				),
			),
			'kiddlez_header_search' => array(
				'type'          => 'select',
				'parent'        => 'header_elements_tab',
				'title'         => esc_html__( 'Header Search', 'kiddlez' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options' => array(
					'inherit' => esc_html__( 'Inherit', 'kiddlez' ),
					'true'    => esc_html__( 'Enable', 'kiddlez' ),
					'false'   => esc_html__( 'Disable', 'kiddlez' ),
				),
			),
			'kiddlez_breadcrumbs_visibillity' => array(
				'type'          => 'radio',
				'parent'        => 'breadcrumbs_tab',
				'title'         => esc_html__( 'Breadcrumbs visibillity', 'kiddlez' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options' => array(
					'inherit' => array(
						'label' => esc_html__( 'Inherit', 'kiddlez' ),
					),
					'true'    => array(
						'label' => esc_html__( 'Enable', 'kiddlez' ),
					),
					'false'   => array(
						'label' => esc_html__( 'Disable', 'kiddlez' ),
					),
				),
			),
			'kiddlez_footer_layout_type' => array(
				'type'    => 'select',
				'parent'  => 'footer_tab',
				'title'   => esc_html__( 'Footer Layout', 'kiddlez' ),
				'value'   => 'inherit',
				'options' => kiddlez_get_footer_layout_pm_options(),
			),
			'kiddlez_footer_widget_area_visibility' => array(
				'type'          => 'select',
				'parent'        => 'footer_tab',
				'title'         => esc_html__( 'Footer Widgets Area', 'kiddlez' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options' => array(
					'inherit' => esc_html__( 'Inherit', 'kiddlez' ),
					'true'    => esc_html__( 'Enable', 'kiddlez' ),
					'false'   => esc_html__( 'Disable', 'kiddlez' ),
				),
			),
			'kiddlez_footer_contact_block_visibility' => array(
				'type'          => 'select',
				'parent'        => 'footer_tab',
				'title'         => esc_html__( 'Footer Contact Block', 'kiddlez' ),
				'value'         => 'inherit',
				'display_input' => false,
				'options' => array(
					'inherit' => esc_html__( 'Inherit', 'kiddlez' ),
					'true'    => esc_html__( 'Enable', 'kiddlez' ),
					'false'   => esc_html__( 'Disable', 'kiddlez' ),
				),
			),
		),
	) ) ;
}

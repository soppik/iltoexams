<?php
// change the following path if necessary
defined('YII_PATH') or define('YII_PATH', (dirname(__FILE__)) . '/yii/framework');

require_once YII_PATH . '/yii.php';

$mainConfig = require dirname(__FILE__) . '/ilto3/protected/config/main.php';

include(dirname(__FILE__) . '/ilto3/protected/models/Clients.php');

$connection=new CDbConnection('mysql:host=localhost;dbname=iltoexam_newversion','iltoexam_newvers','Ilto.2015');
//$connection=new CDbConnection('mysql:host=localhost;dbname=thetec_iltoexam_newversion','thetec_newvers','Ilto.2015');
$connection->active=true;
$body_background = 'background-image: url(/ilto3/themes/ilto/img/cyber-security.jpg) !important;';


?>
<!DOCTYPE html>
<html>
  <head>
    <meta name="description" content="inbox" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="google" content="notranslate" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="/ilto3/themes/ilto/html/assets/img/favicon.png" type="image/x-icon">
    
    <!--Basic Styles-->
    <link href="/ilto3/themes/ilto/html/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="/ilto3/themes/ilto/html/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/ilto3/themes/ilto/html/assets/css/weather-icons.min.css" rel="stylesheet" />
    
    <!--Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css">
    
    <!--Beyond styles-->
    <link id="beyond-link" href="/ilto3/themes/ilto/html/assets/css/beyond.min.css" rel="stylesheet" />
    <link href="/ilto3/themes/ilto/html/assets/css/demo.min.css" rel="stylesheet" />
    <link href="/ilto3/themes/ilto/html/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="/ilto3/themes/ilto/html/assets/css/animate.min.css" rel="stylesheet" />
    
    <link id="skin-link" href="" rel="stylesheet" type="text/css" />
    <script src="/ilto3/assets/js/jquery.min.js"></script>
    <script src="/ilto3/assets/js/bootstrap.min.js"></script>
    <title>ILTOEXAMS | Fraud Alert</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF8" />
    <style> 
      body {
      <?php echo $body_background; ?>
      background-position: center;
      background-repeat: no-repeat;
      background-size: cover;
      }
      body:before {
      background-color: transparent !important;
      }
      .login-container .loginbox  {
          width: 400px !important;
          padding: 30px 0px !important;
      }
      .login-container .logobox {
          box-shadow: 3px 3px 3px #444;
          width: 400px !important;
      }
      .flash-error {
          border-color: #df5138;
          background: #e46f61;
          margin-bottom: 20px;
          margin-top: 0;
          color: #fff;
          border-width: 0;
          border-left-width: 5px;
          padding: 10px;
          border-radius: 0;
          }
          
      .login-container .logobox {
      height: auto !important;
      position: relative;
          font-size: 14px;
      padding: 12px !important;
      }    
      
      .login-container{margin:2% auto !important;
      }    
      
      .form-control, select {
          font-size: 16px !important;
      }
      .spacer {
          display:block;
          height:120px;
      }
      
      #home_link {
          color: #fff;
          text-decoration:none;
      }
      
      #home_link over{
          color: #fff;
          text-decoration:none;
          font-weight:bold;
      }
      
      #superscore {
      opacity: 10;
      position: relative;
      z-index: 12;
      left:0px;
      width: 18px;
      height: 18px;
      cursor: pointer;
      margin-right: 10px;
      }
   </style>

    
</head>
  <body>
    <div class="row">
      <div class="spacer">
      </div>  
      <div class="col-sm-5">
        <div class="login-container animated fadeInDown">
          <div class="loginbox bg-white">
            <div class="loginbox-social">
              <div class="social-buttons">
                <a id="home_link" href="https://www.iltoexams.com">
                  <img src="/ilto3/images/iltoexams-logo-100.png" alt="ILTO EXAMS">
                </a>
              </div>
            </div>
            <div class="loginbox-title">&nbsp;</div>
              <div class="loginbox-title">Certificates validation</div>
                <div class="loginbox-textbox">
                  <input name="id_number" id="id_number" type="text" class="form-control" placeholder="Id Number *" required onInvalid="The Id number is required">
                </div>
                <div class="loginbox-textbox">
                  <input name="fraud_preco" id="fraud_preco" type="text" class="form-control" placeholder="Fraud Prevention Code *" required ></input>
                </div>
                <div class="loginbox-textbox">
                  <input type="checkbox" id="superscore" name="superscore" value="1"><label for="vehicle1">SUPERSCORE Certificate</label><br>
                </div>
                <div class="loginbox-submit">
                  <input id="get_report" class="btn btn-success btn-block" value="Validate" type="submit" name="yt0"><br/><br/>
                </div>
          </div>
        </div>
    </div>
    <div class="col-sm-7" style="padding:11px 25px;"> 
            <div class="row" id="info" style="  text-align: justify;background-color:#fff;width: 100%;">
            </div> 
              
        </div>
    </div>
  </body>
</html>

<script>
  $("#get_report").click(function() {
		var id = $("#id_number").val();
		var code = $("#fraud_preco").val();
		if($("#superscore").is(':checked')) {  
            var superscore = 1; 
        } else {  
            var superscore = 0;  
        }  
		$.ajax({
			url : "/ilto3/index.php?r=Api/certificateValidate",
			type: "POST",
			dataType: "text",
			data: {
			    id: id,
			    code: code,
			    superscore: superscore
			},
			success: function(data) {
				$("#info").html(data);
			}
		});
	});
	
	</script>
<style>
   
    
    .information-text p {
        font-size: 12px;
        line-height: initial;
        margin-left: 20px;
        color: #666;
        font-weight: normal;
    }
    
    .information-text {
        position: absolute;
        margin-left: -109px;
        background: white;
        padding: 10px;
        font-size: 12px;
        border: 1px solid #ccc;
        color: black;
        font-weight: bolder;
        width: 516px;
        margin-top: -46px;
    }
    
    .info-i {
        background: #B7B7B7;
        width: 14px;
        display: inline-block;
        text-align: center;
        border-radius: 50%;
        padding-left: 4px;
        padding-right: 4px;
        color: white;
        text-shadow: 1px 1px 1px grey;
    }
    .info-i:hover {
        background: #868686;
        cursor:pointer;
    }
    
    input[type='text']{
        width: 256px;
    }
    
    
    .porquedemo.info-i {
        background: #B7B7B7;
        width: 10px;
        display: inline-block;
        text-align: center;
        border-radius: 50%;
        padding-left: 2px;
        padding-right: 2px;
        color: white;
        text-shadow: 1px 1px 1px grey;
        height: 14px;
        font-size: 10px;
    }
    
    .demo.information-text {
        position: absolute;
        margin-left: 298px;
        background: white;
        padding: 10px;
        font-size: 12px;
        border: 1px solid #ccc;
        color: black;
        font-weight: bolder;
        width: 216px;
        margin-top: 6px;
    }
    
    .porquedemo.info-i:hover {
        background: #868686;
        cursor:pointer;
    }
    
    
    .nombre_rsocial{
        padding-left: 10px;
    }
    
    input.nombre_rsocial {
        padding-left: 53px;
        width: 213px!important;
    }
    
    .is_demo { 
        position: absolute;
        margin-top: 16px;
        margin-left: 5px;
        background: white;
    }
    
    
    input[type="password"]
    {
    	background: #fff;
        color: #666;
        font-family: "Calibri", sans-serif;
        float: left;
        font-size: 14px;
        margin-right: 20px;
        margin-top: 10px;
        width: 257px;
        height: 20px;
        line-height: 20px;
        padding: 5px 10px;
        border-top: 2px solid #8c8c8c;
        border-right: 1px solid #e1e1e1;
        border-left: 1px solid #e1e1e1;
        -webkit-border-radius: 7px;
        -moz-border-radius: 7px;
        border-radius: 7px;
    }
    
    #confirmMessage{
        font-weight: bold;
        color: rgb(255, 102, 102);
        position: absolute;
        width: 146px;
        font-size: 13px;
        margin-top: 17px;
    }
    
    .info-i {
        
        
        background: #1363dc;
        width: 9px;
        display: inline-block;
        text-align: center;
        border-radius: 50%;
        padding-left: 4px;
        padding-right: 4px;
        color: white;
        text-shadow: 1px 1px 1px grey;
        height: 17px;
        line-height: 16px;
        font-size: 15px;
    }

    .info-i.porquedemo{
        background: #1363dc;
        width: 10px;
        display: inline-block;
        text-align: center;
        border-radius: 50%;
        padding-left: 2px;
        padding-right: 2px;
        color: white;
        text-shadow: 1px 1px 1px grey;
        height: 14px;
        font-size: 10px;
        top: -14px;
        position: relative;
        margin-left: 25px;
        line-height: 13px;
    }
    
    .alert_message {
        padding-top:5px;
        font-size: 10px;
        color: #31B744;
        display:block;
    }
    
    input[type=number]::-webkit-outer-spin-button,
	input[type=number]::-webkit-inner-spin-button {
    -webkit-appearance: none;
	}
	
	input[type=number] {
    -moz-appearance:text;
    background: #fff;
    color: #666;
    font-family: "Calibri", sans-serif;
    float: left;
    font-size: 14px;
    margin-right: 20px;
    margin-top: 10px;
    width: 205px;
    height: 20px;
    line-height: 20px;
    padding: 5px 10px;
    border-top: 2px solid #8c8c8c;
    border-right: 1px solid #e1e1e1;
    border-left: 1px solid #e1e1e1;
    -webkit-border-radius: 7px;
    -moz-border-radius: 7px;
    border-radius: 7px;
	}
	
	#id_validate{
		text-align:center;
		color:red;
		font-size:12px;
	}
	#alertG_lbl {
		color:#53a93f;
	}
	#alertR_lbl {
		color:#ee5959;
	}
    
</style>
<p></p>
<p>
    
    <form id="ff" method="post" enctype="multipart/form-data">
        <table width="597">
            <tr>
                <td>
                    <input type="hidden" name="typeform" value="tecsagent">
                    <span class="is_demo">
                        DEMO:
                    </span>
                    <input name="Clients[nombre_rsocial]" type="text" class="nombre_rsocial" placeholder="Company Name *" required oninvalid="this.setCustomValidity('The Company Name is required!')" oninput="setCustomValidity('')">
                  
                </td>
                 
                <td>
                    <input name="Clients[nit_id_legal]" id="nit_id_legal" type="text" placeholder="Company ID *" required oninvalid="this.setCustomValidity('The ID Company is required!')" oninput="setCustomValidity('')"></input>
                </td>
                
                
            </tr>
            <tr>
                <td colspan=3>
                   <!-- <input name="EducationLevel" type="text" placeholder="Education Level" required></input> -->
                   <input name="Clients[direccion]" type="text" placeholder="Address *" required style="width:93%" oninvalid="this.setCustomValidity('The address field is required!')" oninput="setCustomValidity('')"></input>
                </td>
                
            </tr>
             <tr>
                 <?php 
                    $sql='SELECT * FROM country';
                    $list= $connection->createCommand($sql)->queryAll();
                 ?>
                <td>
                    <select name="Clients[id_pais]" required class="country-select" id="country-select" oninvalid="this.setCustomValidity('Country field is required!')" oninput="setCustomValidity('')">
                        <option value="" default>Country *</option>
                        <?php
                            foreach($list as $paises){
                                ?>
                                    <option value="<?php echo $paises['Code']?>"><?php echo utf8_encode($paises['Name'])?></option>
                                <?php
                            }
                        ?>
                    </select><span class="required_span">*</span>
        
                </td>
                <td>
                     <select name="Clients[id_ciudad]" required class="ciudades-select" id="city-select" oninvalid="this.setCustomValidity('City field is required!')" oninput="setCustomValidity('')">
                        <option value="" default>City *</option>
                        <option></option>
                     </select><span class="required_span">*</span>
                    
                </td>
            </tr>
            
            <tr>
                <td>
                    <input name="Clients[website]" type="text" placeholder="Website" oninput="setCustomValidity('')">
                </td>
                <td>
                    <input name="Clients[telefono]" type="text" placeholder="Phone number *" required oninvalid="this.setCustomValidity('The Phone Number is required!')" oninput="setCustomValidity('')"></input>
                </td>
            </tr>
            <tr>
                <td>
                    <p style="margin-top:20px;">Master Distributor</p>
                </td>
                <td>
                    
                    <?php 
                        $sql='SELECT id_cliente, nombre_rsocial FROM clients WHERE id_cliente="9004302647"';
                        $list= $connection->createCommand($sql)->queryAll();
                        //var_dump($list);
                     ?>
                      <select name="Clients[id_mdistributor]" required class="mdistributor-select" oninvalid="This field is required!">
                        <option value="NULL" default>None</option>
                        <?php
                            foreach($list as $mdistributor){
                                ?>
                                    <option value="<?php echo $mdistributor['id_cliente']?>"><?php echo $mdistributor['nombre_rsocial']?></option>
                                <?php
                            }
                        ?>
                    </select><span class="required_span">*</span>
                    
                </td>
                  
            </tr>
           <tr>
                <td colspan=2>
                    <input type="hidden" value=0 name="Clients[logo_en_certificado]" >
                    <label style="top: 14px;position: relative;">
                        Logo: (upload your company logo here)
                        <input name="ruta_logo" type="file" placeholder="">
                    </label>
                </td>
               
                
            </tr>

            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <br>
                    <br>
                    <p><strong>PERSONAL INFORMATION: <i class="info-i personal">i</i></strong>
                        <br>
                    </p>
                </td>
                <td>
                    <div class='information-text personal' style="display:none">
                        Fill out the information about de main administrator of the  
                        Site or TOAA (Total Access Administrator). 
                        <br>
                        This user is able to:
                        <p>● Manage user accounts (create, edit, delete)</p>
                        <p>● Allocate test licenses.</p>
                        <p>● Manage groups or test rooms (create, edit, delete) Purchase test licenses.</p>
                        <p>● Tutor access (Perform the speaking test and assign test licenses).</p>
                        <p>● Manage the test agenda.(set and modify exam dates)</p>
                        <p>● Approve or reject test takers.</p>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <select class="prefix" name="prefix">
                        <option value="">
                            Prefix
                        </option>
                        <option value="1">
                            Miss.
                        </option>
                        <option value="2">
                            Mr.
                        </option>
                        <option value="3">
                            Mrs.
                        </option>
                        <option value="4">
                            Sr.
                        </option>
                        
                    </select<span class="required_span">*</span>
                </td>
            </tr>
                    
            <tr>
                <td>
                    <input name="UsersC[nombres]" type="text" placeholder="Name *" required oninvalid="this.setCustomValidity('First Name is required!')" oninput="setCustomValidity('')">
                </td>
  
                <td>
                    <input name="UsersC[apellidos]" type="text" placeholder="Surname *" required oninvalid="this.setCustomValidity('Surname is required!')" oninput="setCustomValidity('')"></input>
                </td>
            </tr>
             <tr>
                <td>
                    <input name="JobTitle" type="text" placeholder="Job Title" oninvalid="this.setCustomValidity('Job title is required!')" oninput="setCustomValidity('')">
                </td>
                <td>
                     <input name="UsersC[email]" type="text" placeholder="Email *" required oninvalid="this.setCustomValidity('Email is required!')" oninput="setCustomValidity('')"></input>
                </td>
               
            </tr>
            
          
            <tr>
                <td>
                    <input onkeyup="ttUsername(); return false;" id="UsersC_numero_id" name="UsersC[numero_id]" type="text" placeholder="Username *" required oninvalid="this.setCustomValidity('Username is required!')" oninput="setCustomValidity('')">
                </td>
                <td>
                    <p id="id_validate"></p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <p class="alert_message" id="username_alert">Don't use blank and spaces. (Example usernames: "b.greg", "tom-c", "martha89").</p>
                </td>
            </tr>
            <tr>
                <td id="clave">
                     <input name="UsersC[clave]" type="password" placeholder="Password *" id="inp_clave" maxlength="200" size="60" required oninvalid="this.setCustomValidity('Password is required!')" oninput="setCustomValidity('')"></input>
                </td>
                <td id="clave2">
                     <input onkeyup="checkPass(); return false;" name="UsersC[clave2]" id="inp_clave2" type="password" placeholder="Verify Password *" maxlength="200" size="60" required oninvalid="this.setCustomValidity('Verify Password is required!')" oninput="setCustomValidity('')"></input>
                     <span id="confirmMessage" class="confirmMessage" style="font-weight:bold;"></span>
                </td>
            </tr>
            <tr>
                <td colspan="2"><p class="alert_message" id="pass_alert">Password must have letters, numbers and symbols.</p></td>
            </tr>
            <tr>
                
                <td colspan=3>
                   
                  <label style="top: 14px;position: relative;">
                    Your Photo: (upload your photo here)
                    <input name="ruta_foto" type="file" placeholder="" >
                  </label>
                </td>
            </tr>
            
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>How did you know about us?</td>
              
                <td>
                    <select name="howdidyouknow" required>
                        <option value="Facebook">Facebook</option>
                        <option value="Google search">Google search</option>
                        <option value="Magazine">Magazine</option>
                        <option value="TV add">TV add</option>
                        <option value="Newspaper">Newspaper</option>
                        <option value="Email">Email</option>
                        <option value="Twitter">Twitter</option>
                        <option value="Other Website">Other Website</option>
                        <option value="Education Events">Education Events</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>I would like to receive more information on the TECS and ILTO products or new releases.</td>
          
                <td>
                    <p>
                        <label>
                            <input type="radio" name="Wouldlike" value="Yes" checked> Yes
                        </label>
                        <label>
                            <input type="radio" name="Wouldlike" value="No"> No
                        </label>
                        <br>
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                    <input type="hidden" name="reference" value="<?php echo $_REQUEST['reference']?>"/>
                    <input type="submit" value="Submit"></input><br/><br/>
                    <a href="/terms.php" style="color: #069;text-decoration: underline;font-size: 16px;" target="_blank">Terms and Conditions</a> 
                </td>
            </tr>
        </table>
    </form>
</p>
<script>

$(document).ready(function(){
    $('#country-select').change(function() {
        
        loadCity($(this).find(':selected').val())
    })

})


function loadCity(countryId){
        $("#city-select").children().remove()
        $.ajax({
            type: "POST",
            url: "/ajax.php",
            data: "get=city&countryId=" + countryId
            }).done(function( result ) {
                $(result).each(function(i,j){
                    $("#city-select").append($('<option>', {
                        value: j.ID,
                        text: j.Name,
                    }));
                })
            });
}



jQuery('#confirmMessage').click(function(){
    checkPass()
})

$("#combo_perfil").change(function(){
        if($("#combo_perfil").val()=="6"){
           $('#clave').hide(); 
           $('#clave2').hide(); 
           document.getElementById('numero_id_lbl').innerHTML = 'Id number';
           
           
        } else {
          $('#clave').val("");
          $('#clave2').val("");
          $('#clave').show(); 
          $('#clave2').show();
          
          document.getElementById('numero_id_lbl').innerHTML = 'Username';        }
    }) 
    
function ttUsername()
{
 
    document.getElementById('username_alert').style.display="block";
    
}
    
    
    
 function checkPass()
{
    //Store the password field objects into variables ...
    var pass1 = document.getElementById('inp_clave').value;
    var pass2 = document.getElementById('inp_clave2').value;
    
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    document.getElementById('pass_alert').style.display="block";
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field
    //and the confirmation field
    if(pass1 == pass2){
      //The passwords match.
      //Set the color to the good color and inform
      //the user that they have entered the correct password
      //pass2.style.backgroundColor = goodColor;
      message.style.color = goodColor;
      message.innerHTML = "Passwords Match!";
      
      $('input[type="submit"]').prop('disabled', false);
    }else{
      //The passwords do not match.
      //Set the color to the bad color and
      //notify the user.
      //pass2.style.backgroundColor = badColor;
      message.style.color = badColor;
      message.innerHTML = "Passwords Do Not Match!";
      
      $('input[type="submit"]').prop('disabled', true);
    }
}

    jQuery('.info-i.personal').hover(function(){
        jQuery('.information-text.personal').show();
    }, function(){
        jQuery('.information-text.personal').hide();
    })
    
    jQuery('.info-i.porquedemo').hover(function(){
        jQuery('.information-text.demo').show();
    }, function(){
        jQuery('.information-text.demo').hide();
    })
    
    
   
    
    jQuery(document).on('keyup', '[name="UsersC[numero_id]"]', function(){
      var str = jQuery(this).val();
      str = str.replace(/[_\W]+/g, "");
      jQuery(this).val(str);
    })
    
    $('#nit_id_legal').bind('keypress', function (event) {
		var regex = new RegExp("^[a-zA-Z0-9]+$");
		var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
		if (!regex.test(key)) {
		    event.preventDefault();
			return false;
		}
	});
	
	$("#UsersC_numero_id").change(function() {
		var id = $("#UsersC_numero_id").val();
		$.ajax({
			url : "/ilto3/index.php?r=Api/username",
			type: "POST",
			dataType: "text",
			data: {
			    id: id
			},
			success: function(data) {
				$("#id_validate").html(data);
			}
		});
	});
    
</script>
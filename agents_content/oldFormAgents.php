<p>
    <form id="ff" action="form1_proc.php" method="post">
        <table width="597">
            <tr>
                <td>
                    <input type="hidden" name="typeform" value="tecsagent">
                    <input name="Organizationname" type="text" placeholder="Organization name" required>
                </td>
                <td>&nbsp;</td>
                <td>
                    <input name="EducationLevel" type="text" placeholder="Education Level" required></input>
                </td>
            </tr>
            <tr>
                <td>
                    <input name="Website" type="text" placeholder="Website" required>
                </td>
                <td>&nbsp;</td>
                <td>
                    <input name="Phonenumber" type="text" placeholder="Phone number" required></input>
                </td>
            </tr>
            <tr>
                <td>
                    <input name="City" type="text" placeholder="City" required>
                </td>
                <td>&nbsp;</td>
                <td>
                    <input name="Country" type="text" placeholder="Country" required>
                </td>
            </tr>

            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <p><strong>Contact Information:</strong>
                        <br>
                    </p>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <input name="FirstName" type="text" placeholder="First Name" required>
                </td>
                <td>&nbsp;</td>
                <td>
                    <input name="LastName" type="text" placeholder="Last Name" required></input>
                </td>
            </tr>
            <tr>
                <td>
                    <input name="PhoneNumber" type="text" placeholder="Phone Number" required>
                </td>
                <td>&nbsp;</td>
                <td>
                    <input name="Email" type="text" placeholder="Email" required></input>
                </td>
            </tr>
            <tr>
                <td>How did you know about us?</td>
                <td>&nbsp;</td>
                <td>
                    <select name="howdidyouknow" required>
                        <option value="Facebook">Facebook</option>
                        <option value="Google search">Google search</option>
                        <option value="Magazine">Magazine</option>
                        <option value="TV add">TV add</option>
                        <option value="Newspaper">Newspaper</option>
                        <option value="Email">Email</option>
                        <option value="Twitter">Twitter</option>
                        <option value="Other Website">Other Website</option>
                        <option value="Education Events">Education Events</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td> I would like to receive more information on ILTO or TECS products or new releases. </td>
                <td>&nbsp;</td>
                <td>
                    <p>
                        <label>
                            <input type="radio" name="Wouldlike" value="Yes"> Yes
                        </label>
                        <label>
                            <input type="radio" name="Wouldlike" value="No"> No
                        </label>
                        <br>
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                    <input type="submit" value="Submit"></input>
                </td>
            </tr>
        </table>
    </form>
</p>
              <p>&nbsp;</p>
              <p>The INTERNATIONAL LANGUAGE TESTING ORGANIZATION (ILTO) cares for its agents, thus provide them with all the academic and technical support needed to implement the TECS in the organization. In addition, our agents are provided with promotion material to get students acquainted with the test.</p>
              <p>&nbsp;</p>
              <p>Thinking on the reliability and security of the TECS results, our PREMIUM agents are provided with the basic implementation of a biometrics security system to avoid any kind of fraud. </p>
              <p>&nbsp;</p>
              <p>Depending on the amount of licenses purchased, you will have special discounts and exclusive offers and the test customization with the corporate image of your organization. </p>
              <p>&nbsp;</p>
              <p>Should you need further information, you may contact our international mailbox service at <br>
              305-853-8088. Your information request will be answered by one our agents according to the corresponding country and city in order to give you the personalized service you deserve.</p>
              <p>&nbsp;</p>
              <p>If your organization would like to have 5 test licenses, please provide the information below.</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p><strong>Education Organizations:</strong></p>
              <p>Register as a teacher or administrator of an education organization and receive 1 free TECS licenses that you can use in your institution. Fill out the form if you would like to do the test and receive more information. </p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>Remember that fields marked with * are mandatory. </p>
              <p>&nbsp;</p>
              <p><strong>Organization Information:</strong></p>
			
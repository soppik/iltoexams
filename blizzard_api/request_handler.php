<?php

namespace BlizzardApi;

define('REGION_US', 'us');
define('REGION_EU', 'eu');
define('REGION_KO', 'ko');
define('REGION_TW', 'tw');

define('STATIC_NAMESPACE', 'static');
define('DYNAMIC_NAMESPACE', 'dynamic');

use BlizzardApi\Cache\IApiCache;
use \Exception;

class ApiException extends Exception {};

class Request {
  # One minute cache
  const CACHE_MINUTE = 60;
  # One hour cache
  const CACHE_HOUR = 60 * self::CACHE_MINUTE;
  # One day cache
  const CACHE_DAY = 24 * self::CACHE_HOUR;
  # One week cache
  const CACHE_WEEK = self::CACHE_DAY * 7;
  # One (commercial) month cache
  const CACHE_MONTH = self::CACHE_DAY * 30;
  # Three (commercial) months cache
  const CACHE_TRIMESTER = self::CACHE_MONTH * 3;

  # Common endpoints
  const BASE_URLS = [
    'game_data' => 'https://%s.api.blizzard.com/data/%s',
    'community' => 'https://%s.api.blizzard.com/%s',
    'profile' => 'https://%s.api.blizzard.com/profile/%s',
    'media' => 'https://%s.api.blizzard.com/data/%s/media',
    'oauth/auth' => 'https://%s.battle.net/oauth/authorize',
    'oauth/token' => 'https://%s.battle.net/oauth/token'
  ];

  /**
   * @var $accessToken string Cached access token.
   */
  public $accessToken;

  /**
   * @var $region string API region
   */
  protected $region;

  /**
   * @var $game string Game name
   */
  protected $game;

  /**
   * @var $cache IApiCache
   */
  protected $cache;

  /**
   * @param $scope string API scope to apply the base URL
   * @return mixed Base URL to call endpoints
   */
  protected function baseUrl($scope) {
    return sprintf(self::BASE_URLS[$scope], $this->region, $this->game);
  }

  /**
   * @param $scope string The desired scope for the namespace: STATIC_NAMESPACE or DYNAMIC_NAMESPACE
   * @return string The appropriate namespace for the endpoint and region
   * @throws ApiException
   */
  protected function endpointNamespace($scope) {
    switch ($scope) {
      case 'dynamic':
      case 'static':
      case 'profile':
        return "$scope-$this->region";
        break;
      default:
        throw new ApiException('Invalid namespace scope');
    }
  }

  /**
   * Creates a valid url for authorizing a user using BNet OAuth2 provider for the current region.
   * @see https://develop.battle.net/documentation/guides/using-oauth/authorization-code-flow
   *
   * @param string $scope The desired OAuth2 scope.
   * @return string The url for the login button.
   */
  public function authorizationURL($scope = 'wow.profile') {
    $queryString = http_build_query([
      'auth_flow'		=> 'auth_code',
      'client_id' => Config::$apiKey,
      'scope' => $scope,
      'response_type' => 'code',
      'redirect_uri' => Config::$redirectURI
    ]);
    return sprintf("%s?%s", $this->baseUrl('oauth/auth'), $queryString);
  }

  /**
   * Used to create a new access token using the OAuth2
   * Api client and secret must be configured using the BlizzardAPi\Config class.
   *
   * If the argument $code is provided the authorization code flow will be used:
   * @see https://develop.battle.net/documentation/guides/using-oauth/authorization-code-flow
   *
   * If no $code argument is provided a access token will be created using the client credentials flow
   * @see https://develop.battle.net/documentation/guides/using-oauth/client-credentials-flow
   *
   * @param string|null $code An optional authorization code
   * @return string
   * @throws ApiException
   */
  public function createAccessToken($code = null) {
    if ($code !== null) {
      $postFields = [
        'grant_type' => 'authorization_code',
        'redirect_uri' => Config::$redirectURI,
        'code' => $code
      ];
    } else {
      $postFields = ['grant_type' => 'client_credentials'];
    }

    $curl_handle = curl_init();
    try {
      curl_setopt($curl_handle, CURLOPT_URL, $this->baseUrl('oauth/token'));
      curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $postFields);
      curl_setopt($curl_handle, CURLOPT_USERPWD, Config::$apiKey . ':' . Config::$apiSecret);
      curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl_handle, CURLOPT_HTTPHEADER, ['Content-Type: multipart/form-data']);
      $response = curl_exec($curl_handle);
      $status = curl_getinfo($curl_handle, CURLINFO_HTTP_CODE);

      if ($status !== 200) {
        throw new ApiException("Failed to create client_credentials access token. Code $status");
      }

      return json_decode($response)->access_token;
    } finally {
      curl_close($curl_handle);
    }
  }

  /**
   * @param $url string API endpoint full url with querystring params
   * @param $responseStatus int Response status
   * @return mixed stdClass JSON object response
   */
  public function execute($url, &$responseStatus) {
    try {
      $curl_handle = curl_init();
      try {
        curl_setopt($curl_handle, CURLOPT_URL, $url);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_handle, CURLOPT_HTTPHEADER, ["Authorization: Bearer $this->accessToken"]);
        $response = curl_exec($curl_handle);

        $responseStatus = (int) curl_getinfo($curl_handle, CURLINFO_HTTP_CODE);
      } finally {
        curl_close($curl_handle);
      }
    } finally {
      return $response;
    }
  }

  protected function apiRequest($url, $options = []) {
    $queryString = $this->extractQueryString($options);

    if ($queryString) {
      $url .= "?$queryString";
    }

    if ($this->cache && $this->cache->retrieve($url, $data)) {
      return json_decode($data);
    }
    $data = $this->execute($url, $responseCode);
    if ($this->cache && $responseCode === 200) {
      $ttl = isset($options['ttl']) ?  $options['ttl'] : 86400;
      $this->cache->store($url, $data, $ttl);
    }

    return json_decode($data);
  }

  protected function createSlug($name) {
    $name = strtolower($name);
    $name = str_replace(' ', '-', $name);
    return $name;
  }

  private function extractQueryString(&$options) {
    $defaultOptions = ['ttl' => 86400, 'region' => $this->region, $accessToken = $this->accessToken];
    $queryString = array_diff_key($options, $defaultOptions);
    $options = array_intersect_key($options, $defaultOptions);
    return http_build_query($queryString);
  }

  /**
   * Creates an interface for calling API Endpoints
   * @param $region string One of the supported API regions: REGION_US, REGION_EU, REGION_KO or REGION_TW
   * @param null $accessToken Allow to specify a access_token for the requests, useful for specifying a token obtained
   *   using authorization_code flow or a custom cache.
   * @param $cache IApiCache Cache interface
   * @throws ApiException In case a token cannot be obtained.
   */
  public function __construct($region, $accessToken = null, IApiCache $cache = null) {
    $this->region = $region;
    $this->cache = $cache;

    if ($accessToken) {
      # Using an externally created token
      $this->accessToken = $accessToken;
    } elseif (Config::$storeAccessTokenInSession && isset($_SESSION[Config::$accessTokenSessionKey])) {
      # Checking for a token in the session
      $this->accessToken = $_SESSION[Config::$accessTokenSessionKey];
    } else {
      # Creating a new client_credentials token
      $this->accessToken = $this->createAccessToken();
    }
  }
}
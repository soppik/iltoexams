<?php /** @noinspection PhpIncludeInspection */

namespace BlizzardApi\WoW;

use BlizzardApi\Cache\IApiCache;
use Exception;

require_once __DIR__ . '/request.php';
require_once __DIR__ . '/game_data/generic_endpoint.php';

class WoW {
  const community_endpoints = ['boss', 'auction', 'challenge_mode', 'pvp', 'quest', 'recipe', 'spell', 'zone', 'battlegroup', 'talents'];
  const game_data_endpoints = ['achievements', 'playable_class', 'playable_specialization', 'playable_race', 'power_type',
    'connected_realm', 'creatures', 'realm', 'wow_token', 'region', 'mount', 'pet', 'mythic_keystone_affix', 'title',
    'guild_crest', 'mythic_raid_leaderboard', 'item', 'pvp_tier', 'mythic_keystone', 'pvp_season', 'guild'];
  const profile_endpoints = ['character'];

  /**
   * @var string
   */
  private $defaultRegion;

  /**
   * @var IApiCache
   */
  public $cache;

  /**
   * Using PHP magic getter to lazy load classes
   * @param $endpointName
   * @return mixed
   * @throws
   */
  public function __get($endpointName) {
    if (in_array($endpointName, self::community_endpoints)) {
      require_once __DIR__ . "/community/$endpointName.php";
      $className = "\\BlizzardApi\\WoW\\Community\\{$this->camelize($endpointName)}";
    } elseif (in_array($endpointName, self::game_data_endpoints)) {
      require_once __DIR__ . "/game_data/$endpointName.php";
      $className = "\\BlizzardApi\\WoW\\GameData\\{$this->camelize($endpointName)}";
    } elseif (in_array($endpointName, self::profile_endpoints)) {
      $className = "\\BlizzardApi\\WoW\\Profile\\{$this->camelize($endpointName)}";
      require_once __DIR__ . "/profile/$endpointName.php";
    } else {
      throw new Exception("Invalid endpoint name $endpointName");
    }
    return new $className($this->defaultRegion, null, $this->cache);
  }

  public function __construct($region = 'us', IApiCache $cache = null) {
    $this->defaultRegion = $region;
    $this->cache = $cache;
  }

  private function camelize($className) {
    $words = explode('_', $className);
    foreach ($words as $index => $word) {
      $words[$index] = ucfirst($word);
    }
    return implode($words);
  }
}
<?php

namespace BlizzardApi\WoW\GameData;

class Item extends GenericDataEndpoint {

  /**
   * Return data from an item
   * @param $id integer Item ID
   * @param $useCommunityEndpoint bool If true the legacy community endpoint will be used
   * @param $options array Request options
   * @return mixed
   */
  public function get($id, $useCommunityEndpoint = false, $options = []) {
    if ($useCommunityEndpoint) {
      return $this->apiRequest("{$this->baseUrl('community')}/item/$id");
    }
    return parent::get($id);
  }

  /**
   * Return detailed set information along with item ID's of other items in the set
   * @param $id int Item Set ID
   * @return mixed
   */
  public function getItemSet($id) {
    return $this->apiRequest("{$this->baseUrl('community')}/item/set/$id");
  }

  /**
   * Returns an index of azerite essences
   * @param $options array Request options
   * @return mixed
   */
  public function azeriteEssences($options = []) {
    return $this->apiRequest("{$this->baseUrl('game_data')}/azerite-essence/index", $this->defaultOptions($options));
  }

  /**
   * Returns an azerite essence by ID
   * @param $id int Azerite Essence ID
   * @param $options array Request options
   * @return mixed
   */
  public function azeriteEssence($id, $options = []) {
    return $this->apiRequest("{$this->baseUrl('game_data')}/azerite-essence/$id", $this->defaultOptions($options));
  }

  /**
   * Returns an index of item classes
   * @param $useCommunityEndpoint bool If true the legacy community endpoint will be used
   * @param $options array Request options
   * @return mixed
   */
  public function itemClasses($useCommunityEndpoint = false, $options = []) {
    if ($useCommunityEndpoint) {
      return $this->apiRequest("{$this->baseUrl('community')}/data/item/classes");
    }
    return $this->apiRequest("{$this->endpointUri('class')}/index", $this->defaultOptions($options));
  }

  /**
   * Returns an item class by ID
   * @param $id int Item Class ID
   * @param $options array Request options
   * @return mixed
   */
  public function itemClass($id, $options = []) {
    return $this->apiRequest("{$this->endpointUri('class')}/$id", $this->defaultOptions($options));
  }

  /**
   * Returns an item subclass by ID and SubClass ID
   * @param $id int Item class ID
   * @param $subClassID int Item Subclass ID
   * @param $options array Request options
   * @return mixed
   */
  public function itemSubClass($id, $subClassID, $options = []) {
    return $this->apiRequest("{$this->endpointUri('class')}/$id/item-subclass/$subClassID", $this->defaultOptions($options));
  }

  /**
   * Returns media for an Item by ID
   * @param $id int Item ID
   * @param $options array Request options
   * @return mixed
   */
  public function itemMedia($id, $options = []) {
    return $this->apiRequest("{$this->baseUrl('media')}/$this->endpoint/$id", $this->defaultOptions($options));
  }

  protected function endpointSetup() {
    $this->namespace = $this->endpointNamespace('static');
    $this->ttl = self::CACHE_TRIMESTER;
    $this->endpoint = 'item';
  }
}
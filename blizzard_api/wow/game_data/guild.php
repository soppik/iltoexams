<?php

namespace BlizzardApi\WoW\GameData;

use BlizzardApi\WoW\Request;
use Error;

class Guild extends Request {

  const GUILD_FIELDS = array(
    'achievements',
    'achievementsCompleted',
    'achievementsCompletedTimestamp',
    'challenge',
    'criteria',
    'criteriaCreated',
    'criteriaQuantity',
    'criteriaTimestamp',
    'members',
    'news'
  );

  /**
   * Returns a list of all guild perks
   * @param $options array Request options
   * @return mixed
   */
  public function perks($options = []) {
    return $this->apiRequest("{$this->baseUrl('community')}/data/guild/perks", array_merge(['ttl' => self::CACHE_DAY], $options));
  }

  /**
   * Returns all guild rewards
   * @param $options array Request options
   * @return mixed
   */
  public function rewards($options = []) {
    return $this->apiRequest("{$this->baseUrl('community')}/data/guild/rewards", array_merge(['ttl' => self::CACHE_DAY], $options));
  }

  /**
   * Return information about a wow guild
   * @param $realmName string Realm name
   * @param $guildName string Guild name
   * @param bool $useCommunityEndpoint If true the legacy community endpoint will be used
   * @param $fields array A list of additional fields to include
   * @param $options array Request options
   * @return mixed
   */
  public function get($realmName, $guildName, $useCommunityEndpoint = false, $fields = [], $options = []) {
    if (!$useCommunityEndpoint) {
      return $this->guild_request($realmName, $guildName, $options);
    }

    $realmName = rawurlencode($realmName);
    $guildName = rawurlencode($guildName);
    return $this->apiRequest("{$this->baseUrl('community')}/guild/$realmName/$guildName", array_merge(['fields' => implode(',', $fields), 'ttl' => self::CACHE_DAY], $options));
  }

  /**
   * Return information about a wow guild roster
   * @param $realmName string Realm name
   * @param $guildName string Guild name
   * @param $options array Request options
   * @return mixed
   */
  public function roster($realmName, $guildName, $options = []) {
    return $this->guild_request($realmName, $guildName, 'roster', $options);
  }

  /**
   * Return information about a wow guild
   * @param $realmName string Realm name
   * @param $guildName string Guild name
   * @param $useCommunityEndpoint boolean If true community endpoint will be used
   * @param $options array Request options
   * @return mixed
   */
  public function achievements($realmName, $guildName, $useCommunityEndpoint = false, $options = []) {
    if ($useCommunityEndpoint) {
      return $this->apiRequest("{$this->baseUrl('community')}/data/guild/achievements", array_merge(['ttl' => self::CACHE_DAY], $options));
    }
    return $this->guild_request($realmName, $guildName, 'achievements', $options);
  }

  private function guild_request($realmName, $guildName, $variant = null, $options = []) {
    $realmName = rawurlencode($this->createSlug($realmName));
    $guildName = rawurlencode($this->createSlug($guildName));
    $url = "{$this->baseUrl('game_data')}/guild/$realmName/$guildName";
    if ($variant) {
      $url .= "/$variant";
    }
    return $this->apiRequest($url, array_merge(['namespace' => $this->endpointNamespace('profile'), 'ttl' => self::CACHE_DAY], $options));
  }

  /**
   * @param $fields
   * @return bool
   */
  private function validateOptions($fields) {
    foreach ($fields as $field) {
      if (!in_array($field, self::GUILD_FIELDS)) {
        throw new Error('Invalid field.');
      }
    }
  }
}
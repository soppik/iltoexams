<?php

namespace BlizzardApi\WoW\GameData;

use Error;

class Pet extends GenericDataEndpoint {
  /**
   * Returns a list containing all battle and vanity pets
   * @param $useCommunityEndpoint bool If true the legacy community endpoint will be used
   * @return mixed
   */
  public function index($useCommunityEndpoint = false) {
    if ($useCommunityEndpoint) {
      return $this->apiRequest("{$this->baseUrl('community')}/pet/");
    }
    return parent::index();
  }

  /**
   * Returns data about an individual battle pet ability ID
   * @param $id int Battle Pet Ability ID
   * @return mixed
   */
  public function ability($id) {
    return $this->apiRequest("{$this->baseUrl('community')}/pet/ability/$id");
  }

  /**
   * Returns data about an individual pet species
   * @param $id int Battle Pet Species ID
   * @return mixed
   */
  public function species($id) {
    return $this->apiRequest("{$this->baseUrl('community')}/pet/species/$id");
  }

  /**
   * Returns data about an individual pet stats based on species
   * @param $speciesId int Pet Species ID
   * @param $level int Pet level, 1 - 25. If omitted, default = 1.
   * @param $breedId int Pet Breed, 3 - 22. If omitted, default = 3.
   * @param $qualityId int Pet quality, 0 - 5 (0 = poor quality and 5 = legendary). If omitted, default = 1.
   * @return mixed
   */
  public function getStats($speciesId, $level = 1, $breedId = 3, $qualityId = 1) {
    if ($level < 1 || $level > 25) {
      throw new Error('Invalid Pet Level');
    } elseif ($breedId < 3 || $breedId > 22) {
      throw new Error('Invalid Pet Breed ID');
    } elseif ($qualityId < 0 || $qualityId > 5) {
      throw new Error('Invalid Pet Quality ID');
    }
    return $this->apiRequest("{$this->baseUrl('community')}/pet/stats/$speciesId", ['level' => $level, 'breedId' => $breedId, 'qualityId' => $qualityId]);
  }

  /**
   * Returns data about pet types
   * @return mixed
   */
  public function types() {
    return $this->apiRequest("{$this->baseUrl('community')}/data/pet/types");
  }

  protected function endpointSetup() {
    $this->namespace = $this->endpointNamespace('static');
    $this->endpoint = 'pet';
  }
}
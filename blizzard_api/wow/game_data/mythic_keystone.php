<?php

namespace BlizzardApi\WoW\GameData;

use Error;

class MythicKeystone extends GenericDataEndpoint {
  public function get($id, $options = []) {
    throw new Error('This endpoint does not have a get method.');
  }

  public function dungeons($options = []) {
    return $this->apiRequest("{$this->endpointUri()}/dungeon/index", $this->defaultOptions($options));
  }

  public function dungeon($id, $options = []) {
    return $this->apiRequest("{$this->endpointUri()}/dungeon/$id", $this->defaultOptions($options));
  }

  public function periods($options = []) {
    return $this->apiRequest("{$this->endpointUri()}/period/index", $this->defaultOptions($options));
  }

  public function period($id, $options = []) {
    return $this->apiRequest("{$this->endpointUri()}/period/$id", $this->defaultOptions($options));
  }

  public function seasons($options = []) {
    return $this->apiRequest("{$this->endpointUri()}/season/index", $this->defaultOptions($options));
  }

  public function season($id, $options = []) {
    return $this->apiRequest("{$this->endpointUri()}/season/$id", $this->defaultOptions($options));
  }

  protected function endpointSetup() {
    $this->namespace = $this->endpointNamespace('dynamic');
    $this->ttl = self::CACHE_WEEK;
    $this->endpoint = 'mythic-keystone';
  }
}
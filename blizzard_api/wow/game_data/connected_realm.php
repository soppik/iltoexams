<?php

namespace BlizzardApi\WoW\GameData;

class ConnectedRealm extends GenericDataEndpoint {
  protected function endpointSetup() {
    $this->namespace = $this->endpointNamespace('dynamic');
    $this->ttl = self::CACHE_MONTH;
    $this->endpoint = 'connected-realm';
  }
}
<?php

namespace BlizzardApi\WoW\GameData;

use Error;

class PvPTier extends GenericDataEndpoint {
  public function get($id, $options = []) {
    throw new Error('This endpoint does not have a get method.');
  }

  public function pvpTiers() {
    return $this->apiRequest("{$this->endpointUri()}/index", ['namespace' => $this->namespace]);
  }

  public function pvpTier($id) {
    return $this->apiRequest("{$this->endpointUri()}/$id", ['namespace' => $this->namespace]);
  }

  public function pvpTierMedia($id) {
    return $this->apiRequest("{$this->baseUrl('media')}/$this->endpoint/$id", ['namespace' => $this->namespace]);
  }


  protected function endpointSetup() {
    $this->namespace = $this->endpointNamespace('static');
    $this->ttl = self::CACHE_TRIMESTER;
    $this->endpoint = 'pvp-tier';
  }

}
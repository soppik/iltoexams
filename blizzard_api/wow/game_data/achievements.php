<?php

namespace BlizzardApi\WoW\GameData;

class Achievements extends GenericDataEndpoint{
  /**
   * Returns data from a WoW achievement by ID
   * @param integer id Achievement ID
   * @param bool $useCommunityEndpoint If true the legacy community endpoint will be used
   * @param $options array Request options
   * @return mixed
   */
  public function get($id, $useCommunityEndpoint = false, $options = []) {
    if ($useCommunityEndpoint) {
      return parent::get($id);
    }
    return $this->apiRequest("{$this->baseUrl('community')}/achievement/$id", $this->defaultOptions($options));
  }

  /**
   * Returns an index of achievement categories
   * @param $options array Request options
   * @return mixed
   */
  public function categories($options = []) {
    return $this->apiRequest("{$this->endpointUri('category')}/index", $this->defaultOptions($options));
  }

  /**
   * Returns an Achievement Category by ID
   * @param $id int Achievement ID
   * @param $options array Request options
   * @return mixed
   */
  public function category($id, $options = []) {
    return $this->apiRequest("{$this->endpointUri('category')}/$id", $this->defaultOptions($options));
  }

  /**
   * Returns media for an Achievement by ID
   * @param $id int Achievement ID
   * @param $options array Request options
   * @return mixed
   */
  public function achievementMedia($id, $options = []) {
    return $this->apiRequest("{$this->baseUrl('game_data')}/media/achievement/$id", $this->defaultOptions($options));
  }

  protected function endpointSetup($options = []) {
    $this->namespace = $this->endpointNamespace('static');
    $this->ttl = self::CACHE_TRIMESTER;
    $this->endpoint = 'achievement';
  }
}
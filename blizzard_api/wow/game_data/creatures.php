<?php

namespace BlizzardApi\WoW\GameData;
use Error;

class Creatures extends GenericDataEndpoint {
  public function index($options = []) {
    throw new Error("Creatures endpoint doesn't have a index method.", $this->defaultOptions($options));
  }

  /**
   * Returns an index of Creature Families
   * @param $options array Request options
   * @return mixed
   */
  public function families($options = []) {
    return $this->apiRequest("{$this->endpointUri('family')}/index", $this->defaultOptions($options));
  }

  /**
   * Returns a creature family by ID
   * @param $id int creature family ID
   * @param $options array Request options
   * @return mixed
   */
  public function family($id, $options = []) {
    return $this->apiRequest("{$this->endpointUri('family')}/$id", $this->defaultOptions($options));
  }

  /**
   * Returns a creature family media by ID
   * @param $id integer Creature family ID
   * @param $options array Request options
   * @return mixed
   */
  public function creatureFamilyMedia($id, $options = []) {
    return $this->apiRequest("{$this->baseUrl('media')}/creature-family/$id", $this->defaultOptions($options));
  }

  /**
   * Returns an index of Creature Types
   * @param $options array Request options
   * @return mixed
   */
  public function types($options = []) {
    return $this->apiRequest("{$this->endpointUri('type')}/index", $this->defaultOptions($options));
  }

  /**
   * Returns a creature type by ID
   * @param $id int creature type ID
   * @param $options array Request options
   * @return mixed
   */
  public function type($id, $options = []) {
    return $this->apiRequest("{$this->endpointUri('type')}/$id", $this->defaultOptions($options));
  }

  public function creatureDisplayMedia($id, $options = []) {
    return $this->apiRequest("{$this->baseUrl('media')}/creature-display/$id", $this->defaultOptions($options));
  }

  protected function endpointSetup() {
    $this->namespace = $this->endpointNamespace('static');
    $this->ttl = self::CACHE_TRIMESTER;
    $this->endpoint = 'creature';
  }
}
<?php

namespace BlizzardApi\WoW\GameData;

class Realm extends GenericDataEndpoint {
  /**
   * Returns a list of realm status information depending on the state of the realm
   * @param $realms array returns status of an individual realm
   * @param $options array Request options
   * @return mixed
   */
  public function status($realms = [], $options = []) {
    if (count($realms) > 0) {
      foreach ($realms as $index => $realm) {
        $realms[$index] = $this->createSlug($realm);
      }
      $options['realms'] = rawurlencode(implode(',', $realms));
    }
    return $this->apiRequest("{$this->baseUrl('community')}/realm/status", $this->defaultOptions($options));
  }

  public function getRealmBySlug($realm, $options = []){
    $realmBySlug = $this->createSlug($realm);
    return $this->apiRequest("{$this->baseUrl('game_data')}/realm/$realmBySlug", $this->defaultOptions($options));
  }

  protected function endpointSetup() {
    $this->namespace = $this->endpointNamespace('dynamic');
    $this->ttl = self::CACHE_TRIMESTER;
    $this->endpoint = 'realm';
  }
}
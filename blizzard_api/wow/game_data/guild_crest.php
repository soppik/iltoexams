<?php

namespace BlizzardApi\WoW\GameData;
use Error;

class GuildCrest extends GenericDataEndpoint {
  public function get($id, $option = []) {
    throw new Error('This endpoint does not have a get method.');
  }

  /**
   * Returns media for a guild crest border by ID
   * @param $id int Guild Crest Border ID
   * @param $options array Request options
   * @return mixed
   */
  public function borderMedia($id, $options = []) {
    return $this->apiRequest("{$this->baseUrl('media')}/$this->endpoint/border/$id", $this->defaultOptions($options));
  }


  /**
   * Returns media for a guild crest emblem by ID
   * @param $id int Guild Crest Emblem ID
   * @param $options array Request options
   * @return mixed
   */
  public function emblemMedia($id, $options = []) {
    return $this->apiRequest("{$this->baseUrl('media')}/$this->endpoint/emblem/$id", $this->defaultOptions($options));
  }

  protected function endpointSetup() {
    $this->namespace = $this->endpointNamespace('static');
    $this->ttl = self::CACHE_TRIMESTER;
    $this->endpoint = 'guild-crest';
  }
}
<?php

namespace BlizzardApi\WoW\GameData;

class PlayableRace extends GenericDataEndpoint {
  /**
   * Returns a list of races and their faction, name, unique ID and skin
   * @param $useCommunityEndpoint boolean If true community endpoint will be used
   * @return mixed
   */
  public function index($useCommunityEndpoint = false) {
    if ($useCommunityEndpoint) {
      return $this->apiRequest("{$this->baseUrl('community')}/data/character/races");
    }
    return parent::index();
  }

  protected function endpointSetup() {
    $this->namespace = $this->endpointNamespace('static');
    $this->endpoint = 'playable-race';
  }
}
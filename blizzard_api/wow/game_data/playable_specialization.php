<?php

namespace BlizzardApi\WoW\GameData;

class PlayableSpecialization extends GenericDataEndpoint {
  protected function endpointSetup() {
    $this->namespace = $this->endpointNamespace('static');
    $this->ttl = self::CACHE_TRIMESTER;
    $this->endpoint = 'playable-specialization';
  }
}
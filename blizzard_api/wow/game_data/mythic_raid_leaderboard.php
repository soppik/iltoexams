<?php

namespace BlizzardApi\WoW\GameData;

use BlizzardApi\WoW\Request;

class MythicRaidLeaderboard extends Request {
  /**
   * Returns the leaderboard for a given raid and faction
   * @param $raid string name of the raid
   * @param $faction string player faction (horde or alliance)
   * @param $options array Request options
   * @return mixed
   * @throws
   */
  public function get($raid, $faction, $options = []) {
    return $this->apiRequest("{$this->baseUrl('game_data')}/leaderboard/hall-of-fame/$raid/$faction", array_merge(['namespace' => $this->endpointNamespace('dynamic'), 'ttl' => self::CACHE_DAY], $options));
  }
}
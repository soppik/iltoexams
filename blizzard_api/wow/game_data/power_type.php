<?php

namespace BlizzardApi\WoW\GameData;

class PowerType extends GenericDataEndpoint  {
  protected function endpointSetup() {
    $this->namespace = $this->endpointNamespace('static');
    $this->ttl = self::CACHE_TRIMESTER;
    $this->endpoint = 'power-type';
  }
}
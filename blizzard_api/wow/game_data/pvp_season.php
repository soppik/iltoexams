<?php

namespace BlizzardApi\WoW\GameData;

use Error;

class PvPSeason extends GenericDataEndpoint {
  public function get($id, $options = []) {
    throw new Error('This endpoint does not have a get method.');
  }

  public function pvpSeason($id) {
    return $this->apiRequest("{$this->endpointUri()}/$id", ['namespace' => $this->namespace]);
  }

  public function pvpLeaderboards($id) {
    return $this->apiRequest("{$this->endpointUri()}/$id/pvp-leaderboard/index", ['namespace' => $this->namespace]);
  }

  public function pvpLeaderboard($id, $bracket) {
    return $this->apiRequest("{$this->endpointUri()}/$id/pvp-leaderboard/$bracket", ['namespace' => $this->namespace]);
  }

  public function pvpRewards($id) {
    return $this->apiRequest("{$this->endpointUri()}/$id/pvp-reward/index", ['namespace' => $this->namespace]);
  }

  protected function endpointSetup() {
    $this->namespace = $this->endpointNamespace('dynamic');
    $this->ttl = self::CACHE_WEEK;
    $this->endpoint = 'pvp-season';
  }
}
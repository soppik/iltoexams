<?php

namespace BlizzardApi\WoW\GameData;

class Mount extends GenericDataEndpoint {
  /**
   * Returns a list containing all supported mounts
   * @param $useCommunityEndpoint bool If true the legacy community endpoint will be used
   * @param $options array Request options
   * @return mixed|void
   */
  public function index($useCommunityEndpoint = false, $options = []) {
    if ($useCommunityEndpoint) {
      return $this->apiRequest("{$this->baseUrl('community')}/mount/", $this->defaultOptions($options));
    }
    return parent::index($options);
  }

  protected function endpointSetup() {
    $this->namespace = $this->endpointNamespace('static');
    $this->endpoint = 'mount';
  }
}
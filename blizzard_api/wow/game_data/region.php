<?php

namespace BlizzardApi\WoW\GameData;

class Region extends GenericDataEndpoint {
  protected function endpointSetup() {
    $this->namespace = $this->endpointNamespace('dynamic');
    $this->ttl = self::CACHE_TRIMESTER;
    $this->endpoint = 'region';  }
}
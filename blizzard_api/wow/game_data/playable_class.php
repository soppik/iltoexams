<?php

namespace BlizzardApi\WoW\GameData;

class PlayableClass extends GenericDataEndpoint {
  /**
   * Return a list of playable classes
   * @param $useCommunityEndpoint boolean If true community endpoint will be used
   * @return mixed
   */
  public function index($useCommunityEndpoint = false) {
    if ($useCommunityEndpoint) {
      return $this->apiRequest("{$this->baseUrl('community')}/data/character/classes");
    }
    return parent::index();
  }

  /**
   * Returns the PVP Talent slots for a playable class by ID
   * @param $id int Playable Class ID
   * @return mixed
   */
  public function pvpTalentSlots($id) {
    return $this->apiRequest("{$this->baseUrl('game_data')}/playable-class/$id/pvp-talent-slots", ['namespace'=> 'static-'.$this->region]);
  }

  protected function endpointSetup() {
    $this->namespace = $this->endpointNamespace('static');
    $this->endpoint = 'playable-class';
  }
}
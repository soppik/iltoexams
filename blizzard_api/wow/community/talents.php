<?php

namespace BlizzardApi\WoW\Community;

use BlizzardApi\WoW\Request;

class Talents extends Request {

  /**
   * Returns a list of all talents, specs and glyphs for each class
   * @param $options array Request options
   * @return mixed
   */
  public function index($options = []) {
    return $this->apiRequest("{$this->baseUrl('community')}/data/talents", array_merge(['ttl' => self::CACHE_TRIMESTER], $options));
  }

}
<?php

namespace BlizzardApi\WoW\Community;

use BlizzardApi\WoW\Request;

class Auction extends Request {

  /**
   * Return a link to a json file with auction data
   * @param $realmName string Realm Name
   * @param $options array Request options
   * @return mixed
   */
  public function get($realmName, $options = []) {
    $realmName = rawurlencode($realmName);
    return $this->apiRequest("{$this->baseUrl('community')}/auction/data/$realmName", array_merge(['ttl' => 15 * self::CACHE_MINUTE], $options));
  }
}
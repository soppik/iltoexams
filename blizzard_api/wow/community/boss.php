<?php

namespace BlizzardApi\WoW\Community;

use BlizzardApi\WoW\Request;

class Boss extends Request {

  /**
   * Returns a list containing all WoW instance bosses
   * @param $options array Request options
   * @return mixed
   */
  public function index($options = []) {
    return $this->apiRequest("{$this->baseUrl('community')}/boss/", array_merge(['ttl' => self::CACHE_TRIMESTER], $options));
  }

  /**
   * Return information about a wow boss by its id
   * @param $id int Boss id
   * @param $options array Request options
   * @return mixed
   */
  public function get($id, $options = []) {
    return $this->apiRequest("{$this->baseUrl('community')}/boss/$id", array_merge(['ttl' => self::CACHE_TRIMESTER], $options));
  }
}
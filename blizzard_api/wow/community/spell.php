<?php

namespace BlizzardApi\WoW\Community;

use BlizzardApi\WoW\Request;

class Spell extends Request {

  /**
   * Returns information about spells
   * @param $id int Spell ID
   * @param $options array Request options
   * @return mixed
   */
  public function get($id, $options = []) {
    return $this->apiRequest("{$this->baseUrl('community')}/spell/$id", array_merge(['ttl' => self::CACHE_TRIMESTER], $options));
  }
}
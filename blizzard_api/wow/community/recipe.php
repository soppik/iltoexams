<?php

namespace BlizzardApi\WoW\Community;

use BlizzardApi\WoW\Request;

class Recipe extends Request {

  /**
   * Returns basic recipe information
   * @param $id int Recipe ID
   * @param $options array Request options
   * @return mixed
   */
  public function get($id, $options = []) {
    return $this->apiRequest("{$this->baseUrl('community')}/recipe/$id", array_merge(['ttl' => self::CACHE_TRIMESTER], $options));
  }
}
<?php

namespace BlizzardApi\WoW\Community;

use BlizzardApi\WoW\Request;

class Battlegroup extends Request {

  /**
   * Returns a list of battlegroups for a specific region
   * @param $options array Request options
   * @return mixed
   */
  public function index($options = []) {
    return $this->apiRequest("{$this->baseUrl('community')}/data/battlegroups/", array_merge(['ttl' => self::CACHE_TRIMESTER], $options));
  }
}
<?php

namespace BlizzardApi\WoW\Community;

use BlizzardApi\WoW\Request;

class ChallengeMode extends Request {
  /**
   * Return Realm Leaderboard for Challenge Modes
   * @param $realmName string Realm Name
   * @param $options array Request options
   * @return mixed
   */
  public function realmIndex($realmName, $options = []) {
    $realmName = rawurlencode($realmName);
    return $this->apiRequest("{$this->baseUrl('community')}/challenge/$realmName", array_merge(['ttl' => self::CACHE_TRIMESTER], $options));
  }

  /**
   * Return top 100 results for Region Leaderboard Challenge Modes
   * @param $options array Request options
   * @return mixed
   */
  public function regionIndex($options = []) {
    return $this->apiRequest("{$this->baseUrl('community')}/challenge/region", array_merge(['ttl' => self::CACHE_TRIMESTER], $options));
  }
}
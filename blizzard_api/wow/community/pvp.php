<?php

namespace BlizzardApi\WoW\Community;

use BlizzardApi\WoW\Request;
use Error;

class PvP extends Request {
  const PVP_BRACKETS = ['2v2', '3v3', 'rbg'];

  /**
   * Returns leaderboard information for 2v2, 3v3 and Rated Battleground
   * @param $bracket string valid options are 2v2, 3v3 or rbg
   * @param $options array Request options
   * @return mixed
   */
  public function get($bracket, $options = []) {
    if (!in_array($bracket, self::PVP_BRACKETS)) {
      throw new Error('Invalid PVP Bracket');
    }
    return $this->apiRequest("{$this->baseUrl('community')}/leaderboard/$bracket", array_merge(['ttl' => self::CACHE_WEEK], $options));
  }
}
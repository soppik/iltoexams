<?php

namespace BlizzardApi\WoW\Community;

use BlizzardApi\WoW\Request;

class Zone extends Request {

  /**
   * Returns a list containing all Dungeon and Raid Zones including bosses
   * @param $options array Request options
   * @return mixed
   */
  public function index($options = []) {
    return $this->apiRequest("{$this->baseUrl('community')}/zone/", array_merge(['ttl' => self::CACHE_TRIMESTER], $options));
  }

  /**
   * Return information about a wow zone (Dungeon or Raid) by its id
   * @param $id int Zone id
   * @param $options array Request options
   * @return mixed
   */
  public function get($id, $options = []) {
    return $this->apiRequest("{$this->baseUrl('community')}/zone/$id", array_merge(['ttl' => self::CACHE_TRIMESTER], $options));
  }
}
<?php

namespace BlizzardApi\WoW\Community;

use BlizzardApi\WoW\Request;

class Quest extends Request {

  /**
   * Returns metadata for a specific quest
   * @param $id int Quest ID
   * @param $options array Request options
   * @return mixed
   */
  public function get($id, $options = []) {
    return $this->apiRequest("{$this->baseUrl('community')}/quest/$id", array_merge(['ttl' => self::CACHE_TRIMESTER], $options));
  }
}
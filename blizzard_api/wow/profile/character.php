<?php

namespace BlizzardApi\WoW\Profile;

use BlizzardApi\WoW\Request;
use Error;

class Character extends Request {
  const CHARACTER_FIELDS = array(
    'achievements',
    'appearance',
    'audit',
    'feed',
    'guild',
    'hunterPets',
    'items',
    'mounts',
    'pets',
    'petSlots',
    'professions',
    'progression',
    'pvp',
    'quests',
    'reputation',
    'statistics',
    'stats',
    'talents',
    'titles'
  );

  /**
   * Return information about a wow character
   * @param $realmName string Realm name
   * @param $characterName string Character name
   * @param $fields array A list of additional fields to include (Only for commnunity endpoint)
   * @param $useCommunityEndpoint boolean If true community endpoint will be used
   * @param $options array Request options
   * @return mixed
   */
  public function get($realmName, $characterName, $fields = [], $useCommunityEndpoint = false, $options = []) {
    if (!$useCommunityEndpoint) {
      return $this->character_request($realmName, $characterName, null, $options);
    }

    $realmName = rawurlencode($realmName);
    $characterName = rawurlencode($characterName);
    $this->validateOptions($fields);
    return $this->apiRequest("{$this->baseUrl('community')}/character/$realmName/$characterName", array_merge(['fields' => implode(',', $fields)], $options));
  }

  public function achievements($realmName, $characterName, $useCommunityEndpoint = false, $options = []) {
    if ($useCommunityEndpoint) {
      return $this->apiRequest("{$this->baseUrl('community')}/data/character/achievements", array_merge(['ttl' => self::CACHE_DAY], $options));
    }
    return $this->character_request($realmName, $characterName, 'achievements', $options);
  }

  public function appearance($realmName, $characterName, $options = []) {
    return $this->character_request($realmName, $characterName, 'appearance', $options);
  }

  public function equipment($realmName, $characterName, $options = []) {
    return $this->character_request($realmName, $characterName, 'equipment', $options);
  }

  public function media($realmName, $characterName, $options = []) {
    return $this->character_request($realmName, $characterName, 'character-media', $options);
  }

  public function pvpBracket($realmName, $characterName, $bracket, $options = []) {
    if (preg_match('/^(2v2|3v3|rbg)$/', $bracket) === 0) {
      throw new Error('Invalid bracket.');
    }
    return $this->character_request($realmName, $characterName, "pvp-bracket/$bracket", $options);
  }

  public function pvpSummary($realmName, $characterName, $options = []) {
    return $this->character_request($realmName, $characterName, 'pvp-summary', $options);
  }

  public function specializations($realmName, $characterName, $options = []) {
    return $this->character_request($realmName, $characterName, 'specializations', $options);
  }

  public function statistics($realmName, $characterName, $options = []) {
    return $this->character_request($realmName, $characterName, 'statistics', $options);
  }

  public function titles($realmName, $characterName, $options = []) {
    return $this->character_request($realmName, $characterName, 'titles', $options);
  }

  public function keystoneProfile($realmName, $characterName, $options = []) {
    return $this->character_request($realmName, $characterName, 'mythic-keystone-profile', $options);
  }

  public function keystoneSeasonDetails($realmName, $characterName, $id, $options = []) {
    return $this->character_request($realmName, $characterName, "mythic-keystone-profile/season/$id", $options);
  }

  private function validateOptions($fields) {
    foreach ($fields as $field) {
      if (!in_array($field, self::CHARACTER_FIELDS)) {
        throw new Error('Invalid field.');
      }
    }
  }


  private function character_request($realmName, $characterName, $variant = null, $options = []) {
    $realmName = rawurlencode($this->createSlug($realmName));
    $characterName = rawurlencode($this->createSlug($characterName));
    $url = "{$this->baseUrl('profile')}/character/$realmName/$characterName";
    if ($variant) {
      $url .= "/$variant";
    }
    return $this->apiRequest($url, array_merge($options, ['namespace' => $this->endpointNamespace('profile')]));
  }
}
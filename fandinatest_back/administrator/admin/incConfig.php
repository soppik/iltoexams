<?php
	$adminConfig['adminUsername']='admin';
	$adminConfig['adminPassword']='cd71ff45a6b502d8ffdaa51dd27c4e56';
	$adminConfig['notifyAdminNewMembers']=0;
	$adminConfig['defaultSignUp']=1;
	$adminConfig['anonymousGroup']='anonymous';
	$adminConfig['anonymousMember']='guest';
	$adminConfig['groupsPerPage']=10;
	$adminConfig['membersPerPage']=10;
	$adminConfig['recordsPerPage']=10;
	$adminConfig['custom1']='Full Name';
	$adminConfig['custom2']='Address';
	$adminConfig['custom3']='City';
	$adminConfig['custom4']='State';
	$adminConfig['MySQLDateFormat']='%c/%e/%Y';
	$adminConfig['PHPDateFormat']='n/j/Y';
	$adminConfig['PHPDateTimeFormat']='m/d/Y, h:i a';
	$adminConfig['senderName']='Membership management';
	$adminConfig['senderEmail']='admin@www.iltoexams.com';
	$adminConfig['approvalMessage']="Dear member,\r\n\r\nYour membership is now approved by the admin. You can log in to your account here:\r\nhttp://www.iltoexams.com/newtestistrator\r\n\r\nRegards,\r\nAdmin";
	$adminConfig['approvalSubject']='Your membership is now approved';
	?>
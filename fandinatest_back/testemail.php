<?php 



require_once("dompdf/dompdf_config.inc.php"); 
$html = 
  '
  
  <table width="100%" border="0" style="border:#FC0 1px solid; background-position:center; background-repeat:no-repeat; background-position:" background="backgroundtecs.jpg">
  <tr>
    <td height="700" valign="top">
    
    <table width="98%" border="0" cellpadding="0" cellspacing="0" style="border:#FC0 1px solid" align="center">
  <tr>
    <td  bgcolor="#F2F2F2"><img src="logotecs.jpg" width="233" height="110" style="margin:5px"></td>
    <td  height="95" bgcolor="#F2F2F2" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size:26px; font-weight:bold" >SCORE REPORT FORM </td>
  </tr>
</table>
    <br>
    <table width="98%" border="0" cellpadding="0" cellspacing="0" style="border:#FC0 1px solid" align="center">
  <tr>
    <td width="23%"  bgcolor="#FFFFFF"><strong>Test Administrator</strong></td>
    <td width="1%"  bgcolor="#F2F2F2">&nbsp;</td>
    <td width="43%"  bgcolor="#F2F2F2">Fundacion Universitaria del Area</td>
    <td width="2%"  bgcolor="#FFFFFF">&nbsp;</td>
    <td width="13%"  bgcolor="#FFFFFF"><strong>Adm. ID</strong></td>
    <td width="1%"  bgcolor="#F2F2F2">&nbsp;</td>
    <td width="17%"  bgcolor="#F2F2F2">28062011</td>
  </tr>
  <tr>
    <td  bgcolor="#FFFFFF"><strong>Test date</strong></td>
    <td  bgcolor="#F2F2F2">&nbsp;</td>
    <td  bgcolor="#F2F2F2">January 25, 2011</td>
    <td  bgcolor="#FFFFFF">&nbsp;</td>
    <td  bgcolor="#FFFFFF"><strong>Country</strong></td>
    <td  bgcolor="#F2F2F2">&nbsp;</td>
    <td  bgcolor="#F2F2F2">Colombia</td>
  </tr>
    </table>
    <br>
    <center>
      <strong>TEST TAKER INFORMATION </strong>
    </center><br>
<table width="98%" border="0" cellpadding="0" cellspacing="0" style="border:#FC0 1px solid" align="center">
  <tr>
    <td width="22%"  bgcolor="#FFFFFF"><strong>Surname</strong></td>
    <td width="2%"  bgcolor="#F2F2F2">&nbsp;</td>
    <td width="38%"  bgcolor="#F2F2F2">Viancha Cortes</td>
    <td width="2%"  bgcolor="#FFFFFF">&nbsp;</td>
    <td width="19%"  bgcolor="#FFFFFF"><strong>ID</strong></td>
    <td width="17%"  bgcolor="#F2F2F2">1110493458</td>
  </tr>
  <tr>
    <td  bgcolor="#FFFFFF"><strong>First Name</strong></td>
    <td  bgcolor="#F2F2F2">&nbsp;</td>
    <td  bgcolor="#F2F2F2">Juan Camilo</td>
    <td  bgcolor="#FFFFFF">&nbsp;</td>
    <td  bgcolor="#FFFFFF"><strong>First Language</strong></td>
    <td  bgcolor="#F2F2F2">Spanish </td>
  </tr>
    </table>
<br><center>
  <strong>TEST RESULTS
  </strong>
</center>
</strong><br>
<table align="center" width="65%" border="0" cellpadding="0" cellspacing="0" style="border:#FC0 1px solid" >
  <tr>
    <td colspan="3"  bgcolor="#FFFFFF" align="center"><strong>MODULE</strong></td>
    <td  bgcolor="#FFFFFF">&nbsp;</td>
    <td  bgcolor="#FFFFFF"><strong>SCORE</strong></td>
    </tr>
  <tr>
    <td width="6%"  bgcolor="#F2F2F2"><strong>1</strong></td>
    <td width="1%"  bgcolor="#F2F2F2">&nbsp;</td>
    <td width="70%"  bgcolor="#F2F2F2">Reading in Context</td>
    <td width="5%"  bgcolor="#F2F2F2">&nbsp;</td>
    <td width="18%" align="center"  bgcolor="#F2F2F2"><strong>80</strong></td>
    </tr>
  <tr>
    <td><strong>2</strong></td>
    <td>&nbsp;</td>
    <td>Listening in Context</td>
    <td>&nbsp;</td>
    <td  align="center"><strong>35</strong></td>
    </tr>
  <tr>
    <td  bgcolor="#F2F2F2"><strong>3</strong></td>
    <td  bgcolor="#F2F2F2">&nbsp;</td>
    <td  bgcolor="#F2F2F2">Language for meaning and interaction</td>
    <td  bgcolor="#F2F2F2">&nbsp;</td>
    <td  bgcolor="#F2F2F2"  align="center"><strong>56</strong></td>
  </tr>
  <tr>
    <td><strong>4</strong></td>
    <td>&nbsp;</td>
    <td>Examen Oral</td>
    <td>&nbsp;</td>
    <td  align="center"><strong>20</strong></td>
  </tr>
    </table>
    <br>
    <table width="52%" height="96" border="0" align="center" cellpadding="0" cellspacing="0" style="border:#FC0 1px solid">
  <tr>
    <td align="center"><strong>Total Score %</strong></td>
    <td align="center"><strong>57</strong></td>
    </tr>
  <tr>
    <td width="67%" align="center"><strong>TECS Proficiency Stage </strong></td>
    <td width="33%" align="center"><strong>3</strong></td>
    </tr>
    </table>
    <br>
    <p><center>
      <strong>Correspondence of CEF Levels to levels used by the TECS</strong>
  </center></p><table width="49%" height="175" border="0" align="center" cellpadding="0" cellspacing="0" style="border:#FC0 1px solid">
  <tr>
    <td height="59" align="center"  bgcolor="#EAEAEA"><strong>TECS PROFICIENCY STAGE</strong></td>
    <td  bgcolor="#FFFFFF" align="center"><strong>TECS<br>
      SCORE<br>
    </strong></td>
    <td  bgcolor="#EAEAEA" align="center"><strong>TECS<br>
      SCORE
    </strong></td>
    </tr>
  <tr>
    <td width="45%"  bgcolor="#EAEAEA" align="center">0<br>
      1<br>
      2<br>
      3<br>
      4<br>
      5</td>
    <td width="29%"  bgcolor="#FFFFFF" align="center">0-12<br>
      13-31<br>
      31-55<br>
      56-74<br>
      75-89<br>
      90-100</td>
    <td width="26%" height="47"  bgcolor="#EAEAEA" align="center">- A1<br>
      A1<br>
      A2<br>
      B1<br>
      B2<br>
      C1</td>
    </tr>
    </table>
    <p style=" font-size:13px; ">&nbsp;  </p>
    <SPAN  style=" font-size:13px; ">
    <p>For a description of the level refer to The Common European Framework of Reference for Languages: Learning, Teaching, Assessment, developed by the Language Policy Division of the Council of Europe (Strasbourg) (c) 2001 Council of Europe, Language Policy Division.</p>
    
    <p>For more information on the TECS refer to www.iltoexams.com <br>
    </p>
    </SPAN></td>
    </tr>
</table>

  '; 
$dompdf = new DOMPDF(); 
$dompdf->load_html($html); 
$dompdf->render(); 
$pdfoutput = $dompdf->output(); 
$filename = "examspdf/sample334.pdf"; 
$fp = fopen($filename, "a"); 
fwrite($fp, $pdfoutput); 
fclose($fp); 

function mail_attachment($filename, $path, $mailto, $from_mail, $from_name, $replyto, $subject, $message) {
	
    $file = $path.$filename;
    $file_size = filesize($file);
    $handle = fopen($file, "r");
    $content = fread($handle, $file_size);
    fclose($handle);
    $content = chunk_split(base64_encode($content));
    $uid = md5(uniqid(time()));
    $name = basename($file);
    $header = "From: ".$from_name." <".$from_mail.">\r\n";
    $header .= "Reply-To: ".$replyto."\r\n";
    $header .= "MIME-Version: 1.0\r\n";
    $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
    $header .= "This is a multi-part message in MIME format.\r\n";
    $header .= "--".$uid."\r\n";
    $header .= "Content-type:text/html; charset=iso-8859-1\r\n";
    $header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
    $header .= $message."\r\n\r\n";
    $header .= "--".$uid."\r\n";
    $header .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n"; // use different content types here
    $header .= "Content-Transfer-Encoding: base64\r\n";
    $header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
    $header .= $content."\r\n\r\n";
    $header .= "--".$uid."--";
    mail($mailto, $subject, "", $header);
}

$my_file = "sample334.pdf";
$my_path = $_SERVER['DOCUMENT_ROOT']."/newtest2/examspdf/";

$my_name = "IltoExams.com";
$my_mail = "auto-reply@iltoexams.com";
$my_replyto = "welcome@iltoexams.com";
$my_subject = "Welcome to IltoExams.com";
$message = "test mensaje ";

		
mail_attachment($my_file, $my_path, "jlairbanas@hotmail.com", $my_mail, $my_name, $my_replyto, $my_subject, $message);

?>
<?php
/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = $session['profile'];


if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}

if($profile == 6){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}

/* session manager
end
*/

/* @var $this ExamsUserController */
/* @var $model ExamsUser */

$this->breadcrumbs=array(
	'Exams Users'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Back to Licenses', 'url'=>array('LicensesUser/admin/id/'.$model->idLicenciaUsuario->id_usuario)),
);

/*
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#exams-user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
*/ 
$config = Yii::app()->getComponents(false);
$connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
// establish connection. You may try...catch possible exceptions
$connection->active=true;


$SQL = "SELECT exams.nombre, t.id_examen_usuario, t.calificacion, t.fecha_presentacion, t.nivel, t.estado as estado_e FROM `exams_user` `t`, exams, licenses_user, licenses_client WHERE id_licencia_usuario='".$model->id_licencia_usuario."' and exams.id_examen = t.id_examen and id_licencias_usuario = id_licencia_usuario and licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente LIMIT 50"; 

//$SQL = "SELECT * FROM `exams_user` `t` WHERE id_licencia_usuario='".$model->id_licencia_usuario."' LIMIT 50";
$list_u= $connection->createCommand($SQL)->queryAll();


// nos traemos el registro de fotos
$SQL = "SELECT * FROM `answers_exam_user` WHERE id_examen_usuario = '".$list_u[1]["id_examen_usuario"]."' ORDER BY date_answered ASC LIMIT 1";
$dateExams= Yii::app()->db->createCommand($SQL)->queryAll(); 


$SQL = "SELECT * FROM `users_c`  WHERE id_usuario_c='".$model->idLicenciaUsuario->id_usuario."'"; 
$usr= $connection->createCommand($SQL)->queryAll();


$sql='SELECT * FROM `fraud_report` WHERE `document` = "'.$model->idLicenciaUsuario->id_usuario.'"';
$fraud= $connection->createCommand($sql)->queryAll();

$flagFraud = 0;
$fraud_result = array($fraud[0]['document']);

$licenseID = base64_decode($_GET['id']);
$id_usuario = $model->idLicenciaUsuario->id_usuario;

// agregamos al log del usuario
$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 4, '".Yii::app()->user->getState('usuario')."', NOW(), '$licenseID', '', 'Access test taker exams score,username and password', '$id_usuario')";
$log= Yii::app()->db->createCommand($SQL)->query(); 

// nos traemos el registro de fotos
$SQL = "SELECT * FROM `license_photo` WHERE id_license = '$licenseID'";
$photos= Yii::app()->db->createCommand($SQL)->query(); 


if(!is_null($fraud_result[0])){
    $flagFraud=1;
}

//test email notification
$modUtil = new Utilidades();
$modUtil->sendEmailNotification($id_usuario, $licenseID);

$id_admin = Yii::app()->user->getState('usuario');


?>

<style>
   
button.accordion {
    background-color: #31B744;
    color: #FFF;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
    font-weight:bolder;
    text-transform: uppercase;
}

#levels {
    background-color:#1363DC;
}

#levels:hover {
    background-color:#1e8ce0;
}

#important {
    background-color:#FFDA74;
}

#important:hover {
    background-color:#F8F400;
}

button.accordion.active, button.accordion:hover {
    background-color: #00dd06;
    color:#fff;
}

button.accordion:after {
    content: '\02795';
    font-size: 12px;
    color: #212121;
    float: right;
    margin-left: 5px;

}

button.accordion.active:after {
    content: "\2796";
}

div.panel {
    max-height: 0;
    overflow: hidden;
    transition: 0.6s ease-in-out;
    opacity: 0;
    margin-bottom:15px;
    font-size: 14px;
    color: #212121;
}

div.panel ul {
    list-style: url("/../../../../../../images/icons/next.png");
}

div.panel.show {
    opacity: 1;
    max-height: 1000px;  
    padding: 10px 30px;
}


.w3-table-all {
    border: 1px solid #ccc;
     font-size: 14px;
    color: #212121;
}


.w3-table-all th {
    background-color:  #7FC789;
}

.w3-table-all tr:nth-child(odd) {
    background-color: rgba(255, 255, 255, 0.74);
}
.w3-bordered tr, .w3-table-all tr {
    border-bottom: 1px solid #ddd;
}

.w3-table-all td, .w3-table-all th, .w3-table-all th:first-child, .w3-table-all td:first-child {
    padding: 8px;
}

.w3-table td, .w3-table th, .w3-table-all td, .w3-table-all th {
    padding: 6px 5px;
    display: table-cell;
    text-align: left;
    vertical-align: middle;
}

#foo {
    background-color: rgba(57, 181, 74, 0.09);
}

table, th, td {
    border: none;
}

.claseXX.last.col1 {
     display:block;   
}

.licenses {
    border:1px solid black;
}


</style>
<div class="row">
   <div class="col-lg-12 col-sm-12 col-xs-12">
       <div class="widget">
           <div class="widget-header ">
               <span class="widget-caption">Exams for <?php echo $usr[0]['nombres']." ".$usr[0]['apellidos']." - ".$model->idLicenciaUsuario->idLicenciaCliente->idLicencia->nombre;?></span>
            </div>
            <div class="widget-body">
                <table class="table table-striped table-hover table-bordered" id="editabledatatable">
                    <thead>
                        <tr role="row">
                            <th>
                                Test
                            </th>
                            <th>
                                Test Date (yy-mm-dd)
                            </th>
                            <th>
                                Score
                            </th>
                            <th>
                                CEFR Level
                            </th>
                            <th>
                                Status
                            </th>
                            <th>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        
                        $cont = 1;
                        foreach($list_u as $rooms){
                            if(Yii::app()->user->getState('usuario')=='o.garcia' || Yii::app()->user->getState('usuario')=='partialito'){
                                $rooms['estado'] = $rooms['estado_e'];
                            
                            $exam = explode("V", $rooms['nombre']);
                            ?>
                            <tr>
                                <td><?php echo $exam[0]?></td>
                                <td><?php 
                                if($dateExams[0]["date_answered"] != NULL){
                                    echo substr($dateExams[0]["date_answered"],0,10);
                                }else {
                                    echo substr($rooms['fecha_presentacion'],0,10);
                                }
                                ?>
                                </td>
                                <td><input type="number" value="<?php echo number_format($rooms['calificacion'],2)?>" name="score_<?php echo $cont ?>" id="score_<?php echo $cont ?>"/></td>
                                <td class="center "><?php echo $rooms['nivel']?></td>
                                <td class="center "><?php echo ($rooms['estado']=="A")? 'PENDING' : 'COMPLETED' ?></td>
                                <td>
                                    <?php
                                    if (strpos(strtolower($rooms['nombre']), 'speaking') !== false) {
                                        if ($rooms['estado']=='A'){
                                            if($flagFraud==0){
                                                echo '<a href="/ilto3/index.php?r=Clients/AnswersExamUser/takeExam&y='.base64_encode($usr[0]['id_usuario_c']).'&p='.base64_encode($model->idLicenciaUsuario->id_licencias_usuario).'&b='.base64_encode($rooms['id_examen_usuario']).'">TAKE EXAM</a>';
                                            }
                                        }
                                    }
                                    ?>
                                </td>
                            </tr>
                            
                            <?php
                            $cont++;    
                            }else{
                                $rooms['estado'] = $rooms['estado_e'];
                            
                                $exam = explode("V", $rooms['nombre']);
                                
                               
                            ?>
                            <tr>
                                <td><?php echo $exam[0]?></td>
                                <td><?php 
                                if($dateExams[0]["date_answered"] != NULL){
                                    echo substr($dateExams[0]["date_answered"],0,10);
                                }else {
                                    echo substr($rooms['fecha_presentacion'],0,10);
                                }
                                ?>
                                </td>
                                <td><?php echo number_format($rooms['calificacion'],2)?></td>
                                <td class="center "><?php echo $rooms['nivel']?></td>
                                <td class="center "><?php echo ($rooms['estado']=="A")? 'PENDING' : 'COMPLETED' ?></td>
                                <td>
                                    <?php
                                    if (strpos(strtolower($rooms['nombre']), 'speaking') !== false) {
                                        if ($rooms['estado']=='A'){
                                            if($flagFraud==0){
                                                echo '<a href="/ilto3/index.php?r=Clients/AnswersExamUser/takeExam&y='.base64_encode($usr[0]['id_usuario_c']).'&p='.base64_encode($model->idLicenciaUsuario->id_licencias_usuario).'&b='.base64_encode($rooms['id_examen_usuario']).'">TAKE EXAM</a>';
                                            }
                                        }
                                    }
                                    ?>
                                </td>
                                
                            </tr>
                            <?php
                            }
                        }
                        if(Yii::app()->user->getState('usuario')=='o.garcia' || Yii::app()->user->getState('usuario')=='partialito'){
                          ?>
                           <tr>
                               <td colspan="4">
                                   
                               </td>
                           <td>
                                    <input type="hidden" name="id_admin" id="id_admin" value="<?php echo $id_admin; ?>" />
                                <input type="hidden" name="id_user" id="id_user" value="<?php echo $id_usuario ?>" />
                                <input type="hidden" name="id_license" id="id_license" value="<?php echo $licenseID ?>" />
                                 <input id="send"  type="button" class="btn btn-danger" value="Send"/></td>
                                </td>
                                <td>
                                    
                                </td>
                           </tr>
                           <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<?php 
if($flagFraud == 1){
    ?>
    <button class="accordion active" style="background-color:#d42525;">USERNAME AND PASSWORD FOR THE CURRENT TEST LICENSE</button>
        <div class="panel show">
            <span class="widget-caption">The taker has been blocked, due to suspicious fraud actions. <a href="/ilto3/index.php?r=Clients/FraudReport/admin"><b>Click here for more information</b></a></span></br>
    </div>
<?php 
}

    if(Yii::app()->user->getState('id_perfil')==2 || Yii::app()->user->getState('id_perfil')==3 || Yii::app()->user->getState('id_perfil')==8){ //Es un tutor, ver solo sus compromisos
    ?>
    <div class="row">
       <div class="col-lg-12 col-sm-12 col-xs-12">
           <div class="widget">
               <div class="widget-header ">
                   <span class="widget-caption">Test Taker Picture Record</span>
                </div>
                <div class="widget-body">
                <?php 
                foreach ($photos as $photo) {
                   echo '<img src="/images/'.$photo['photo_path'].'" title="'.$photo['created_at'].'">';
                }//Es un tutor, ver solo sus compromisos
                ?>
                <p><strong>Picture records will be deleted 7 days after the test day.</strong></p>
                </div>
            </div>
        </div>
    </div>
<?php 
    }

?>

<script>
$(document).ready(function(){     
    var acc = document.getElementsByClassName("accordion");
    var i;
    
    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function(){
            this.classList.toggle("active");
            this.nextElementSibling.classList.toggle("show");
      }
    }
    
     $("#send").click(function(){
         
       var confirmation = confirm("Do you confirm the changes?");
       spe = $('#score_1').val();
       gra = $('#score_2').val();
       rea = $('#score_3').val();
       lis = $('#score_4').val();
       id_user = $('#id_user').val();
       id_license = $('#id_license').val();
       id_admin = $('#id_admin').val();
       if(confirmation == true){
           console.log(lis);
          $.ajax({
              url : "/ilto3/index.php?r=Api/qualifyLicense",
              type: "POST",
              dataType: "text",
              data: {
                  id_license: id_license, id_user: id_user, id_admin: id_admin, spe: spe, gra: gra, rea: rea, lis: lis,
              },
              success: function(data){
            
              window.location.href ="/ilto3/index.php?r=Clients/LicensesUser/admin&id="+id_user;
               
              }
          })
       }
        
    });
});




</script>

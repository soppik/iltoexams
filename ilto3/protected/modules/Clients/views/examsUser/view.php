<?php
/* @var $this ExamsUserController */
/* @var $model ExamsUser */

$this->breadcrumbs=array(
	'Exams Users'=>array('index'),
	$model->id_examen_usuario,
);

$this->menu=array(
	array('label'=>'List ExamsUser', 'url'=>array('index')),
	array('label'=>'Create ExamsUser', 'url'=>array('create')),
	array('label'=>'Update ExamsUser', 'url'=>array('update', 'id'=>$model->id_examen_usuario)),
	array('label'=>'Delete ExamsUser', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_examen_usuario),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ExamsUser', 'url'=>array('admin')),
);
?>

<h1>View ExamsUser #<?php echo $model->id_examen_usuario; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_examen_usuario',
		'id_licencia_usuario',
		'id_examen',
		'fecha_presentacion',
		'calificacion',
		'nivel',
		'estado',
	),
)); ?>

<?php 
/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = Yii::app()->user->getState('id_perfil');
if($profile == 6){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}


if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}


/* session manager
end
*/


// agregamos al log del usuario access
$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 4, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'Access the test schedule', '')";
$log= Yii::app()->db->createCommand($SQL)->query(); 

$siteUrl=Yii::app()->baseUrl; 
$baseUrl = Yii::app()->theme->baseUrl; 

$modUtil = new Utilidades();
$arregloMeses = $modUtil->arregloMeses();

$userId = Yii::app()->user->getState('cliente');

//echo '<pre>';

$config = Yii::app()->getComponents(false);
$connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
// establish connection. You may try...catch possible exceptions
$connection->active=true;


/*$SQL = "SELECT * FROM `licenses_user` 
INNER JOIN users_c ON licenses_user.id_usuario = users_c.id_usuario_c 
INNER JOIN exams_user ON licenses_user.id_licencias_usuario = exams_user.id_licencia_usuario
WHERE month(licenses_user.fecha_presentacion) = '".date('m')."' and year(licenses_user.fecha_presentacion) = '".date('Y')."' 
AND users_c.id_cliente = $userId 
GROUP BY day(licenses_user.fecha_presentacion), salon";
*/
$SQL = "SELECT * FROM `licenses_user` 
INNER JOIN users_c ON licenses_user.id_usuario = users_c.id_usuario_c 
INNER JOIN exams_user ON licenses_user.id_licencias_usuario = exams_user.id_licencia_usuario 
WHERE month(licenses_user.fecha_presentacion) >= '".date('m')."' and year(licenses_user.fecha_presentacion) >= '".date('Y')."' 
AND users_c.id_cliente = '".$userId."' 
GROUP BY day(licenses_user.fecha_presentacion), month(licenses_user.fecha_presentacion), exams_user.salon
ORDER BY exams_user.salon";

$list_u= $connection->createCommand($SQL)->queryAll();
$arreglo = $List =[];
foreach($list_u as $rowExam){
    
            $fecha = new DateTime($rowExam['fecha_presentacion']);
            
            $link = "/ilto3/index.php?r=Clients/ExamsUser/viewDay&day=".$fecha->format("d")."&month=".$fecha->format("m")."&year=".$fecha->format("Y")."&room=".$rowExam['salon'];
            /*if(isset($arreglo[$fecha->format("d")])){
                $arreglo["'".$fecha->format("d")."'"] .= $link;
            } else {
                $arreglo["'".$fecha->format("d")."'"] = $link;
            }*/
           // var_dump($rowExam);
            $SQL="SELECT * FROM `class_rooms` WHERE `id` = ".$rowExam['salon']."";
            //var_dump($SQL);
            //die;
            //$SQL = "SELECT * FROM `exams_user` `t` WHERE id_licencia_usuario='".$model->id_licencia_usuario."' LIMIT 50";
            $list_salon= $connection->createCommand($SQL)->queryAll();
           
            $monthView = $fecha->format("m")-1;
            
            
            $hora_ = explode(':',$rowExam['hora']);
            $hora = reset($hora_);
            $min = end($hora_);
            $List[] = "{
    				    start: new Date(".$fecha->format("Y").", '".$monthView."', '".$fecha->format("d")."', ".$hora.", ".$min."),
    				    allDay: true,
    				    borderColor: '#e75b8d',
    				    html: '<button class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#date-info-".$rowExam['id_examen_usuario']."\">".$list_salon[0]['nombre']."</button>'
    				}";
    				
  ?>
 <div id="date-info-<?php echo $rowExam['id_examen_usuario'] ?>" class="modal modal-message modal-info fade" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <i class="glyphicon glyphicon-check"></i>
                </div>
                <div class="modal-title">Schedule</div>

                <div class="modal-body">
                    Check the test dates previously scheduled and print the usernames and passwords of test takers.
                    <?php 
                    
                    
                    //$Link = $siteUrl.'/index.php?r=Clients/ExamsUser/printDay&day='.$fecha->format("d").'&month='.$fecha->format("m").'&year='.$fecha->format("Y").'&room='.$rowExam->salon.'&pdf=1&time='.$rowExam->hora.'';
                        // /ilto3/index.php?r=Clients/ExamsUser/printDay&day=30&month=05&year=2016&room=114&pdf=1&time=6:00
                    ?>
                    <table cellpadding="0" cellspacing="0" class="calendar table table-striped" style="width:400px;">
                            <tr>
                                <th colspan="2">
                                    <h2>SCHEDULE FOR <?php echo $fecha->format("d")."-".$fecha->format("m")."-".$fecha->format("Y");?> - ROOM: <?php echo $list_salon[0]['nombre']; ?></h2> 
                                </th>
                            </tr>
                        <?php
                            $SQL = "SELECT * FROM `licenses_user` 
                            INNER JOIN users_c ON licenses_user.id_usuario = users_c.id_usuario_c 
                            INNER JOIN exams_user ON licenses_user.id_licencias_usuario = exams_user.id_licencia_usuario 
                            WHERE day(licenses_user.fecha_presentacion) = '".$fecha->format("d")."' AND month(licenses_user.fecha_presentacion) >= '".$fecha->format("m")."' and year(licenses_user.fecha_presentacion) >= '".date('Y')."' 
                            AND users_c.id_cliente = '".$userId."' 
                            AND exams_user.salon= ".$rowExam['salon']." 
                            GROUP BY licenses_user.id_licencias_usuario
                            ORDER BY licenses_user.hora";
                            $hours= $connection->createCommand($SQL)->queryAll();
                            $flagHour = "";
                            foreach($hours as $hour){
                                //validar que sea fecha diferente y día diferente.
                                if($flagHour!=$hour['hora']){
                                    echo '<tr class="calendar-row">';
                                    echo "<td class='calendar-day-head'>".$hour['hora']."</td><td class='calendar-day'>";
                                    $conector="";
                                    $enlace="";
                                    $room = $hour['salon'];
                                    $year = $fecha->format("Y");
                                    $month = $fecha->format("m");
                                    $day = $fecha->format("d");
                                    $hora = $hour['hora'];
                                    $enlace = "/ilto3/index.php?r=Clients/ExamsUser/printDay&day=$day&month=$month&year=$year&room=$room&pdf=1&time=$hora";
                                    $enlace = "<a target='_blank' href='$enlace'><img style='padding-left:10px;' src='images/iconos/print.png'> Users and Passwords list</a>"; 
                                    echo $enlace;   
                                    echo "</td></tr>";
                                    $flagHour = $hora;
                                }
                            }
                        ?>
                        </table>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal">CLOSE</button>
                </div>
            </div> <!-- / .modal-content -->
        </div> <!-- / .modal-dialog -->
    </div>
 <?php
}

?>
<script>
        $(document).ready(function () {
            /* initialize the external events
            -----------------------------------------------------------------*/
            $('#external-events .external-event').each(function () {

                // store data so the calendar knows to render an event upon drop
                $(this).data('event', {
                    title: $.trim($(this).text()), // use the element's text as the event title
                    stick: true // maintain when user navigates (see docs on the renderEvent method)
                });

                // make the event draggable using jQuery UI
                $(this).draggable({
                    zIndex: 999,
                    revert: true,      // will cause the event to go back to its
                    revertDuration: 0  //  original position after the drag
                });

            });
            /* initialize the calendar
            -----------------------------------------------------------------*/
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay',
                   
                },
                editable: true,
                droppable: false, // this allows things to be dropped onto the calendar
                drop: function () {
                    // is the "remove after drop" checkbox checked?
                    if ($('#drop-remove').is(':checked')) {
                        // if so, remove the element from the "Draggable Events" list
                       // $(this).remove();
                    }
                },
                events: [
    				<?php echo implode(',',$List); ?>
                ]
            });


        });
    </script>
    
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="widget flat">
        <div class="widget-header bordered-bottom bordered-sky">
            <i class="widget-icon fa fa-calendar sky"></i>
            <span class="widget-caption">Events and Schedules</span>
        </div><!--Widget Header-->
        <div class="widget-body">
            <div id='calendar'></div>
        </div><!--Widget Body-->
    </div>
</div>
                        
    
<script src="<?php echo $siteUrl ?>/themes/ilto/html/assets/js/bootbox/bootbox.js"></script>
<style>
    .modal-message .modal-dialog {
        width: 436px;
    }
    
    .modal-body h2 {
        font-size: 18px;
        text-align: center;
        font-weight: bolder!important;
        padding-bottom: 6px;
    }
    
    td.calendar-day-head {
        text-align: right;
    }
    
    td.calendar-day a {
        font-size: 13px;
    }
    .calendar-day img {
        width: 26px;
        position: relative;
        top: -2px;
    }
</style>



<script>
        $(".tool_tip").mouseover(function(){
           eleOffset = $(this).offset();
          $(this).next().fadeIn("fast").css({
                                  left: eleOffset.left + $(this).outerWidth(),
                                  top: eleOffset.top
                          });
          }).mouseout(function(){
                  $(this).next().hide();
          });
$('.sort-link').attr('title','Click to Sort');  
</script>



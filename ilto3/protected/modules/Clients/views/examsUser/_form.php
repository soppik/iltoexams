<?php
/* @var $this ExamsUserController */
/* @var $model ExamsUser */
/* @var $form CActiveForm */

?>
<script>
<?php

if(Yii::app()->user->getState('id_perfil')<=4){
    echo "var esTutor=true;";
} else {
    echo "var esTutor=false;";
}
?>
</script>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'exams-user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

    
	<div class="row">
		<?php echo $form->labelEx($model,'id_licencia_usuario'); ?>
		<?php
            $modeloCombo = LicensesClient::model()->clienteActual()->disponibles()->findAllByAttributes(array('estado'=>'A',));
            foreach($modeloCombo as $modelo){
                $dataCombo[$modelo->id_licencias_cliente]=$modelo->idLicencia->nombre;
            }    
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'ExamsUser[id_licencia_usuario]',
            'id'=> 'combo_licencia',
            'data'=>$dataCombo,
                'value'=>$model->id_licencia_usuario,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'250px',
                'height'=>'33px',
                
            ),
          ));
            ?>
		<?php echo $form->error($model,'id_licencia_usuario'); ?>
	</div>
		<div class="row">
                    <?php echo $form->labelEx($model,'salon'); ?>
                    <?php
                $dataCombo=CHtml::listData(ClassRooms::model()->esteCliente()->findAllByAttributes(array('estado'=>'A')),'id','nombre');
                $this->widget('ext.select2.ESelect2',array(
                'name'=>'ExamsUser[salon]',
                'id'=> 'combo_salon',
                'data'=>$dataCombo,
                    'value'=>$model->salon,
                'options'=>array(
                  'placeholder'=>'Choose one...',
                  'allowClear'=>true,
                    'width'=>'250px',
                'height'=>'33px',
                ),
              ));
                ?>
                    <?php echo $form->error($model,'salon'); ?>
            </div>

            <div class="row">
                    <?php echo $form->labelEx($model,'fecha_presentacion'); ?>
            <div class="fecha"><?php
            echo CHtml::image(Yii::app()->request->baseUrl.'/images/iconos/calendar.png','Calendar',array('onclick'=>"jQuery('#ExamsUser_fecha_presentacion').trigger('focus');", 'style'=>'height:20px; vertical-align:top;opacity: 0.4; filter: alpha(opacity=40);','class'=>'hasDatepicker'));
            echo $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'name'=>'ExamsUser[fecha_presentacion]',
                'value'=>$model->fecha_presentacion, 
                            // 'i18nScriptFile' => 'jquery.ui.datepicker-ja.js', (#2)
                            'htmlOptions' => array(
                                'id' => 'ExamsUser_fecha_presentacion',
                                'size' => '12',
                                'style'=>'width:230px;',
                                'required'=>'required'
                            ),
                            'options' => array(  // (#3)
                                'showOn' => 'focus', 
                                'dateFormat' => 'yy-mm-dd',
                                'showOtherMonths' => true,
                                'selectOtherMonths' => true,
                                'changeMonth' => true,
                                'changeYear' => true,
                                'showButtonPanel' => false,
                                
                            )
                        ), 
                        true);
            ?></div>
                    <?php echo $form->error($model,'fecha_presentacion'); ?>
            </div>

            <div class="row">
                    <?php echo $form->labelEx($model,'hora'); ?>
                    <?php 
                    $dataCombo=array();
                    for($i=5;$i<=22;$i++){
                        $hora = $i.":00";
                        $dataCombo[$hora]=$hora;
                        $hora = $i.":30";
                        $dataCombo[$hora]=$hora;
                    }                    
                    $this->widget('ext.select2.ESelect2',array(
                        'name'=>'ExamsUser[hora]',
                        'id'=> 'combo_horas',
                        'data'=>$dataCombo,
                            'value'=>$model->hora,
                        'options'=>array(
                          'placeholder'=>'Choose one...',
                          'allowClear'=>true,
                            'width'=>'250px',
                        ),
                      ));
                  ?>
                    <?php echo $form->error($model,'hora'); ?>
            </div>
            <div class="row" id="picture" style="display:none;">
                    <?php echo $form->labelEx($model,'ruta_foto'); ?>
                <div class="tool_tip"><img style="width:24px;" src='images/iconos/help.png'/></div><div class="tooltip">This image must be a jpg, png, or gif, and it must be less than 2 MB in size. Its best that the image be 1024x768 pixels or lower</div>
                    <?php echo $form->fileField($model,'ruta_foto',array('size'=>60,'maxlength'=>250)); ?>
                    <?php echo $form->error($model,'ruta_foto'); ?>
            </div>
        
            <div class="row" id="divTutor" style="display:none;">
                    <?php echo $form->labelEx($model,'tutor'); ?>
                    <?php 
                    $dataCombo = CHtml::listData(UsersC::model()->tutores()->findAll(),'id_usuario_c','nombresapellidos');
                    $this->widget('ext.select2.ESelect2',array(
                        'name'=>'ExamsUser[tutor]',
                        'id'=> 'combo_tutores',
                        'data'=>$dataCombo,
                            'value'=>$model->tutor,
                        'options'=>array(
                          'placeholder'=>'Choose one...',
                          'allowClear'=>true,
                            'width'=>'250px',
                        ),
                      ));
                  ?>
                    <?php echo $form->error($model,'tutor'); ?>
            </div>

            <div class="row buttons">
                <input type="hidden" name="doitnow" id="doitnow" value="">
                    <?php echo CHtml::submitButton('Schedule'); ?> 
		<?php echo CHtml::submitButton('Do it Now',array('id'=>'btnNow','class'=>'oculto')); ?>
            </div>

<?php $this->endWidget(); 
$config = array( 
    'scrolling' => 'yes', 
    'titleShow' => false,
    'autoscale'=> true,
    'overlayColor' => '#000',
    'showCloseButton' => true,
    'transitionIn'=>'elastic',
    'transitionOut'=>'elastic',
        // change this as you need
    );
    $this->widget('application.extensions.fancybox.EFancyBox', array('target'=>'.fancy', 'config'=>$config));
?>

</div><!-- form -->
<script>
$('#combo_licencia').change(function(){
    var licencia_cliente = $('#combo_licencia').val();
       $.get('index.php?r=Clients/ExamsUser/tieneTutor/id/'+licencia_cliente, function(data){
        var datos = $.parseJSON(data);
        if(datos.certificado==="1"){
            $('#picture').show();
        } else {
            $('#picture').hide();
        }
        if(datos.estutor==="1"){
            $('#divTutor').show();
            $('#tieneTutor').val('1');
            if(esTutor){
                $('#btnNow').val('Schedule and Take '+datos.nombre+' now');
                $('#btnNow').show();
            }
        } else {
            $('#divTutor').hide();
            $('#tieneTutor').val('0');
        }
    });
});
$('#btnNow').click(function(){
    $('#doitnow').val('1');
    //$('#exams-user-form').attr('target','_blank');
    $('#exams-user-form').submit();
});
</script>

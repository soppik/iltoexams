<?php
/* @var $this ExamsUserController */
/* @var $data ExamsUser */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_examen_usuario')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_examen_usuario), array('view', 'id'=>$data->id_examen_usuario)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_licencia_usuario')); ?>:</b>
	<?php echo CHtml::encode($data->id_licencia_usuario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_examen')); ?>:</b>
	<?php echo CHtml::encode($data->id_examen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_presentacion')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_presentacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('calificacion')); ?>:</b>
	<?php echo CHtml::encode($data->calificacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nivel')); ?>:</b>
	<?php echo CHtml::encode($data->nivel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado')); ?>:</b>
	<?php echo CHtml::encode($data->estado); ?>
	<br />


</div>
<?php
/* @var $this ExamsUserController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Exams Users',
);

$this->menu=array(
	array('label'=>'Create ExamsUser', 'url'=>array('create')),
	array('label'=>'Manage ExamsUser', 'url'=>array('admin')),
);
?>

<h1>Exams Users</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

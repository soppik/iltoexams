<?php

/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = $session['profile'];


if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}



if(Yii::app()->user->getState('id_perfil') != 8){
	if(Yii::app()->user->getState('id_perfil') > 3){
		Yii::app()->user->setFlash('error', 'Acces Denied.');
	    $this->redirect('index.php?r=Clients/default/index');
	}

}
/* session manager
end
*/

$config = Yii::app()->getComponents(false);
$connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
// establish connection. You may try...catch possible exceptions
$connection->active=true;
/* @var $this ExamsUserController */
/* @var $model ExamsUser */

$this->breadcrumbs=array(
	'Exams Users'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Go Back', 'url'=>array('LicensesUser/admin/id/'.$id)),
);


$SQL = "SELECT * FROM `users_c` WHERE `id_usuario_c` = '".Yii::app()->user->getState('usuario')."' ";
$userAll = $connection->createCommand($SQL)->queryAll();

$id_cliente = $userAll[0]["id_cliente"];


$SQL = "SELECT * FROM `users_c` WHERE `id_cliente` LIKE '$id_cliente' AND `id_perfil` IN (2,3,4,8) AND estado = 'A' ORDER BY nombres ASC";
$tutores = $connection->createCommand($SQL)->queryAll();

$SQL= "SELECT id, name from city WHERE CountryCode LIKE 'COL' AND id IN (2257,2259,2265,2275)";
$cities = $connection->createCommand($SQL)->queryAll();

$SQL = "SELECT id_licencias_cliente, licenses_client.id_licencia, nombre, cantidad, licenses_client.estado FROM `licenses_client` INNER JOIN licenses ON licenses_client.id_licencia = licenses.id_licencia WHERE `id_cliente` LIKE '$id_cliente' AND licenses_client.`estado` LIKE 'A'";
$licenciasCliente = $connection->createCommand($SQL)->queryAll();
$quantity = count($licenciasCliente);

$SQL = "SELECT id_licencias_cliente, licenses_client.id_licencia, nombre, cantidad, licenses_client.estado FROM `licenses_client` INNER JOIN licenses ON licenses_client.id_licencia = licenses.id_licencia WHERE `id_cliente` LIKE '$id_cliente' AND licenses_client.`estado` LIKE 'A'";
$licenciasCliente = $connection->createCommand($SQL)->queryAll();
$quantity = count($licenciasCliente);


?>
<script>
	var esTutor=true;
</script>
<style>
	.50_50{
		float:left;
	}
	
	img.hasDatepicker {
	    position: absolute;
	    margin-top: 6px;
	    float: left;
	}
	
	#ui-datepicker-div{
		opacity:0;
	}
	input[name="ExamsUser[ruta_foto]"]{
		opacity: 0;
	    position: absolute;
	    width: 100%;
	    top: 0px;
	    height: 31px;
	}
	
	#photo_name{
		padding: 15px;
		color:#53a93f;
	}
	
	#checked_img {
		/*display:none;*/
	}
</style>
<div class="row">
	<div class="col-lg-7 col-sm-7 col-xs-12">
		<div class="widget radius-bordered">
        	<div class="widget-header bg-blue">
            	<span class="widget-caption">Allocate License to <?php echo UsersC::model()->findByAttributes(array('id_usuario_c'=>$id))->nombres." ".UsersC::model()->findByAttributes(array('id_usuario_c'=>$id))->apellidos;?></span>
            </div>
            <div class="widget-body">
            	<?php 
				// SI ES UN ADMINISTRADOR DE LA FUAA CARGAMOS ESTOS ADICIONALES
				if($id_cliente == '86051730212'){
					?>
                	<form  role="form"  id="groups-users-clients-form" enctype="multipart/form-data" action="/ilto3/index.php?r=Clients/ExamsUser/createFuaa/id/<?php echo $id?>" method="post">
                    <?php 
				}else{
					?>
					<form  role="form"  id="groups-users-clients-form" enctype="multipart/form-data" action="/ilto3/index.php?r=Clients/ExamsUser/create/id/<?php echo $id?>" method="post">
                    <?php 
				}
				?>
                <button type="submit" class="bv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
                <div class="form-group" style="padding: 0;margin: 0;">
					<span class="input-icon icon-right">
						<label>License</label>
						<br>
						<?php
						if($quantity>=1){
							?>
							<select style="width:100%" class="form-control"  id="combo_licencia" name="ExamsUser[id_licencia_usuario]" required="required"><option value="" default></option>
							<?php
								foreach($licenciasCliente as $modelo){
									$str = explode("V", $modelo["nombre"]);
									$licenseName = $str[0];
									echo '<option value="'.$modelo["id_licencias_cliente"].'">'.$licenseName.'</option>';
								}    
							echo '</select>'   ;
						}else{
							echo '<p><a href="index.php?r=Clients/MyAccount">You do not have licences available.  Click here to purchase more.</a></p>';  ?>
							<script>
								$( document ).ready(function() {
									document.getElementById('buttons').style.display="none";
								});
							</script>
						<?php
						}
						?>
					</span>
				</div>
				<div class="form-group" style="padding: 0;margin: 0;">
					<span class="input-icon icon-right">
						<label>Test Room</label>
						<br>
						<?php
						$dataCombo=CHtml::listData(ClassRooms::model()->esteCliente()->findAllByAttributes(array('estado'=>'A')),'id','nombre');
						$this->widget('ext.select2.ESelect2',array(
							'name'=>'ExamsUser[salon]',
							'id'=> 'combo_salon',
							'data'=>$dataCombo,
							'value'=>$model->salon,
							'options'=>array(
							'placeholder'=>'Choose one...',
							'allowClear'=>true,
							'width'=>'250px',
							'height'=>'33px',
							),'htmlOptions' => array(
								'required'=>'required',
								'style'=>'width:100%',
					     		'class'=>'form-control',  
					     	)	
						));
						?>
					</span>
				</div>
				<?php 
				// SI ES UN ADMINISTRADOR DE LA FUAA CARGAMOS ESTOS ADICIONALES
				if($id_cliente == '86051730212'){
				?>
					<div class="form-group" style="padding: 0;margin: 0;">
						<span class="input-icon icon-right">
							<label>City</label>
							<br>
							<select required="required" style="width:100%" class="form-control" id="city" name="ExamsUser[city]">
	                        	<option value=""></option>
	                            <?php 
								foreach($cities as $city){
									echo '<option value="'.$city['id'].'">'.utf8_encode($city['name']).'</option>';
								}
								?>
							</select>
						</span>
					</div>
					<div class="form-group" id="program_cont" style="padding: 0;margin: 0;">
						<span class="input-icon icon-right">
							<label>Program</label>
							<br>
							<div class="form-group" id="program_container" style="padding: 0;margin: 0;display:none;">
							</div>
						</span>
	                </div>
				    <div class="form-group" id="period_container"style="padding: 0;margin: 0;display:none;">
				    	<span class="input-icon icon-right">
				        	<label>Period</label>
							<br>
					        <select required="required" style="width:100%" class="form-control" id="period" name="ExamsUser[period]">
					        	<option value=""></option>
					            <option value="1">1</option>
					            <option value="2">2</option>
					            <option value="3">3</option>
					            <option value="4">4</option>
					            <option value="5">5</option>
					            <option value="6">6</option>
					            <option value="7">7</option>
					            <option value="8">8</option>
					            <option value="9">9</option>
					            <option value="10">10</option>
                            </select>
					   	</span>
					</div>
				<?php 
				}
				?>
				<div class="form-group" style="padding: 0;margin: 0;">
					<span class="input-icon icon-right">
						<br>
						<div class="well with-header">
							<div class="header bordered-pink">Test Date
							</div>
							<div>
								<div class="input-group">
									<input class="form-control date-picker" id="ExamsUser_fecha_presentacion" type="text" name="ExamsUser[fecha_presentacion]" onkeyup="checkPass(); return false;" required="required">
										<span class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</span>
								</div>
							</div>
						</div>
					</span>
				</div>
				<div class="well with-header">
					<div class="header bordered-blue">Hour
					</div>
					<div>
						<div class="input-group">
							<input class="form-control" id="combo_horas" type="text" name="ExamsUser[hora]">
							<span class="input-group-addon">
								<i class="fa fa-clock-o"></i>
							</span>
						</div>
					</div>
				</div>
				<div id="picture" style="display:none;">
					<div class="form-group" style="padding: 0;margin: 0;">
						<span class="input-icon icon-right">
					    	<br>
					        <div class="well with-header">
				            	<div class="header bordered-blue">Certificate Photo
				            	</div>
				                <div>
			                    	<div class="input-group custom-upload">
										<p>This image must be a <b>JPG</b>, and it must be less than 1 MB in size. Its best that the image be 1024x768 pixels or lower</p>
										<span class="file-input btn btn-block btn-default btn-file">
											<input id="ytExamsUser_ruta_foto" type="hidden" value="" name="ExamsUser[ruta_foto]">
												Please select a Photo. <input type="file" multiple="" size="60" maxlength="250" name="ExamsUser[ruta_foto]" id="ExamsUser_ruta_foto"> 
										</span>
											<p style="text-align:center;color: #4296e2; font-weight: bold;"><label id="photo_name" for = "unit"><b>File name</b></label><img id="checked_img" src="/ilto3/images/iconos/checked.png" height="16"/></p>
									</div>
								</div>
							</div>
						</span>
					</div>
				</div>
				<div  id="divTutor" style="display:none;">
					<div class="form-group" style="padding: 0;margin: 0;"> 
						<span class="input-icon icon-right">
					    	<br>
						    <div class="well with-header">
					        	<div class="header bordered-blue">	Please Select a tutor
					        	</div>
						        <div>
							    	<div class="input-group custom-upload">
										<select required="required" style="width:100%" class="form-control" id="combo_tutores" name="ExamsUser[tutor]">
											<option value="" selected="selected"></option>
												<?php 
												foreach($tutores as $tu){
													echo "<option value='".$tu['id_usuario_c']."'>".$tu['nombres']." ".utf8_encode($tu['apellidos'])."</option>";
												}    
												?>
										</select>
									</div>
								</div>
							</div>
						</span>
					</div>
				</div>
				<div class="form-group" id="buttons" style="margin-bottom: 76px;">
                  	<div class="col-lg-8" style="    margin-top: 10px;">
                    	<input type="hidden" name="doitnow" id="doitnow" value="">
                        <input type="submit" name="yt0" value="Schedule" class="btn btn-palegreen">
    					<!--<input id="btnNow" class="oculto btn btn-palegreen" type="submit" name="yt1" value="Do it Now">-->
					</div>
                </div>
                    </form>
            </div> <!-- widget-body edn -->
       </div>
    </div>
</div>
    <!--Bootstrap Date Picker-->
    <script src="/ilto3/themes/ilto/html/assets/js/datetime/bootstrap-datepicker.js"></script>

    <!--Bootstrap Time Picker-->
    <script src="/ilto3/themes/ilto/html/assets/js/datetime/bootstrap-timepicker.js"></script>
<script>
$("#city").change(function() {
	var id = $("#city").val();
	$.ajax({
		url : "/ilto3/index.php?r=Api/getPrograms",
		type: "POST",
		dataType: "text",
		data: {
		id: id,
		},
		success: function(data) {
		    $("#program_container").html(data);
		    $("#program_container").show();
		    $("#period_container").show();
	    }
    });
});

$( document ).ready(function() {
	
	var banned = '<?php echo $banned; ?>';
	if(banned == 1){
		document.getElementById('buttons').style.display="none";
	}
});
	//tal cual Dk
	 //$('.date-picker').datepicker();
		var nowTemp = new Date();
		var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
		 
		var checkin = $('.date-picker').datepicker({
		  onRender: function(date) {
		    return date.valueOf() < now.valueOf() ? 'disabled' : '';
		  }
		}).on('changeDate', function(ev) {
		  if (ev.date.valueOf() > checkout.date.valueOf()) {
		    var newDate = new Date(ev.date)
		    newDate.setDate(newDate.getDate() + 1);
		    checkout.setValue(newDate);
		  }
		  checkin.hide();
		  $('#dpd2')[0].focus();
		}).data('datepicker');
		var checkout = $('#dpd2').datepicker({
		  onRender: function(date) {
		    return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
		  }
		}).on('changeDate', function(ev) {
		  checkout.hide();
}).data('datepicker');

        //--Bootstrap Time Picker--
       //$('#combo_horas').timepicker();
       $(function () {
		    $('#combo_horas').timepicker({
		    });
		});
  $("#combo_horas").change(function(){
    
  var nowTemp = new Date();
  var day = nowTemp.getDate();
  var dateInput = $('#ExamsUser_fecha_presentacion').val(); 
  var res = dateInput.split("/");
  var hour = $('#combo_horas').val(); 
  var res2 = hour.split(" ");
  var newH = res2[0].split(":");  
  var hToCheck = parseInt(newH[0]);
  var mToCheck = parseInt(newH[1]); 
  var dToCheck = parseInt(res[1]);
  var d = new Date();
  var da = d.getDate();
  var h = d.getHours();
  var m = d.getMinutes(); 
  
  
  if(res2[1]=="PM"){
    hToCheck = hToCheck + 12;
  }

if(dToCheck == da){
  if(hToCheck <= h){
    if(mToCheck < m){
       if(h<=24){
        var replace = h+1 + ":00";
       }
      $("#combo_horas").val(replace);
    }else{
    	if(h<=24){
        var replace = h + ":30";
       }
      $("#combo_horas").val(replace);
    } 
  } 
} 

});

</script>

<script>
$('#combo_licencia').change(function(){
    var licencia_cliente = $('#combo_licencia').val();
       $.get('index.php?r=Clients/ExamsUser/tieneTutor/id/'+licencia_cliente, function(data){
        var datos = $.parseJSON(data);
        if(datos.certificado==="1"){
            $('#picture').show();
            $("#ExamsUser_ruta_foto").prop('required',true);
        } else {
            $('#picture').hide();
            $("#ExamsUser_ruta_foto").prop('required',false);
        }
        if(datos.estutor==="1"){
            $('#divTutor').show();
            $('#tieneTutor').val('1');
            $("#combo_tutores").prop('required',true);
            if(esTutor){
                $('#btnNow').val('Schedule and Take '+datos.nombre+' now');
                $('#btnNow').show();
            }
        } else {
            $('#divTutor').hide();
            $("#combo_tutores").prop('required',false);
            $('#tieneTutor').val('0');
        }
    });
});
$('#btnNow').click(function(){
    $('#doitnow').val('1');
    //$('#exams-user-form').attr('target','_blank');
    $('#exams-user-form').submit();
});

$("#ExamsUser_ruta_foto").change(function(){
  var photoName = $("#ExamsUser_ruta_foto").val();
  var finalName = photoName.split("fakepath");
console.log(finalName); 
$("#checked_img").show();
$("#photo_name").html("<b>Selected Photo:</b> "+finalName[1].substring(1));
});
</script>

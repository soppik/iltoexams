<?php
/* @var $this ExamsUserController */
/* @var $model ExamsUser */

$this->breadcrumbs=array(
	'Exams Users'=>array('index'),
	$model->id_examen_usuario=>array('view','id'=>$model->id_examen_usuario),
	'Update',
);

$this->menu=array(
	array('label'=>'Create ExamsUser', 'url'=>array('create')),
	array('label'=>'Manage ExamsUser', 'url'=>array('admin')),
);
?>

<h1>Update ExamsUser <?php echo $model->id_examen_usuario; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
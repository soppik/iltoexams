<?php
/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = $session['profile'];


if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}

if($profile == 6){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}

/* session manager
end
*/

$config = Yii::app()->getComponents(false);
$connection = new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
$connection->active=true;
$id_cliente = Yii::app()->user->getState('cliente');

$this->breadcrumbs=array(
	'Create Users'=>array('index'),
	'transfer',
);

$this->menu=array(
	array('label'=>'Transfer', 'url'=>array('transfer')),
);

$SQL = "SELECT * FROM `groups_users_clients` `t` WHERE id_cliente='".Yii::app()->user->getState('cliente')."' ";
$list_g= $connection->createCommand($SQL)->queryAll();

$SQL = "SELECT licenses.id_licencia, licenses.nombre FROM `licenses_client` INNER JOIN `licenses` ON licenses.id_licencia = licenses_client.id_licencia WHERE id_cliente='".Yii::app()->user->getState('cliente')."' AND licenses.estado ='A' GROUP BY id_licencia";
$list_t= $connection->createCommand($SQL)->queryAll();

$SQL = "SELECT id_cliente FROM `users_c_period` WHERE `id_cliente` = '".$id_cliente."' GROUP BY id_cliente";
$period = $connection->createCommand($SQL)->queryAll();
$status = array("A"=>"Pending","F"=>"Completed");

$SQL = "SELECT programs.id_program, programs.name FROM `programs` INNER JOIN program_school_city ON programs.id_program = program_school_city.id_program INNER JOIN school ON school.id_school = program_school_city.id_school WHERE school.id_client ='".$id_cliente."' GROUP BY programs.id_program ORDER BY programs.name ASC";
$list_pr= $connection->createCommand($SQL)->queryAll();

$SQL = "SELECT id, name from city WHERE CountryCode LIKE 'MEX' AND id IN (2518,2582)";
$list_ct= $connection->createCommand($SQL)->queryAll();

$id_cliente = Yii::app()->user->getState('cliente');
foreach($list_g as $g){
    $grupos[$g['id']] = $g['nombre'];
}

switch ($id_cliente) {
    case '86051730211':
        $program = 1;
        break;
    case '8605173021':
        $program = 1;
        break;
    case '81030525':
        $program = 1;
        break;
    default:
        $program = 0;
}

// agregamos al log del usuario
$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 4, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'get report download', '')";
$log= Yii::app()->db->createCommand($SQL)->query(); 


?>
<style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
    /* display: none; <- Crashes Chrome on hover */
    -webkit-appearance: none;
    margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
}

#alertR_lbl {
		color:#fff;
		background-color:#d73d32 !important;
		padding:15px;
		text-align:center;
	}
	
.check_inp {
    position:relative !important;
    left:0px !important;
    opacity: 10 !important;
}
    
</style>

    
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
                                    <div class="widget">
                                        <div class="widget-header bg-blue">
                                            <i class="widget-icon fa fa-arrow-left"></i>
                                            <span class="widget-caption">SPECIFIC REPORTS</span>
                                           
                                        </div><!--Widget Header-->
                                        <div class="widget-body">
                                            <div>
                                               <h4>Important:</h4>
                                                <b>Specific reports:</b> Use the filters and click the <b>download report</b> button. </br>
                                                <b>Full report:</b> Do not use any filter. Just click the <b>download report</b> button. </br>
                                                The files will be downloaded as CSV file.
                                             </div>
                                        </div><!--Widget Body-->
                                    </div><!--Widget-->
                                </div>
                                
                       
                    </div>
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="widget">
                                <div class="widget-header ">
                                    <div class="row" style="margin:0px !important;">
                                       
                                        </div>
                                </div>
                                <div class="widget-body">
                                    <?php if(Yii::app()->user->getState('cliente') == '86051730211' || Yii::app()->user->getState('cliente') == '860517302112'){
                                        echo '<form action="/ilto3/index.php?r=Clients/LicensesUser/ReportDownloadFuaaDa" method="POST">';
                                    }else{
                                        echo '<form action="/ilto3/index.php?r=Clients/LicensesUser/ReportDownload" method="POST">';
                                    }
                                    ?>
                                    
                                    <input name="LicensesUser[id_cliente]" type="hidden" value="<?php echo (Yii::app()->user->getState('cliente'));?>">
                                        <div class="row">
                                            <div class="col-lg-2 col-sm-2 col-xs-2">
                                            </div>  
                                            <div class="col-lg-4 col-sm-4 col-xs-4">   
                                                <div class="form-group">
                                                    <label>License</label>
                                                    <select class="form-control search" name="LicensesUser[id_license]" data-related="users_c.id_grupo_cliente">
                                                        <option value="">
                                                        </option>
                                                        <?php 
                                                            foreach($list_t as $t){
                                                                ?>
                                                                <option value="<?php echo $t['id_licencia']?>">
                                                                    <?php echo utf8_encode($t['nombre'])?>
                                                                </option>
                                                                <?php
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>   
                                            <div class="col-lg-4 col-sm-4 col-xs-4">  
                                                <div class="form-group">
                                                    <label>Group</label>
                                                    <select class="form-control search" name="LicensesUser[id_grupo_cliente]" data-related="users_c.id_grupo_cliente">
                                                        <option value="">
                                                        </option>
                                                        <?php 
                                                            foreach($list_g as $g){
                                                                ?>
                                                                <option value="<?php echo $g['id']?>">
                                                                    <?php echo utf8_encode($g['nombre'])?>
                                                                </option>
                                                                <?php
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div> <!-- row end -->   
                                        <div class="row">
                                            <div class="col-lg-2 col-sm-2 col-xs-2">
                                            </div> 
                                            <div class="col-lg-4 col-sm-4 col-xs-4">   
                                                <div class="form-group">
                                                    <label>CEFR LEVEL</label><br/>
                                                    <input type="checkbox" name="level[]" class="check_inp" value="A1" style="left:0;">A1&nbsp;
                                                    <input type="checkbox" name="level[]" class="check_inp" value="A2">A2&nbsp;
                                                    <input type="checkbox" name="level[]" class="check_inp" value="B1">B1&nbsp;
                                                    <input type="checkbox" name="level[]" class="check_inp" value="B2">B2&nbsp;
                                                    <input type="checkbox" name="level[]" class="check_inp" value="C1">C1
                                                </div>
                                            </div>   
                                            <div class="col-lg-4 col-sm-4 col-xs-4">  
                                                <div class="form-group">
                                                    <label>STATUS </label>
                                                    <select class="form-control search" name="LicensesUser[status]" data-related="licenses_user.nivel" style="width: 61px;font-weight: lighter;">
                                                        <option value=""></option>
                                                        <option value="F">Completed</option>
                                                        <option value="A">Pending</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div> <!-- row end --> 
                                        <div class="row">
                                            <div class="col-lg-2 col-sm-2 col-xs-2">
                                            </div> 
                                            <div class="col-lg-4 col-sm-4 col-xs-4">   
                                                <div class="form-group">
                                                    <label>USER ID</label>
                                                    <input type="number" class="form-control search" name="LicensesUser[id_user]" style="width: 300px;font-weight: lighter;">
                                                </div>
                                            </div>   
                                            <div class="col-lg-4 col-sm-4 col-xs-4">  
                                                <div class="form-group">
                                                    <label>NAME</label>
                                                    <input type="text" class="form-control search" name="LicensesUser[name]" style="width: 300px;font-weight: lighter;">
                                                </div>
                                            </div>
                                        </div> <!-- row end -->  
                                        <div class="row">
                                            <div class="col-lg-2 col-sm-2 col-xs-2">
                                            </div> 
                                            <div class="col-lg-4 col-sm-4 col-xs-4">   
                                                <div class="form-group">
                                                    <label>SURNAME</label>
                                                    <input type="text" class="form-control search" name="LicensesUser[surname]" style="width: 300px;font-weight: lighter;">
                                                </div>
                                            </div>   
                                            <div class="col-lg-4 col-sm-4 col-xs-4">  
                                                <div class="form-group">
                                                    <label>SCORE ></label>
                                                    <input type="number" class="form-control search" name="LicensesUser[score]" style="width: 100px;font-weight: lighter;">
                                                </div>
                                            </div>
                                        </div> <!-- row end -->  
                                        <div class="row">
                                            <div class="col-lg-2 col-sm-2 col-xs-2">
                                            </div> 
                                            <div class="col-lg-4 col-sm-4 col-xs-4">   
                                                <div class="form-group">
                                                   <input class="search form-control date-picker" id="dp4" type="text" name="LicensesUser[startDate]" data-related="licenses_user.fecha_presentacion" value="FROM" style="width: 300px;font-weight: lighter;">
    								                <span class="input-group-addon">
        								                <i class="fa fa-calendar"></i>
    								                </span>
    								            </div>
                                            </div>   
                                            <div class="col-lg-4 col-sm-4 col-xs-4">  
                                                <div class="form-group">
                                                    <input class="search form-control date-picker" id="dp5" type="text" name="LicensesUser[endDate]" data-related="licenses_user.fecha_presentacion" value="TO" style="width: 300px;font-weight: lighter;">
    								                <span class="input-group-addon" >
        								                <i class="fa fa-calendar"></i>
    								                </span>
                                                </div>
                                            </div>
                                        </div> <!-- row end --> 
                                        <?php
                                        //Si el cliente usa el periodo
                            	        if(!empty($period)){
                            	            ?>
                            	           <div class="row">
                                            <div class="col-lg-2 col-sm-2 col-xs-2">
                                            </div> 
                                            <div class="col-lg-5 col-sm-5 col-xs-5">   
                                                <div class="form-group">
                                                    <label>PERIOD</label><br/>
                                                    <input type="checkbox" name="period[]" class="check_inp" value="1" style="left:0;">1&nbsp;
                                                    <input type="checkbox" name="period[]" class="check_inp" value="2">2&nbsp;
                                                    <input type="checkbox" name="period[]" class="check_inp" value="3">3&nbsp;
                                                    <input type="checkbox" name="period[]" class="check_inp" value="4">4&nbsp;
                                                    <input type="checkbox" name="period[]" class="check_inp" value="5">5&nbsp;
                                                    <input type="checkbox" name="period[]" class="check_inp" value="6">6&nbsp;
                                                    <input type="checkbox" name="period[]" class="check_inp" value="7">7&nbsp;
                                                    <input type="checkbox" name="period[]" class="check_inp" value="8">8&nbsp;
                                                    <input type="checkbox" name="period[]" class="check_inp" value="9">9&nbsp;
                                                    <input type="checkbox" name="period[]" class="check_inp" value="10">10&nbsp;
                                                </div>
                                            </div>   
                                            <div class="col-lg-3 col-sm-3 col-xs-3">  
                                                
                                            </div>
                                        </div> <!-- row end -->  
                            	        <?php    
                            	        }
                            	        if($program == 1){
                            	         ?>
                            	           <div class="row">
                                                <div class="col-lg-2 col-sm-2 col-xs-2">
                                                </div> 
                                                <div class="col-lg-4 col-sm-4 col-xs-4">  
                                                    <div class="form-group">
                                                        <label>Program</label>
                                                        <select class="form-control search" name="LicensesUser[id_program]" data-related="users_c.id_grupo_cliente">
                                                            <option value="">
                                                            </option>
                                                            <?php 
                                                                foreach($list_pr as $program){
                                                                    ?>
                                                                    <option value="<?php echo $program['id_program']?>">
                                                                        <?php echo ucfirst(utf8_encode($program['name']))?>
                                                                    </option>
                                                                <?php
                                                                }
                                                                ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-sm-4 col-xs-4">  
                                                </div>
                                            </div>
                                        <?php
                                        }
                                        ?><!-- row end --> 
                                        <div class="row">
                                            <div class="col-lg-2 col-sm-2 col-xs-2">
                                            </div> 
                                            <div class="col-lg-4 col-sm-4 col-xs-4">   
                                                
                                            </div>   
                                            <div class="col-lg-4 col-sm-4 col-xs-4">  
                                                <div class="form-group">
                                                    <input type="submit" name="yt0" value="DOWNLOAD REPORT" class="btn btn-blue" style="margin-left: 100px;float:left">
                                                </div>
                                            </div>
                                        </div> <!-- row end --> 
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                        


  <!--Page Related Scripts-->
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/jquery.dataTables.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/ZeroClipboard.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.tableTools.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.bootstrap.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/datatables-init.js"></script>
    <!--Bootstrap Date Picker-->
    <script src="/ilto3/themes/ilto/html/assets/js/datetime/bootstrap-datepicker.js"></script>
    <script>
        InitiateSimpleDataTable.init();
        InitiateExpandableDataTable.init();
        InitiateSearchableDataTable.init();
       
    </script>
    <script>
    
            var startDate = new Date();
			var endDate = new Date();
			$('#dp4').datepicker({
				format: 'yyyy-mm-dd'
			});
			$('#dp5').datepicker({
				format: 'yyyy-mm-dd'
			});
			
     $('#dp4').datepicker()
				.on('changeDate', function(ev){
					if (ev.date.valueOf() > endDate.valueOf()){
						$('#alert').show().find('strong').text('The final date cannot be previous to  the starting date.');
					} else {
						$('#alert').hide();
						startDate = new Date(ev.date);
						$('#startDate').text($('#dp4').data('date'));
					}
					$('#dp4').datepicker('hide');
				});
			$('#dp5').datepicker()
				.on('changeDate', function(ev){
					if (ev.date.valueOf() < startDate.valueOf()){
						$('#alert').show().find('strong').text('The final date cannot be previous to  the starting date.');
					} else {
						$('#alert').hide();
						endDate = new Date(ev.date);
						$('#endDate').text($('#dp5').data('date'));
					}
					$('#dp5').datepicker('hide');
				});
				$(function(){
											  $('#calificacion').keypress(function(e){
											    if(e.which == 101){
											    	return false;
											    }
											  });
											});
    </script>

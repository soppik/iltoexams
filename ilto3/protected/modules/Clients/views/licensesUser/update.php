<?php
/* @var $this LicensesUserController */
/* @var $model LicensesUser */

$this->breadcrumbs=array(
	'Licenses Users'=>array('index'),
	$model->id_licencias_usuario=>array('view','id'=>$model->id_licencias_usuario),
	'Update',
);

$this->menu=array(
	array('label'=>'Create LicensesUser', 'url'=>array('create')),
	array('label'=>'Manage LicensesUser', 'url'=>array('admin')),
);
?>

<h1>Update LicensesUser <?php echo $model->id_licencias_usuario; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
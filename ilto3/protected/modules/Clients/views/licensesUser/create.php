<?php
/* @var $this LicensesUserController */
/* @var $model LicensesUser */
$this->breadcrumbs=array(
	'Licenses Users'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage LicensesUser', 'url'=>array('admin')),
);
?>

<?php 
$this->renderPartial('_form', array('model'=>$model)); 

?>


<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
	
                                    <div class="widget radius-bordered">
                                        <div class="widget-header bg-blue">
                                            <span class="widget-caption">Create LicensesUser</span>
                                        </div>
                                        <div class="widget-body">
                                            <form  id="groups-users-clients-form" action="/ilto3/index.php?r=Clients/groupsUsersClients/create" method="post" class="form-horizontal bv-form" data-bv-message="This value is not valid" data-bv-feedbackicons-valid="glyphicon glyphicon-ok" data-bv-feedbackicons-invalid="glyphicon glyphicon-remove" data-bv-feedbackicons-validating="glyphicon glyphicon-refresh" novalidate="novalidate">
                                            	<button type="submit" class="bv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
                                               
                                                
                                                <div class="form-group has-feedback">
                                                    <label class="col-lg-4 control-label">Name *</label>
                                                    <div class="col-lg-8">
                                                        <input required=required type="text" class="form-control" name="GroupsUsersClients[nombre]" id="GroupsUsersClients_nombre" data-bv-message="The name is not valid" data-bv-notempty="true" data-bv-notempty-message="The name is required and cannot be empty" data-bv-regexp="true" data-bv-regexp-regexp="[a-zA-Z0-9_\.]+" data-bv-regexp-message="The name can only consist of alphabetical, number, dot and underscore" data-bv-stringlength="true" data-bv-stringlength-min="6" data-bv-stringlength-max="30" data-bv-stringlength-message="The name must be more than 6 and less than 30 characters long"  data-bv-field="GroupsUsersClients[nombre]">
                                                        <i class="form-control-feedback" data-bv-icon-for="username" style="display: none;"></i>
                                                    <small class="help-block" data-bv-validator="different" data-bv-for="username" data-bv-result="NOT_VALIDATED" style="display: none;">The name cannot be the same as each other</small><small class="help-block" data-bv-validator="notEmpty" data-bv-for="username" data-bv-result="NOT_VALIDATED" style="display: none;">The name is required and cannot be empty</small><small class="help-block" data-bv-validator="regexp" data-bv-for="username" data-bv-result="NOT_VALIDATED" style="display: none;">The name can only consist of alphabetical, number, dot and underscore</small><small class="help-block" data-bv-validator="stringLength" data-bv-for="username" data-bv-result="NOT_VALIDATED" style="display: none;">The name must be more than 6 and less than 30 characters long</small></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-lg-offset-4 col-lg-8">
                                                        <input class="btn btn-palegreen" type="submit"  name="yt0" value="Create">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                      
                                </div>
</div>
	
	
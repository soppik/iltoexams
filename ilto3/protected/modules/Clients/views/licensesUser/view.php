<?php
/* @var $this LicensesUserController */
/* @var $model LicensesUser */

$this->breadcrumbs=array(
	'Licenses Users'=>array('index'),
	$model->id_licencias_usuario,
);

$this->menu=array(
	array('label'=>'List LicensesUser', 'url'=>array('index')),
	array('label'=>'Create LicensesUser', 'url'=>array('create')),
	array('label'=>'Update LicensesUser', 'url'=>array('update', 'id'=>$model->id_licencias_usuario)),
	array('label'=>'Delete LicensesUser', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_licencias_usuario),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage LicensesUser', 'url'=>array('admin')),
);
?>

<h1>View LicensesUser #<?php echo $model->id_licencias_usuario; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_licencias_usuario',
		'id_licencia_cliente',
		'id_usuario',
		'fecha_asignacion',
		'estado',
	),
)); ?>

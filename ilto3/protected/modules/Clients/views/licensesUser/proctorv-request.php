<?php
/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = Yii::app()->user->getState('id_perfil');


if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}

if($profile == 6){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}

/* session manager
end
*/

/* @var $this ExamsUserController */
/* @var $model ExamsUser */

$this->breadcrumbs=array(
	'Exams Users'=>array('index'),
	'Manage',
);

$config = Yii::app()->getComponents(false);
//$connection=new CDbConnection('mysql:host=localhost;dbname=iltoexam_newversion;','iltoexam_newvers','Ilto.2015');
$connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
// establish connection. You may try...catch possible exceptions
$connection->active=true;


$id_request = base64_decode($_GET["id"]);

$id_client = Yii::app()->user->getState('cliente');
$user_type = Yii::app()->user->getState('id_perfil');



$SQL = "SELECT date_request, step_request, id_user, id_license, questions_step, photo_step, photo_route, card_step, card_route, status_request, identity_verified, nombres, apellidos FROM `proctorv_request` INNER JOIN users_c ON proctorv_request.id_user = users_c.id_usuario_c WHERE id_proctorv_request = '".$id_request."'";
$proctorv_request= $connection->createCommand($SQL)->queryAll();

$id_user_to_email = $proctorv_request[0]["id_user"];


// agregamos al log del usuario
$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 5, '".Yii::app()->user->getState('usuario')."', NOW(), '".$proctorv_request[0]["id_license"]."', '$id_request', 'ACCESS test taker TECS2GO registration', '".$proctorv_request[0]["id_user"]."')";
$log= Yii::app()->db->createCommand($SQL)->query(); 

// GET QUESTIONS
$SQL = "SELECT * FROM proctorv_user_answers WHERE id_user = '".$proctorv_request[0]["id_user"]."'";
$proctorv_questions= $connection->createCommand($SQL)->queryAll();


$id_admin = Yii::app()->user->getState('usuario');


if($proctorv_request["id_license"] != NULL){
    $SQL = "SELECT * FROM `exams_user` WHERE `id_licencia_usuario` = '".$proctorv_request["id_license"]."' ORDER BY id_examen_usuario ASC LIMIT 1 ";
    $speaking= $connection->createCommand($SQL)->queryAll();
    
    $btn_speaking = '<a href="/ilto3/index.php?r=Clients/AnswersExamUser/takeExam&userId='.$speaking[0]["id_examen_usuario"].'">TAKE EXAM</a>';
}else{


}

//identity btn status
if($proctorv_request[0]["photo_step"]==1){
        $identity_button = "";
    }else{
        $identity_button = "disabled";
    }

//STATUS SWITCH
switch ($proctorv_request[0]["status_request"]) {
    
    case 0:
        $status = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Pending for check it!" width="32"> <span class="red_text">PENDING</span></h4>';
        $available_at = '<input id="enableValidation" type="button" class="btn btn-success" value="Enable Validation" '.$identity_button.'/>';
        break;
    case 1:
        $status = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Pending for check it!" width="32"> <span class="red_text">PENDING</span></h4>
        <p>Waiting Identity Validation</p>';
        break;
    case 2:
        $status = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Pending for check it!" width="32"> <span class="green_text">READY TO TAKE SPEAKING</span></h4>
        '.$btn_speaking.'';
        ;
    break;
    case 3:
       $status = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Pending for check it!" width="32"> <span class="yellow_text">READY TO TAKE ONLINE SECTIONS</span>';
       $available_at = '<p>READY TO TAKE ONLINE SECTIONS</p></h4>';
    break;
    case 4:
       $status = '<h4><img src="/ilto3/images/iconos/verified.png" title="Pending for check it!" width="32"> <span class="green_text">DONE</span>';
       $available_at = '<p>DONE</p></h4>';
    break;
    case 5:
       $status = '<h4><img src="/ilto3/images/iconos/alert-danger.png" title="Pending for check it!" width="32"> <span class="red_text">REVOKED</span>';
       $available_at = '<p>Please contact an administrator.</p></h4>';
    break;
    
    default:
        // code...
        break;
}

// Presentation date for be able the IDENTITY VERIFY


//IDENTITY SWITCH
switch ($proctorv_request[0]["identity_verified"]) {
    case 0:
        $identity_step = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Not Done!" width="32"> 
        <span class="yellow_text">PENDING</span><br/>
        Identity Validation</h4><br>
        '.$available_at.' ';
        break;
    case 1:
        $identity_step = '<h4><img src="/ilto3/images/iconos/verified.png" title="Done!" width="32">
        <span class="green_text">DONE</h4> 
        <h4>Identity Validation</h4><br>';
        break;
    case 2:
        $identity_step = '<h4><img src="/ilto3/images/iconos/alert-danger.png" title="failed!" width="32">
        <span class="red_text">FAILED</h4>
        Identity Validation</h4><br>
        <p>Validation not sucessful. Contact the TECS2GO Administrator</p>';
        break;
    default:
        // code...
        break;
        
        
        
}


 //SPEAKING DATE
    $SQL = "SELECT * FROM proctorv_user_dates  WHERE id_request = '".$id_request."' AND speaking_date = 1";
    $date_speaking_exist = $connection->createCommand($SQL)->queryAll();

    
    //ONLINE SECTION DATE
    $SQL = "SELECT * FROM proctorv_user_dates  WHERE id_request = '".$id_request."' AND speaking_date = 0";
    $date_online_exist = $connection->createCommand($SQL)->queryAll();


//DATE SPEAKING
if(count($date_speaking_exist)>0){
    $disabled_verify= "";
    $date_speaking = explode(" ", $date_speaking_exist[0]['date_picked']);
    if($date_speaking[1]=="14:00:00"){
        $time_zone_speaking = "PM";
    }else{
        $time_zone_speaking = "AM";
    }
    
   $btn_reset_speaking = '<input type="button" class="btn btn-danger" id="resetSpeakingDate" name="resetSpeakingDate" value="Reset Speaking Date" />';
   //$btn_reset_speaking = '';
    $date_speaking = '<h4><img src="/ilto3/images/iconos/verified.png" title="DONE!" width="32">
                        <span class="green_text">DONE</span></h4>
                        <p><b>5. Datetime speaking</b></p>
                        <h5>'.$date_speaking[0].' '.$time_zone_speaking.'</h5>';

}else{
    $date_speaking = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="PENDING!" width="32"> 
                            <span class="yellow_text">PENDING</span></h4>
                            <p><b>5. Datetime Speaking</b></p>
                             <div id="date_pick_notify"><input id="date_pick_btn_speaking" type="button" class="btn btn-success" value="Notify Date Pick" '.$proctorv_request[0]["id_user"].'/></div> ';
    $disabled_verify= "disabled";
}




//DATE ONLINE SECTIONS
if(count($date_online_exist)>0){
    $date_online = '<h4><img src="/ilto3/images/iconos/verified.png" title="DONE!" width="32">
                        <span class="green_text">DONE</span></h4>
                        <p><b>6. Datetime ONLINE</b></p>
                        <h5>'.$date_online_exist[0]["date_picked"].'</h5>';      
     $btn_reset_online = '<input type="button" class="btn btn-danger" id="resetDates" name="resetDates" value="Reset Online Date" />';
    // $btn_reset_online = '';
    }else{
    $date_online = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="PENDING!" width="32"> 
                            <span class="yellow_text">PENDING</span></h4>
                             <p><b>6. Datetime ONLINE</b></p>
                             <div id="date_pick_notify"><input id="date_pick_btn" type="button" class="btn btn-success" value="Notify Date Pick" '.$proctorv_request[0]["id_user"].'/></div> ';
    $btn_reset_online = '';
                      
}




//profile picture
switch ($proctorv_request[0]["photo_step"]) {
    case 0:
        $photo_step = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Not Done!" width="32"> 
                            <span class="yellow_text">PENDING VALIDATION</span><h4>
                            <h4>3. Face Photo</h4><br/>
                            <input id="verifyProfilePhoto" type="button" class="btn btn-success" value="ACCEPT" '.$disabled_verify.'/> &nbsp; <input id="denyProfilePhoto" type="button" class="btn btn-danger"  value="DENY"/><br><br>';
                         if($proctorv_request[0]["photo_route"]!=NULL){
                                $photo_step .= '<img class="requirement-picture" src="/ilto3/images/proctorv_users/'.$proctorv_request[0]['photo_route'].'" title="'.$proctorv_request[0]['photo_route'].'"> ';
                            }else{
                                $photo_step .= '<img class="requirement-picture" src="/ilto3/images/no-profile-photo-ilto.jpg" title="No profile picture found"> ';
                            }
        break;
    case 1:
        $photo_step = '<h4><img src="/ilto3/images/iconos/verified.png" title="Done!" width="32">
                        <span class="green_text">DONE AND MATCH</span></h4>
                        <h4>3. Face Photo</h4><br/>';
                            if($proctorv_request[0]["photo_step"]!=NULL){
                               $photo_step .= '<img class="requirement-picture"  src="/ilto3/images/proctorv_users/'.$proctorv_request[0]['photo_route'].'" title="'.$proctorv_request[0]['photo_route'].'"> ';
                            }else{
                                $photo_step .= '<img class="requirement-picture" src="/ilto3/images/no-profile-photo-ilto.jpg" title="No profile picture found"> ';
                            }
        break;
     case 2:
        $photo_step = '<h4><img src="/ilto3/images/iconos/alert-danger.png" title="Alert!" width="32">
                <span class="red_text">PENDING PHOTO AND VALIDATION</span><h4>
                 <h4>3. Face Photo</h4><br/>
                  <img class="requirement-picture" src="/ilto3/images/no-profile-photo-ilto.jpg" title="No profile picture found"> <br><br>
                  <div id="photo_notify"><input id="photo_notify_btn" type="button" class="btn btn-success" value="Picture missing reminder" '.$proctorv_request[0]["id_user"].'/></div>';
        break;
    
    case 3:
        $photo_step = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Completed!" width="32">
                        <span class="yellow_text">ACCEPTED. PENDING FACE MATCH</span></h4>
                        <h4>3. Face Photo</h4>
                        <p>Does the person on the camera match the photo?</p>
                        <input id="matchPhotoFace" type="button" class="btn btn-success" value="MATCH"/> &nbsp; <input id="NotMatchPhotoFace" type="button" class="btn btn-danger"  value="DO NOT MATCH"/><br><br>
                        <img class="requirement-picture" src="/ilto3/images/proctorv_users/'.$proctorv_request[0]['photo_route'].'" title="taker face photo"> ';
        break;
    case 4:
        $photo_step = '<h4><img src="/ilto3/images/iconos/alert-danger.png" title="Completed!" width="32">
                        <span class="red_text">MATCH FAILED</span></h4>
                        <h4>3. Face Photo</b><br/></h4>
                        <p>The person on the camera does not match the photo, please contact an ILTO administrator</p>
                        <img class="requirement-picture" src="/ilto3/images/proctorv_users/'.$proctorv_request[0]['photo_route'].'" title="taker face photo"> ';
        break;
    
    default:
        // code...
        break;
}

//ID SWITCH
switch ($proctorv_request[0]["card_step"]) {
    case 0:
        $card_step = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Not Done!" width="32"> 
                            <span class="yellow_text">PENDING VALIDATION</span><h4>
                            <h4>4. ID Card Photo - '.$proctorv_request[0]["id_user"].'</h4><br/>
                            <input id="verifyIdPhoto" type="button" class="btn btn-success" value="ACCEPT"/> &nbsp; <input id="denyIdPhoto" type="button" class="btn btn-danger"  value="DENY"/><br><br>';
                         if($proctorv_request[0]["card_route"]!=NULL){
                                $card_step .= '<img class="requirement-picture" src="/ilto3/images/proctorv_users/'.$proctorv_request[0]['card_route'].'" title="'.$proctorv_request[0]['card_route'].'"> ';
                            }else{
                                $card_step .= '<img class="requirement-picture" src="/ilto3/images/no-id-photo-ilto.jpg" title="No profile picture found"> ';
                            }
        break;
    case 1:
        $card_step = '<h4><img src="/ilto3/images/iconos/verified.png" title="Done!" width="32">
                        <span class="green_text">DONE</span></h4>
                        <h4>4. ID Card Photo - '.$proctorv_request[0]["id_user"].'</h4><br/>';
                            if($proctorv_request[0]["card_route"]!=NULL){
                               $card_step .= '<img  class="requirement-picture" src="/ilto3/images/proctorv_users/'.$proctorv_request[0]['card_route'].'" title="'.$proctorv_request[0]['card_route'].'"> ';
                            }else{
                                $card_step .= '<img class="requirement-picture" src="/ilto3/images/no-id-photo-ilto.jpg" title="No profile picture found"> ';
                            }
        break;
     case 2:
        $card_step = '<h4><img src="/ilto3/images/iconos/alert-danger.png" title="Alert!" width="32">
                <span class="red_text">PENDING PHOTO AND VALIDATION</span><h4>
                <h4>4. ID Card Photo</h4><br/>
                <img class="requirement-picture" src="/ilto3/images/no-id-photo-ilto.jpg" title="No profile picture found"> <br><br>
                 <div id="card_notify"><input id="card_notify_btn" type="button" class="btn btn-success" value="Picture missing reminder" '.$proctorv_request[0]["id_user"].'/></div>';
        break;
    
    default:
        // code...
        break;
}



//TECS2GO PAYMENT
$SQL = "SELECT * FROM proctorv_user_payment  WHERE id_request = '".$id_request."' ORDER BY `id_proctorv_user_payment` DESC LIMIT 1";
$user_payment = $connection->createCommand($SQL)->queryAll();

if(count($user_payment)>0){
    switch ($user_payment[0]["status"]) {
         case 4:
             $payment = '<h4><img src="/ilto3/images/iconos/verified.png" title="DONE!" width="32">
                        <span class="green_text">DONE via PSE</span></h4>
                        <p><b>Payment <br>Taker is able to select the dates</b></p>
                        <h5>Reference: '.$user_payment[0]["transaction_reference"].'</h5>
                        <h5>'.$user_payment[0]["bank"].'</h5>' ;
             break;
         case 6:
             $payment = '<h4><img src="/ilto3/images/iconos/alert-danger.png" title="DONE!" width="32">
                        <span class="red_text">REJECTED via PSE</span></h4>
                        <p><b>Payment Pending to Verify</b></p>
                        <h5>Reference: '.$user_payment[0]["transaction_reference"].'</h5>
                        <h5>'.$user_payment[0]["bank"].'</h5>';
                        $payment .= '<div id="payment_notify"><input id="confirm_payment_btn" type="button" class="btn btn-success" value="Confirm Payment" '.$user_payment[0]["id_request"].'/>  </div>';    
             break;
         case 7:
             $payment = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="DONE!" width="32">
                        <span class="yellow_text">PENDING via PSE</span></h4>
                        <p><b>Payment Pending to Verify</b></p>
                        <h5>Reference: '.$user_payment[0]["transaction_reference"].'</h5>
                        <h5>'.$user_payment[0]["bank"].'</h5>';
                        if($profile == 8){
                            $payment .= '<div id="payment_notify"><input id="confirm_payment_btn" type="button" class="btn btn-success" value="Confirm Payment" '.$user_payment[0]["id_request"].'/>  <input id="reject_payment_btn" type="button" class="btn btn-danger" value="Reject Payment" '.$user_payment[0]["id_request"].'/></div>';    
                        }
                            
                        
                        
             break;   
         case NULL:
             $payment = '<h4><img src="/ilto3/images/iconos/verified.png" title="DONE!" width="32">
                        <span class="green_text">DONE by University</span></h4>
                        <p><b>Payment <br>Taker is able to select the dates</b></p>
                        <div id="payment_notify"><h5>Payment No: '.$user_payment[0]['payment_number'].'</h5>';
                        $payment .= '<input id="reject_university_payment_btn" type="button" class="btn btn-danger" value="Reject Payment" '.$user_payment[0]["id_request"].'/></div>';
             break;
         
         default:
             // code...
             break;
    }
}else{
     
    
     $payment = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="PENDING!" width="32"> 
                            <span class="yellow_text">PENDING</span><h4>
                        <p><b>Taker Payment</b></p>
                        <h5>There is no payment registered.</h5>';
                        
     if($profile == 8){
        $payment .= '<div id="payment_notify"><input id="confirm_noregister_payment_btn" type="button" class="btn btn-success" value="Confirm Payment" '.$id_request.'/></div>';                            
    }
}


//license content
if($proctorv_request[0]["id_license"] == NULL){
    $license_content = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Pending!" width="32"><span class="yellow_text"> PENDING</span></h4>
    <h4>License Allocation</h4><br><br>';
}else{
    //DATES FOR TECS
    $SQL = "SELECT * FROM licenses_user  WHERE id_licencias_usuario = '".$proctorv_request[0]["id_license"]."'";
    $license = $connection->createCommand($SQL)->queryAll();
    switch($license[0]["estado"]){
        case 'A':
            $SQL = "SELECT * FROM exams_user WHERE id_licencia_usuario = '".$proctorv_request[0]["id_license"]."' ORDER BY id_examen_usuario ASC LIMIT 1";
            $speaking = $connection->createCommand($SQL)->queryAll();
            if($speaking[0]["estado"] == 'F'){
                $speaking_exam = "<p>Speaking Exam Completed</p>";
                $btn_reset_speaking = "";
                
            }else{
                $speaking_exam = "<p>Speaking Exam Pending</p>";
            }    
            $license_content ='<h4><img src="/ilto3/images/iconos/verified.png" title="License Allocated!" width="32"><span class="green_text"> DONE</span></h4>
            <h4>License Allocation</h4><br>
            <p>Status: Pending</p>
            '.$speaking_exam.'
            <a href="/ilto3/index.php?r=Clients/ExamsUser/admin&amp;id='.base64_encode($proctorv_request[0]["id_license"]).'" " class="btn btn-success btn-xs edit"><i class="fa fa-check"></i>Results</a>
            ';
            $btn_revoke = '<input id="revokeLicense" type="button" class="btn btn-danger" value="Revoke License"/>';
        break;
        case 'F':
            $license_content ='<h4><img src="/ilto3/images/iconos/verified.png" title="License Allocated!" width="32"><span class="green_text"> DONE</span></h4>
            <h4>License Allocation</h4><br>
            <p>Status: Completed</p>
            <p>Score: '.$license["calificacion"].'  CEFR: '.$license[0]["nivel"].'</p>
            <a href="/ilto3/index.php?r=Clients/ExamsUser/admin&amp;id='.base64_encode($proctorv_request[0]["id_license"]).'" " class="btn btn-success btn-xs edit"><i class="fa fa-check"></i>Results</a>';
            $btn_revoke = '<input id="revokeLicense" type="button" class="btn btn-danger" value="Revoke License"/>';
            $btn_reset_online = "";
            $btn_reset_speaking = "";
        break;
         case 'B':
            $license_content ='<h4><img src="/ilto3/images/iconos/verified.png" title="License Allocated!" width="32"><span class="green_text"> DONE</span></h4>
            <h4>License Allocation</h4><br>
            <p>Status: Revoked</p>
            <a href="/ilto3/index.php?r=Clients/ExamsUser/admin&amp;id='.base64_encode($proctorv_request[0]["id_license"]).'" " class="btn btn-success btn-xs edit"><i class="fa fa-check"></i>Results</a>';
            $btn_revoke = '<h4 class="red_text">This license has been Revoked.</h4>';
            if($id_admin == "partialito" || $id_admin == "o.garcia"){
                $btn_revoke .= '<input id="UndoRevokeLicense" type="button" class="btn btn-danger" value="Undo Revoked"/>';
            }
                           
        break;
    }
    
}

$speaking_flag = 0;


//test email notification
$modUtil = new Utilidades();
$modUtil->sendEmailNotification($id_usuario, $licenseID);


?>

<style>
   
.widget-body {
    
    padding: 1px !important;
    text-align: center;
    
}


.widget-header {
    font-weight:800;
    text-align: center;
    
}

input[type=checkbox], input[type=radio] {
    opacity: 1 !important;
    position:relative;
    left:0px;
}

.red_text {
    color: #d73d32 !important;
}
.green_text {
    color: #53a93f !important;
}

.yellow_text {
    color: #efb71a !important;
}

h4 { 
 font-weight:400 !important;   
}

.widget-body {
    padding: 10px !important;
}



</style>
<form action="#">
    <div class="widget-header ">
        <span class="widget-caption">TECS2GO Request</span>
    </div>
    <?php  
    if($id_client == '900430264712' && $user_type == '4'){
    ?>    
        <div class="widget">
            <div class="widget-body">
                <div class="row">
                    <div class="col-lg-4 col-sm-4 col-xs-4">
                        <h4><img src="/ilto3/images/iconos/verified.png" title="Pending!" width="32"><span class="green_text"> DONE</span></h4>
                        <b><h4>1. Test Taker Registration.</h4></b><br/>
                        <h4><?php echo utf8_decode(ucfirst($proctorv_request[0]["nombres"]))." ".utf8_decode(ucfirst($proctorv_request[0]["apellidos"]))?><br/>  <br/>
                        <?php echo $proctorv_request[0]["id_user"]?></h4>
                        <input type="hidden" name="id_request" id="id_request" value="<?php echo $id_request; ?>" />
                        <input type="hidden" name="id_admin" id="id_admin" value="<?php echo $id_admin; ?>" />
                        <input type="hidden" name="id_license" id="id_license" value="<?php echo $proctorv_request[0]["id_license"]; ?>" />
                         <input type="hidden" name="id_user" id="id_user" value="<?php echo $proctorv_request[0]["id_user"]; ?>" />
                    </div>
                    <div class="col-lg-4 col-sm-4 col-xs-4">
                         <?php 
                               echo $license_content;
                             ?>
                        <b><h4>Revoke License.</h4></b>
                    </div>
                    <div class="col-lg-4 col-sm-4 col-xs-4" id="license_action">
                        <br>
                        <h4>License actions</h4><br><br><br>
                        <?php  
                        
                            echo $btn_revoke;
                        ?>
                        
                    </div>
                </div>
            </div>    
        </div> 
    <?php    
    }else{
    ?>
    <div class="widget">
                <div class="widget-body">
                    <div class="row">
                        <?php  
                             if($profile == 8 || $profile == 4){
                                if($proctorv_request[0]["questions_step"] != null){
                                    ?>    
                                    <input type="hidden" name="id_license" id="id_license" value="<?php echo $proctorv_request[0]["id_license"]; ?>" />
                                    <input type="hidden" name="id_user" id="id_user" value="<?php echo $proctorv_request[0]["id_user"]; ?>" />
                                    <div class="row" id="license_action">
                                    <?php  
                                        echo $btn_revoke; 
                                }
                            }
                            ?>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-sm-2 col-xs-2">
                            <h4><img src="/ilto3/images/iconos/verified.png" title="Pending!" width="32"><span class="green_text"> DONE</span></h4>
                                    <b><h4>1. Test Taker Registration.</h4></b>
                                    <p><?php echo $proctorv_request[0]["nombres"]." ".$proctorv_request[0]["apellidos"]?></p><br/>  
                                    <input type="hidden" name="id_request" id="id_request" value="<?php echo $id_request; ?>" />
                                     <input type="hidden" name="id_admin" id="id_admin" value="<?php echo $id_admin; ?>" />
                                 <?php
                        if($proctorv_request[0]["questions_step"] == 0){
                                    echo '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Pending!" width="32"><span class="red_text"> PENDING TO COMPLETE</span></h4>
                                    <b><h4>2. Security Questions</h4></b><br><br>';
                                }else{
                                    
                                   echo '<h4><img src="/ilto3/images/iconos/verified.png" title="License Allocated!" width="32"><span class="green_text"> DONE</span></h4>
                                    <b><h4>2. Security Questions</h4></b><br><br>';
                                    if(Yii::app()->user->getState('id_perfil')!=5 && Yii::app()->user->getState('id_perfil')!=6){
                                    
                                    echo '<!-- Trigger the modal with a button -->
                                      <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">View Questions</button>
                                    
                                      <!-- Modal -->
                                      <div class="modal fade" id="myModal" role="dialog">
                                        <div class="modal-dialog">
                                        
                                          <!-- Modal content-->
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                              <h4 class="modal-title">User Security Questions - Answers</h4>
                                            </div>
                                            <div class="modal-body">
                                            <ol style="text-align:left;">';
                                            foreach ($proctorv_questions as $value) {
                                               //get question description
                                                $SQL = "SELECT * FROM `proctorv_questions` WHERE `id_proctorv_question` = ".$value["id_proctorv_question"]." ";
                                                $ques = $connection->createCommand($SQL)->queryAll();
                                                echo "<li>".utf8_encode($ques[0]["description"])." - <b>".utf8_encode($value["description"])."</b></li> ";
                                            };
                                            
                                            echo  '</ol>
                                            </div>
                                            <div class="modal-footer">
                                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                          </div>
                                          
                                        </div>
                                      </div>';
                                    }
                                        
                                }
                             ?>
                             
                             
                        </div>
                        <div class="col-lg-4 col-sm-4 col-xs-4" id="photo_step_ajax">
                            <?php echo $photo_step; ?>
                        </div>
                        <div class="col-lg-4 col-sm-4 col-xs-4" id="card_step_ajax">
                             <?php echo $card_step; ?> 
                        </div>
                        <div class="col-lg-2 col-sm-2 col-xs-2" id="card_step_ajax">
                              <?php echo $date_speaking."<br>"; 
                                if($profile == 8){
                                echo $btn_reset_speaking."<br><br>";
                                }
                             echo $date_online."<br>";
                             
                              if($profile == 8){
                                echo $btn_reset_online;
                            }
                             ?>
                             
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-lg-5 col-sm-5 col-xs-5" style="text-align:left;">
                           <h3>TECS2GO Administrator Requirements</h3>  
                        </div>
                        <div class="col-lg-5 col-sm-5 col-xs-5">
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-sm-3 col-xs-3">
                            <?php 
                               echo $license_content;
                             ?>
                            
                        </div>
                        <div class="col-lg-3 col-sm-3 col-xs-3" id="validationIdentity">
                            <?php 
                                echo '<h4>'.$identity_step.'</h4>';
                                
                             ?>
                             
                             
                        </div>
                        <div class="col-lg-3 col-sm-3 col-xs-3" id="status_request">
                            <?php echo $status; ?>
                            <h4>Online Sections</h4><br>
                            
                        </div>
                        <div class="col-lg-3 col-sm-3 col-xs-3">
                            <?php
                             echo $payment;
                            
                            ?>
                            <input type="hidden" name="id_payment" id="id_payment" value="<?php echo $user_payment[0]['id_proctorv_user_payment']; ?>" />
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
</form>
<script>
    

$(document).ready(function(){
    // Get value on button click and show alert
    $("#verifyProfilePhoto").click(function(){
        
        var id_request = $("#id_request").val();
        var id_admin = $("#id_admin").val();
        // action to verify(1)  or deny (0) profile
        var action = 1;
        // 1 to set ProfilePicture 2 to set IdPicture
        var flag = 1;
        
        $.ajax({
			url : "/ilto3/index.php?r=Api/verifyPicture",
			type: "POST",
			dataType: "text",
			data: {
			    request: id_request, flag: flag, action: action, id_admin: id_admin
			},
			success: function(data) {
			    document.getElementById("photo_step_ajax").innerHTML = data;
				location.reload();
			}
		});
                
    });
    
    // Get value on button click and show alert
    //test
    $("#denyProfilePhoto").click(function(){
        
        var id_request = $("#id_request").val();
        var id_admin = $("#id_admin").val();
        // action to verify(1)  or deny (0) profile
        var action = 0;
        // 1 to set ProfilePicture 2 to set IdPicture
        var flag = 1;
        
        $.ajax({
			url : "/ilto3/index.php?r=Api/verifyPicture",
			type: "POST",
			dataType: "text",
			data: {
			    request: id_request, flag: flag, action: action, id_admin: id_admin
			},
			success: function(data) {
			    document.getElementById("photo_step_ajax").innerHTML = data;
			}
		});
                
    });
    // Get value on button click and show alert
    $("#verifyIdPhoto").click(function(){
        
        var id_request = $("#id_request").val();
        var id_admin = $("#id_admin").val();
        // action to verify(1)  or deny (0) profile
        var action = 1;
        // 1 to set ProfilePicture 2 to set IdPicture
        var flag = 2;
        
        $.ajax({
			url : "/ilto3/index.php?r=Api/verifyPicture",
			type: "POST",
			dataType: "text",
			data: {
			    request: id_request, flag: flag, action: action, id_admin: id_admin
			},
			success: function(data) {
				document.getElementById("card_step_ajax").innerHTML = data;
			}
		});
                
    });
    
    // Get value on button click and show alert
    $("#denyIdPhoto").click(function(){
        
        var id_request = $("#id_request").val();
        var id_admin = $("#id_admin").val();
        // action to verify(1)  or deny (0) profile
        var action = 0;
        // 1 to set ProfilePicture 2 to set IdPicture
        var flag = 2;
        
        $.ajax({
			url : "/ilto3/index.php?r=Api/verifyPicture",
			type: "POST",
			dataType: "text",
			data: {
			    request: id_request, flag: flag, action: action, id_admin: id_admin
			},
			success: function(data) {
				document.getElementById("card_step_ajax").innerHTML = data;
			}
		});
                
    });
    
     // Get value on button click and show alert
    $("#enableValidation").click(function(){
        
        var id_request = $("#id_request").val();
        
        $.ajax({
			url : "/ilto3/index.php?r=Api/enableIdentityValidation",
			type: "POST",
			dataType: "text",
			data: {
			    request: id_request
			},
			success: function(data) {
			    document.getElementById("status_request").innerHTML = data;
			    $("#enableValidation").hide();
			}
		});
                
    });
    
    // MATCH OR NOT MATCH FACE PHOTO
    $("#matchPhotoFace").click(function(){
        console.log("aca"); 
        var id_request = $("#id_request").val();
        var id_admin = $("#id_admin").val();
        // 1 to set match, 0 to deny match
        var flag = 1;
        var confirmation = confirm("Do you want to confirm the Match?");
        if(confirmation == true){
           $.ajax({
    			url : "/ilto3/index.php?r=Api/validateMatchFace",
    			type: "POST",
    			dataType: "text",
    			data: {
    			    request: id_request, flag: flag, id_admin: id_admin
    			},
    			success: function(data) {
    			    document.getElementById("photo_step_ajax").innerHTML = data;
    			    document.getElementById("enableValidation").disabled = false;
    			}
    		}); 
        }
        
                
    });
    
    // Get value on button click and show alert
    //test
    $("#NotMatchPhotoFace").click(function(){
        
        var id_request = $("#id_request").val();
        var id_admin = $("#id_admin").val();
        // 1 to set match, 0 to deny match
        var flag = 0;
        var confirmation = confirm("Do you want to revoke the Match?");
        if(confirmation == true){
           $.ajax({
    			url : "/ilto3/index.php?r=Api/validateMatchFace",
    			type: "POST",
    			dataType: "text",
    			data: {
    			    request: id_request, flag: flag, id_admin: id_admin
    			},
    			success: function(data) {
    			    document.getElementById("photo_step_ajax").innerHTML = data;
    			}
    		}); 
        }
        
                
    });
    
    //ajax to get identity status, for enable
    
    $('#revokeLicense').click(function(){
       var confirmation = confirm("Do you want to Revoke this License?");
        var id_request = $("#id_request").val();
        var id_admin = $("#id_admin").val();
        var id_license = $("#id_license").val();
        var id_user = $("#id_user").val();
        
       if(confirmation == true){
          $.ajax({
              url : "/ilto3/index.php?r=Api/revokeLicense",
              type: "POST",
              dataType: "text",
              data: {
                  id_request: id_request, id_admin: id_admin, id_license: id_license, id_user: id_user
              },
              success: function(data){
                  document.getElementById("license_action").innerHTML = data;
              }
          })
       }
        
    });
    
    
     //ajax to UNDO revoked License
    
    $('#UndoRevokeLicense').click(function(){
       var confirmation = confirm("Do you want to REMOVE the Revoked status for this License?");
        var id_request = $("#id_request").val();
        var id_admin = $("#id_admin").val();
        var id_license = $("#id_license").val();
        var id_user = $("#id_user").val();
        
       if(confirmation == true){
          $.ajax({
              url : "/ilto3/index.php?r=Api/UndoRevokeLicense",
              type: "POST",
              dataType: "text",
              data: {
                  id_request: id_request, id_admin: id_admin, id_license: id_license, id_user: id_user
              },
              success: function(data){
                  document.getElementById("license_action").innerHTML = data;
              }
          })
       }
        
    });
    
    
     $('#resetDates').click(function(){
        
        var id_request = $("#id_request").val();
        var id_admin = $("#id_admin").val();
        var id_license = $("#id_license").val();
        var id_user = $("#id_user").val();
        console.log(id_license, id_request, id_admin, id_user);
        var confirmation = confirm("Do you want to reset the Dates?");
       if(confirmation == true){
          $.ajax({
              url : "/ilto3/index.php?r=Api/resetDates",
              type: "POST",
              dataType: "text",
              data: {
                  id_request: id_request, id_admin: id_admin, id_license: id_license, id_user: id_user
              },
              success: function(data){
                 location.reload();
              }
          })
       }
        
    });
    
     $('#resetSpeakingDate').click(function(){
        
        var id_request = $("#id_request").val();
        var id_admin = $("#id_admin").val();
        var id_license = $("#id_license").val();
        var id_user = $("#id_user").val();
        var confirmation = confirm("Do you want to reset the Dates?");
       if(confirmation == true){
          $.ajax({
              url : "/ilto3/index.php?r=Api/resetSpeakingDate",
              type: "POST",
              dataType: "text",
              data: {
                  id_request: id_request, id_admin: id_admin, id_license: id_license, id_user: id_user
              },
              success: function(data){
                 location.reload();
              }
          })
       }
        
    });
    
    //ajax to register tecs2go user payment
    
    $('#jumpToClient').click(function(){
        var confirmation = confirm("Confirm the transfer:");
        var payment_code = $("#payment_code").val();
        var id_user = $("#id_user").val();;
        console.log(payment_code, id_user);
       if(confirmation == true){
          $.ajax({
             // url : "/ilto3/index.php?r=Api/transferAdmin",
              type: "POST",
              dataType: "text",
              data: {
                  id_client: id_client, id_admin: id_admin
              },
              success: function(data){
                // window.location.href ="/ilto3/index.php?r=site/logout&id="+id_admin;
                 window.location.href = "/ilto3/index.php?r=Clients/default/login&type=2";
              }
          })
       }
        
    });
    
    //ajax to send notification 
    
    $('#date_pick_btn').click(function(){
        var confirmation = confirm("Do you want to send a notification email to the user?");
        var id_user = <?php echo $id_user_to_email ?>;
        console.log(id_user);
        if(confirmation == true){
          $.ajax({
              url : "/ilto3/index.php?r=Api/rememberDatePick",
              type: "POST",
              dataType: "text",
              data: {
                  id_user: id_user
              },
              success: function(data){
            
                document.getElementById("date_pick_notify").innerHTML = data; 
                // window.location.href ="/ilto3/index.php?r=site/logout&id="+id_admin;
               
              }
          })
       }
        
    });
    
    //recordar seleccionar fecha speaking
     $('#date_pick_btn_speaking').click(function(){
        var confirmation = confirm("Do you want to send a notification email to the user?");
        var id_user = <?php echo $id_user_to_email ?>;
        console.log(id_user);
        if(confirmation == true){
          $.ajax({
              url : "/ilto3/index.php?r=Api/rememberDatePickSpeaking",
              type: "POST",
              dataType: "text",
              data: {
                  id_user: id_user
              },
              success: function(data){
            
                document.getElementById("date_pick_notify").innerHTML = data; 
                // window.location.href ="/ilto3/index.php?r=site/logout&id="+id_admin;
               
              }
          })
       }
        
    });
    
    //send email to notify photo take
    $('#photo_notify_btn').click(function(){
        var confirmation = confirm("Do you want to send a notification email to the user?");
        var id_user = <?php echo $id_user_to_email ?>;
        console.log(id_user);
        if(confirmation == true){
          $.ajax({
              url : "/ilto3/index.php?r=Api/rememberPhotoPick",
              type: "POST",
              dataType: "text",
              data: {
                  id_user: id_user
              },
              success: function(data){
            
                document.getElementById("photo_notify").innerHTML = data; 
                // window.location.href ="/ilto3/index.php?r=site/logout&id="+id_admin;
               
              }
          })
       }
        
    });
    
    //send email to notify card-photo take
    $('#card_notify_btn').click(function(){
        var confirmation = confirm("Do you want to send a notification email to the user?");
        var id_user = <?php echo $id_user_to_email ?>;
        console.log(id_user);
        if(confirmation == true){
          $.ajax({
              url : "/ilto3/index.php?r=Api/rememberCardPick",
              type: "POST",
              dataType: "text",
              data: {
                  id_user: id_user
              },
              success: function(data){
            
                document.getElementById("card_notify").innerHTML = data; 
                // window.location.href ="/ilto3/index.php?r=site/logout&id="+id_admin;
               
              }
          })
       }
        
    });
    
    //update to OK peding payment 
    $('#confirm_payment_btn').click(function(){
        var confirmation = confirm("Do you want to verify and approve the payment?");
        var id_payment = $("#id_payment").val();
        
        var id_request = $("#id_request").val();
        var id_admin = $("#id_admin").val();
        var id_user = $("#id_user").val();
        flag = 4;
        console.log(id_payment);
        if(confirmation == true){
          $.ajax({
              url : "/ilto3/index.php?r=Api/confirmPayment",
              type: "POST",
              dataType: "text",
              data: {
                  id_payment: id_payment, id_admin: id_admin, id_user: id_user, id_request: id_request, flag: flag
              },
              success: function(data){
            
                document.getElementById("payment_notify").innerHTML = data; 
                // window.location.href ="/ilto3/index.php?r=site/logout&id="+id_admin;
               
              }
          })
       }
        
    });
    
    //update to REJECT peding payment 
    $('#reject_payment_btn').click(function(){
        var confirmation = confirm("Do you want to REJECT the payment?");
        var id_payment = $("#id_payment").val();
        
        var id_request = $("#id_request").val();
        var id_admin = $("#id_admin").val();
        var id_user = $("#id_user").val();
        flag = 6;
        console.log(id_payment);
        if(confirmation == true){
          $.ajax({
              url : "/ilto3/index.php?r=Api/confirmPayment",
              type: "POST",
              dataType: "text",
              data: {
                  id_payment: id_payment, id_admin: id_admin, id_user: id_user, id_request: id_request, flag: flag
              },
              success: function(data){
            
                document.getElementById("payment_notify").innerHTML = data; 
                // window.location.href ="/ilto3/index.php?r=site/logout&id="+id_admin;
               
              }
          })
       }
        
    });
    
    //update to REJECT University payment 
    $('#reject_university_payment_btn').click(function(){
        var confirmation = confirm("Do you want to REJECT the payment?");
        var id_payment = $("#id_payment").val();
        
        var id_request = $("#id_request").val();
        var id_admin = $("#id_admin").val();
        var id_user = $("#id_user").val();
        flag = 6;
        console.log(id_payment);
        if(confirmation == true){
          $.ajax({
              url : "/ilto3/index.php?r=Api/rejectUniPayment",
              type: "POST",
              dataType: "text",
              data: {
                  id_payment: id_payment, id_admin: id_admin, id_user: id_user, id_request: id_request, flag: flag
              },
              success: function(data){
            
                document.getElementById("payment_notify").innerHTML = data; 
                // window.location.href ="/ilto3/index.php?r=site/logout&id="+id_admin;
               
              }
          })
       }
        
    });
    
    //confirm new no register payment 
    $('#confirm_noregister_payment_btn').click(function(){
        var confirmation = confirm("Do you want to register the payment?");
        var id_request = $("#id_request").val();
        var id_admin = $("#id_admin").val();
        var id_user = $("#id_user").val();
        
        console.log(id_payment);
        if(confirmation == true){
          $.ajax({
              url : "/ilto3/index.php?r=Api/confirmNewPayment",
              type: "POST",
              dataType: "text",
              data: {
                  id_request: id_request, id_admin: id_admin, id_user: id_user
              },
              success: function(data){
            
                document.getElementById("payment_notify").innerHTML = data; 
                // window.location.href ="/ilto3/index.php?r=site/logout&id="+id_admin;
               
              }
          })
       }
        
    });
    
});

</script>
<?php
/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = Yii::app()->user->getState('id_perfil');

if($profile == 6){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}


if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}


/* session manager
end
*/

/* @var $this LicensesUserController */
/* @var $model LicensesUser */
$config = Yii::app()->getComponents(false);
$connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
// establish connection. You may try...catch possible exceptions
$connection->active=true;



$this->breadcrumbs=array(
	'Licenses Users'=>array('index'),
	'Manage',
);
$oculta=0;

//verificar si el usuario tiene una licencia TEC

//Verificar el numero de licencias que tiene activas. Si hay más de una no debe dejarle crear más
$totLicencias = LicensesUser::model()->countByAttributes(array('id_usuario'=>$model->id_usuario, 'estado'=>'A'));
$test = LicensesUser::model()->findAllByAttributes(array('id_usuario'=>$model->id_usuario, 'estado'=>'A'));

//$licTecs = LicensesUser::model()->countByAttributes(array('id_usuario'=>$model->id_usuario, 'estado'=>'F', 'id_licenses'=>'6'));

$fecha=strtotime("0000/00/00 00:00:00");
    
    $oculta=0;
    $modUltimoExamen = LicensesUser::model()->findAllByAttributes(array('id_usuario'=>$model->id_usuario));
    
    
    foreach($modUltimoExamen as $rowExam){
        if(strtotime($rowExam->fecha_presentacion) > $fecha){ $fecha=strtotime($rowExam->fecha_presentacion);}
    }
    $segundosAhora = strtotime('now');
    $segundos = $segundosAhora - $fecha;
    $segundos=intval($segundos/60/60/24);
    if($segundos <= 180) {
        $oculta=1;
    }
//if($oculta==0){
$this->menu=array(
	array('label'=>'Allocate License', 'url'=>array('ExamsUser/create/id/'.$id_usuario)),
);  
//}
/*Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#licenses-user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");*/
//echo '<pre>';


$SQL = "SELECT id_licencias_usuario, id_licencia_cliente, id_usuario, `t`.estado, fecha_presentacion, licenses_client.estado as estado_c, fecha_final, id_cliente FROM `licenses_user` `t` INNER JOIN licenses_client ON `t`.id_licencia_cliente = licenses_client.id_licencias_cliente WHERE `t`.`id_usuario` LIKE '".$model->id_usuario."'";
$list_u= $connection->createCommand($SQL)->queryAll();
$alert_revoked = 0;

//check superscore
$modUtil = new Utilidades;
$tecs_ids = $modUtil->getIdsTecs();
$SQL = "SELECT id_licencias_usuario FROM `licenses_user` INNER JOIN licenses_client ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente WHERE `id_usuario` LIKE '".$model->id_usuario."' AND licenses_client.id_licencia IN ($tecs_ids) AND licenses_user.estado = 'F';";
$superscore= $connection->createCommand($SQL)->queryAll();


// agregamos al log del usuario access
$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 4, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'Access test taker licenses list', '".$_GET['id']."')";
$log= Yii::app()->db->createCommand($SQL)->query(); 


//get superscore
$SQL = "SELECT superscore FROM clients WHERE id_cliente = '".Yii::app()->user->getState('cliente')."'";
$superscore_result = $connection->createCommand($SQL)->queryAll();


//superscore var, 0 don't show, 1 show it
$superscore_display = $superscore_result[0]["superscore"];


?>
<style>
#noImgClose {
     position: inherit;
    width: 100%;
    cursor:pointer;
}
</style>


                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="widget">
                                <div class="widget-header ">
                                    <span class="widget-caption">Licenses allocated to <?php echo $model->idUsuario->nombres." ".$model->idUsuario->apellidos;?></span>
                                    <?php 
                                    if(Yii::app()->user->getState('id_perfil')!=5){
                                    ?>
                                        <a href="/ilto3/index.php?r=Clients/ExamsUser/create/id/<?php echo $id_usuario;?>" class="btn btn-success" id="allocate">Allocate License</a>
                                    <?php 
                                    }
                                    ?>
                                </div>
                                <div class="widget-body">
                                    
                                    <table class="table table-striped table-hover table-bordered" id="editabledatatable">
                                        <thead>
                                            <tr role="row">
                                                <th>
                                                    CLIENT LICENSE
                                                </th>
                                                <th>
                                                    TEST DATE (YY-MM-DD)
                                                </th>
                                               
                                                <th>
                                                    STATUS
                                                </th>
                                                <th>
                                                    
                                                </th>
                                                
                                            </tr>
                                        </thead>

                                        <tbody>
                                             <?php 
                                             
                                              if(Yii::app()->user->getState('usuario')=='o.garcia'){
                                                if(!empty($superscore)){
                                                    ?>
                                            <tr role="row">
                                                <th>
                                                   <strong>SUPERSCORE </strong>
                                                </th>
                                                <th>
                                                    
                                                </th>
                                               
                                                <th>
                                                    
                                                </th>
                                                <th>
                                                 <a href="/ilto3/index.php?r=Clients/licensesUser/superScore&amp;id=<?php echo base64_encode(base64_encode($_GET['id']))?>"class="btn btn-primary btn-xs edit" target="_blank" title="Warning: Clicking this button will generate a Certificate"><i class="fa fa-check"></i>View_Certificate</a>   
                                                </th>
                                                
                                            </tr>
                                            <?php 
                                                }
                                              }else{
                                                if($superscore_display == 1 && Yii::app()->user->getState('id_perfil') != 6){
                                                    if(!empty($superscore)){
                                                        ?>
                                                        <tr role="row">
                                                            <th>
                                                               <strong>SUPERSCORE CLIENT</strong>
                                                            </th>
                                                            <th>
                                                                
                                                            </th>
                                                           
                                                            <th>
                                                                
                                                            </th>
                                                            <th>
                                                             <a href="/ilto3/index.php?r=Clients/licensesUser/superScore&amp;id=<?php echo base64_encode(base64_encode($_GET['id']))?>"class="btn btn-primary btn-xs edit" target="_blank" title="Warning: Clicking this button will generate a Certificate"><i class="fa fa-check"></i>View_Certificate</a>   
                                                            </th>
                                                            
                                                        </tr>
                                                        <?php 
                                                    }
                                                }   
                                              }
                                                    ?>
                                            <?php 
                                                foreach($list_u as $rooms){
                                                    
                                                    $SQL =  "SELECT * FROM `licenses_client`, licenses WHERE `id_licencias_cliente` = ".$rooms['id_licencia_cliente']." and licenses.id_licencia=licenses_client.id_licencia ";
                                                    $LicenceClient= $connection->createCommand($SQL)->queryAll();
                                                    //var_dump($LicenceClient);
                                                    $date = ($rooms['fecha_presentacion']);
                                                    $lic = explode("V", $LicenceClient[0]['nombre']);
                                                    ?>
                                                        <tr>
                                                            <td><?php echo $lic[0]?></td>
                                                            <td style="text-align:center;"><?php echo substr($date, 0,16);?></td>
                                                             
                                                            <td class="center "><?php 
                                                                    
                                                                    $status = $rooms['estado'];
                                                                
                                                                    switch ($rooms['estado']) {
                                                                        case 'F':
                                                                            $rooms['estado']= 'COMPLETED';
                                                                            break;
                                                                        case 'A':
                                                                            $rooms['estado']= 'PENDING';
                                                                            break;
                                                                        case 'U':
                                                                            $rooms['estado']= 'UNDER REVISION';
                                                                            break;
                                                                        case 'B':
                                                                            $rooms['estado']= 'REVOKED';
                                                                            $alert_revoked = 1;
                                                                            break;
                                                                        default:
                                                                            $rooms['estado']= 'PENDING';
                                                                    }
                                                                    
                                                                    echo $rooms['estado'];
                                                                    ?>
                                                                    
                                                            </td>
                                                            <td>
                                                                <?php 
                                                                
                                                                    if($status=='F'){
                                                                        if(Yii::app()->user->getState('id_perfil')!=5){
                                                                            
                                                                        ?>
                                                                            <a href="/ilto3/index.php?r=Clients/ExamsUser/admin&id=<?php echo base64_encode($rooms['id_licencias_usuario'])?>" class="btn btn-success btn-xs edit"><i class="fa fa-check"></i>Results</a>
                                                                        <?php 
                                                                            }
                                                                            
                                                                            
                                                                        ?>
                                                                        <a title="View_Certificate" href="/ilto3/index.php?r=Clients/licensesUser/certificate&amp;id=<?php echo $rooms['id_licencias_usuario']?>"class="btn btn-warning btn-xs edit" target="_blank"><i class="fa fa-check"></i>View_Certificate</a>
                                                                        <a title="Email_Certificate" href="/ilto3/index.php?r=Clients/licensesUser/emailcertificate&amp;id=<?php echo $rooms['id_licencias_usuario']?>" class="btn btn-info btn-xs"><i class="fa fa-edit"></i>Send certificate to the test taker</a>
                                                                        
                                                                        <?php
                                                                        
                                                                    }
                                                                    
                                                                    
                                                                    if($status=='U'){
                                                                        if(Yii::app()->user->getState('usuario')=='o.garcia' || Yii::app()->user->getState('usuario')=='partialito'){
                                                                        ?>
                                                                            <a href="/ilto3/index.php?r=Clients/ExamsUser/admin&id=<?php echo base64_encode($rooms['id_licencias_usuario'])?>" class="btn btn-success btn-xs edit" ><i class="fa fa-check"></i>Results</a>
                                                                        <?php
                                                                        }
                                                                    }
                                                                    if($status=='B'){
                                                                        if(Yii::app()->user->getState('usuario')=='o.garcia' || Yii::app()->user->getState('usuario')=='partialito'){
                                                                        ?>
                                                                         <a href="/ilto3/index.php?r=Clients/ExamsUser/admin&id=<?php echo base64_encode($rooms['id_licencias_usuario'])?>" class="btn btn-success btn-xs edit" ><i class="fa fa-check"></i>Results</a>   
                                                                        <?php
                                                                        }
                                                                    }
                                                                    if($status=='A' && Yii::app()->user->getState('id_perfil')!=5){
                                                                        
                                                                            $SQL =  "SELECT COUNT(id_examen_usuario) as exams, fecha_final FROM `exams_user` INNER JOIN licenses_user ON licenses_user.id_licencias_usuario = exams_user.id_licencia_usuario INNER JOIN licenses_client ON licenses_client.id_licencias_cliente = licenses_user.id_licencia_cliente WHERE id_licencia_usuario = '".$rooms['id_licencias_usuario']."' AND exams_user.`estado` ='F' AND licenses_client.estado !='E'";
                                                                            $total= $connection->createCommand($SQL)->queryAll();
                                                                            $now = date("Y-m-d G:i:s");
                                                                            if(Yii::app()->user->getState('id_perfil')!=5){
                                                                            ?>
                                                                                <a href="/ilto3/index.php?r=Clients/ExamsUser/admin&id=<?php echo base64_encode($rooms['id_licencias_usuario'])?>" class="btn btn-success btn-xs edit" ><i class="fa fa-check"></i>Results</a>
                                                                                <?php 
                                                                            }
                                                                            if($total[0]["exams"] == 0){
                                                                                if(strtotime($now) < strtotime($total[0]["fecha_final"])){
                                                                                    ?>   
                                                                                    <a href="/ilto3/index.php?r=Clients/LicensesUser/delete&id=<?php echo $rooms['id_licencias_usuario']?>" id="<?php echo $rooms['id_licencias_usuario']?>" class="btn btn-danger btn-xs edit cd-popup-trigger" ><i class="fa fa-check"></i>Delete License</a>
                                                                                    <?php
                                                                                }
                                                                            } 
                                                                           
                                                                            if(Yii::app()->user->getState('id_perfil')==8){
                                                                                
                                                                               if($rooms["fecha_presentacion"] > date("Y-m-d h:i:sa")){
                                                                                    ?>
                                                                                     <a href="/ilto3/index.php?r=Clients/LicensesUser/enableDate&id=<?php echo $rooms['id_licencias_usuario']?>" id="<?php echo $rooms['id_licencias_usuario']?>" class="btn btn-primary btn-xs edit" ><i class="fa fa-check"></i>Enable Presentation Date</a>
                                                                                    <?php 
                                                                                }
                                                                            }
                                                                             
                                                                    }       
                                                                    $SQL =  "SELECT * FROM `proctorv_request` WHERE `id_license` = ".$rooms['id_licencias_usuario']." ";
                                                                    $tec2goLic= $connection->createCommand($SQL)->queryAll();
                                                                    
                                                                        if(isset($tec2goLic[0])){
                                                                            ?>
                                                                            <a href="/ilto3/index.php?r=Clients/LicensesUser/proctorvRequest/id/<?php echo base64_encode($tec2goLic[0]["id_proctorv_request"])?>" class="btn btn-primary btn-xs edit" ><i class="fa fa-check"></i>TECS2GO</a>
                                                                            <?php
                                                                        }
                                                                        
                                                                ?>
                                                            </td>
                                                        </tr>
                                                    <?php
                                                }
                                            ?>
                                            
                                            
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
    <link rel="stylesheet" href="/ilto3/themes/ilto/html/assets/css/simple-popup/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="/ilto3/themes/ilto/html/assets/css/simple-popup/style.css"> <!-- Resource style -->
	<script src="/ilto3/themes/ilto/html/assets/js/simple-popup/modernizr.js"></script> <!-- Modernizr -->
	<script src="/ilto3/themes/ilto/html/assets/js/simple-popup/main.js"></script> <!-- Modernizr -->
	<div class="cd-popup" role="alert">
    	<div class="cd-popup-container">
    		<p>Please confirm that you want to delete the test taker license:</p>
    		<ul class="cd-buttons">
    			<li><a id="confirm-btn" href="">Yes</a></li>
    			<li><a href"#0" class="cd-popup-close" id="noImgClose">No</a></li>
    		</ul>
    		<a href="#0" class="cd-popup-close img-replace">Close</a>
    	</div> <!-- cd-popup-container -->
    </div> <!-- cd-popup --> 
	
                        
                        <?php /*
                        /ilto3/index.php?r=Clients/licensesUser/emailcertificate&amp;id=<?php echo $rooms['id_licencias_usuario']?>
                        <div class="col-lg-3 col-sm-3 col-xs-12">
                                    <div class="widget">
                                        <div class="widget-header bg-blue">
                                            <i class="widget-icon fa fa-arrow-left"></i>
                                            <span class="widget-caption">USERS INSTRUCTIONS </span>
                                           
                                        </div><!--Widget Header-->
                                        <div class="widget-body">
                                            <div class="instrucciones">Here you can create, edit or delete authorized users who would manage the platforms according to the &nbsp;</div>
                                            <span class="tool_tip veintes" style="margin-top:-28px; font-size: 11px; color:blue; text-decoration: underline;">profile</span>
                                            <br>
                                            <br>
                                            <div class=" cuarenta">
                                                <b>TOAA (Total Access Administrator)</b>, 
                                                <br>This user is able to:
                                                <br>
                                                <ul>
                                                    <li>Manage user accounts (create, delete, modify),</li>
                                                    <li>Allocate test licenses.</li>
                                                    <li>Manage groups or test rooms (Create, delete, modify)</li>
                                                    <li>Purchase test licenses</li>
                                                    <li>Tutor access (Perform the speaking test and assign test licenses)</li>
                                                    <li>Manage the test agenda (set and modify exam dates, approve or reject test takers)</li>
                                                </ul>
                                                <b>PAA (Partial Access Administrator)</b>, This user is able to:<br>
                                                <ul>
                                                    <li>Allocate test licenses,</li>
                                                    <li>Manage groups or test rooms (Create, delete, modify)</li>
                                                    <li>Tutor access (Perform the speaking test and assign test licenses)</li>
                                                    <li>Manage the test agenda (set and modify exam dates, approve or reject test takers)</li>
                                                </ul>
                                                <b>TUAA (Tutor Access Administrator)</b>, This user is able to:<br>
                                                <ul>
                                                    <li>Allocate test licenses,</li>
                                                    <li>See groups or test rooms</li>
                                                    <li>Tutor access (Perform the speaking test and assign test licenses)</li>
                                                    <li>See the test agenda</li>
                                                </ul>
                                                <b>RAA (Report Access Administrator)</b>, This user is able just to see the test reports
                                                <br>
                                                <b>TAKER</b>: This user is able just to take the Exam
                                            </div>
                                        </div><!--Widget Body-->
                                    </div><!--Widget-->
                                </div> */?>
                                
                       
                    </div>


   <!--Page Related Scripts-->
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/jquery.dataTables.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/ZeroClipboard.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.tableTools.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.bootstrap.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/datatables-init.js"></script>
    <script>
        InitiateSimpleDataTable.init();
        InitiateExpandableDataTable.init();
        InitiateSearchableDataTable.init();

    
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            var status = "<?php echo $alert_revoked?>";
            console.log(status);
            if(status=='1'){
                $('#allocate').css({ 'pointer-events': 'none'});
                $('.btn-main').css({ 'pointer-events': 'none'});
            }
        });
        
        
        
        </script>

<?php 



/*
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'licenses-user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
                array(
                    'name'=>'id_licencia_cliente',
                    'value'=>'$data->idLicenciaCliente->idLicencia->nombre'
                ),
		'fecha_asignacion',
                array(
                    'name'=>'estado',
                    'value'=>'$data->estado=="A" ? "Enabled" : ($data->estado=="F" ? "Final" : "Disabled")',
                    'filter'=>array("A"=>"Enable","I"=>"Disable","F"=>"Final"),
                    ),
		array(
			'class'=>'CButtonColumn',
                    'template'=>'{Tests}{Certificate}{Email Certificate}',
                    'buttons'=>array(
                        'delete'=>array(
                            'imageUrl'=>NULL
                        ),
                        'Tests'=>array(
                            'url'=> 'Yii::app()->createUrl("Clients/ExamsUser/admin",array("id"=>$data->id_licencias_usuario))',
                            'imageUrl' => NULL,
                            'label'=>'Results'
                        ),
                        'Certificate'=>array(
                            'url'=> 'Yii::app()->createUrl("Clients/licensesUser/certificate",array("id"=>$data->id_licencias_usuario))',
                            'imageUrl' => NULL,
                            'label'=>'View_Certificate',
                            'visible'=>'$data->estado=="F"',
                        ),
                        'Email Certificate'=>array(
                            'url'=> 'Yii::app()->createUrl("Clients/licensesUser/emailcertificate",array("id"=>$data->id_licencias_usuario))',
                            'imageUrl' => NULL,
                            'label'=>'Email_Certificate',
                            'visible'=>'$data->estado=="F"',
                        )
                    )
		),
	),
)); */
?>

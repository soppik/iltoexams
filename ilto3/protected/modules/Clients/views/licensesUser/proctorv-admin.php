<?php
 /* session manager
 start
*/
$session=new CHttpSession;
$session->open();
$profile = Yii::app()->user->getState('id_perfil');
$id_client = Yii::app()->user->getState('cliente');
if($profile == 6){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}


if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}


/* session manager
end
*/

/* @var $this LicensesUserController */
/* @var $model LicensesUser */
$config = Yii::app()->getComponents(false);
$connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
// establish connection. You may try...catch possible exceptions
$connection->active=true;

if($id_client == '90043026471' ){
    $SQL = "SELECT id_proctorv_request, date_request, step_request, id_license, id_user, questions_step, photo_step, card_step, status_request, nombres, apellidos FROM `proctorv_request` INNER JOIN users_c ON proctorv_request.id_user = users_c.id_usuario_c WHERE id_cliente = '".$id_client."' AND status_request !=4 ORDER BY date_request DESC";
    $proctorv_list= $connection->createCommand($SQL)->queryAll();
    $api_link = '<form class="search-form" action="/ilto3/index.php?r=Api/tecs2goIltoAdmin" onsubmit="return false">';
}else {
    $SQL = "SELECT id_proctorv_request, date_request, step_request, id_license, id_user, questions_step, photo_step, card_step, status_request, nombres, apellidos FROM `proctorv_request` INNER JOIN users_c ON proctorv_request.id_user = users_c.id_usuario_c WHERE id_cliente = '".$id_client."' AND status_request !=4 ORDER BY date_request DESC";
    $proctorv_list= $connection->createCommand($SQL)->queryAll();
    
    $api_link = '<form class="search-form" action="/ilto3/index.php?r=Api/tecs2goAdmin" onsubmit="return false">
                <input type="hidden" value="'.Yii::app()->user->getState('cliente').'" name="id_cliente">';
}


?>
<style>
#noImgClose {
     position: inherit;
    width: 100%;
    cursor:pointer;
}
</style>


<div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="widget">
                                <div class="widget-header ">
                                    <span class="widget-caption">TECS2GO ADMINISTRATOR TABLE</span>
                                </div>
                                <div class="widget-body">
                                    <?php echo $api_link; ?>
                                         <input type="hidden" value="<?php echo Yii::app()->user->getState('cliente') ?>" name="id_cliente">
                                    <table class="table table-striped table-hover table-bordered" id="editabledatatable">
                                        <thead>
                                            <tr role="row">
                                                <th>
                                                    DATE REQUEST
                                                </th>
                                                <th>
                                                    ID
                                                    <br>
                                                    <!--<input type="text" name="id_user" class="search form-control" data-related="proctorv_request.id_user"/>-->
                                                </th>
                                                <th>
                                                   FULL NAME 
                                                </th>
                                                <th>
                                                    SECURITY QUESTIONS
                                                </th>
                                                <th>
                                                    FACE PHOTO
                                                </th>
                                                <th>
                                                    FACE ID CARD
                                                    
                                                </th>
                                                <th>
                                                    FACE ID CARD
                                                </th>
                                                <th>
                                                    SPEAKING
                                                </th>
                                                <th>
                                                    ONLINE SECTION
                                                </th>
                                                <th>
                                                    
                                                </th>
                                                
                                            </tr>
                                        </thead>

                                        <tbody class="search-rows">
                                            <?php
                                             foreach($proctorv_list as $request){
                                             
                                             ?>
                                             <tr>
                                                <td>
                                                    <?php $dat = explode(" ", $request["date_request"]); 
                                                    echo $dat[0];?>
                                                </td>
                                                <td>
                                                    <?php echo $request["id_user"]; ?>
                                                </td>
                                               
                                                <td>
                                                    <?php echo utf8_encode($request["nombres"])." ".utf8_encode($request["apellidos"]); ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                    //questions
                                        	    	if($request["questions_step"]==1){
                                                        echo '<img src="/ilto3/images/iconos/verified.png" title="Done!" width="16">';
                                                    }else{
                                                    	echo '<img src="/ilto3/images/iconos/alert-warning.png" title="Pending!" width="16">'; 
                                                    }
                                                     ?>
                                                </td>
                                                <td>
                                                    <?php  //photo
                                                    switch ($request["photo_step"]) {
                                                        case 0:
                                                        	echo '<img src="/ilto3/images/iconos/alert-warning.png" title="Pending Validation!" width="16">'; 
                                        				break;
                                                        case 1:
                                                        	echo '<img src="/ilto3/images/iconos/verified.png" title="Done and Match!" width="16">&nbsp;<img src="/ilto3/images/iconos/verified.png" title="Done and Match!" width="16">';
                                                        break;
                                                        case 2:
                                                        	echo '<img src="/ilto3/images/iconos/alert-danger.png" title="Pending Photo and Validation!" width="16">'; 
                                                        break;
                                                        case 3:
                                                        	echo '<img src="/ilto3/images/iconos/verified.png" title="Validation Done" width="16"><img src="/ilto3/images/iconos/alert-warning.png" title="Pending Match!" width="16">';
                                                        break;
                                                        case 4:
                                                        	echo '<img src="/ilto3/images/iconos/alert-danger.png" title="Match Failed" width="16">'; 
                                                        break;
                                                        default:
                                                    	    // code...
                                                        break;
                                                    } ?>
                                                </td>
                                                <td>
                                                   <?php //card_step
                                                    switch ($request["card_step"]) {
                                                        case 0:
                                                        	echo'<img src="/ilto3/images/iconos/alert-warning.png" title="Pending Validation!" width="16">'; 
                                                        break;
                                                        case 1:
                                                        	echo '<img src="/ilto3/images/iconos/verified.png" title="Done" width="16">';
                                                        break;
                                                        case 2:
                                                        	echo '<img src="/ilto3/images/iconos/alert-danger.png" title="Pending Photo and Validation!" width="16">'; 
                                                        break;
                                                        default:
                                                        	// code...
                                                        break;
                                                    } ; ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                    //license
                                                    if($request["id_license"] != NULL){
                                                     echo '<img src="/ilto3/images/iconos/verified.png" title="Allocation Done" width="16">'; 
                                                    }else{
                                                       echo'<img src="/ilto3/images/iconos/alert-warning.png" title="Pending Allocation" width="16">';
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                    //license
                                                    if($request["id_license"] != NULL){
                                                        $SQL = "SELECT * FROM exams_user WHERE id_licencia_usuario = '".$request["id_license"]."' ORDER BY id_examen_usuario ASC LIMIT 1";
                                                        $speaking = $connection->createCommand($SQL)->queryAll();
                                                        
                                                        switch($speaking[0]["estado"]){
                                                            case 'A':
                                                                echo '<img src="/ilto3/images/iconos/alert-warning.png" title="Pending Speaking!" width="16">';
                                                            break;
                                                            case 'F':
                                                                echo '<img src="/ilto3/images/iconos/verified.png" title="Speaking Done" width="16">';
                                                            break;
                                                            case 'B':
                                                                echo '<img src="/ilto3/images/iconos/alert-danger.png" title="License REVOKED" width="16">'; 
                                                            break;
                                                        }
                                                        
                                                         
                                                    }else {
                                                        echo '<img src="/ilto3/images/iconos/alert-danger.png" title="Pending License allocation!" width="16">'; 
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    
                                                    <?php switch ($request["status_request"]) {
                                                        case 0:
                                                            echo "PENDING VALIDATIONS";
                                                            break;
                                                        case 1:
                                                            echo "READY TO TAKE SPEAKING TEST";
                                                            break;
                                                        case 2:
                                                            echo "READY TO TAKE SPEAKING TEST";
                                                        break;
                                                        case 3:
                                                            echo "READY TO TAKE ONLINE SECTIONS";
                                                        break;
                                                        case 4:
                                                            echo "DONE";
                                                        break;
                                                        case 5:
                                                            echo "REVOKED";
                                                            break;
                                                        default:
                                                            // code...
                                                            break;
                                                    } ; ?>
                                                </td>
                                                <td>
                                                    <a href="/ilto3/index.php?r=Clients/LicensesUser/proctorvRequest/id/<?php echo base64_encode($request["id_proctorv_request"]);?>" class="btn btn-success" id="allocate">Admin</a>
                                                </td>
                                                
                                            </tr>
                                             <?php
                                             }
                                             ?>
                                            
                                            
                                            
                                        </tbody>
                                    </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                        


   <!--Page Related Scripts-->
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/jquery.dataTables.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/ZeroClipboard.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.tableTools.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.bootstrap.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/datatables-init.js"></script>
    <script>
        InitiateSimpleDataTable.init();
        InitiateExpandableDataTable.init();
        InitiateSearchableDataTable.init();
    </script>


<?php 


?>

<?php
/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = $session['profile'];


if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}

if($profile == 6){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}

/* session manager
end
*/

/* @var $this FraudReportController */
/* @var $model FraudReport */

$this->breadcrumbs=array(
	'Create Users'=>array('index'),
	'transfer',
);

$this->menu=array(
	array('label'=>'Transfer', 'url'=>array('transfer')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#fraud-report-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

$modUtil = new Utilidades();
$types = $modUtil->arregloTiposFraude();

$config = Yii::app()->getComponents(false);
$connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
// establish connection. You may try...catch possible exceptions
$connection->active=true;

if(isset($_REQUEST['search_box'])){
    $SQL = "SELECT * FROM `fraud_report`, clients WHERE fraud_report.id_client = clients.id_cliente and (fraud_report.`document` LIKE '%".$_REQUEST['search_box']."%' OR first_name LIKE '%".$_REQUEST['search_box']."%' OR sure_name LIKE '%".$_REQUEST['search_box']."%' )";
    $list_u= $connection->createCommand($SQL)->queryAll();    
}else{
//    $SQL = "SELECT * FROM `fraud_report`, clients WHERE fraud_report.id_client = clients.id_cliente and fraud_report.`id_client` LIKE '".Yii::app()->user->getState('cliente')."'";
    $SQL = "SELECT * FROM `fraud_report` ORDER BY report_date ASC LIMIT 50";
    $list_u= $connection->createCommand($SQL)->queryAll();
}

$id_cliente = Yii::app()->user->getState('cliente');
?>
<style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
    /* display: none; <- Crashes Chrome on hover */
    -webkit-appearance: none;
    margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
}

#alertR_lbl {
		color:#fff;
		background-color:#d73d32 !important;
		padding:15px;
		text-align:center;
	}
    
</style>

    
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
                                    <div class="widget">
                                        <div class="widget-header bg-blue">
                                            <i class="widget-icon fa fa-arrow-left"></i>
                                            <span class="widget-caption">WARNING</span>
                                           
                                        </div><!--Widget Header-->
                                        <div class="widget-body">
                                            <div>
                                             Please enter the ID number of the test taker you need to enable in your system.</div>
                                        </div><!--Widget Body-->
                                    </div><!--Widget-->
                                </div>
                                
                       
                    </div>
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="widget">
                                <div class="widget-header ">
                                     </div>
                                <div class="widget-body">
                                    <input class="search_box" name="search_box" type="text" id="search_box" style="width:350px;" value="<?php echo $busca;?>">
                                    <input name="btnSearch" class="btn btn-warning btn-xs edit" type="submit" value="Search">  <span class="instrucciones">Enter the ID number of the test taker. </span><br/><br/>
                                     
                                    
                                    <form action="/ilto3/index.php?r=Clients/usersC/UserTransfer" method="POST">
                                    <input name="id_cliente" type="hidden" value="<?php echo (Yii::app()->user->getState('cliente'));?>">
                                    <table class="table table-striped table-hover table-bordered" id="editabledatatable">
                                        <thead>
                                            <tr role="row" style="text-align:center;">
                                                <th>
                                                    ID NUMBER
                                                </th>
                                                <th>
                                                    NAME
                                                </th>
                                                <th>
                                                    SURNAME
                                                </th>
                                                <th>
                                                    EMAIL
                                                </th>
                                                <th>
                                                    AGENT
                                                </th>
                                                <th>
                                                    GROUP
                                                </th>
                                                <th>
                                                    
                                                </th>
                                                <tr id="id_validate">
                                                    
                                                    
                                                </tr>
                                            </tr>
                                        </thead>

                                            <tbody>
                                        </tbody>
                                    </table>
                                      </form>
                                </div>
                            </div>
                        </div>
                        
                        


   <!--Page Related Scripts-->
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/jquery.dataTables.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/ZeroClipboard.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.tableTools.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.bootstrap.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/datatables-init.js"></script>
    <script>
        InitiateSimpleDataTable.init();
        InitiateExpandableDataTable.init();
        InitiateSearchableDataTable.init();

    $("#search_box").change(function() {
		var id = $("#search_box").val();
		var id_cliente = <?php echo (json_encode($id_cliente)); ?>;
		$.ajax({
			url : "/ilto3/index.php?r=Api/getUserInfo",
			type: "POST",
			dataType: "text",
			data: {
			    id: id,
			    id_cliente: id_cliente
			},
			success: function(data) {
				$("#id_validate").html(data);
			}
		});
	});
	
	$( "#send" ).click(function() {
      alert( "The order will be send to ILTO and the next 24 hours it will be attend." );
    });
    </script>

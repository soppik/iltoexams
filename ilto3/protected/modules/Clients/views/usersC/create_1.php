<?php
/* @var $this UsersCController */
/* @var $model UsersC */

$this->breadcrumbs=array(
	'Client Users'=>array('index'),
	'Create',
);
//Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/swfobject.js');
//Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/scriptCam/scriptcam.js');
//Include librerias para editor de fotos
$baseUrl = Yii::app()->baseUrl; 
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/js/picEditMaster/picedit.min.js');
$cs->registerCssFile($baseUrl.'/js/picEditMaster/picedit.min.css');
//Fin de librerias de fotos

$this->menu=array(
	array('label'=>'Back to Manager', 'url'=>array('admin')),
);
$this->layout = "//layouts/column2a";
?>

<h1>Create Users</h1>

<?php $this->renderPartial('_form_1', array('model'=>$model)); ?>



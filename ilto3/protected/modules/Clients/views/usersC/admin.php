<?php
/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = Yii::app()->user->getState('id_perfil');
if($profile == 6){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}


if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}


/* session manager
end
*/

// agregamos al log del usuario access
$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 4, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'Access users list', '')";
$log= Yii::app()->db->createCommand($SQL)->query(); 


/* @var $this UsersCController */
/* @var $model UsersC */

$this->breadcrumbs=array(
	'Users Cs'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create Users', 'url'=>array('create')),
);

/*
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#users-c-grid').yiiGridView('update', {
		data: $(this).serialize()
	}); 
	return false;
});
");
*/

$config = Yii::app()->getComponents(false);
$connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
// establish connection. You may try...catch possible exceptions
$connection->active=true;


//SELECT * FROM `groups_users_clients` `t` WHERE id_cliente='12340987'   
//SELECT * FROM `users_c` `t` WHERE id_cliente='12340987' and id_perfil >= 2 LIMIT 50
//SELECT `idGrupoCliente`.`id` AS `t1_c0`, `idGrupoCliente`.`id_cliente` AS `t1_c1`, `idGrupoCliente`.`nombre` AS `t1_c2`, `idGrupoCliente`.`estado` AS `t1_c3` FROM `groups_users_clients` `idGrupoCliente`  WHERE (id_cliente='12340987') AND (`idGrupoCliente`.`id`=NULL)

$SQL = "SELECT * FROM `groups_users_clients` `t` WHERE id_cliente='".Yii::app()->user->getState('cliente')."' ";
$list_g= $connection->createCommand($SQL)->queryAll();

$SQL = "SELECT * FROM `users_c` `t` WHERE id_cliente='".Yii::app()->user->getState('cliente')."' order by id_cliente DESC LIMIT 50";
$list_u= $connection->createCommand($SQL)->queryAll();

$SQL = "SELECT * FROM `profiles` `t` WHERE estado='A' and id_perfil != '1'";
$list_t= $connection->createCommand($SQL)->queryAll();
        
$Status = array("A"=>"Enabled", "I"=>"Disabled");

$this->layout = "//layouts/column2a";

?>

<style>
    .table-bordered {
        border: 1px solid #ddd;
        font-size: 12px;
    }
    td.center {
        font-size: 11px;
    }
    .btn-xs {
        font-size: 11px;
        padding: 2px 4px;
    }
    
    @media (max-width: 1400px) {
      .col-lg-9.col-sm-9.col-xs-12 {
        width: 100%!important;
      }
      
      .col-lg-3.col-sm-3.col-xs-12 {
        width: 100%!important;
      }
    }
</style>


<div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="widget">
                                <div class="widget-header ">
                                    <span class="widget-caption">Users</span>
                                    <a href="/ilto3/index.php?r=Clients/usersC/create" class="btn btn-success">Create Users</a>
                                </div>
                                <div class="widget-body">
                                    <form class="search-form" action="/ilto3/index.php?r=Api/user" onsubmit="return false">
                                         <input type="hidden" value="<?php echo Yii::app()->user->getState('cliente') ?>" name="id_cliente">
                                    <table class="table table-striped table-hover table-bordered" id="editabledatatable">
                                        <thead>
                                            <tr role="row">
                                                <th>
                                                    PROFILE
                                                    <br>
                                                    <select class="form-control search" name="id_perfil" data-related="users_c.id_perfil">
                                                        <option value="">
                                                            
                                                        </option>
                                                        <?php
                                                            foreach($list_t as $g){
                                                                ?>
                                                                    <option value="<?php echo $g['id_perfil']?>">
                                                                        <?php echo $g['nombre']?>
                                                                    </option>
                                                                    
                                                                <?php
                                                            }
                                                        ?>
                                                    </select>
                                                </th>
                                                <th>
                                                    GROUPS
                                                    <br>
                                                    <select class="form-control search" name="id_grupo_cliente" data-related="users_c.id_grupo_cliente">
                                                        <option value="">
                                                            
                                                        </option>
                                                        <?php
                                                            foreach($list_g as $g){
                                                                ?>
                                                                    <option value="<?php echo $g['id']?>">
                                                                        <?php echo utf8_encode($g['nombre'])?>
                                                                    </option>
                                                                    
                                                                <?php
                                                            }
                                                        ?>
                                                    </select>
                                                </th>
                                                <th>
                                                    USERNAME / ID
                                                    <br>
                                                    <input type="text" name="id_usuario_c" class="search form-control" data-related="users_c.id_usuario_c"/>
                                                </th>
                                                <th>
                                                    NAME
                                                    <br>
                                                    <input type="text" name="nombres" class="search form-control" data-related="users_c.nombres"/>
                                                </th>
                                                <th>
                                                    SURNAME
                                                    <br>
                                                    <input type="text" name="apellidos" class="search form-control" data-related="users_c.apellidos"/>
                                                </th>
                                                <th>
                                                    STATUS
                                                    <br>
                                                    <select class="form-control search" name="status" data-related="users_c.estado">
                                                        <option value="">
                                                            
                                                        </option>
                                                         <?php
                                                            foreach($Status as $key=>$g){
                                                                ?>
                                                                    <option value="<?php echo $key?>">
                                                                        <?php echo $g?>
                                                                    </option>
                                                                    
                                                                <?php
                                                            }
                                                        ?>
                                                    </select>
                                                </th>
                                                <th>
                                                    
                                                </th>
                                                
                                            </tr>
                                        </thead>

                                        <tbody class="search-rows">
                                            
                                            <?php 
                                                foreach($list_u as $rooms){
                                                    
                                                    $SQL = "SELECT * FROM `profiles` WHERE `id_perfil` = ".(int)$rooms['id_perfil']." ";
                                                    $userData= $connection->createCommand($SQL)->queryAll();
                                                    
                                                    
                                                    $SQL = "SELECT * FROM `groups_users_clients` WHERE `id` = ".(int)$rooms['id_grupo_cliente']." ORDER BY `id` DESC ";
                                                    $userGroup= $connection->createCommand($SQL)->queryAll();
                                                    
                                                    ?>
                                                        <tr class="<?php echo $rooms['id_usuario_c']?>">
                                                            <td><?php echo utf8_encode($userData[0]['nombre'])?></td>
                                                            <td><?php echo utf8_encode($userGroup[0]['nombre'])?></td>
                                                            <td><?php echo $rooms['id_usuario_c']?></td>
                                                            <td><?php echo utf8_encode($rooms['nombres'])?></td>
                                                            <td><?php echo utf8_encode($rooms['apellidos'])?></td>
                                                            
                                                            <td class="center "><?php
                                                                switch ($rooms['estado']) {
                                                                    case 'A':
                                                                       $status = 'ENABLED';
                                                                        break;
                                                                    case 'B':
                                                                        $status = 'DISABLED';
                                                                        break;
                                                                    default:
                                                                        // code...
                                                                        break;
                                                                }
                                                            
                                                            echo $status;
                                                            
                                                            ?></td>
                                                            <td>
                                                                
                                                                <?php 
                                                                    if($rooms['id_perfil'] == 2){
                                                                        ?>
                                                                            <a href="/ilto3/index.php?r=Clients/UsersC/view&id=<?php echo $rooms['id_usuario_c']?>" class="btn btn-warning btn-xs edit"><i class="fa fa-edit"></i>View</a>
                                                                        <?php
                                                                    }
                                                                    
                                                                    if($rooms['id_perfil'] == 3){
                                                                        ?>
                                                                            <a href="/ilto3/index.php?r=Clients/UsersC/view&id=<?php echo $rooms['id_usuario_c']?>" class="btn btn-warning btn-xs edit"><i class="fa fa-edit"></i>View</a>
                                                                            <a href="/ilto3/index.php?r=Clients/UsersC/update&id=<?php echo $rooms['id_usuario_c']?>" class="btn btn-info btn-xs edit"><i class="fa fa-edit"></i>Edit</a>
                                                                            
                                                                            <?php
                                                                    }
                                                                    
                                                                    if($rooms['id_perfil'] == 6){
                                                                        ?>
                                                                            <a href="/ilto3/index.php?r=Clients/LicensesUser/admin&id=<?php echo $rooms['id_usuario_c']?>" class="btn btn-success btn-xs edit"><i class="fa fa-check"></i>Licenses</a>
                                                                            <a href="/ilto3/index.php?r=Clients/UsersC/view&id=<?php echo $rooms['id_usuario_c']?>" class="btn btn-warning btn-xs edit"><i class="fa fa-edit"></i>View</a>
                                                                            
                                                                            <?php 
                                                                                $SQL = "SELECT COUNT(*) as total FROM `licenses_user` `t` WHERE `t`.`id_usuario`='".$rooms['id_usuario_c']."' ";
                                                                                $LicensesCount = $connection->createCommand($SQL)->queryAll();
                                                                                if($LicensesCount[0]['total']==0){
                                                                                ?>
                                                                                    <a href="/ilto3/index.php?r=Clients/UsersC/update&id=<?php echo $rooms['id_usuario_c']?>" class="btn btn-info btn-xs edit"><i class="fa fa-edit"></i>Edit</a>
                                                                                    <a title="Delete" href="/ilto3/index.php?r=Clients/UsersC/delete&id=<?php echo $rooms['id_usuario_c']?>" class="btn btn-danger btn-xs delete" value="<?php echo $rooms['id_usuario_c']?>"><i class="fa fa-trash-o"></i>Delete</a>  <?php
                                                                                }
                                                                            ?>
                                                                        <?php
                                                                    }
                                                                    
                                                                    
                                                                    
                                                                    if($rooms['id_perfil'] == 4){
                                                                        ?>
                                                                            <a href="/ilto3/index.php?r=Clients/UsersC/view&id=<?php echo $rooms['id_usuario_c']?>" class="btn btn-warning btn-xs edit"><i class="fa fa-edit"></i>View</a>
                                                                            <a href="/ilto3/index.php?r=Clients/UsersC/update&id=<?php echo $rooms['id_usuario_c']?>" class="btn btn-info btn-xs edit"><i class="fa fa-edit"></i>Edit</a>
                                                                           
                                                                         <?php
                                                                    }
                                                                    
                                                                     if($rooms['id_perfil'] == 5){
                                                                        ?>
                                                                            <a href="/ilto3/index.php?r=Clients/UsersC/view&id=<?php echo $rooms['id_usuario_c']?>" class="btn btn-warning btn-xs edit"><i class="fa fa-edit"></i>View</a>
                                                                            <a href="/ilto3/index.php?r=Clients/UsersC/update&id=<?php echo $rooms['id_usuario_c']?>" class="btn btn-info btn-xs edit"><i class="fa fa-edit"></i>Edit</a>
                                                                            <?php
                                                                            
                                                                    }
                                                                ?>
                                                                
                                                                
                                                            </td>
                                                        </tr>
                                                    <?php
                                                }
                                            ?>
                                            
                                            
                                            
                                        </tbody>
                                    </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-lg-3 col-sm-3 col-xs-12">
                                    <div class="widget">
                                        <div class="widget-header bg-blue">
                                            <i class="widget-icon fa fa-arrow-left"></i>
                                            <span class="widget-caption">PROFILE DESCRIPTION </span>
                                           
                                        </div><!--Widget Header-->
                                        <div class="widget-body">
                                            <div>
                                            	<b>TOAA (Total Access Administrator)</b>, This user is able to:<br><ul><li>Manage user accounts (create, delete, modify),</li><li>Allocate test licenses.</li><li>Manage groups or test rooms (Create, delete, modify)</li><li>Purchase test licenses</li><li>Tutor access (Perform the speaking test and assign test licenses)</li><li>Manage the test agenda (set and modify exam dates, approve or reject test takers)</li></ul>
						                        <b>PAA (Partial Access Administrator)</b>, This user is able to:<br><ul><li>Allocate test licenses,</li><li>Manage groups or test rooms (Create, delete, modify)</li><li>Tutor access (Perform the speaking test and assign test licenses)</li><li>Manage the test agenda (set and modify exam dates, approve or reject test takers)</li></ul>
						                        <b>TUAA (Tutor Access Administrator)</b>, This user is able to:<br><ul><li>Allocate test licenses,</li><li>See groups or test rooms</li><li>Tutor access (Perform the speaking test and assign test licenses)</li><li>See the test agenda</li></ul>
						                        <b>RAA (Report Access Administrator)</b>, This user is able just to see the test reports<br><b>TAKER</b>: This user is able just to take the Exam
						                    </div>
                                        </div><!--Widget Body-->
                                    </div><!--Widget-->
                                </div>
                                
                       
                    </div>


   <!--Page Related Scripts-->
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/jquery.dataTables.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/ZeroClipboard.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.tableTools.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.bootstrap.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/datatables-init.js"></script>
    <script>
        InitiateSimpleDataTable.init();
        InitiateExpandableDataTable.init();
        InitiateSearchableDataTable.init();
    </script>
    
<script>
jQuery(document).on('click', '.btn.btn-danger.btn-xs.delete',function(e){
    e.preventDefault();
        var url = jQuery(this).attr('href')
        var id = $(this).attr('value')
        jQuery.post(url, { id: $(this).attr('value')}, function(data){
             console.log(data)
             var cl = '.';
             var clas = cl.concat(id)
             jQuery(clas).remove()
        })
})
    
</script>
    
<?php


?>







<?php 

/*
//var_dump($data->id_perfil);
$cliente = Yii::app()->user->getState('cliente');

$visible=true; $txtVisible='true';
//if($cliente=='86051730214'){ $visible=false; $txtVisible ='false';} else {$visible=true; $txtVisible='true';}
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-c-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
                array(
                    'name'=>'id_perfil',
                    'value'=>'$data->idPerfil->nombre',
                    'filter'=>CHtml::listData(Profiles::model()->findAllByAttributes(array('es_cliente'=>1)),"id_perfil","nombre"),
                    'htmlOptions'=>array('style'=>'width:100px;')
                ),
                array(
                    'name'=>'id_grupo_cliente',
                    'value'=>'$data->idGrupoCliente->nombre',
                    'filter'=>CHtml::listData(GroupsUsersClients::model()->findAll(),"id","nombre"),
                ),
		'id_usuario_c',
		'nombres',
		'apellidos',
                array(
                    'name'=>'estado',
                    'value'=>'$data->traeEstado()',
                    'filter'=> array("A"=>"Enabled","P"=>"Pending","R"=>"Registered","I"=>"Disabled"),
                    ),
	
		array(
			'class'=>'CButtonColumn',
                    'template'=>'{update}{view}{Licenses}{delete}',
                    'buttons'=>array(
                        'update'=>array(
                            'label'=>'Edit&nbsp;', 
                            'url'=> 'Yii::app()->createUrl("Clients/UsersC/update",array("id"=>$data->id_usuario_c))',
                            'visible' => '(Yii::app()->user->getState("id_perfil") <= 3  && ($data->id_perfil > 2 && $data->id_perfil <=6) && '.$txtVisible.') ? true:false',
                            'imageUrl'=>NULL
                        ),
                        'view'=>array(
                            'label'=>'View&nbsp;',
                            'url'=> 'Yii::app()->createUrl("Clients/UsersC/view",array("id"=>$data->id_usuario_c))',
                            'imageUrl'=>NULL
                        ),
                        'delete'=>array(
                            'url'=> 'Yii::app()->createUrl("Clients/UsersC/delete",array("id"=>$data->id_usuario_c))',
                            'imageUrl'=>NULL,
                            'label'=>'Delete', 
                            'visible' => '$data->seElimina()',
                        ),
                        'Email'=>array(
                            'url'=> 'Yii::app()->createUrl("Clients/UsersC/email",array("id"=>$data->id_usuario_c,"login"=>1))',
                            'imageUrl'=>NULL,
                            'label'=>'Email&nbsp;',
                            'visible' => '$visible',
                            
                        ),
                        'Licenses'=>array(
                            'url'=> 'Yii::app()->createUrl("Clients/LicensesUser/admin",array("id"=>$data->id_usuario_c))',
                            'visible'=>'(Yii::app()->user->getState("id_perfil") <= 4  && $data->id_perfil == 6 && '.$txtVisible.') ? true:false',
                            'imageUrl'=>NULL,
                            'label'=>'Licenses'
                        ),
                    )
		),
	),
)); ?>
<script>
        $(".tool_tip").mouseover(function(){
           eleOffset = $(this).offset();
          $(this).next().fadeIn("fast").css({
                                  left: eleOffset.left + $(this).outerWidth(),
                                  top: eleOffset.top
                          });
          }).mouseout(function(){
                  $(this).next().hide();
          });
$('.sort-link').attr('title','Click to Sort');  
    </script> */ ?>
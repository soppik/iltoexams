<?php
/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = $session['profile'];


if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}

if($profile == 6){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}

/* session manager
end
*/

/* @var $this UsersCController */
/* @var $model UsersC */
//Include librerias para editor de fotos
/*$baseUrl = Yii::app()->baseUrl; 
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/js/picEditMaster/picedit.min.js');
$cs->registerCssFile($baseUrl.'/js/picEditMaster/picedit.min.css');*/
//Fin de librerias de fotos

$config = Yii::app()->getComponents(false);
$connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
// establish connection. You may try...catch possible exceptions
$connection->active=true;


$this->breadcrumbs=array(
	'Client Users'=>array('index'),
	$model->id_usuario_c=>array('view','id'=>$model->id_usuario_c),
	'Update',
);

$this->menu=array(
	array('label'=>'Create Users', 'url'=>array('create')),
	array('label'=>'Back to Manager', 'url'=>array('admin')),
);
$this->layout = "//layouts/column2a";

	$sql='SELECT * FROM country';
	$list= $connection->createCommand($sql)->queryAll();
?>

<style>
	
	select#combo_perfil {
	    width: 100%;
	    
	}
	select {
	    height: 33px;
	    border-radius: 0;
	}
	.btn-file input[type=file] {
	    position: absolute;
	    top: 0;
	    right: 0;
	    min-width: 100%;
	    min-height: 100%;
	    font-size: 100px;
	    text-align: right;
	    filter: alpha(opacity=0);
	    opacity: 0;
	    outline: none;
	    background: #fff;
	    cursor: inherit;
	    display: block;
	}
	
	.custom-upload{
	    text-align: center;
	    margin-left: auto;
	    margin-right: auto;
	    display: block;
	    position: relative;
	    margin-left: 25%;
	}
</style>
<?php 
	$sql='SELECT * FROM country';
	$list= $connection->createCommand($sql)->queryAll();
	
	if($model->id_perfil == 6){
		$sql='SELECT estado FROM licenses_user where id_usuario = "'.$model->id_usuario_c.'"';
		$lics= $connection->createCommand($sql)->queryAll();

	$flagUpdate = 0;
	
	foreach ($lics as $lic_status) {
		if($lic_status['estado']=="F"){
			$flagUpdate = 0;
		}
	}
	}
?>


<?php //$this->renderPartial('_form', array('model'=>$model)); ?>


<div class="row">
<div class="col-lg-8 col-sm-8 col-xs-12">
    <div class="widget flat radius-bordered">
        <div class="widget-header bg-blue">
            <span class="widget-caption">UPDATE USER <?php echo $model->id_usuario_c; ?></span>
        </div>
        <div class="widget-body">
            <div id="registration-form">
                <form role="form" id="users-c-form" enctype="multipart/form-data" action="/ilto3/index.php?r=Clients/UsersC/update&id=<?php echo $model->id_usuario_c; ?>" method="post">
                    <div class="form-title">
                        User Information
                    </div>
                    <div class="form-group">
                        <span class="input-icon icon-right">
                            <input type="text" class="form-control" name="UsersC[nombres]" id="UsersC_nombres" placeholder="First Name" maxlength="100" size=60 value="<?php echo $model->nombres; ?>">
                            <i class="glyphicon glyphicon-user circular"></i>
                        </span>
                    </div>
                     <div class="form-group">
                        <span class="input-icon icon-right">
                            <input type="text" class="form-control" name="UsersC[apellidos]" id="UsersC_apellidos" placeholder="Surname"  maxlength="100" size=60 value="<?php echo $model->apellidos; ?>">
                            <i class="glyphicon glyphicon-user circular"></i>
                        </span>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <span class="input-icon icon-right">
                                	<label>COUNTRY:</label>
			                    	<br>
                                    <select name="UsersC[id_pais]" required class="country-select" id="country-select" style="width:100%">
                                    	<option value="" default></option>
									<?php
			                            foreach($list as $paises){
			                                ?>
			                                    <option id="<?php echo $paises['Code']?>" value="<?php echo $paises['Code']?>"><?php echo utf8_encode($paises['Name'])?></option>
			                                <?php
			                            }
			                        ?>
									</select>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <span class="input-icon icon-right">
                                	<label>CITY:</label>
									<br>
                                    <select id="city-select" name="UsersC[id_ciudad]" required class="ciudades-select" class="city-select" style="width:100%">
                                    	
                                    </select>
                                </span>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="form-group">
                        <span class="input-icon icon-right">
                            <input type="text" class="form-control" maxlength="200" size="60" required="required" id="emailInput" name="UsersC[email]" id="UsersC_email" value="<?php echo $model->email; ?>" placeholder="Email Address">
                            <i class="fa fa-envelope-o circular"></i>
                        </span>
                    </div>
                    <hr class="wide">
                    
                    <div id="datosTaker" style="<?php echo ($model->id_perfil == 6)?'display: block;':'display: none;'; ?>">
                    	
			        <div class="form-group">
			        	<div class="col-sm-12" style="padding: 0;">
			        	    <label for="UsersC_id_grupo_cliente">User Group</label>                    
			        	   
			        	    <br>
			        		<select id="combo_grupo" name="UsersC[id_grupo_cliente]" style="width:100%">
								<option value="" selected="selected"></option>
								
								<?php 
									$SQL = "SELECT * FROM `groups_users_clients` WHERE `id_cliente` LIKE '".Yii::app()->user->getState('cliente')."' ORDER BY `groups_users_clients`.`id` ASC";
									$list= $connection->createCommand($SQL)->queryAll();
									
									foreach($list as $groups){
										?>
											<option value="<?php echo $groups['id']?>"><?php echo utf8_encode($groups['nombre'])?></option>
										<?php
									}
								?>
								
								
							</select>   
							
							 <script>
		                    	 jQuery(document).ready(function(){
		                            	jQuery('select[name="UsersC[id_grupo_cliente]"]').val('<?php echo $model->id_grupo_cliente; ?>')
		                         });
		                    </script>
						</div>        
					
			    	</div>
			    		<br>
			        	<p><b><i style="    color: grey;">Please select the group where the user belongs to</i></b></p>
			    	<div class="form-group">
			            <div class="col-sm-12">
			        		<input id="ytUsersC_email_certificate" type="hidden" value="0" name="UsersC[email_certificate]" >
			        		
							
							<div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input name="UsersC[email_certificate]" id="UsersC_email_certificate"  type="checkbox" value="1" <?php echo ($model->email_certificate==1)?'checked=checked':''; ?>>
                                                                <span class="text">Send TECS certificate to the test taker.</span>
                                                            </label>
                                                        </div>
                                                    </div>

		        		</div>
			        </div>
			    	<br>
				    <br>
			    </div>
			    <div class="clear" style="margin-bottom:30px">
			    	
			    </div>
			   
                   
                 
                   

                <div class="form-group">
				         <div class="col-sm-6">
				         	<label id="comb_estado" for="combo_estado">Status</label>    	
				    		<br>
		                	<select id="combo_estado" name="UsersC[estado]" class="form-control">
								<option value=""></option>
								<option value="A">Enabled</option>
								<option value="I">Disabled</option>
							</select>
				    	</div>
		    	</div>
    			<br>
    			<br>
    			<div class="form-group" style="margin-top:20px;">
			        <div class="col-sm-12">
	    				<input class="btn btn-blue" type="submit"  name="yt0" value="Save">
	    			</div>
    			</div>
    			<script>
                	 jQuery(document).ready(function(){
                        	jQuery('select[name="UsersC[estado]"]').val('<?php echo $model->estado; ?>')
                     });
                </script>
    			<div class="clear"></div>
    			<br>
    			<br>
    			<br>
    			<br>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-4 col-sm-4 col-xs-12">
                                    <div class="widget">
                                        <div class="widget-header bg-blue">
                                            <i class="widget-icon fa fa-arrow-left"></i>
                                            <span class="widget-caption">PROFILE DESCRIPTION</span>
                                           
                                        </div><!--Widget Header-->
                                        <div class="widget-body">
                                            <div>
                                            	<b>TOAA (Total Access Administrator)</b>, This user is able to:<br><ul><li>Manage user accounts (create, delete, modify),</li><li>Allocate test licenses.</li><li>Manage groups or test rooms (Create, delete, modify)</li><li>Purchase test licenses</li><li>Tutor access (Perform the speaking test and assign test licenses)</li><li>Manage the test agenda (set and modify exam dates, approve or reject test takers)</li></ul>
						                        <b>PAA (Partial Access Administrator)</b>, This user is able to:<br><ul><li>Allocate test licenses,</li><li>Manage groups or test rooms (Create, delete, modify)</li><li>Tutor access (Perform the speaking test and assign test licenses)</li><li>Manage the test agenda (set and modify exam dates, approve or reject test takers)</li></ul>
						                        <b>TUAA (Tutor Access Administrator)</b>, This user is able to:<br><ul><li>Allocate test licenses,</li><li>See groups or test rooms</li><li>Tutor access (Perform the speaking test and assign test licenses)</li><li>See the test agenda</li></ul>
						                        <b>RAA (Report Access Administrator)</b>, This user is able just to see the test reports<br><b>TAKER</b>: This user is able just to take the Exam
						                    </div>
                                        </div><!--Widget Body-->
                                    </div><!--Widget-->
                                </div>
                                
                                </div>
                                <script>
                                $(document).ready(function(){
                                	
                                 var flagState = "<?php echo $flagUpdate ?>"
                                 if(flagState==1){
                                 	$("#country-select").prop('disabled', true);
                                 	$("#city-select").prop('disabled', true);
                                 	$("#combo_grupo").prop('disabled', true);
                                 	$("#UsersC_ruta_foto").prop('disabled', true);
                                 	
                                 }
      
							      var country = "<?php echo $model->id_country ?>"
							      var city = "<?php echo $model->id_ciudad ?>"
							      
							    if(country!=""){
							        document.getElementById(country).selected = "true";
							        loadCity($(this).find(':selected').val(), city)
							        
							    }
							      
								$('#country-select').change(function() {
									    loadCity($(this).find(':selected').val())
									    document.getElementById(city).selected = "true";
								})
								})
				
								function loadCity(countryId){
								        $("#city-select").children().remove()
								        $.ajax({
								            type: "POST",
								            url: "/ajax.php",
								            data: "get=city&countryId=" + countryId
								            }).done(function( result ) {
								                $(result).each(function(i,j){
								                    $("#city-select").append($('<option>', {
								                        id: j.ID,
								                        value: j.ID,
								                        text: j.Name,
								                    }));
								                })
								            });
								}
                                
                                	$("#combo_perfil").change(function(){
								        if($("#combo_perfil").val()=="6"){
								           $('#datosTaker').show(); 
								           $('#picture').hide();
								           $('#btnTaker').show();  
								           document.getElementById('numero_id_lbl').innerHTML = 'Id number';
								        } else {
								           $('#datosTaker').hide();   
								           $('#picture').show();
								           $('#btnTaker').hide(); 
								            document.getElementById('numero_id_lbl').innerHTML = 'Username'; 
								        }
								    })
								    
								    $('#UsersC_ruta_foto').bind('change', function() {
									    if(this.files[0].size > '2048000'){
									            alert('The image excedes the size limit. Please select an image up to 2 Mb');
									            $('#UsersC_ruta_foto').val("");
									            document.getElementById('UsersC_ruta_foto').value = '';
									
									    }
									});
                                </script>
                                
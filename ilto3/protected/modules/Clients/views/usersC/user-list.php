<?php
/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = $session['profile'];


if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}

if($profile == 6){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}


$id_admin = Yii::app()->user->getState('usuario');

if($id_admin != 'o.garcia' || $id_admin != 'partialito'){
	//var_dump($id_admin);die();
    // $this->redirect('index.php?r=Clients/default/login&message=12');
}

/* session manager
end
*/

/* @var $this UsersCController */
/* @var $model UsersC */
$config = Yii::app()->getComponents(false);
$connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
// establish connection. You may try...catch possible exceptions
$connection->active=true;
//echo "<pre>";
//var_dump($noRegTakerArray);
$this->breadcrumbs=array(
	'Client Users'=>array('index'),
	'Create_users',
);
//Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/swfobject.js');
//Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/scriptCam/scriptcam.js');
//Include librerias para editor de fotos
/*$baseUrl = Yii::app()->baseUrl; 
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/js/picEditMaster/picedit.min.js');
$cs->registerCssFile($baseUrl.'/js/picEditMaster/picedit.min.css');*/
//Fin de librerias de fotos

$this->menu=array(
	array('label'=>'Back to Manager', 'url'=>array('admin')),
);
$this->layout = "//layouts/column2a";

$idClient = Yii::app()->user->getState('cliente');
$postData = json_encode(array($alreadyRegistered));
?>
<style>
	
	select#combo_perfil {
	    width: 100%;
	    
	}
	select {
	    height: 33px;
	    border-radius: 0;
	}
	.btn-file input[type=file] {
	    position: absolute;
	    top: 0;
	    right: 0;
	    min-width: 100%;
	    min-height: 100%;
	    font-size: 100px;
	    text-align: right;
	    filter: alpha(opacity=0);
	    opacity: 0;
	    outline: none;
	    background: #fff;
	    cursor: inherit;
	    display: block;
	}
	
	.custom-upload{
	    text-align: center;
	    margin-left: auto;
	    margin-right: auto;
	    display: block;
	    position: relative;
	    margin-left: 25%;
	}
	#picture {
		display:none;
	}
	.btn-blue {
		margin-left: 10px;
	}
	input[type=number]::-webkit-outer-spin-button,
	input[type=number]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
	}
	
	input[type=number] {
    -moz-appearance:textfield;
	}
	
	#alertG_lbl {
		color:#fff;
		background-color:#53a93f !important;
		padding:15px;
	}
	
	#alertR_lbl {
		color:#fff;
		background-color:#d73d32 !important;
		padding:15px;
	}
	
	#photo_name, #photo_name_2 {
		padding: 10px 140px;
    	height: 34px;
	}
	
	.btn-main {
		color:#fff;
	}
	
	.checked_img {
		display:none;
	}
	    
</style>


<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
		<div class="widget">
		    	<div class="widget-header bg-blue">
		            <span class="widget-caption">Check Takers List (CSV) by TECS2GO Status </span>
		        </div><!--Widget Header-->
		        <div class="widget-body">
		        	<div>
		        		<h4>Steps to check taker Status using a CSV file</h4>
		        		<p>	<b>1.</b>	Upload the file using the “<b>select file</b>” button. Click Check.<br/>
<b>2.</b>	The CSV file must have one taker per file, separated by comas with no space, use the sample from the list.csv file attached. The taker information in the CSV file must the as follows:<br/>
	  <span style="margin:auto;font-style: italic;">id_taker,name,email,phone</span>

		        		</p>
						</div>
		        </div><!--Widget Body-->
		</div><!--Widget-->
	</div><!--Widget-->    
	<div class="col-lg-8 col-sm-8 col-xs-12">
	    <div class="widget flat radius-bordered">
	        <div class="widget-header bg-blue">
	            <span class="widget-caption">Check Taker List (CSV FILE)</span>
	        </div>
	        <div class="widget-body">
	        	<form role="form" id="users-c-form" enctype="multipart/form-data" action="/ilto3/index.php?r=Clients/usersC/checkList" method="post" >
	            	<div id="registration-form">
	                	<div class="form-group">
	                    	<span class="input-icon icon-right">
	                        	<!--<input type="file" class="form-control" name="UsersC[file]" id="file" placeholder="CSV" >-->
	                            <input type="file" name="UsersC[file]" id="file" class="inputfile" multiple="" />
								<label for="file" id="photo_name"><strong>Select CSV File</strong></label>
								<img class="checked_img" id="checked_img" src="/ilto3/images/iconos/checked.png" height="16"/>
								<i class="glyphicon glyphicon-user circular"></i>
	                        </span>
	                    </div>
	            	</div>
					<div class="row" style="padding:20px 0px;">
						<div class="col-sm-12">
							<input type="submit" name="submit" id="submit" value="CHECK" class="btn btn-success btn-main form-control">
						</div>
                    </div>
                </form>
	    	</div>
		</div>
	</div>
</div>
<div class="row" id="results">
	<div class="col-lg-12 col-sm-12 col-xs-12">
	    <div class="widget flat radius-bordered">
	        <div class="widget-header bg-blue">
	            <span class="widget-caption">RESULTS</span>
	            
	        </div>
	        <div class="widget-body">
	        	 <table class="table table-striped table-hover table-bordered" id="editabledatatable">
                                        <thead>
                                            <tr role="row">
                                            	<td>
                                            		ID TAKER	
                                            	</td>
                                            	<td>
                                            		FULL NAME	
                                            	</td>
                                            	<td>
                                            		EMAIL	
                                            	</td>
                                            	<td>
                                            		CONTACT	
                                            	</td>
                                            	<td>
                                            	TECS2GO	REGISTERED	
                                            	</td>
                                            	<td>
                                            		TECS2GO LICENSE	
                                            	</td>
                                            	<td>
                                            		CLIENT	
                                            	</td>
                                            	<td>
                                            		LICENSE STATUS 	
                                            	</td>
                                            	
                                            </tr>
				<?php
				if(isset($alreadyRegistered)){
					?>
						<form action="/ilto3/index.php?r=Clients/UsersC/ReportCheckList" method="POST"> 
							<input type="hidden" name="reportInfo" id="reportInfo" value="<?php echo htmlspecialchars(json_encode($alreadyRegistered)) ?>" class="btn btn-success btn-main form-control">
							<input type="submit" class="btn btn-danger" id="reportDownload" name="reportDownload" value="Export Results" />
						</form>
						
					<?php		
					foreach ($alreadyRegistered as $item) {
			                    echo "<tr><td>".$item[0]."</td><td>".utf8_encode($item[1])."</td><td>".utf8_encode($item[2])."</td><td>".utf8_encode($item[3])."</td><td>".$item[4]."</td><td>".$item[5]."</td><td>".$item[6]."</td></td><td>".$item[7]."</td></tr>";
			                };
				}
				
				?>
			</div>
		</div>
	</div>
</div>
	
	<script src="/ilto3/themes/ilto/html/assets/js/simple-popup/main.js"></script> <!-- Modernizr -->

    <!--Bootstrap Date Picker-->
    <script src="/ilto3/themes/ilto/html/assets/js/datetime/bootstrap-datepicker.js"></script>

    <!--Bootstrap Time Picker-->
    <script src="/ilto3/themes/ilto/html/assets/js/datetime/bootstrap-timepicker.js"></script>
<script>
	//tal cual Dk
	 //$('.date-picker').datepicker();

$("#file").change(function(){
    var photoName = $("#file").val();
    var finalName = photoName.split("fakepath");
    console.log(finalName); 
    $("#photo_name").html(finalName[1].substring(1));
    $("#checked_img").show();
});




</script>
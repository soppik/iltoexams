<?php
/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = $session['profile'];


if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}

if($profile == 6){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}

/* session manager
end
*/

/* @var $this UsersCController */
/* @var $model UsersC */
$config = Yii::app()->getComponents(false);
$connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
// establish connection. You may try...catch possible exceptions
$connection->active=true;
//echo "<pre>";
//var_dump($noRegTakerArray);
$this->breadcrumbs=array(
	'Client Users'=>array('index'),
	'Create_users',
);
//Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/swfobject.js');
//Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/scriptCam/scriptcam.js');
//Include librerias para editor de fotos
/*$baseUrl = Yii::app()->baseUrl; 
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/js/picEditMaster/picedit.min.js');
$cs->registerCssFile($baseUrl.'/js/picEditMaster/picedit.min.css');*/
//Fin de librerias de fotos

$this->menu=array(
	array('label'=>'Back to Manager', 'url'=>array('admin')),
);
$this->layout = "//layouts/column2a";

$sql='SELECT * FROM country';
$countries= $connection->createCommand($sql)->queryAll();

$idClient = Yii::app()->user->getState('cliente');

$SQL= "SELECT id, name from city WHERE CountryCode LIKE 'COL' AND id IN (2257,2259,2265,2275)";
$cities = $connection->createCommand($SQL)->queryAll();


?>
<style>
	
	select#combo_perfil {
	    width: 100%;
	    
	}
	select {
	    height: 33px;
	    border-radius: 0;
	}
	.btn-file input[type=file] {
	    position: absolute;
	    top: 0;
	    right: 0;
	    min-width: 100%;
	    min-height: 100%;
	    font-size: 100px;
	    text-align: right;
	    filter: alpha(opacity=0);
	    opacity: 0;
	    outline: none;
	    background: #fff;
	    cursor: inherit;
	    display: block;
	}
	
	.custom-upload{
	    text-align: center;
	    margin-left: auto;
	    margin-right: auto;
	    display: block;
	    position: relative;
	    margin-left: 25%;
	}
	#picture {
		display:none;
	}
	.btn-blue {
		margin-left: 10px;
	}
	input[type=number]::-webkit-outer-spin-button,
	input[type=number]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
	}
	
	input[type=number] {
    -moz-appearance:textfield;
	}
	
	#alertG_lbl {
		color:#fff;
		background-color:#53a93f !important;
		padding:15px;
	}
	
	#alertR_lbl {
		color:#fff;
		background-color:#d73d32 !important;
		padding:15px;
	}
	
	#photo_name, #photo_name_2 {
		padding: 10px 140px;
    	height: 34px;
	}
	
	.btn-main {
		color:#fff;
	}
	
	.checked_img {
		display:none;
	}
	    
</style>


<?php //$this->renderPartial('_form', array('model'=>$model)); ?>
<?php 
	$sql='SELECT * FROM country';
	$country= $connection->createCommand($sql)->queryAll();

if(!is_null($invalidId) > 0 || !is_null($alreadyRegistered)>0 || !is_null($noExamAssing)>0){
	echo 
	'<div class="row">
		<div class="col-lg-12 col-sm-12 col-xs-12">
			<div class="widget">
		    	<div class="widget-header bg-blue">
		        	<i class="widget-icon fa fa-arrow-left"></i>
		            <span class="widget-caption">CSV SUMMARY</span>
				</div><!--Widget Header-->
		        <div class="widget-body">
		        	<div>';
		        	if(!empty($invalidId)){
		        		echo '<b>The test takers have an invalid ID:</b><br/><br/>';
		            	foreach ($invalidId as $value) {
		            		echo "<span>".$value."</span>, ";
		            	}
		        	}
		        	
		        	if($alreadyRegistered){
		        		echo '<br/><br/><b>The test takers ID are already registered:</b><br/><br/>';
		            	foreach ($alreadyRegistered as $value) {
		            		echo "<span>".$value."</span>, ";
		            	}
		        	}
		        	
		        	if($noExamAssing){
		        		echo '<br/><br/><b>The assing exams was unsucesfull for the following ID Taker:</b><br/><br/>';
		            	foreach ($noExamAssing as $value) {
		            		echo "<span>".$value."</span>, ";
		            	}
		        	}
		            	
				echo'</div>
		        </div><!--Widget Body-->
		    </div><!--Widget-->
		</div>
	</div>';
}
?>



<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
		<div class="widget">
		    	<div class="widget-header bg-blue">
		            <span class="widget-caption">Multiple users creator (CSV File) </span>
		        </div><!--Widget Header-->
		        <div class="widget-body">
		        	<div>
		        		<h4>Steps to create several test takers using a CSV file</h4>
		        		<p>	<b>1.</b>	Upload the file using the “<b>select file</b>” button. Click send.<br/>
<b>2.</b>	The CSV file must have one taker per file, separated by comas with no space, use the sample from the list.csv file attached. The taker information in the CSV file must the as follows:<br/>
	  <span style="margin:auto;font-style: italic;">id_taker,name,surname,email</span>

		        		</p>
						</div>
		        </div><!--Widget Body-->
		</div><!--Widget-->
	</div><!--Widget-->    
	<div class="col-lg-8 col-sm-8 col-xs-12">
	    <div class="widget flat radius-bordered">
	        <div class="widget-header bg-blue">
	            <span class="widget-caption">MULTIPLE USERS CREATOR (CSV FILE)</span>
	        </div>
	        <div class="widget-body">
	        	<form role="form" id="users-c-form" enctype="multipart/form-data" action="/ilto3/index.php?r=Clients/usersC/csvUsers" method="post" >
	            	<div id="registration-form">
	                	<div class="form-group">
	                    	<span class="input-icon icon-right">
	                        	<!--<input type="file" class="form-control" name="UsersC[file]" id="file" placeholder="CSV" >-->
	                            <input type="file" name="UsersC[file]" id="file" class="inputfile" multiple="" />
								<label for="file" id="photo_name"><strong>Select CSV File</strong></label>
								<img class="checked_img" id="checked_img" src="/ilto3/images/iconos/checked.png" height="16"/>
								<i class="glyphicon glyphicon-user circular"></i>
	                        </span>
	                    </div>
	            	</div>
	            	<div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <span class="input-icon icon-right">
                                	<label>COUNTRY:</label>
			                    	<br>
                                    <select oninvalid="this.setCustomValidity('Please select a country')" name="UsersC[id_pais]" required class="country-select" id="country-select" style="width:100%">
                                    	<option value="" default></option>
									<?php
			                        foreach($countries as $country){
									?>
			                        	<option value="<?php echo $country['Code']?>"><?php echo utf8_encode($country['Name'])?></option>
									<?php
			                        }
			                        ?>
									</select>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <span class="input-icon icon-right">
                                	<label>CITY:</label>
									<br>
                                    <select id="city-select" name="UsersC[id_ciudad]" required class="ciudades-select" class="city-select" style="width:100%">
                                    	
                                    </select>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
				        <div class="col-sm-12">
				        	<label for="UsersC_id_grupo_cliente">GROUP</label><br>
				        	<select id="combo_grupo" name="UsersC[id_grupo_cliente]" style="width:100%">
								<option value="" selected="selected"></option>
								<?php 
								$SQL = "SELECT * FROM `groups_users_clients` WHERE `id_cliente` LIKE '".Yii::app()->user->getState('cliente')."' ORDER BY `groups_users_clients`.`id` ASC";
								$list= $connection->createCommand($SQL)->queryAll();
								foreach($list as $groups){
											?>
												<option value="<?php echo $groups['id']?>"><?php echo utf8_encode($groups['nombre'])?></option>
											<?php
										}
									?>
								</select>            
						</div>        
					</div>
					<div class="row" style="padding:20px 0px;">
						<div class="col-sm-12">
							<input type="submit" name="submit" id="submit" value="SEND" class="btn btn-success btn-main form-control">
						</div>
                    </div>
                </form>
	    	</div>
		</div>
	<div class="col-lg-4 col-sm-4 col-xs-12">
		
	</div>
</div>
<div class="row">
	<div class="col-lg-8 col-sm-8 col-xs-12">
	    <div class="widget flat radius-bordered">
	        <div class="widget-header bg-blue">
	            <span class="widget-caption">MULTIPLE USERS CREATOR AND ASSING EXAMS (CSV FILE)</span>
	        </div>
	        <div class="widget-body">
	        	<form role="form" id="users-c-form" enctype="multipart/form-data" action="/ilto3/index.php?r=Clients/usersC/csvUsersExams<?php if($idClient == '86051730212'){ echo "Fuaa";  }?>" method="post" >
	            <div id="registration-form">
	            	 <span class="input-icon icon-right">
	            		<input type="file" name="UsersC[file_2]" id="file_2" class="inputfile"  multiple="" />
						<label for="file_2" id="photo_name_2"><strong>Select CSV File</strong></label>
						<img class="checked_img" id="checked_img_2" src="/ilto3/images/iconos/checked.png" height="16"/>
	                    <i class="glyphicon glyphicon-user circular"></i>
					</span>
	            <div class="form-group">
	                 <div class="form-group" style="padding: 0;margin: 0;">
						<span class="input-icon icon-right">
							<label>License</label><br>
							<?php
							$SQL = "SELECT * FROM `licenses_client` WHERE `id_cliente` LIKE '".Yii::app()->user->getState('cliente')."' AND estado != 'F'";
							$licenses= $connection->createCommand($SQL)->queryAll();
							$quantity = count($licenses);
							if($quantity>0){
								echo '<select class="form-control search" name="license_client" data-related="users_c.estado" id="combo_licencia">
                                <option value=""></option>';
                                foreach($licenses as $lic){
                                	$SQL = "SELECT nombre FROM `licenses` WHERE `id_licencia` = '".$lic["id_licencia"]."'";
									$lic_name= $connection->createCommand($SQL)->queryAll();
									echo '<option value="'.$lic["id_licencias_cliente"].'">'.$lic_name[0]["nombre"].'</option>';
                                }                                          
                                echo '</select><br/>';
							}
							?>
						</span>
					</div>
					<div class="form-group" style="padding: 0;margin: 0;">
						<span class="input-icon icon-right">
							<label>Test Room</label><br>
							<?php
							$SQL = "SELECT * FROM `class_rooms` WHERE `id_cliente` LIKE '".Yii::app()->user->getState('cliente')."' AND estado != 'F'";
							$rooms= $connection->createCommand($SQL)->queryAll();
							$quantity = count($rooms);
							if($quantity>0){
								echo '<select class="form-control search" name="room" data-related="users_c.estado">
                                <option value=""></option>';
                                foreach($rooms as $room){
                                	echo '<option value="'.$room["id"].'">'.utf8_encode($room["nombre"]).'</option>';
                                }                                          
                                echo '</select><br/>';
							}
							?>
						</span>
					</div>
					<div class="form-group"  id="divTutor" style="padding: 0;margin: 0;">
						<span class="input-icon icon-right">
							<label>Tutor</label><br>
							<?php 
							$SQL = "SELECT id_usuario_c, nombres, apellidos FROM `users_c` WHERE `id_cliente` LIKE '".Yii::app()->user->getState('cliente')."' AND `id_perfil` IN (2,3,4) AND estado = 'A'";
							$tutors= $connection->createCommand($SQL)->queryAll();
							$quantity = count($tutors);
							if($quantity>0){
            					//aca voy
								echo '<select class="form-control search" name="tutor" data-related="users_c.estado">
					            <option value=""></option>';
					            foreach($tutors as $tutor){
					            	echo '<option value="'.$tutor["id_usuario_c"].'">'.utf8_encode($tutor["nombres"]).'</option>';
					            }                                          
					            echo '</select><br/>';
								}
								?>
						</span>
					</div>
					<div class="form-group" style="padding: 0;margin: 0;">
						<span class="input-icon icon-right"><br>
							<div class="well with-header">
								<div class="header bordered-pink">Test Date
								</div>
								<div>
									<div class="input-group">
										<input class="form-control date-picker" id="ExamsUser_fecha_presentacion" type="text" name="date" onkeyup="checkPass(); return false;" required="required">
										<span class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</span>
									</div>
								</div>
							</div>
						</span>
						<div class="well with-header">
							<div class="header bordered-blue">Hour
							</div>
							<div>
								<div class="input-group">
									<input class="form-control" id="combo_horas" type="text" name="hour">
								    <span class="input-group-addon">
								    	<i class="fa fa-clock-o"></i>
								    </span>
								</div>
							</div>
						</div>
					</div>
						<?php 
						// SI ES UN ADMINISTRADOR DE LA FUAA CARGAMOS ESTOS ADICIONALES
						if($idClient != '8605173021'){
						?>
							<div class="col-sm-6">
		                	<div class="form-group">
		                    	<span class="input-icon icon-right">
		                        	<label>COUNTRY:</label><br>
		                            <select oninvalid="this.setCustomValidity('Please select a country')" name="UsersC[id_pais]" required class="country-select" id="country-select-two" style="width:100%">
		                            	<option value="" default></option>
										<?php
					                    foreach($countries as $country){
										?>
					                    	<option value="<?php echo $country['Code']?>"><?php echo utf8_encode($country['Name'])?></option>
										<?php
					                    }
					                    ?>
									</select>
		                        </span>
		                    </div>
		                </div>
		                <div class="col-sm-6">
		                	<div class="form-group">
		                    	<span class="input-icon icon-right">
		                        	<label>CITY:</label><br>
		                            <select name="UsersC[id_ciudad]" required class="ciudades-select" id="city-select-two" style="width:100%">
		                            </select>
		                        </span>
		                    </div>
		                 </div>
		            </div>
		            <div class="row">
						<div class="col-sm-12">
							<label for="UsersC_id_grupo_cliente">GROUP</label><br>
							<select id="combo_grupo" name="UsersC[id_grupo_cliente]" style="width:100%">
								<option value="" selected="selected"></option>
								<?php 
								$SQL = "SELECT * FROM `groups_users_clients` WHERE `id_cliente` LIKE '".Yii::app()->user->getState('cliente')."' ORDER BY `groups_users_clients`.`id` ASC";
								$list= $connection->createCommand($SQL)->queryAll();
								foreach($list as $groups){
								?>
									<option value="<?php echo $groups['id']?>"><?php echo utf8_encode($groups['nombre'])?></option>
								<?php
								}
								?>
							</select>            
						</div>        
					</div>	
						<?php 
						}else{
						?>
						
					<?php 
					}
					?>
					<div class="row" style="padding:20px 0px;">
						<div class="col-sm-12">
			            	<span class="input-icon icon-right">
			                	<input type="submit" name="submit" id="submit" value="SEND" class="btn btn-success btn-main form-control">
							</span>
	                    </div>
	            	</div>
	            </form>	
	            </div>
	        </div>
		</div>
	</div>
</div>
<
	
	<script src="/ilto3/themes/ilto/html/assets/js/simple-popup/main.js"></script> <!-- Modernizr -->

    <!--Bootstrap Date Picker-->
    <script src="/ilto3/themes/ilto/html/assets/js/datetime/bootstrap-datepicker.js"></script>

    <!--Bootstrap Time Picker-->
    <script src="/ilto3/themes/ilto/html/assets/js/datetime/bootstrap-timepicker.js"></script>
<script>
	//tal cual Dk
	 //$('.date-picker').datepicker();
		var nowTemp = new Date();
		var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
		 
		var checkin = $('.date-picker').datepicker({
		  onRender: function(date) {
		    return date.valueOf() < now.valueOf() ? 'disabled' : '';
		  }
		}).on('changeDate', function(ev) {
		  if (ev.date.valueOf() > checkout.date.valueOf()) {
		    var newDate = new Date(ev.date)
		    newDate.setDate(newDate.getDate() + 1);
		    checkout.setValue(newDate);
		  }
		  checkin.hide();
		  $('#dpd2')[0].focus();
		}).data('datepicker');
		var checkout = $('#dpd2').datepicker({
		  onRender: function(date) {
		    return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
		  }
		}).on('changeDate', function(ev) {
		  checkout.hide();
}).data('datepicker');

        //--Bootstrap Time Picker--
       //$('#combo_horas').timepicker();
       $(function () {
		    $('#combo_horas').timepicker({
		    });
		});
  $("#combo_horas").change(function(){
    
  var nowTemp = new Date();
  var day = nowTemp.getDate();
  var dateInput = $('#ExamsUser_fecha_presentacion').val(); 
  var res = dateInput.split("/");
  var hour = $('#combo_horas').val(); 
  var res2 = hour.split(" ");
  var newH = res2[0].split(":");  
  var hToCheck = parseInt(newH[0]);
  var mToCheck = parseInt(newH[1]); 
  var dToCheck = parseInt(res[1]);
  var d = new Date();
  var da = d.getDate();
  var h = d.getHours();
  var m = d.getMinutes(); 
  
  
  if(res2[1]=="PM"){
    hToCheck = hToCheck + 12;
  }

if(dToCheck == da){
  if(hToCheck <= h){
    if(mToCheck < m){
       if(h<=24){
        var replace = h+1 + ":00";
       }
      $("#combo_horas").val(replace);
    }else{
    	if(h<=24){
        var replace = h + ":30";
       }
      $("#combo_horas").val(replace);
    } 
  } 
} 

});
$('#combo_licencia').change(function(){
    var licencia_cliente = $('#combo_licencia').val();
       $.get('index.php?r=Clients/ExamsUser/tieneTutor/id/'+licencia_cliente, function(data){
        var datos = $.parseJSON(data);
        if(datos.estutor==="1"){
            $('#divTutor').show();
            $('#tieneTutor').val('1');
        } else {
            $('#divTutor').hide();
            $('#tieneTutor').val('0');
        }
    });
});

$("#file").change(function(){
      var photoName = $("#file").val();
      var finalName = photoName.split("fakepath");
    console.log(finalName); 
    $("#photo_name").html(finalName[1].substring(1));
    $("#checked_img").show();
});

$("#file_2").change(function(){
      var photoName = $("#file_2").val();
      var finalName = photoName.split("fakepath");
    console.log(finalName); 
    $("#photo_name_2").html(finalName[1].substring(1));
    $("#checked_img_2").show();
});

$(document).ready(function(){
	$('#country-select').change(function() {
		loadCity($(this).find(':selected').val())
	})
})

function loadCity(countryId){
	$("#city-select").children().remove()
		$.ajax({
			type: "POST",
			url: "/ajax.php",
			data: "get=city&countryId=" + countryId
		}).done(function( result ) {
			$(result).each(function(i,j){
				$("#city-select").append($('<option>', {
					value: j.ID,
					text: j.Name,
			}));
		})
	});
}

$(document).ready(function(){
	$('#country-select-two').change(function() {
		loadCity2($(this).find(':selected').val())
	})
})

function loadCity2(countryId){
	$("#city-select-two").children().remove()
		$.ajax({
			type: "POST",
			url: "/ajax.php",
			data: "get=city&countryId=" + countryId
		}).done(function( result ) {
			$(result).each(function(i,j){
				$("#city-select-two").append($('<option>', {
					value: j.ID,
					text: j.Name,
			}));
		})
	});
}

//ajax to get program by idcity
$("#city").change(function() {
	var id = $("#city").val();
	$.ajax({
		url : "/ilto3/index.php?r=Api/getPrograms",
		type: "POST",
		dataType: "text",
		data: {
		id: id,
		},
		success: function(data) {
		    $("#program_container").html(data);
		    $("#program_container").show();
		    $("#period_container").show();
	    }
    });
});



</script>
<?php
/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = $session['profile'];

/* session manager
end
*/

/* @var $this UsersCController */
/* @var $model UsersC */
$config = Yii::app()->getComponents(false);
$connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
// establish connection. You may try...catch possible exceptions
$connection->active=true;



if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}


if(Yii::app()->user->getState('id_perfil') != 8){
	if(Yii::app()->user->getState('id_perfil') > 3){
		Yii::app()->user->setFlash('error', 'Acces Denied.');
	    $this->redirect('index.php?r=Clients/default/index');
	}

}



if(Yii::app()->user->getState('id_perfil') == 6){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}



if(Yii::app()->user->getState('usuario')=='o.garcia' || Yii::app()->user->getState('usuario')=='partialito'){
	$SQL = "SELECT * FROM `profiles` WHERE `id_perfil` NOT IN (1,2)"; 	
}else{
 	$SQL = "SELECT * FROM `profiles` WHERE `id_perfil` NOT IN (1,2,7,8,9)"; 
}
 
$profiles = $connection->createCommand($SQL)->queryAll();



$this->breadcrumbs=array(
	'Client Users'=>array('index'),
	'Create',
);
//Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/swfobject.js');
//Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/scriptCam/scriptcam.js');
//Include librerias para editor de fotos
/*$baseUrl = Yii::app()->baseUrl; 
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/js/picEditMaster/picedit.min.js');
$cs->registerCssFile($baseUrl.'/js/picEditMaster/picedit.min.css');*/
//Fin de librerias de fotos

$this->menu=array(
	array('label'=>'Back to Manager', 'url'=>array('admin')),
);
$this->layout = "//layouts/column2a";

?>
<style>
	
	select#combo_perfil {
	    width: 100%;
	    
	}
	select {
	    height: 33px;
	    border-radius: 0;
	}
	.btn-file input[type=file] {
	    position: absolute;
	    top: 0;
	    right: 0;
	    min-width: 100%;
	    min-height: 100%;
	    font-size: 100px;
	    text-align: right;
	    filter: alpha(opacity=0);
	    opacity: 0;
	    outline: none;
	    background: #fff;
	    cursor: inherit;
	    display: block;
	}
	
	.custom-upload{
	    text-align: center;
	    margin-left: auto;
	    margin-right: auto;
	    display: block;
	    position: relative;
	    margin-left: 25%;
	}
	#picture {
		display:none;
	}
	.btn-blue {
		margin-left: 10px;
	}
	input[type=number]::-webkit-outer-spin-button,
	input[type=number]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
	}
	
	input[type=number] {
    -moz-appearance:textfield;
	}
	
	#alertG_lbl {
		color:#fff;
		background-color:#53a93f !important;
		padding:15px;
	}
	
	#alertR_lbl {
		color:#fff;
		background-color:#d73d32 !important;
		padding:15px;
	}
	
</style>


<?php //$this->renderPartial('_form', array('model'=>$model)); ?>
<?php 
	$sql='SELECT * FROM country';
	$list= $connection->createCommand($sql)->queryAll();
	//var_dump($list);
                    
?>
<div class="row">
<div class="col-lg-8 col-sm-8 col-xs-12">
    <div class="widget flat radius-bordered">
        <div class="widget-header bg-blue">
            <span class="widget-caption">CREATE USER</span>
        </div>
        <div class="widget-body">
            <div id="registration-form">
                <form role="form" id="users-c-form" enctype="multipart/form-data" action="/ilto3/index.php?r=Clients/usersC/create" method="post" >
                    <div class="form-title">
                        User Information
                    </div>
                    <div class="form-group">
                        <span class="input-icon icon-right">
                            <input type="text" class="form-control" name="UsersC[nombres]" id="UsersC_nombres" placeholder="Name" maxlength="100" size=60 oninvalid="setCustomValidity('This field is required')" oninput="setCustomValidity('')"  required pattern=".*\S+.*" title="This field is required">
                            <i class="glyphicon glyphicon-user circular"></i>
                        </span>
                    </div>
                     <div class="form-group">
                        <span class="input-icon icon-right">
                            <input type="text" class="form-control" name="UsersC[apellidos]" id="UsersC_apellidos" placeholder="Surname"  maxlength="100" size=60 oninvalid="setCustomValidity('This field is required')" oninput="setCustomValidity('')" required pattern=".*\S+.*" title="This field is required">
                            <i class="glyphicon glyphicon-user circular"></i>
                        </span>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <span class="input-icon icon-right">
                                	<label>COUNTRY:</label>
			                    	<br>
                                    <select name="UsersC[id_pais]" required class="country-select" id="country-select" style="width:100%">
                                    	<option value="" default></option>
									<?php
			                            foreach($list as $paises){
			                                ?>
			                                    <option value="<?php echo $paises['Code']?>"><?php echo utf8_encode($paises['Name'])?></option>
			                                <?php
			                            }
			                        ?>
									</select>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <span class="input-icon icon-right">
                                	<label>CITY:</label>
									<br>
                                    <select id="city-select" name="UsersC[id_ciudad]" required class="ciudades-select" class="city-select" style="width:100%">
                                    	
                                    </select>
                                </span>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="form-group">
                        <span class="input-icon icon-right">
                            <input type="text" class="form-control" maxlength="200" size="60" required="required" id="emailInput" name="UsersC[email]" id="UsersC_email" placeholder="Email" oninvalid="setCustomValidity('This field is required')" oninput="setCustomValidity('')" required>
                            <i class="fa fa-envelope-o circular"></i>
                        </span>
                    </div>
                    <hr class="wide">
                    
                    <div class="form-group">
                    	<label>PROFILE:</label>
                    	<br>
                        <select id="combo_perfil" name="UsersC[id_perfil]" required>
							<option value="" selected="selected"></option>
							<?php
								foreach ($profiles as $profile) {
									echo '<option value="'.$profile["id_perfil"].'">'.$profile["nombre"].'</option>';
								}
							?>
						</select>
                    </div>
                    
                    <div class="form-group">
				    	<div class="col-sm-12">
					    	<label id="numero_id_lbl" for="UsersC_Username">Id number</label><br>
					    	<input size="20" class="form-control" maxlength="20" required="required" name="UsersC[numero_id]" id="UsersC_numero_id" type="text" style="  margin-bottom: 20px;" oninvalid="setCustomValidity('This field is required')" onchange="setCustomValidity('')" required pattern=".*\S+.*" title="This field is required">    	
					    	<a href="/ilto3/index.php?r=Clients/usersC/transfer"><p id="id_validate"></p></a>
					    </div>
				    </div>
                    
                    <div id="datosTaker" style="display: none;">
                    	
                    	
			        <div class="form-group">
			        	<div class="col-sm-12" style="padding: 0;">
			        	    <label for="UsersC_id_grupo_cliente">GROUP</label>                    
			        	   
			        	    <br>
			        		<select id="combo_grupo" name="UsersC[id_grupo_cliente]" style="width:100%">
								<option value="" selected="selected"></option>
								
								<?php 
									$SQL = "SELECT * FROM `groups_users_clients` WHERE `id_cliente` LIKE '".Yii::app()->user->getState('cliente')."' ORDER BY `groups_users_clients`.`id` ASC";
									$list= $connection->createCommand($SQL)->queryAll();
									foreach($list as $groups){
										?>
											<option value="<?php echo $groups['id']?>"><?php echo utf8_encode($groups['nombre'])?></option>
										<?php
									}
								?>
								
								
							</select>            
						</div>        
					
			    	</div>
			    		<br>
			        	<p><b><i style="    color: grey;">Please select the group where the user belongs to</i></b></p>
			    	<div class="form-group">
			            <div class="col-sm-12">
			        		<input id="ytUsersC_email_certificate" type="hidden" value="0" name="UsersC[email_certificate]">
			        		
							
							<div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input name="UsersC[email_certificate]" id="UsersC_email_certificate" value="1" type="checkbox">
                                                                <span class="text">Send TECS certificate to the test taker.</span>
                                                            </label>
                                                        </div>
                                                    </div>

		        		</div>
			        </div>
			        
			    	<br>
				    <br>
			    </div>
			    <div class="clear" style="margin-bottom:30px">
			    	
			    </div>
			    
                  
                   
                   <!-- <div class="row" id="picture">
	                    <div class="col-sm-6 custom-upload">
	                        <span class="file-input btn btn-block btn-default btn-file">
	                            <input id="ytUsersC_ruta_foto" type="hidden" value="" name="UsersC[ruta_foto]">
	                            UPLOAD PHOTO HERE <input type="file" multiple="" name="UsersC[ruta_foto]" id="UsersC_ruta_foto">
	                        </span>
	                        <p>Picture restrictions:<br/>
	                        File type: jpg, png, gif.<br/>
	                        Max weight: 2 MB.<br/>
	                        Best size: 1024 x 768 pxls.</p>
	                         <?php if(!$model->isNewRecord){
	                            if(strlen($model->ruta_foto)>0){
	                                echo "<img src='images/users/tn1_$model->ruta_foto' style='width:100px;'>";
	                                echo "<input type='hidden' name='ruta_imagen_actual' value='$model->ruta_foto'>";
	                                echo "&nbsp;&nbsp;<input type='checkbox' name='delete_imagen' value='1'>&nbsp;Delete Picture Saved";
	                            }
	                        } ?>
	                    </div>

                	</div> -->
                
                
                
                <div class="row buttons">
            
					<input type="submit" name="yt0" value="Create" class="btn btn-blue" style="margin-left: 20px;float:left">            
					<div id="btnTaker" style="display: block;">
	                  &nbsp;&nbsp;<a  class="yt1 btn btn-blue" id="yt1">Save &amp; Assign Exam</a> 
					</div>   
				            
				 </div>
		    	
                </form>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-4 col-sm-4 col-xs-12">
	<a href="/ilto3/index.php?r=Clients/usersC/create_users" class="btn btn-success" style="margin:auto;width:100%;margin-bottom:10px;">MULTIPLE USERS CREATOR</a>
                                        
                                    <div class="widget">
                                    	<div class="widget-header bg-blue">
                                            <i class="widget-icon fa fa-arrow-left"></i>
                                            <span class="widget-caption">PROFILE DESCRIPTION</span>
                                           
                                        </div><!--Widget Header-->
                                        <div class="widget-body">
                                            <div>
                                            	<b>TOAA (Total Access Administrator)</b>, This user is able to:<br><ul><li>Manage user accounts (create, delete, modify),</li><li>Allocate test licenses.</li><li>Manage groups or test rooms (Create, delete, modify)</li><li>Purchase test licenses</li><li>Tutor access (Perform the speaking test and assign test licenses)</li><li>Manage the test agenda (set and modify exam dates, approve or reject test takers)</li></ul>
						                        <b>PAA (Partial Access Administrator)</b>, This user is able to:<br><ul><li>Allocate test licenses,</li><li>Manage groups or test rooms (Create, delete, modify)</li><li>Tutor access (Perform the speaking test and assign test licenses)</li><li>Manage the test agenda (set and modify exam dates, approve or reject test takers)</li></ul>
						                        <b>RATER (Rater Access Administrator)</b>, This user is able to:<br><ul><li></li>Perform the speaking test<li>See the test agenda</li></ul>
						                        <b>RAA (Report Access Administrator)</b>, This user is able just to see the test reports<br><b>TAKER</b>: This user is able just to take the Exam
						                    </div>
                                        </div><!--Widget Body-->
                                    </div><!--Widget-->
                                </div>
                                
                                </div>
                                
                                <script>
                                
                                $(document).ready(function(){
								    $('#country-select').change(function() {
								        
								        loadCity($(this).find(':selected').val())
								    })
								
								})

								function loadCity(countryId){
								        $("#city-select").children().remove()
								        $.ajax({
								            type: "POST",
								            url: "/ajax.php",
								            data: "get=city&countryId=" + countryId
								            }).done(function( result ) {
								                $(result).each(function(i,j){
								                    $("#city-select").append($('<option>', {
								                        value: j.ID,
								                        text: j.Name,
								                    }));
								                })
								            });
								}
                                	$("#combo_perfil").change(function(){
								        if($("#combo_perfil").val()=="6"){
								           $('#datosTaker').show();
								           $("#combo_grupo").prop('required',true);
								           $('#picture').hide();
								           $('#btnTaker').show();
								           $('#picture').show();
								           $('#UsersC_numero_id').val('');
								           document.getElementById('numero_id_lbl').innerHTML = 'Id number';
								           $("#UsersC_numero_id").click(function(){
										        $(this).prop('type', 'number');
										        $('#UsersC_numero_id').bind('keypress', function (event) {
										        	var regex = new RegExp("^[a-zA-Z0-9]+$");
												    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
												    if (!regex.test(key)) {
												       event.preventDefault();
												       return false;
												    }
												});
											});
											$(function(){
											  $('#UsersC_numero_id').keypress(function(e){
											    if(e.which == 101){
											    	return false;
											    }
											  });
											});
								        } else {
								           $('#datosTaker').hide();   
								           $("#combo_grupo").prop('required',false);
								           $('#picture').show();
								           $('#btnTaker').hide(); 
								           $('#picture').hide();
								            document.getElementById('numero_id_lbl').innerHTML = 'Username'; 
								             $("#UsersC_numero_id").click(function(){
										        $(this).prop('type', 'text');
											});
								        }
								    })
								    
								    $('#UsersC_ruta_foto').bind('change', function() {
									    if(this.files[0].size > '2048000'){
									            alert('The image excedes the size limit. Please select an image up to 2 Mb');
									            $('#UsersC_ruta_foto').val("");
									            document.getElementById('UsersC_ruta_foto').value = '';
									
									    }
									});
									jQuery(document).ready(function(){
									 jQuery("#yt1").click(function(e){
								        e.preventDefault();
								         jQuery('#users-c-form').attr('action', '/ilto3/index.php?r=Clients/usersC/create_exam');
								         jQuery('#users-c-form').submit()
									jQuery('#users-c-form').prepend('<input type="hidden" name="yt1" value="1"/>')
								   })

})

	$("#UsersC_numero_id").change(function() {
		var id = $("#UsersC_numero_id").val();
		$.ajax({
			url : "/ilto3/index.php?r=Api/username",
			type: "POST",
			dataType: "text",
			data: {
			    id: id
			},
			success: function(data) {
				$("#id_validate").html(data);
			}
		});
	});

				

							    
									
                                </script>
<?php
/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = Yii::app()->user->getState('id_perfil');
if($profile == 6){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}


if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}


$id_request = base64_decode($_GET["id"]);
/* session manager
end
*/

// agregamos al log del usuario access
$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 4, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'Access TECS ACADEMY request ".$id_request."', '')";
$log= Yii::app()->db->createCommand($SQL)->query(); 


/* @var $this UsersCController */
/* @var $model UsersC */

$this->breadcrumbs=array(
	'Users Cs'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create Users', 'url'=>array('create')),
);

/*
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#users-c-grid').yiiGridView('update', {
		data: $(this).serialize()
	}); 
	return false;
});
");
*/

$config = Yii::app()->getComponents(false);
$connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
// establish connection. You may try...catch possible exceptions
$connection->active=true;


$SQL = "SELECT * FROM `tecs_academy` WHERE id_tecs_academy = '".$id_request."' ";
$request_info = $connection->createCommand($SQL)->queryAll();

$id_license = $request_info[0]["id_license"];

if($id_license == NULL){
    $license_type = 27; 
    
    $SQL =  "SELECT id_licencias_cliente FROM licenses_client  WHERE id_cliente = '90043026471' AND id_licencia = ".$license_type." AND estado = 'A'";  
    $request = $connection->createCommand($SQL)->queryAll();
    $id_licencias_cliente = $request[0]["id_licencias_cliente"];
    
    $data = array();
    //tratar de llamar la asignación de licencia
    //variables para pasar por data
    $data['id_cliente'] = "90043026471";
    $data["period"] = 10;
    $data["city"] = "2257";
    $data["school"] = 0;
    $data["program"] = 0;
    $data['id_licencias_cliente'] = $id_licencias_cliente;
    $dateHour = date("Y-m-d h:i:s");				
   
    $data['fecha_presentacion'] = $dateHour;
    $explode_hour = explode(" ", $dateHour);
    $hour_separate = explode(":", $explode_hour[1]);
    $data['hora'] = $hour_separate[0].":00";
        						
    $data['salon'] = 625;
    $data['tutor'] = "partialito";
    $modUtil = new Utilidades();
    $id_license =     $modUtil->allocateEDELicense($request_info[0]["id_user"], $data, $id_request, Yii::app()->user->getState('usuario'), $reference);
}



$SQL = "SELECT * FROM `users_c` WHERE id_usuario_c = '".$request_info[0]["id_user"]."' ";
$user = $connection->createCommand($SQL)->queryAll();

/* TECS INTRO */
$SQL = "SELECT * FROM `tecs_academy_dates` WHERE id_tecs_academy = '".$id_request."' AND type=0 ";
$tecs_intro_date = $connection->createCommand($SQL)->queryAll();


/* TECS INTRO INFO  */

if($request_info[0]["tecs_intro"]==1){
     $intro = '<img src="/ilto3/images/iconos/verified.png" title="PAYMENT STATUS" width="32">  The taker has selected TECS INTRO.<br><br>';
    if(isset($tecs_intro_date[0]["date_selected"]) && $tecs_intro_date[0]["date_selected"] != ""){
        $intro_date = "<h4><b>".$tecs_intro_date[0]["date_selected"]."</h4></b>
                        Date Selected";
    }else{
        $intro_date = "<b>TECS INTRO IS SELECTED BUT THE DATE IS PENDING</b>";
    }
}else{
    $intro = "The taker has not selected TECS INTRO.<br><br>";
}
/* -- END TECS INTRO INFO*/


/* TECS INTRO PAYMENT */ 

$SQL = "SELECT * FROM `proctorv_user_payment` WHERE id_request = '".$id_request."' AND sell_reference = 'TECS-INTRO' ";
$tecs_intro_payment = $connection->createCommand($SQL)->queryAll();


if(isset($tecs_intro_payment[0]["id_proctorv_user_payment"]) && $tecs_intro_payment[0]["id_proctorv_user_payment"] != NULL){
    switch($tecs_intro_payment[0]["status"]){
        case 4:
            $ti_payment_status = '<h4><img src="/ilto3/images/iconos/verified.png" title="PAYMENT STATUS" width="32"><span class="green_text">  APPROVED</span></h4>';
        break;
        case 6:
            $ti_payment_status = '<h4><img src="/ilto3/images/iconos/alert-danger.png" title="PAYMENT STATUS" width="32"><span class="green_text">  REJECTED</span></h4>';
        break;
        case 7:
            $ti_payment_status = '<div id="payment_notify">
                                    <h4><img src="/ilto3/images/iconos/alert-warning.png" title="PAYMENT STATUS" width="32"><span class="green_text">  PENDING  </span><input id="verify_payu_payment" type="button" class="btn btn-warning" value="Verify Payment" '.$user_payment[0]["id_request"].'/></h4>
                                    </div>'; 
        break;
    } 
    $ti_payment = "<h4>".$ti_payment_status."</h4>
                <h5><b>Payment Status</b></h5><br>
                <h4>".$tecs_intro_payment[0]["transaction_reference"]."</h4>
                <h5><b>Transaction Reference</b></h5><br><br>";
}else{
      $ti_payment = '<div id="payment_notify"><br>
                    There is not payment registered, Do you want to confirm the Payment?<br><br>
                    <input id="confirm_noreg_intro_payment_btn" type="button" class="btn btn-success" value="Approve Payment" '.$user_payment[0]["id_request"].'/></div>';    
}

/* -- END TECS INTRO*/


//GET EDE LICENSE INFO

$license =  "<a href='/ilto3/index.php?r=Clients/ExamsUser/admin&id=".base64_encode($id_license)."' class='btn btn-success btn-xs edit'><i class='fa fa-check'></i>Results</a>
                <h6><b>EDE LICENSE ALLOCATED</b></h6>";




$this->layout = "//layouts/column2a";

?>

<style>
    .table-bordered {
        border: 1px solid #ddd;
        font-size: 12px;
    }
    td.center {
        font-size: 11px;
    }
    .btn-xs {
        font-size: 11px;
        padding: 2px 4px;
    }
    
    @media (max-width: 1400px) {
      .col-lg-9.col-sm-9.col-xs-12 {
        width: 100%!important;
      }
      
      .col-lg-3.col-sm-3.col-xs-12 {
        width: 100%!important;
      }
    }
</style>

<!-- TECS INTRO -->
<div class="row">
    <div class="col-lg-4 col-sm-4 col-xs-4">
        <div class="widget">
            <div class="widget-header bg-blue">
                <i class="widget-icon fa fa-arrow-left"></i>
                <span class="widget-caption">TECS ACADEMY REQUEST INFO</span>
            </div><!--Widget Header-->
            <div class="widget-body" style="    text-align: center;padding: 5px;">
                    <h4><?php echo ucfirst(utf8_encode($user[0]["nombres"]))." ".ucfirst(utf8_encode($user[0]["apellidos"])) ?></h4>
                    <h5><b>ID Number: </b><?php echo $user[0]["id_usuario_c"] ?>
                    </h5>
                    <h5><b>Contact: </b><?php echo $user[0]["telefono"] ?>
                    </h5>
                    <h5><b>Emai: </b><?php echo $user[0]["email"] ?></h5>
                    </h5>
                    <h5><b>Request Date: </b><?php echo $request_info[0]["date_request"] ?>
                    </h5>
            </div><!--Widget Body-->
        </div><!--Widget-->
    </div>
    
    <div class="col-lg-4 col-sm-4 col-xs-4">
        <div class="widget">
            <div class="widget-header bg-blue">
                <i class="widget-icon fa fa-arrow-left"></i>
                <span class="widget-caption">ALLOCATED LICENSES</span>
            </div><!--Widget Header-->
            <div class="widget-body" style="    text-align: center;padding: 5px;">
                
             <?php
                echo $license;
             ?>
            </div><!--Widget Body-->
        </div><!--Widget-->
    </div>
    
</div>
</div>
<!-- END TECS INTRO -->
<!-- TECS MASTER -->

<div class="row">
    <div class="col-lg-6 col-sm-6 col-xs-6">
        <div class="widget">
            <div class="widget-header bg-yellow">
                <i class="widget-icon fa fa-arrow-left"></i>
                <span class="widget-caption">TECS INTRO</span>
            </div><!--Widget Header-->
            <div class="widget-body" style="    text-align: center;padding: 25px;">
                <img src="/ilto3/images/tecs-intro.png" alt="TECS-INTRO"><br><br><br>
                <div class="row">
                    <div class="col-lg-6 col-sm-6 col-xs-6">
                        <?php
                        echo $intro; 
                        echo $intro_date;
                            ?>
                            <input type="hidden" name="id_user" id="id_user" value="<?php echo $request_info[0]["id_user"]; ?>" />
                        <input type="hidden" name="id_user" id="id_user" value="<?php echo $request_info[0]["id_user"]; ?>" />
                        <input type="hidden" name="id_request" id="id_request" value="<?php echo $id_request; ?>" />
                        <input type="hidden" name="id_admin" id="id_admin" value="<?php echo Yii::app()->user->getState('usuario') ?>" />
                       
                    </div>
                    <div class="col-lg-6 col-sm-6 col-xs-6">
                         <?php
                        echo $ti_payment;
                         ?>
                    </div>
                </div><!--Widget Body-->
            </div><!--Widget-->
        </div>
    </div>
    <div class="col-lg-6 col-sm-6 col-xs-6">
        <div class="widget">
            <div class="widget-header bg-red">
                <i class="widget-icon fa fa-arrow-left"></i>
                <span class="widget-caption">TECS MASTER</span>
            </div><!--Widget Header-->
            <div class="widget-body" style="    text-align: center;padding: 25px;">
                <img src="/ilto3/images/tecs-master.png" alt="TECS-INTRO"><br><br><br>
                <?php
                    /* TECS MASTER */
                   

                    if($request_info[0]["tecs_master"]==1){
                         $SQL = "SELECT * FROM `tecs_academy_dates` WHERE id_tecs_academy = '".$id_request."' AND type=1 ";
                         $tecs_master_date = $connection->createCommand($SQL)->queryAll();
                         $master = '<img src="/ilto3/images/iconos/verified.png" title="PAYMENT STATUS" width="32">  The User has selected TECS MASTER<br><br>';
                        
                        if(isset($tecs_master_date[0]["date_selected"]) && $tecs_master_date[0]["date_selected"] != ""){
                            $master_date = "<h4><b>".$tecs_intro_date[0]["date_selected"]."</h4></b>Date Selected";
                        }else{
                            $master_date = "<b>TECS MASTER IS SELECTED BUT THE DATE IS PENDING</b>";
                        }
                    }else{
                        $master = "The User has not selected TECS MASTER<br><br>";
                    }
                    /* -- END TECS INTRO INFO*/
                   ?>
                    <div class="row">
                    <div class="col-lg-6 col-sm-6 col-xs-6">
                           <?php
                            echo $master;
                            echo $master_date;
                            
                            ?>
                             <input type="hidden" name="id_user" id="id_user" value="<?php echo $request_info[0]["id_user"]; ?>" />
                        <input type="hidden" name="id_user" id="id_user" value="<?php echo $request_info[0]["id_user"]; ?>" />
                        <input type="hidden" name="id_request" id="id_request" value="<?php echo $id_request; ?>" />
                        <input type="hidden" name="id_admin" id="id_admin" value="<?php echo Yii::app()->user->getState('usuario') ?>" />
                    </div>
             <?php
             
                /* TECS MASTER PAYMENT */ 
                
                $SQL = "SELECT * FROM `proctorv_user_payment` WHERE id_request = '".$id_request."' AND sell_reference = 'TECS-MASTER' ";
                $tecs_master_payment = $connection->createCommand($SQL)->queryAll();
                
                if(isset($tecs_master_payment[0]["id_proctorv_user_payment"]) && $tecs_master_payment[0]["id_proctorv_user_payment"] != NULL){
                    switch($tecs_master_payment[0]["status"]){
                        case 4:
                            $master_payment_status = '<h4><img src="/ilto3/images/iconos/verified.png" title="PAYMENT STATUS" width="32"><span class="green_text">  APPROVED</span></h4>';
                        break;
                        case 6:
                            $master_payment_status = '<h4><img src="/ilto3/images/iconos/alert-danger.png" title="PAYMENT STATUS" width="32"><span class="green_text">  REJECTED</span></h4>';
                        break;
                        case 7:
                            $master_payment_status = '<div id="payment_notify">
                                                    <h4><img src="/ilto3/images/iconos/alert-warning.png" title="PAYMENT STATUS" width="32"><span class="green_text">  PENDING  </span><input id="verify_payu_payment" type="button" class="btn btn-warning" value="Verify Payment" '.$user_payment[0]["id_request"].'/></h4>
                                                    </div>'; 
                        break;
                    } 
                    $master_payment = "<h4>".$master_payment_status."</h4>
                                <h5><b>Payment Status</b></h5><br>
                                <h4>".$tecs_master_payment[0]["transaction_reference"]."</h4>
                                <h5><b>Transaction Reference</b></h5><br><br>";
                }else{
                   
                   $master_payment = '<div id="payment_notify"><br>
                                    There is not payment registered, Do you want to confirm the Payment?<br><br>
                                    <input id="confirm_noreg_master_payment_btn" type="button" class="btn btn-success" value="Approve Payment" '.$user_payment[0]["id_request"].'/></div>';    
                }
                 ?>
                    <div class="col-lg-6 col-sm-6 col-xs-6">
                <?php
                /* -- END TECS INTRO*/
                echo $master_payment;
             ?> 
                 </div>
            </div>
            </div><!--Widget Body-->
        </div><!--Widget-->
    </div>
</div>
<!-- TECS INTRO -->



   <!--Page Related Scripts-->
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/jquery.dataTables.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/ZeroClipboard.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.tableTools.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.bootstrap.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/datatables-init.js"></script>
    <script>
        InitiateSimpleDataTable.init();
        InitiateExpandableDataTable.init();
        InitiateSearchableDataTable.init();
    </script>
    
<script>
$(document).ready(function(){
 
    //confirm new no register payment 
    $('#confirm_noreg_intro_payment_btn').click(function(){
        var confirmation = confirm("Do you want to register the payment?");
        var id_request = $("#id_request").val();
        var id_admin = $("#id_admin").val();
        var id_user = $("#id_user").val();
        var reference = "TECS-INTRO";
        
        if(confirmation == true){
          $.ajax({
              url : "/ilto3/index.php?r=Api/confirmNewAcademyPayment",
              type: "POST",
              dataType: "text",
              data: {
                  id_request: id_request, id_admin: id_admin, id_user: id_user, reference: reference
              },
              success: function(data){
            
                document.getElementById("payment_notify").innerHTML = data; 
                //refresh page
                location.reload();
               
              }
          })
       }
        
    });
    
     //confirm new no register payment 
    $('#confirm_noreg_master_payment_btn').click(function(){
        var confirmation = confirm("Do you want to register the payment?");
        var id_request = $("#id_request").val();
        var id_admin = $("#id_admin").val();
        var id_user = $("#id_user").val();
        var reference = "TECS-MASTER";
        
        if(confirmation == true){
          $.ajax({
              url : "/ilto3/index.php?r=Api/confirmNewAcademyPayment",
              type: "POST",
              dataType: "text",
              data: {
                  id_request: id_request, id_admin: id_admin, id_user: id_user, reference: reference
              },
              success: function(data){
            
                document.getElementById("payment_notify").innerHTML = data; 
                //refresh page
                location.reload();
               
              }
          })
       }
        
    });
   
 
    
    //confirm new no register payment 
    $('#verify_payu_payment').click(function(){
        var confirmation = confirm("Do you want to register the payment?");
        var id_request = $("#id_request").val();
        var id_admin = $("#id_admin").val();
        var id_user = $("#id_user").val();
        var reference = "TECS-INTRO";
        
        var pagos = {
           "test": false,
           "language": "en",
           "command": "PING",
           "merchant": {
              "apiLogin": "pRRXKOl8ikMmt9u",
              "apiKey": "4Vj8eK4rloUd272L48hsrarnUA"
           }
        };
        
        if(confirmation == true){
          $.ajax({
              url : "https://sandbox.api.payulatam.com/reports-api/4.0/service.cgi",
              type: "POST",
              dataType: "text",
              data: {
                  pagos: pagos
              },
              success: function(data){
                alert("done");
                //document.getElementById("payment_notify").innerHTML = data; 
                //refresh page
                
               
              }
          })
       }
        
    });
    

    
});
    
</script>

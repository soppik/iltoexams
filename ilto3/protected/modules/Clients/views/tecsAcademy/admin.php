<?php
/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = Yii::app()->user->getState('id_perfil');
if($profile == 6){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}


if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}


/* session manager
end
*/

// agregamos al log del usuario access
$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 4, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'Access users list TECS ACADEMY', '')";
$log= Yii::app()->db->createCommand($SQL)->query(); 


/* @var $this UsersCController */
/* @var $model UsersC */

$this->breadcrumbs=array(
	'Users Cs'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create Users', 'url'=>array('create')),
);

/*
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#users-c-grid').yiiGridView('update', {
		data: $(this).serialize()
	}); 
	return false;
});
");
*/

$config = Yii::app()->getComponents(false);
$connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
// establish connection. You may try...catch possible exceptions
$connection->active=true;


$SQL = "SELECT tecs_academy.id_tecs_academy, tecs_academy.id_user, nombres, apellidos, tecs_intro, tecs_master, id_license, date_request, telefono, email FROM `tecs_academy` 
        INNER JOIN users_c ON tecs_academy.id_user = users_c.id_usuario_c 
        ORDER BY `tecs_academy`.`date_request` DESC";
$list_u= $connection->createCommand($SQL)->queryAll();

        
$Status = array("A"=>"Enabled", "I"=>"Disabled");

$this->layout = "//layouts/column2a";

?>

<style>
    .table-bordered {
        border: 1px solid #ddd;
        font-size: 12px;
    }
    td.center {
        font-size: 11px;
    }
    .btn-xs {
        font-size: 11px;
        padding: 2px 4px;
    }
    
    @media (max-width: 1400px) {
      .col-lg-9.col-sm-9.col-xs-12 {
        width: 100%!important;
      }
      
      .col-lg-3.col-sm-3.col-xs-12 {
        width: 100%!important;
      }
    }
</style>


<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <button id="tecsAcademy_admin" name="tecsAcademy_admin" class="btn btn-primary">TECS ACADEMY</button>
        <button id="intro_admin" name="intro_admin" class="btn btn-success">TECS INTRO</button>
        <button id="master_admin" name="master_admin" class="btn btn-danger">TECS MASTER</button>
        </div>
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="widget" id="academy_admin">
                                <div class="widget-header ">
                                    <span class="widget-caption">TECS ACADEMY REQUESTS</span>
                                    <a href="/ilto3/index.php?r=Clients/usersC/create" class="btn btn-success">Create Users</a>
                                </div>
                                <div class="widget-body">
                                    <form class="search-form" action="/ilto3/index.php?r=Api/userAcademy" onsubmit="return false">
                                    <table class="table table-striped table-hover table-bordered" id="editabledatatable">
                                        <thead>
                                            <tr role="row">
                                                <th>
                                                    DATE REQUEST
                                                    <br>
                                                </th>
                                                <th>
                                                    USERNAME / ID
                                                    <br>
                                                    <input type="text" name="id_usuario_c" class="search form-control" data-related="users_c.id_usuario_c"/>
                                                    </select>
                                                </th>
                                                <th>
                                                    NAME
                                                  <br>
                                                    <input type="text" name="nombres" class="search form-control" data-related="users_c.nombres"/>
                                                </th>
                                                <th>
                                                    SURNAME
                                                    <br>
                                                    <input type="text" name="apellidos" class="search form-control" data-related="users_c.apellidos"/>
                                                </th>
                                                <th>
                                                    CONTACT
                                                    <br>
                                                </th>
                                                <th>
                                                    EMAIL
                                                    <br>
                                                </th>
                                                <th>
                                                    CONSULTANT
                                                    <br>
                                                </th>
                                                <th>
                                                    TECS INTRO
                                                    <br>
                                                </th>
                                                <th>
                                                    TECS MASTER
                                                    <br>
                                                </th>
                                                <th>
                                                   EDE LVL
                                                    <br>
                                                    <select class="form-control search" name="status" data-related="users_c.estado">
                                                        <option value=""></option>
                                                        <option value="PA1">PRE A1</option>
                                                         <option value="A1">A1</option>
                                                         <option value="A2">A2</option>
                                                         <option value="A2+">A2+</option>
                                                         <option value="B1">B1</option>
                                                         <option value="B1+">B1+</option>
                                                         <option value="B2">B2</option>
                                                         <option value="B2+">B2+</option>
                                                         <option value="C1">C1</option>
                                                        
                                                    </select>
                                                </th>
                                                <th>
                                                    ACTIONS
                                                </th>
                                                
                                            </tr>
                                        </thead>

                                        <tbody class="search-rows">
                                            
                                            <?php 
                                                foreach($list_u as $user){
                                                    
                                                    $SQL = "SELECT * FROM `tecs_academy_dates` WHERE `id_tecs_academy` = ".$user['id_tecs_academy']." ";
                                                    $date= $connection->createCommand($SQL)->queryAll();
                                                    
                                                    //get consultant info
                                                    $SQL = "SELECT id_consultant, nombres FROM `consultant_history` 
                                                            INNER JOIN users_c ON consultant_history.id_consultant = users_c.id_usuario_c
                                                            WHERE `id_product` = ".$user['id_tecs_academy']." ";
                                                    $consultant = $connection->createCommand($SQL)->queryAll();
                                                    
                                                    //ede license
                                                    if(isset($user['id_license']) && $user['id_license'] != NULL){
                                                        //get consultant 
                                                        $SQL = "SELECT nivel FROM `licenses_user` `t` WHERE id_licencias_usuario= '".$user['id_license']."' ";
                                                        $ede_lvl= $connection->createCommand($SQL)->queryAll();
                                                        $lvl = $ede_lvl[0]["nivel"];
                                                        if($lvl == ""){
                                                            $lvl = "PENDING PLACEMENT";
                                                        }
                                                    }else{
                                                        $lvl = "PENDING ALLOCATING";
                                                    }
                                                    
                                                    ?>
                                                        <tr>
                                                            <td><?php echo $user['date_request']?></td>
                                                            <td><?php echo $user['id_user']?></td>
                                                            <td><?php echo utf8_encode($user['nombres'])?></td>
                                                            <td><?php echo utf8_encode($user['apellidos'])?></td>
                                                            <td><?php echo utf8_encode($user['telefono'])?></td>
                                                            <td><?php echo utf8_encode($user['email'])?></td>
                                                            <td><?php echo utf8_encode($consultant[0]['nombres'])?></td>
                                                            <td><?php 
                                                                    if($user['tecs_intro']==1){
                                                                        echo '<img src="/ilto3/images/iconos/verified.png" title="Pending Speaking!" width="16">';
                                                                    }
                                                                ?>
                                                            </td>
                                                            <td><?php 
                                                                    if($user['tecs_master']==1){
                                                                        echo '<img src="/ilto3/images/iconos/verified.png" title="Pending Speaking!" width="16">';
                                                                    }
                                                                ?>
                                                            </td>
                                                            <td><?php 
                                                                echo $lvl;
                                                                ?>
                                                            </td>
                                                            <td>
                                                                 <a href="/ilto3/index.php?r=Clients/TecsAcademy/RequestDetail/id/<?php echo base64_encode($user["id_tecs_academy"]);?>" class="btn btn-primary" id="allocate">Details</a>
                                                            </td>
                                                        </tr>
                                                    <?php
                                                }
                                            ?>
                                            
                                            
                                            
                                        </tbody>
                                    </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                       
                                
                       
                    </div>


   <!--Page Related Scripts-->
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/jquery.dataTables.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/ZeroClipboard.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.tableTools.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.bootstrap.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/datatables-init.js"></script>
    <script>
        InitiateSimpleDataTable.init();
        InitiateExpandableDataTable.init();
        InitiateSearchableDataTable.init();
    </script>
    
<script>
jQuery(document).on('click', '.btn.btn-danger.btn-xs.delete',function(e){
    e.preventDefault();
        var url = jQuery(this).attr('href')
        var id = $(this).attr('value')
        jQuery.post(url, { id: $(this).attr('value')}, function(data){
             console.log(data)
             var cl = '.';
             var clas = cl.concat(id)
             jQuery(clas).remove()
        })
})

    //confirm new no register payment 
$('#tecsAcademy_admin').click(function(){
    id_request = "data";
    $.ajax({
        url : "/ilto3/index.php?r=Api/AcademyAdmin",
        type: "POST",
        dataType: "text",
        data: {
            id_request: id_request
        },
        success: function(data){
            document.getElementById("academy_admin").innerHTML = data; 
        }
    })
});

//confirm new no register payment 
    $('#intro_admin').click(function(){
        id_request = "data";
        $.ajax({
              url : "/ilto3/index.php?r=Api/IntroAdmin",
              type: "POST",
              dataType: "text",
              data: {
                  id_request: id_request
              },
              success: function(data){
            
               document.getElementById("academy_admin").innerHTML = data; 
                //refresh page
                //location.reload();
               
              }
          })

    });
    
    //confirm new no register payment 
$('#master_admin').click(function(){
    id_request = "data";
    $.ajax({
        url : "/ilto3/index.php?r=Api/MasterAdmin",
        type: "POST",
        dataType: "text",
        data: {
            id_request: id_request
        },
        success: function(data){
            document.getElementById("academy_admin").innerHTML = data; 
        }
    })
});


    
</script>
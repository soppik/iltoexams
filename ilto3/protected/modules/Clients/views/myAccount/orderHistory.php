<?php
/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = $session['profile'];


if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}

if($profile == 6){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}

/* session manager
end
*/

/* @var $this FraudReportController */
/* @var $model FraudReport */

$this->breadcrumbs=array(
	'My Licenses'=>array('index'),
	'MY QUOTES',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#fraud-report-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

$modUtil = new Utilidades();
$types = $modUtil->arregloTiposFraude();

$config = Yii::app()->getComponents(false);
$connection=new CDbConnection('mysql:host=localhost;dbname=iltoexam_newversion;','iltoexam_newvers','Ilto.2015');
//$connection=new CDbConnection('mysql:host=localhost;dbname=thetec_iltoexam_newversion','thetec_newvers','Ilto.2015');
//$connection=new CDbConnection($config["db"]["connectionString"],$config["db"]["username"],$config["db"]["password"]);
$connection->active=true;


$SQL = "SELECT * FROM `orders` WHERE id_client =  '".Yii::app()->user->getState('cliente')."' ORDER BY id_order DESC LIMIT 1;";
$list_u= $connection->createCommand($SQL)->queryAll();    

$SQL = "SELECT * FROM `invoices` INNER JOIN orders ON invoices.id_order = orders.id_order WHERE orders.id_client = '".Yii::app()->user->getState('cliente')."' ORDER BY id_invoice DESC;";
$list_inv= $connection->createCommand($SQL)->queryAll();    

?>
<style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
    /* display: none; <- Crashes Chrome on hover */
    -webkit-appearance: none;
    margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
}
    
</style>

<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
                                    <div class="widget">
                                        <div class="widget-header bg-blue">
                                            <span class="widget-caption">INSTRUCTIONS</span>
                                        </div><!--Widget Header-->
                                        <div class="widget-body">
                                            Following the quote information. Check if your order has been approved in the status box. You may also check the quotation details in the VIEW box and print the quotation by clicking the PRINT button.
                                        </div>
                                    </div>
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="widget">
                                <div class="widget-header ">
                                    <span class="widget-caption">MY QUOTES</span>
                                   </div>
                                <div class="widget-body">
                                    <table class="table table-striped table-hover table-bordered" id="editabledatatable">
                                        <thead>
                                            <tr role="row" style="text-align:center;">
                                                <th style="text-align:center;">
                                                    QUOTATION ID 
                                                </th>
                                                <th style="text-align:center;">
                                                    ISSUE DATE
                                                </th>
                                                <th style="text-align:center;">
                                                    SUBTOTAL
                                                </th>
                                                <th style="text-align:center;">
                                                    DISCOUNT
                                                </th>
                                                <th style="text-align:center;">
                                                    TOTAL
                                                </th>
                                                 <th style="text-align:center;">
                                                    STATUS
                                                </th>
                                                <th style="text-align:center;">
                                                    VIEW
                                                </th>
                                                <th style="text-align:center;">
                                                    PDF
                                                </th>
                                              
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            foreach ($list_u as $value) {
                                                   switch ($value['status']) {
                                                    case 'P':
                                                        $status= "Pending";
                                                    break;
                                                    case 'R':
                                                        $status= "<span style='color:#53a93f;'>Approved</span>";
                                                    break;
                                                    case 'A':
                                                        $status= "confirmed";
                                                    break;
                                                    case 'C':
                                                        $status= "Canceled";
                                                    break;
                                                    default:
                                                    $status= "Pending";
                                            }
                                                
                                            ?>
                                                <tr>
                                                    <td style="text-align:center;"><?php echo $value['id_order']?> <?php if($value["check_client"]=='0' && $value["check_ilto"]=='1'){ echo '<img src="/ilto3/images/notification.png" title="checked" style="position:absolute;left:35px;">'; }
                                                     ?></td>
                                                    <td style="text-align:center;"><?php echo substr($value['date_expedition'], 0, 10)?></td>
                                                    <td style="text-align:center;">$ <?php echo number_format($value['subtotal']);?></td>
                                                    <td style="text-align:center;"><?php echo $value['discount']?> %</td>
                                                    <td style="text-align:center;">$ <?php echo number_format($value['total']);?></td>
                                                    <td style="text-align:center;"><?php echo $status?></td>
                                                    <td>
                                                     <?php if($value["check_client"]=='0' && $value["check_ilto"]=='1'){
                                                         echo '<a title="View_order_Detail" target="_blank" style="font-size:10px;" href="/ilto3/index.php?r=Clients/MyAccount/OrderDec&amp;id='.$value['id_order'].'" class="btn btn-warning btn-xs edit"><i class="fa fa-check"></i>Detail</a>';
                                                     }
                                                     if($value["check_client"]=='1' && $value["check_ilto"]=='1'){
                                                         echo '<a title="View_order_Detail" target="_blank" style="font-size:10px;" href="/ilto3/index.php?r=Clients/MyAccount/OrderDec&amp;id='.$value['id_order'].'" class="btn btn-warning btn-xs edit"><i class="fa fa-check"></i>Detail</a>';
                                                     }
                                                     ?>
                                                     </td>
                                                     <td>
                                                        <?php if($value["check_ilto"]=='1'){
                                                            echo '<a title="View_order_Detail" target="_blank" style="font-size:10px;" href="/ilto3/index.php?r=Clients/MyAccount/CuoteDetail&amp;id='.$value['id_order'].'" class="btn btn-success btn-xs edit"><i class="fa fa-check"></i>Detail</a>';
                                                        }
                                                        ?>
                                                     </td>
                                                </tr>
                                             <?php 
                                             }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                       
                        <?php if(!empty($list_inv)){
                        
                            echo '<div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="widget">
                                <div class="widget-header ">
                                    <span class="widget-caption">MY INVOICES</span>
                                   </div>
                                <div class="widget-body">
                                <table class="table table-striped table-hover table-bordered" id="editabledatatable">
                                        <thead>
                                            <tr role="row" style="text-align:center;">
                                                <th style="text-align:center;">
                                                    INVOICE ID 
                                                </th>
                                                <th style="text-align:center;">
                                                    ISSUE DATE
                                                </th>
                                                <th style="text-align:center;">
                                                    DUE DATE
                                                </th>
                                                <th style="text-align:center;">
                                                    QUOTATION ID
                                                </th>
                                                <th style="text-align:center;">
                                                    SUBTOTAL
                                                </th>
                                                <th style="text-align:center;">
                                                    DISCOUNT
                                                </th>
                                                 <th style="text-align:center;">
                                                    TOTAL
                                                </th>
                                                <th style="text-align:center;">
                                                    PDF
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>';
                                            foreach($list_inv as $inv){
                                                echo '
                                                <tr>
                                                    <td style="text-align:center;">
                                                    '.$inv['id_invoice'].'
                                                    </td>
                                                    <td style="text-align:center;">
                                                    '.substr($inv['date_expedition'], 0, 10).'
                                                    </td>
                                                    <td style="text-align:center;">
                                                    '.substr($inv['date_expire'], 0, 10).'
                                                    </td>
                                                    <td style="text-align:center;">
                                                    '.$inv['id_order'].'
                                                    </td>
                                                    <td style="text-align:center;">$ 
                                                    '.number_format($inv['subtotal']).'
                                                    </td>
                                                    <td style="text-align:center;">
                                                    '.$inv['discount'].'
                                                    %</td>
                                                    <td style="text-align:center;">$ 
                                                    '.number_format($inv['total']).'
                                                    </td>
                                                    <td>
                                                     <a title="View_order_Detail" target="_blank" style="font-size:10px;" href="/ilto3/index.php?r=Clients/MyAccount/InvoiceDetail&amp;id='.$inv['id_invoice'].'" class="btn btn-success btn-xs edit"><i class="fa fa-check"></i>Detail</a>
                                                    </td>
                                                </tr>';    
                                                                                         
                                            }
                            }
                            ?>                
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                       
                        
                        


   <!--Page Related Scripts-->
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/jquery.dataTables.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/ZeroClipboard.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.tableTools.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.bootstrap.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/datatables-init.js"></script>
    <script>
        InitiateSimpleDataTable.init();
        InitiateExpandableDataTable.init();
        InitiateSearchableDataTable.init();
    </script>
<?php 
/*
//echo "Searching...".$busca;
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'fraud-report-grid',
	'dataProvider'=>$model->search($busca),
	'filter'=>$model,
	'columns'=>array(
		'document',
		'first_name',
		'sure_name',
            array(
                'name'=>'country',
                'value'=>'$data->nombrePais()',
                'filter'=>CHtml::listData(Countries::model()->findAll(),"id_pais","nombre"),
            ),
            array(
                'name'=>'type',
                'filter'=>$types,
                'value'=>'$data->nombreTipo()',
            ),
		'report_date',

	),
));*/
?>

<?php
/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = $session['profile'];


if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}

if($profile == 6){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}

/* session manager
end
*/
$this->layout="//layouts/column2";

$config = Yii::app()->getComponents(false);
//$connection=new CDbConnection('mysql:host=localhost;dbname=thetec_iltoexam_newversion','thetec_newvers','Ilto.2015');
$connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
$connection->active=true;

// agregamos al log del usuario access
$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 4, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'Accesss license quote tool', '')";
$log= Yii::app()->db->createCommand($SQL)->query(); 

?>

<?php /*$form=$this->beginWidget('CActiveForm', array(
	'id'=>'buyLicenses-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
));*/ 


// consulto el ultimo ID del TECS y EDE
$modUtil = new Utilidades();
$id_tecs = $modUtil->getIdTecs();
$id_ede =  $modUtil->getIdEde();
$cop= $modUtil->getExchangeRate();
$cop_value = $modUtil->tofloat($cop);

$id_client = Yii::app()->user->getState('cliente');

// TECS ECLUV
if($id_client = '8903990166' || $id_client = '8903990166'){
   $SQL = "SELECT * FROM `licenses` `t` WHERE estado='A' AND id_licencia IN(".$id_ede.",".$id_tecs.",18)"; 
}
$SQL = "SELECT * FROM `licenses` `t` WHERE estado='A' AND id_licencia IN(".$id_ede.",".$id_tecs.")";
$list_t= $connection->createCommand($SQL)->queryAll();

$SQL = "SELECT * FROM `clients` WHERE id_cliente='".Yii::app()->user->getState('cliente')."'";
$client= $connection->createCommand($SQL)->queryAll();




?>


<style>
 .quantity {
    width:100px !important;
    margin:auto;
    text-align:center;
 }
 
 .dibujo {
     text-align:left;border-top:1px solid #d3d3d3;border-bottom:1px solid #d3d3d3
 }
 
 #summary_tecs{
    display:none; 
 }
 
 .col-xs-12 {
     /*padding: 5px;*/
 }
 input[type=number]::-webkit-outer-spin-button,
 input[type=number]::-webkit-inner-spin-button {
 -webkit-appearance: none;
 margin: 0;
}
	
 input[type=number] {
 -moz-appearance:textfield;
}

.radio_license {
    left:0px !important;
    opacity:10 !important;
    width: 12px !important;
    height: 12px !important;
}
#ilt19_car, #ilt21_car, #ilt17_car, #ilt18_car  {
    display:none;
}
.licences_preview {
    font-weight:bold;
}


</style>


<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bg-blue">
                <i class="widget-icon fa fa-arrow-left"></i>
                <span class="widget-caption">CUSTOMER INFORMATION </span>
                
            </div><!--Widget Header-->
            <div class="widget-body">
                <div class="row">
                    <div class="col-lg-4 col-sm-4 col-xs-4" style="text-align:center;">
                        <h5 class="bold_h"><b>Contact information</b></h5>
                        <h5 style="text-align:left;margin-left:10px;"><b>Profile: </b> Total Administrator (TOAA)<h5/>
                        <h5 style="text-align:left;margin-left:10px;"><b>Name: </b> <?php echo utf8_encode(Yii::app()->user->getState('nombres')) . " " . Yii::app()->user->getState('apellidos'); ?><h5/>
                        <h5 style="text-align:left;margin-left:10px;"><b>Email:</b>  <?php echo Yii::app()->user->getState('email'); ?><h5/>
                    </div> 
                    <div class="col-lg-4 col-sm-4col-xs-4" style="text-align:center;">
                         <?php if($client[0]["id_distributor"]==NULL){
                            echo "<h5 class='bold_h'><b>DISTRIBUTOR</h5>
                            <h5 class='bold_h'>ILTOEXAMS</h5>";
                        }else{
                            echo "<h5 class='bold_h'>DISTRIBUTOR</h5><h5> ". $client[0]["id_distributor"]."</h5>";
                        }
                        ?>
                    </div> 
                    <div class="col-lg-4 col-sm-4 col-xs-4" style="text-align:center;">
                        <h5 class="bold_h"><b>Company Information</b></h5>
                        <h5 style="text-align:left;margin-left:10px;"><b>Company Name:</b>  <?php echo utf8_encode($client [0]["nombre_rsocial"]); ?></h5>
                        <h5 style="text-align:left;margin-left:10px;"><b>Company ID:</b>  <?php echo $client[0]["nit_id_legal"];?></h5>
                        <h5 style="text-align:left;margin-left:10px;"><b>Address:</b>  <?php echo utf8_encode($client[0]["direccion"]);?></h5>
                        <h5 style="text-align:left;margin-left:10px;"><b>Phone Number:</b>  <?php echo $client[0]["telefono"];?></h5>
                       
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget radius-bordered">
            <div class="widget-header bg-blue">
                <span class="widget-caption">LICENCES INFORMATION</span>
            </div>
            <div class="widget-body">
                <form  id="buyLicenses-form" action="/ilto3/index.php?r=Clients/MyAccount/orderLicenses" method="post" class="form-horizontal bv-form" data-bv-message="This value is not valid" data-bv-feedbackicons-valid="glyphicon glyphicon-ok" data-bv-feedbackicons-invalid="glyphicon glyphicon-remove" data-bv-feedbackicons-validating="glyphicon glyphicon-refresh" novalidate="novalidate">
                    <button type="submit" class="bv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
                    <div class="row">
                        <div class="col-lg-8" style="text-align:center;padding:10px;boder:1px solid black !important;">
                            <input  type="hidden" value="<?php echo $client[0]["nit_id_legal"]; ?>" class="form-control quantity" id="ilt'.$t["id_licencia"].'" name="BuyLicensesForm[client_id]" />
                            <?php
                            
                            
                            foreach($list_t as $t){
                                switch ($t["logo"]) {
                                case '1':
                                    $rutalogo = "images/tecs.png";
                                    break;
                                case '2':
                                       $rutalogo = "images/RPT.png";
                                    break;
                                case '3':
                                       $rutalogo = "images/ede.png";
                                    break;   
                                case '4':
                                       $rutalogo = "images/cedt.png";
                                    break;    
                                default:
                                    // code...
                                    break;
                            }
                            switch ($t["id_licencia"]) {
                                case $id_tecs:
                                    $description = "<span style='font-weight: 100;font-size: 15px;color: #000;'>Speaking - Grammar - Reading - Listening.</span>";
                                    $quantity_add ="";
                                    break;
                                case $id_ede:
                                    $description = "<span style='font-weight: 100;font-size: 15px;color: #000;'>Grammar - Reading - Listening.</span>";
                                    $quantity_add ="";
                                    break;
                                case '17':
                                      $description = "<span style='font-weight: 100;font-size: 15px;color: #000;'>Assess two skills from listening, reading or language use.</span>";
                                      $quantity_add ="50+";
                                    break;   
                                case '18':
                                      $description = "<span style='font-weight: 100;font-size: 15px;color: #000;'>Grammar - Reading - Listening.</span>";
                                      $quantity_add ="";
                                break;   
                                }
                                
                                $lics[$t['id_licencia']] = $t['nombre'];
                                $nombre = explode("V",$t["nombre"]);
                                $min = 'min="0"';
                                if($t["id_licencia"]=='ilt17'){
                                    $min = 'min="50"';
                                }
                                
                                echo    '<div class="col-xs-12" style="text-align:center;padding:10px; style="">
                                            <div class="row" style="width:90%;margin:auto;">  
                                                <div class="col-xs-8" style="text-align:center;background-color: #bdbbb6;padding: 10px;min-height:208px;">
                                                    <h6 style="font-size: 14px;important;color:#fff !important;font-weight:bold;">'.$nombre[0].'</h6>';
                                                    echo $description;
                                                    $SQL = "SELECT * FROM `licenses_prices` WHERE id_licencia='".$t["id_licencia"]."'";
                                                    $prices= $connection->createCommand($SQL)->query();
                                                    
                                                echo    
                                                    '<div class="col-xs-12" style="text-align:left;font-size:12px;line-height: 17px;">
                                                        <div class="row"><div class="col-xs-8" style="background-color:#7cc56b;padding: 5px;color:#FFF;text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;<b>QUANTITY</b></div><div class="col-xs-4" style="padding: 5px;background-color:#7cc56b;color: #FFF;text-align:center;"><b>UNIT PRICE</b></div></div>';
                                                        foreach ($prices as $price) {
                                                            echo '<div class="row"><div class="col-xs-8" style="border-top: 1px solid #cfd8dc;padding: 5px;color:black;font-weight: 100;font-size: 15px;">'.$price["min"].'&nbsp;to '.$price["max"].' Licenses</div><div class="col-xs-4" style="padding: 5px;border-top: 1px solid #cfd8dc;color: black;text-align:center;font-weight: 100;font-size: 15px;">$'.$price["price"].'</div></div>
                                                            ';
                                                        }
                                                        echo '<input  type="hidden" value="'.$t["id_licencia"].'" class="id_licencia" name="Licenses_'.$t["id_licencia"].'[id_licencia]" />
                                                        
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-xs-4" style="text-align:center;min-height:208px;background-color:#f3f2f2;">
                                                <img src="'.$rutalogo.'" height="60" style="margin-top: 10px;">
                                                            <p style="text-align:center;margin-top: 20px;font-weight: lighter;font-size: 16;">Quantity '.$quantity_add.'</p>
                                                            <input maxlength="10000" '.$nim.' style="width:70px !important;" placeholder="0" value="0" class="form-control quantity" id="ilt'.$t["id_licencia"].'" type="number" name="Licenses_'.$t["id_licencia"].'[amount]" />
                                                            <input type="hidden" value="'.$t["price"].'" class="form-control quantity" id="price_ilt'.$t["id_licencia"].'" name="Licenses_'.$t["id_licencia"].'[price]" /><br/>
                                                            <span style="font-size:12px;top:5px;font-weight: lighter;">Enter the quantity</span>
                                                </div>
                                            </div>
                                        </div>';
                            }
                            ?>
                        </div>
                        <div class="col-lg-4" style="text-align:center;padding: 20px 20px 20px 0px">
                            <div class="col-lg-12" style="background-color: #e8e8e8;">
                            <h4><b>Summary</b></h4>
                            <h4><b>Currency</b></h4>
                            
                            <select class="form-control search" name="currency" id="currency" disabled "">
                                <option value="<?php echo $cop_value; ?>">COP - COLOMBIA</option>
                                <option value="1" selected>USD - UNITED STATES</option>
                            </select></br>
                            <?php
                            foreach($list_t as $t){
                                 $nombre = explode(".",$t["nombre"]);
                            echo'<div class="row" id="ilt'.$t["id_licencia"].'_car"  style="padding:10px 0px;margin:0px !important;font-size:12px !important;font-weight:bold;">
                                    <div class="col-xs-12" class="licences_preview">
                                        <p style="font-size: 16px;font-weight:bold;">'.$nombre[0].'</p>
                                    </div>
                                    <div class="col-xs-8" style="text-align:left;">
                                        <span><b>Quantity</b></span>
                                    </div>
                                    <div class="col-xs-4" style="text-align:right;">
                                        <span id="cart'.$t["id_licencia"].'_amount"></span>
                                    </div>
                                    <div class="col-xs-8" style="text-align:left;">
                                        <span><b>Unit price</b></span>
                                    </div>
                                    <div class="col-xs-4" style="text-align:right;">
                                        $<span id="cart'.$t["id_licencia"].'"><b></b></span>
                                        <input type="hidden" value="" class="form-control" name="cart'.$t["id_licencia"].'_inp" id="cart'.$t["id_licencia"].'_inp"/>
                                               
                                    </div>
                                    <div class="col-xs-8" style="text-align:left;border-bottom:1px solid #d3d3d3;">
                                        <span><b>SUBTOTAL</b></span>
                                    </div>
                                    <div class="col-xs-4" style="border-bottom:1px solid #d3d3d3;text-align:right;">
                                        $<span id="subtotal_'.$t["id_licencia"].'"><b>0.0</b></span>
                                    </div>
                                </div>';
                            }
                            ?>
                            <div class="row" style="margin: 0px !important;font-size: 12px !important;display: none;padding: 10px 0px;">
                                <div class="col-xs-5">
                                    <span style="font-size:13px;">Suggested Discount: </span>
                                </div>
                                <div class="col-xs-4">
                                    <!--<input id="points" name="points" type="range" min="0" max="100" step="1" value="0" width="100"/>-->
                                    <input id="points" name="points" type="hidden" value="0"/>
                                   </div>
                                <div class="col-xs-1">
                                <span id="percent" style="font-size:16px;">0</span>&nbsp;%
                                </div>
                            </div>
                            <div class="row" id="agree" style="margin: 0px !important;font-size: 14px !important;display: block;">
                                
                                <div class="col-xs-6" style="text-align:left;background-color: #bdbbb6;padding: 10px 5px;">
                                    <span><b>SUBTOTAL</b></span>
                                </div>
                                <div class="col-xs-6"  style="text-align:right;background-color: #bdbbb6;padding: 10px 5px;">
                                    <b>$<span id="subtotal_lic">0.0</span></b>
                                </div>
                                <div class="col-xs-6" style="text-align:left;background-color: #fbcd41;;padding: 10px 5px;">
                                    <span><b>DISCOUNT</b></span>
                                </div>
                                <div class="col-xs-6"  style="text-align:right;background-color: #fbcd41;;padding: 10px 5px;">
                                    <b>$<span id="discount">0.0</span></b>
                                </div>
                                <div class="col-xs-6" style="text-align:left;background-color: #bdbbb6;padding: 10px 5px;">
                                    <span><b>TOTAL</b></span>
                                </div>
                                <div class="col-xs-6" style="text-align:right;background-color: #bdbbb6;padding: 10px 5px;">
                                    <b>$<span id="total_lic" >0.0</span></b>
                                </div>
                                
                            </div>
                            <div class="row" style="padding:10px 0px;">
                                <div class="col-xs-12">
                                   <input class="btn btn-success send" type="submit" id="send"  name="yt0" value="QUOTE NOW" style="width:100%;font-size:16px;font-weight:bold;">
                                </div>
                            </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>                                        
        </div>
    </div>
</div>
</div>
                                           

<script>



//global variables
    var sub_ilt27 = 0;
    var sub_ilt25 = 0;
    var sub_ilt19 = 0;
    var sub_ilt21 = 0;
    var sub_ilt17 = 0;
    var sub_ilt18 = 0;
    
    var ilt27_valu= 0;
    var ilt25_valu= 0;
    var ilt19_valu= 0;
    var ilt21_valu = 0;
    var ilt17_valu = 0;
    var ilt18_valu = 0;
    ilt27_quantity =  parseInt($('#ilt27').val());
    ilt25_quantity =  parseInt($('#ilt25').val());
    ilt19_quantity =  parseInt($('#ilt19').val());
    ilt21_quantity =  parseInt($('#ilt21').val());
    ilt17_quantity =  parseInt($('#ilt17').val());
    ilt18_quantity =  parseInt($('#ilt18').val());
    if(ilt17_quantity !== 'undefined'){
       ilt17_quantity = 0; 
    }
    
    if(ilt18_quantity !== 'undefined'){
       ilt18_quantity = 0; 
    }
    
    //tecs 7.0
    $( "#ilt19" ).keyup(function() {
        currency =  parseFloat($('#currency').val());
        ilt19_quantity =  parseInt($('#ilt19').val());
        if(ilt19_quantity > 0){
            $('#currency').prop('disabled', false);
        }
        switch(true) {
            case ilt19_quantity < 199:
                ilt19_valu = 30*currency;
                break;
            case ilt19_quantity >= 200 && ilt19_quantity < 500:
                ilt19_valu = 25*currency;
                break;
            case ilt19_quantity >=500 && ilt19_quantity < 1000:
                ilt19_valu = 20*currency;
                break;
            case ilt19_quantity >= 1000:
                ilt19_valu = 15*currency;
                break;
            default:
                ilt19_valu = 30*currency;
        }
        if(ilt19_quantity>0){
             $('#ilt19_car').show(); 
             $('#ilt19_car').css("background", "#f9e4a6");
        }else{
            $('#ilt19_car').hide();
        }
        
        
    	sub_ilt19 = ilt19_quantity*ilt19_valu;
        $('#subtotal_19').text(sub_ilt19.toLocaleString('de-DE'));
        $('#cart19_amount').text(ilt19_quantity);
        $('#cart19').text(ilt19_valu.toLocaleString('de-DE'));
        $('#cart19_inp').val(ilt19_valu.toLocaleString('de-DE'));
        total = (sub_ilt25 + sub_ilt27 + sub_ilt21 + sub_ilt19 + sub_ilt17 + sub_ilt18);
        $('#subtotal_lic').text(total.toLocaleString('de-DE'));
        $('#total_lic').text(total.toLocaleString('de-DE'));
    });
    
    //tecs AA
    $( "#ilt25" ).keyup(function() {
        currency =  parseFloat($('#currency').val());
        ilt25_quantity =  parseInt($('#ilt25').val());
        if(ilt25_quantity > 0){
            $('#currency').prop('disabled', false);
        }
        switch(true) {
            case ilt25_quantity < 199:
                ilt25_valu = 30*currency;
                break;
            case ilt25_quantity >= 200 && ilt25_quantity < 500:
                ilt25_valu = 25*currency;
                break;
            case ilt25_quantity >=500 && ilt25_quantity < 1000:
                ilt25_valu = 20*currency;
                break;
            case ilt25_quantity >= 1000:
                ilt25_valu = 15*currency;
                break;
            default:
                ilt25_valu = 30*currency;
        }
        if(ilt25_quantity>0){
             $('#ilt25_car').show(); 
             $('#ilt25_car').css("background", "#f9e4a6");
        }else{
            $('#ilt25_car').hide();
        }
        
        
    	sub_ilt25 = ilt25_quantity*ilt25_valu;
        $('#subtotal_25').text(sub_ilt25.toLocaleString('de-DE'));
        $('#cart25_amount').text(ilt25_quantity);
        $('#cart25').text(ilt25_valu.toLocaleString('de-DE'));
        $('#cart25_inp').val(ilt25_valu.toLocaleString('de-DE'));
        total = (sub_ilt25 + sub_ilt27 + sub_ilt21 + sub_ilt17 + sub_ilt18);
        $('#subtotal_lic').text(total.toLocaleString('de-DE'));
        $('#total_lic').text(total.toLocaleString('de-DE'));
    });
    
    //EDE7.0 
    $( "#ilt27" ).keyup(function() {
        currency =  parseFloat($('#currency').val());
        ilt27_quantity =  parseInt($('#ilt27').val());
        if(ilt27_quantity > 0){
            $('#currency').prop('disabled', false);
        }
        switch(true) {
            case ilt27_quantity < 999:
                ilt27_valu = 10*currency;
                break;
            case ilt27_quantity >= 1000:
                ilt27_valu = 7*currency;
                break;
            default:
                ilt27_valu = 10*currency;
        }
        if(ilt27_quantity>0){
             $('#ilt27_car').show(); 
             $('#ilt27_car').css("background", "#8cc6e6");
        }else{
            $('#ilt27_car').hide(); 
        }
        
    	sub_ilt27 = ilt27_quantity*ilt27_valu;
        $('#subtotal_27').text(sub_ilt27.toLocaleString('de-DE'));
        $('#cart27_amount').text(ilt27_quantity);
        $('#cart27').text(ilt27_valu.toLocaleString('de-DE'));
        $('#cart27_inp').val(ilt27_valu.toLocaleString('de-DE'));
        total = (sub_ilt25 + sub_ilt27 + sub_ilt17 + sub_ilt18);
        $('#subtotal_lic').text(total.toLocaleString('de-DE'));
        $('#total_lic').text(total.toLocaleString('de-DE'));
    });
    
    //EDE4.0 
    $( "#ilt21" ).keyup(function() {
        currency =  parseFloat($('#currency').val());
        ilt21_quantity =  parseInt($('#ilt21').val());
        if(ilt21_quantity > 0){
            $('#currency').prop('disabled', false);
        }
        switch(true) {
            case ilt21_quantity < 999:
                ilt21_valu = 10*currency;
                break;
            case ilt21_quantity >= 1000:
                ilt21_valu = 7*currency;
                break;
            default:
                ilt21_valu = 10*currency;
        }
        if(ilt21_quantity>0){
             $('#ilt21_car').show(); 
             $('#ilt21_car').css("background", "#8cc6e6");
        }else{
            $('#ilt21_car').hide(); 
        }
        
    	sub_ilt21 = ilt21_quantity*ilt21_valu;
        $('#subtotal_21').text(sub_ilt21.toLocaleString('de-DE'));
        $('#cart21_amount').text(ilt21_quantity);
        $('#cart21').text(ilt21_valu.toLocaleString('de-DE'));
        $('#cart21_inp').val(ilt21_valu.toLocaleString('de-DE'));
        total = (sub_ilt25 + sub_ilt21 + sub_ilt17 + sub_ilt18);
        $('#subtotal_lic').text(total.toLocaleString('de-DE'));
        $('#total_lic').text(total.toLocaleString('de-DE'));
    });
    //CEDT4.0 
    $( "#ilt17" ).keyup(function() {
        ilt17_quantity =  parseInt($('#ilt17').val());
        if(ilt17_quantity > 0){
            $('#currency').prop('disabled', false);
        }
        ilt17_valu = 3*currency;
        if(ilt17_quantity>0){
             $('#ilt17_car').show();
             $('#ilt17_car').css("background", "#d0aef5");
        }else{
            $('#ilt17_car').hide(); 
        }
        if(ilt17_quantity<50){
               ilt17_quantity = 50;
            }
    	sub_ilt17 = ilt17_quantity*ilt17_valu;
        $('#subtotal_17').text(sub_ilt17.toLocaleString('de-DE'));
        $('#cart17_amount').text(ilt17_quantity);
        $('#cart17').text(ilt17_valu.toLocaleString('de-DE'));
        $('#cart17_inp').val(ilt17_valu.toLocaleString('de-DE'));
        total = (sub_ilt19 + sub_ilt21 + sub_ilt17 + sub_ilt18);
        
        $('#subtotal_lic').text(total.toLocaleString('de-DE'));
        $('#total_lic').text(total.toLocaleString('de-DE'));
    });
    $( "#ilt17" ).change(function() {
        ilt17_quantity =  parseInt($('#ilt17').val());
        if(ilt17_quantity<50){
             $('#ilt17').val("50");
        }
        
    });
    //TECS ECLUV
    $( "#ilt18" ).keyup(function() {
        ilt18_quantity =  parseInt($('#ilt18').val());
        currency =  parseFloat($('#currency').val());
        if(ilt18_quantity > 0){
            $('#currency').prop('disabled', false);
        }
        switch(true) {
            case ilt18_quantity < 199:
                ilt18_valu = 29*currency;
                break;
            case ilt18_quantity >= 200 && ilt18_quantity < 500:
                ilt18_valu = 19*currency;
                break;
            case ilt18_quantity >=500 && ilt18_quantity < 1000:
                ilt18_valu = 17*currency;
                break;
            case ilt18_quantity >= 1000:
                ilt18_valu = 12*currency;
                break;
            default:
                ilt18_valu = 29*currency;
        }
        if(ilt18_quantity>0){
             $('#ilt18_car').show(); 
             $('#ilt18_car').css("background", "#f9e4a6");
        }else{
            $('#ilt18_car').hide();
        }
        
        
    	sub_ilt18 = ilt18_quantity*ilt18_valu;
        $('#subtotal_18').text(sub_ilt18.toLocaleString('de-DE'));
        $('#cart18_amount').text(ilt18_quantity);
        $('#cart18').text(ilt18_valu.toLocaleString('de-DE'));
        $('#cart18_inp').val(ilt18_valu.toLocaleString('de-DE'));
        total = (sub_ilt19 + sub_ilt21 + sub_ilt17 + sub_ilt18);
        $('#subtotal_lic').text(total.toLocaleString('de-DE'));
        $('#total_lic').text(total.toLocaleString('de-DE'));
    });
    
    $('#ilt25').keydown(function(e) {
      if(e.keyCode == 13) { // enter key was pressed
        // run own code
        return false; // prevent execution of rest of the script + event propagation / event bubbling + prevent default behaviour
      }
    });
    
    $('#ilt19').keydown(function(e) {
      if(e.keyCode == 13) { // enter key was pressed
        // run own code
        return false; // prevent execution of rest of the script + event propagation / event bubbling + prevent default behaviour
      }
    });
    $('#ilt21').keydown(function(e) {
      if(e.keyCode == 13) { // enter key was pressed
        // run own code
        return false; // prevent execution of rest of the script + event propagation / event bubbling + prevent default behaviour
      }
    });
    $('#ilt17').keydown(function(e) {
      if(e.keyCode == 13) { // enter key was pressed
        // run own code
        return false; // prevent execution of rest of the script + event propagation / event bubbling + prevent default behaviour
      }
    });
    $('#ilt18').keydown(function(e) {
      if(e.keyCode == 13) { // enter key was pressed
        // run own code
        return false; // prevent execution of rest of the script + event propagation / event bubbling + prevent default behaviour
      }
    });
    $('#points').keydown(function(e) {
      if(e.keyCode == 13) { // enter key was pressed
        // run own code
        return false; // prevent execution of rest of the script + event propagation / event bubbling + prevent default behaviour
      }
    });
    /*
    $( "#points" ).change(function() {
        
        
    	points =  parseInt($('#points').val());
        subtotal = (sub_ilt19 + sub_ilt21 + sub_ilt17 + sub_ilt18);
        discount = parseFloat(Math.round(subtotal*points)/100).toFixed(2);
        total = subtotal - discount;
        $('#percent').text(points);
        $('#discount').text(discount);
        $('#total_lic').text(total);
    });
    */
    $( "#currency" ).change(function() {
        currency =  parseFloat($('#currency').val());
        console.log(currency);
       //tecs
        switch(true) {
            case ilt25_quantity < 199:
                ilt25_valu = 30*currency;
                break;
            case ilt25_quantity >= 200 && ilt25_quantity < 500:
                ilt25_valu = 25*currency;
                break;
            case ilt25_quantity >=500 && ilt25_quantity < 1000:
                ilt25_valu = 20*currency;
                break;
            case ilt25_quantity >= 1000:
                ilt25_valu = 15*currency;
                break;
            default:
                ilt25_valu = 30*currency;
        }
        
        sub_ilt25 = ilt25_quantity*ilt25_valu;
        console.log(ilt25_valu,sub_ilt25);
        $('#subtotal_25').text(sub_ilt25.toLocaleString('de-DE'));
        $('#cart25_amount').text(ilt25_quantity);
        $('#cart25').text(ilt25_valu.toLocaleString('de-DE'));
        $('#cart25_inp').val(ilt25_valu.toLocaleString('de-DE'));
        console.log(sub_ilt25, sub_ilt27, sub_ilt21, sub_ilt17, sub_ilt18);
        total = (sub_ilt25 + sub_ilt27 + sub_ilt17 + sub_ilt18);
        console.log(total);
        $('#subtotal_lic').text(total.toLocaleString('de-DE'));
        $('#total_lic').text(total.toLocaleString('de-DE'));
        
        //tecs old
        switch(true) {
            case ilt19_quantity < 199:
                ilt19_valu = 30*currency;
                break;
            case ilt19_quantity >= 200 && ilt19_quantity < 500:
                ilt19_valu = 25*currency;
                break;
            case ilt19_quantity >=500 && ilt19_quantity < 1000:
                ilt19_valu = 20*currency;
                break;
            case ilt19_quantity >= 1000:
                ilt19_valu = 15*currency;
                break;
            default:
                ilt19_valu = 30*currency;
        }
        
        sub_ilt19 = ilt19_quantity*ilt19_valu;
        console.log(ilt19_valu,sub_ilt19);
        $('#subtotal_19').text(sub_ilt19.toLocaleString('de-DE'));
        $('#cart19_amount').text(ilt19_quantity);
        $('#cart19').text(ilt25_valu.toLocaleString('de-DE'));
        $('#cart19_inp').val(ilt19_valu.toLocaleString('de-DE'));
        console.log(sub_ilt25, sub_ilt27, sub_ilt21, sub_ilt17, sub_ilt18);
        total = (sub_ilt25 + sub_ilt27 + sub_ilt17 + sub_ilt18);
        console.log(total);
        $('#subtotal_lic').text(total.toLocaleString('de-DE'));
        $('#total_lic').text(total.toLocaleString('de-DE'));
       
       
       //EDE 7.0
       switch(true) {
            case ilt27_quantity < 999:
                ilt27_valu = 10*currency;
                break;
            case ilt27_quantity >= 1000:
                ilt27_valu = 7*currency;
                break;
            default:
                ilt27_valu = 10*currency;
        }
        
    	sub_ilt27 = ilt27_quantity*ilt27_valu;
        $('#subtotal_27').text(sub_ilt27.toLocaleString('de-DE'));
        $('#cart27_amount').text(ilt27_quantity);
        $('#cart27').text(ilt27_valu.toLocaleString('de-DE'));
        $('#cart27_inp').val(ilt27_valu.toLocaleString('de-DE'));
        total = (sub_ilt25 + sub_ilt27 + sub_ilt17 + sub_ilt18);
        $('#subtotal_lic').text(total.toLocaleString('de-DE'));
        $('#total_lic').text(total.toLocaleString('de-DE')); 
       
       
       
       //EDE
       switch(true) {
            case ilt21_quantity < 999:
                ilt21_valu = 10*currency;
                break;
            case ilt21_quantity >= 1000:
                ilt21_valu = 7*currency;
                break;
            default:
                ilt21_valu = 10*currency;
        }
        
    	sub_ilt21 = ilt21_quantity*ilt21_valu;
        $('#subtotal_21').text(sub_ilt21.toLocaleString('de-DE'));
        $('#cart21_amount').text(ilt21_quantity);
        $('#cart21').text(ilt21_valu.toLocaleString('de-DE'));
        $('#cart21_inp').val(ilt21_valu.toLocaleString('de-DE'));
        total = (sub_ilt25 + sub_ilt27 +  sub_ilt17 + sub_ilt18);
        $('#subtotal_lic').text(total.toLocaleString('de-DE'));
        $('#total_lic').text(total.toLocaleString('de-DE'));
       
        //TECS ECLV
        switch(true) {
            case ilt18_quantity < 199:
                ilt18_valu = 30*currency;
                break;
            case ilt18_quantity >= 200 && ilt18_quantity < 500:
                ilt18_valu = 25*currency;
                break;
            case ilt18_quantity >=500 && ilt18_quantity < 1000:
                ilt18_valu = 20*currency;
                break;
            case ilt18_quantity >= 1000:
                ilt18_valu = 15*currency;
                break;
            default:
                ilt18_valu = 30*currency;
        }
        
    	sub_ilt18 = ilt18_quantity*ilt18_valu;
        $('#subtotal_18').text(sub_ilt18.toLocaleString('de-DE'));
        $('#cart18_amount').text(ilt18_quantity);
        $('#cart18').text(ilt18_valu.toLocaleString('de-DE'));
        $('#cart18_inp').val(ilt18_valu.toLocaleString('de-DE'));
        total = (sub_ilt25 + sub_ilt27 + sub_ilt17 + sub_ilt18);
        $('#subtotal_lic').text(total.toLocaleString('de-DE'));
        $('#total_lic').text(total.toLocaleString('de-DE'));
    });
    
    
        
</script>
	
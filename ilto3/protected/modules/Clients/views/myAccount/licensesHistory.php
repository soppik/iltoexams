<?php
/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = $session['profile'];


if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}

if($profile == 6){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}

/* session manager
end
*/

/* @var $this FraudReportController */
/* @var $model FraudReport */

$this->breadcrumbs=array(
	'My Licenses'=>array('index'),
	'HISTORY',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#fraud-report-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

$modUtil = new Utilidades();
$types = $modUtil->arregloTiposFraude();

$config = Yii::app()->getComponents(false);
$connection=new CDbConnection('mysql:host=localhost;dbname=iltoexam_newversion;','iltoexam_newvers','Ilto.2015');
//$connection=new CDbConnection('mysql:host=localhost;dbname=thetec_iltoexam_newversion','thetec_newvers','Ilto.2015');
//$connection=new CDbConnection($config["db"]["connectionString"],$config["db"]["username"],$config["db"]["password"]);
$connection->active=true;


$SQL = "SELECT * FROM `orders` WHERE id_client =  '".Yii::app()->user->getState('cliente')."' ORDER BY id_order DESC LIMIT 1;";
$list_u= $connection->createCommand($SQL)->queryAll();    

$SQL = "SELECT id_licencias_cliente, licenses.nombre, cantidad, fecha_asignacion, fecha_final, licenses_client.estado, demo FROM `licenses_client` INNER JOIN licenses ON licenses_client.id_licencia = licenses.id_licencia WHERE `id_cliente` = '".Yii::app()->user->getState('cliente')."' ORDER BY fecha_asignacion DESC;";

$licenses= $connection->createCommand($SQL)->queryAll();    

// agregamos al log del usuario access
$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 4, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'Access licence history', '')";
$log= Yii::app()->db->createCommand($SQL)->query(); 

?>
<style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
    /* display: none; <- Crashes Chrome on hover */
    -webkit-appearance: none;
    margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
}
    
</style>

<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
                                    <!--<div class="widget">
                                        <div class="widget-header bg-blue">
                                            <span class="widget-caption">INSTRUCTIONS</span>
                                        </div>
                                        <div class="widget-body">
                                            Following the quote information. Check if your order has been approved in the status box. You may also check the quotation details in the VIEW box and print the quotation by clicking the PRINT button.
                                        </div>
                                    </div>-->
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="widget">
                                <div class="widget-header ">
                                    <span class="widget-caption">HISTORY</span>
                                   </div>
                                <div class="widget-body">
                                    <table class="table table-striped table-hover table-bordered" id="editabledatatable">
                                        <thead>
                                            <tr role="row" style="text-align:center;">
                                                <th style="text-align:center;">
                                                    LICENSE
                                                </th>
                                                <th style="text-align:center;">
                                                    QUANTITY
                                                </th>
                                                <th style="text-align:center;">
                                                    USED
                                                </th>
                                                <th style="text-align:center;">
                                                    USED AS TECS 
                                                </th>
                                                <th style="text-align:center;">
                                                    USED AS TECS2GO
                                                </th>
                                                <th style="text-align:center;">
                                                    PURCHASE DATE
                                                </th>
                                                <th style="text-align:center;">
                                                    EXPIRATION DATE
                                                </th>
                                                 <th style="text-align:center;">
                                                    STATUS
                                                </th>
                                                <th style="text-align:center;">
                                                    DEMO
                                                </th>
                                                
                                              
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            foreach ($licenses as $value) {
                                                
                                                
                                                //tecs USED
                                                $SQL = "SELECT COUNT(id_licencias_usuario) as used FROM `licenses_user` WHERE id_licencia_cliente =  '".$value["id_licencias_cliente"]."'";
                                                $used= $connection->createCommand($SQL)->queryAll();    
                                                
                                                //tecs2go allocated
                                                $SQL = "SELECT COUNT(id_proctorv_request) as total FROM `proctorv_request` INNER JOIN licenses_user ON proctorv_request.id_license = licenses_user.id_licencias_usuario WHERE id_licencia_cliente =  '".$value["id_licencias_cliente"]."'";
                                                $tecs2go_allocated= $connection->createCommand($SQL)->queryAll();    
                                                
                                                   switch ($value['estado']) {
                                                    case 'A':
                                                        $status= "<span style='color:#53a93f;'>AVAILABLE</span>";
                                                    break;
                                                    case 'F':
                                                        $status= "Completed";
                                                    break;
                                                    case 'S':
                                                        $status= "<span style='color:#f4b400;'>Suspended</span>";
                                                    break;
                                                    default:
                                                    $status= "Expired";
                                            }
                                                $lic = explode("V", $value["nombre"]);
                                            ?>
                                                <tr>
                                                    <td style="text-align:center;"><?php echo $value["id_licencias_cliente"]." - ".$lic[0]?></td>
                                                    <td style="text-align:center;"><?php echo $value['cantidad']?></td>
                                                    <td style="text-align:center;"><?php echo $used[0]['used']?></td>
                                                    
                                                     <td style="text-align:center;"><?php 
                                                        echo $used[0]['used'] - $tecs2go_allocated[0]["total"];
                                                     
                                                     ?></td>
                                                     <td style="text-align:center;"><?php echo $tecs2go_allocated[0]["total"] ?></td>
                                                    <td style="text-align:center;"><?php echo substr($value['fecha_asignacion'], 0, 10)?></td>
                                                    <td style="text-align:center;"><?php echo substr($value['fecha_final'], 0, 10);?></td>
                                                    <td style="text-align:center;"><?php echo $status?></td>
                                                    <td>
                                                     <?php if($value["demo"]=='1'){
                                                         echo 'DEMO';
                                                     }else{
                                                         
                                                     }
                                                     
                                                     ?>
                                                     </td>
                                                </tr>
                                             <?php 
                                             }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                       
                        <?php if(!empty($list_inv)){
                        
                            echo '<div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="widget">
                                <div class="widget-header ">
                                    <span class="widget-caption">MY INVOICES</span>
                                   </div>
                                <div class="widget-body">
                                <table class="table table-striped table-hover table-bordered" id="editabledatatable">
                                        <thead>
                                            <tr role="row" style="text-align:center;">
                                                <th style="text-align:center;">
                                                    INVOICE ID 
                                                </th>
                                                <th style="text-align:center;">
                                                    ISSUE DATE
                                                </th>
                                                <th style="text-align:center;">
                                                    DUE DATE
                                                </th>
                                                <th style="text-align:center;">
                                                    QUOTATION ID
                                                </th>
                                                <th style="text-align:center;">
                                                    SUBTOTAL
                                                </th>
                                                <th style="text-align:center;">
                                                    DISCOUNT
                                                </th>
                                                 <th style="text-align:center;">
                                                    TOTAL
                                                </th>
                                                <th style="text-align:center;">
                                                    PDF
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>';
                                            foreach($list_inv as $inv){
                                                echo '
                                                <tr>
                                                    <td style="text-align:center;">
                                                    '.$inv['id_invoice'].'
                                                    </td>
                                                    <td style="text-align:center;">
                                                    '.substr($inv['date_expedition'], 0, 10).'
                                                    </td>
                                                    <td style="text-align:center;">
                                                    '.substr($inv['date_expire'], 0, 10).'
                                                    </td>
                                                    <td style="text-align:center;">
                                                    '.$inv['id_order'].'
                                                    </td>
                                                    <td style="text-align:center;">$ 
                                                    '.number_format($inv['subtotal']).'
                                                    </td>
                                                    <td style="text-align:center;">
                                                    '.$inv['discount'].'
                                                    %</td>
                                                    <td style="text-align:center;">$ 
                                                    '.number_format($inv['total']).'
                                                    </td>
                                                    <td>
                                                     <a title="View_order_Detail" target="_blank" style="font-size:10px;" href="/ilto3/index.php?r=Clients/MyAccount/InvoiceDetail&amp;id='.$inv['id_invoice'].'" class="btn btn-success btn-xs edit"><i class="fa fa-check"></i>Detail</a>
                                                    </td>
                                                </tr>';    
                                                                                         
                                            }
                            }
                            ?>                
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                       
                        
                        


   <!--Page Related Scripts-->
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/jquery.dataTables.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/ZeroClipboard.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.tableTools.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.bootstrap.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/datatables-init.js"></script>
    <script>
        InitiateSimpleDataTable.init();
        InitiateExpandableDataTable.init();
        InitiateSearchableDataTable.init();
    </script>
<?php 
/*
//echo "Searching...".$busca;
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'fraud-report-grid',
	'dataProvider'=>$model->search($busca),
	'filter'=>$model,
	'columns'=>array(
		'document',
		'first_name',
		'sure_name',
            array(
                'name'=>'country',
                'value'=>'$data->nombrePais()',
                'filter'=>CHtml::listData(Countries::model()->findAll(),"id_pais","nombre"),
            ),
            array(
                'name'=>'type',
                'filter'=>$types,
                'value'=>'$data->nombreTipo()',
            ),
		'report_date',

	),
));*/
?>

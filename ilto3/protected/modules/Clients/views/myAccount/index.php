<?php
/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = $session['profile'];


if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}

if($profile == 6){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}

/* session manager
end
*/

/* @var $this MyAccountController */
$this->layout="//layouts/column2";
$criterio = new CDbCriteria;
$criterio->order = 'fecha_asignacion ASC';
$modLicencias = LicensesClient::model()->clienteActual()->disponibles()->findAll($criterio);
$modCliente = Clients::model()->findByPk(Yii::app()->user->getState('cliente'));


$config = Yii::app()->getComponents(false);
$connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
// establish connection. You may try...catch possible exceptions
$connection->active=true;

$SQL = "SELECT id_licencias_cliente, id_cliente, licenses_client.id_licencia, nombre, sum(cantidad) as cantidad, sum(utilizados) as utilizados, fecha_asignacion, fecha_final, licenses_client.estado, logo FROM `licenses_client` INNER JOIN licenses ON licenses_client.id_licencia = licenses.id_licencia WHERE `id_cliente` LIKE '".Yii::app()->user->getState('cliente')."' AND licenses.estado LIKE 'A' AND licenses_client.estado = 'A' GROUP BY id_licencia ORDER BY fecha_asignacion DESC";
$licenses= Yii::app()->db->createCommand($SQL)->queryAll();

//tecs2go total
$SQL = "SELECT COUNT(id_proctorv_request) AS total FROM `proctorv_request` INNER JOIN users_c ON proctorv_request.id_user = users_c.id_usuario_c WHERE id_cliente = '".Yii::app()->user->getState('cliente')."' ";
$tecs2go_total= Yii::app()->db->createCommand($SQL)->queryAll();

//tecs2go allocated
$SQL = "SELECT COUNT(id_proctorv_request) AS total FROM `proctorv_request` INNER JOIN users_c ON proctorv_request.id_user = users_c.id_usuario_c INNER JOIN licenses_user ON proctorv_request.id_license = licenses_user.id_licencias_usuario WHERE id_cliente = '".Yii::app()->user->getState('cliente')."' AND id_license IS NOT NULL ";
$tecs2go_allocated= Yii::app()->db->createCommand($SQL)->queryAll();


//tecs2go allocated
$SQL = "SELECT COUNT(id_proctorv_request) AS total FROM `proctorv_request` INNER JOIN users_c ON proctorv_request.id_user = users_c.id_usuario_c INNER JOIN licenses_user ON proctorv_request.id_license = licenses_user.id_licencias_usuario WHERE id_cliente = '".Yii::app()->user->getState('cliente')."' AND id_license IS NOT NULL AND licenses_user.estado = 'F'";
$tecs2go_finished= Yii::app()->db->createCommand($SQL)->queryAll();

// agregamos al log del usuario access
$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 4, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'Accesss customer licences', '')";
$log= Yii::app()->db->createCommand($SQL)->query(); 

?>
<style>
    .widget-body h3 {
        font-weight:bold;
    }
    
    .history_licences {
        font-size:14px;
    }
    
    .history_licences td{
        padding:5px;
        text-align:center;
    }
    
    .calendar-day-head{
        background-color:#5db2ff;
        color:#fff;
        font-weight:800;
    }
    
    .widget-body h3{
        text-transform:uppercase;
        }
    
    .bold_h{
        font-weight:bold !important;
    }
    
    .items_licenses {
        padding:0px !important;
        padding-left:0px !important;

    }
    
    #buttons_container {
        text-align: center;
        margin-bottom: 20px;
    }
    
    .controls {
        font-size: 14px;
        width: 200px;
        padding: 20px;
        margin: 20px 18px;
    }
    
</style>
<div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget" style="margin-bottom:0px;">
            <div class="widget-header bg-blue">
                <i class="widget-icon fa fa-arrow-left"></i>
                <span class="widget-caption">LICENSES INFORMATION </span>
               
            </div><!--Widget Header-->
            <div class="widget-body">
                According to the Administrator profile, you can see the amount of purchased and available licenses, the test type, date of purchase as well as order more test licenses.
            </div>&nbsp;
            
        </div>
    </div>
</div>
<div class="col-lg-12 col-sm-12 col-xs-12" id="buttons_container">
    <a  href="/ilto3/index.php?r=Clients/MyAccount/buylicenses" class="btn btn-success controls">BUY MORE LICENSES</a>
    <a  href="/ilto3/index.php?r=Clients/MyAccount/orderHistory" class="btn btn-warning controls">MY QUOTES</a>
    <a  href="/ilto3/index.php?r=Clients/MyAccount/licensesHistory" class="btn btn-info controls" style="width:250px;">LICENSE ALLOCATION HISTORY</a>
</div>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header ">
                <span class="widget-caption">Purchase history</span>
                 <?php if(Yii::app()->user->getState('id_perfil')<=2){ //Solo para el TOAA?>
                <!--<a href="index.php?r=Clients/MyAccount/buyLicenses" class="btn btn-success">Buy more Licences</a>-->
                 <?php } ?>
            </div>
            <div class="widget-body">
                <div class="row">
                            <!--<div class="col-xs-12" style="text-align:center;margin: 10px;">
                                <div class="row" style="border-bottom:1px solid #d3d3d3;width:100%;margin:auto;padding:15px;">         
                                    <div class="col-xs-2">
                                    	<img src="images/tecs-2-go.png" height="80">
                                    	
                                        
                                    </div>
                                    <div class="col-xs-5" style="text-align:left;">
                                        <h5><b>TECS2GO LICENSES USED</b></h5>
                                        <a href="/ilto3/index.php?r=Clients/MyAccount/licensesHistory" class="btn btn-success">GET MORE INFO</a>
                                        
                                    </div>
                                    <div class="col-xs-5" >
                                        <div class="col-xs-4" style="color:#fff;text-align:center;background-color:#5db2ff;font-weight:bold;padding:10px;">
                                            <h6 style="color:#333;font-size:14px;font-weight:400 !important;">TOTAL REQUESTS<br/>
                                           <?php // echo $tecs2go_total[0]["total"]; ?></h6>
                                        </div>
                                         <div class="col-xs-4 items_licenses" style="color:#fff;text-align:center;background-color:#49a4f7;padding:10px !important;color:#fff;">
                                             <h6 style="color:#333;font-size:14px;font-weight:400 !important;">TOTAL ALLOCATED<br/>
                                             <?php // echo $tecs2go_allocated[0]["total"]; ?></h6>
                                        </div>
                                         <div class="col-xs-4" style="color:#fff;text-align:center;background-color:#2b96f7;padding:10px;">
                                           <h6 style="color:#333;font-size:14px;font-weight:400 !important;">TOTAL COMPLETED<br/>
                                             <?php // echo $tecs2go_finished[0]["total"]; ?></h6>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                <div class="row">
                    <?php 
                    
                        foreach($licenses as $license){
                            
                            switch ($license["logo"]) {
                                case '1':
                                    $rutalogo = "images/tecs.png";
                                    break;
                                case '2':
                                       $rutalogo = "images/RPT.png";
                                    break;
                                case '3':
                                       $rutalogo = "images/ede.png";
                                    break;   
                                case '4':
                                       $rutalogo = "images/cedt.png";
                                    break;   
                                case '5':
                                       $rutalogo = "images/relang.png";
                                    break;  
                                default:
                                    // code...
                                    break;
                            }
                            
                            $lic = explode("V", $license["nombre"]);
                            
                           echo '<div class="col-xs-12" style="text-align:center;margin: 10px;">
                                <div class="row" style="border-bottom:1px solid #d3d3d3;width:100%;margin:auto;padding:15px;">         
                                    <div class="col-xs-2">
                                        <img src="'.$rutalogo.'" height="60">
                                    </div>
                                    <div class="col-xs-5" style="text-align:left;">
                                        <h5><b>'.strtoupper($lic[0]).'</b></h5>
                                        <h5><b>Allocation date: </b>'.substr($license["fecha_asignacion"], 0, 10).'</h5>
                                        <h5><b>Expiration date: </b>'.substr($license["fecha_final"], 0, 10).'</h5>
                                    </div>
                                    <div class="col-xs-5" style="padding:0px !important;">
                                        <div class="col-xs-4" style="color:#fff;text-align:center;background-color:#5db2ff;font-weight:bold;padding:10px;">
                                            <h6 style="color:#333;font-size:14px;font-weight:400 !important;">AMOUNT<br/>
                                           '.number_format(($license["cantidad"]),0).'</h6>
                                        </div>
                                         <div class="col-xs-4 items_licenses" style="color:#fff;text-align:center;background-color:#49a4f7;padding:10px !important;color:#fff;">
                                             <h6 style="color:#333;font-size:14px;font-weight:400 !important;">USED<br/>
                                             '.number_format(($license["utilizados"]),0).'</h6>
                                        </div>
                                         <div class="col-xs-4" style="color:#fff;text-align:center;background-color:#2b96f7;padding:10px;">
                                             <h6 style="color:#333;font-size:14px;font-weight:400 !important;">REMAINING<br/>
                                            '.number_format(($license["cantidad"] - $license["utilizados"]),0).'</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>';
                           
                           /* echo "<tr><td class='calendar-day mediana'>".str_replace(" ","&nbsp;",$rowLicencia->idLicencia->nombre)."</td>";
                            echo "<td class='calendar-day mediana centrado'>".  substr($rowLicencia->fecha_asignacion,0,10)."</td>";
                            echo "<td class='calendar-day mediana centrado'>".  number_format(($rowLicencia->cantidad),0)."</td>";
                            echo "<td class='calendar-day  mediana centrado'>".  number_format(($rowLicencia->utilizados),0)."</td>";
                            echo "<td class='calendar-day  mediana centrado'>".  number_format(($rowLicencia->cantidad - $rowLicencia->utilizados),0)."</td></tr>";
                        */}
                        if(Yii::app()->user->getState('id_perfil')==2){
                        ?>
                        <!-- HISTORY Y BUY LINCENCES 
                        <<tr class="calendar-row"><td class="calendar-day  grande" colspan="4"> <a href='index.php?r=Clients/MyAccount/buyLicenses' style="color:blue; text-decoration: underline;">Buy More Licenses</a></td><td class="calendar-day  grande"><a href="#" style="color:blue; text-decoration: underline;">History</a></td></tr>-->
                        <?php } ?>
                        </div> 
                        
                    <?php
                    if($modCliente->is_distributor == 1){
                        $criterio = new CDbCriteria;
                        $criterio->order = 'fecha_asignacion DESC';
                        $modCompradores = Clients::model()->findAllByAttributes(array('id_distributor'=>$modCliente->id_cliente));
                        if(count($modCompradores)>0){
                            echo "<h3><b>My Clients</b></h3>";
                        }
                        foreach($modCompradores as $rowCompradores){
                            
                            echo "<table class='history_licences'>";
                            echo '<tr class="calendar-row"><td class="calendar-day-head" style="width:200px;">'.$rowCompradores->nombre_rsocial.'</td><td class="calendar-day-head" >&nbsp;&nbsp;&nbsp;&nbsp;Last&nbsp;Purchased&nbsp;&nbsp;&nbsp;&nbsp;</td><td class="calendar-day-head">Amount</td><td class="calendar-day-head">Used</td><td class="calendar-day-head">Remaining</td></tr>';
                            $modLicenciasComprador = LicensesClient::model()->disponibles()->findAllByAttributes(array('id_cliente'=>$rowCompradores->id_cliente));
                            foreach($modLicenciasComprador as $rowLicencia){
                                echo "<tr><td class='calendar-day mediana'>".str_replace(" ","&nbsp;",$rowLicencia->idLicencia->nombre)."</td>";
                                echo "<td class='calendar-day mediana centrado '>".  substr($rowLicencia->fecha_asignacion,0,10)."</td>";
                                echo "<td class='calendar-day mediana centrado'>".  number_format(($rowLicencia->cantidad),0)."</td>";
                                echo "<td class='calendar-day  mediana centrado'>".  number_format(($rowLicencia->utilizados),0)."</td>";
                                echo "<td class='calendar-day  mediana centrado'>".  number_format(($rowLicencia->cantidad - $rowLicencia->utilizados),0)."</td></tr><br/>";
                            }
                            echo "</table>";
                        }
                    }
                    ?>
                    
            </div>
        </div>
    </div>
    
    
    
<script>
        $(".tool_tip").mouseover(function(){
           eleOffset = $(this).offset();
          $(this).next().fadeIn("fast").css({
                                  left: eleOffset.left + $(this).outerWidth(),
                                  top: eleOffset.top
                          });
          }).mouseout(function(){
                  $(this).next().hide();
          });

    </script>
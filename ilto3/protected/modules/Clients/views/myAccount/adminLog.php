<?php
/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = Yii::app()->user->getState('id_perfil');
if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}

if($profile != 2){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}

/* session manager
end
*/

/*
Consulta de resultados de cada exámen (EDE)
SELECT concat(users_c.nombres," ",users_c.apellidos) as nombres, answers_exam_user. * , questions.texto AS text_question, questions.titulo_respuesta AS text_image, answers.texto AS text_answer, answers.es_verdadera, licenses_user.calificacion, licenses_user.nivel
FROM answers_exam_user, questions, answers, exams_user, licenses_user, users_c
WHERE answers_exam_user.id_pregunta = questions.id_pregunta
AND answers_exam_user.id_respuesta = answers.id_respuesta
AND (
answers_exam_user.id_examen_usuario = exams_user.id_examen_usuario
AND exams_user.id_licencia_usuario = licenses_user.id_licencias_usuario
AND licenses_user.id_licencia_cliente = 20
AND licenses_user.id_usuario = users_c.id_usuario_c
)
ORDER BY  `answers_exam_user`.`id_examen_usuario` ASC  
 */

/* @var $this LicensesUserController */
/* @var $model LicensesUser */


$config = Yii::app()->getComponents(false);

$connection = new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
$connection->active=true;

$this->breadcrumbs=array(
	'MyAccount'=>array('index'),
	'Admin log',
);


//$SQL = "SELECT *  FROM `licenses_user` `t`  LEFT OUTER JOIN `users_c` `idUsuario` ON (`t`.`id_usuario`=`idUsuario`.`id_usuario_c`) AND (id_cliente='".Yii::app()->user->getState('cliente')."' and id_perfil >= 2) 
//LEFT OUTER JOIN `licenses_client` `idLicenciaCliente` ON (`t`.`id_licencia_cliente`=`idLicenciaCliente`.`id_licencias_cliente`) WHERE (idLicenciaCliente.id_cliente LIKE '%".Yii::app()->user->getState('cliente')."%') LIMIT 100";

//$SQL = "SELECT `id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user` FROM `adm_log` INNER JOIN `users_c` ON `users_c`.id_usuario_c = `adm_log`.id_adm WHERE users_c.id_cliente = '".Yii::app()->user->getState('cliente')."' AND users_c.id_usuario_c != '".Yii::app()->user->getState('usuario')."' AND date_time >= '2019-09-24 11:30:00' ORDER BY date_time DESC LIMIT 100";
$SQL = "SELECT `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user` FROM `adm_log` INNER JOIN `users_c` ON `users_c`.id_usuario_c = `adm_log`.id_adm WHERE users_c.id_cliente = '".Yii::app()->user->getState('cliente')."' AND users_c.id_usuario_c != '".Yii::app()->user->getState('usuario')."' AND `adm_log`.`date_time` >= '2019-09-30 12:00:00' ORDER BY date_time DESC LIMIT 100";
$list_l= $connection->createCommand($SQL)->queryAll();


// agregamos al log del usuario
$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 4, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'get Dinamic report', '')";
$log= Yii::app()->db->createCommand($SQL)->query(); 


?>
<style>
    input[type=number]::-webkit-outer-spin-button,
	input[type=number]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
	}
	
	input[type=number] {
    -moz-appearance:textfield;
	}
</style>
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="widget">
                                        <div class="widget-header bg-blue">
                                            <i class="widget-icon fa fa-arrow-left"></i>
                                            <span class="widget-caption">Log USER HISTORY LOG</span>
                                           
                                        </div><!--Widget Header-->
                                        <div class="widget-body">
                                            <div>
                                               This section records the user activity in the administror dashboard	
                                            </div>
                                        </div><!--Widget Body-->
                                    </div><!--Widget-->
                            <div class="widget">
                                <div class="widget-header ">
                                    <span class="widget-caption">Reports</span>
                                        <div class="alert alert-error" id="alert" style="margin:0px; padding:0px; color:red; float: left;margin-top: 8px;">
    				                    <strong></strong>
    			                     </div>
                                <div class="row">
                                    <!-- <a class="btn btn-success" href="/ilto3/index.php?r=Clients/LicensesUser/ReportDownload">Click here to download the Report</a> -->
                                </div>
                                <div class="widget-body">
                                    <form class="search-form" action="/ilto3/index.php?r=Api/log" onsubmit="return false">
                                         <input type="hidden" value="<?php echo Yii::app()->user->getState('cliente') ?>" name="id_cliente">
                                    <table class="table table-striped table-hover table-bordered" id="editabledatatable" > 
                                   
                                            <tr role="row">
                                                <th>
                                                    LOG TYPE
                                                    <select class="form-control search" name="log_type" data-related="adm_log.log_type">
                                                        <option value="">
                                                        <option value="0">Login</option>
                                                        <option value="1">Action</option>
                                                        <option value="3">Logout</option>
                                                        <option value="4">Access</option>
                                                    </select>
                                                </th>
                                                <th>
                                                  <div class="input-group">
    								                    <input class="search form-control date-picker" id="dp4" type="text" name="startDate" data-related="licenses_user.fecha_presentacion" value="FROM">
    								                    <span class="input-group-addon">
        								                    <i class="fa fa-calendar"></i>
    								                    </span>
								                    </div>
								                     <div class="input-group">
    								                    <input class="search form-control date-picker" id="dp5" type="text" name="endDate" data-related="licenses_user.fecha_presentacion" value="TO">
    								                    <span class="input-group-addon">
        								                    <i class="fa fa-calendar"></i>
    								                    </span>
								                    </div>
                                                 </th>
                                                 <th>
                                                    USERNAME ADMIN
                                                    <input type="text" name="id_adm" class="search form-control" data-related="adm_log.id_adm"/> 
                                                 </th>
                                                 <th>
                                                    DESCRIPTION
                                                    <input type="text" name="comment" class="search form-control" data-related="id_adm.comment"/> 
                                                 </th>
                                                 <th>
                                                    USERNAME TAKER 
                                                    <input type="text" name="id_user" class="search form-control" data-related="id_adm.id_user"/> 
                                                </th>
                                                <th>
                                                    ID LICENSE
                                                    <input type="number" name="id_license" id="calificacion" class="search form-control" data-related="adm_log.id_license"/> 
                                                 </th>
                                                 <th>
                                                    ID EXAM
                                                </th>
                                                
                                            </tr>
                                   

                                        <tbody class="search-rows">
                                            
                                            <?php 
                                                foreach($list_l as $log){
                                                    ?>
                                                        <tr>
                                                            <td>
                                                                <?php 
                                                                    switch($log['log_type']){
                                                                        case 0:
                                                                            $type = "Login"; 
                                                                        break;
                                                                        case 1:
                                                                            $type = "Action"; 
                                                                        break;
                                                                        case 3:
                                                                            $type = "Logout"; 
                                                                        break;
                                                                        case 4:
                                                                            $type = "Access"; 
                                                                        break;
                                                                        default:
                                                                            $type = "Action";
                                                                        break;
                                                                    
                                                                    }
                                                                    echo $type;
                                                                ?>
                                                            </td>
                                                            <td><?php echo $log['date_time'];?></td>
                                                            <td><?php echo $log['id_adm'];?></td>
                                                            <td><?php echo utf8_encode($log['comment']); ?></td>
                                                            <td><?php echo $log['id_user'];?></td>
                                                            <td><?php echo $log['id_license'];?></td>   
                                                            <td><?php echo $log['id_exam'];?></td>   
                                                        </tr>
                                                    <?php
                                                }
                                            ?>
                                            
                                            
                                            
                                        </tbody>
                                    </table>
                                    </form>
                                </div>
                            </div>
                        </div>


   <!--Page Related Scripts-->
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/jquery.dataTables.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/ZeroClipboard.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.tableTools.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.bootstrap.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/datatables-init.js"></script>
    <!--Bootstrap Date Picker-->
    <script src="/ilto3/themes/ilto/html/assets/js/datetime/bootstrap-datepicker.js"></script>
    <script>
        InitiateSimpleDataTable.init();
        InitiateExpandableDataTable.init();
        InitiateSearchableDataTable.init();
       
    </script>
    <script>
    
            var startDate = new Date();
			var endDate = new Date();
			$('#dp4').datepicker({
				format: 'yyyy-mm-dd'
			});
			$('#dp5').datepicker({
				format: 'yyyy-mm-dd'
			});
			
     $('#dp4').datepicker()
				.on('changeDate', function(ev){
					if (ev.date.valueOf() > endDate.valueOf()){
						$('#alert').show().find('strong').text('The final date cannot be previous to  the starting date.');
					} else {
						$('#alert').hide();
						startDate = new Date(ev.date);
						$('#startDate').text($('#dp4').data('date'));
					}
					$('#dp4').datepicker('hide');
				});
			$('#dp5').datepicker()
				.on('changeDate', function(ev){
					if (ev.date.valueOf() < startDate.valueOf()){
						$('#alert').show().find('strong').text('The final date cannot be previous to  the starting date.');
					} else {
						$('#alert').hide();
						endDate = new Date(ev.date);
						$('#endDate').text($('#dp5').data('date'));
					}
					$('#dp5').datepicker('hide');
				});
				$(function(){
											  $('#calificacion').keypress(function(e){
											    if(e.which == 101){
											    	return false;
											    }
											  });
											});
    </script>

<?php /*
<h1 style="float:left;" ></h1> 
<div class="instrucciones">Here you can see the test results and filter them by different categories to find out the amount of students in each category. 
    For example the amount of students in level A1 or the amount of students of a certain group.</div> 
<?php 
$modUtil = new Utilidades;
$arregloCef = $modUtil->arregloNiveles();
$gridWidget=$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'licenses-user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
                array(
                    'name'=>'laLicencia',
                    'value'=>'$data->idLicenciaCliente->idLicencia->nombre',
                    'filter'=>CHtml::listData(Licenses::model()->forsale()->findAll(),'id_licencia','nombre'),
                    //'filter'=>CHtml::listData(LicensesClient::model()->with('idLicencia')->findAll(),'id_licencia_cliente','idLicencia.nombre'),
                ),
                array(
                    'name'=>'usuario_grupo',
                    'value'=>'$data->idUsuario->idGrupoCliente->nombre',
                    'filter'=>CHtml::listData(GroupsUsersClients::model()->findAll(),'id','nombre'),
                ),
                array(
                    'name'=>'usuario_id',
                    'header'=>'User ID',
                    'value'=> '$data->id_usuario'
                ),
                array(
                    'header'=>'Email',
                    'value'=> '$data->idUsuario->email'
                ),
                array(
                    'name'=>'usuario_nombre',
                    'value'=> '$data->idUsuario->nombres'
                ),
                array(
                    'name'=>'usuario_apellido',
                    'value'=> '$data->idUsuario->apellidos'
                ),
		'fecha_asignacion',
		'calificacion',
            array(
                'name'=>'nivel',
                'filter'=> $arregloCef,
            ),
                array(
                    'name'=>'estado',
                    'value'=>'$data->estado=="A" ? "Not Finished" : ($data->estado=="F" ? "Final" : "")',
                    'filter'=>array("A"=>"Not Finished","F"=>"Final"),
                    ),
	),
)); 

//$this->renderExportGridButton($gridWidget,'Export Results',array('class'=>'btn btn-info pull-right'));?>
<script>
        $(".tool_tip").mouseover(function(){
           eleOffset = $(this).offset();
          $(this).next().fadeIn("fast").css({
                                  left: eleOffset.left + $(this).outerWidth(),
                                  top: eleOffset.top
                          });
          }).mouseout(function(){
                  $(this).next().hide();
          });
        $('.sort-link').attr('title','Click to Sort');  
    </script>

*/ ?>
<?php
/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = $session['profile'];


if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}

if($profile == 6){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}

/* session manager
end
*/

$this->layout = "//layouts/column2";
$modCliente = Clients::model()->findByPk(Yii::app()->user->getState('cliente'));
$modUsuario = UsersC::model()->findByAttributes(array('id_usuario_c'=>Yii::app()->user->getState('usuario')));


if($modUsuario->id_perfil<=2){
    $display = "";
} else {
    $display = "readonly='readonly'";
}

?>

<?php 
$model = $modUsuario;
$model->clave='';
$model->clave2='';
/*$form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-c-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
)); */

$config = Yii::app()->getComponents(false);
$connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
// establish connection. You may try...catch possible exceptions
$connection->active=true;



$SQL = "SELECT id_country, id_ciudad FROM `clients` WHERE id_cliente='".Yii::app()->user->getState('cliente')."'";
$list= $connection->createCommand($SQL)->queryAll();

$SQL = "SELECT co.Name as country, ci.Name as city FROM `country` co, `city` ci WHERE Code='".$list[0]['id_country']."' AND CountryCode='".$list[0]['id_country']."' AND ID='".$list[0]['id_ciudad']."'";
$list = $connection->createCommand($SQL)->queryAll();


?>


<script>
    function divclose(divObj){
        $(divObj).fadeOut();
    }
</script>    
<?php
  foreach(Yii::app()->user->getFlashes() as $key => $message) {
      echo '<div onClick="divclose(this);" id="flash-'.$key.'" class="flash-' . $key . '">' . $message . "&nbsp;&nbsp;&nbsp;Click Here to dismiss</div>\n";
  }
?>
<style>
    .profile-stats .stats-col .stats-value {
        font-size: 25px!important;
    }
    .tab-content.tabs-flat {
        top: 25px;
        width: 97.6%;
        margin-left: 14px;
    }
    
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: #fff;
    cursor: inherit;
    display: block;
}

.custom-upload{
    text-align: center;
    margin-left: auto;
    margin-right: auto;
    display: block;
    position: relative;
    margin-left: 25%;
}
input[type=number]::-webkit-outer-spin-button,
	input[type=number]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
	}

</style>

    <div class="tab-content tabs-flat">
        <form role="form" id="users-c-form" action="/ilto3/index.php?r=Clients/MyAccount/myAccount" enctype="multipart/form-data" method="post">
        <div id="settings" class="tab-pane active">
            <div class="form-title">
                EDIT PERSONAL INFORMATION 
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <span class="input-icon icon-right">
                            <input type="text" maxlength="100" size="60" class="form-control" name="UsersC[nombres]" value="<?php echo $model->nombres?>" id="UsersC_nombres" placeholder="First Name">
                            <i class="fa fa-user blue"></i>
                        </span>
                    </div>
                </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <span class="input-icon icon-right">
                        <input type="text" class="form-control" name="UsersC[apellidos]" value="<?php echo $model->apellidos?>" id="UsersC_apellidos" maxlength="100" size="60" placeholder="Surname">
                        <i class="fa fa-user orange"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <span class="input-icon icon-right">
                        <input type="email" class="form-control" placeholder="Email Address" value="<?php echo $model->email?>" value="<?php echo $model->email?>" name="UsersC[email]" id="UsersC_email" maxlength="200">
                        <i class="glyphicon glyphicon-envelope yellow"></i>
                    </span>
                </div>
            </div>
        </div>
        <hr class="wide">
            <div class="form-title">
                CHANGE PASSWORD
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <span class="input-icon icon-right">
                            <input type="password" class="form-control" name="UsersC[clave]" id="UsersC_clave" placeholder="New Password" size="60" maxlength="200">
                        </span>
                    </div>
                </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <span class="input-icon icon-right">
                        <input type="password" class="form-control" name="UsersC[clave2]" id="UsersC_clave2" placeholder="Re-type New Password" size="60" maxlength="200" onkeyup="checkPass(); return false;">
                    </span>
                    <span id="confirmMessage" class="confirmMessage" style="font-weight:bold;"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 custom-upload">
                <span class="file-input btn btn-block btn-default btn-file">
                    <input id="ytUsersC_ruta_foto" type="hidden" value="" name="UsersC[ruta_foto]">
                    UPLOAD PHOTO HERE <input type="file" multiple="" name="UsersC[ruta_foto]" id="UsersC_ruta_foto">
                </span>
        	   <p>Picture restrictions:<br/>
        	   File type: jpg, png, gif.<br/>
        	   Max weight: 2 MB.<br/>
        	   Best size: 1024 x 768 pxls.</p><?php if(!$model->isNewRecord){
               if(strlen($model->ruta_foto)>0){
               echo "<img src='/ilto3/images/users/tn1_$model->ruta_foto' style='width:100px;'>";
               echo "<input type='hidden' name='ruta_imagen_actual' value='$model->ruta_foto'>";
               ?>
               <div class="form-group">
                   <div class="checkbox">
                       <label><input name="delete_imagen"  value="1" type="checkbox"><span class="text">Delete Picture Saved</span></label>
                    </div>
                </div>
                <?php
                //echo "<input type='checkbox' name='delete_imagen' value='1'>&nbsp;Delete Image on Save";
                }
                } ?>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Save Profile</button>
    </form>
    </div>
    <?php //$this->endWidget(); ?>
    <script>    $(".tool_tip").mouseover(function(){
           eleOffset = $(this).offset();
          $(this).next().fadeIn("fast").css({
                                  left: eleOffset.left + $(this).outerWidth(),
                                  top: eleOffset.top
                          });
          }).mouseout(function(){
                  $(this).next().hide();
          });
          
     function checkPass()
{
    //Store the password field objects into variables ...
    var pass1 = document.getElementById('UsersC_clave').value;
    var pass2 = document.getElementById('UsersC_clave2').value;
    
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field
    //and the confirmation field
    if(pass1 == pass2){
      //The passwords match.
      //Set the color to the good color and inform
      //the user that they have entered the correct password
      //pass2.style.backgroundColor = goodColor;
      message.style.color = goodColor;
      message.innerHTML = "Passwords Match!";
      
      $('input[type="submit"]').prop('disabled', false);
    }else{
      //The passwords do not match.
      //Set the color to the bad color and
      //notify the user.
      //pass2.style.backgroundColor = badColor;
      message.style.color = badColor;
      message.innerHTML = "Passwords Do Not Match!";
      
      $('input[type="submit"]').prop('disabled', true);
    }
}      

$(function(){
	$('#nit_id_legal').keypress(function(e){
		if(e.which == 101){
	        return false;
		}
	});
});
$(function(){
	$('#telefono').keypress(function(e){
		if(e.which == 101){
	        return false;
		}
	});
});

        </script>
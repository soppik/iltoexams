<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */
if($taker==1){
    $bg='images/bg_login_client.jpg';
} else {
    $bg='images/bg_login_client2.jpg';
}

?>
<style>
    body{
    background: url('<?php echo $bg;?>')  no-repeat fixed !important;
-webkit-background-size: cover !important;
  -moz-background-size: cover !important;
  -o-background-size: cover !important;
  background-size: cover !important;
    } 
    .flash-error {
            border-color: #df5138;
            background: #e46f61;
            margin-bottom: 20px;
            margin-top: 0;
            color: #fff;
            border-width: 0;
            border-left-width: 5px;
            padding: 10px;
            border-radius: 0;
        }
</style>
<script>
    function divclose(divObj){
        $(divObj).fadeOut();
    }
</script>    

<div class="row-fluid">
    <div style="width:200px; margin:auto;padding:20px;">
        <br>
    </div>
	
    <div class="transparente"> 
<?php 
Yii::app()->session->destroy();
if(isset($_GET['nolicense'])){
    $model->addError('agent', "Pending Activation");
}

	$this->beginWidget('zii.widgets.CPortlet', array(
		'title'=>"Restricted Access", 
	));
?>
       

    <p>Enter the following information to access the test or administrator site. </p>    
     
    <div class="form" style="margin: auto;max-width: 216px;">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'login-form',
        'enableClientValidation'=>false,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ), 
    )); ?>
        <?php echo $form->errorSummary($model); ?>
       <div class="row">
            <?php echo $form->textField($model,'username',array('placeholder'=>'Username', 'id'=>'LoginForm_agent')); ?>
            <?php echo $form->error($model,'username'); ?>
        </div>
    
        <div class="row">
            <?php echo $form->passwordField($model,'password',array('placeholder'=>'Password')); ?>
            <?php echo $form->error($model,'password'); ?>
        </div>
        <div class="row rememberMe" style="width:250px;">
            <?php echo $form->checkBox($model,'rememberMe',array("style"=>"float:left;vertical-align:top")); ?>
            <?php echo $form->label($model,'rememberMe',array("style"=>"float:none;width:250px;vertical-align:middle;line-height:30px;")); ?>
            <?php echo $form->error($model,'rememberMe'); ?>
        </div>
    
        <div class="row buttons">
            <?php echo CHtml::submitButton('Login',array('class'=>'btn btn btn-primary iltoLoginbtn')); ?>
        </div>
        <a href="index.php?r=Clients/default/rememberPass">Forgot my Password</a>
        
    <?php $this->endWidget(); ?>
    </div><!-- form -->
    
   

<?php $this->endWidget();?>

    </div>

</div>
<script>
$('#LoginForm_agent').on('blur',function(){
    if($('#LoginForm_agent').val()=='Deicy'){
        $('#login-form').attr('action','/ilto3/index.php?r=site/login');
    } else {
        $('#login-form').attr('action','/ilto3/index.php?r=Clients/default/changePass');
    }
});
</script>
<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

?>
<style>
    body{
    background: url('images/bg_login_client.jpg')  no-repeat fixed !important;
-webkit-background-size: cover !important;
  -moz-background-size: cover !important;
  -o-background-size: cover !important;
  background-size: cover !important;
    } 
</style>

<div class="row-fluid">
    <div style="width:200px; margin:auto;padding:20px;">
        <br>
    </div>
	
    <div class="transparente"> 
<?php 
if(isset($_GET['nolicense'])){
    $model->addError('agent', "Pending Activation");
}
	$this->beginWidget('zii.widgets.CPortlet', array(
		'title'=>"RESET YOUR PASSWORD", 
	));
	
?>

    <p>Please fill the following form to Reset your Password. </p>    
    
    <div class="form" style="margin: auto;max-width: 216px;">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'remember-form',
        'enableClientValidation'=>false,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ), 
    )); ?>
        
        <div class="row">
            <?php echo $form->textField($model,'username',array('placeholder'=>'Username')); ?>
            <?php echo $form->error($model,'username'); ?>
        </div>
        <div class="row">
            <?php echo $form->textField($model,'email',array('placeholder'=>'Email Address')); ?>
            <?php echo $form->error($model,'email'); ?>
        </div>
    
    
        <div class="row buttons">
            <?php echo CHtml::submitButton('Submit',array('class'=>'btn btn btn-primary iltoLoginbtn')); ?>
        </div>
        
    <?php $this->endWidget(); ?>
    </div><!-- form -->

<?php $this->endWidget();?>

    </div>

</div>
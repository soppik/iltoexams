<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

?>
<style>
    body{
    background: url('images/bg_login_client.jpg')  no-repeat fixed !important;
-webkit-background-size: cover !important;
  -moz-background-size: cover !important;
  -o-background-size: cover !important;
  background-size: cover !important;
    } 
    .flash-error {
            border-color: #df5138;
            background: #e46f61;
            margin-bottom: 20px;
            margin-top: 0;
            color: #fff;
            border-width: 0;
            border-left-width: 5px;
            padding: 10px;
            border-radius: 0;
        }
        
        .page-body {
            background: #eee;
            /*background:url('images/security.jpg')  no-repeat !important;
            background-size:100%;*/
            padding: 18px 20px 24px;
        }
        
        #change_pws{
            position:relative;
            right:100px;
            top:50px;
        }
    
</style>

<div class="row-fluid" id="change_pws">
    <div style="margin:auto;padding:20px;">
        <?php
            foreach(Yii::app()->user->getFlashes() as $key => $message) {
                echo '<div onClick="divclose(this);" id="flash-'.$key.'" class="flash-' . $key . '">' . $message . "&nbsp;&nbsp;&nbsp;Click Here to dismiss</div>\n";
            }
        ?>
    </div>
	<div class="loginbox-social" style="text-align:center;">
                    <div class="social-buttons">
                        <img src="/ilto3/images/logo_ILTO.png" alt="ILTO EXAMS">
                    </div>
                </div>
    <div class="transparente"> 
<?php 
if(isset($_GET['nolicense'])){
    $model->addError('agent', "Pending Activation");
}
	$this->beginWidget('zii.widgets.CPortlet', array(
	));
	
?>
 <h2 style="text-align:center;">RESET YOUR PASSWORD</h2>   
    <p style="text-align:center;">Your safety is important to ILTO. Please change your password.</p>    
    <div class="form" style="margin: auto;max-width: 216px;">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'remember-form',
        'enableClientValidation'=>false,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ), 
    )); ?>
    
        <div class="span6">
			<?php echo $form->error($model,'nombre'); ?>
		</div>
        <div class="row">
            <?php echo $form->textField($model,'username',array('placeholder'=>'Username','class'=>'form-control','style'=>'margin-bottom:10px;', 'required'=>'required', 'oninvalid'=>'setCustomValidity("This field is required")')); ?>
            <?php echo $form->error($model,'username'); ?>
        </div>
        <div class="row">
            <?php echo $form->passwordField($model,'oldPass',array('placeholder'=>'Old Password','class'=>'form-control','style'=>'margin-bottom:10px;')); ?>
            <?php echo $form->error($model,'oldPass'); ?>
        </div>
        <div class="row">
            <?php echo $form->passwordField($model,'newPass',array('placeholder'=>'New Password','class'=>'form-control', 'id'=>'pass', 'style'=>'margin-bottom:10px;')); ?>
            <?php echo $form->error($model,'newPass'); ?>
        </div>
        <div class="row">
            <?php echo $form->passwordField($model,'newPass',array('placeholder'=>'Retype New Password','class'=>'form-control', 'id'=>'re_pass', 'style'=>'margin-bottom:10px;')); ?>
            <?php echo $form->error($model,'renewPass'); ?>
        </div>
    
        <div class="row">
            <label id="pass_alert"></label>
        </div>
    
        <div class="row buttons">
            <?php echo CHtml::submitButton('Submit',array('class'=>'btn btn btn-primary iltoLoginbtn', 'disabled'=>'disabled', 'id'=>'send')); ?>
        </div>
        
    <?php $this->endWidget(); ?>
    </div><!-- form -->

<?php $this->endWidget();?>

    </div>

</div>
<script>
 $( "#re_pass" ).keyup(function() {
    //Store the password field objects into variables ...
    var pass1 = document.getElementById('pass').value;
    var pass2 = document.getElementById('re_pass').value;
    
    //Store the Confimation Message Object ...
    var message = document.getElementById('pass_alert');
    document.getElementById('pass_alert').style.display="block";
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field
    //and the confirmation field
    if(pass1 == pass2){
      //The passwords match.
      //Set the color to the good color and inform
      //the user that they have entered the correct password
      //pass2.style.backgroundColor = goodColor;
      message.style.color = goodColor;
      message.innerHTML = "Passwords Match!";
      
      $('#send').prop('disabled', false);
    }else{
      //The passwords do not match.
      //Set the color to the bad color and
      //notify the user.
      //pass2.style.backgroundColor = badColor;
      message.style.color = badColor;
      message.innerHTML = "Passwords Do Not Match!";
      
      $('#send').prop('disabled', true);
    }
});
function divclose(divObj){
        $(divObj).fadeOut();
    }
</script>
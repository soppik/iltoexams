<?php

$modUtil = new Utilidades();
if(Yii::app()->user->getState('id_perfil')==6){ //TAKER
    $this->layout = "//layouts/columnx";
    $cs = Yii::app()->getClientScript();
    $cs->registerCssFile(Yii::app()->theme->baseUrl."/css/taker.css",  CClientScript::POS_END);
}else {
     $this->layout = "//layouts/column2";
}
if(isset($_GET['ede'])){
    echo "<style>.ede_exam{margin-top:2%;}</style>";
}
//
//user info
$sql='SELECT * FROM `users_c` WHERE `id_usuario_c` = "'.Yii::app()->user->getState('usuario').'"';
$usr= Yii::app()->db->createCommand($sql)->queryAll();

$usr_name = $usr[0]["nombres"]." ".$usr[0]["apellidos"];

//normalize demo licenses for prevent spam
$modUtil->normalizeDemo();

  //obtenemos ID de licencia solicitada y consultamos exámenes
$id_license = base64_decode($_GET["license"]);

//cambiar por ID FUAA
if(Yii::app()->user->getState('cliente')=="2030405060"){
    echo "
    <style>
        body {
            background-image: url(/ilto3/themes/ilto/img/ede-background.jpg) !important;
        }
    </style>";
}

echo '
<style>




input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    /* display: none; <- Crashes Chrome on hover */
    -webkit-appearance: none;
    margin: 0; /
}

input[type=number] {
    -moz-appearance:textfield; /* Firefox */
}

/* Popup box BEGIN */
.modal-header, .modal-header h4, .close {
    background-color: #5cb85c;
    color:white !important;
    text-align: center;
    font-size: 24px;
  }
  .modal-footer {
    background-color: #f9f9f9;
  }

</style>

<link rel="stylesheet" href="/ilto3/themes/ilto/html/assets/css/simple-popup/reset.css"> <!-- CSS reset -->
        <link rel="stylesheet" href="/ilto3/themes/ilto/html/assets/css/simple-popup/style.css"> <!-- Resource style -->
        <script src="/ilto3/themes/ilto/html/assets/js/simple-popup/modernizr.js"></script> <!-- Modernizr -->
        <script src="/ilto3/themes/ilto/html/assets/js/simple-popup/main.js"></script> <!-- Modernizr -->


'; 


/* session manager
start
*/
$session=new CHttpSession;
$session->open();

if($session['user'] == null || $session=''){
    
    $this->redirect('index.php?r=Clients/default/login&type=1');
}/* session 
manager end */

   $userId = Yii::app()->user->getState('usuario');
    $edeId = $modUtil->getIdEde();
    $tecsId = $modUtil->getIdTecs();
    $speakingId = $modUtil->getIdSpeaking();
    
    //Check if taker is in Fraud Report
    $sql='SELECT * FROM `fraud_report` WHERE `document` = "'.$userId.'"';
    $fraud= Yii::app()->db->createCommand($sql)->queryAll();
    
    //fraud_result result query
        $fraud_result = array($fraud[0]['document']);
    if(!is_null($fraud_result[0])){
        Yii::app()->user->setFlash('error', 'The taker has been blocked, due to suspicious fraud actions. Contact info@iltoexams.com for more information.');
        $this->redirect('index.php?r=Clients/default/login&message=6');
    }
      
    
    //Consultamos si el taker requiere foto
    $SQL="SELECT ruta_foto FROM `users_c` WHERE `id_usuario_c` = '".$userId."'";
    $requirePhoto= Yii::app()->db->createCommand($SQL)->queryAll();
    
    $photoFlag = 0;
    if($requirePhoto[0]["ruta_foto"] !=""){
        $photoFlag = 1;
    }else{
        $photoFlag = 0;
    }
    
    
 
    //consultamos Y VALIDAMOS LA LICENCIA QUE VIENE DEL LOGIN Y ESTÁ PENDIENTE
    $SQL="SELECT id_licencias_usuario, id_licencia_cliente, id_usuario, licenses_user.fecha_asignacion, licenses_user.estado, licenses_user.fecha_presentacion, hora, calificacion, nivel, terms, licenses_client.id_licencia FROM `licenses_user` INNER JOIN licenses_client ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente WHERE `id_usuario` = '".$userId."' AND `licenses_user`.estado LIKE 'A' AND `id_licencias_usuario` = '".$id_license."'";
    $modLicUs= Yii::app()->db->createCommand($SQL)->queryAll();

if(Yii::app()->user->getState('id_perfil')==6){
   
    
    echo '<div class="container">

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Términos y Condiciones</h4>
        </div>
        <div class="modal-body" style="font-size:14px">';
        if( $modLicUs[0]["id_licencia"] == 5){
            echo "<span class='btn btn-warning btn-block'>This demo shows the look and feel of the test. <br>It does not issue real results of your English Communication Skills.</span></br></br>";
        }
          echo '<p>A continuación, los principales términos y condiciones relacionados con el <b>TECS</b> (Test of English Communication Skills). Para leer el documento completo, por favor haga clic <a href="https://www.iltoexams.com/?page_id=132">aquí</a>.<br><br>
            1. Fotos y vídeo del usuario: El software de seguridad del Examen <b>TECS</b>, tomara fotos y hara una grabacion desde la camara web del computador y celular del usuario, durante la presentación del examen para comprobar la identidad de quien toma el examen y confirmar que no se presentan anomalias que indiquen un posuble fraude durante la prueba.<br><br>  
            2. Inconsistencia en los resultados: Estudios académicos y científicos coinciden en que existe una cercana relación entre las competencias de Conversación y Escucha de una persona. De acuerdo al más reciente análisis psicométrico del examen <b>TECS</b>, es posible asegurar una consistencia del 100% entre las 4 habilidades evaluadas. Según lo anterior, si la diferencia entre las diferentes habilidades es mayor al 40%, <b>ILTO</b> se reserva el derecho de anular automáticamente dicho certificado, sin posibilidad de reclamación.<br><br>
            3. Anulación de certificados: ILTO se reserva el derecho de anular el certificado, sin posibilidad de reclamación si se encuentra que el usuario ha cometido alguna clase de fraude establecido en la sección “Annulment of TECS certificates at ILTO”.<br><br>
            4. <b>ILTO</b> se reserva el derecho de vetar permanentemente de la presentación de cualquiera de sus productos al usuario a quien se ha confirmado algún tipo de fraude.<br><br>
            5. Disputa de resultados y acciones legales: La institución administradora no es responsable de los resultados del examen. Para disputa de resultados puede escribir a info@iltoexams.com Para interponer una acción legal, puede hacerlo en la corte del estado de la Florida, USA. Domicilio principal de <b>ILTO</b> (International Language Testing Organization)</p><br><br>
            <button type="button" class="btn btn-success btn-block" data-dismiss="modal">Acepto</button>
        </div>
      </div>
      
    </div>
  </div>
</div>
 
<script>
$(document).ready(function(){
  // Show the Modal on load
  $("#myModal").modal("show");
    
  // Hide the Modal
  $("#myBtn").click(function(){
    $("#myModal").modal("hide");
  });
});
</script>
';
    
 
    
    
    $terms ="<div class='content_scroll' style='position:relative;width:100%;height:200px;padding:10px;overflow: scroll;overflow-x: hidden;background-color: #232121;'>
                    <p style='font-size:14px;text-align:justify;line-height: 15px;'><strong>Terms and Conditions of use of ILTO products</strong><br><br>
                            ILTO - The International Language Testing Organization is a United States Limited
                            Liability Company established in the state of Florida, which follows US regulations and is
                            protected by US agencies to assure its users the use of reliable English Language
                            Communications scores for the individual purpose, under similar opportunity conditions
                            of test performance to guarantee the score validity.
                            Following the terms and conditions applied to all the ILTO products. By clicking on the
                            accept terms and conditions box, you agreed with all the policies and accept further
                            actions involving the protection the consistency and reliability of all ILTO products.
                            <br><br>
                            <strong>Confidentiality</strong><br><br>
                            ILTO understands that we are dealing with personal information of individuals who
                            administer, rate and take the tests, and thus acknowledges their right to information
                            privacy. ILTO is concerned with protecting the information stored in our servers and thus
                            we only disclose information under the express authorization of the individual,
                            nevertheless ILTO reserves the right to reveal information under the following cases: <br><br>
							•	ILTO may reveal score data for verification purposes with organizations where an
                            individual has submitted a certificate. This data will be available under the
                            Certificates Validation section in the website, under the assumption that the
                            individual acknowledges that the organizations have the right to validate the scores
                            in the certificate.  <br><br>
							•	TECS scores will be used for research, statistical and psychometric purposes
                            where permanent analysis is being performed by ILTO Academic Department,
                            without taking into account the test takers’ personal information.  <br><br>
							•	The TECS certificates and information will be available in the test administrator’s
                            dashboard where the individual took the test, under the assumption that the test
                            taker acknowledges that it is mandatory to provide personal information to fill in
                            the administrator forms and to display in the TECS certificate to identify the test
                            information<br><br>
							•	The personal and organizational information of the test taker and the test
                            administrator will be disclosed if local government authorities require it as part of
                            an investigation concerning violations of the law or behaviors that put in danger
                            the test security or reliability. 
							<br><br>
                            <strong>ILTO use of the individuals’ personal data</strong><br><br>
                            By registering to ILTO as administrator or test taker and providing data such as
                            identification, your picture or company logo, you expressly allow ILTO to utilize such
                            information for information purposes to ILTO third parties interested in knowing more
                            about the organizations using our products or performing demographic studies regarding
                            our test takers, test administrators and raters. You also allow ILTO to use your contact
                            information to send you relevant materials concerning your test performance and
                            strategies to improve or maintain your English language level. These emails may include
                            links from third parties involving possible offers on courses or training material.  <br><br>
                            <strong>Information storage and certificates issue </strong><br><br>
                            ILTO undertakes several strategies to protect the information of the organizations, raters
                            and test takers registered in our dashboard. Servers are secure and policies are
                            implemented to avoid unauthorized information disclosure, unless the country
                            government or private agencies require this information for criminal or fraud attempts
                            investigations. ILTO reserves the right to keep the test takers information and scores for
                            a maximum of three years, time after which ILTO is no longer responsible for test takers
                            information or their scores. 
                            <br><br> 
                            <strong>Face picture and Video Recording:</strong><br><br>
                            In order to confirm the identity of the test taker and to control and confirm that no fraud anomalies are present during the test. ILTO will take pictures of the person and use the computer webcam and cellphone, to record the test taker face and environment. <br><br> 
                            <strong>Frequency of TECS administration to the same test taker and certificate expiration</strong><br><br>
                            There is extensive academic research and academic evidence that repeating a
                            standardized test such as the TECS – Test of English Communication Skills, would not
                            change the results or increase the score if there is not previous study of the subject being
                            assessed. <br><br>
                            Based on the above ILTO reserves the right to block the test taker for 180 days if after
                            taking the test in two successive events, the result is undistinguishable. For instance if
                            the first and second time the result is A2, the system will automatically block the student
                            for 180 days. If the first result is A2, but the second result is B1, the test taker will not be
                            blocked as the system detects the advance of the English proficiency level of the test
                            taker. <br><br>
                            The test results are valid for two years from the date stated in the TECS certificate.<br><br>
                            <strong>Questions about the items and scores disputes</strong><br><br>
                            The academic team at ILTO is made of a group of professionals who design and write
                            test items following the item creation cycle consisting in three steps: creation, validation,
                            publication. ILTO follows standardized processes that assure the reliability of the items in
                            order to keep to consistency of the scores in all the test takers. <br><br>
                            The TECS affordably assesses the English proficiency level of test takers in a timely
                            manner through a comprehensive items bank, based on updated topics used by people
                            in real communication contexts. These topics have to do with so today´s cultural reality
                            such as music, sports, social networks, world news, technology and other general interest
                            topics that assess real communication among individuals in a day-to-day basis. The
                            TECS measures the test takers performance in their production and perception areas,
                            through four different sections. Language production assessed through speaking and
                            language use sections and Language perception evaluated through reading
                            comprehension of different text samples, including iconographic interpretation as well as
                            listening or real communication samples such as government debates, radio shows, TV
                            news among others. TECS results are standardized according to the Common European
                            Framework for Language Reference – CEFR.<br><br>
                            The academic team, permanently oversees the quality of the test items, nonetheless,
                            ILTO understands that there may be questions, concerns or comments regarding test
                            items or test scores; for information or request on this matter, the individual should send
                            write to info@iltoexams.com with the corresponding information.<br><br>
                            With the aim of making the TECS a more accessible test, where results can be issued in
                            a timely manner, ILTO has opened the administrators the possibility or rating the speaking
                            skills of the test takers. The scores for the speaking skills are validated by a set of rubrics
                            that are followed by raters and which are verified to make scores reliable to test takers
                            and organizations. Should the individual require further information or have a special
                            request on this matter, the individual should send write to info@iltoexams.com with the
                            corresponding information.<br><br>
                            
                            <strong>Annulment of TECS certificates at ILTO  </strong><br><br>
                            The effective use of the TECS certificates relies on reporting the English Communication
                            skills of an individual after taking a test, under a secure environment that reveals the
                            transparency and individual performance of each test taker. As a result ILTO makes
                            special emphasis on assuring that all the individuals taking the test, undergo the same
                            process to perform tasks that serve as an evidence of their communication skills in the
                            second language. Thus overseeing that no indivual has supplementary benefits that do
                            not reflect the knowledge of the language, but demonstrate the use of certain doubtful
                            activity in the process, is of utmost importance. <br><br>
                            As completely discretionary of ILTO and without prejudice or permission of the test taker
                            or test administrator, ILTO may open an investigation to the test taker and thus the TECS
                            certificate will be held and in revocation status until the investigation has determined the
                            identity and legitimacy of the test taker and the test scores. <br><br>
                            In compliance with the above, ILTO reserves the right to temporarily revoke or even
                            perform the annulment of TECS certificates where there is evidence of behavioral or
                            irregular actions that disturb the impartial result of the test takers or the consistency of the
                            items. <br><br>
                            You expressly acknowledge that the result of this investigation may have an effect on the
                            individual’s criminal record in the country where the test is taken. In addition ILTO and the
                            TECS are protected by the United States of the America anti-identity theft and antifraud
                            regulations, which may derive in a US based investigation; as well as the prohibition of
                            the possibility taking the test in any of the test administrator organizations around the
                            world. ILTO also reserves the right to share the results of the investigation to third parties
                            to prevent the individual’s fraudulent actions in the future.<br><br>
                            Following the causes (not limited), that would result in the commencement of an
                            investigation case and feasibly of the annulment of the TECS certificate:<br><br>


							•	The use of techniques that suggest that the test taker has performed actions that
                            allowed the gaining of advantage to answer the test items, such as the
                            memorization of the test items, test topics or any kind of trickery to answer the
                            test without the corresponding knowledge of the language. . <br><br>
							•	The assistance of an individual that, by using technological tools, answers the
                            test instead of the registered test taker, becoming a variation of identity theft.  <br><br>
							•	Disruption of the test administration that may result in the dispute of test scores,
                            caused by external factor such as technological issues, computer configuration,
                            extreme weather or other that affect the normal development of the test.  <br><br> 
							•	If there are issues with the test administrator premises or procedures that are
                            evidence of dishonest actions or fraudulent actions. In cases where there is
                            evidence of fraudulent or doubly behavior by the test administrator and it has
                            been determined is involved in criminal and fraudulent actions; ILTO at its sole
                            discretion, may terminate the contract with the test administrator. Annulment of
                            certificates may take place, even if the there is no clear evidence of the test taker
                            doubtful or dishonest behavior.  
							•	According to the United States of America, Federal Bureau of Investigation (FBI),
                            Identity Theft occurs when someone assumes the identity of another person, to
                            perform a fraud or other criminal act. ILTO considers this a major fraudulent
                            behavior, which will result in the immediate annulation of the certificate, the
                            insertion in the fraud alert report of the test taker, lifetime ban to take the test in
                            any test administrator location around the world and notify the local organization
                            and government authorities, which will perform the corresponding investigation.  <br><br>

							If any of the above fraud behaviors or any other is detected during of after a test, ILTO reserves the right to immediately dismiss the test taker from the test administrator’s premises 
							and open the corresponding fraud case. In the case of TECS2GO the test will be revoked, the test taker scores will be cancelled and the test fee will not be reimbursed. You herein acknowledge 
							that if the tests taker performs threatening or aggressive behavior, the test administrator personnel reserves the right to request support of the organization Security Company or local 
							government authorities.
							<br><br>
							<strong>ILTO Liability policy</strong><br><br>
                            You herein acknowledge that ILTO is not liable for any test taker moral or economic
                            damage, harm or loss for the annulation of the certificate, banning of the test for future
                            test dates or any action resulting from the notification of the test taker to local
                            organization or government authorities.  <br><br>  
                            <strong>Terms and conditions amendments and modifications </strong><br><br>
                            ILTO reserves the right to make modifications, to the current document, according to the
                            laws, technological advances or other reasons that the ILTO considers important for the
                            security and reliability of its products and its test administrators and other stakeholders.  <br><br>  
                            <strong>Legal jurisdiction and disputes </strong><br><br>
                            You herein agree that legal actions deriving from this document will be ruled under the
                            regulation of the United States of America and the State of Florida, without concerns of
                            the principles of conflict of laws. Any legal action related to these terms, condition must
                            be resolved exclusively in the state or federal courts located in the State of Florida, and
                            you agree to the jurisdiction of those courts. <br><br>  
                    </p>
                </div>";
    
    function license_description($id){
    //user info
    $sql='SELECT * FROM `users_c` WHERE `id_usuario_c` = "'.Yii::app()->user->getState('usuario').'"';
    $usr= Yii::app()->db->createCommand($sql)->queryAll();
    $modUtil = new Utilidades();
    $edeId = $modUtil->getIdEde();
    $tecsId = $modUtil->getIdTecs();
    $speakingId = $modUtil->getIdSpeaking();
    $usr_name = $usr[0]["nombres"]." ".$usr[0]["apellidos"];
    
    switch ($id) {
        case 25:
            $description = "<div class='col-sm-12' id='description_exam'>
                <h4 style='text-shadow: 1px 1px #191919;'>Dear ".$usr_name.",</h4>
                <p>The Test of English Communication Skills – <span style='color:#ffc000;font-weight:bold;'>TECS</span> – is a Standardized exam that assesses your English proficiency level through a comprehensive items bank, based on updated topics used by individuals in real communication contexts. 
                </p>
                <ul>
                    <li>Speaking test administered by trained raters. </li>
                    <li>Listening in context.</li>
                    <li>Reading in context. </li>
                    <li>Language Use.</li>
                </ul>
                <p style='width:100%;text-align:center;'><strong>Please allow pop up windows in your browser. Go to options and select allow pop up windows. If not you won't be able to do the test.</strong>
                </p> 
            </div>";
            break;
        case 22:
            $description = "<div class='col-sm-12' id='description_exam'>
                <h4 style='text-shadow: 1px 1px #191919;'>Dear ".$usr_name.",</h4>
                <p>The Test of English Communication Skills – <span style='color:#ffc000;font-weight:bold;'>TECS</span> – is a Standardized exam that assesses your English proficiency level through a comprehensive items bank, based on updated topics used by individuals in real communication contexts. 
                </p>
                <ul>
                    <li>Speaking test administered by trained raters. </li>
                    <li>Listening in context.</li>
                    <li>Reading in context. </li>
                    <li>Language Use.</li>
                </ul>
                <p style='width:100%;text-align:center;'><strong>Please allow pop up windows in your browser. Go to options and select allow pop up windows. If not you won't be able to do the test.</strong>
                </p> 
            </div>";
            break;
        case 18:
            $description = "<div class='col-sm-12' id='description_exam'>
                <h4 style='text-shadow: 1px 1px #191919;'>Dear ".$usr_name.",</h4>
                <p>The Test of English Communication Skills – <span style='color:#ffc000;font-weight:bold;'>TECS</span> – is a Standardized exam that assesses your English proficiency level through a comprehensive items bank, based on updated topics used by individuals in real communication contexts. 
                </p>
                <ul>
                    <li>Listening in context.</li>
                    <li>Reading in context. </li>
                    <li>Language Use.</li>
                </ul>
                <p style='width:100%;text-align:center;'><strong>Please allow pop up windows in your browser. Go to options and select allow pop up windows. If not you won't be able to do the test.</strong>
                </p> 
            </div>";
            break;    
        case $edeId:
            $description = "<div class='col-sm-12' id='ede_desc'>
                <h4 style='text-shadow: 1px 1px #191919;'>Dear ".$usr_name.",</h4>
                <p>
                    The <span style='color:#00a3ff;font-weight:bold;'>EDE</span> (English Diagnosis Exam), as stated by its name, is an exam that allows you to have a better understanding of your English knowledge in the three main communication skills. This test aims to assess the basic proficiency in three main areas as a way of knowing your strengths or weaknesses, for a more comprehensive analysis of your communication skills, we suggest taking the TECS (Test of English Communication Skills) which will grant you an official certificate. 
                </p>
                <p><strong>Please allow pop up windows in your browser. Go to options and select allow pop up windows. If not you won't be able to do the test.</strong>
                </p>    
            </div>"; 
            break;
        case $tecsId:
            $description = "<div class='col-sm-12' id='description_exam'>
                <h4 style='text-shadow: 1px 1px #191919;'>Dear ".$usr_name.",</h4>
                <p>The Test of English Communication Skills – <span style='color:#ffc000;font-weight:bold;'>TECS</span> – is a Standardized exam that assesses your English proficiency level through a comprehensive items bank, based on updated topics used by individuals in real communication contexts. 
                </p>
                <ul>
                    <li>Speaking test administered by trained raters. </li>
                    <li>Listening in context.</li>
                    <li>Reading in context. </li>
                    <li>Language Use.</li>
                </ul>
                <p style='width:100%;text-align:center;'><strong>Please allow pop up windows in your browser. Go to options and select allow pop up windows. If not you won't be able to do the test.</strong>
                </p> 
            </div>";
            break;
            case 19:
            $description = "<div class='col-sm-12' id='description_exam'>
                <h4 style='text-shadow: 1px 1px #191919;'>Dear ".$usr_name.",</h4>
                <p>The Test of English Communication Skills – <span style='color:#ffc000;font-weight:bold;'>TECS</span> – is a Standardized exam that assesses your English proficiency level through a comprehensive items bank, based on updated topics used by individuals in real communication contexts. 
                </p>
                <ul>
                    <li>Speaking test administered by trained raters. </li>
                    <li>Listening in context.</li>
                    <li>Reading in context. </li>
                    <li>Language Use.</li>
                </ul>
                <p style='width:100%;text-align:center;'><strong>Please allow pop up windows in your browser. Go to options and select allow pop up windows. If not you won't be able to do the test.</strong>
                </p> 
            </div>";
            break;
            case 5:
            $description = "<div class='col-sm-12' id='description_exam'>
                    <h4 style='text-shadow: 1px 1px #191919;'><b>TECS DEMO</b></h4>
                            <p style='text-align:justify;'>The Test of English Communication Skills – <span style='color:#ffc000;font-weight:bold;'>TECS</span> – is a Standardized exam that assesses your English proficiency level through a comprehensive items bank, based on updated topics used by individuals in real communication contexts.
                    The <span style='color:#ffc000;font-weight:bold;'>TECS</span> is made of four different sections that refer to each of the main communication skills as follows:
                            </p>
                            <ul>
                                
                                <li>Listening in context.</li>
                                <li>Reading in context. </li>
                                <li>Language Use.</li>
                            </ul><br>
                            <h4>Rated interview</h4>
                            <p style='text-align:justify;'>A trained and certified <span style='color:#ffc000;font-weight:bold;'>TECS</span> Rater who will ask questions written in a TECS official online form, will rate each answer considering the comprehension and response of such question. </br></br>
                            The questions asked to the test taker are presented in progressive difficulty, from A1 to C1 levels according to the CEFR. The <span style='color:#ffc000;font-weight:bold;'>TECS</span> algorithm will stop showing questions according to the level reached by the taker. For instance, if questions pertaining to a B1 level are not being answered correctly or the comprehension and response performance is low, the system will stop showing questions of a higher level and ask the test taker to continue to the remaining sections. </br></br> 
                    Click on the following button to access a demo test of 10 questions per section. This will give you an idea of the <span style='color:#ffc000;font-weight:bold;'>TECS</span> Structure.
                            </p>
                            <p style='width:100%;text-align:center;'><strong>Please allow pop up windows in your browser. Go to options and select allow pop up windows. <br>If not, you will not be able to access the test.</strong>
                      </p> ";
            break;
        default:
            $description = "<div class='col-sm-12' id='description_exam'>
                <h4 style='text-shadow: 1px 1px #191919;'>Dear ".$usr_name.",</h4>
                <p>The Test of English Communication Skills – <span style='color:#ffc000;font-weight:bold;'>TECS</span> – is a Standardized exam that assesses your English proficiency level through a comprehensive items bank, based on updated topics used by individuals in real communication contexts.<br><br>  
                The test sections you are about to take, will measure your performance in four different communication skills:
                </p>
                <ul>
                    <li>Speaking test administered by trained raters. </li>
                    <li>Listening in context.</li>
                    <li>Reading in context. </li>
                    <li>Language Use.</li>
                </ul><br> 
                <p style='width:100%;text-align:center;'><strong>Please allow pop up windows in your browser. Go to options and select allow pop up windows. <br>If not you won't be able to do the test.</strong>
                </p> 
            </div>";
    }
    
        
    return $description;
}
    echo '<div class="spacer" style="height:10px;">
    </div>  
    <div class="row" style="padding:20px;max-width:900px;text-align:center;margin:auto;">
        <div class="col-sm-4">
            <img src="/ilto3/images/iltoexams-logo-100-white.png" alt="ILTO EXAMS">
        </div>
        <div class="col-sm-4">
            
        </div>
        <div class="col-sm-4" id="logo_license">
            <img src="/ilto3/images/tecs-short-logo.png" height="75"  alt="TECS ILTO">
        </div>
    </div>';
    $date_description = "";
    
    
        
        // armarmos el criterio de consulta de examenes de la licencia
        $clientLicense = $modLicUs[0]['id_licencia_cliente'];
        $criteria=new CDbCriteria;
        $criteria->addcondition('id_licencia_usuario = '.$id_license.' and estado != "F" ORDER BY id_examen_usuario DESC');
        $examenes = ExamsUser::model()->findAll($criteria);
        $now = new Datetime("now");
        $dateLic = new Datetime($modLicUs[0]["fecha_presentacion"]);
        $result = $dateLic ->format('Y-m-d H:i:s');
        
        
        
        //validamos que se encuentren examenes asignados a la licencia
        if(sizeof($examenes)>0){
            $examUserId = $examenes[0]->id_examen_usuario;
        } else {
            $examUserId=NULL;
        }
        
        if($examenes[0]->id_examen == 13 || $examenes[0]->id_examen == 35 || $examenes[0]->id_examen == $speakingId){
            if($examenes[0]->estado != 'F'){
                $this->redirect('index.php?r=Clients/default/login&message=4');
            }
        }
        
        $examenes = array_reverse($examenes);
        
        echo '<div class="row" style="padding:20px;max-width:900px;text-align:justify;margin:auto;">';
                if($now <= $dateLic){
                    
                    echo "<h4 style='text-shadow: 1px 1px #191919;font-size:26px;text-align:center;'>Dear ".$usr_name.",</h4>
                        <p style='font-size:26px;text-align:center;'>You can only access this test until</br> ".$result."
                        </p>";
                }else{
                    echo license_description($modLicUs[0]["id_licencia"]);
                    if($modLicUs[0]["id_licencia"] != 5){
                       echo $terms; 
                    }    
                }
                if($now <= $dateLic){
                    echo "<p  style='font-size:26px;text-align:center;'><a href='https://iltoexams.com/' class='a_back'>Back to Iltoexams.com</a></p>";
                }else{
                     switch ($modLicUs[0]["id_licencia"]) {
                        case 23:
                            echo '<div class="row" id="tecs_license" style="padding: 20px;">';
                                if($now <= $dateLic){
                                    echo $date_description.$result.'<br/>';
                                    echo "<br/><a href='https://iltoexams.com/' class='a_back'>Back to Iltoexams.com</a>";
                                }else{
                                    if($photoFlag == 0){
                                    ?>
                                        <form id="tecs_form" enctype= "multipart/form-data" action='index.php?r=Clients/UsersC/UpdatePhotoTecs' method="POST">    
                                    <?php
                                    }else{
                                    ?>
                                         <form id="tecs_form" enctype= "multipart/form-data" action='index.php?r=Clients/AnswersExamUser/takeExam&y=<?php echo base64_encode($userId);?>&p=<?php echo base64_encode($modLicUs[0]["id_licencias_usuario"]);?>&b=<?php echo base64_encode($examUserId);?>' method="POST">
                                    <?php
                                    }
                                    ?>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-6">
                                            <input type="number" name="phone" id="contact-number" placeholder="Enter contact number.">
                                        </div>
                                        <div class="col-sm-6 step_warning" >
                                           <span class="btn btn-warning btn-xs edit" id="require_1" style="font-size:14px;color: #fff;cursor: auto;font-weight: 700;">1- The phone number must include at least 7 digits. Do not use symbols o letters. (Required*).</span>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-6">
                                            
                                            <input type="checkbox" name="agree_terms" id="agree_terms_tecs" disabled><b>&nbsp;&nbsp;I agree to the terms and conditions.</b>
                                        </div>
                                        <div class="col-sm-6 step_warning">
                                           <span class="btn btn-warning btn-xs edit" id="require_2" style="font-size:14px;color: #fff;cursor: auto;font-weight: 700;display:none;">2- Please agree to the terms to start the test.</span>
                                        </div>
                                    </div>  
                                    <div class="row imagenTaker" style="margin-bottom: 10px;">
                                        <?php 
                                        if($photoFlag == 0){
                                        ?>
                                            <div class="col-sm-6">
                                                <input id="id_user" type="hidden" value="<?php echo $userId;?>" name="ExamsUser[id_user]">
                                            	<input id="license" type="hidden" value="<?php echo $modLicUs[0]["id_licencias_usuario"];?>" name="ExamsUser[license]">
                                            	<input id="examUserId" type="hidden" value="<?php echo $examUserId;?>" name="ExamsUser[examUserId]">
                                                <input id="ytExamsUser_ruta_foto" type="hidden" value="" name="ExamsUser[ruta_foto]">
                                				<!--<input type="file" class="inputfile" style="font-size:14px" multiple="" name="UsersC[ruta_foto]" data-multiple-caption="{count} files selected" id="UsersC_ruta_foto">-->
                                				<input disabled="disabled" type="file" name="UsersC[ruta_foto]" id="file" class="inputfile" data-multiple-caption="{count} files selected" multiple="" />
                                				<label for="file" id="photo_name" style="background-color:#aba7a7;"><strong>3- Upload the test taker picture</strong></label>
                                        <?php    
                                        }else{
                                        ?>
                                    	    <div class="col-sm-6">    
                                        <?php  
                                        }
                                        ?>
                                    	</div>
                                            <div class="col-sm-6">
                                                 <?php if($license == 18) {  echo "<img class='imgUser' style='width:65px !important;' src='/ilto3/images/clients/tn2_universidad del valle.gif'>";}?>
                                                 <?php if(!is_null($examUserId)){?> <input disabled="disabled" class="btnExam" id="btnExam_tecs" type="submit" value="start test" style="border:none;height: 80px !important;"><?php ;}?> 
                                            </div>
                                        </form>    
                                    </div>
                                </div>
                                </div>
                            <?php }  
                        break;
                        case 22:
                            echo '<div class="row" id="tecs_license" style="padding: 20px;">';
                                if($now <= $dateLic){
                                    echo $date_description.$result.'<br/>';
                                    echo "<br/><a href='https://iltoexams.com/' class='a_back'>Back to Iltoexams.com</a>";
                                }else{
                                    ?>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-6">
                                            <input type="number" name="phone" id="contact-number" placeholder="Enter contact number.">
                                        </div>
                                        <div class="col-sm-6 step_warning" >
                                           <span class="btn btn-warning btn-xs edit" id="require_1" style="font-size:14px;color: #fff;cursor: auto;font-weight: 700;">1- The phone number must include at least 7 digits. Do not use symbols o letters. (Required*).</span>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-6">
                                            <input type="checkbox" name="agree_terms" id="agree_terms_tecs" disabled><b>&nbsp;&nbsp;I agree to the terms and conditions.</b>
                                        </div>
                                        <div class="col-sm-6 step_warning">
                                           <span class="btn btn-warning btn-xs edit" id="require_2" style="font-size:14px;color: #fff;cursor: auto;font-weight: 700;display:none;">2- Please agree to the terms to start the test.</span>
                                        </div>
                                    </div>  
                                    <div class="row imagenTaker" style="margin-bottom: 10px;">
                                        <?php 
                                        if($photoFlag == 0){
                                        ?>
                                            <form id="tecs_form" enctype= "multipart/form-data" action='index.php?r=Clients/UsersC/UpdatePhotoTecs' method="POST">
                                                <div class="col-sm-6">
                                                
                                                    <input id="id_user" type="hidden" value="<?php echo $userId;?>" name="ExamsUser[id_user]">
                                            		<input id="license" type="hidden" value="<?php echo $modLicUs[0]["id_licencias_usuario"];?>" name="ExamsUser[license]">
                                            		<input id="examUserId" type="hidden" value="<?php echo $examUserId;?>" name="ExamsUser[examUserId]">
                                                    <input id="ytExamsUser_ruta_foto" type="hidden" value="" name="ExamsUser[ruta_foto]">
                                					<!--<input type="file" class="inputfile" style="font-size:14px" multiple="" name="UsersC[ruta_foto]" data-multiple-caption="{count} files selected" id="UsersC_ruta_foto">-->
                                					<input disabled="disabled" type="file" name="UsersC[ruta_foto]" id="file" class="inputfile" data-multiple-caption="{count} files selected" multiple="" />
                                					<label for="file" id="photo_name" style="background-color:#aba7a7;"><strong>3- Upload the test taker picture</strong></label>
                                                <?php    
                                                }else{
                                                ?>
                                    			
                                                    <form id="tecs_form" enctype= "multipart/form-data" action='index.php?r=Clients/AnswersExamUser/takeExam&y=<?php echo base64_encode($userId);?>&p=<?php echo base64_encode($modLicUs[0]["id_licencias_usuario"]);?>&b=<?php echo base64_encode($examUserId);?>' method="POST">
                                                    <div class="col-sm-6">    
                                                <?php  
                                                }
                                                ?>
                                    			
                            				</div>
                                            <div class="col-sm-6">
                                                 <?php if($license == 18) {  echo "<img class='imgUser' style='width:65px !important;' src='/ilto3/images/clients/tn2_universidad del valle.gif'>";}?>
                                                 <?php if(!is_null($examUserId)){?> <input disabled="disabled" class="btnExam" id="btnExam_tecs" type="submit" value="start test" style="border:none;height: 80px !important;"><?php ;}?> 
                                            </div>
                                        </form>    
                                    </div>
                                </div>
                            <?php }  
                        break;
                        case 19:
                            echo '<div class="row" id="tecs_license" style="padding: 20px;">';
                                if($now <= $dateLic){
                                    echo $date_description.$result.'<br/>';
                                    echo "<br/><a href='https://iltoexams.com/' class='a_back'>Back to Iltoexams.com</a>";
                                }else{
                                    if($photoFlag == 0){
                                    ?>
                                        <form id="tecs_form" enctype= "multipart/form-data" action='index.php?r=Clients/UsersC/UpdatePhotoTecs' method="POST">    
                                    <?php
                                    }else{
                                    ?>
                                         <form id="tecs_form" enctype= "multipart/form-data" action='index.php?r=Clients/AnswersExamUser/takeExam&y=<?php echo base64_encode($userId);?>&p=<?php echo base64_encode($modLicUs[0]["id_licencias_usuario"]);?>&b=<?php echo base64_encode($examUserId);?>' method="POST">
                                    <?php
                                    }
                                    ?>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-6">
                                            <input type="number" name="phone" id="contact-number" placeholder="Enter contact number.">
                                        </div>
                                        <div class="col-sm-6 step_warning" >
                                           <span class="btn btn-warning btn-xs edit" id="require_1" style="font-size:14px;color: #fff;cursor: auto;font-weight: 700;">1- The phone number must include at least 7 digits. Do not use symbols o letters. (Required*).</span>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-6">
                                            
                                            <input type="checkbox" name="agree_terms" id="agree_terms_tecs" disabled><b>&nbsp;&nbsp;I agree to the terms and conditions.</b>
                                        </div>
                                        <div class="col-sm-6 step_warning">
                                           <span class="btn btn-warning btn-xs edit" id="require_2" style="font-size:14px;color: #fff;cursor: auto;font-weight: 700;display:none;">2- Please agree to the terms to start the test.</span>
                                        </div>
                                    </div>  
                                    <div class="row imagenTaker" style="margin-bottom: 10px;">
                                        <?php 
                                        if($photoFlag == 0){
                                        ?>
                                            <div class="col-sm-6">
                                                <input id="id_user" type="hidden" value="<?php echo $userId;?>" name="ExamsUser[id_user]">
                                            	<input id="license" type="hidden" value="<?php echo $modLicUs[0]["id_licencias_usuario"];?>" name="ExamsUser[license]">
                                            	<input id="examUserId" type="hidden" value="<?php echo $examUserId;?>" name="ExamsUser[examUserId]">
                                                <input id="ytExamsUser_ruta_foto" type="hidden" value="" name="ExamsUser[ruta_foto]">
                                				<!--<input type="file" class="inputfile" style="font-size:14px" multiple="" name="UsersC[ruta_foto]" data-multiple-caption="{count} files selected" id="UsersC_ruta_foto">-->
                                				<input disabled="disabled" type="file" name="UsersC[ruta_foto]" id="file" class="inputfile" data-multiple-caption="{count} files selected" multiple="" />
                                				<label for="file" id="photo_name" style="background-color:#aba7a7;"><strong>3- Upload the test taker picture</strong></label>
                                        <?php    
                                        }else{
                                        ?>
                                    	    <div class="col-sm-6">    
                                        <?php  
                                        }
                                        ?>
                                    	</div>
                                            <div class="col-sm-6">
                                                 <?php if($license == 18) {  echo "<img class='imgUser' style='width:65px !important;' src='/ilto3/images/clients/tn2_universidad del valle.gif'>";}?>
                                                 <?php if(!is_null($examUserId)){?> <input disabled="disabled" class="btnExam" id="btnExam_tecs" type="submit" value="start test" style="border:none;height: 80px !important;"><?php ;}?> 
                                            </div>
                                        </form>    
                                    </div>
                                </div>
                                </div>
                            <?php }  
                        break;
                        case 18:
                            echo '<div class="row" id="tecs_license" style="padding: 20px;">';
                                if($now <= $dateLic){
                                    echo $date_description.$result.'<br/>';
                                    echo "<br/><a href='https://iltoexams.com/' class='a_back'>Back to Iltoexams.com</a>";
                                }else{
                                    ?>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-6">
                                            <input type="number" minlength="6" name="phone" id="contact-number" placeholder="Enter contact number.">
                                        </div>
                                        <div class="col-sm-6 step_warning" >
                                           <span class="btn btn-warning btn-xs edit" id="require_1" style="font-size:14px;color: #fff;cursor: auto;font-weight: 700;">1- The phone number must include at least 7 digits. Do not use symbols o letters. (Required*).</span>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-6">
                                            <input type="checkbox" name="agree_terms" id="agree_terms_tecs" disabled><b>&nbsp;&nbsp;I agree to the terms and conditions.</b>
                                        </div>
                                        <div class="col-sm-6 step_warning">
                                           <span class="btn btn-warning btn-xs edit" id="require_2" style="font-size:14px;color: #fff;cursor: auto;font-weight: 700;display:none;">2- Please agree to the terms to start the test.</span>
                                        </div>
                                    </div>  
                                    <div class="row imagenTaker" style="margin-bottom: 10px;">
                                        <?php 
                                        if($photoFlag == 0){
                                        ?>
                                            <form id="tecs_form" enctype= "multipart/form-data" action='index.php?r=Clients/UsersC/UpdatePhotoTecs' method="POST">
                                                <div class="col-sm-6">
                                                
                                                    <input id="id_user" type="hidden" value="<?php echo $userId;?>" name="ExamsUser[id_user]">
                                            		<input id="license" type="hidden" value="<?php echo $modLicUs[0]["id_licencias_usuario"];?>" name="ExamsUser[license]">
                                            		<input id="examUserId" type="hidden" value="<?php echo $examUserId;?>" name="ExamsUser[examUserId]">
                                                    <input id="ytExamsUser_ruta_foto" type="hidden" value="" name="ExamsUser[ruta_foto]">
                                					<!--<input type="file" class="inputfile" style="font-size:14px" multiple="" name="UsersC[ruta_foto]" data-multiple-caption="{count} files selected" id="UsersC_ruta_foto">-->
                                					<input disabled="disabled" type="file" name="UsersC[ruta_foto]" id="file" class="inputfile" data-multiple-caption="{count} files selected" multiple="" />
                                					<label for="file" id="photo_name" style="background-color:#aba7a7;"><strong>3- Upload the test taker picture</strong></label>
                                                <?php    
                                                }else{
                                                ?>
                                    			
                                                    <form id="tecs_form" enctype= "multipart/form-data" action='index.php?r=Clients/AnswersExamUser/takeExam&y=<?php echo base64_encode($userId);?>&p=<?php echo base64_encode($modLicUs[0]["id_licencias_usuario"]);?>&b=<?php echo base64_encode($examUserId);?>' method="POST">
                                                    <div class="col-sm-6">    
                                                <?php  
                                                }
                                                ?>
                                    			
                            				</div>
                                            <div class="col-sm-6">
                                                 <?php if($license == 18) {  echo "<img class='imgUser' style='width:65px !important;' src='/ilto3/images/clients/tn2_universidad del valle.gif'>";}?>
                                                 <?php if(!is_null($examUserId)){?> <input disabled="disabled" class="btnExam" id="btnExam_tecs" type="submit" value="start test" style="border:none;height: 80px !important;"><?php ;}?> 
                                            </div>
                                        </form>    
                                    </div>
                                </div>
                                </div>
                            <?php }  
                        break;
                        case $tecsId:
                            echo '<div class="row" id="tecs_license" style="padding: 20px;">';
                                if($now <= $dateLic){
                                    echo $date_description.$result.'<br/>';
                                    echo "<br/><a href='https://iltoexams.com/' class='a_back'>Back to Iltoexams.com</a>";
                                }else{
                                    ?>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-6">
                                            <input type="number" minlength="6" name="phone" id="contact-number" placeholder="Enter contact number.">
                                        </div>
                                        <div class="col-sm-6 step_warning" >
                                           <span class="btn btn-warning btn-xs edit" id="require_1" style="font-size:14px;color: #fff;cursor: auto;font-weight: 700;">1- The phone number must include at least 7 digits. Do not use symbols o letters. (Required*).</span>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-6">
                                            <input type="checkbox" name="agree_terms" id="agree_terms_tecs" disabled><b>&nbsp;&nbsp;I agree to the terms and conditions.</b>
                                        </div>
                                        <div class="col-sm-6 step_warning">
                                           <span class="btn btn-warning btn-xs edit" id="require_2" style="font-size:14px;color: #fff;cursor: auto;font-weight: 700;display:none;">2- Please agree to the terms to start the test.</span>
                                        </div>
                                    </div>  
                                    <div class="row imagenTaker" style="margin-bottom: 10px;">
                                        <?php 
                                        if($photoFlag == 0){
                                        ?>
                                            <form id="tecs_form" enctype= "multipart/form-data" action='index.php?r=Clients/UsersC/UpdatePhotoTecs' method="POST">
                                                <div class="col-sm-6">
                                                
                                                    <input id="id_user" type="hidden" value="<?php echo $userId;?>" name="ExamsUser[id_user]">
                                            		<input id="license" type="hidden" value="<?php echo $modLicUs[0]["id_licencias_usuario"];?>" name="ExamsUser[license]">
                                            		<input id="examUserId" type="hidden" value="<?php echo $examUserId;?>" name="ExamsUser[examUserId]">
                                                    <input id="ytExamsUser_ruta_foto" type="hidden" value="" name="ExamsUser[ruta_foto]">
                                					<!--<input type="file" class="inputfile" style="font-size:14px" multiple="" name="UsersC[ruta_foto]" data-multiple-caption="{count} files selected" id="UsersC_ruta_foto">-->
                                					<input disabled="disabled" type="file" name="UsersC[ruta_foto]" id="file" class="inputfile" data-multiple-caption="{count} files selected" multiple="" />
                                					<label for="file" id="photo_name" style="background-color:#aba7a7;"><strong>3- Upload the test taker picture</strong></label>
                                                <?php    
                                                }else{
                                                ?>
                                    			
                                                    <form id="tecs_form" enctype= "multipart/form-data" action='index.php?r=Clients/AnswersExamUser/takeExam&y=<?php echo base64_encode($userId);?>&p=<?php echo base64_encode($modLicUs[0]["id_licencias_usuario"]);?>&b=<?php echo base64_encode($examUserId);?>' method="POST">
                                                    <div class="col-sm-6">    
                                                <?php  
                                                }
                                                ?>
                                    			
                            				</div>
                                            <div class="col-sm-6">
                                                 <?php if($license == 18) {  echo "<img class='imgUser' style='width:65px !important;' src='/ilto3/images/clients/tn2_universidad del valle.gif'>";}?>
                                                 <?php if(!is_null($examUserId)){?> <input disabled="disabled" class="btnExam" id="btnExam_tecs" type="submit" value="start test" style="border:none;height: 80px !important;"><?php ;}?> 
                                            </div>
                                        </form>    
                                    </div>
                                </div>
                                </div>
                            <?php }  
                        break;
                        case $edeId:
                             echo '<div class="row" style="padding: 20px;"> <br/>'; 
                                    echo $ede_description;
                                    ?>
                                    <div style="display:inline;">
                                        <input type="checkbox" name="agree_terms" id="agree_terms_ede">I agree to the terms and conditions.
                                    </div><br><br><br>
                                    <div class="imagenTaker" style="float: left; margin:auto;width:100%;">
                                        <?php 
                                            if(!is_null($examUserId)){?> 
                                                <form id="tecs_form" enctype= "multipart/form-data" action='index.php?r=Clients/AnswersExamUser/takeExam&y=<?php echo base64_encode($userId);?>&p=<?php echo base64_encode($modLicUs[0]["id_licencias_usuario"]);?>&b=<?php echo base64_encode($examUserId);?>' method="POST">
                                                    <input class="btnExam" id="btnExam_ede" type="submit" value="start test" style="border:none;height: 80px !important;margin-top: 10px;" disabled="disabled">
                                                </form>
                                            <?php }?>
                                    </div>
                                    </div><br>
                                    <?php 
                        break;
                        case 15:
                            echo '<div class="row" id="tecs_license" style="padding: 20px;">';
                                if($now <= $dateLic){
                                    echo $date_description.$result.'<br/>';
                                    echo "<br/><a href='https://iltoexams.com/' class='a_back'>Back to Iltoexams.com</a>";
                                }else{
                                    ?>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-6">
                                            <input type="number" name="phone" id="contact-number" placeholder="Enter contact number.">
                                        </div>
                                        <div class="col-sm-6 step_warning" >
                                           <span class="btn btn-warning btn-xs edit" id="require_1" style="font-size:14px;color: #fff;cursor: auto;font-weight: 700;">1- The phone number must include at least 7 digits. Do not use symbols o letters. (Required*).</span>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-sm-6">
                                            <input type="checkbox" name="agree_terms" id="agree_terms_tecs" disabled><b>&nbsp;&nbsp;I agree to the terms and conditions.</b>
                                        </div>
                                        <div class="col-sm-6 step_warning">
                                           <span class="btn btn-warning btn-xs edit" id="require_2" style="font-size:14px;color: #fff;cursor: auto;font-weight: 700;display:none;">2- Please agree to the terms to start the test.</span>
                                        </div>
                                    </div>  
                                    <div class="row imagenTaker" style="margin-bottom: 10px;">
                                        <?php 
                                        if($photoFlag == 0){
                                        ?>
                                            <form id="tecs_form" enctype= "multipart/form-data" action='index.php?r=Clients/UsersC/UpdatePhotoTecs' method="POST">
                                                <div class="col-sm-6">
                                                
                                                    <input id="id_user" type="hidden" value="<?php echo $userId;?>" name="ExamsUser[id_user]">
                                            		<input id="license" type="hidden" value="<?php echo $modLicUs[0]["id_licencias_usuario"];?>" name="ExamsUser[license]">
                                            		<input id="examUserId" type="hidden" value="<?php echo $examUserId;?>" name="ExamsUser[examUserId]">
                                                    <input id="ytExamsUser_ruta_foto" type="hidden" value="" name="ExamsUser[ruta_foto]">
                                					<!--<input type="file" class="inputfile" style="font-size:14px" multiple="" name="UsersC[ruta_foto]" data-multiple-caption="{count} files selected" id="UsersC_ruta_foto">-->
                                					<input disabled="disabled" type="file" name="UsersC[ruta_foto]" id="file" class="inputfile" data-multiple-caption="{count} files selected" multiple="" />
                                					<label for="file" id="photo_name" style="background-color:#aba7a7;"><strong>3- Upload the test taker picture</strong></label>
                                                <?php    
                                                }else{
                                                ?>
                                    			
                                                    <form id="tecs_form" enctype= "multipart/form-data" action='index.php?r=Clients/AnswersExamUser/takeExam&y=<?php echo base64_encode($userId);?>&p=<?php echo base64_encode($modLicUs[0]["id_licencias_usuario"]);?>&b=<?php echo base64_encode($examUserId);?>' method="POST">
                                                    <div class="col-sm-6">    
                                                <?php  
                                                }
                                                ?>
                                    			
                            				</div>
                                            <div class="col-sm-6">
                                                 <?php if($license == 18) {  echo "<img class='imgUser' style='width:65px !important;' src='/ilto3/images/clients/tn2_universidad del valle.gif'>";}?>
                                                 <?php if(!is_null($examUserId)){?> <input disabled="disabled" class="btnExam" id="btnExam_tecs" type="submit" value="start test" style="border:none;height: 80px !important;"><?php ;}?> 
                                            </div>
                                        </form>    
                                    </div>
                                </div>
                                </div>
                            <?php }  
                        break;
                        case 5:
                            ?>
                            <div class="imagenTaker" style="float: left; margin:auto;width:100%;">
                                <?php 
                                if(!is_null($examUserId)){?> 
                                    <form id="tecs_form" enctype= "multipart/form-data" action='index.php?r=Clients/AnswersExamUser/takeExamDemo&y=<?php echo base64_encode($userId);?>&p=<?php echo base64_encode($modLicUs[0]["id_licencias_usuario"]);?>&b=<?php echo base64_encode($examUserId);?>' method="POST">
                                        <input class="btnExam" id="btnExam_ede" type="submit" value="start test" style="border:none;height: 80px !important;margin-top: 10px;">
                                    </form>
                                <?php }?>
                            </div>
                        <?php
                        break;
                }
            echo '</div>'; //END second col-sm-6
            
?>        
        </div>
           
            <?php 
                }  
        
        echo '</div>';
            
        if(sizeof($modLicUs)==0) {
            $this->redirect('index.php?r=Clients/default/login&nolicense=1');
        }
        
 ?>
    <script src="js/custom-file-input.js"></script>
    <script>
            $( "a.btnExam" ).click(function( event ) {
                event.preventDefault();
                window.open(this.href,'takeExam','fullscreen=yes,location=no,menubar=no,titlebar=no,toolbar=no,width='+screen.width+',height='+screen.height);
            });
            
            var photoFlag = <?php echo $photoFlag; ?>
            
            
            $("#file").change(function(){
                var photoName = $("#file").val();
                var finalName = photoName.split("fakepath");
                var ext = finalName[1].split(".");
                var extension = ext[1];
                if(extension === "png" || extension === "jpg" || extension === "jpeg"){
                    $("#photo_name").html(finalName[1].substring(1));
                    $('#btnExam_tecs').prop("disabled", false);
                    $('#btnExam_tecs').prop("background-color", "blue");
                }else{
                    $("#photo_name").html("Please select a valid file.");
                    $('#btnExam_tecs').prop("disabled", true);
                }
                
            });
            
            $("#contact-number").on("change paste keyup", function() {
                chars = $("#contact-number").val().length;
                
                if(chars > 6){
                    $("#agree_terms_tecs").prop("disabled", false);
                    $("#agree_terms_tecs_m").prop("disabled", false); 
                    $("#require_1").hide();
                    $("#require_2").show(); 
                }else{
                    $("#agree_terms_tecs").prop("disabled", true);
                    $("#agree_terms_tecs_m").prop("disabled", true);
                    $("#require_1").show();
                    $("#require_2").hide(); 
                }
                
            });
            
            $(function(){
				$('#contact-number').keypress(function(e){
				    if(e.which == 101){
					    return false;
					}
				});
			});
            
            
            $("#agree_terms_tecs").change(function(){
                //console.log(photoFlag);
                if(photoFlag == 1){
                    if ($('#agree_terms_tecs').is(":checked"))
                    {
                        phone = $("#contact-number").val();
                        $('#btnExam_tecs').prop("disabled", false);
                        $('#btnExam_tecs').css("cursor","pointer");
                        $("#require_2").hide();
                        $('#contact-number').prop("disabled", true);
                    }else{
                        $('#btnExam_tecs').prop("disabled", true);
                        $('#btnExam_tecs').css("cursor","not-allowed");
                        $("#require_2").show(); 
                        $('#contact-number').prop("disabled", false);
                    } 
                    
                    
                }else{
                    //si no tiene foto
                    if ($('#agree_terms_tecs').is(":checked"))
                    {
                        phone = $("#contact-number").val();
                        $('#file').prop("disabled", false);
                        $('#photo_name').css("background-color","#d3394c");
                        $('#file').css("cursor","pointer");
                        $("#require_2").hide(); 
                        $("#require_3").show(); 
                    }else{
                        $('#file').prop("disabled", true);
                        $('#photo_name').css("background-color","#aba7a7");
                        $('#file').css("cursor","not-allowed");
                        $("#require_2").show(); 
                        
                    } 
                }
                var id_user = <?php echo $userId ?>;
                
                //save the contact using AJAX
                  $.ajax({
                      url : "/ilto3/index.php?r=Api/updateContactTaker",
                      type: "POST",
                      dataType: "text",
                      data: {
                          id_user: id_user, phone: phone
                      },
                      success: function(data){
                    
                        //document.getElementById("card_notify").innerHTML = data; 
                        // window.location.href ="/ilto3/index.php?r=site/logout&id="+id_admin;
                       
                      }
                  })

            });
            
            $("#agree_terms_ede").change(function(){
                if ($('#agree_terms_ede').is(":checked"))
                {   
                    //console.log("si");
                    $('#btnExam_ede').css("cursor","pointer");
                    $('#btnExam_ede').prop("disabled", false);
                    $('#contact-number').prop("disabled", true);
                }else{
                   // console.log("no");
                    $('#btnExam_ede').css("cursor","not-allowed");
                    $('#btnExam_ede').prop("disabled", true);
                    $('#contact-number').prop("disabled", false);
                    
                }
            });
            
            
            $("#ede_lic").click(function(){
            //console.log("abrimos ede, cerramos tecs");
               $("#ede_license").toggle();
               $("#tecs_license").hide();
            });
            
             $("#tecs_lic").click(function(){
            //console.log("abrimos tecs, cerramos ede");
               $("#tecs_license").toggle();
               $("#ede_license").hide();
            });
                    
        </script>
    
        <?php         
} else {
    
   if(!Yii::app()->user->isGuest){
            if(!is_null(Yii::app()->user->getState('ruta_foto'))){
                $logo = "images/users/tn2_".Yii::app()->user->getState('ruta_foto');
            } else {
                $logo= "images/clients/tn2_".Yii::app()->user->getState('logo_cliente');
            }
     
}                
    
?>


<style>
   
button.accordion {
    background-color: #31B744;
    color: #FFF;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
    font-weight:bolder;
    text-transform: uppercase;
}

#levels {
    background-color:#1363DC;
}

#levels:hover {
    background-color:#1e8ce0;
}

#important {
    background-color:#FFDA74;
}

#important:hover {
    background-color:#F8F400;
}

button.accordion.active, button.accordion:hover {
    background-color: #00dd06;
    color:#fff;
}

button.accordion:after {
    content: '\02795';
    font-size: 12px;
    color: #212121;
    float: right;
    margin-left: 5px;

}

button.accordion.active:after {
    content: "\2796";
}

div.panel {
    max-height: 0;
    overflow: hidden;
    transition: 0.6s ease-in-out;
    opacity: 0;
    margin-bottom:15px;
    font-size: 14px;
    color: #212121;
}

div.panel ul {
    list-style: url("/../../../../../../images/icons/next.png");
}

div.panel.show {
    opacity: 1;
    max-height: 1000px;  
    padding: 10px 30px;
}


.w3-table-all {
    border: 1px solid #ccc;
     font-size: 14px;
    color: #212121;
}


.w3-table-all th {
    background-color:  #7FC789;
}

.w3-table-all tr:nth-child(odd) {
    background-color: rgba(255, 255, 255, 0.74);
}
.w3-bordered tr, .w3-table-all tr {
    border-bottom: 1px solid #ddd;
}

.w3-table-all td, .w3-table-all th, .w3-table-all th:first-child, .w3-table-all td:first-child {
    padding: 8px;
}

.w3-table td, .w3-table th, .w3-table-all td, .w3-table-all th {
    padding: 6px 5px;
    display: table-cell;
    text-align: left;
    vertical-align: middle;
}

#foo {
    background-color: rgba(57, 181, 74, 0.09);
}

table, th, td {
    border: none;
}

.claseXX.last.col1 {
     display:block;   
}

.licenses {
    border:1px solid black;
}
/* Popup box BEGIN */
.hover_bkgr_fricc{
    background:rgba(0,0,0,.6);
    cursor:pointer;
    display:none;
    height:100%;
    position:fixed;
    text-align:center;
    top:0;
    width:100%;
    z-index:10000;
    font-family: "Open Sans","Segoe UI";font-size: 13px;color: #444;
}

#popup-title{
    font-weight: bold;
    color: #fff;
    padding: 10px 20px;
    background-color: #f4b400 !important;
    border-color: #f4b400;
}    
.hover_bkgr_fricc .helper{
    display:inline-block;
    height:100%;
    vertical-align:middle;
}
.hover_bkgr_fricc > div {
    background-color: #fff;
    box-shadow: 10px 10px 60px #555;
    display: inline-block;
    height: auto;
    max-width: 551px;
    min-height: 100px;
    vertical-align: middle;
    width: 60%;
    position: relative;
    border-radius: 8px;
}
.popupCloseButton {
   background-color: #c50d0d;
    border: 2px solid #fff;
    border-radius: 50px;
    cursor: pointer;
    display: inline-block;
    font-family: arial;
    font-weight: bold;
    position: absolute;
    top: -20px;
    right: -20px;
    font-size: 25px;
    line-height: 30px;
    width: 30px;
    height: 30px;
    text-align: center;
    color: #fff;
}
.popupCloseButton:hover {
    background-color: #ccc;
}
.trigger_popup_fricc {
    cursor: pointer;
    font-size: 20px;
    margin: 20px;
    display: inline-block;
    font-weight: bold;
}

input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    /* display: none; <- Crashes Chrome on hover */
    -webkit-appearance: none;
    margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
}

input[type=number] {
    -moz-appearance:textfield; /* Firefox */
}




</style>

<link rel="stylesheet" href="/ilto3/themes/ilto/html/assets/css/simple-popup/reset.css"> <!-- CSS reset -->
<link rel="stylesheet" href="/ilto3/themes/ilto/html/assets/css/simple-popup/style.css"> <!-- Resource style -->
<script src="/ilto3/themes/ilto/html/assets/js/simple-popup/modernizr.js"></script> <!-- Modernizr -->
<script src="/ilto3/themes/ilto/html/assets/js/simple-popup/main.js"></script> <!-- Modernizr -->
<div style="float:left;">
    <?php
        $id_admin = Yii::app()->user->getState('usuario');
        if($id_admin == 'o.garcia' ||  $id_admin == 'partialito'){
             ?>
             <a href="/ilto3/index.php?r=Clients/usersC/check" class="btn btn-success btn-xs edit"><i class="fa fa-check"></i>CHECK TAKER LIST</a>
             <?php
        }
    ?>
 
    <button class="accordion active">What's New?</button>
    <div class="panel show">
            <ul>
                <li><span><b>Unique session login:</b></span>
Users will only have one unique session simultaneously opened. This means that access to the same user (Test taker or Test Administrator) can only be done by that user ONLY. This action will prevent to log in a test from two different terminals as part of a possible fraud or have test administrators log in simultaneously from two different terminals, preventing any possible fraud as well. 
We strongly recommend test administrators to log out from their sessions as they will not be able to log back in as a security measure.<br><br>
<span><b>Session Log follow up:</b></span>
This security update, will also record the history of the actions done by tutors or test administrators in the platform. Thus, if there the need to check what a tutor of test administrator did in the platform, a session log history will be issued under the Total Test Administrator request.<br><br> 
Maximum Timing:The online sections (Reading, Listening and Language Structure) time limit has been adjusted to last a maximum of 110 minutes according to international standards. The speaking section may last from 5 to 20 minutes depending to the test taker language proficiency.<br><br></li>
                
            </ul>
    </div>
    
    <button class="accordion" id="important">Important information about the speaking section</button>
    <div class="panel">
        <p>
            <ul>
                <li>The speaking section is made of a total of 30 questions.</li>
                <li>These questions asses the speaking interaction and speaking production according to the CEFR - Common European Framework Languages.</li>
                <li>The speaking section will last from 5 to 10 minuest according to the proficiency exposed by the candidate.</li>
                <li>The proficiency assessment level increases every 6 questions.</li>
                <li>Do not approve or correct the candidate's answer, simply click on the correct or incorrect option according to the answer.</li>
                <li>Do not provide any feedback to the candidate.</li>
                <li>Read the questions loud and clearly.</li>
                <li>Encourage the candidate to answer the questions as completely as possible.</li>
                <li>Repeat each question only once, if requested word it differently but keeping the sense of the question.  </li>
            </ul>
        </p>   
    </div>
    
    <button class="accordion" id="levels">(CEFR) Levels Description</button>
    <div id="foo" class="panel">
        <table class="w3-table-all" style="width:100%">
          <tr>
            <th>Level</th>
            <th>Spoken Interaction</th> 
            <th>Spoken Production</th>
          </tr>
          <tr>
            <td>A1</td>
            <td>I can interact in a simple way provided the other person is prepared to repeat or rephrase things at a slower rate of speech and help me formulate what I'm trying to say. I can ask and answer simple questions in areas of inmediate need or on very familiar topics.</td> 
            <td>I can use simple phrases and sentences to describe where I live and people I know.</td>
          </tr>
          <tr>
            <td>A2</td>
            <td>I can communicate in simple and routine tasks requiring a simple and direct exchange of information on familiar topics and activities.  I can handle very short social exchanges, even though I can't usually understand enough to keep the conversation going myself.</td> 
            <td>I can use series of phrases and sentences to describe in simple terms my family and other people, living conditions, my educational background and my present or most recent job.</td>
          </tr>
          <tr>
            <td>B1</td>
            <td>I can deal with most situations likely to arise whilst traveling in an area where the language is spoken.  I can enter unpreparaed into conversation on topics that are familiar, of personal interest or pertinent to everyday life (e.g. family, hobbies, work, travel and current events).</td> 
            <td>I can connect phrases in a simple way in order to describe experiences and events, my dreams, hopes and ambitions.  I can briefly give reasons and explanations for opinions and plans.  I can narrate a story or relate the plot of a book or film and describe my reactions.</td>
          </tr>
          <tr>
            <td>B2</td>
            <td>I can interact with a degree or fluency and spontaneity that makes regular interaction with native speakers quite possible.  I can take an active part in a discussion in familiar contexts, accounting for an sustaining my views.</td> 
            <td>I can present clear, detailed descriptions on a wide range of subjects related to my field of interest.  I can explain a viewpoint on a topical issue giving the advantanges and disadvantages of various options.</td>
          </tr>
          <tr>
            <td>C1</td>
            <td>I can express myself fluently and spontaneously without much obvious searching for expressions.  I can use language flexibly and effectively for social and relate my contribution skillfully to those of other speakers.</td> 
            <td>I can present clear, detalled descriptions of complex subjects integrating sub-themes, developing particular points and rouding off with an appropriate conclusion.</td>
          </tr>
        </table>
    </div>
    <?php
    $notifications = array();
    $notificationsEnd = array();
    $notifications = $modUtil->LicensesAlertDashboard();
    $notificationsEnd = $modUtil->LicensesEndAlertDashboard();
    
    
    if(!empty($notifications)>0 || !empty($notificationsEnd)>0){
        echo '<div class="hover_bkgr_fricc">
            <span class="helper"></span>
            <div style="text-align:left;">
                <div class="popupCloseButton">X</div>
                <h3 id="popup-title">LICENSE ALERT</h3>
                <ul style="padding: 20px 40px;">';
                foreach($notifications as $notify){
                    echo '<li style="padding: 10px;list-style: square;">You have '.$notify[3].' '.$notify[1].' licenses that will expire '.$notify[2].'.</b>
                    </li>';
                }
                foreach($notificationsEnd as $notify){
                    echo '<li style="padding: 10px;list-style: square;">You have '.$notify[2].' TECS licenses left. If you need to purchase more licenses, send an email to info@iltoexams.com and a CSR will contact you shortly.</b>
                    </li>';
                }
            echo '</ul></div>
    </div>';
    }
    
       
    ?>
    <script>
    $(window).load(function () {
    
       $('.hover_bkgr_fricc').show();
    
    $('.hover_bkgr_fricc').click(function(){
        $('.hover_bkgr_fricc').hide();
    });
    $('.popupCloseButton').click(function(){
        $('.hover_bkgr_fricc').hide();
    });
});
    
    var acc = document.getElementsByClassName("accordion");
    var i;
    
    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function(){
            this.classList.toggle("active");
            this.nextElementSibling.classList.toggle("show");
      }
    }
    
    
  
    </script>
       

</div>

<?php
}
$config = array( 
    'scrolling' => 'yes', 
    'titleShow' => false,
    'autoscale'=> true,
    'overlayColor' => '#000',
    'showCloseButton' => true,
    'transitionIn'=>'elastic',
    'transitionOut'=>'elastic',
        // change this as you need
    );
    //$this->widget('application.extensions.fancybox.EFancyBox', array('target'=>'.fancy', 'config'=>$config));
?>

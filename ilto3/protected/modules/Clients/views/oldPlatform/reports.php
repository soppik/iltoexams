<?php
/*
Consulta de resultados de cada exámen (EDE)
SELECT concat(users_c.nombres," ",users_c.apellidos) as nombres, answers_exam_user. * , questions.texto AS text_question, questions.titulo_respuesta AS text_image, answers.texto AS text_answer, answers.es_verdadera, licenses_user.calificacion, licenses_user.nivel
FROM answers_exam_user, questions, answers, exams_user, licenses_user, users_c
WHERE answers_exam_user.id_pregunta = questions.id_pregunta
AND answers_exam_user.id_respuesta = answers.id_respuesta
AND (
answers_exam_user.id_examen_usuario = exams_user.id_examen_usuario
AND exams_user.id_licencia_usuario = licenses_user.id_licencias_usuario
AND licenses_user.id_licencia_cliente = 20
AND licenses_user.id_usuario = users_c.id_usuario_c
)
ORDER BY  `answers_exam_user`.`id_examen_usuario` ASC  
 */

/* @var $this LicensesUserController */
/* @var $model LicensesUser */


$config = Yii::app()->getComponents(false);
$connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
$connection->active=true;

$this->breadcrumbs=array(
	'Licenses Users'=>array('index'),
	'Manage',
);


/*Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#licenses-user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");*/



$SQL = "SELECT *  FROM `licenses_user` `t`  LEFT OUTER JOIN `users_c` `idUsuario` ON (`t`.`id_usuario`=`idUsuario`.`id_usuario_c`) AND (id_cliente='".Yii::app()->user->getState('cliente')."' and id_perfil >= 2) 
LEFT OUTER JOIN `licenses_client` `idLicenciaCliente` ON (`t`.`id_licencia_cliente`=`idLicenciaCliente`.`id_licencias_cliente`) WHERE (idLicenciaCliente.id_cliente LIKE '%".Yii::app()->user->getState('cliente')."%') LIMIT 100";
$list_l= $connection->createCommand($SQL)->queryAll();


$SQL = "SELECT * FROM `groups_users_clients` `t` WHERE id_cliente='".Yii::app()->user->getState('cliente')."' ";
$list_g= $connection->createCommand($SQL)->queryAll();

$SQL = "SELECT * FROM `licenses` `t` WHERE estado='A' ";
$list_t= $connection->createCommand($SQL)->queryAll();


foreach($list_t as $t){
    $lics[$t['id_licencia']] = $t['nombre'];
}

foreach($list_g as $g){
    $grupos[$g['id']] = $g['nombre'];
}


?>


<div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="widget">
                                <div class="widget-header ">
                                    <span class="widget-caption">Reports</span>
                                    <a class="btn btn-success" href="/ilto3/index.php?r=Api/ReportExport&id_cliente=<?php echo Yii::app()->user->getState('cliente') ?>">Click here to download the report</a>
                                </div>
                                <div class="widget-body">
                                    <form class="search-form" action="/ilto3/index.php?r=Api/reports" onsubmit="return false">
                                         <input type="hidden" value="<?php echo Yii::app()->user->getState('cliente') ?>" name="id_cliente">
                                    <table class="table table-striped table-hover table-bordered" id="editabledatatable" >
                                   
                                            <tr role="row">
                                                <th>
                                                    LICENSE
                                                    
                                                    <select class="form-control search" name="id_licencia" data-related="users_c.id_licencia_cliente">
                                                        <option value="">
                                                        <option value="6">English Diagnosis Exam (EDE)</option>
                                                        <option value="7">TECS - Test of English Communication Skills. v 3.0</option>
                                                        <option value="15">TECS - Test of English Communication Skills. v 4.0</option>
                                                        <option value="16">(EDE) English diagnosis exam. V 4.0</option>
                                                    </select>
                                                </th>
                                                <th>
                                                    GROUP
                                                      <br>
                                                      
                                                    <select class="form-control search" name="id_grupo_cliente" data-related="users_c.id_grupo_cliente">
                                                        <option value="">
                                                            
                                                         </option>
                                                        <?php 
                                                            foreach($list_g as $g){
                                                                ?>
                                                                    <option value="<?php echo $g['id']?>">
                                                                        <?php echo utf8_encode($g['nombre'])?>
                                                                    </option>
                                                                    
                                                                <?php
                                                            }
                                                        ?>
                                                     </select>
                                                 </th>
                                                  <th>
                                                    USER ID
                                                 <input type="text" name="id_usuario_c" class="search form-control" data-related="licenses_user.id_usuario_c"/> 
                                                 </th>
                                                 <th>
                                                EMAIL
                                                    <input type="text" name="email" class="search form-control" data-related="licenses_user.email"/> 
                                                </th>
                                                 <th>
                                                    NAME
                                                    <input type="text" name="nombres" class="search form-control" data-related="licenses_user.nombres"/> 
                                                 </th>
                                                 <th>
                                                    SURNAME
                                                    <input type="text" name="apellidos" class="search form-control" data-related="licenses_user.apellidos"/> 
                                                 </th>
                                                 <th>
                                                    
                                                    ALLOCATION DATE <br/>(YY-MM-DD)
                                                 </th>
                                                 <th>
                                                    
                                                    SCORE
                                                 </th>
                                                 <th>
                                                    
                                                    CEFR-LEVEL
                                                  
                                                    <select class="form-control search" name="nivel" data-related="licenses_user.nivel" width="50">
                                                        <option value=""></option>
                                                        <option value="A1">A1</option>
                                                        <option value="A2">A2</option>
                                                        <option value="B1">B1</option>
                                                        <option value="B2">B2</option>
                                                        <option value="C1">C1</option>
                                                    </select>
                                                </th>
                                                <th>
                                                    STATUS
                                                        
                                                </th>
                                                <th>
                                                    
                                                </th>
                                                
                                            </tr>
                                   

                                        <tbody class="search-rows">
                                            
                                            <?php 
                                                foreach($list_l as $rooms){
                                                    ?>
                                                        <tr>
                                                            <td><?php echo utf8_encode($lics[$rooms['id_licencia']]); ?></td>
                                                            <td><?php echo utf8_encode($grupos[$rooms['id_grupo_cliente']]) ?></td>
                                                            <td><?php echo $rooms['id_usuario_c']?></td>
                                                            <td><?php echo $rooms['email']?></td>
                                                            <td><?php echo utf8_encode($rooms['nombres'])?></td>
                                                            <td><?php echo utf8_encode($rooms['apellidos'])?></td>
                                                             <td><?php echo substr($rooms['fecha_asignacion'],0,10)?></td>
                                                             <td><?php echo $rooms['calificacion']?></td>
                                                              <td><?php echo utf8_encode($rooms['nivel'])?></td>
                                                            <td class="center "><?php echo $rooms['estado']=="A" ? "Not Finished" : ($rooms['estado']=="F" ? "Final" : "");  ?></td>
                                                            <td> <a title="View_Certificate" target="_blank" style="font-size:10px;" href="/ilto3/index.php?r=Clients/licensesUser/certificate&amp;id=<?php echo $rooms['id_licencias_usuario']?>"class="btn btn-warning btn-xs edit"><i class="fa fa-check"></i>Certificate</a>
                                                                       </td>      
                                                        </tr>
                                                    <?php
                                                }
                                            ?>
                                            
                                            
                                            
                                        </tbody>
                                    </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                                    <div class="widget">
                                        <div class="widget-header bg-blue">
                                            <i class="widget-icon fa fa-arrow-left"></i>
                                            <span class="widget-caption">REPORTS INSTRUCTIONS </span>
                                           
                                        </div><!--Widget Header-->
                                        <div class="widget-body">
                                            <div>
                                               Here you can see the test results and filter them by different categories to find out the amount of students in each category. 
    For example the amount of students in level A1 or the amount of students of a certain group.	
                                            </div>
                                        </div><!--Widget Body-->
                                    </div><!--Widget-->
                                </div>
                                
                       
                    </div>


   <!--Page Related Scripts-->
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/jquery.dataTables.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/ZeroClipboard.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.tableTools.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.bootstrap.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/datatables-init.js"></script>
    <script>
        InitiateSimpleDataTable.init();
        InitiateExpandableDataTable.init();
        InitiateSearchableDataTable.init();
    </script>

<?php /*
<h1 style="float:left;" ></h1> 
<div class="instrucciones">Here you can see the test results and filter them by different categories to find out the amount of students in each category. 
    For example the amount of students in level A1 or the amount of students of a certain group.</div> 
<?php 
$modUtil = new Utilidades;
$arregloCef = $modUtil->arregloNiveles();
$gridWidget=$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'licenses-user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
                array(
                    'name'=>'laLicencia',
                    'value'=>'$data->idLicenciaCliente->idLicencia->nombre',
                    'filter'=>CHtml::listData(Licenses::model()->forsale()->findAll(),'id_licencia','nombre'),
                    //'filter'=>CHtml::listData(LicensesClient::model()->with('idLicencia')->findAll(),'id_licencia_cliente','idLicencia.nombre'),
                ),
                array(
                    'name'=>'usuario_grupo',
                    'value'=>'$data->idUsuario->idGrupoCliente->nombre',
                    'filter'=>CHtml::listData(GroupsUsersClients::model()->findAll(),'id','nombre'),
                ),
                array(
                    'name'=>'usuario_id',
                    'header'=>'User ID',
                    'value'=> '$data->id_usuario'
                ),
                array(
                    'header'=>'Email',
                    'value'=> '$data->idUsuario->email'
                ),
                array(
                    'name'=>'usuario_nombre',
                    'value'=> '$data->idUsuario->nombres'
                ),
                array(
                    'name'=>'usuario_apellido',
                    'value'=> '$data->idUsuario->apellidos'
                ),
		'fecha_asignacion',
		'calificacion',
            array(
                'name'=>'nivel',
                'filter'=> $arregloCef,
            ),
                array(
                    'name'=>'estado',
                    'value'=>'$data->estado=="A" ? "Not Finished" : ($data->estado=="F" ? "Final" : "")',
                    'filter'=>array("A"=>"Not Finished","F"=>"Final"),
                    ),
	),
)); 

//$this->renderExportGridButton($gridWidget,'Export Results',array('class'=>'btn btn-info pull-right'));?>
<script>
        $(".tool_tip").mouseover(function(){
           eleOffset = $(this).offset();
          $(this).next().fadeIn("fast").css({
                                  left: eleOffset.left + $(this).outerWidth(),
                                  top: eleOffset.top
                          });
          }).mouseout(function(){
                  $(this).next().hide();
          });
        $('.sort-link').attr('title','Click to Sort');  
    </script>

*/ ?>
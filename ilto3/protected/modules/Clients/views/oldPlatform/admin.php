<?php
/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = $session['profile'];


if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}

if($profile == 6){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}

/* session manager
end
*/

/* @var $this OldPlatformController */
/* @var $model OldPlatform */

$config = Yii::app()->getComponents(false);
$connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
// establish connection. You may try...catch possible exceptions
$connection->active=true;

$SQL = "SELECT * FROM `old_platform` WHERE `client` LIKE '".Yii::app()->user->getState('cliente')."' LIMIT 100";
$list_old= $connection->createCommand($SQL)->queryAll();


?>
<div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="widget">
                                <div class="widget-header ">
                                    <span class="widget-caption">Reports</span>
                                    <a class="btn btn-success" href="/ilto3/index.php?r=Api/ReportExport&id_cliente=<?php echo Yii::app()->user->getState('cliente') ?>">Click here to download the report</a>
                                </div>
                                <div class="widget-body">
                                    <form class="search-form" action="/ilto3/index.php?r=Api/oldReport" onsubmit="return false">
                                         <input type="hidden" value="<?php echo Yii::app()->user->getState('cliente') ?>" name="id_cliente">
                                    <table class="table table-striped table-hover table-bordered" id="editabledatatable" >
                                   
                                            <tr role="row">
                                                <th>
                                                   CODE
                                                   <input type="text" name="code" class="search form-control" data-related="old_platform.code"/> 
                                                </th>
                                                <th>
                                                    NAME
                                                         <input type="text" name="name" class="search form-control" data-related="old_platform.name"/> 
                                              </th>
                                                  <th>
                                                    EXAM DATE
                                                 </th>
                                                 <th>
                                                SCORE
                                                </th>
                                                 <th>   
                                                    CEFR-LEVEL
                                                  
                                                    <select class="form-control search" name="cef" data-related="licenses_user.nivel" width="50">
                                                        <option value=""></option>
                                                        <option value="A1">A1</option>
                                                        <option value="A2">A2</option>
                                                        <option value="B1">B1</option>
                                                        <option value="B2">B2</option>
                                                        <option value="C1">C1</option>
                                                    </select>
                                                </th>
                                                <th>
                                                </th>
                                                <th>
                                                    
                                                </th>
                                                
                                            </tr>
                                   

                                        <tbody class="search-rows">
                                            
                                            <?php 
                                                foreach($list_old as $reg){
                                                    $pdf = ".pdf";
                                                    $code = $reg['code'].$pdf;                                                
                                                    ?>
                                                        <tr>
                                                            <td><?php echo $reg['code']?></td>
                                                            <td><?php echo utf8_encode($reg['name'])?></td>
                                                            <td><?php echo $reg['exam_date']?></td>
                                                            <td><?php echo $reg['score']?></td>
                                                            <td><?php echo $reg['cef']?></td>
                                                            <td> <a title="View_Certificate" target="_blank" style="font-size:10px;" href="/ilto3/../newtest2/examspdf/results<?php echo $code?>"class="btn btn-warning btn-xs edit"><i class="fa fa-check"></i>Certificate here</a>
                                                                       </td>      
                                                        </tr>
                                                    <?php
                                                }
                                            ?>
                                            
                                        </tbody>
                                    </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                                    <div class="widget">
                                        <div class="widget-header bg-blue">
                                            <i class="widget-icon fa fa-arrow-left"></i>
                                            <span class="widget-caption">REPORTS INSTRUCTIONS </span>
                                           
                                        </div><!--Widget Header-->
                                        <div class="widget-body">
                                            <div>
                                               Here you can see the test results and filter them by different categories to find out the amount of students in each category. 
    For example the amount of students in level A1 or the amount of students of a certain group.	
                                            </div>
                                        </div><!--Widget Body-->
                                    </div><!--Widget-->
                                </div>
                                
                       
                    </div>


   <!--Page Related Scripts-->
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/jquery.dataTables.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/ZeroClipboard.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.tableTools.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.bootstrap.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/datatables-init.js"></script>
    <script>
        InitiateSimpleDataTable.init();
        InitiateExpandableDataTable.init();
        InitiateSearchableDataTable.init();
    </script>
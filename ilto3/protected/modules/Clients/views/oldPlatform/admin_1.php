<?php
/* @var $this OldPlatformController */
/* @var $model OldPlatform */

?>
<style>
    .claseXX.last.col1 {
         display:block;   
    }    
</style>
<h1 style="float:left;" >Previous Version Tests</h1>
<div class="instrucciones">Here you can find the test reports of the takers who took the TECS test in our previous version. </div>

<?php 
if(Yii::app()->user->getState('cliente')=='9002120741'){ $mpath='eww';} //English
if(Yii::app()->user->getState('cliente')=='8605173021'){ $mpath='newtest2';}  // AreaAndina
if(Yii::app()->user->getState('cliente')=='9004302647'){ $mpath='exam_language_2_0';}  // Language


$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'old-platform-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'code',
		'name',
		'exam_date',
		'score',
		'cef',
		array(
			'class'=>'CButtonColumn',
                    'template'=>'{certificate}',
                    'buttons'=> array(
                        'certificate'=>array(
                            'class'=>'CLinkColumn',
                            'header'=>'Certificate',
                            'url'=> 'Yii::app()->baseUrl."/../'.$mpath.'/examspdf/results".$data->code.".pdf"',
                            'options'=>array("target"=>"_blank"),
                        )
                    )
		),
	),
)); ?>
<script>
        $(".tool_tip").mouseover(function(){
           eleOffset = $(this).offset();
          $(this).next().fadeIn("fast").css({
                                  left: eleOffset.left + $(this).outerWidth(),
                                  top: eleOffset.top
                          });
          }).mouseout(function(){
                  $(this).next().hide();
          });
$('.sort-link').attr('title','Click to Sort');  
    </script>
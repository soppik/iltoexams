<?php
/* @var $this OldPlatformController */
/* @var $model OldPlatform */

$this->breadcrumbs=array(
	'Old Platforms'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List OldPlatform', 'url'=>array('index')),
	array('label'=>'Create OldPlatform', 'url'=>array('create')),
	array('label'=>'Update OldPlatform', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete OldPlatform', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage OldPlatform', 'url'=>array('admin')),
);
?>

<h1>View OldPlatform #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'client',
		'code',
		'name',
		'exam_date',
		'score',
		'cef',
	),
)); ?>

<?php
/* @var $this OldPlatformController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Old Platforms',
);

$this->menu=array(
	array('label'=>'Create OldPlatform', 'url'=>array('create')),
	array('label'=>'Manage OldPlatform', 'url'=>array('admin')),
);
?>

<h1>Old Platforms</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

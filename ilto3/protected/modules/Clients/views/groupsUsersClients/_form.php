<?php
/* @var $this GroupsUsersClientsController */
/* @var $model GroupsUsersClients */
/* @var $form CActiveForm */

/* CAMBIOS EFECTUADOS POR ING.CHRISTIAN VELANDIA  */
/* Agregar divs .SPAN6 para separar y dar formato a los errores de no llenado de campos, también 
    se modificó la clase errorMessage en CSS para darle el fomato azul que ahora tiene
*/

?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'groups-users-clients-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php //echo $form->errorSummary($model); ?>


	<div class="row">
		<div class="span6">
			<?php echo $form->labelEx($model,'nombre'); ?>
			<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>200, 'required'=>required)); ?>
		</div>
		<div class="span6">
			<?php echo $form->error($model,'nombre'); ?>
		</div>
	</div>
<?php if(!$model->isNewRecord){ ?>
        <div class="row">
	        <div class="span6">
				<?php echo $form->labelEx($model,'estado'); ?>
				<?php
		            $dataCombo=array('A'=>'Enabled','I'=>'Disabled');
		            $this->widget('ext.select2.ESelect2',array(
		            'name'=>'GroupsUsersClients[estado]',
		            'id'=> 'combo_estado',
		            'data'=>$dataCombo,
		                'value'=>$model->estado,
		            'options'=>array(
		              'placeholder'=>'Choose one...',
		              'allowClear'=>true,
		                'width'=>'250px',
		            ),
		          ));
		            ?>
	         </div>
	         <div class="span6">
				<?php echo $form->error($model,'estado'); ?>
			</div>
	</div>
<?php } ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
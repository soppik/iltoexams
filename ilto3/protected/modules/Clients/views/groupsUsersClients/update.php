<?php
/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = $session['profile'];


if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}

if($profile == 6){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}

/* session manager
end
*/

/* @var $this GroupsUsersClientsController */
/* @var $model GroupsUsersClients */

$this->breadcrumbs=array(
	'Groups Users Clients'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Create Groups', 'url'=>array('create')),
	array('label'=>'Manage Groups', 'url'=>array('admin')),
);
$this->layout = "//layouts/column2a";
?>


<?php //$this->renderPartial('_form', array('model'=>$model)); ?>


<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
	
                                    <div class="widget radius-bordered">
                                        <div class="widget-header bg-blue">
                                            <span class="widget-caption">Update Group <?php echo $model->id; ?></span>
                                        </div>
                                        <div class="widget-body">
                                            <form  id="groups-users-clients-form" action="/ilto3/index.php?r=Clients/groupsUsersClients/update&id=<?php echo $model->id; ?>" method="post" class="form-horizontal bv-form" data-bv-message="This value is not valid" data-bv-feedbackicons-valid="glyphicon glyphicon-ok" data-bv-feedbackicons-invalid="glyphicon glyphicon-remove" data-bv-feedbackicons-validating="glyphicon glyphicon-refresh" novalidate="novalidate">
                                            	<button type="submit" class="bv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
                                               
                                                
                                                <div class="form-group has-feedback">
                                                    <label class="col-lg-4 control-label">Name *</label>
                                                    <div class="col-lg-8">
                                                        <input required=required maxlength="200" value="<?php echo $model->nombre ?>" size="60" type="text" class="form-control" name="GroupsUsersClients[nombre]" id="GroupsUsersClients_nombre" >

                                                    </div>
                                                </div>
                                                
                                                <div class="form-group has-feedback">
                                                    <label class="col-lg-4 control-label">Status *</label>
                                                    <div class="col-lg-8">
		                                                <select id="combo_estado" name="GroupsUsersClients[estado]">
																<option value=""></option>
																<option value="A" <?php echo ($model->estado=='A')?'selected="selected"':'' ?>>Enabled</option>
																<option value="I" <?php echo ($model->estado=='I')?'selected="selected"':'' ?>>Disabled</option>
														</select>
													</div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <div class="col-lg-offset-4 col-lg-8">
                                                        
                                                        <input class="btn btn-palegreen" type="submit"  name="yt0" value="Save">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                      
                                </div>
</div>
	
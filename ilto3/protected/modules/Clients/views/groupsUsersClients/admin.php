<?php

/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = $session['profile'];


if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}

if($profile == 6){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}

/* session manager
end
*/

// agregamos al log del usuario access
$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 4, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'Access users group', '')";
$log= Yii::app()->db->createCommand($SQL)->query(); 


/* @var $this GroupsUsersClientsController */
/* @var $model GroupsUsersClients */

$this->breadcrumbs=array(
	'Groups Users Clients'=>array('index'),
	'Manage',
);
$visible = (Yii::app()->user->getState('id_perfil') < 4) ? 'true':'false';
if($visible=='true'){
$this->menu=array(
	array('label'=>'Create Groups', 'url'=>array('create')),
);
}
$config = Yii::app()->getComponents(false);
$connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
// establish connection. You may try...catch possible exceptions
$connection->active=true;


//"SELECT * FROM `groups_users_clients` WHERE `id_cliente` LIKE '12340987' ORDER BY `id` DESC";
$SQL = "SELECT * FROM `groups_users_clients` `t` WHERE id_cliente='".Yii::app()->user->getState('cliente')."' order by id DESC LIMIT 50";
$list= $connection->createCommand($SQL)->queryAll();


/*Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#groups-users-clients-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");*/
$this->layout = "//layouts/column2a";
?>






<div class="row">
                        <div class="col-lg-8 col-sm-8 col-xs-12">
                            <div class="widget">
                                <div class="widget-header ">
                                    <span class="widget-caption">Manage Groups</span>
                                    <a href="/ilto3/index.php?r=Clients/groupsUsersClients/create" class="btn btn-success">Create Groups</a>
                                </div>
                                <div class="widget-body">
                                    <form class="search-form" action="/ilto3/index.php?r=Api/group" onsubmit="return false">
                                         <input type="hidden" value="<?php echo Yii::app()->user->getState('cliente') ?>" name="id_cliente">
                                    <table class="table table-striped table-hover table-bordered" id="editabledatatable">
                                        <thead>
                                            <tr role="row">
                                                <th>
                                                    
                                                    NAME
                                                    <br>
                                                    <input type="text" name="nombre" class="search form-control" data-related="groups_users_clients.nombre"/>
                                                </th>
                                                <th>
                                                    STATUS
                                                    <br>
                                                    <select class="form-control search" name="estado" data-related="class_rooms.estado">
                                                        <option value="">
                                                            
                                                        </option>
                                                        <option value="A">Enable</option>
                                                        <option value="I">Disable</option>
                                                    </select>
                                                </th>
                                                <th>
                                                    
                                                </th>
                                                
                                            </tr>
                                        </thead>

                                        <tbody class="search-rows">
                                            
                                            <?php 
                                                foreach($list as $rooms){
                                                    ?>
                                                        <tr class="<?php echo $rooms['id']?>">
                                                            <?php
                                                               // $SQL = "SELECT * FROM `clients` WHERE `id_cliente` = '".$rooms['id_cliente']."'";
                                                               // $userData= $connection->createCommand($SQL)->queryAll();
                                                            ?>
                                                      
                                                            <td><?php echo utf8_encode($rooms['nombre'])?></td>
                                                            
                                                            <td class="center "><?php echo ($rooms['estado']=='A')?'ENABLED':'DISABLED' ?></td>
                                                            <td>
                                                                <a href="/ilto3/index.php?r=Clients/groupsUsersClients/update&id=<?php echo $rooms['id']?>" class="btn btn-info btn-xs edit"><i class="fa fa-edit"></i> Edit</a>
                                                            <?php 
                                                                    $SQL = "SELECT COUNT(*) as total FROM `users_c` WHERE `id_grupo_cliente`='".$rooms['id']."' ";
                                                                
                                                                    $LicensesCount = $connection->createCommand($SQL)->queryAll();
                                                                    
                                                                    if($LicensesCount[0]['total']==0){
                                                                ?>
                                                                    <a href="/ilto3/index.php?r=Clients/groupsUsersClients/delete&id=<?php echo $rooms['id']?>" value="<?php echo $rooms['id']?>" class="btn btn-danger btn-xs delete"><i class="fa fa-trash-o"></i> Delete</a>
                                                                <?php 
                                                                    }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                    <?php
                                                }
                                            ?>
                                            
                                            
                                            
                                        </tbody>
                                    </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-lg-4 col-sm-4 col-xs-12">
                                    <div class="widget">
                                        <div class="widget-header bg-blue">
                                            <i class="widget-icon fa fa-arrow-left"></i>
                                            <span class="widget-caption">GROUPS INSTRUCTIONS </span>
                                           
                                        </div><!--Widget Header-->
                                        <div class="widget-body">
                                            <div>
                                                Create groups to better classify the offices or groups in your organization. <br/><br/>
                                                Example: Group the students or employees of a certain program or office, to create better reports. Engineering students or Accounting Department employees.	
                                            </div>
                                        </div><!--Widget Body-->
                                    </div><!--Widget-->
                                </div>
                                
                       
                    </div>


   <!--Page Related Scripts-->
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/jquery.dataTables.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/ZeroClipboard.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.tableTools.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.bootstrap.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/datatables-init.js"></script>
    <script>
        InitiateSimpleDataTable.init();
        InitiateExpandableDataTable.init();
        InitiateSearchableDataTable.init();
    </script>
    <script>
jQuery(document).on('click', '.btn.btn-danger.btn-xs.delete',function(e){
    e.preventDefault();
        var url = jQuery(this).attr('href')
        var id = $(this).attr('value')
        jQuery.post(url, { id: $(this).attr('value')}, function(data){
             console.log(data)
             var cl = '.';
             var clas = cl.concat(id)
             jQuery(clas).remove()
        })
})
    
</script>



<?php 
/*
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'groups-users-clients-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'nombre',
                array(
                    'name'=>'estado',
                    'value'=>'($data->estado=="A" ? "Enabled" : "Disabled")',
                    'filter'=>array("A"=>"Enable","I"=>"Disable"),
                    ),
		array(
		'class'=>'CButtonColumn',
                    'template'=>'{update}{delete}',
                    'buttons'=>array(
                        'delete'=>array(
                            'label'=>'Delete&nbsp;',
                            'imageUrl'=>NULL,
                            'visible'=>$visible,
                        ),
                        'update'=>array(
                            'label'=>'Edit&nbsp;',
                            'imageUrl'=>NULL,
                            'visible'=>$visible,
                        )
                    )
		),
	),
)); ?>
<script>
        $(".tool_tip").mouseover(function(){
           eleOffset = $(this).offset();
          $(this).next().fadeIn("fast").css({
                                  left: eleOffset.left + $(this).outerWidth(),
                                  top: eleOffset.top
                          });
          }).mouseout(function(){
                  $(this).next().hide();
          });

    </script>*/ ?>
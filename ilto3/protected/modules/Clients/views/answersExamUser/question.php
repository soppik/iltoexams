<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 

$pregunta = Questions::model()->findByPk($parametros['id_pregunta']);
$tplQuestion = "tpl_".$pregunta->template."_txtQuestion";
$tplAnswer = "tpl_".$pregunta->template."_txtAnswer";

$SQL = "SELECT id_licencia FROM `licenses_user` INNER JOIN licenses_client ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente WHERE `id_licencias_usuario` = '".$_GET["license"]."'";
$license_type= Yii::app()->db->createCommand($SQL)->queryAll();


echo "<div class='row' style='min-height:50px;'>";

//SPEAKING
if($pregunta->tipo_presentacion=='S'){
    echo "<div class='col-sm-1'  style='text-align:right;'>
        <img class='bullet' id='img_".$parametros['id_pregunta']."'  src='images/circle-shape-outline.png'/>
    </div>
    <div class='col-sm-4 a $tplQuestion'>";
        echo  $pregunta->texto;
    echo  "</div> 
    <div class='col-sm-2 respuesta $tplAnswer'>";
        echo "<span class='lit_title'>Comprehension</span><input type='hidden' name='response' value=0 id='response'> <input type='range' style='display:inline;'  class='range_inp' min='0' max='100' step='1'  id='1_".$parametros['id_pregunta']."' list='tickmarks'>
    </div>
    <div class='col-sm-2 respuesta $tplAnswer'>
        <input class='respuesta' id='preguntas_".$parametros['id_pregunta']."' type='hidden' value='0' name='preguntas[".$parametros['id_pregunta']."]'>
        <span class='lit_title'>Production</span><input type='range' style='display:inline;' class='range_inp second_range' min='0' max='100' step='1' list='tickmarks2'  id='2_".$parametros['id_pregunta']."'>
    </div>
    <div class='col-sm-1'  style='text-align:right; padding-top: 20px;'>
        <input class='no_answer' type='checkbox' data-toggle='tooltip' data-placement='right' data-original-title='NO ANSWER' id='nocontesta_".$parametros['id_pregunta']."' name='nocontesta_".$parametros['id_pregunta']."' style='opacity: 1;left: 0;z-index: 1;top:23px;'>N.A.
    </div>
    <div class='col-sm-1'  style='text-align:right;'>
        <span type='text' class='sub_total' id='amt_".$parametros['id_pregunta']."'  style='float:left;'>0 %</span></div>
    <script>hacetotal=1;</script>";
    
}


if($pregunta->tipo_presentacion=='D'){
        $SQL = "SELECT description FROM question_header WHERE id_question_header ='".$pregunta->id_question_header."'";
        $description= Yii::app()->db->createCommand($SQL)->queryAll();
        echo  "<div class='col-sm-6 txtQuestion $tplQuestion' style='text-align:center;min-height: 300px;vertical-align: middle;padding:25px;'>";
    if(strlen($pregunta->ruta_imagen)>0){
        echo  "<img src='".Yii::app()->baseUrl."/images/questions/tn3_".$pregunta->ruta_imagen."'><br>";
    }
    if(strlen($pregunta->ruta_audio)>0){
        Yii::app()->controller->widget("ext.jouele.jouele.Jouele", array(
                            "file" => Yii::app()->baseUrl."/".$pregunta->ruta_audio,
                            "name" => "Question",
                            "htmlOptions" => array(
                                "class" => "jouele-skin-silver",
                                "style"=>"width:180px; background-color:#f2f2f2",
                             )
                ));
    }
    echo "<br>";
    
    
    echo  "</div><div class='col-sm-6 respuesta $tplAnswer' style='text-align:left;'>
            <h3 style='margin-top: 13px; margin-bottom: 20px;font-weight:400 !important;'>".$description[0]["description"]."</h3>";
    $posIni = strpos($pregunta->texto,'DROP_HERE')-1;
    $modResPreg = Answers::model()->findAllByAttributes(array('id_pregunta'=>$parametros['id_pregunta']));
    shuffle($modResPreg);
    $arrResp = array();
    foreach($modResPreg as $respuesta){
        $arrResp[$respuesta->id_respuesta]=$respuesta->texto;
    }
    echo substr($pregunta->texto,0,$posIni)."&nbsp;<span>";
    echo CHtml::dropDownList($parametros['nombreCampo'],'',$arrResp,array('empty' => 'Choose one...','onclick'=>'do_response();','class'=>'drop-answer'));
    echo "</span>".substr($pregunta->texto,$posIni+10);
    echo  "<div class='row' style='text-align:right; margin-right: 5px;'>
                <a href='#' class='myButton btn btn-success'>Next</a>
            </div></div>";
}

if($pregunta->tipo_presentacion=='M'){
    $SQL = "SELECT description FROM question_header WHERE id_question_header ='".$pregunta->id_question_header."'";
    $description= Yii::app()->db->createCommand($SQL)->queryAll();
    echo  "<div class='col-sm-6 txtQuestion $tplQuestion' style='text-align:center;'>";
    echo "<script>marcaradio=1;</script>";
    
    
    if(strlen($pregunta->ruta_audio)>0){
       echo '<audio class="audio-control" controls autoplay controlsList="nodownload" style="margin-top:5px;">
                <source src="'.$pregunta->ruta_audio.'" type="audio/ogg" >
                <source src="'.$pregunta->ruta_audio.'" type="audio/mpeg">
                Your browser does not support the audio element.
            </audio>';
    }
    
    if(strlen($pregunta->ruta_imagen)>0){
        echo  "<br/><img src='".Yii::app()->baseUrl."/images/questions/tn3_".$pregunta->ruta_imagen."'><br><br>";
    }
    $modResPreg = Answers::model()->findAllByAttributes(array('id_pregunta'=>$parametros['id_pregunta']));
    shuffle($modResPreg);
    $col=1;
    echo "</div>
<div style='vertical-align:middle; padding-left:10px;' class='col-sm-6 respuesta $tplAnswer'>";
    echo "<h3 style='margin-top: 13px; margin-bottom: 20px;font-weight:400 !important;'>".$description[0]["description"]."</h3>";
    echo  "<h3 style='margin-top: 13px; margin-bottom: 20px;font-weight:400 !important;'>".$pregunta->texto."</h3>"; 
   echo "<div class='row radio_inps'>
   <input type='radio'  class='img_radio' checked='checked'  onchange='do_response();' value='1' name='".$parametros['nombreCampo']."' style='display:none;opacity: 0;left: 0px;position: inherit !important;z-index: 10;'>";  
       
           foreach($modResPreg as $respuesta){
                echo "<div class='col-sm-6 td_$tplAnswer' style='min-height: 100px;'>";
                            
                            //image answer
                            if(strlen($respuesta->ruta_imagen)>0){ 
                                echo '<div class="radio_img">
                                    <label class="img_radio">
                                        <input type="radio" class="img_radio" onchange="do_response();"  value='.$respuesta->id_respuesta.' name="'.$parametros['nombreCampo'].'">
                                        <img class="img_answer" src="'.Yii::app()->baseUrl.'/images/answers/tn2_'.$respuesta->ruta_imagen.'">
                                    </label></div>';
                                
                            }
                            
                            //sound answer
                            if(strlen($respuesta->ruta_audio)>0){
                                echo '<label class="sound_radio"><input type="radio" class="img_radio" onchange="do_response();"  value='.$respuesta->id_respuesta.' name="'.$parametros['nombreCampo'].'"> <i></i>'.$respuesta->texto.'</label>';
                                        Yii::app()->controller->widget("ext.jouele.jouele.Jouele", array(
                                                "file" => Yii::app()->baseUrl."/".$respuesta->ruta_audio,
                                                "name" => "Question",
                                                "ready"=> 'function(){ $(this).jPlayer("play");}',
                                                "htmlOptions" => array(
                                                    "class" => "jouele-skin-silver",
                                                    "style"=>"width:180px; background-color:#f2f2f2",
                                                 )
                                    ));
                            }
                            
                            //text answer
                            if(strlen($respuesta->ruta_imagen)==0 && strlen($respuesta->ruta_audio)==0){
                                echo '<label class="i-checks"><input type="radio" class="img_radio" onchange="do_response();"  value='.$respuesta->id_respuesta.' name="'.$parametros['nombreCampo'].'"> <i></i>'.$respuesta->texto.'</label>';
                            }
                    echo '</div>';
            }
        echo '<div class="row" style="text-align:right;margin-right: 5px;">
                                <a href="#" class="myButton btn btn-success">Next</a>
                        </div></div>';
    }
    
if($pregunta->tipo_presentacion=='P'){
    $SQL = "SELECT description FROM question_header WHERE id_question_header ='".$pregunta->id_question_header."'";
    $description= Yii::app()->db->createCommand($SQL)->queryAll();
    echo  "<div class='col-sm-6 txtQuestion $tplQuestion' style='text-align:center;'>";
    echo "<script>marcaradio=1;</script>";
    
    if(strlen($pregunta->ruta_audio)>0){
       echo '<audio controls autoplay controlsList="nodownload" style="margin-top:5px;">
                <source src="'.$pregunta->ruta_audio.'" type="audio/ogg" >
                <source src="'.$pregunta->ruta_audio.'" type="audio/mpeg">
                Your browser does not support the audio element.
            </audio>';
    }
    
    if(strlen($pregunta->ruta_imagen)>0){
        echo  "<br/><img src='".Yii::app()->baseUrl."/images/questions/tn3_".$pregunta->ruta_imagen."'><br><br>";
    }
    $modResPreg = Answers::model()->findAllByAttributes(array('id_pregunta'=>$parametros['id_pregunta']));
    shuffle($modResPreg);
    $col=1;
    $box = '<span class="dragdrop-target">
                <img src="'.Yii::app()->baseUrl.'/images/answers/tn2_0-dd.png" class="img-responsive dragdrop" data-id="0" width="45" height="20" >
            </span>
                ';
    echo "</div>
<div style='vertical-align:middle; padding-left:10px;' class='col-sm-6 respuesta $tplAnswer'>";
    echo "<h3 style='margin-top: 13px; margin-bottom: 20px;font-weight:400 !important;'>".$description[0]["description"]."</h3>";
    $question = str_replace("DRAG_DROP",$box,$pregunta->texto);
    echo  $question; 
   echo "<div class='row'>";
            $i = 0;
           foreach($modResPreg as $respuesta){
            //image answer
            if(strlen($respuesta->ruta_imagen)>0){ 
                $i = $i+1;
                echo '<span class="col-md-3">
                    <div class="dragdrop-target">
                        <img src="'.Yii::app()->baseUrl.'/images/answers/tn2_'.$respuesta->ruta_imagen.'" class="img-responsive dragdrop" data-id="'.$i.'">
                    </div>
                </span>';
            }
                            
                            
            }
        echo '</div><div class="row" style="text-align:right;margin-right: 5px;">
                                <a href="#" class="myButton btn btn-success">Next</a>
                        </div>
                        ';
    
    
    
}
   
    
    
echo '</div><script>function do_response(){debugger;$("#response").val(1);}</script>';
?>
<script>
    function checkno(numpreg){
        var selector = "#preguntas_"+numpreg+"_slider span";
        var selspan = "#amt_"+numpreg;
        var selimg = "#img_"+numpreg;
        $(selector).css("left","0%");
        $(selspan).html("0%");
        
        
    }
    
    $(".range_inp").change(function(){
        id = this.id;
        var res = id.split("_");
        controller = res[0];
        total = 0;
        
            
        controller_2 = parseInt($("#2_"+ res[1]).val());
        controller_1 = parseInt($("#1_"+ res[1]).val());
        total = (controller_2 + controller_1)/2;
        $('#preguntas_'+res[1]).val(total);
        $('#amt_'+res[1]).html(total+'%');
        $('#img_'+res[1]).attr('src', "images/circular-shape-silhouette.png");
        //$('p').css('background-color', '#000').css('color', '#fff');
    
        
    });
    
    $(function () {
        $('.range_inp').val(0);
    
    });
    
    
    $('body').bind('contextmenu', function(e) {
        return false;
    }); 
  
    
   
    
</script>
<?php
/* @var $this AnswersExamUserController */
/* @var $model AnswersExamUser */

$this->breadcrumbs=array(
	'Answers Exam Users'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create AnswersExamUser', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#answers-exam-user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Answers Exam Users</h1>




<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'answers-exam-user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_respuesta_examen_usuario',
		'id_examen_usuario',
		'id_pregunta',
		'id_respuesta',
		'escala',
		array(
			'class'=>'CButtonColumn',
                    'template'=>'{delete}{update}',
                    'buttons'=>array(
                        'delete'=>array(
                            'label'=>'Delete&nbsp;',
                            'imageUrl'=>''
                        ),
                        'update'=>array(
                            'label'=>'Edit&nbsp;',
                            'imageUrl'=>''
                        )
                    )
		),
	),
)); ?>

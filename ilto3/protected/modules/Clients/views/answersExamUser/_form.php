<?php
/* @var $this AnswersExamUserController */
/* @var $model AnswersExamUser */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'answers-exam-user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_examen_usuario'); ?>
		<?php echo $form->textField($model,'id_examen_usuario'); ?>
		<?php echo $form->error($model,'id_examen_usuario'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_pregunta'); ?>
		<?php echo $form->textField($model,'id_pregunta'); ?>
		<?php echo $form->error($model,'id_pregunta'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_respuesta'); ?>
		<?php echo $form->textField($model,'id_respuesta'); ?>
		<?php echo $form->error($model,'id_respuesta'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'escala'); ?>
		<?php echo $form->textField($model,'escala'); ?>
		<?php echo $form->error($model,'escala'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
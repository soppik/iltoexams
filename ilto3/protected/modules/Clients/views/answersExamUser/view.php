<?php
/* @var $this AnswersExamUserController */
/* @var $model AnswersExamUser */

$this->breadcrumbs=array(
	'Answers Exam Users'=>array('index'),
	$model->id_respuesta_examen_usuario,
);

$this->menu=array(
	array('label'=>'List AnswersExamUser', 'url'=>array('index')),
	array('label'=>'Create AnswersExamUser', 'url'=>array('create')),
	array('label'=>'Update AnswersExamUser', 'url'=>array('update', 'id'=>$model->id_respuesta_examen_usuario)),
	array('label'=>'Delete AnswersExamUser', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_respuesta_examen_usuario),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AnswersExamUser', 'url'=>array('admin')),
);
?>

<h1>View AnswersExamUser #<?php echo $model->id_respuesta_examen_usuario; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_respuesta_examen_usuario',
		'id_examen_usuario',
		'id_pregunta',
		'id_respuesta',
		'escala',
	),
)); ?>

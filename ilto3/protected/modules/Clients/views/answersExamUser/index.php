<?php
/* @var $this AnswersExamUserController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Answers Exam Users',
);

$this->menu=array(
	array('label'=>'Create AnswersExamUser', 'url'=>array('create')),
	array('label'=>'Manage AnswersExamUser', 'url'=>array('admin')),
);
?>

<h1>Answers Exam Users</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

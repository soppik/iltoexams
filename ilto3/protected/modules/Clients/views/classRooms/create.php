<?php
/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = $session['profile'];


if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}

if($profile == 6){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}

/* session manager
end
*/

/* @var $this ClassRoomsController */
/* @var $model ClassRooms */

$this->breadcrumbs=array(
	'Class Rooms'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Test Rooms', 'url'=>array('admin')),
);
$this->layout = "//layouts/column2a";
?>

<?php //$this->renderPartial('_form', array('model'=>$model)); ?>
<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
	
                                    <div class="widget radius-bordered">
                                        <div class="widget-header bg-blue">
                                            <span class="widget-caption">Create Test rooms</span>
                                        </div>
                                        <div class="widget-body">
                                            <form  id="class-rooms-form" action="/ilto3/index.php?r=Clients/classRooms/create" method="post" class="form-horizontal bv-form" data-bv-message="This value is not valid" data-bv-feedbackicons-valid="glyphicon glyphicon-ok" data-bv-feedbackicons-invalid="glyphicon glyphicon-remove" data-bv-feedbackicons-validating="glyphicon glyphicon-refresh" novalidate="novalidate">
                                            	<button type="submit" class="bv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
                                               
                                                
                                                <div class="form-group has-feedback">
                                                    <label class="col-lg-2 control-label">Test Room Name *</label>
                                                    <div class="col-lg-8">
                                                        <input required=required maxlength="200" size="50" type="text" class="form-control" name="ClassRooms[nombre]" id="ClassRooms_nombre"   data-bv-field="ClassRooms[nombre]">

                                                    </div>
                                                </div>
                                                <div class="form-group has-feedback">
                                                    <label class="col-lg-2 control-label">Location *</label>
                                                    <div class="col-lg-8">
                                                        <input required=required maxlength="250" size="60" type="text" class="form-control" name="ClassRooms[ubicacion]" id="ClassRooms_ubicacion"   data-bv-field="ClassRooms[ubicacion]">
                                                      
                                                    </div>
                                                </div>
                                                <div class="form-group has-feedback">
                                                    <label class="col-lg-2 control-label">Class Size *</label>
                                                    <div class="col-lg-8">
                                                        <input required=required maxlength="4" size="4" type="number" class="form-control" name="ClassRooms[capacidad]" id="ClassRooms_capacidad"   data-bv-field="ClassRooms[capacidad]">
                                                   
                                                    </div>
                                                </div>
                                                

                                                <div class="form-group">
                                                    <div class="col-lg-offset-1 col-lg-8">
                                                        <input class="btn btn-palegreen" type="submit"  name="yt0" value="Create">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                      
                                </div>
</div>
	
	
	
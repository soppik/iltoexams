<?php

/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = $session['profile'];


if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}

if($profile == 6){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}

/* session manager
end
*/


/* @var $this ClassRoomsController */
/* @var $model ClassRooms */

$this->breadcrumbs=array(
	'Test Rooms'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Create Test Rooms', 'url'=>array('create')),
	array('label'=>'Manage Test Rooms', 'url'=>array('admin')),
);
$this->layout = "//layouts/column2a";
?>

<?php //$this->renderPartial('_form', array('model'=>$model)); ?>

<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
	
                                    <div class="widget radius-bordered">
                                        <div class="widget-header bg-blue">
                                            <span class="widget-caption">Update Test rooms <?php echo $model->id; ?></span>
                                        </div>
                                        <div class="widget-body">
                                            <form  id="class-rooms-form" action="/ilto3/index.php?r=Clients/classRooms/update&id=<?php echo $link = base64_encode($model->id);?>" method="post" class="form-horizontal bv-form" data-bv-message="This value is not valid" data-bv-feedbackicons-valid="glyphicon glyphicon-ok" data-bv-feedbackicons-invalid="glyphicon glyphicon-remove" data-bv-feedbackicons-validating="glyphicon glyphicon-refresh" novalidate="novalidate">
                                            	<button type="submit" class="bv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
                                               
                                                
                                                <div class="form-group has-feedback">
                                                    <label class="col-lg-4 control-label">Group Name *</label>
                                                    <div class="col-lg-8">
                                                        <input required=required maxlength="50" value="<?php echo $model->nombre ?>" size="50" type="text" class="form-control" name="ClassRooms[nombre]" id="ClassRooms_nombre"   data-bv-field="ClassRooms[nombre]">

                                                    </div>
                                                </div>
                                                <div class="form-group has-feedback">
                                                    <label class="col-lg-4 control-label">Location *</label>
                                                    <div class="col-lg-8">
                                                        <input required=required maxlength="250" size="60" type="text" value="<?php echo $model->ubicacion ?>" class="form-control" name="ClassRooms[ubicacion]" id="ClassRooms_ubicacion"   data-bv-field="ClassRooms[ubicacion]">
                                                      
                                                    </div>
                                                </div>
                                                <div class="form-group has-feedback">
                                                    <label class="col-lg-4 control-label">Class Size *</label>
                                                    <div class="col-lg-8">
                                                        <input required=required maxlength="4" size="4" type="number" value="<?php echo $model->capacidad ?>" class="form-control" name="ClassRooms[capacidad]" id="ClassRooms_capacidad"   data-bv-field="ClassRooms[capacidad]">
                                                   
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group has-feedback">
                                                    <label class="col-lg-4 control-label">Status *</label>
                                                    <div class="col-lg-8">
		                                                <select id="combo_estado" name="ClassRooms[estado]">
																<option value=""></option>
																<option value="A" <?php echo ($model->estado=='A')?'selected="selected"':'' ?>>Enabled</option>
																<option value="I" <?php echo ($model->estado=='I')?'selected="selected"':'' ?>>Disabled</option>
														</select>
													</div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <div class="col-lg-offset-4 col-lg-8">
                                                        <input class="btn btn-palegreen" type="submit"  name="yt0" value="Save">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                      
                                </div>
</div>
	
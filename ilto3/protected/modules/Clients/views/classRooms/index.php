<?php
/* @var $this ClassRoomsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Test Rooms',
);

$this->menu=array(
	array('label'=>'Create Test Rooms', 'url'=>array('create')),
	array('label'=>'Manage Test Rooms', 'url'=>array('admin')),
);
?>

<h1>Test Rooms</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

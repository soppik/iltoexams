<?php

/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = $session['profile'];


if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}

if($profile == 6){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}

/* session manager
end
*/



/* @var $this ClassRoomsController */
/* @var $model ClassRooms */
$this->layout = "//layouts/column2a";
$this->breadcrumbs=array(
	'Class Rooms'=>array('index'),
	'Manage',
);
$visible = (Yii::app()->user->getState('id_perfil') < 4) ? 'true':'false';
if($visible=='true'){
$this->menu=array(
	array('label'=>'Create Test rooms', 'url'=>array('create')),
);
}

/*Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#class-rooms-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");*/

$config = Yii::app()->getComponents(false);
$connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
// establish connection. You may try...catch possible exceptions
$connection->active=true;

$SQL = "SELECT * FROM `class_rooms` `t` WHERE id_cliente='".Yii::app()->user->getState('cliente')."' order by id DESC LIMIT 50";
$list= $connection->createCommand($SQL)->queryAll();

// agregamos al log del usuario
$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 4, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'Access Test room list', '')";
$log= Yii::app()->db->createCommand($SQL)->query(); 


?>



<div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="widget">
                                <div class="widget-header ">
                                    <span class="widget-caption">Manage Test rooms</span>
                                    <a href="/ilto3/index.php?r=Clients/classRooms/create" class="btn btn-success">Create Test rooms</a>
                                </div>
                                <div class="widget-body">
                                    <form class="search-form" action="/ilto3/index.php?r=Api/room" onsubmit="return false">
                                        
                                    <input type="hidden" value="<?php echo Yii::app()->user->getState('cliente') ?>" name="id_cliente">
                                    <table class="table table-striped table-hover table-bordered" id="editabledatatable">
                                        <thead>
                                            <tr role="row">
                                                <th>
                                                    NAME
                                                    <br>
                                                    <input type="text" name="description" class="search form-control" data-related="class_rooms.nombre"/>
                                                </th>
                                                <th>
                                                    LOCATION
                                                    <br>
                                                    <input type="text" name="ubicacion" class="search form-control" data-related="class_rooms.ubicacion"/>
                                                </th>
                                                <th>
                                                    CLASS SIZE
                                                    <input type="text" name="capacidad" class="search form-control" data-related="class_rooms.capacidad"/>
                                                </th>
                                                <th>
                                                    STATUS
                                                    <br>
                                                    <select class="form-control search" name="estado" data-related="class_rooms.estado">
                                                        <option value="">
                                                            
                                                        </option>
                                                        <option value="A">Enable</option>
                                                        <option value="I">Disable</option>
                                                    </select>
                                                </th>
                                                <th>
                                                    
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody class="search-rows">
                                            
                                            <?php 
                                                foreach($list as $rooms){
                                                    ?>
                                                        <tr class="<?php echo $rooms['id']?>">
                                                            <td><?php echo utf8_encode($rooms['nombre'])?></td>
                                                            <td><?php echo utf8_encode($rooms['ubicacion'])?></td>
                                                            <td class="center "><?php echo $rooms['capacidad']?></td>
                                                            <td class="center "><?php echo ($rooms['estado']=='A')?'ENABLED':'DISABLED' ?></td>
                                                            <td>
                                                                <?php 
                                                                $link = base64_encode($rooms['id']);?>
                                                                <a href="/ilto3/index.php?r=Clients/classRooms/update&id=<?php echo $link?>" class="btn btn-info btn-xs edit"><i class="fa fa-edit"></i> Edit</a>
                                                                <?php 
                                                                    $SQL = "SELECT COUNT(*) as total FROM `exams_user` WHERE `salon`='".$rooms['id']."' ";
                                                                
                                                                    $LicensesCount = $connection->createCommand($SQL)->queryAll();
                                                                    
                                                                    if($LicensesCount[0]['total']==0){
                                                                ?>
                                                                
                                                                <a href="/ilto3/index.php?r=Clients/classRooms/delete&id=<?php echo $link?>" value="<?php echo $link?>" class="btn btn-danger btn-xs delete"><i class="fa fa-trash-o"></i> Delete</a>
                                                                <?php 
                                                                    }
                                                                ?>
                                                               
                                                            </td>
                                                        </tr>
                                                    <?php
                                                }
                                            ?>
                                            
                                            
                                            
                                        </tbody>
                                    </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                                    <div class="widget">
                                        <div class="widget-header bg-blue">
                                            <i class="widget-icon fa fa-arrow-left"></i>
                                            <span class="widget-caption">TEST ROOMS INSTRUCTIONS </span>
                                           
                                        </div><!--Widget Header-->
                                        <div class="widget-body">
                                            <div>
                                                Add and name test rooms where candidates would take the test.	
                                            </div>
                                        </div><!--Widget Body-->
                                    </div><!--Widget-->
                                </div>
                                
                       
                    </div>


   <!--Page Related Scripts-->
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/jquery.dataTables.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/ZeroClipboard.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.tableTools.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.bootstrap.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/datatables-init.js"></script>
    <script>
        InitiateSimpleDataTable.init();
        InitiateExpandableDataTable.init();
        InitiateSearchableDataTable.init();
        
        

    </script>
    <script>
jQuery(document).on('click', '.btn.btn-danger.btn-xs.delete',function(e){
    e.preventDefault();
        var url = jQuery(this).attr('href')
        var id = $(this).attr('value')
        jQuery.post(url, { id: $(this).attr('value')}, function(data){
             console.log(data)
             var cl = '.';
             var clas = cl.concat(id)
             jQuery(clas).remove()
        })
})
    
</script>


<?php 

/*
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'class-rooms-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'nombre',
		'ubicacion',
		'capacidad',
                array(
                    'name'=>'estado',
                    'value'=>'($data->estado=="A" ? "Enabled" : "Disabled")',
                    'filter'=>array("A"=>"Enable","I"=>"Disable"),
                    ),
		array(
			'class'=>'CButtonColumn',
                    'template'=>'{update}{delete}',
                    'buttons'=>array(
                        'delete'=>array(
                            'label'=>'Delete',
                            'imageUrl'=>NULL,
                            'visible'=>'$data->seElimina() && '.$visible
                        ),
                        'update'=>array(
                            'label'=>'Edit',
                            'imageUrl'=>NULL,
                            'visible'=>$visible
                        )
                    )
                    
		),
	),
)); ?>
<script>
        $(".tool_tip").mouseover(function(){
           eleOffset = $(this).offset();
          $(this).next().fadeIn("fast").css({
                                  left: eleOffset.left + $(this).outerWidth(),
                                  top: eleOffset.top
                          });
          }).mouseout(function(){
                  $(this).next().hide();
          });

    </script>
    */ ?>
<?php
/* @var $this FraudReportController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Fraud Reports',
);

$this->menu=array(
	array('label'=>'Create FraudReport', 'url'=>array('create')),
	array('label'=>'Manage FraudReport', 'url'=>array('admin')),
);
?>

<h1>Fraud Reports</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

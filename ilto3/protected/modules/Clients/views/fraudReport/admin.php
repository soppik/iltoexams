<?php
/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = $session['profile'];


if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}

if($profile == 6){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}

/* session manager
end
*/

// agregamos al log del usuario access
$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 4, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'Access Fraud Alert', '')";
$log= Yii::app()->db->createCommand($SQL)->query(); 

/* @var $this FraudReportController */
/* @var $model FraudReport */

$this->breadcrumbs=array(
	'Fraud Alert'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create TECS Fraud Report', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#fraud-report-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

$modUtil = new Utilidades();
$types = $modUtil->arregloTiposFraude();

$config = Yii::app()->getComponents(false);
$connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
// establish connection. You may try...catch possible exceptions
$connection->active=true;

if(isset($_REQUEST['search_box'])){
    $SQL = "SELECT * FROM `fraud_report`, clients WHERE fraud_report.id_client = clients.id_cliente and (fraud_report.`document` LIKE '%".$_REQUEST['search_box']."%' OR first_name LIKE '%".$_REQUEST['search_box']."%' OR sure_name LIKE '%".$_REQUEST['search_box']."%' )";
    $list_u= $connection->createCommand($SQL)->queryAll();    
}else{
//    $SQL = "SELECT * FROM `fraud_report`, clients WHERE fraud_report.id_client = clients.id_cliente and fraud_report.`id_client` LIKE '".Yii::app()->user->getState('cliente')."'";
    $SQL = "SELECT * FROM `fraud_report` ORDER BY report_date DESC LIMIT 50";
    $list_u= $connection->createCommand($SQL)->queryAll();
}



?>
<style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
    /* display: none; <- Crashes Chrome on hover */
    -webkit-appearance: none;
    margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
}
    
</style>

<form action="<?php echo Yii::app()->request->requestUri;?>" method="POST">
    <input class="search_box" name="search_box" type="text" id="search_box" style="width:350px;" value="<?php echo $busca;?>">
    <input name="btnSearch" type="submit" value="Search"> <a class="btn btn-success btn-xs edit" target="_blank" href="https://iltoexams.com/fraud-alert.php" style="margin-left:25px;">CERTIFICATES VALIDATION</a>  
    <div class="instrucciones">Enter the ID number of the test taker. </div>
      
</form>

<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
                                    <div class="widget">
                                        <div class="widget-header bg-blue">
                                            <i class="widget-icon fa fa-arrow-left"></i>
                                            <span class="widget-caption">WARNING</span>
                                           
                                        </div><!--Widget Header-->
                                        <div class="widget-body">
                                            <div>
                                             This is a list of test takers under fraud alert investigations performed by the Fraud Prevention Office. If the test taker is found to have made fraud during the test, the certificate is counterfeited or the Office detects an identity theft, the person will be banned from taking the test for the lifetime in any location around the world. Be advised that ILTO Exams is a United States based company and is protected by the FBI (Federal Bureau of Investigations) identity theft protection act, and actions may be taken to protect the security and reliability of our products.    </div>
                                        </div><!--Widget Body-->
                                    </div><!--Widget-->
                                </div>
                                
                       
                    </div>
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="widget">
                                <div class="widget-header ">
                                    <span class="widget-caption">TECS Fraud Alert</span>
                                    <a href="/ilto3/index.php?r=Clients/fraudReport/create" class="btn btn-success">Create TECS Fraud Report</a>
                                </div>
                                <div class="widget-body">
                                    <table class="table table-striped table-hover table-bordered" id="editabledatatable">
                                        <thead>
                                            <tr role="row" style="text-align:center;">
                                                <th>
                                                    ID NUMBER
                                                </th>
                                                <th>
                                                    NAME
                                                </th>
                                                <th>
                                                    SURNAME
                                                </th>
                                                <th>
                                                    CLIENT
                                                </th>
                                                <th>
                                                    COUNTRY
                                                </th>
                                                <th>
                                                    REPORT DATE
                                                </th>
                                                 <th>
                                                    FRAUD TYPE
                                                </th>
                                                <th>
                                                    COMMENTS
                                                </th>
                                              
                                            </tr>
                                        </thead>

                                        <tbody>
                                            
                                            <?php 
                                                foreach($list_u as $rooms){
                                                    $SQL = 'SELECT nombres, id_cliente FROM `users_c` where id_usuario_c ="'.$rooms['document'].'" ';
                                                    $list_cl= $connection->createCommand($SQL)->queryAll();
                                                    
                                                    $SQL = 'SELECT cl.nombre_rsocial, co.Name FROM `clients` cl, `country` co WHERE id_cliente ="'.$list_cl[0]['id_cliente'].'" AND cl.id_country = co.Code COLLATE utf8_unicode_ci';
                                                    $list_com= $connection->createCommand($SQL)->queryAll();
                                                      //
                                                    ?>
                                                        <tr>
                                                            <td><?php echo $rooms['document']?></td>
                                                            <td><?php echo  utf8_encode($rooms['first_name'])?></td>
                                                            <td><?php echo  utf8_encode($rooms['sure_name'])?></td>
                                                            <td><?php echo utf8_encode($list_com[0]['nombre_rsocial'])?> </td>
                                                            <td><?php echo $list_com[0]['nombre']?> </td>
                                                           <td><?php echo $rooms['report_date']?></td>
                                                            <td class="center "><?php echo $types[$rooms['type']]?></td>
                                                            
                                                            <td class="center "><?php echo utf8_encode($rooms['comments'])?></td>

                                                            
                                                        </tr>
                                                    <?php
                                                }
                                            ?>
                                            
                                            
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                        


   <!--Page Related Scripts-->
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/jquery.dataTables.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/ZeroClipboard.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.tableTools.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.bootstrap.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/datatables-init.js"></script>
    <script>
        InitiateSimpleDataTable.init();
        InitiateExpandableDataTable.init();
        InitiateSearchableDataTable.init();
    </script>
<?php 
/*
//echo "Searching...".$busca;
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'fraud-report-grid',
	'dataProvider'=>$model->search($busca),
	'filter'=>$model,
	'columns'=>array(
		'document',
		'first_name',
		'sure_name',
            array(
                'name'=>'country',
                'value'=>'$data->nombrePais()',
                'filter'=>CHtml::listData(Countries::model()->findAll(),"id_pais","nombre"),
            ),
            array(
                'name'=>'type',
                'filter'=>$types,
                'value'=>'$data->nombreTipo()',
            ),
		'report_date',

	),
));*/
?>

<?php
/* session manager
start
*/
$session=new CHttpSession;
$session->open();
$profile = $session['profile'];


if($session['user'] == null || $session=''){
    $this->redirect('index.php?r=Clients/default/login&message=11');
}

if($profile == 6){
    $this->redirect('index.php?r=Clients/default/login&message=12');
}

/* session manager
end
*/

/* @var $this ClassRoomsController */
/* @var $model ClassRooms */

$this->breadcrumbs=array(
	'Class Rooms'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Fraud Reports', 'url'=>array('admin')),
);
$this->layout = "//layouts/column2a";
?>

<?php //$this->renderPartial('_form', array('model'=>$model)); ?>
<style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
    /* display: none; <- Crashes Chrome on hover */
    -webkit-appearance: none;
    margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
    }
    
	#alertG_lbl {
		color:#53a93f;
	}
	#alertR_lbl {
		color:#ee5959;
	}

    
</style>
<div class="row">
	<div class="col-lg-12 col-sm-12 col-xs-12">
	
                                    <div class="widget radius-bordered">
                                        <div class="widget-header bg-blue">
                                            <span class="widget-caption">Create TECS Fraud Report</span>
                                        </div>
                                        <div class="widget-body">
                                            <form  id="class-rooms-form" action="/ilto3/index.php?r=Clients/fraudReport/create" method="post" class="form-horizontal bv-form" data-bv-message="This value is not valid" data-bv-feedbackicons-valid="glyphicon glyphicon-ok" data-bv-feedbackicons-invalid="glyphicon glyphicon-remove" data-bv-feedbackicons-validating="glyphicon glyphicon-refresh" novalidate="novalidate">
                                            	<button type="submit" class="bv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
                                               
                                                
                                                <div class="form-group has-feedback">
                                                    <label class="col-lg-4 control-label">ID NUMBER: *</label>
                                                    <div class="col-lg-6">
                                                        <input required=required maxlength="200" size="50" type="number" class="form-control" name="FraudReport[id_usuario_c]" id="FraudReport_id_usuario_c"   data-bv-field="FraudReport[id_usuario_c]">
                                                    </div>
                                                </div>  
                                                <div id="id_validate">
                                                    
                                                </div>  
                                                <input class="form-control" type="hidden" value="15" id="combo_examen" name="FraudReport[exam]">
												<div class="form-group has-feedback">
                                                    <label class="col-lg-4 control-label">Fraud Type *</label>
                                                    <div class="col-lg-6">
                                                        <select name="FraudReport[type]" id="FraudReport_type">
															<option value="">Select type</option>
															<option value="6">Impersonation</option>
															<option value="7">Use of HDMI Cable</option>
															<option value="8">Use of notes during the test</option>
															<option value="9">Assistance by a third party</option>
															<option value="10">Incoherent scores not supported</option>
															<option value="5">Other</option>
														</select>
                                                    </div>
                                                </div><div class="form-group has-feedback">
                                                    <label class="col-lg-4 control-label">Comments *</label>
                                                    <div class="col-lg-6">
                                                        <textarea rows="6" style="max-width:515px" class="form-control" name="FraudReport[comments]" id="FraudReport_comments"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-lg-offset-1 col-lg-8">
                                                        <input class="btn btn-palegreen" type="submit"  name="yt0" value="Create"> 
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                      
                                </div>
</div>
<script>	

$("#FraudReport_id_usuario_c").change(function() {
		var id = $("#FraudReport_id_usuario_c").val();
		$.ajax({
			url : "/ilto3/index.php?r=Api/userToReport",
			type: "POST",
			dataType: "text",
			data: {
			    id: id
			},
			success: function(data) {
				$("#id_validate").html(data);
			}
		});
});
$(function(){
    $('#UsersC_numero_id').keypress(function(e){
	    if(e.which == 101){
		    return false;
		}
	});
});
</script>	
	
<?php
/* @var $this FraudReportController */
/* @var $model FraudReport */
/* @var $form CActiveForm */
$modUtil= new Utilidades();
$types = $modUtil->arregloTiposFraude();

?>


<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'fraud-report-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>


	<?php echo $form->errorSummary($model); ?>
    
     <div class="form-group">
                        
                        	<?php echo $form->label($model,'document',['label'=>'Select the Taker to report:']); ?>
                        
                        
		<?php
            $modeloCombo = CHtml::listData(UsersC::model()->resetScope()->takers()->findAll(),'id_usuario_c','NombresapellidosId');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'FraudReport[document]',
            'id'=> 'combo_doc',
            'data'=>$modeloCombo,
                'value'=>$model->document,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'450px',
                'height'=>'33px',
            ),
             'htmlOptions' => array(
                 'class'=>'form-control'
                 )
          ));
            ?>
		<?php echo $form->error($model,'document'); ?>
                    </div>
                    


	<div class="form-group">
		<?php echo $form->labelEx($model,'exam'); ?>
		<?php
            $modeloLicencia = CHtml::listData(Licenses::model()->forsale()->findAll(),'id_licencia','nombre');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'FraudReport[exam]',
            'id'=> 'combo_examen',
            'data'=>$modeloLicencia,
                'value'=>$model->exam,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'450px',
                'height'=>'33px',
            ),
             'htmlOptions' => array(
                 'class'=>'form-control'
                 )
          ));
            ?>
		<?php echo $form->error($model,'exam'); ?>
	</div>
    	<div class="form-group">
		<?php echo $form->labelEx($model,'type'); ?>
        <?php echo $form->dropDownList($model,'type', $types, array('prompt'=>'Select type', )); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'comments'); ?>
		<?php echo $form->textArea($model,'comments',array('rows'=>6, 'style'=>'width:450px','class'=>'form-control')); ?>
		<?php echo $form->error($model,'comments'); ?>
	</div>


	<div class="form-group">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>
<script> 
$('#combo_doc').on('change',function(){
    
});
</script>
</div><!-- form -->
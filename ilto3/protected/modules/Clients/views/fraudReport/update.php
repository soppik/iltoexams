<?php
/* @var $this FraudReportController */
/* @var $model FraudReport */

$this->breadcrumbs=array(
	'Fraud Reports'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Create FraudReport', 'url'=>array('create')),
	array('label'=>'Manage FraudReport', 'url'=>array('admin')),
);
?>

<h1>Update FraudReport <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
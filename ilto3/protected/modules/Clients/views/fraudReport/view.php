<?php
/* @var $this FraudReportController */
/* @var $model FraudReport */

$this->breadcrumbs=array(
	'Fraud Reports'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List FraudReport', 'url'=>array('index')),
	array('label'=>'Create FraudReport', 'url'=>array('create')),
	array('label'=>'Update FraudReport', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete FraudReport', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FraudReport', 'url'=>array('admin')),
);
?>

<h1>View FraudReport #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'document',
		'first_name',
		'sure_name',
		'email',
		'id_client',
		'picture',
		'id_user_report',
		'name_user_report',
		'report_date',
		'comments',
		'exam',
	),
)); ?>

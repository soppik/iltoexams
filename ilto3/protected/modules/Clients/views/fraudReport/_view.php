<?php
/* @var $this FraudReportController */
/* @var $data FraudReport */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('document')); ?>:</b>
	<?php echo CHtml::encode($data->document); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_name')); ?>:</b>
	<?php echo CHtml::encode($data->first_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sure_name')); ?>:</b>
	<?php echo CHtml::encode($data->sure_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_client')); ?>:</b>
	<?php echo CHtml::encode($data->id_client); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('picture')); ?>:</b>
	<?php echo CHtml::encode($data->picture); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('id_user_report')); ?>:</b>
	<?php echo CHtml::encode($data->id_user_report); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_user_report')); ?>:</b>
	<?php echo CHtml::encode($data->name_user_report); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('report_date')); ?>:</b>
	<?php echo CHtml::encode($data->report_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comments')); ?>:</b>
	<?php echo CHtml::encode($data->comments); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('exam')); ?>:</b>
	<?php echo CHtml::encode($data->exam); ?>
	<br />

	*/ ?>

</div>
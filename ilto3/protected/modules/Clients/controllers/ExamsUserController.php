<?php

class ExamsUserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('viewDay','printDay','schedule', 'tieneTutor','create','createFuaa','update','admin','delete'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

        public function actionTieneTutor($id){
            $modUtil = new Utilidades();
            echo $modUtil->tieneTutor($id);
        }
        
        public function actionPrintDay($day,$month,$year,$room,$time){
            
            //close taker vulnerability
        	$profile = Yii::app()->user->getState('id_perfil');
            if($profile == 6){
			    $this->redirect('index.php?r=Clients/default/login&message=12');
			}
			
			//traemos las licencias TECS
			$modUtil = new Utilidades();
			$tecs_ids = $modUtil->getIdsEde();
           
            if(!isset($room)){
                $room = $_REQUEST['room'];
            }
            
            //get ID ROOM
            $SQL = "SELECT * FROM `class_rooms` WHERE `id_cliente` = '".Yii::app()->user->getState('cliente')."' AND `nombre` LIKE '%TECS2GO%'  ";
            $id_room_tecs2go = Yii::app()->db->createCommand($SQL)->queryAll();
            
             
            if(!isset($day)){
                $day = $_REQUEST['day'];
            }
            
            if(!isset($month)){
                $month = $_REQUEST['month'];
            }
            
            if(!isset($year)){
                $year = $_REQUEST['year'];
            }
            
            if(!isset($time)){
                $time = $_REQUEST['time'];
            }
            
            
            $nomSalon = ClassRooms::model()->findByPk($room)->nombre;
            
            // ACCESS con GET
            $SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 1, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'Accesss usernames and paswords pdf ID ROOM. ".$_GET['room']."  ".$_GET['day']."-".$_GET['month']."-".$_GET['year']." ".$_GET['time']."', '')";
            $log= Yii::app()->db->createCommand($SQL)->query(); 
            
            
       // require '/home/thetec/public_html/vendor/autoload.php';
            require '/home/iltoexam/public_html/vendor/autoload.php';
					
			$mpdf = new \Mpdf\Mpdf(['tempDir' => __DIR__ . '/../temp']);
			
            $informe='<html><head><style>table.externa{ border-spacing: 10px;width:640px;} td.externa{width:300px;} table.interna{border-width: 3px;border-spacing: 10px;border-style: dashed;border-color: gray;
	border-collapse: collapse;background-color: white; width:300px;} td.interna {border-width: thin; border-style: none;
	border-color: gray; background-color: white; }h2,h3{ text-align:center;}</style></head><body>';
            $informe.= "<h2>USER AND PASSWORD LIST FOR $nomSalon</h2>"; 
            $informe.= "<h3>(MM/DD/AAAA) $month/$day/$year</h3>"; 
            $criteria = new CDbCriteria();
            $criteria->addCondition("month(fecha_presentacion) = $month and year(fecha_presentacion)=$year and day(fecha_presentacion)=$day and salon=$room and hora='$time'");
            $criteria->group = "id_licencia_usuario";
            
            
            
            if(Yii::app()->user->getState('id_perfil')==4){ //Es un tutor, ver solo sus compromisos
            //  q    $criteria->addCondition("tutor = '$usuario'");
            }
            $model = ExamsUser::model()->findAll($criteria);
            
            $informe .= '<table class="externa">';
            $col=0;
            foreach($model as $registro){
            
            //Nombre licencia
            $SQL = "SELECT nombre, id_usuario FROM `licenses_client`
                    INNER JOIN licenses_user
                    ON licenses_client.id_licencias_cliente = licenses_user.id_licencia_cliente
                    INNER JOIN licenses
                    ON licenses_client.id_licencia = licenses.id_licencia
                    WHERE licenses_user.id_licencias_usuario = '".$registro->id_licencia_usuario."' ";
                    
            $licenseName= Yii::app()->db->createCommand($SQL)->queryAll();
            
            $SQL = "SELECT nombres, apellidos, email, telefono FROM `users_c`WHERE id_usuario_c = '".$licenseName[0]["id_usuario"]."' ";
            
            $usr = Yii::app()->db->createCommand($SQL)->queryAll();
            
            $str = explode("V", $licenseName[0]["nombre"]);
            $licName = $str[0];
                if($col==0){
                    $informe .= "<tr>";
                }
               
               // TECS2GO RPDF
                if($room == $id_room_tecs2go[0]["id"]){
                     $informe .= '<td><table class="interna"><tr><td colspan="2" style="font-size:80%;">'.ucfirst($usr[0]["nombres"]).' '.ucfirst($usr[0]["apellidos"]).' please enter <strong>www.iltoexams.com</strong> in your browser, click on the Test Taker button and use the following information:</td></tr>
                    <tr><td colspan="2" style="text-align:center;">&nbsp;&nbsp;&nbsp;</td></tr>
                    <tr><td colspan="2" style="text-align:center;">'.ucfirst($licName).'&nbsp;&nbsp;&nbsp;</td></tr>
                    <tr><td>User:  '.$licenseName[0]["id_usuario"].'</td></tr>
                    <tr><td>Email: '.$usr[0]["email"].'</td></tr>
                    <tr><td>Contact: '.$usr[0]["telefono"].'</td></tr>';
                    
                }else {
                   $informe .= '<td><table class="interna"><tr><td colspan="2" style="font-size:80%;">'.ucfirst($usr[0]["nombres"]).' '.ucfirst($usr[0]["apellidos"]).' please enter <strong>www.iltoexams.com</strong> in your browser, click on the Test Taker button and use the following information:</td></tr>
                    <tr><td colspan="2" style="text-align:center;">&nbsp;&nbsp;&nbsp;</td></tr>
                    <tr><td colspan="2" style="text-align:center;">'.ucfirst($licName).'&nbsp;&nbsp;&nbsp;</td></tr>
                    <tr><td>User:  </td><td>'.$licenseName[0]["id_usuario"].'</td></tr>'; 
                }
                
                    
                $ids = explode(",",$tecs_ids);
                $SQL = "SELECT id_licencia FROM `licenses_client` WHERE id_licencias_cliente = '".$registro->idLicenciaUsuario->id_licencia_cliente."'";
                $type = Yii::app()->db->createCommand($SQL)->queryAll();
                
                foreach($ids as $id){
                    if($id == $type[0]["id_licencia"]){
                        $informe .='<tr><td>Password:</td><td>'.$registro->idLicenciaUsuario->idUsuario->clave2.'</td></tr>';
                    }
                }
                $informe .='</table></td>';
                
                $col++;
                if($col==2) {
                    $col=0;
                    $informe .= "</tr>";
                }
            }
            if($col==1) {
                $informe .= "<td></td></tr>";
            }
            $informe.='</table></body></html>';
            $mpdf->WriteHTML($informe);
			$mpdf->Output();
        }

        public function actionViewDay($day,$month,$year,$room){
            $usuario = Yii::app()->user->getState('usuario');
            $criteria = new CDbCriteria();
            $criteria->addCondition("month(fecha_presentacion) = $month and year(fecha_presentacion)=$year and day(fecha_presentacion)=$day and salon=$room");
            $criteria->group = "id_licencia_usuario";
            if(Yii::app()->user->getState('id_perfil')==4){ //Es un tutor, ver solo sus compromisos
                $criteria->addCondition("tutor = '$usuario'");
            }
            $model = ExamsUser::model()->findAll($criteria);            
            $nomSalon = ClassRooms::model()->findByPk($room)->nombre;
            $this->renderPartial('viewDay',array('model'=>$model, 'day'=>$day,'month'=>$month,'year'=>$year,'room'=>$room,'salon'=>$nomSalon));
        }
        
        public function actionSchedule($mes = NULL,$anio = NULL){
            if(is_null($mes)){
                $mes = date("m");
            }
            if(is_null($anio)){
                $anio = date("Y");
            }
            $mesPost=$mes+1;
            $mesAnte=$mes-1;
            $anioAnte = $anio;
            $anioPost = $anio;
            if($mes==1){
                $mesAnte = 12;
                $anioAnte = date("Y")-1;
            }
            if($mes==12){
                $mesPost = 1;
                $anioPost = $anio+1;
            }
            $usuario = Yii::app()->user->getState('usuario');
            $criteria = new CDbCriteria();
            $criteria->addCondition("month(fecha_presentacion) = $mes and year(fecha_presentacion)=$anio");
            
            $criteria->group = "day(fecha_presentacion), salon";
            if(Yii::app()->user->getState('id_perfil')==4){ //Es un tutor, ver solo sus compromisos
                $criteria->addCondition("tutor = '$usuario'");
            }
           // $model = ExamsUser::model()->findAll($criteria);
		$this->render('schedule',array('model'=>$model,'mes'=>$mes,'anio'=>$anio,'mesAnte'=>$mesAnte,'anioAnte'=>$anioAnte,'mesPost'=>$mesPost,'anioPost'=>$anioPost));
        }
        
        
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

 	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id)
	{   
	    
        $model=new ExamsUser;
		$countLicencias = LicensesUser::model()->countByAttributes(array('id_usuario'=>$id));
		

        if(isset($_POST['ExamsUser'])) {
            
            $sql='SELECT * FROM `fraud_report` WHERE `document` = "'.$id.'"';
            $fraud= Yii::app()->db->createCommand($sql)->queryAll();
            
            $fraud_result = array($fraud[0]['document']);
            if(!is_null($fraud_result[0])){
                Yii::app()->user->setFlash('error', 'The taker has been blocked, due to suspicious fraud actions. Contact info@iltoexams.com for more information.');
                $this->redirect(array('ExamsUser/create/id/'.$id));
            }
            
            $modUtil = new Utilidades();
            $id_tecs = $modUtil->getIdTecs();
            $id_ede =  $modUtil->getIdEde();
            
            if($modUtil->isTecs($_POST['ExamsUser']['id_licencia_usuario']) == true){
               Yii::app()->user->setFlash('error', 'Only TECS2GO are allowed, please contact an ILTO administrator.');
           //     $this->redirect(array('ExamsUser/create/id/'.$id)); 
            }
            
            //verificamos el tipo de licencia que viene del post
            $SQL = "SELECT * FROM `licenses_client` WHERE `id_licencias_cliente` = '".$_POST['ExamsUser']['id_licencia_usuario']."' ";
            $licenseClient= Yii::app()->db->createCommand($SQL)->queryAll();
            $tipo_licencia = $licenseClient[0]["id_licencia"];
            $id_cliente = $licenseClient[0]["id_cliente"];
            
            $model->attributes=$_POST;
            $ruta_foto = CUploadedFile::getInstance($model,'ruta_foto');
            
            //validamos el tipo de licencia para hacer obligatoria la foto
            $sql='SELECT id_licencia FROM `licenses_client` WHERE `id_licencias_cliente` = "'.$_POST['ExamsUser']['id_licencia_usuario'].'"';
            $list= Yii::app()->db->createCommand($sql)->queryAll();
            
            //armamos la fecha de presentacion
            $hora = strtotime($_POST['ExamsUser']['hora']);
            $hora = date("H:i", $hora);
            
            
            $_POST['ExamsUser']['hora'] = $hora; 
            $datos = $_POST['ExamsUser'];
            
		    $now = date('Y/m/d');
		    $mDate = date('Y/m/d', strtotime($datos['fecha_presentacion']));
		    
		    //validaciones de lugar y tiempo
		     if($datos['salon']==""){
		         Yii::app()->user->setFlash('error', 'Please choose a room.');
                $this->redirect(array('ExamsUser/create/id/'.$id));
		     }
		     
		     if($datos['fecha_presentacion']==""){
		         Yii::app()->user->setFlash('error', 'Please choose a presentation date.');
                $this->redirect(array('ExamsUser/create/id/'.$id));
		     }else{
		         
		      if($mDate<$now){
		             Yii::app()->user->setFlash('error', 'Please choose a VALID presentation date.');
                $this->redirect(array('ExamsUser/create/id/'.$id));
		         }
		     }
		     if($datos['hora']==""){
		         Yii::app()->user->setFlash('error', 'Please choose a hour to the exam.');
                $this->redirect(array('ExamsUser/create/id/'.$id));
		     }
		    
		    $splitDate = explode("/", $datos['fecha_presentacion']);
		    $mm = $splitDate[0];
		    $dd = $splitDate[1];
		    $yy = $splitDate[2];
		    $hr= $datos['hora'];
		    
		    $date = $yy."-".$mm."-".$dd." ".$hr;
		    $datos['fecha_presentacion'] = $date;
            
            
            //licencia TECS
            if($tipo_licencia == 23 || $tipo_licencia == $id_tecs || $tipo_licencia == 15){
                
                //validaciones de lugar y tiempo
    		     if($datos['tutor']==""){
    		         Yii::app()->user->setFlash('error', 'Please choose a tutor.');
                    $this->redirect(array('ExamsUser/create/id/'.$id));
    		     }
                
                //validar fotografía
                $extension = ".".strtolower($ruta_foto->getExtensionName());
                
                if(empty($ruta_foto)){
                    Yii::app()->user->setFlash('error', 'Photo is required.');
                    $this->redirect(array('ExamsUser/create/id/'.$id));
                }
                if(is_null($ruta_foto)){
                    Yii::app()->user->setFlash('error', 'Photo is required.');
                    $this->redirect(array('ExamsUser/create/id/'.$id));
                }
                $photoFlag = 1;
                
                $fileZise = $_FILES['ExamsUser']["size"]["ruta_foto"];
		        if($fileZise > 999999){
                    Yii::app()->user->setFlash('error','This file exceed the maximum size allowed (1MB) for the picture.');
                    $this->redirect(array('ExamsUser/create/id/'.$id));
                }
                //extensiones validas
                switch ($extension) {
                    case '.jpg':
                        $photoFlag = 0;
                        break;
                    case '.jpeg':
                        $photoFlag = 0;
                        break;
                    case '.png':
                        $photoFlag = 0;
                        break;
                    default:
                        $photoFlag = 1;
                }
            
                if($photoFlag ==1){
                    Yii::app()->user->setFlash('error', 'The test taker picture was not uploaded, because you are using a invalid ('.$extension.') file. Please use a valid file such as .JPG, .JPEG or .PNG.');
                    $this->redirect(array('ExamsUser/create/id/'.$id));
                }
                
                //verificamos si el usuario tiene licencias asignadas tipo tecs
                //verificar las licencias TECS (x2)
                $SQL = "SELECT id_licencias_usuario, id_licencia_cliente, id_usuario, licenses_user.fecha_asignacion, fecha_presentacion, licenses_user.estado, id_licencia, calificacion FROM `licenses_user` inner join `licenses_client` ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente WHERE id_usuario = '".$id."' AND licenses_client.id_licencia IN (".$tipo_licencia.",15) ORDER BY fecha_presentacion DESC LIMIT 2 ";
                $tecs= Yii::app()->db->createCommand($SQL)->queryAll();
                //modificamos esto
    		    if(count($tecs) == 0){
    		         //generamos y asignamos una contraseña ramdomica para el usuario
                    
                    $pass = $modUtil->generateRandomString(6);
                    
                    $SQL = "UPDATE `users_c` SET `clave`='".md5($pass)."', `clave2`='".$pass."' WHERE `id_usuario_c`='".$id."'";
                    $passUpdate= Yii::app()->db->createCommand($SQL)->query();
                    // si ha pasado todas las validaciones y restricciones,                    
                    self::finalStepToCreate($id, $datos, $ruta_foto);
    		    }
                
                if(count($tecs)>=1){
                    if($tecs[0]["estado"]=='A'){
                        Yii::app()->user->setFlash('error', 'There is a license that is still valid.');
                        $this->redirect(array('ExamsUser/create/id/'.$id));
                    }
                    
                    //validamos asignar licencia 10 dias despues
                    $datetime1 = new Datetime($tecs[0]["fecha_presentacion"]);
                    $today = date("Y-m-d H:i:s");
                    $todayDt = new Datetime($today);
                    $ban_to_date =  date('Y-m-d H:i:s', strtotime("+10 days", strtotime($tecs[0]["fecha_presentacion"])));
                    $interval = date_diff($datetime1, $todayDt);
                	//obtenemos el valor en numero
                	$days = explode("+",$interval->format('%R%a'));
                	
                	/*
                	if($days[1] <= 10){
                	    Yii::app()->user->setFlash('error', 'This test taker will be enabled to take the test on "'.$ban_to_date.'".');
                        $this->redirect(array('ExamsUser/create/id/'.$id));
                	}*/
                }   
                
                if(count($tecs)>=2){
                	//var_dump("después revisamos la diferencia de fechas, si es menor a 6 meses comparamos puntajes para posible ban");die();
                	//buscamos la diferencia de fechas de presentación del examen mas reciente con el anterior
                	$datetime1 = new Datetime($tecs[0]["fecha_presentacion"]);
                	$today = date("Y-m-d H:i:s");
                    $todayDt = new Datetime($today);
                    
                    $interval = date_diff($datetime1, $todayDt);
                	
                	//obtenemos el valor en numero
                	$days = explode("+",$interval->format('%R%a'));
                	
                	//si la calificaicón del examen actual es menor o igual al anterior, penalizamos
                    if($tecs[0]["nivel"] == $tecs[1]["nivel"]){
                        //la penalizacion debe ser de 180 días, por tanto revisamos los días que van
                        if($days[1] <= 180){
                            
                            //estimamos el tiempo del banneo
                            $ban_to_date =  date('Y-m-d H:i:s', strtotime("+180 days", strtotime($tecs[0]["fecha_presentacion"])));
                            //consultamos la fecha actual y comparamos la diferencia con la de banneo
                            
                            
                            Yii::app()->user->setFlash('error', 'The test taker level on the two previous TECS is the same. The test taker has been blocked for 180 days. This test taker will be enabled to retake the TECS on "'.$ban_to_date.'".');
                            $this->redirect(array('ExamsUser/create/id/'.$id));
                        }
                    }   
                }
            
            //generamos y asignamos una contraseña ramdomica para el usuario
            $modUtil = new Utilidades();
            $pass = $modUtil->generateRandomString(6);
            
            $SQL = "UPDATE `users_c` SET `clave`='".md5($pass)."', `clave2`='".$pass."' WHERE `id_usuario_c`='".$id."'";
            $passUpdate= Yii::app()->db->createCommand($SQL)->query();
            // si ha pasado todas las validaciones y restricciones,                    
            self::finalStepToCreate($id, $datos, $ruta_foto);
                
                    
                }elseif($tipo_licencia == $id_ede || $tipo_licencia == 16){
                    
                    //ahora a validar asignacion de EDE
                    $SQL = "SELECT id_licencias_usuario, id_licencia_cliente, id_usuario, licenses_user.fecha_asignacion, fecha_presentacion, licenses_user.estado, id_licencia, calificacion FROM `licenses_user` inner join `licenses_client` ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente WHERE id_usuario = '".$id."' AND licenses_client.id_licencia = '".$tipo_licencia."' ORDER BY fecha_presentacion DESC LIMIT 1 ";
                    $ede= Yii::app()->db->createCommand($SQL)->queryAll();
                    
                    //generar password randomica para ede UVPmx
                    if($id_cliente == '81030525'){
            		    //generamos y asignamos una contraseña ramdomica para el usuario
                        $pass = $modUtil->generateRandomString(6);
                            
                        $SQL = "UPDATE `users_c` SET `clave`='".md5($pass)."', `clave2`='".$pass."' WHERE `id_usuario_c`='".$id."'";
                        $passUpdate= Yii::app()->db->createCommand($SQL)->query();
        		    }
                    
                    if(count($ede) == 0){
        		        self::finalStepToCreate($id, $datos, $ruta_foto);
        		    } 
                    
                    if(count($ede)>=1){
                        if($ede[0]["estado"]=='A'){
                            Yii::app()->user->setFlash('error', 'There is a license that is still valid.');
                            $this->redirect(array('ExamsUser/create/id/'.$id));
                        }
                        
                        
                        
                        //validamos asignar licencia 10 dias despues
                        $datetime1 = new Datetime($ede[0]["fecha_presentacion"]);
                        $today = date("Y-m-d H:i:s");
                        $todayDt = new Datetime($today);
                        $ban_to_date =  date('Y-m-d H:i:s', strtotime("+10 days", strtotime($ede[0]["fecha_presentacion"])));
                        $interval = date_diff($datetime1, $todayDt);
                    	//obtenemos el valor en numero
                    	$days = explode("+",$interval->format('%R%a'));
                    	/*
                    	if($days[1] <= 10){
                    	    Yii::app()->user->setFlash('error', 'This test take will be enable to take the test on "'.$ban_to_date.'".');
                            $this->redirect(array('ExamsUser/create/id/'.$id));
                    	}*/
                    	self::finalStepToCreate($id, $datos, $ruta_foto);
                    } 
                }else{
                //ahora a validar asignacion de EDE
                $SQL = "SELECT id_licencias_usuario, id_licencia_cliente, id_usuario, licenses_user.fecha_asignacion, fecha_presentacion, licenses_user.estado, id_licencia, calificacion FROM `licenses_user` inner join `licenses_client` ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente WHERE id_usuario = '".$id."' AND licenses_client.id_licencia = '".$tipo_licencia."' ORDER BY fecha_presentacion DESC LIMIT 1 ";
                $ede= Yii::app()->db->createCommand($SQL)->queryAll();
                
                if(count($ede) == 0){
    		        self::finalStepToCreate($id, $datos, $ruta_foto);
    		    }
                
                if(count($ede)>=1){
                    if($ede[0]["estado"]=='A'){
                        Yii::app()->user->setFlash('error', 'There is a license that is still valid.');
                        $this->redirect(array('ExamsUser/create/id/'.$id));
                    }
                    
                    //validamos asignar licencia 10 dias despues
                    $datetime1 = new Datetime($ede[0]["fecha_presentacion"]);
                    $today = date("Y-m-d H:i:s");
                    $todayDt = new Datetime($today);
                    $ban_to_date =  date('Y-m-d H:i:s', strtotime("+10 days", strtotime($ede[0]["fecha_presentacion"])));
                    $interval = date_diff($datetime1, $todayDt);
                	//obtenemos el valor en numero
                	$days = explode("+",$interval->format('%R%a'));
                	/*
                	if($days[1] <= 10){
                	    Yii::app()->user->setFlash('error', 'This test take will be enable to take the test on "'.$ban_to_date.'".');
                        $this->redirect(array('ExamsUser/create/id/'.$id));
                	}*/
                	self::finalStepToCreate($id, $datos, $ruta_foto);
                }   
            }
            
               
        } // END VALIDATE POST EXIST
            
            $this->render('create',array(
			'model'=>$model,'id'=>$id
		        ));
    }
	
	
	public function actionCreateFuaa($id)
	{   
	    
        $model=new ExamsUser;
		$countLicencias = LicensesUser::model()->countByAttributes(array('id_usuario'=>$id));
	    
        
        if(isset($_POST['ExamsUser'])) {
            
            //verifico si el ID está reportado por fraude
            $sql='SELECT * FROM `fraud_report` WHERE `document` = "'.$id.'"';
            $fraud= Yii::app()->db->createCommand($sql)->queryAll();
                    
            if(!is_null($fraud[0]['document'])){
                Yii::app()->user->setFlash('error', 'The taker has been blocked, due to suspicious fraud actions. Contact info@iltoexams.com for more information.');
                $this->redirect(array('ExamsUser/create/id/'.$id));
            }
            
            // consulto el ultimo ID del TECS y EDE
            $modUtil = new Utilidades();
            $id_tecs = $modUtil->getIdTecs();
            $id_ede =  $modUtil->getIdEde();
            
            if($modUtil->isTecs($_POST['ExamsUser']['id_licencia_usuario']) == true){
               Yii::app()->user->setFlash('error', 'Only TECS2GO are allowed, please contact an ILTO administrator.');
                //$this->redirect(array('ExamsUser/create/id/'.$id)); 
            }
             
            //verificamos el tipo de licencia que viene del post
            $SQL = "SELECT * FROM `licenses_client` WHERE `id_licencias_cliente` = '".$_POST['ExamsUser']['id_licencia_usuario']."' ";
            $licenseClient= Yii::app()->db->createCommand($SQL)->queryAll();
            $tipo_licencia = $licenseClient[0]["id_licencia"];
            $id_cliente = $licenseClient[0]["id_cliente"];
            
            $model->attributes=$_POST;
            $ruta_foto = CUploadedFile::getInstance($model,'ruta_foto');
            
           //validamos el tipo de licencia para hacer obligatoria la foto
            $sql='SELECT id_licencia FROM `licenses_client` WHERE `id_licencias_cliente` = "'.$_POST['ExamsUser']['id_licencia_usuario'].'"';
            $list= Yii::app()->db->createCommand($sql)->queryAll();
            
            //armamos la fecha de presentacion
            $hora = strtotime($_POST['ExamsUser']['hora']);
            $hora = date("H:i", $hora);
            
            if($_POST['ExamsUser']['salon']==""){
		         Yii::app()->user->setFlash('error', 'Please choose a room.');
                $this->redirect(array('ExamsUser/create/id/'.$id));
		    }
		    if($_POST['ExamsUser']['city']==""){
		         Yii::app()->user->setFlash('error', 'Please choose a City.');
                $this->redirect(array('ExamsUser/create/id/'.$id));
		    }
		    if($_POST['ExamsUser']['period']==""){
		         Yii::app()->user->setFlash('error', 'Please choose a Period.');
                $this->redirect(array('ExamsUser/create/id/'.$id));
		    }
		    if($_POST['program']==""){
		         Yii::app()->user->setFlash('error', 'Please choose a Program.');
                $this->redirect(array('ExamsUser/create/id/'.$id));
		     }
            
            //consultamos el ID de la school
            $sql='SELECT id_school FROM `program_school_city` WHERE `id_program` = '.$_POST["program"].' ORDER BY `id_psc` DESC';
            $list= Yii::app()->db->createCommand($sql)->queryAll();
            $id_school = $list[0]["id_school"];
            
            $_POST['ExamsUser']['hora'] = $hora; 
            $datos = $_POST['ExamsUser'];
            $datos['program'] = $_POST['program'];
            $datos['school'] = $id_school;
            $datos['id_cliente'] = $id_cliente;
            
            
            
		    $now = date('Y/m/d');
		    $mDate = date('Y/m/d', strtotime($datos['fecha_presentacion']));
		    
		    //validaciones de lugar y tiempo
		    
		    if($datos['fecha_presentacion']==""){
		         Yii::app()->user->setFlash('error', 'Please choose a presentation date.');
                $this->redirect(array('ExamsUser/create/id/'.$id));
		    }else{
		         
		      if($mDate<$now){
		             Yii::app()->user->setFlash('error', 'Please choose a VALID presentation date.');
                $this->redirect(array('ExamsUser/create/id/'.$id));
		         }
		     }
		     if($datos['hora']==""){
		         Yii::app()->user->setFlash('error', 'Please choose a hour to the exam.');
                $this->redirect(array('ExamsUser/create/id/'.$id));
		     }
		    
		    $splitDate = explode("/", $datos['fecha_presentacion']);
		    $mm = $splitDate[0];
		    $dd = $splitDate[1];
		    $yy = $splitDate[2];
		    $hr= $datos['hora'];
		    
		    $date = $yy."-".$mm."-".$dd." ".$hr;
		    $datos['fecha_presentacion'] = $date;
            
            //licencia TECS
            //id 23 para licencias tecs aa 6.0
            if($tipo_licencia == 23){
                
                //validar fotografía
                $extension = ".".strtolower($ruta_foto->getExtensionName());
                
                if(empty($ruta_foto)){
                    Yii::app()->user->setFlash('error', 'Photo is required.');
                    $this->redirect(array('ExamsUser/create/id/'.$id));
                }
                if(is_null($ruta_foto)){
                    Yii::app()->user->setFlash('error', 'Photo is required.');
                    $this->redirect(array('ExamsUser/create/id/'.$id));
                }
                $photoFlag = 1;
                
                $fileZise = $_FILES['ExamsUser']["size"]["ruta_foto"];
		        if($fileZise > 999999){
                    Yii::app()->user->setFlash('error','This file exceed the maximum size allowed (1MB) for the picture.');
                    $this->redirect(array('ExamsUser/create/id/'.$id));
                }
                //extensiones validas
                switch ($extension) {
                    case '.jpg':
                        $photoFlag = 0;
                        break;
                    case '.jpeg':
                        $photoFlag = 0;
                        break;
                    case '.png':
                        $photoFlag = 0;
                        break;
                    default:
                        $photoFlag = 1;
                }
            
                if($photoFlag ==1){
                    Yii::app()->user->setFlash('error', 'The test taker picture was not uploaded, because you are using a invalid ('.$extension.') file. Please use a valid file such as .JPG, .JPEG or .PNG.');
                    $this->redirect(array('ExamsUser/create/id/'.$id));
                }
                
                //verificamos si el usuario tiene licencias asignadas tipo tecs
                //verificar las licencias TECS (x2)
                $SQL = "SELECT id_licencias_usuario, id_licencia_cliente, id_usuario, licenses_user.fecha_asignacion, fecha_presentacion, licenses_user.estado, id_licencia, calificacion FROM `licenses_user` inner join `licenses_client` ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente WHERE id_usuario = '".$id."' AND licenses_client.id_licencia IN (".$tipo_licencia.",15) ORDER BY fecha_presentacion DESC LIMIT 2 ";
                $tecs= Yii::app()->db->createCommand($SQL)->queryAll();
                
                //modificamos esto
    		    if(is_null($tecs)){
    		         //generamos y asignamos una contraseña ramdomica para el usuario
                    
                    $pass = $modUtil->generateRandomString(6);
                    
                    $SQL = "UPDATE `users_c` SET `clave`='".md5($pass)."', `clave2`='".$pass."' WHERE `id_usuario_c`='".$id."'";
                    $passUpdate= Yii::app()->db->createCommand($SQL)->query();
                    // si ha pasado todas las validaciones y restricciones,                    
                    self::finalStepToCreate($id, $datos, $ruta_foto);
    		    }
                
                if(!is_null($tecs)){
                    if($tecs[0]["estado"]=='A'){
                        Yii::app()->user->setFlash('error', 'There is a license that is still valid.');
                        $this->redirect(array('ExamsUser/create/id/'.$id));
                    }
                    
                    //validamos asignar licencia 10 dias despues
                    $datetime1 = new Datetime($tecs[0]["fecha_presentacion"]);
                    $today = date("Y-m-d H:i:s");
                    $todayDt = new Datetime($today);
                    $ban_to_date =  date('Y-m-d H:i:s', strtotime("+10 days", strtotime($tecs[0]["fecha_presentacion"])));
                    $interval = date_diff($datetime1, $todayDt);
                	//obtenemos el valor en numero
                	$days = explode("+",$interval->format('%R%a'));
                	/*
                	if($days[1] <= 10){
                	    Yii::app()->user->setFlash('error', 'This test taker will be enabled to take the test on "'.$ban_to_date.'".');
                        $this->redirect(array('ExamsUser/create/id/'.$id));
                	}*/
                } /*  
                if(count($tecs)>=2){
                    
                	//var_dump("después revisamos la diferencia de fechas, si es menor a 6 meses comparamos puntajes para posible ban");die();
                	//buscamos la diferencia de fechas de presentación del examen mas reciente con el anterior
                	$datetime1 = new Datetime($tecs[0]["fecha_presentacion"]);
                	$datetime2 = new Datetime($tecs[1]["fecha_presentacion"]);
                	$interval = date_diff($datetime2, $datetime1);
                	//obtenemos el valor en numero
                	$days = explode("+",$interval->format('%R%a'));
                	
                	//si es menor a 6 meses, procedemos a revisar si hay banneo
                    if($days[1] <= 180){
                        //si la calificaicón del examen actual es menor o igual al anterior, estimamos el tiempo de banneo
                        
                        
                        if($tecs[0]["nivel"] == $tecs[1]["nivel"]){
                            //estimamos el tiempo del banneo
                            $ban_to_date =  date('Y-m-d H:i:s', strtotime("+180 days", strtotime($tecs[0]["fecha_presentacion"])));
                            //consultamos la fecha actual y comparamos la diferencia con la de banneo
                            $today = date("Y-m-d H:i:s");
                            $interval = date_diff($datetime2, $datetime1);
                            $daysToExpire= explode("+",$interval->format('%R%a'));
                            
                            Yii::app()->user->setFlash('error', 'The test taker level on the two previous TECS is the same. The test taker has been blocked for 180 days. This test taker will be enabled to retake the TECS on "'.$ban_to_date.'".');
                            $this->redirect(array('ExamsUser/create/id/'.$id));
                            
                    	}
                    }
                }*/
            
            //generamos y asignamos una contraseña ramdomica para el usuario
            $modUtil = new Utilidades();
            $pass = $modUtil->generateRandomString(6);
            
            $SQL = "UPDATE `users_c` SET `clave`='".md5($pass)."', `clave2`='".$pass."' WHERE `id_usuario_c`='".$id."'";
            $passUpdate= Yii::app()->db->createCommand($SQL)->query();
            // si ha pasado todas las validaciones y restricciones,                    
            self::finalStepToCreate($id, $datos, $ruta_foto);
                
                    
            }elseif($tipo_licencia == $id_ede){
                
                //ahora a validar asignacion de EDE
                $SQL = "SELECT id_licencias_usuario, id_licencia_cliente, id_usuario, licenses_user.fecha_asignacion, fecha_presentacion, licenses_user.estado, id_licencia, calificacion FROM `licenses_user` inner join `licenses_client` ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente WHERE id_usuario = '".$id."' AND licenses_client.id_licencia = '".$tipo_licencia."' ORDER BY fecha_presentacion DESC LIMIT 1 ";
                $ede= Yii::app()->db->createCommand($SQL)->queryAll();
                
                if(is_null($ede)){
    		        self::finalStepToCreate($id, $datos, $ruta_foto);
    		    } 
                
                if(!is_null($ede)){
                    if($ede[0]["estado"]=='A'){
                        Yii::app()->user->setFlash('error', 'There is a license that is still valid.');
                        $this->redirect(array('ExamsUser/create/id/'.$id));
                    }
                    
                    //validamos asignar licencia 10 dias despues
                    $datetime1 = new Datetime($ede[0]["fecha_presentacion"]);
                    $today = date("Y-m-d H:i:s");
                    $todayDt = new Datetime($today);
                    $ban_to_date =  date('Y-m-d H:i:s', strtotime("+10 days", strtotime($ede[0]["fecha_presentacion"])));
                    $interval = date_diff($datetime1, $todayDt);
                	//obtenemos el valor en numero
                	$days = explode("+",$interval->format('%R%a'));
                	/*
                	if($days[1] <= 10){
                	    Yii::app()->user->setFlash('error', 'This test take will be enable to take the test on "'.$ban_to_date.'".');
                        $this->redirect(array('ExamsUser/create/id/'.$id));
                	}*/
                	self::finalStepToCreate($id, $datos, $ruta_foto);
                } 
            }else{
                //ahora a validar asignacion de EDE
                $SQL = "SELECT id_licencias_usuario, id_licencia_cliente, id_usuario, licenses_user.fecha_asignacion, fecha_presentacion, licenses_user.estado, id_licencia, calificacion FROM `licenses_user` inner join `licenses_client` ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente WHERE id_usuario = '".$id."' AND licenses_client.id_licencia = '".$tipo_licencia."' ORDER BY fecha_presentacion DESC LIMIT 1 ";
                $ede= Yii::app()->db->createCommand($SQL)->queryAll();
                
                if(is_null($ede)){
    		        self::finalStepToCreate($id, $datos, $ruta_foto);
    		    }
                
                if(!is_null($ede)){
                    if($ede[0]["estado"]=='A'){
                        Yii::app()->user->setFlash('error', 'There is a license that is still valid.');
                        $this->redirect(array('ExamsUser/create/id/'.$id));
                    }
                    
                    //validamos asignar licencia 10 dias despues
                    $datetime1 = new Datetime($ede[0]["fecha_presentacion"]);
                    $today = date("Y-m-d H:i:s");
                    $todayDt = new Datetime($today);
                    $ban_to_date =  date('Y-m-d H:i:s', strtotime("+10 days", strtotime($ede[0]["fecha_presentacion"])));
                    $interval = date_diff($datetime1, $todayDt);
                	//obtenemos el valor en numero
                	$days = explode("+",$interval->format('%R%a'));
                	/*
                	if($days[1] <= 10){
                	    Yii::app()->user->setFlash('error', 'This test take will be enable to take the test on "'.$ban_to_date.'".');
                        //$this->redirect(array('ExamsUser/create/id/'.$id));
                	}*/
                	self::finalStepToCreate($id, $datos, $ruta_foto);
                }   
            }
            
               
        } // END VALIDATE POST EXIST
            
            $this->render('create',array(
			'model'=>$model,'id'=>$id
		        ));
    }
	
	
    	
    public function finalStepToCreate($id, $datos, $ruta_foto){
                                
                                //Asociar la licencia al usuario
                                $modLicUsu = new LicensesUser();
                                
                                $modLicUsu->id_licencia_cliente = $datos['id_licencia_usuario'];
                                $modLicUsu->id_usuario=$id;
                                $modLicUsu->fecha_asignacion = new CDbExpression('NOW()');
                                $modLicUsu->fecha_presentacion = $datos['fecha_presentacion'];
                                $modLicUsu->hora = $datos['hora'];
                                $modLicUsu->estado="A";
                                
                                $modLicUsu->save();
                                $id_licence = Yii::app()->db->getLastInsertID();

                                //Gastar la licencia
                                $modUtil = new Utilidades();
                                $modUtil->gastarLicencia($datos['id_licencia_usuario']);
                                //Asignar los exámenes al usuario
                                $modLicenciasCliente = LicensesClient::model()->findByPk($modLicUsu->id_licencia_cliente);
                                $modExamenes = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>$modLicenciasCliente->id_licencia));
                                
                                foreach($modExamenes as $examen){
                                    $modExamenUsuario = new ExamsUser();
                                    $modExamenUsuario->id_examen = $examen->id_examen;
                                    $modExamenUsuario->id_licencia_usuario = $modLicUsu->id_licencias_usuario;
                                    $modExamenUsuario->fecha_presentacion = $datos['fecha_presentacion'];
                                    $modExamenUsuario->hora = $datos['hora'];
                                    $modExamenUsuario->salon = $datos['salon'];
                                    if(strlen($datos['tutor'])>0){
                                        $modExamenUsuario->tutor = $datos['tutor']; 
                                    }
                                        $modExamenUsuario->estado = "A";
                                        $modExamenUsuario->save();
                                    }
                                
                                
                                if($datos['id_cliente'] == '8605173021' || $datos['id_cliente'] = '86051730212'){
                                    // Guardamos el registro en la tabla de period
                                    $SQL = "INSERT INTO `users_c_period`(`id_users_c_period`, `id_usuario_c`, `id_cliente`, `period`, `id_city`, `id_school`, `id_program`, `id_licencias_usuario`) 
                                    VALUES ('', '".$id."', '".$datos['id_cliente']."', '".$datos["period"]."', '".$datos["city"]."', '".$datos["school"]."', '".$datos["program"]."', '".$modLicUsu->id_licencias_usuario."')";
                                    $list= Yii::app()->db->createCommand($SQL)->query();   
                                }
                                
                                // agregamos al log del usuario
                                $SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 1, '".Yii::app()->user->getState('usuario')."', NOW(), '$id_licence', '', 'Assign a license to a test taker', '$id')";
                                $log= Yii::app()->db->createCommand($SQL)->query(); 
                                
                                    
                                if(!is_null($ruta_foto)){
                                    //$modUsu = new UsersC::model()->findByAttributes(array('id_usuario_c'=>$id));
                                    $modUsu = new UsersC();
                                    $modUsu->id_usuario_c = $id;
                                    //$modUsu = UsersC::model()->findByAttributes(array('id_usuario_c'=>$id));
                                    
                                    $extension = ".".strtolower($ruta_foto->getExtensionName());
                                    $ahora = time();
                                    $nombreImagen = $modLicenciasCliente->id_cliente."_".$id."_".$ahora.$extension;
                                    $modUsu->ruta_foto=$nombreImagen;
                                    $modUsu->telefono="";
                                    $modUsu->save(array('ruta_foto'),true);
                                    $SQL = "UPDATE `users_c` SET `ruta_foto`='".$nombreImagen."' WHERE `id_usuario_c`='".$id."'";
                                    $rutaUpdate= Yii::app()->db->createCommand($SQL)->query();
                                    $ruta_foto->saveAs("images/users/".$nombreImagen);
                                    $modUtil->generaThumb("images/users/",$nombreImagen);
                                }
                                
                                if(isset($_POST['doitnow'])){
                                    if($_POST['doitnow']=="1"){
                                        $this->redirect(array('AnswersExamUser/exam/userId/'.$id.'/license/'.base64_encode($modLicUsu->id_licencias_usuario).'/now/1'));
                                    }
                                }
                                $this->redirect(array('ExamsUser/admin/id/'.base64_encode($modLicUsu->id_licencias_usuario)));
                                          
    }
	

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ExamsUser']))
		{
			$model->attributes=$_POST['ExamsUser'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();
		
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('ExamsUser');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin($id)
	{   
	    
	    $id = base64_decode($id);
		$model=new ExamsUser('search');
		$model->unsetAttributes();  // clear any default values
                $model->id_licencia_usuario = $id;
		if(isset($_GET['ExamsUser']))
			$model->attributes=$_GET['ExamsUser'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ExamsUser the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ExamsUser::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ExamsUser $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='exams-user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
?>

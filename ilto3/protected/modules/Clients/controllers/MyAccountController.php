<?php

class MyAccountController extends Controller
{
    	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','licenses','statistics','myAccount','buyLicenses', 'orderLicenses', 'orderHistory', 'licensesHistory', 'orderDec', 'video-tutorials', 'personalInfo', 'adminLog'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

        public function actionBuyLicenses(){
            $model = new BuyLicensesForm();
		if(isset($_POST['BuyLicensesForm']))
		{
                    $model->attributes=$_POST['BuyLicensesForm'];
                    $licenseName = Licenses::model()->findByPk($model->licensetype)->nombre;
                    $mail = new YiiMailer('buyLicenses', array('model' => $model,'header_title'=>'Purchase Licenses',  'description'=>'Buy Licenses Form','licenseName'=>"$licenseName"));
                    $mail->setFrom("buy@iltoexams.com", "Buy Licenses");
                    $mail->setSubject("Licenses Purchase Request");
                    $mail->setTo("info@iltoexams.com");
                    //$mail->setTo("sairgo@gmail.com");
                    if ($mail->send()) {
                            Yii::app()->user->setFlash('success','Your request is being processed. One of our consultants will contact you to complete the purchase.');
                    } else {
                            Yii::app()->user->setFlash('error','Error while sending email: '.$mail->getError());
                    }
                    $this->redirect(array('index'));
		}
            $this->render('buyLicenses',array('model'=>$model));
        }
    
    public function actionOrderLicenses(){
        //$model = new OrderLicensesForm();
	    if(isset($_POST['BuyLicensesForm']))
		{
            //$model->attributes=$_POST['OrderLicensesForm'];
            $licenseName = Licenses::model()->findByPk($model->licensetype)->nombre;
            
            /*$mail = new YiiMailer('buyLicenses', array('model' => $model,'header_title'=>'Purchase Licenses',  'description'=>'Buy Licenses Form','licenseName'=>"$licenseName"));
            $mail->setFrom("buy@iltoexams.com", "Buy Licenses");
            $mail->setSubject("Licenses Purchase Request");
            $mail->setTo("info@iltoexams.com");
            //$mail->setTo("sairgo@gmail.com");
            if ($mail->send()) {
                Yii::app()->user->setFlash('success','Your request is being processed. One of our consultants will contact you to complete the purchase.');
            } else {
                Yii::app()->user->setFlash('error','Error while sending email: '.$mail->getError());
            }
            $this->redirect(array('index'));*/
	    }
        $this->render('orderLicenses',array('model'=>$model));
    }
        
        
	public function actionIndex()
	{
		$this->render('index');
	}
	
	public function actionMyAccount()
	{
	    
	    if(isset($_POST['UsersC'])){
                $post = $_POST['UsersC'];
                $modUsuario = UsersC::model()->findByAttributes(array('id_usuario_c'=>Yii::app()->user->getState('usuario')));
                $model = $modUsuario;
                $ruta_imagen=CUploadedFile::getInstance($model,'ruta_foto');
                $model->nombres = $post['nombres'];
                $model->apellidos = $post['apellidos'];
                $model->email = $post['email'];
                if(strlen($post['clave'])>0){
                    $model->clave = md5($post['clave']);
                    $model->clave2 = $post['clave2'];
                    if(!$model->save(true,array('clave','clave2'))){
                        Yii::app()->user->setFlash('error','The password could not be saved. Please review the information'); 
                    }
                }
                $modUtil = new Utilidades();
                if($model->save(true,array('nombres', 'apellidos', 'email'))){
                    Yii::app()->user->setFlash('success','Changes made.'); 
                    if(!is_null($ruta_imagen)){
                        //Si tenia una imagen, eliminarla
                        if(isset($_POST['ruta_imagen_actual'])){
                            $modUtil->eliminarThumbs("images/users/",$_POST['ruta_imagen_actual']);
                        }
                        //Nombre de la nueva imagen
                        $extension = ".".$ruta_imagen->getExtensionName();
                        $ahora = time();
                        $nombreImagen = $model->id_cliente."_".$model->id_usuario_c.$ahora.$extension;
                        $model->ruta_foto=$nombreImagen;
                        $model->save();
                        $ruta_imagen->saveAs("images/users/".$nombreImagen);
                        $modUtil->generaThumb("images/users/",$nombreImagen);
                    } elseif(strlen($_POST['ruta_imagen_actual'])>0){ //Si existia una imagen previa
                        if(isset($_POST['delete_imagen'])){
                            $model->ruta_foto=NULL;
                            $model->save(true,array('ruta_foto'));
                            $modUtil->eliminarThumbs("images/users/",$_POST['ruta_imagen_actual']);
                        }
                    }
                } else {
                    Yii::app()->user->setFlash('error','The changes were not made. Please review the information'); 
                }
                
            } 
            if(isset($_POST['send'])){
                $mail = new YiiMailer('changeinformation', array('datos'=>$_POST['company_changes']));
                //set properties
                $mail->setFrom("support@iltoexams.com", "ILTO Exams");
                $mail->setSubject("Information Change Request");
                $mail->setTo("support@iltoexams.com");
                if ($mail->send()) {
                        Yii::app()->user->setFlash('success','An Email was sent to '.$model->email.' with the Login Information.');
                } else {
                        Yii::app()->user->setFlash('error','Error while sending email: '.$mail->getError());
                }
            }
      	$this->render('myAccount');

        }

    public function actionCompanyLogo(){
       // $model = new Clients;
        
	    if(isset($_POST['Clients']))
		{
		    
		    $modC = Clients::model()->findByAttributes(array('id_cliente'=>$_POST['Clients']['id_cliente']));
		    $model = $modC;
		    $model->nombre_rsocial = $_POST['Clients']['nombre_rsocial'];
		    $model->direccion = $_POST['Clients']['direccion'];
		    $model->nit_id_legal = $_POST['Clients']['nit_id_legal'];
		    $model->telefono = $_POST['Clients']['telefono'];
		    
		    $SQL = "UPDATE `clients` SET `id_cliente`='".$_POST['Clients']['id_cliente']."', `nombre_rsocial`='".$_POST['Clients']['nombre_rsocial']."', `nit_id_legal`='".$_POST['Clients']['nit_id_legal']."', `direccion`='".$_POST['Clients']['direccion']."', `telefono`='".$_POST['Clients']['telefono']."', `link`='".$_POST['Clients']['link']."' WHERE `id_cliente`='".$_POST['Clients']["id_cliente"]."'";
            $list= Yii::app()->db->createCommand($SQL)->query();
		    
			$ruta_logo=CUploadedFile::getInstance($model,'ruta_logo');
		    $ahora = time();
            $modUtil = new Utilidades();
            if(!is_null($ruta_logo)){
                
                $extension = ".".$ruta_logo->getExtensionName();
                $nombreImagen = $model->id_cliente."_".$ahora.$extension;
                $model->ruta_logo=$nombreImagen;
                $model->save();
                $ruta_logo->saveAs("images/clients/".$nombreImagen);
                $modUtil->generaThumb("images/clients/",$nombreImagen);
                
            }
            
            if($_POST['Clients']['link']=='1'){
                $description = 'Activate EDE link';
            }else{
                $description = 'Deactivate EDE link';
            }
            
            // ACCESS con GET
            $SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 1, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'Edit Personal Information and ".$description."', '')";
            $log= Yii::app()->db->createCommand($SQL)->query(); 
            $this->render('myAccount');
		}
	}
	
	 public function actionCompanyLink(){
       if(isset($_POST['Clients']))
		{
		    //var_dump($_POST['Clients']['link']);die();
		    $SQL = "UPDATE `clients` SET `link`='".$_POST['Clients']['link']."' WHERE `id_cliente`='".$_POST['Clients']["id_cliente"]."'";
            $list= Yii::app()->db->createCommand($SQL)->query();
            
            $this->render('myAccount');
            
		}
	}
	
	public function actionOrderConfirm(){
	    if(isset($_POST['order']))
		{
		    
		    //validamos la ultima orden hecha por éste cliente para que no se repita
		    $SQL = "SELECT * FROM `orders` WHERE `id_client` = '".$_POST['order']["id_cliente"]."' AND `date_expedition` = '".$_POST['order']["date_exped"]."' AND `subtotal` = '".$_POST['order']["subtotal"]."' AND `discount` = '".$_POST['order']["discount"]."' AND `total` = '".$_POST['order']["total"]."'";
		    $order_exist= Yii::app()->db->createCommand($SQL)->queryAll();
		    if(!empty($order_exist)){
		        //si existe, no llenamos nada
		        $this->render('orderHistory');
		    }else{
    		    // Creamos la orden de compra
    	        $SQL = "INSERT INTO `orders` (`id_client`, `date_expedition`, `date_expire`, `subtotal`, `discount`, `total`, `status`, `id_distributor`, `check_ilto`, `check_client`, `currency_type`, `currency_value`)
        		VALUES ('".$_POST['order']['id_cliente']."', '".$_POST['order']['date_exped']."', '".$_POST['order']['date_expire']."', '".$_POST['order']['subtotal']."', '".$_POST['order']['discount']."', '".$_POST['order']['total']."', 'P', '".$_POST['order']['id_distributor']."', 0, 0,'".$_POST['order']['currency_type']."','".$_POST['order']['currency_value']."')";
                $order= Yii::app()->db->createCommand($SQL)->query();
                
                // Consultamos la última orden de compra
                $sql='SELECT id_order FROM orders ORDER BY id_order DESC LIMIT 1';
                $id_order= Yii::app()->db->createCommand($sql)->queryAll();
                
                // insertar licencias tecs 4.0 compradas 
                if(isset($_POST['orderLic15'])){
                    $SQL = "INSERT INTO `order_licenses` (`id_order`, `id_licencia`, `quantity`, `price`, `total`)
        		    VALUES ('".$id_order[0]["id_order"]."', '".$_POST['orderLic15']['id_licencia']."', '".$_POST['orderLic15']['amount']."', '".$_POST['orderLic15']['price']."', '".$_POST['orderLic15']['total']."')";
                    $order_item= Yii::app()->db->createCommand($SQL)->query();
                }
                
                // insertar licencias ede 4.0 compradas 
                if(isset($_POST['orderLic16'])){
                    $SQL = "INSERT INTO `order_licenses` (`id_order`, `id_licencia`, `quantity`, `price`, `total`)
        		    VALUES ('".$id_order[0]["id_order"]."', '".$_POST['orderLic16']['id_licencia']."', '".$_POST['orderLic16']['amount']."', '".$_POST['orderLic16']['price']."', '".$_POST['orderLic16']['total']."')";
                    $order_item= Yii::app()->db->createCommand($SQL)->query();
                }
                
                // insertar licencias cedt compradas 
                if(isset($_POST['orderLic17'])){
                    $SQL = "INSERT INTO `order_licenses` (`id_order`, `id_licencia`, `quantity`, `price`, `total`)
        		    VALUES ('".$id_order[0]["id_order"]."', '".$_POST['orderLic17']['id_licencia']."', '".$_POST['orderLic17']['amount']."', '".$_POST['orderLic17']['price']."', '".$_POST['orderLic17']['total']."')";
                    $order_item= Yii::app()->db->createCommand($SQL)->query();
                }
                
                // insertar licencias tecs 4.0 compradas 
                if(isset($_POST['orderLic18'])){
                    $SQL = "INSERT INTO `order_licenses` (`id_order`, `id_licencia`, `quantity`, `price`, `total`)
        		    VALUES ('".$id_order[0]["id_order"]."', '".$_POST['orderLic18']['id_licencia']."', '".$_POST['orderLic18']['amount']."', '".$_POST['orderLic18']['price']."', '".$_POST['orderLic18']['total']."')";
                    $order_item= Yii::app()->db->createCommand($SQL)->query();
                }
                
                // CONSTRUCCIÓN DEL PDF
                
                $SQL = "SELECT * FROM `clients` WHERE id_cliente='".Yii::app()->user->getState('cliente')."'";
                $client= Yii::app()->db->createCommand($SQL)->queryAll();
                
                $SQL = "SELECT city.name as city, country.name as country FROM `city` INNER JOIN country ON city.CountryCode = country.Code WHERE city.ID ='".$client[0]["id_ciudad"]."'";
                $country= Yii::app()->db->createCommand($SQL)->queryAll();
                
                $SQL = "SELECT nombres, apellidos, email FROM `users_c` WHERE id_cliente='".Yii::app()->user->getState('cliente')."' AND id_perfil='2'";
                $toaa= Yii::app()->db->createCommand($SQL)->queryAll();
                
                
                // Se enviar correo de la orden de compra al cliente y al distribuidor, copia a ilto
                $SQL = "SELECT email FROM `users_c` WHERE id_cliente='".$_POST['order']['id_distributor']."' AND id_perfil='2' LIMIT 1";
                $distributor= Yii::app()->db->createCommand($SQL)->queryAll();
                
                $clientName = $client[0]["nombre_rsocial"];
                
                // Se envia correo de la orden de compra al cliente y a iltoexams
                //$to = 'iltoexams@outlook.com';
                $to = 'iltoexams@outlook.com';
                $subject = ''.$clientName.', NEW Quote No. '.$id_order[0]["id_order"].'';
                $msg = 'Please check your pending quote.';
                $data = "{From: 'noreply@iltoexams.com', To: '$to', Subject: '$subject', HtmlBody: '$msg'}";//array('From' => 'noreply@iltoexams.com', 'To'=>'dokho_02@hotmail.com', 'Subject'=>'Hola Chris Fer', 'HtmlBody'=>'<strong>Hello</strong> dear Postmark user.');
                
                
                // agregamos al log del usuario + last iD
				$lastID = Yii::app()->db->getLastInsertID();
				$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 1, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'Purchase license Quotation No. ".$id_order[0]["id_order"]."', '')";
				$log= Yii::app()->db->createCommand($SQL)->query(); 
                
                $modUtil = new Utilidades();
                $modUtil->sendEmail($data);
                $this->render('orderHistory');
               
		    }
		}
	}
	
		public function actionUpatePersonalInfo(){
	    var_dump("update personal info");die();
	}


	public function actionLicenses()
	{
		$this->render('licenses');
	}
	
	public function actionVideoTutorials()
	{
		$this->render('video-tutorials');
	}
	
	public function actionPersonalInfo()
	{
		$this->render('personalInfo');
	}
	
	public function actionOrderHistory()
	{
	    $this->render('orderHistory');
	}
	
	public function actionLicensesHistory()
	{
	    $this->render('licensesHistory');
	}
	
	public function actionOrderDec($id)
	{
	    if(isset($_GET['id'])){
		    $this->render('orderDec');
		}else{
		    $this->render('orderHistory');   
		}
	}
	
	public function actionOrderLastStep()
	{
	   if(isset($_POST['order'])){
		    
		    
		    //validamos la ultima orden hecha por éste cliente para que no se repita
		    $SQL = "SELECT * FROM `invoices` WHERE `id_order` = '".$_POST['order']["id_order"]."'";
		    $order_exist= Yii::app()->db->createCommand($SQL)->queryAll();
		    
		    //get Client name
		    
		    $SQL = "SELECT nombre_rsocial FROM `clients` WHERE `id_cliente` = '".$_POST['order']["id_client"]."'";
		    $client= Yii::app()->db->createCommand($SQL)->queryAll();
		    $clientName = $client[0]["nombre_rsocial"];
		    
		    if(!empty($order_exist)){
		        //si existe, no llenamos nada
		        $this->render('orderHistory');
		    }else{
		        //Actualizamos el estado de la Orden de Compra
    		    $SQL = "UPDATE `orders` SET `status`='".$_POST["order"]["status"]."',`check_client`='1' WHERE `id_order`='".$_POST["order"]["id_order"]."'";
                $update_order = Yii::app()->db->createCommand($SQL)->query();
    		    if($_POST["order"]["status"]=='C') {
    		        
    		        // Envio correo orden cancelada
                    $to = 'iltoexams@outlook.com';
                    $subject = 'Quotation No. '.$_POST["order"]["id_order"].' Cancelled';
                    $msg = ''.$clientName.' has cancelled the cuote.';
                    $data = "{From: 'noreply@iltoexams.com', To: '$to', Subject: '$subject', HtmlBody: '$msg'}";//array('From' => 'noreply@iltoexams.com', 'To'=>'dokho_02@hotmail.com', 'Subject'=>'Hola Chris Fer', 'HtmlBody'=>'<strong>Hello</strong> dear Postmark user.');
                    $modUtil = new Utilidades();
                    $modUtil->sendEmail($data);
    		    }else{
    		        $modUtil = new Utilidades();
        		    
        		    if($_POST['order']['id_country']!="COL"){
        		        // INSERTAMOS DATOS FACTURA ILTO EN LA TABLA
            		    $SQL = "INSERT INTO `invoices` (`id_order`, `date_expedition`, `status`)
                		VALUES ('".$_POST['order']['id_order']."', '".date("Y-m-d H:i:s")."', 'P')";
                        $inv= Yii::app()->db->createCommand($SQL)->query();
                            
                        // CONSULTAMOS ID FACTURA
                        $SQL = "SELECT id_invoice FROM `invoices` WHERE id_order='".$_POST['order']['id_order']."'";
                        $invoice= Yii::app()->db->createCommand($SQL)->queryAll();
                        
                        //notificamos a ILTOEXAMS de la compra para USA
                        /*$to = 'iltoexams@outlook.com';
                        $subject = 'New purchase '.$clientName.', Invoice No.'.$invoice[0]["id_invoice"].'';
                        $msg = ''.$clientName.' has confirmed your quote and the invoice number '.$invoice[0]["id_invoice"].' has been issued. ';
                        $data = "{From: 'noreply@iltoexams.com', To: '$to', Subject: '$subject', HtmlBody: '$msg'}";//array('From' => 'noreply@iltoexams.com', 'To'=>'dokho_02@hotmail.com', 'Subject'=>'Hola Chris Fer', 'HtmlBody'=>'<strong>Hello</strong> dear Postmark user.');
                        $modUtil->sendEmail($data);*/
                        
                        // envio correo de FACTURA a iltoexams y al cliente
            		    $modUtil->invoiceGenerate($invoice[0]["id_invoice"]);
            		        
        		    }else{
        		        //notificamos a ILTOEXAMS de la compra
                        $to = 'iltoexams@outlook.com';
                        $subject = ''.$clientName.', has confirmed his Order, Cuote No.'.$_POST["order"]["id_order"].'';
                        $msg = '<p style="text-aling:justify;color:#5d5c5c;">'.$clientName.' has confirmed his Order, Cuote No.'.$_POST["order"]["id_order"].'<br/><br/>
                        Sincerely,<br/><br/>
                    <a href="http://www.iltoexams.com/"><img style="float:left;top:20px;" alt="ILTO Exams" src="iltoexams.com/logo_ILTO.png" /></a></p>';
                        $data = "{From: 'noreply@iltoexams.com', To: '$to', Subject: '$subject', HtmlBody: '$msg'}";//array('From' => 'noreply@iltoexams.com', 'To'=>'dokho_02@hotmail.com', 'Subject'=>'Hola Chris Fer', 'HtmlBody'=>'<strong>Hello</strong> dear Postmark user.');
                        $modUtil->sendEmail($data);
        		    }
        		    
        		    $this->render('orderHistory');  
        		}
    		    $this->render('orderHistory');    
		    }    
		}else{
		    $this->render('orderHistory');   
		}
	}
	
	public function actionCuoteDetail($id){
	    // agregamos al log del usuario
		$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 1, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'See quote PDF file No. ".$id."', '')";
		$log= Yii::app()->db->createCommand($SQL)->query(); 
	    $modUtil = new Utilidades;
        $informe = $modUtil->cuoteDetail($id);
                
    }
    
    public function actionInvoiceDetail($id){
	    
	    // agregamos al log del usuario
		$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 1, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'See invoice PDF file No. ".$id."', '')";
		$log= Yii::app()->db->createCommand($SQL)->query(); 
	    $modUtil = new Utilidades;
        $informe = $modUtil->invoiceDetail($id);
                
    }

	public function actionStatistics()
	{
		$this->render('statistics');
	}
	
	public function actionAdminLog()
	{
		$this->render('adminLog');
	}
	

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
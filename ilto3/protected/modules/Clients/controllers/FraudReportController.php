<?php

class FraudReportController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','delete'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new FraudReport;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['FraudReport']))
		{
			
			//get TECS2GO REGISTER from the reported taker
        	$SQL = 'SELECT * FROM `proctorv_request` WHERE `id_user` = "'.$_POST['FraudReport']['id_usuario_c'].'" ORDER BY id_proctorv_request DESC LIMIT 1';
        	$request= Yii::app()->db->createCommand($SQL)->queryAll();
			
			//SET USER STATUS TO BLOCKED
			//REVOKE LICENSE'S EXAMS
			$SQL =  "UPDATE users_c SET estado = 'B' WHERE id_usuario_c = '".$_POST['FraudReport']['id_usuario_c']."' ";  
			$success= Yii::app()->db->createCommand($SQL)->query();
			
			//si hay tecs2go Bloqueo
			if(isset($request[0]["id_proctorv_request"])){
				
				//DELETE REVOKE STATUS TECS2GO REQUEST
				$SQL =  "DELETE FROM `proctorv_user_dates` WHERE id_request = '".$request[0]["id_proctorv_request"]."' ";  
				$success= Yii::app()->db->createCommand($SQL)->query();
				
				//UPDATE REVOKE STATUS TECS2GO REQUEST
				$SQL =  "DELETE FROM `proctorv_request` WHERE id_proctorv_request = '".$request[0]["id_proctorv_request"]."' ";
				$success= Yii::app()->db->createCommand($SQL)->query();
				
	        	
		        if(isset($request[0]["id_license"])){
		        
				    //REVOKE LICENSE
					$SQL =  "UPDATE licenses_user SET estado = 'B' WHERE id_licencias_usuario = '".$request[0]["id_license"]."' ";  
					$success= Yii::app()->db->createCommand($SQL)->query();
					
					//REVOKE LICENSE'S EXAMS
					$SQL =  "UPDATE exams_user SET estado = 'B' WHERE id_licencia_usuario = '".$request[0]["id_license"]."' ";  
					$success= Yii::app()->db->createCommand($SQL)->query();
					
					
		        }
			}
			
			
			//validate already reported
			$SQL = 'SELECT * FROM `fraud_report` WHERE `document` = "'.$_POST['FraudReport']['id_usuario_c'].'"';
            $id= Yii::app()->db->createCommand($SQL)->queryAll();
            
            if(!is_null($list)>0){
            	Yii::app()->user->setFlash('error', 'The taker is already reported.');
                $this->redirect(array('fraudReport/create'));
            }
            $id_cliente = Yii::app()->user->getState('cliente');
            $id_user_report = Yii::app()->user->getState('usuario');
			$name_user_report = Yii::app()->user->getState('nombres_apellidos');
			
			//preguntamos si es un nuevo usuario
			if(isset($_POST['FraudReport']['new_user'])){
				$SQLIN = 'INSERT INTO `users_c`(`id_usuario_c`, `nombres`, `apellidos`, `email`, `id_cliente`, `ultimo_acceso`, `id_perfil`, `numero_id`, `ruta_foto`, `estado`, `clave`, `clave2`, `fecha_creado`, `id_grupo_cliente`, `id_pais`, `id_country`, `id_ciudad`, `email_certificate`, `isDemo`, `reference`, `telefono`) 
				VALUES ("'.$_POST['FraudReport']['id_usuario_c'].'","'.$_POST['FraudReport']['nombres'].'","'.$_POST['FraudReport']['apellidos'].'","PENDING","'.$id_cliente.'",NOW(),6,"'.$_POST['FraudReport']['id_usuario_c'].'","PENDING","B","'.md5($_POST['FraudReport']['id_usuario_c']).'","'.$_POST['FraudReport']['id_usuario_c'].'",NOW(),NULL,3,"COL",1,0,0,NULL,0)';
	            $list= Yii::app()->db->createCommand($SQLIN)->query(); 
				
				$SQLIN = 'INSERT INTO `fraud_report` (`document`, `first_name`, `sure_name`, `email`, `id_client`, `picture`, `id_user_report`, `name_user_report`, report_date, `comments`, `exam`, `type`)
	            VALUES ("'.$_POST['FraudReport']['id_usuario_c'].'", "'.$_POST['FraudReport']['nombres'].'", "'.$_POST['FraudReport']['apellidos'].'", "PENDING", "'.$id_cliente.'", "PENDING",
	            "'.$id_user_report.'", "'.$name_user_report.'", NOW(), "'.$_POST['FraudReport']['comments'].'", 19, "'.$_POST['FraudReport']['type'].'")';
	            $list= Yii::app()->db->createCommand($SQLIN)->query(); 
	            	
	            $mensaje = 'Report sent successfully';
	            Yii::app()->user->setFlash('Message',$mensaje);
			    $this->redirect(array('admin', $mensaje));  	
				
			}else{
				$SQL = 'SELECT * FROM `users_c` where `id_usuario_c` = "'.$_POST['FraudReport']['id_usuario_c'].'"';
	            $list= Yii::app()->db->createCommand($SQL)->queryAll();
				
				if(!is_null($list)>0){
	            	
	            	$SQLIN = 'INSERT INTO `fraud_report` (`document`, `first_name`, `sure_name`, `email`, `id_client`, `picture`, `id_user_report`, `name_user_report`, report_date, `comments`, `exam`, `type`)
	            	VALUES ("'.$_POST['FraudReport']['id_usuario_c'].'", "'.$list[0]['nombres'].'", "'.$list[0]['apellidos'].'", "'.$list[0]['email'].'", "'.$list[0]['id_cliente'].'", "'.$list[0]['ruta_foto'].'",
	            	"'.$id_user_report.'", "'.$name_user_report.'", NOW(), "'.$_POST['FraudReport']['comments'].'", 19, "'.$_POST['FraudReport']['type'].'")';
	            	$list= Yii::app()->db->createCommand($SQLIN)->query(); 
	            	
	            	// agregamos al log del usuario + last iD
				$lastID = Yii::app()->db->getLastInsertID();
	            	
	            	$mensaje = 'Report sent successfully';
	            	Yii::app()->user->setFlash('Message',$mensaje);
			        $this->redirect(array('admin', $mensaje));  
	            }
	            
	            
				$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 1, '".Yii::app()->user->getState('usuario')."', NOW(), '$licenseID', '', 'Create a Fraud Alert', '".$id_user_report."')";
				$log= Yii::app()->db->createCommand($SQL)->query(); 

	            
	            
	            $this->render('create',array(
					'model'=>$model,
				));
			}
			
		   
          /*	
			$model->attributes=$_POST['FraudReport'];
                        $usuarioc = UsersC::model()->resetScope()->findByAttributes(array('id_usuario_c'=>$model->document));
                        $model->email = $usuarioc->email;
                        $model->first_name = $usuarioc->nombres;
                        $model->sure_name = $usuarioc->apellidos;
                        $model->id_client = Yii::app()->user->getState('cliente');
                        $model->picture = $usuarioc->ruta_foto;
                        $model->id_user_report = Yii::app()->user->getState('usuario');
                        $model->name_user_report = Yii::app()->user->getState('nombres_apellidos');
                        $model->report_date = new CDbExpression('NOW()');
			if($model->save())
				$this->redirect(array('admin'));
		*/}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['FraudReport']))
		{
			$model->attributes=$_POST['FraudReport'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('FraudReport');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
        $busca=NULL;
        if(isset($_POST['btnSearch'])){
        	$busca = $_POST['search_box'];
        }
       	$model=new FraudReport('search');
       	$model->unsetAttributes();  // clear any default values
        $model->attributes=$_GET['FraudReport'];
        $this->render('admin',array(
			'model'=>$model,'busca'=>$busca
		));
		
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return FraudReport the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=FraudReport::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param FraudReport $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='fraud-report-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

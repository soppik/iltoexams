<?php

class LicensesUserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),  
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('certificate','emailcertificate', 'reports', 'create','update','admin','delete','reportDownload', 'ReportDownloadFuaaDa', 'SuperScore', 'proctorvAdmin', 'proctorvRequest', 'tecs2goSchedule', 'viewDay', 'printDay', 'enableDate'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin2'),
				'expression'=>"Yii::app()->user->getState('ILTO')",
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

 
        public function actionCertificate($id){
        	
        	//close taker vulnerability
        	$profile = Yii::app()->user->getState('id_perfil');
			if($profile == 6){
			    $this->redirect('index.php?r=Clients/default/login&message=12');
			}
        	
        	
        	if($id=="37490"){
        		Yii::app()->user->setFlash('error','This certificate has been voided. For further information contact the test administrator.');
                $this->redirect(array('ExamsUser/admin&id='.$id));	
        	}
        	
        	
					        	
				$modUtil = new Utilidades;
                $modExamsUser = LicensesUser::model()->findAllByAttributes(array('id_licencias_usuario'=>$id));
            	$modLicType = LicensesClient::model()->findAllByAttributes(array('id_licencias_cliente'=>$modExamsUser[0]->id_licencia_cliente));
                // $idExamen= $modExamsUser[0]->id_examen;
                
                $id_usuario = $modExamsUser[0]['id_usuario'];
                
                // agregamos al log del usuario
				$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 1, '".Yii::app()->user->getState('usuario')."', NOW(), '$id', '', 'Access test taker certificate', '$id_usuario')";
				$log= Yii::app()->db->createCommand($SQL)->query(); 
                
                 if($modExamsUser[0]->estado=='B'){
                 	Yii::app()->user->setFlash('error','This certificate has been revoked. For further information contact the test administrator.');
                	$this->redirect(array('ExamsUser/admin&id='.$id));
                 }
                
                if($modExamsUser[0]->estado=='F'){
                    switch ($modLicType[0]->id_licencia) {
					    case 6:
					        $informe = $modUtil->generaCertificadoEde($id);
					        break;
					    case 7:
					        $informe = $modUtil->generaCertificado($id);
					        break;
					    case 15:
					        $informe = $modUtil->generaCertificadoAA($id);
					    	break;
					     case 16:
					     	
					        $informe = $modUtil->generaCertificadoEde($id);
					        break;
                    	case 18:
					        $informe = $modUtil->generaCertificadoEcluv($id);
					        break;
					    case 19:
					        $informe = $modUtil->generaCertificadoAa($id);
					        break;
					    case 20:
					        $informe = $modUtil->generaCertificadoEde($id);
					        break;
					    case 22:
					        $informe = $modUtil->generaCertificado($id);
					        break;
					    case 23:
					        $informe = $modUtil->generaCertificadoAa($id);
					        break;
					     case 25:
					        $informe = $modUtil->generaCertificadoAa($id);
					        break;
					    default:
					        $informe = $modUtil->generaCertificadoEde($id);
					}
					
					
					
					//require '/home/thetec/public_html/vendor/autoload.php';
					require '/home/iltoexam/public_html/vendor/autoload.php';
					
					$mpdf = new \Mpdf\Mpdf(['tempDir' => __DIR__ . '/../temp']);
					$mpdf->WriteHTML($informe);
					$mpdf->Output();
					
				}else{
                	Yii::app()->user->setFlash('error','This user still has pending exams');
                    $this->redirect(array('LicensesUser/admin/id/'.$modExamsUser[0]->id_usuario));
                       	
                }
                
                
        }
        
        public function actionSuperScore($id){
        	
        	$id = base64_decode(base64_decode($id));

        	$modUtil = new Utilidades;
        	$tecs_ids = $modUtil->getIdsTecs();
        	
        	$SQL = "SELECT id_licencias_usuario FROM `licenses_user` INNER JOIN licenses_client ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente WHERE `id_usuario` LIKE '".$id."' AND licenses_client.id_licencia IN ($tecs_ids) AND licenses_user.estado = 'F';";
        	$list= Yii::app()->db->createCommand($SQL)->queryAll();
        	
        	//array exam name, score, id_exam, level
        	$exams = array(
        			array("speaking",0,0,"lvl"),
        			array("grammar",0,0,"lvl"),
        			array("reading",0,0,"lvl"),
        			array("listening",0,0,"lvl")
        		); 
        	
        	//select best scores by exams
        	foreach ($list as $license) {
        		$SQL = "SELECT id_examen_usuario, calificacion, nivel FROM `exams_user` WHERE `id_licencia_usuario` = '".$license["id_licencias_usuario"]."' ORDER BY id_examen ASC";
        		$exams_info= Yii::app()->db->createCommand($SQL)->queryAll();
        		
        		
        		// speaking result
        		if($exams[0][1] < $exams_info[0]["calificacion"]){
        			$exams[0][1] = $exams_info[0]["calificacion"];
        			$exams[0][2] = $exams_info[0]["id_examen_usuario"];
        			$exams[0][3] = $exams_info[0]["nivel"];
        		}
        		// grammar result
        		if($exams[1][1] < $exams_info[1]["calificacion"]){
        			$exams[1][1] = $exams_info[1]["calificacion"];
        			$exams[1][2] = $exams_info[1]["id_examen_usuario"];
        			$exams[1][3] = $exams_info[1]["nivel"];
        		}
        		
        		//reading result
        		if($exams[2][1] < $exams_info[2]["calificacion"]){
        			$exams[2][1] = $exams_info[2]["calificacion"];
        			$exams[2][2] = $exams_info[2]["id_examen_usuario"];
        			$exams[2][3] = $exams_info[2]["nivel"];
        		}
        		
        		//reading result
        		if($exams[3][1] < $exams_info[3]["calificacion"]){
        			$exams[3][1] = $exams_info[3]["calificacion"];
        			$exams[3][2] = $exams_info[3]["id_examen_usuario"];
        			$exams[3][3] = $exams_info[3]["nivel"];
        		}
        	}	
        	
        	
        	
        	//check if there is a user superscore in db
        	$SQL = "SELECT id_exams_superscore FROM `exams_superscore` WHERE `id_user` = '".$id."'";
        	$super_score= Yii::app()->db->createCommand($SQL)->queryAll();
        	
        	// check if there are a superScore already for this user and delete it
        	if(!empty($super_score)){
        		//delete and re 
        		$SQL = "DELETE FROM `exams_superscore` WHERE id_user = '".$id."'";
				$log= Yii::app()->db->createCommand($SQL)->query();
        	}
        	
        	//save best scores in table
	        foreach($exams as $exam){
	        	$SQL = "INSERT INTO `exams_superscore` (`id_exams_superscore`, `id_user`, `id_examen_usuario`, `date_generate`) VALUES (NULL, '$id', '".$exam[2]."', NOW())";
				$log= Yii::app()->db->createCommand($SQL)->query();
			}
        	
        
        	//generate the Certificate
        	$informe = $modUtil->getSuperScoreCertificate($id, $exams);
        	//require '/home/thetec/public_html/vendor/autoload.php';
			require '/home/iltoexam/public_html/vendor/autoload.php';
					
					$mpdf = new \Mpdf\Mpdf(['tempDir' => __DIR__ . '/../temp']);
					$mpdf->WriteHTML($informe);
					$mpdf->Output();
        	
        }

        public function actionEmailcertificate($id){
        	
        		$modExamsUser = ExamsUser::model()->findAllByAttributes(array('id_licencia_usuario'=>$id));
        		$idExamen= $modExamsUser[0]->id_examen;
				$modUtil = new Utilidades; 
                $modLicencias = LicensesUser::model()->findByPk($id);
                $modUtil->enviarCertificado($modLicencias->id_usuario,$id);
                $id_usuario = $modLicencias->id_usuario;
                
                // agregamos al log del usuario
				$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 1, '".Yii::app()->user->getState('usuario')."', NOW(), '$id', '', 'Send certificate to test taker', '$id_usuario')";
				$log= Yii::app()->db->createCommand($SQL)->query(); 
                
                $this->redirect(array('LicensesUser/admin/id/'.$modLicencias->id_usuario));        
                
        		
                
        }
        
        
        /**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new LicensesUser;
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['LicensesUser']))
		{
			$model->attributes=$_POST['LicensesUser'];
			if($model->save()){
						//var_dump($_POST);
			$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['LicensesUser']))
		{
			$model->attributes=$_POST['LicensesUser'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{	
		
		$modTmp = LicensesUser::model()->findByAttributes(array('id_licencias_usuario'=>$id));
		$modUtil = new Utilidades();
		$edeId = $modUtil->getIdEde();
    	$tecsId = $modUtil->getIdTecs();
		
		if(!is_null($modTmp)){
            $modTmp->delete();
            // agregamos al log del usuario + last iD
            $SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 1, '".Yii::app()->user->getState('usuario')."', NOW(), '".$id."', '', 'Delete License', '".$_GET['id']."')";
            $log= Yii::app()->db->createCommand($SQL)->query(); 
            $cli_lic = LicensesClient::model()->findByAttributes(array('id_licencias_cliente'=>$modTmp->id_licencia_cliente));
            
            $SQL = "UPDATE `licenses_client` SET `utilizados` = (utilizados - 1), `estado` = 'A' WHERE `licenses_client`.`id_licencias_cliente` = '".$cli_lic ["id_licencias_cliente"]."'";
	        $update= Yii::app()->db->createCommand($SQL)->query();
	        $this->redirect(array('LicensesUser/admin&id='.$modTmp->id_usuario));
	       
		}
	}	
	
	
	public function actionEnableDate($id)
	{	
		
		
		if(!is_null($id)){
			//instance the License object by ID
			$modTmp = LicensesUser::model()->findByAttributes(array('id_licencias_usuario'=>$id));
			
            // agregamos al log del usuario + last iD
            $SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 1, '".Yii::app()->user->getState('usuario')."', NOW(), '".$id."', '', 'Enable Presentation Date for License', '".$modTmp->id_usuario."')";
            $log= Yii::app()->db->createCommand($SQL)->query(); 
            
            //update License Presentation Date to enable the taker 
            $SQL = "UPDATE `licenses_user` SET `fecha_presentacion` = NOW() WHERE `licenses_user`.`id_licencias_usuario` = '".$id."'";
	        $update= Yii::app()->db->createCommand($SQL)->query();
	        $this->redirect(array('LicensesUser/admin&id='.$modTmp->id_usuario));
	       
		}
	}
	

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('LicensesUser');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin($id)
	{
		$model=new LicensesUser('search');
		$model->unsetAttributes();  // clear any default values
                $model->id_usuario = $id;
		if(isset($_GET['LicensesUser']))
			$model->attributes=$_GET['LicensesUser'];
		
		$this->render('admin',array(
			'model'=>$model,'id_usuario'=>$id
		));
	}
	
	public function actionProctorvAdmin()
	{
	
		$this->render('proctorv-admin');
	}
	
	public function actionProctorvRequest($id)
	{
		
		$this->render('proctorv-request',array(
			'id'=>$id
		));
	}
	
	public function actionAdmin2($id)
	{
		
		$model=new LicensesUser('search');
		$model->unsetAttributes();  // clear any default values
        $model->id_usuario = $id;
		if(isset($_GET['LicensesUser']))
			$model->attributes=$_GET['LicensesUser'];

		$this->render('admin',array(
			'model'=>$model,'id_usuario'=>$id
		));
	}
        
    public function behaviors() {
    return array(
        'exportableGrid' => array(
            'class' => 'application.components.ExportableGridBehavior',
            'filename' => 'ILTO_Reports.csv',
            'csvDelimiter' => ';', //i.e. Excel friendly csv delimiter
            ));
}
	/**
	 * Manages all models.
	 */
	public function actionReports()
	{
		$model=new LicensesUsed('search');
		$model->unsetAttributes();  // clear any default values
                $model->elCliente = Yii::app()->user->getState('cliente');
		if(isset($_GET['LicensesUsed']))
			$model->attributes=$_GET['LicensesUsed'];
                    if ($this->isExportRequest()) { //<==== [[ADD THIS BLOCK BEFORE RENDER]]
                    //set_time_limit(0); //Uncomment to export lage datasets
                    //Add to the csv a single line of text
                    $this->exportCSV(array('POSTS WITH FILTER:'), null, false);
                    //Add to the csv a single model data with 3 empty rows after the data
                    $this->exportCSV($model, array_keys($model->attributeLabels()), false, 3);
                    //Add to the csv a lot of models from a CDataProvider
                    $this->exportCSV($model->search(), array('idUsuario.id_usuario_c', 'idUsuario.email', 'idUsuario.nombres','idUsuario.apellidos', 'fecha_presentacion', 'hora', 'calificacion','nivel','estado'));
                    } 
		$this->render('reports',array(
			'model'=>$model));
	}
	
	public function actionReportDownload()
	{	
		
		if(isset($_POST['LicensesUser'])){
			
			
			// agregamos al log del usuario
			$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 1, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'Download CSV file report', '')";
			$log= Yii::app()->db->createCommand($SQL)->query(); 
			
			//output headers so that the file is downloaded rather than displayed
	        
	        header('Content-Type: text/csv; charset=utf-8');
	        header('Content-Disposition: attachment; filename=list-report-'.date('Y_m_d_H_i_s').'.csv');
	        
	        // create a file pointer connected to the output stream
	        $output = fopen('php://output', 'w');
	        
			if(isset($_POST['level']) && !empty($_POST['level']>0)){
				foreach($_POST['level'] as $check) {
					$level .= "'".$check."',";
				}	
				$level = rtrim($level,",");
			}
			
			if(isset($_POST['period']) && !empty($_POST['period']>0)){
				foreach($_POST['period'] as $number) {
					$periods .= "'".$number."',";
				}	
				$periods = rtrim($periods,",");
			}
			
			if(isset($_POST['city']) && !empty($_POST['city']>0)){
				foreach($_POST['city'] as $number) {
					$cities .= "'".$number."',";
				}	
				$cities = rtrim($cities,",");
			}
			
			
			$IdCliente = $_POST['LicensesUser']['id_cliente'];
	    	$where = 'WHERE (idLicenciaCliente.id_cliente = "'.$IdCliente.'")';
		 
					    
		    if(isset($_POST['LicensesUser']['id_user']) && strlen($_POST['LicensesUser']['id_user'])>0){
		        $where .= ' and id_usuario like "'.$_POST['LicensesUser']['id_user'].'" ';
		    }
		
		    if(isset($_POST['LicensesUser']['name']) && strlen($_POST['LicensesUser']['name'])>0){
		        $where .= ' and nombres like "%'.$_POST['LicensesUser']['name'].'%" ';
		    }
		    
		    
		    if(isset($_POST['LicensesUser']['surname']) && strlen($_POST['LicensesUser']['surname'])>0){
		        $where .= ' and apellidos like "%'.$_POST['LicensesUser']['surname'].'%" ';
		    }
	
	        
		    if(isset($_POST['LicensesUser']['id_grupo_cliente']) && strlen($_POST['LicensesUser']['id_grupo_cliente'])>0){
		        $where .= ' and id_grupo_cliente = "'.$_POST['LicensesUser']['id_grupo_cliente'].'" ';
		    }	    
		    
		    if(isset($level) && strlen($level)>0){
		        $where .= ' and nivel IN ('.$level.') ';
		    }
		    
		    if(isset($periods) && strlen($periods)>0){
		        $where .= ' and period IN ('.$periods.') ';
		    }
		    
		    if(isset($_POST['LicensesUser']['status']) && strlen($_POST['LicensesUser']['status'])>0){
		        $where .= ' and t.estado = "'.$_POST['LicensesUser']['status'].'" ';
		    }
		    
		    if(isset($_POST['LicensesUser']['id_license']) && strlen($_POST['LicensesUser']['id_license']>0)){
		        $where .= ' and id_licencia = "'.$_POST['LicensesUser']['id_license'].'" ';
		    }
		    
		    if(isset($_POST['LicensesUser']['startDate']) && strlen($_POST['LicensesUser']['startDate']>0)){
		        $where .= ' and t.fecha_presentacion >= "'.$_POST['LicensesUser']['startDate'].'" ';
		        if(isset($_POST['LicensesUser']['endDate']) && strlen($_POST['LicensesUser']['endDate']>0)){
		            $where .= ' and t.fecha_presentacion BETWEEN "'.$_POST['LicensesUser']['startDate'].'" AND "'.$_POST['LicensesUser']['endDate'].'"';
		        }
		    }
		    
		    if(isset($_POST['LicensesUser']['id_program']) && strlen($_POST['LicensesUser']['id_program'])>0){
		        $where .= ' and id_program = "'.$_POST['LicensesUser']['id_program'].'" ';
		    }
		    
		    if(isset($_POST['LicensesUser']['score']) && strlen($_POST['LicensesUser']['score']>0)){
		        $where .= ' and calificacion >= "'.$_POST['LicensesUser']['score'].'" ';
		    }
		    
		    $IdCliente = $_POST['LicensesUser']['id_cliente'];
	        
	        //Si el cliente usa el periodo
	        
	        $SQL = "SELECT id_cliente FROM `users_c_period` WHERE `id_cliente` = '".$IdCliente."' GROUP BY id_cliente";
	        $period = Yii::app()->db->createCommand($SQL)->queryAll();
	       
	        if(!empty($period)){
	        	
	            $SQL = "SELECT id_licencia, t.id_licencias_usuario, id_grupo_cliente, id_usuario, email, nombres, apellidos, t.fecha_presentacion as fecha_presentacion, t.calificacion as calificacion, t.nivel as nivel, t.estado as estado, id_city, id_school, id_program, period FROM `licenses_user` `t` 
	            LEFT OUTER JOIN `users_c` `idUsuario` ON (`t`.`id_usuario`=`idUsuario`.`id_usuario_c`) AND (id_perfil >= 2) 
	            LEFT OUTER JOIN `licenses_client` `idLicenciaCliente` ON (`t`.`id_licencia_cliente`=`idLicenciaCliente`.`id_licencias_cliente`) 
	            LEFT OUTER JOIN `users_c_period` `idLicenciaPeriod` ON (`t`.`id_licencias_usuario`=`idLicenciaPeriod`.`id_licencias_usuario`)
	            $where ORDER BY t.fecha_presentacion DESC";
	            
	            fputcsv($output, array('LICENSE_TYPE', 'LICENSE ID', 'GROUP', 'USER ID','EMAIL','NAME','SURNAME','TEST DATE','SCORE','CEFR LEVEL', 'STATUS', 'CITY', 'SCHOOL', 'PROGRAM', 'PERIOD'));
	        }else{
	        	
	            $SQL = "SELECT id_licencia, t.id_licencias_usuario, id_grupo_cliente, id_usuario_c, email, nombres, apellidos, t.fecha_presentacion as fecha_presentacion, t.calificacion as calificacion, t.nivel as nivel, t.estado as estado FROM `licenses_user` `t`  
	            LEFT OUTER JOIN `users_c` `idUsuario` ON (`t`.`id_usuario`=`idUsuario`.`id_usuario_c`) AND (id_perfil >= 2) 
	            LEFT OUTER JOIN `licenses_client` `idLicenciaCliente` ON (`t`.`id_licencia_cliente`=`idLicenciaCliente`.`id_licencias_cliente`) 
	            $where ORDER BY t.fecha_presentacion DESC";
	            fputcsv($output, array('LICENSE', 'LICENSE ID','GROUP', 'USER ID','EMAIL','NAME','SURNAME','TEST DATE','SCORE','CEFR LEVEL', 'STATUS'));
	        }
	        
	        
	        $list= Yii::app()->db->createCommand($SQL)->queryAll();
			
			$SQL = "SELECT * FROM `groups_users_clients` `t` WHERE id_cliente='".$IdCliente."' ";
	        $list_g= Yii::app()->db->createCommand($SQL)->queryAll();
	        
	        foreach($list_g as $g){
	            $grupos[$g['id']] = $g['nombre'];
	        }
	        
	        
	        $SQL = "SELECT * FROM `licenses` `t`";
	        $list_t= Yii::app()->db->createCommand($SQL)->queryAll();
	        
	        foreach($list_t as $t){
	            $lics[$t['id_licencia']] = $t['nombre'];
	        } 
	        
	        $SQL = "SELECT ID, Name FROM `city` ";
		    $cities= Yii::app()->db->createCommand($SQL)->queryAll();
		        
		    foreach($cities as $c){
		       $cities[$c['ID']] = $c['Name'];
		    }
		    
		    $SQL = "SELECT id_school, name FROM `school` ";
		    $schools= Yii::app()->db->createCommand($SQL)->queryAll();
		        
		    foreach($schools as $sc){
		       $schools[$sc['id_school']] = $sc['name'];
		    }
	    
			$SQL = "SELECT id_program, name FROM `programs` ";
		    $programs= Yii::app()->db->createCommand($SQL)->queryAll();
		        
		    foreach($programs as $pr){
		       $programs[$pr['id_program']] = $pr['name'];
		    }
			
			foreach($list as $rooms){
            
	            $rooms['id_licencia'] = utf8_encode($lics[$rooms['id_licencia']]);
	            $rooms['id_grupo_cliente'] = utf8_decode($grupos[$rooms['id_grupo_cliente']]);
	                        
	            $rooms['nombres'] = utf8_decode($rooms['nombres']);
	            $rooms['apellidos'] = utf8_decode($rooms['apellidos']);
	                               
	            $rooms['nivel'] = utf8_encode($rooms['nivel']);
	            
	            $rooms['estado'] = ($rooms['estado']=="A" ? "Pending" : ($rooms['estado']=="F" ? "Completed" : ""));
	            $rooms['id_city'] = utf8_decode($cities[$rooms['id_city']]);
	            $rooms['id_school'] = utf8_decode($schools[$rooms['id_school']]);
	            $rooms['id_program'] = utf8_decode($programs[$rooms['id_program']]);
	        	fputcsv($output, $rooms);    
	        }
	        
			die();	 
		}
		$this->render('reportDownload',array(
			'model'=>$model));
		
	}
	
	public function actionReportDownloadFuaaDa()
	{	
		
		if(isset($_POST['LicensesUser'])){
			
			// agregamos al log del usuario
			$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 1, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'Download CSV FILE report', '')";
			$log= Yii::app()->db->createCommand($SQL)->query(); 
			
			// output headers so that the file is downloaded rather than displayed
	        header('Content-Type: text/csv; charset=utf-8');
	        header('Content-Disposition: attachment; filename=list-report-'.date('Y_m_d_H_i_s').'.csv');
	        
	        // create a file pointer connected to the output stream
	        $output = fopen('php://output', 'w');
	        
			if(isset($_POST['level']) && !empty($_POST['level']>0)){
				foreach($_POST['level'] as $check) {
					$level .= "'".$check."',";
				}	
				$level = rtrim($level,",");
			}
			
			if(isset($_POST['period']) && !empty($_POST['period']>0)){
				foreach($_POST['period'] as $number) {
					$periods .= "'".$number."',";
				}	
				$periods = rtrim($periods,",");
			}
			
			if(isset($_POST['city']) && !empty($_POST['city']>0)){
				foreach($_POST['city'] as $number) {
					$cities .= "'".$number."',";
				}	
				$cities = rtrim($cities,",");
			}
			
			
			$IdCliente = $_POST['LicensesUser']['id_cliente'];
	    	$where = 'WHERE (idLicenciaCliente.id_cliente LIKE "'.$IdCliente.'")';
		 
					    
		    if(isset($_POST['LicensesUser']['id_user']) && strlen($_POST['LicensesUser']['id_user'])>0){
		        $where .= ' and id_usuario like "'.$_POST['LicensesUser']['id_user'].'" ';
		    }
		
		    if(isset($_POST['LicensesUser']['name']) && strlen($_POST['LicensesUser']['name'])>0){
		        $where .= ' and nombres like "%'.$_POST['LicensesUser']['name'].'%" ';
		    }
		    
		    
		    if(isset($_POST['LicensesUser']['surname']) && strlen($_POST['LicensesUser']['surname'])>0){
		        $where .= ' and apellidos like "%'.$_POST['LicensesUser']['surname'].'%" ';
		    }
	
	        
		    if(isset($_POST['LicensesUser']['id_grupo_cliente']) && strlen($_POST['LicensesUser']['id_grupo_cliente'])>0){
		        $where .= ' and id_grupo_cliente = "'.$_POST['LicensesUser']['id_grupo_cliente'].'" ';
		    }	    
		    
		    if(isset($level) && strlen($level)>0){
		        $where .= ' and nivel IN ('.$level.') ';
		    }
		    
		    if(isset($periods) && strlen($periods)>0){
		        $where .= ' and period IN ('.$periods.') ';
		    }
		    
		    if(isset($_POST['LicensesUser']['status']) && strlen($_POST['LicensesUser']['status'])>0){
		        $where .= ' and t.estado = "'.$_POST['LicensesUser']['status'].'" ';
		    }
		    
		    if(isset($_POST['LicensesUser']['id_license']) && strlen($_POST['LicensesUser']['id_license']>0)){
		        $where .= ' and id_licencia = "'.$_POST['LicensesUser']['id_license'].'" ';
		    }
		    
		    if(isset($_POST['LicensesUser']['startDate']) && strlen($_POST['LicensesUser']['startDate']>0)){
		        $where .= ' and t.fecha_presentacion >= "'.$_POST['LicensesUser']['startDate'].'" ';
		        if(isset($_POST['LicensesUser']['endDate']) && strlen($_POST['LicensesUser']['endDate']>0)){
		            $where .= ' and t.fecha_presentacion BETWEEN "'.$_POST['LicensesUser']['startDate'].'" AND "'.$_POST['LicensesUser']['endDate'].'"';
		        }
		    }
		    
		    if(isset($_POST['LicensesUser']['id_program']) && strlen($_POST['LicensesUser']['id_program'])>0){
		        $where .= ' and id_program = "'.$_POST['LicensesUser']['id_program'].'" ';
		    }
		    
		    if(isset($_POST['LicensesUser']['score']) && strlen($_POST['LicensesUser']['score']>0)){
		        $where .= ' and calificacion >= "'.$_POST['LicensesUser']['score'].'" ';
		    }
		    
		    $IdCliente = $_POST['LicensesUser']['id_cliente'];
		    
	        
	        $SQL = "SELECT id_licencia, t.id_licencias_usuario, id_usuario, email, nombres, apellidos, t.fecha_presentacion as fecha_presentacion, t.calificacion as calificacion, t.nivel as nivel, t.estado as estado, id_city, id_category, id_roll FROM `licenses_user` `t` 
	            LEFT OUTER JOIN `users_c` `idUsuario` ON (`t`.`id_usuario`=`idUsuario`.`id_usuario_c`) AND (id_perfil >= 2) 
	            LEFT OUTER JOIN `licenses_client` `idLicenciaCliente` ON (`t`.`id_licencia_cliente`=`idLicenciaCliente`.`id_licencias_cliente`) 
	            LEFT OUTER JOIN `users_c_period` `idLicenciaPeriod` ON (`t`.`id_licencias_usuario`=`idLicenciaPeriod`.`id_licencias_usuario`)
	            $where ORDER BY t.fecha_presentacion DESC";
	            fputcsv($output, array('LICENSE_TYPE', 'LICENSE ID', 'USER ID','EMAIL','NAME','SURNAME','TEST DATE','SCORE','CEFR LEVEL', 'STATUS', 'CITY', 'CATEGORY', 'ROLL'));
	        
	        $list= Yii::app()->db->createCommand($SQL)->queryAll();
			
			$SQL = "SELECT * FROM `groups_users_clients` `t` WHERE id_cliente='".$IdCliente."' ";
	        $list_g= Yii::app()->db->createCommand($SQL)->queryAll();
	        
	        foreach($list_g as $g){
	            $grupos[$g['id']] = $g['nombre'];
	        }
	        
	        
	        $SQL = "SELECT * FROM `licenses` `t`";
	        $list_t= Yii::app()->db->createCommand($SQL)->queryAll();
	        
	        foreach($list_t as $t){
	            $lics[$t['id_licencia']] = $t['nombre'];
	        } 
	        
	        $SQL = "SELECT ID, Name FROM `city` ";
		    $cities= Yii::app()->db->createCommand($SQL)->queryAll();
		        
		    foreach($cities as $c){
		       $cities[$c['ID']] = $c['Name'];
		    }
		    
		    $SQL = "SELECT id_school, name FROM `school` ";
		    $schools= Yii::app()->db->createCommand($SQL)->queryAll();
		        
		    foreach($schools as $sc){
		       $schools[$sc['id_school']] = $sc['name'];
		    }
	    
			$SQL = "SELECT id_program, name FROM `programs` ";
		    $programs= Yii::app()->db->createCommand($SQL)->queryAll();
		        
		    foreach($programs as $pr){
		       $programs[$pr['id_program']] = $pr['name'];
		    }
			
			foreach($list as $rooms){
            
	            $rooms['id_licencia'] = utf8_encode($lics[$rooms['id_licencia']]);
	            $rooms['id_grupo_cliente'] = utf8_decode($grupos[$rooms['id_grupo_cliente']]);
	                        
	            $rooms['nombres'] = utf8_decode($rooms['nombres']);
	            $rooms['apellidos'] = utf8_decode($rooms['apellidos']);
	                               
	            $rooms['nivel'] = utf8_encode($rooms['nivel']);
	            
	            $rooms['estado'] = ($rooms['estado']=="A" ? "Pending" : ($rooms['estado']=="F" ? "Completed" : ""));
	            $rooms['id_city'] = utf8_decode($cities[$rooms['id_city']]);
	            $rooms['id_category'] = ($rooms['id_category']=="1" ? "Aspirante" : ($rooms['id_category']=="2" ? "Empleado" : ""));
	            $rooms['id_roll'] = ($rooms['id_roll']=="1" ? "Administrativo" : ($rooms['id_roll']=="2" ? "Docente" : ""));
	        	fputcsv($output, $rooms);    
	        }
	        
			die();	 
		}
		$this->render('reportDownload',array(
			'model'=>$model));
		
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return LicensesUser the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=LicensesUser::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param LicensesUser $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='licenses-user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionTecs2goSchedule($mes = NULL,$anio = NULL){
            if(is_null($mes)){
                $mes = date("m");
            }
            if(is_null($anio)){
                $anio = date("Y");
            }
            $mesPost=$mes+1;
            $mesAnte=$mes-1;
            $anioAnte = $anio;
            $anioPost = $anio;
            if($mes==1){
                $mesAnte = 12;
                $anioAnte = date("Y")-1;
            }
            if($mes==12){
                $mesPost = 1;
                $anioPost = $anio+1;
            }
            $usuario = Yii::app()->user->getState('usuario');
            $criteria = new CDbCriteria();
            $criteria->addCondition("month(fecha_presentacion) = $mes and year(fecha_presentacion)=$anio");
            
            $criteria->group = "day(fecha_presentacion), salon";
            if(Yii::app()->user->getState('id_perfil')==4){ //Es un tutor, ver solo sus compromisos
                $criteria->addCondition("tutor = '$usuario'");
            }
           // $model = ExamsUser::model()->findAll($criteria);
		$this->render('tecs2go-schedule',array('model'=>$model,'mes'=>$mes,'anio'=>$anio,'mesAnte'=>$mesAnte,'anioAnte'=>$anioAnte,'mesPost'=>$mesPost,'anioPost'=>$anioPost));
        }
        
         public function actionViewDay($day,$month,$year,$room){
            $usuario = Yii::app()->user->getState('usuario');
            $criteria = new CDbCriteria();
            $criteria->addCondition("month(fecha_presentacion) = $month and year(fecha_presentacion)=$year and day(fecha_presentacion)=$day and salon=$room");
            $criteria->group = "id_licencia_usuario";
            if(Yii::app()->user->getState('id_perfil')==4){ //Es un tutor, ver solo sus compromisos
                $criteria->addCondition("tutor = '$usuario'");
            }
            $model = ExamsUser::model()->findAll($criteria);            
            $nomSalon = ClassRooms::model()->findByPk($room)->nombre;
            $this->renderPartial('viewDay',array('model'=>$model, 'day'=>$day,'month'=>$month,'year'=>$year,'room'=>$room,'salon'=>$nomSalon));
        }
        
         public function actionPrintDay($day,$month,$year,$room,$time){
            
            //close taker vulnerability
        	$profile = Yii::app()->user->getState('id_perfil');
            if($profile == 6){
			    $this->redirect('index.php?r=Clients/default/login&message=12');
			}
			
			//traemos las licencias TECS
			$modUtil = new Utilidades();
			$tecs_ids = $modUtil->getIdsEde();
           
            if(!isset($room)){
                $room = $_REQUEST['room'];
            }
            
            if(!isset($day)){
                $day = $_REQUEST['day'];
            }
            
            if(!isset($month)){
                $month = $_REQUEST['month'];
            }
            
            if(!isset($year)){
                $year = $_REQUEST['year'];
            }
            
            if(!isset($time)){
                $time = $_REQUEST['time'];
            }
            
            
            $nomSalon = ClassRooms::model()->findByPk($room)->nombre;
            
            // ACCESS con GET
            $SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 1, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'Accesss usernames and paswords pdf ID ROOM. ".$_GET['room']."  ".$_GET['day']."-".$_GET['month']."-".$_GET['year']." ".$_GET['time']."', '')";
            $log= Yii::app()->db->createCommand($SQL)->query(); 
            
            
            //require '/home/thetec/public_html/vendor/autoload.php';
            require '/home/iltoexam/public_html/vendor/autoload.php';
					
			$mpdf = new \Mpdf\Mpdf(['tempDir' => __DIR__ . '/../temp']);
			
            $informe='<html><head><style>table.externa{ border-spacing: 10px;width:640px;} td.externa{width:300px;} table.interna{border-width: 3px;border-spacing: 10px;border-style: dashed;border-color: gray;
	border-collapse: collapse;background-color: white; width:300px;} td.interna {border-width: thin; border-style: none;
	border-color: gray; background-color: white; }h2,h3{ text-align:center;}</style></head><body>';
            $informe.= "<h2>USER AND PASSWORD LIST FOR $nomSalon</h2>"; 
            $informe.= "<h3>(MM/DD/AAAA) $month/$day/$year</h3>"; 
            $criteria = new CDbCriteria();
            $criteria->addCondition("month(fecha_presentacion) = $month and year(fecha_presentacion)=$year and day(fecha_presentacion)=$day and salon=$room and hora='$time'");
            $criteria->group = "id_licencia_usuario";
            
            
            
            if(Yii::app()->user->getState('id_perfil')==4){ //Es un tutor, ver solo sus compromisos
            //  q    $criteria->addCondition("tutor = '$usuario'");
            }
            $model = ExamsUser::model()->findAll($criteria);
            $informe .= '<table class="externa">';
            $col=0;
            foreach($model as $registro){
            //Nombre licencia
            $SQL = "SELECT nombre FROM `licenses_client`
                    INNER JOIN licenses_user
                    ON licenses_client.id_licencias_cliente = licenses_user.id_licencia_cliente
                    INNER JOIN licenses
                    ON licenses_client.id_licencia = licenses.id_licencia
                    WHERE licenses_user.id_licencias_usuario = '".$registro->idLicenciaUsuario->id_licencias_usuario."' ";
                    
            $licenseName= Yii::app()->db->createCommand($SQL)->queryAll();
            
            $SQL = "SELECT nombres, apellidos FROM `users_c`WHERE id_usuario_c = '".$registro->idLicenciaUsuario->idUsuario->id_usuario_c."' ";
            $usr = Yii::app()->db->createCommand($SQL)->queryAll();
            
            
            $str = explode("V", $licenseName[0]["nombre"]);
            $licName = $str[0];
                if($col==0){
                    $informe .= "<tr>";
                }
                $informe .= '<td><table class="interna"><tr><td colspan="2" style="font-size:80%;">'.ucfirst($usr[0]["nombres"]).' '.ucfirst($usr[0]["apellidos"]).' please enter <strong>www.iltoexams.com</strong> in your browser, click on the Test Taker button and use the following information:</td></tr>
                <tr><td colspan="2" style="text-align:center;">&nbsp;&nbsp;&nbsp;</td></tr>
                <tr><td colspan="2" style="text-align:center;">'.ucfirst($licName).'&nbsp;&nbsp;&nbsp;</td></tr>
                <tr><td>User:</td><td>'.$registro->idLicenciaUsuario->idUsuario->id_usuario_c.'</td></tr>';
                    
                $ids = explode(",",$tecs_ids);
                $SQL = "SELECT id_licencia FROM `licenses_client` WHERE id_licencias_cliente = '".$registro->idLicenciaUsuario->id_licencia_cliente."'";
                $type = Yii::app()->db->createCommand($SQL)->queryAll();
                
                foreach($ids as $id){
                    if($id == $type[0]["id_licencia"]){
                        $informe .='<tr><td>Password:</td><td>'.$registro->idLicenciaUsuario->idUsuario->clave2.'</td></tr>';
                    }
                }
                $informe .='</table></td>';
                
                $col++;
                if($col==2) {
                    $col=0;
                    $informe .= "</tr>";
                }
            }
            if($col==1) {
                $informe .= "<td></td></tr>";
            }
            $informe.='</table></body></html>';
            $mpdf->WriteHTML($informe);
			$mpdf->Output();
        }
}

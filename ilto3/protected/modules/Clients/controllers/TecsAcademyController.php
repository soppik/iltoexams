<?php

class TecsAcademyController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'requestDetail'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('admin'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
	
	 	$model=new UsersC('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['UsersC']))
			$model->attributes=$_GET['UsersC'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function actionRequestDetail($id)
	{
		
		$this->render('request-detail',array(
			'id'=>$id
		));
	}

}


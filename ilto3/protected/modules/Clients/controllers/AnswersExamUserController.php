<?php

class AnswersExamUserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('exam','takeExam', 'takeExamDemo', 'answer','admin','delete','close','finishExam','myExams', 'randomQuestionsOnline', 'normaliceRandomQuestions', 'remove_element'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

        public function actionMyExams(){
            $userId = Yii::app()->user->getState('usuario');
            $modLicUs = LicensesUser::model()->findByAttributes(array('id_usuario'=>$userId));
            $license = $modLicUs->id_licencias_usuario;
		    $criteria=new CDbCriteria;
		    $criteria->addcondition('id_licencia_usuario = '.$license.' and estado != "F"');
            $examenes = ExamsUser::model()->findAll($criteria);
            $examUserId=NULL;
            if(count($examenes)>0){
                $examUserId = $examenes[0]->id_examen_usuario;
                $this->render('myExams',array('userId'=>$userId,'license'=>$license,'examUserId'=>$examUserId));
            } else {
                $this->render('noExams');
            }
        }
        
        public function actionAnswer()
	{
		$model=new AnswersExamUser;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AnswersExamUser']))
		{
			$model->attributes=$_POST['AnswersExamUser'];
                        //$mod2 = AnswersExamUser::model()->findByAttributes(array(''))
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

        /**
	 * Exam ==> Generar el exámen del usuario de acuerdo a lo que tenga asignado o de acuerdo al proceso que lleve.
	 * Si no ha terminado el exámen debe ponerlo en el punto en que vaya
	 */
	public function actionExam($userId,$license=NULL,$now=NULL)
	{
            //Identificar si el usuario ya ha presentado algun exámen y enviarlo al ultimo punto
            //Identificar si es el examen oral que debe tomar el tutor para enviarlo de una vez a presentar
            if(!is_null($license)){
                //Aquí viene cuando acaba de ser creado y le van a tomar el examen de una vez
                $modExamUser = ExamsUser::model()->findAllByAttributes(array('id_licencia_usuario'=>$license));
                foreach($modExamUser as $examen){
                    if($examen->idExamen->tutor=="1"){
                        //Presentar el Examen
                        $this->actionTakeExam($userId,$license,$examen->id_examen_usuario);
                        break;
                    }
                }
            } else {
                //Verificar el estado en el que el usuario se encuentra 
            }
	}
        
     /**
	 * TakeExam ==> Presentar el Exámen
	 * Si no ha terminado el exámen debe ponerlo en el punto en que vaya
	 */
        public function actionTakeExam($y,$p,$b){
            
            //decode variables for execute algoritm
            $userId = base64_decode($y);
            $license = base64_decode($p);
            $examUserId = base64_decode($b);
            
            
            $modUtil = new Utilidades();
            
            if(isset($_POST["phone"])){
                //actualizamos terminos,
                $SQL = "UPDATE `users_c` SET `telefono` = '".$_POST["phone"]."' WHERE `id_usuario_c`='".$userId."'";
                $list= Yii::app()->db->createCommand($SQL)->query();
                
            }
            // save pc info
            $ip= isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? 
            $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
            $dns = gethostbyaddr($_SERVER['REMOTE_ADDR']);  
            
            //revisamos si ya está registrado el equipo 
            //validamcioin FRAUD ALERT
            $sql='SELECT id_host_license FROM `host_license` WHERE `id_licencia_usuario` = "'.$license.'" AND `ip_client` = "'.$ip.'"';
            $check_register= Yii::app()->db->createCommand($sql)->queryAll();
            
            
            $check= array($check_register[0]['id_host_license']);
            
            if(is_null($check[0])){
                 //insertamos fecha de inicio examen, ip cliente y dns
                $SQL = "INSERT INTO  `host_license` (`id_licencia_usuario`, `fecha_inicio_examen`, `ip_client`, `dns_client`)  VALUES ('".$license."', NOW(), '".$ip."', '".$dns."')";
                $host= Yii::app()->db->createCommand($SQL)->query();
            }
            
            //debemos obtener el último valor de Speaking
            //Verificar cuantas preguntas tiene el examen
            //Mirar si las preguntas son fijas o aleatorias
            //Ver cuantas preguntas hay pendientes
            $modExamsUser = ExamsUser::model()->findByPk($examUserId);
            $muestraIntro = Licenses::model()->findByPk($modExamsUser->idLicenciaUsuario->idLicenciaCliente->id_licencia)->intro_secciones;
            $tipologo = Licenses::model()->findByPk($modExamsUser->idLicenciaUsuario->idLicenciaCliente->id_licencia)->logo;
            $examId = $modExamsUser->id_examen;
            $modExamen = Exams::model()->findByPk($modExamsUser->id_examen);
            $totalPreguntas = $modExamen->numero_preguntas;
            $totalContestadas = 0;
            $speakingId = $modUtil->getIdSpeaking();
            $roboFlag = 0;
            
            // si es un rater el que abre el examen, guardamos 
            if(Yii::app()->user->getState('id_perfil')=='6'){
            
                if($examId == 23 || $examId == 35 || $examId == 40 || $examId == $speakingId){
                    //almacenamos el seguimiento al que abre el speaking y no es rater
                    $roboFlag = 1;
                    $SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`) VALUES (NULL, '2', '".Yii::app()->user->getState('numero_id')."', NOW(), '".$license."', '".$examUserId."', 'start speaking exam by no tutor')";
                    $log= Yii::app()->db->createCommand($SQL)->query(); 
                    $this->redirect('index.php?r=Clients/default/login');
                    
                }
            }
            
            //llamamos a robocop para que revise y haga las notificaciones necesarias
            $modUtil->robocop($userId,$license,$examUserId,$roboFlag);
            //actualizamos terminos,
            $SQL = "UPDATE `licenses_user` SET `terms` = '1' WHERE `licenses_user`.`id_licencias_usuario` ='".$license."'";
            $list= Yii::app()->db->createCommand($SQL)->query();
            
            //actualizamos telefono si viene,
            
            $SQL = "UPDATE `licenses_user` SET `terms` = '1' WHERE `licenses_user`.`id_licencias_usuario` ='".$license."'";
            $list= Yii::app()->db->createCommand($SQL)->query();
            
            
            
            
            
            //consultamos el nombre del examen (primer palabra, Grammar or Speaking, or reading or listening)
            $eName = $modExamen->nombre;
            $ePreguntas = $modExamen->numero_preguntas;
            
            $nomExamen = explode(" ", $eName);
            $eName = strtolower($nomExamen[0]); 
            
            $globalModPeguntas;
            
            //get exam type
            $examName = explode(" ",$modExamen["nombre"]);
            
            /* 
                CHECK LICENSE TYPE
                FOR LAUNCH QUESTIONS 
            */
            
            if($modUtil->isTecs($license) == true){
                $ids_license = "23,25";
            }
            
            if($modUtil->isEde($license) == true){
                $ids_license = "21,27";
            }
            
            
            //get 3 last id of this exam type
            $SQL = 'SELECT exams.id_examen FROM `exams` INNER JOIN exams_license ON exams.id_examen = exams_license.id_examen WHERE `nombre` LIKE "%'.$examName[0].'%" AND exams_license.id_licencia IN ('.$ids_license.') ORDER BY exams.id_examen DESC LIMIT 2 ';
            $id_exams= Yii::app()->db->createCommand($SQL)->queryAll();
            
            $idsOutArray = "";
    	    foreach ($id_exams as $id) {
    	        $idsOutArray .= $id["id_examen"].",";
    	    }
    	    
    	    //convert Array ids to string for the SQL IN condition
	        $idsOutArray = substr($idsOutArray, 0, -1);
            
            /*get ramdon Question from these exams
            / parameters 1. exams from License 2. number of questions for this exam, $id ExamUser
            /
            */
            $ramdonQuestions = $this->randomQuestionsOnline($id_exams, $ePreguntas, $examUserId);
            
            if(!isset($_POST['preguntas'])){ 
                //Empezar o Continuar con el Examen
                //Si las preguntas NO son aleatorias
               
                if($modExamen->preguntas_aleatorias==0){
                    
                    //Seleccionar todas las preguntas del examen
                    $modPeguntas = QuestionsExam::model()->findAllByAttributes(array('id_examen'=>$examId));
                    $modPeguntas = $ramdonQuestions;
                    $modContestadas = AnswersExamUser::model()->findAllByAttributes(array('id_examen_usuario'=>$examUserId));
                    $totalContestadas = count($modContestadas);
                    
                    if($totalContestadas>0){
                        //Continuar con el examen porque algo paso
                        $arregloContestadas =  array_keys(CHtml::listData(AnswersExamUser::model()->findAllByAttributes(array('id_examen_usuario'=>$examUserId)),'id_pregunta','id_respuesta'));
                        //Seleccionar todas las preguntas del examen que no se hayan contestado
                        $contestadas = "('".implode("','",$arregloContestadas)."')";
                        $criterio = new CDbCriteria;
                        $criterio->select = '*';
                        $criterio->with ="idPregunta";
                        $criterio->condition = 'id_examen = '.$examId.' and t.id_pregunta not in '.$contestadas;
                        if(!is_null(Yii::app()->user->getState('relacionada'))){
                            $criterio->condition.= ' and relacionada='.Yii::app()->user->getState('relacionada');
                            $modPeguntas = QuestionsExam::model()->findAll($criterio);
                            if(count($modPeguntas)<=0){ 
                                Yii::app()->user->setState('relacionada',NULL);
                                $criterio->condition = 'id_examen = '.$examId.' and t.id_pregunta not in '.$contestadas;
                                $modPeguntas = QuestionsExam::model()->findAll($criterio);
                                
                            }
                        }
                        
                        if(count($modPeguntas)>0){
                            if($modExamen->vistaunoauno==1){
                                //shuffle($modPeguntas);
                            } else {
                            }
                            $model = new AnswersExamUser;
                            
                            switch ($tipologo) {
                                case 1:
                                    $rutalogo = "images/TECS.png";
                                    break;
                                case 2:
                                    $rutalogo = "images/RPT.png";
                                    break;
                                case 3:
                                     $rutalogo = "images/EDE.png";
                                    break;
                                case 4:
                                     $rutalogo = "images/cedt.png";
                                    break;
                                default:
                                     $rutalogo = "images/EDE.png";
                            }
                            
                            Yii::app()->user->setState('rutalogoexam',$rutalogo);
                            if($muestraIntro == 1){
                                $muestraIntro = true;
                            } else {
                                $muestraIntro=false;
                            }
                            $this->render('askQuestion',array(
                            'model'=>$model,'unoauno'=>$modExamen->vistaunoauno,'totalP'=>$totalPreguntas,'totalC'=>$totalContestadas,
                                'modpreguntas'=>$modPeguntas,'titulo'=>$modExamen->nombre,'muestraIntro'=>$muestraIntro,
                                'bgColor'=>$modExamen->idSeccion->bg_color,'txtColor'=>$modExamen->idSeccion->txt_color
                            ));
                        } else {
                            //No hay mas preguntas
                            $this->actionFinishExam($userId,$license,$examUserId);
                        }                       
                    } else {
                        //Seleccionar la pregunta a realizar
                        //Empezar con el examen por primera vez.
                        if($modExamen->vistaunoauno==1){
                           // shuffle($modPeguntas);
                        } 
                        
                        switch ($tipologo) {
                                case 1:
                                    $rutalogo = "images/TECS.png";
                                    break;
                                case 2:
                                    $rutalogo = "images/RPT.png";
                                    break;
                                case 3:
                                     $rutalogo = "images/EDE.png";
                                    break;
                                case 4:
                                     $rutalogo = "images/cedt.png";
                                    break;
                                default:
                                     $rutalogo = "images/EDE.png";
                            }
                        
                        Yii::app()->user->setState('rutalogoexam',$rutalogo);
                            if($muestraIntro == 1){
                                $muestraIntro = true;
                            } else {
                                $muestraIntro=false;
                            }
                        $this->render('askQuestion',array(
                        'model'=>$model,'unoauno'=>$modExamen->vistaunoauno,'totalP'=>$totalPreguntas,'totalC'=>$totalContestadas,
                            'modpreguntas'=>$modPeguntas,'titulo'=>$modExamen->nombre,
                            'bgColor'=>$modExamen->idSeccion->bg_color,'txtColor'=>$modExamen->idSeccion->txt_color,'muestraIntro'=>$muestraIntro
                        ));
                    }
                } else { 
                    
                    //aqui cae speaking y reading
                    // si es un rater el que abre el examen, guardamos 
                    if(Yii::app()->user->getState('id_perfil')!='6'){
                        //almacenamos el seguimiento al tutor que ejecuta el speaking
                        //insertamos fecha de inicio examen, ip cliente y dns
                        // ACCESS con GET
                        $SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 4, '".Yii::app()->user->getState('usuario')."', NOW(), '".$license."', '".$examUserId."', 'Open Speaking test of test taker', '')";
                        $log= Yii::app()->db->createCommand($SQL)->query(); 
                        
                    }
                    
                    
                    //get Speaking Exam IDs
                    $sql='SELECT id_examen FROM `exams` WHERE `nombre` LIKE "%speaking%"';
                    $ids_query= Yii::app()->db->createCommand($sql)->queryAll();
                    
                    //save ids in a string to concatenate in SQL QUERY
                    foreach($ids_query as $id){
                        $ids .= $id["id_examen"].",";
                    }
                    
                    //REMOVE LAS CHARACTER OF THE STRING
                    $ids = substr($ids, 0, -1);
                    $id = $ids;
                    
                    //GET at least 1 answer for this speaking exam
                    $sql='SELECT COUNT(id_respuesta_examen_usuario) AS total FROM `answers_exam_user` 
                    INNER JOIN exams_user ON answers_exam_user.id_examen_usuario = exams_user.id_examen_usuario
                    WHERE answers_exam_user.`id_examen_usuario` = "'.$examUserId.'"
                    AND exams_user.id_examen IN ('.$id.')';  
                    $total= Yii::app()->db->createCommand($sql)->queryAll();
                    
                    // iF there are at least 1 question answered we close the exam
                    if($total[0]["total"] > 0){
                        $this->renderPartial('closeExam');
                    }else {
                           //Si las preguntas son aleatorias
                        //Seleccionar todas las preguntas del examen
                        $modPeguntas = QuestionsExam::model()->findAllByAttributes(array('id_examen'=>$examId));
                        
                        
                        // Al consultar todas las preguntas, procedemos a sacar 6 randomicamente
                        // por nivel
                       //Validación randomica para examenes que comiencen con speaking en el nombre
                       
                        $prexNivel = intval($ePreguntas/5);
                        $finalArray = $this->randomQuestions($modPeguntas, $prexNivel);
                        $modPeguntas = $finalArray;
                        
                        $modContestadas = AnswersExamUser::model()->findAllByAttributes(array('id_examen_usuario'=>$examUserId));
                        
                        $totalContestadas = count($modContestadas);
                        
                        if($totalContestadas>0){
                            
                            //revisarlo
                            //Continuar con el examen porque algo paso
                            $arregloContestadas =  array_keys(CHtml::listData(AnswersExamUser::model()->findAllByAttributes(array('id_examen_usuario'=>$examUserId)),'id_pregunta','id_respuesta'));
                            
                            //Seleccionar todas las preguntas del examen que no se hayan contestado
                            $contestadas = "('".implode("','",$arregloContestadas)."')";
                            
                            $criterio = new CDbCriteria;
                            $criterio->select = '*';
                            $criterio->with ="idPregunta";
                            $criterio->condition = 'id_examen = '.$examId.' and t.id_pregunta not in '.$contestadas;
                            
                            //preguntas relacionadas
                            if(!is_null(Yii::app()->user->getState('relacionada'))){
                                $criterio->condition.= ' and relacionada='.Yii::app()->user->getState('relacionada');
                                $modPeguntas = QuestionsExam::model()->findAll($criterio);
                                $texto=" es este: ".print_r($criterio,true);
                                Yii::app()->user->setState('txtquery',$texto);
                                if(count($modPeguntas)<=0){ 
                                    Yii::app()->user->setState('relacionada',NULL);
                                    $criterio->condition = 'id_examen = '.$examId.' and t.id_pregunta not in '.$contestadas;
                                    $modPeguntas = QuestionsExam::model()->findAll($criterio);
                                }
                            }
                            
                            
                            if(count($modPeguntas)>0){
                                
                                if($modExamen->vistaunoauno==1){
                                    
                                    //order del array ramdomico
                                   // shuffle($modPeguntas);
                                } 
                                
                                
                                $model = new AnswersExamUser;
                                switch ($tipologo) {
                                    case 1:
                                        $rutalogo = "images/TECS.png";
                                        break;
                                    case 2:
                                        $rutalogo = "images/RPT.png";
                                        break;
                                    case 3:
                                         $rutalogo = "images/EDE.png";
                                        break;
                                    case 4:
                                         $rutalogo = "images/cedt.png";
                                        break;
                                    default:
                                         $rutalogo = "images/EDE.png";
                                }
                                Yii::app()->user->setState('rutalogoexam',$rutalogo);
                                if($muestraIntro == 1){
                                    $muestraIntro = true;
                                } else {
                                    $muestraIntro=false;
                                }
                                $this->render('askQuestion',array(
                                'model'=>$model,'unoauno'=>$modExamen->vistaunoauno,'totalP'=>$totalPreguntas,'totalC'=>$totalContestadas,
                                    'modpreguntas'=>$modPeguntas,'titulo'=>$modExamen->nombre,'muestraIntro'=>$muestraIntro,
                                    'bgColor'=>$modExamen->idSeccion->bg_color,'txtColor'=>$modExamen->idSeccion->txt_color
                                ));
                            } else {
                                //No hay mas preguntas
                                $this->actionFinishExam($userId,$license,$examUserId);
                            }                       
                        } else {
                            //Seleccionar la pregunta a realizar
                            //Empezar con el examen por primera vez.
                            if($modExamen->vistaunoauno==1){
                               // shuffle($modPeguntas);
                            }
                            $model = new AnswersExamUser;
                                switch ($tipologo) {
                                    case 1:
                                        $rutalogo = "images/TECS.png";
                                        break;
                                    case 2:
                                        $rutalogo = "images/RPT.png";
                                        break;
                                    case 3:
                                         $rutalogo = "images/EDE.png";
                                        break;
                                    case 4:
                                         $rutalogo = "images/cedt.png";
                                        break;
                                    default:
                                         $rutalogo = "images/EDE.png";
                                }
                            Yii::app()->user->setState('rutalogoexam',$rutalogo);
                                if($muestraIntro == 1){
                                    $muestraIntro = true;
                                } else {
                                    $muestraIntro=false;
                                }
                            $this->render('askQuestion',array(
                            'model'=>$model,'unoauno'=>$modExamen->vistaunoauno,'totalP'=>$totalPreguntas,'totalC'=>$totalContestadas,
                                'modpreguntas'=>$modPeguntas,'titulo'=>$modExamen->nombre,
                                'bgColor'=>$modExamen->idSeccion->bg_color,'txtColor'=>$modExamen->idSeccion->txt_color,'muestraIntro'=>$muestraIntro
                            ));
                        }
                    } 
                    }
                    
                    
            } else { 
                
               /*  //aca guardamos el resultado de las respuestas speaking
               
                    echo "<pre>";
                
                var_dump($_POST);die();*/
                //Guardar las respuestas
                
                foreach($_POST['preguntas'] as $pregunta=>$respuesta){
                    $modRespuestas = new AnswersExamUser;
                    $modRespuestas->id_pregunta = $pregunta;
                    $modRespuestas->date_answered = date('Y-m-d H:i:s');
                    if($modRespuestas->idPregunta->tipo_presentacion=="S"){
                        $modRespuestas->escala = $respuesta;
                    } else {
                        
                        if($respuesta == -1){
                            $modRespuestas->id_respuesta = NULL;
                        } else {
                            $modRespuestas->id_respuesta = $respuesta;
                        }
                    }
                    $modRespuestas->id_examen_usuario = $examUserId;
                    try{
                       
                        $modRespuestas->save();
                    }
                    catch(Exception $e){}
                }
                
                
               /* echo "<pre>";
                echo "---------";
                 
                foreach ($ramdonQuestions as $question) {
                    
                   $key = array_search($str, $question);
                   
                    if($str === $question["id_pregunta"]){
                        echo "coninciden";
                        var_dump($str, $question["id_pregunta"]);
                    }
                    
                }
                
                    
                die();
                var_dump("ya guardamos la primera pregunta, deberíamos sacarla del ArrayFinal");die();
                */
                
                if($modExamen->vistaunoauno==1){ 
                    //Viene una sola pregunta
                    //Continuar con la siguiente pregunta
                    
                    if($modExamen->preguntas_aleatorias==0){
                        
                        //Preguntas contestadas
                        $arregloContestadas =  array_keys(CHtml::listData(AnswersExamUser::model()->findAllByAttributes(array('id_examen_usuario'=>$examUserId)),'id_pregunta','id_respuesta'));
                        //Seleccionar todas las preguntas del examen que no se hayan contestado
                        $contestadas = "('".implode("','",$arregloContestadas)."')";
                        $totalContestadas = count($arregloContestadas);
                         
                        // finish exam if we got all the questions
                        if($totalContestadas >= $totalPreguntas){
                           
                           $this->actionFinishExam($userId,$license,$examUserId);
                        }
                        
                        $criterio = new CDbCriteria;
                        $criterio->select = '*';
                        $criterio->condition = 'id_examen = '.$examId.' and id_pregunta not in '.$contestadas;
                        $criterio->with ="idPregunta";
                        $criterio->condition = 'id_examen = '.$examId.' and t.id_pregunta not in '.$contestadas;
                        if(!is_null(Yii::app()->user->getState('relacionada'))){
                            $criterio->condition.= ' and relacionada='.Yii::app()->user->getState('relacionada');
                            $modPeguntas = QuestionsExam::model()->findAll($criterio);
                            $modPeguntas  = $this->randomQuestionsOnline($id_exams, $ePreguntas, $examUserId);
                            $texto=" es este: ".print_r($criterio,true);
                            Yii::app()->user->setState('txtquery',$texto);
                            if(count($modPeguntas)<=0){ 
                                Yii::app()->user->setState('relacionada',NULL);
                                $criterio->condition = 'id_examen = '.$examId.' and t.id_pregunta not in '.$contestadas;
                                $modPeguntas = QuestionsExam::model()->findAll($criterio);
                                $modPeguntas = $this->randomQuestionsOnline($id_exams, $ePreguntas, $examUserId);
                                
                                
                            }
                        } else {
                            $modPeguntas = QuestionsExam::model()->findAll($criterio);
                            $modPeguntas =  $this->randomQuestionsOnline($id_exams, $ePreguntas, $examUserId);
                            
                        }
                        if(count($modPeguntas)>0){
                            if($modExamen->vistaunoauno==1){
                               // shuffle($modPeguntas);
                            }
                            
                            $model = new AnswersExamUser;
                            switch ($tipologo) {
                                case 1:
                                    $rutalogo = "images/TECS.png";
                                    break;
                                case 2:
                                    $rutalogo = "images/RPT.png";
                                    break;
                                case 3:
                                     $rutalogo = "images/EDE.png";
                                    break;
                                case 4:
                                     $rutalogo = "images/cedt.png";
                                    break;
                                default:
                                     $rutalogo = "images/EDE.png";
                            }
                            Yii::app()->user->setState('rutalogoexam',$rutalogo);
                            $this->render('askQuestion',array(
                            'model'=>$model,'unoauno'=>$modExamen->vistaunoauno,'totalP'=>$totalPreguntas,'totalC'=>$totalContestadas,
                                'modpreguntas'=>$modPeguntas,'titulo'=>$modExamen->nombre,
                                'bgColor'=>$modExamen->idSeccion->bg_color,'txtColor'=>$modExamen->idSeccion->txt_color,'muestraIntro'=>false
                            ));
                        } else {
                            //No hay mas preguntas
                            $this->actionFinishExam($userId,$license,$examUserId);
                        }
                    } else {
                        
                       
                        //Preguntas contestadas
                        $arregloContestadas =  array_keys(CHtml::listData(AnswersExamUser::model()->findAllByAttributes(array('id_examen_usuario'=>$examUserId)),'id_pregunta','id_respuesta'));
                        //Seleccionar todas las preguntas del examen que no se hayan contestado
                        
                        $contestadas = "('".implode("','",$arregloContestadas)."')";
                        $totalContestadas = count($arregloContestadas);
                        
                        if($totalContestadas == intval($ePreguntas)){
                            //$this->actionFinishExam($userId,$license,$examUserId);
                        }
                       
                        $criterio = new CDbCriteria;
                        $criterio->select = '*';
                        $criterio->condition = 'id_examen IN ('.$idsOutArray.') and id_pregunta not in '.$contestadas;
                        $criterio->with ="idPregunta";
                        $criterio->condition = 'id_examen IN ('.$idsOutArray.') and t.id_pregunta not in '.$contestadas;
                       
                        if(!is_null(Yii::app()->user->getState('relacionada'))){
                            
                            $criterio->condition.= ' and relacionada='.Yii::app()->user->getState('relacionada');
                            $modPeguntas = QuestionsExam::model()->findAll($criterio);
                            $modPeguntas =  $this->randomQuestionsOnline($id_exams, $ePreguntas, $examUserId);
                            $texto=" es este: ".print_r($criterio,true);
                            Yii::app()->user->setState('txtquery',$texto);
                            if(count($modPeguntas)<=0){ 
                                Yii::app()->user->setState('relacionada',NULL);
                                $criterio->condition = 'id_examen IN ('.$idsOutArray.') and t.id_pregunta not in '.$contestadas;
                                $modPeguntas = QuestionsExam::model()->findAll($criterio);
                                $modPeguntas =  $this->randomQuestionsOnline($id_exams, $ePreguntas, $examUserId);
                            }
                        } else {
                            
                            $modPeguntas =  $this->randomQuestionsOnline($id_exams, $ePreguntas, $examUserId);
                            
                            
                            //$modPeguntas = QuestionsExam::model()->findAll($criterio);
                            
                        }
                        if(count($modPeguntas)>0){
                            if($modExamen->vistaunoauno==1){
                               // shuffle($modPeguntas);
                            }
                            $model = new AnswersExamUser;
                            switch ($tipologo) {
                                case 1:
                                    $rutalogo = "images/TECS.png";
                                    break;
                                case 2:
                                    $rutalogo = "images/RPT.png";
                                    break;
                                case 3:
                                     $rutalogo = "images/EDE.png";
                                    break;
                                case 4:
                                     $rutalogo = "images/cedt.png";
                                    break;
                                default:
                                     $rutalogo = "images/EDE.png";
                            }
                            Yii::app()->user->setState('rutalogoexam',$rutalogo);
                            $this->render('askQuestion',array(
                            'model'=>$model,'unoauno'=>$modExamen->vistaunoauno,'totalP'=>$totalPreguntas,'totalC'=>$totalContestadas,
                                'modpreguntas'=>$modPeguntas,'titulo'=>$modExamen->nombre,
                                'bgColor'=>$modExamen->idSeccion->bg_color,'txtColor'=>$modExamen->idSeccion->txt_color,'muestraIntro'=>false
                            ));
                        } else {
                            //No hay mas preguntas
                            $this->actionFinishExam($userId,$license,$examUserId);
                        }
                    }
                } else {
                    $this->actionFinishExam($userId,$license,$examUserId);
                }
            }
        }
        
        
        /*  new algorithm to take exam
        *
        *
        * ha terminado el exámen debe ponerlo en el punto en que vaya
        */
        
        
        
        public function actionFinishExam($y,$p,$b){
            
            
            //decode variables for execute algoritm
            $userId = $y;
            $license = $p;
            $examUserId = $b;
            
            //Calificar el exámen y darlo por terminado para el usuario en esa licencia
            $this->calificarExamen($examUserId);
            
           
            
            //Si el que llega calificando es un TAKER hay que enviarle el siguiente exámen
            if(Yii::app()->user->getState('id_perfil')==6){
                
            $modExamsUser = ExamsUser::model()->findAllByAttributes(array('id_licencia_usuario'=>$license));
        
            $criteria=new CDbCriteria;
		    $criteria->addcondition('id_licencia_usuario = '.$license.' and estado != "F"');
            $examenes = ExamsUser::model()->findAll($criteria);
            
            //verify DEMO ID
            $SQL = "SELECT id_licencia FROM licenses_user INNER JOIN licenses_client ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente WHERE id_licencias_usuario ='".$license."'";
            $license_id= Yii::app()->db->createCommand($SQL)->queryAll();
                
         
            if(count($examenes)>0){
                    $examUserId = $examenes[0]->id_examen_usuario;
                    unset($_POST['preguntas']);
                    if($license_id[0]["id_licencia"] == 5){
                        $this->redirect('index.php?r=Clients/AnswersExamUser/takeExamDemo&y='.base64_encode($userId).'&p='.base64_encode($license).'&b='.base64_encode($examUserId));
                    }else{
                        $this->redirect('index.php?r=Clients/AnswersExamUser/takeExam&y='.base64_encode($userId).'&p='.base64_encode($license).'&b='.base64_encode($examUserId));  
                    }
                    
                } else {
                    $modUtil = new Utilidades();
                    $modLicenciaUsuario = LicensesUser::model()->findByAttributes(array('id_licencias_usuario'=>$license));
                    $modLicenciaUsuario->estado="F";
                    $resultados= $modUtil->calificaLicencia($license);
                    $modLicenciaUsuario->calificacion= $resultados['calificacion'];
                    $modLicenciaUsuario->nivel= $resultados['nivel'];
                    $modLicenciaUsuario->estado= $resultados['estado'];
                    $modLicenciaUsuario->save(true,array('estado','nivel','calificacion'));
                    $modUtil = new Utilidades;
                     //check if is EDE
                        $is_ede = $modUtil->isEde($modLicenciaUsuario->id_licencia_cliente);
                       
                        //if is EDE send certificate
                        if($is_ede == true){
                         //  $modUtil->enviarCertificado($userId,$license);
                        }else{
                            
                            //if is TECS we must to check
                            if($modLicenciaUsuario->idLicenciaCliente->idLicencia->genera_certificado==2){
                                $modCliente = UsersC::model()->findByAttributes(array('id_cliente'=>Yii::app()->user->getState('cliente'), 'id_usuario_c'=> $userId));
                                if($modCliente->email_certificate == 1){
                                    //$modUtil->enviarCertificado($userId,$license);
                                }
                            } else {
                                Yii::app()->user->setFlash('success','Ask your Test Administrator for the test results');
                            }
                        }
                   
                    $this->renderPartial('closeExam');
                }
             }else {
                //Si es un Tutor el que manda
               $modUsuario = UsersC::model()->findByAttributes(array('id_usuario_c'=>$userId));
                // ADM LOG
                $SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 4, '".Yii::app()->user->getState('usuario')."', NOW(), '".$license."', '".$examUserId."', 'Rate speaking test', '".$userId."')";
                $log= Yii::app()->db->createCommand($SQL)->query(); 
               if($modUsuario->estado=="P"){ //Pendiente de registro
                   $modUtil = new Utilidades();
                   //Enviar registro al usuario
                   //$modUtil->actionEnviarRegistro($userId);
                   //Actualizar el estado
                   $modUsuario->estado="R";
                   $modUsuario->save(true,array('estado'));
               }
               
               $this->renderPartial('closeExam',array('licencia'=>$license));
            }
        }
        
        public function actionClose(){
            
            $this->renderPartial('closeExam');

        }
        /**
         * 
         * @param type $examUserId
         */
        public function calificarExamen($examUserId){
            //Seleccionar todas las preguntas del examen
            $modUtil = new Utilidades();
            $modPreguntasEx = AnswersExamUser::model()->findAllByAttributes(array('id_examen_usuario'=>$examUserId));
            $numEsc=0;
            $acumEsc=0;
            $numAciertos=0;
            $acumAciertos=0;
            foreach($modPreguntasEx as $rowPregunta){
                if($rowPregunta->idPregunta->tipo_presentacion=="S"){
                    $acumEsc+=$rowPregunta->escala;
                    $numEsc++;
//                    echo "<br>sumando ".$rowPregunta->escala;
                } else {
                    $modResp = Answers::model()->findByPk($rowPregunta->id_respuesta);
                    if($modResp->es_verdadera==1){
                        $acumAciertos++;
                    }
                    $numAciertos++;
                }
            }
            //Guardar Calificacion
            $modExamUser = ExamsUser::model()->findByPk($examUserId);
//             echo "<br>total escalas ".$numEsc;
//             echo "<br>acum escalas ".$acumEsc;
//             die();
            if($numEsc>0){
                $modExamUser->calificacion = $acumEsc/$numEsc;
            }else{
                $modExamUser->calificacion = ($acumAciertos/$numAciertos)*100;
            }
            $modExamUser->nivel = $modUtil->nivelExamen($modExamUser->calificacion);
            $modExamUser->estado="F";
            $modExamUser->save();
            

            
        }
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AnswersExamUser']))
		{
			$model->attributes=$_POST['AnswersExamUser'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('AnswersExamUser');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new AnswersExamUser('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['AnswersExamUser']))
			$model->attributes=$_GET['AnswersExamUser'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return AnswersExamUser the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=AnswersExamUser::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param AnswersExamUser $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='answers-exam-user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function randomQuestions($modPeguntas, $num){
	    
	    $arrayA1 = array();
        $arrayA2 = array();
        $arrayB1 = array();
        $arrayB2 = array();
        $arrayC1 = array();
        $finalArray = array();
        
    
        
        foreach ($modPeguntas as $value) {
            $pregunta = Questions::model()->findByPk(array($value["id_pregunta"]));
            
            
            if($pregunta->nivel == 'A1'){
                if(!in_array($value, $arrayA1, true)){
                    array_push($arrayA1, $value);    
                }
                
            }
            if($pregunta->nivel == 'A2'){
                if(!in_array($value, $arrayA2, true)){
                    array_push($arrayA2, $value);    
                }
            }
            if($pregunta->nivel == 'B1'){
                if(!in_array($value, $arrayB1, true)){
                    array_push($arrayB1, $value);    
                }
            }
            if($pregunta->nivel == 'B2'){
                if(!in_array($value, $arrayB2, true)){
                    array_push($arrayB2, $value);    
                }
            }
            if($pregunta->nivel == 'C1'){
                if(!in_array($value, $arrayC1, true)){
                    array_push($arrayC1, $value);    
                }
            }
        }
        
        //sacar ids de preguntas seleccionadas en un array
        // Nivel A1 5 random
        $ra1 = array_rand($arrayA1, $num); 
       
        for($i = 0; $i<$num; $i++){
            array_push($finalArray, $arrayA1[$ra1[$i]]);
        }
        // Nivel A2 5 random
        $ra2 = array_rand($arrayA2, $num);
        for($i = 0; $i<$num; $i++){
            array_push($finalArray, $arrayA2[$ra2[$i]]);
        }
        // Nivel B1 5 random
        $rb1 = array_rand($arrayB1, $num);
        for($i = 0; $i<$num; $i++){
            array_push($finalArray, $arrayB1[$rb1[$i]]);
        }
        
        // Nivel B2 5 random
        $rb2 = array_rand($arrayB2, $num);
        for($i = 0; $i<$num; $i++){
            array_push($finalArray, $arrayB2[$rb2[$i]]);
        }
        // Nivel C1 5 random
        $rc1 = array_rand($arrayC1, $num);
        for($i = 0; $i<$num; $i++){
            array_push($finalArray, $arrayC1[$rc1[$i]]);
        }
        
        /*
        
         foreach($finalArray as $value){
            var_dump($value["id_pregunta"]);
            $preg = Questions::model()->findByPk(array('id_pregunta'=>$value["id_pregunta"]));
            var_dump($preg->nivel);
        }
          */  
        

        return $finalArray; 
	}
	
	public function randomQuestionsOnline($ids_exam, $totalQuestions, $id_examUser){
	    //get 3 last id of this exam type
	    $idsOutArray = "";
	    foreach ($ids_exam as $id) {
	        $idsOutArray .= $id["id_examen"].",";
	    }
	    //convert Array ids to string for the SQL IN condition
	    $idsOutArray = substr($idsOutArray, 0, -1);
	    
	    //Questions per level
	    $quantityPerLevel = $totalQuestions/5;
	    //final Questions Pack
	    $finalArray = array();
	    
	    // answered questions
	    //get A1 quantity answered questions
	    $SQL = 'SELECT id_pregunta FROM `answers_exam_user` WHERE `id_examen_usuario` = "'.$id_examUser.'" ORDER BY `date_answered` ASC ';
        $answeredQuestions = Yii::app()->db->createCommand($SQL)->queryAll();
	   
	    $a1Quantity = 0;
	    
	    //get A1 quantity answered questions
	    $SQL = 'SELECT id_respuesta_examen_usuario, id_examen_usuario, answers_exam_user.id_pregunta, id_respuesta, escala, date_answered, nivel FROM `answers_exam_user` 
	    INNER JOIN questions ON answers_exam_user.id_pregunta = questions.id_pregunta WHERE `id_examen_usuario` = "'.$id_examUser.'" AND nivel ="A1"';
        $questions = Yii::app()->db->createCommand($SQL)->queryAll();
	    $quantity = count($questions);

	    //FILL A1
	    for($i = $quantity ; $i < $quantityPerLevel ; $i++){
	        $SQL = 'SELECT id_preguntas_examen, id_examen, questions_exam.id_pregunta
            FROM `questions_exam` 
            INNER JOIN questions ON questions_exam.id_pregunta = questions.id_pregunta
            WHERE id_examen IN ('.$idsOutArray.') AND questions.estado ="A" AND nivel ="A1" ORDER BY RAND() LIMIT 1;';
            $question= Yii::app()->db->createCommand($SQL)->queryAll();
            // reviso si la pregunta ya está contestada, si no lo está (0) agrego, si ya lo está (1) retrocedo y vuelvo a preguntar
            $check_question = $this->searchForId($question[0]["id_pregunta"], $answeredQuestions);
            if($check_question == 0){
                array_push($finalArray, $question[0]); 
            }else{
              $i--;
            }
        }
        
        
        //get A2 quantity answered questions
	    $SQL = 'SELECT id_respuesta_examen_usuario, id_examen_usuario, answers_exam_user.id_pregunta, id_respuesta, escala, date_answered, nivel FROM `answers_exam_user` 
	    INNER JOIN questions ON answers_exam_user.id_pregunta = questions.id_pregunta WHERE `id_examen_usuario` = "'.$id_examUser.'" AND nivel ="A2"';
        $questions = Yii::app()->db->createCommand($SQL)->queryAll();
	    $quantity = count($questions);
       
        
        //FILL A2
	    for($i = $quantity ; $i < $quantityPerLevel ; $i++){
	        $SQL = 'SELECT id_preguntas_examen, id_examen, questions_exam.id_pregunta
            FROM `questions_exam` 
            INNER JOIN questions ON questions_exam.id_pregunta = questions.id_pregunta
            WHERE id_examen IN ('.$idsOutArray.') AND questions.estado ="A" AND nivel ="A2" ORDER BY RAND() LIMIT 1;';
            $question= Yii::app()->db->createCommand($SQL)->queryAll();
            // reviso si la pregunta ya está contestada, si no lo está (0) agrego, si ya lo está (1) retrocedo y vuelvo a preguntar
            $check_question = $this->searchForId($question[0]["id_pregunta"], $answeredQuestions);
            if($check_question == 0){
                array_push($finalArray, $question[0]); 
            }else{
              $i--;
            }
        }
        
        
        //get B1 quantity answered questions
	    $SQL = 'SELECT id_respuesta_examen_usuario, id_examen_usuario, answers_exam_user.id_pregunta, id_respuesta, escala, date_answered, nivel FROM `answers_exam_user` 
	    INNER JOIN questions ON answers_exam_user.id_pregunta = questions.id_pregunta WHERE `id_examen_usuario` = "'.$id_examUser.'" AND nivel ="B1"';
        $questions = Yii::app()->db->createCommand($SQL)->queryAll();
	    $quantity = count($questions);

        //FILL B1
	    for($i = $quantity ; $i < $quantityPerLevel ; $i++){
	        $SQL = 'SELECT id_preguntas_examen, id_examen, questions_exam.id_pregunta
            FROM `questions_exam` 
            INNER JOIN questions ON questions_exam.id_pregunta = questions.id_pregunta
            WHERE id_examen IN ('.$idsOutArray.') AND questions.estado ="A" AND nivel ="B1" ORDER BY RAND() LIMIT 1;';
            $question= Yii::app()->db->createCommand($SQL)->queryAll();

            // reviso si la pregunta ya está contestada, si no lo está (0) agrego, si ya lo está (1) retrocedo y vuelvo a preguntar
            $check_question = $this->searchForId($question[0]["id_pregunta"], $answeredQuestions);
            if($check_question == 0){
                array_push($finalArray, $question[0]); 
            }else{
              $i--;
            }
        }
        
        //get B2 quantity answered questions
	    $SQL = 'SELECT id_respuesta_examen_usuario, id_examen_usuario, answers_exam_user.id_pregunta, id_respuesta, escala, date_answered, nivel FROM `answers_exam_user` 
	    INNER JOIN questions ON answers_exam_user.id_pregunta = questions.id_pregunta WHERE `id_examen_usuario` = "'.$id_examUser.'" AND nivel ="B2"';
        $questions = Yii::app()->db->createCommand($SQL)->queryAll();
	    $quantity = count($questions);
        
        //FILL B2
	    for($i = $quantity ; $i < $quantityPerLevel ; $i++){
	        $SQL = 'SELECT id_preguntas_examen, id_examen, questions_exam.id_pregunta
            FROM `questions_exam` 
            INNER JOIN questions ON questions_exam.id_pregunta = questions.id_pregunta
            WHERE id_examen IN ('.$idsOutArray.') AND questions.estado ="A" AND nivel ="B2" ORDER BY RAND() LIMIT 1;';
            $question= Yii::app()->db->createCommand($SQL)->queryAll();
            // reviso si la pregunta ya está contestada, si no lo está (0) agrego, si ya lo está (1) retrocedo y vuelvo a preguntar
            $check_question = $this->searchForId($question[0]["id_pregunta"], $answeredQuestions);
            if($check_question == 0){
                array_push($finalArray, $question[0]); 
            }else{
              $i--;
            }
        }
        
        
         //get C1 quantity answered questions
	    $SQL = 'SELECT id_respuesta_examen_usuario, id_examen_usuario, answers_exam_user.id_pregunta, id_respuesta, escala, date_answered, nivel FROM `answers_exam_user` 
	    INNER JOIN questions ON answers_exam_user.id_pregunta = questions.id_pregunta WHERE `id_examen_usuario` = "'.$id_examUser.'" AND nivel ="C1"';
        $questions = Yii::app()->db->createCommand($SQL)->queryAll();
	    $quantity = count($questions);
        
        //FILL C1
	    for($i = $quantity ; $i < $quantityPerLevel ; $i++){
	        $SQL = 'SELECT id_preguntas_examen, id_examen, questions_exam.id_pregunta
            FROM `questions_exam` 
            INNER JOIN questions ON questions_exam.id_pregunta = questions.id_pregunta
            WHERE id_examen IN ('.$idsOutArray.') AND questions.estado ="A" AND nivel ="C1" ORDER BY RAND() LIMIT 1;';
            $question= Yii::app()->db->createCommand($SQL)->queryAll();
            // reviso si la pregunta ya está contestada, si no lo está (0) agrego, si ya lo está (1) retrocedo y vuelvo a preguntar
            $check_question = $this->searchForId($question[0]["id_pregunta"], $answeredQuestions);
            if($check_question == 0){
                array_push($finalArray, $question[0]); 
            }else{
              $i--;
            }
        }
        
	    
	    return $finalArray;
	    
	    
	    
	}
	
	function searchForId($id, $array) {
	    $flag = 0;
        foreach ($array as $key => $val) {
            if ($val['id_pregunta'] === $id) {
                $flag = 1;
            }
       }
        return $flag;
    }

	/*
	    Check diff between Speaking and listening < 50%
	*/
	public function checkSpeLisdiff($license){
		  var_dump($license);
	}
	
	
	/**
	 * TakeExam ==> Presentar el Exámen
	 * Si no ha terminado el exámen debe ponerlo en el punto en que vaya
	 */
        public function actionTakeExamDemo($y,$p,$b){
            
            //decode variables for execute algoritm
            $userId = base64_decode($y);
            $license = base64_decode($p);
            $examUserId = base64_decode($b);
            
            
            $modUtil = new Utilidades();
            
            if(isset($_POST["phone"])){
                //actualizamos terminos,
                $SQL = "UPDATE `users_c` SET `telefono` = '".$_POST["phone"]."' WHERE `id_usuario_c`='".$userId."'";
                $list= Yii::app()->db->createCommand($SQL)->query();
                
            }
            // save pc info
            $ip= isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? 
            $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
            $dns = gethostbyaddr($_SERVER['REMOTE_ADDR']);  
            
            //revisamos si ya está registrado el equipo 
            //validamcioin FRAUD ALERT
            $sql='SELECT id_host_license FROM `host_license` WHERE `id_licencia_usuario` = "'.$license.'" AND `ip_client` = "'.$ip.'"';
            $check_register= Yii::app()->db->createCommand($sql)->queryAll();
            
            
            $check= array($check_register[0]['id_host_license']);
            
            if(is_null($check[0])){
                 //insertamos fecha de inicio examen, ip cliente y dns
                $SQL = "INSERT INTO  `host_license` (`id_licencia_usuario`, `fecha_inicio_examen`, `ip_client`, `dns_client`)  VALUES ('".$license."', NOW(), '".$ip."', '".$dns."')";
                $host= Yii::app()->db->createCommand($SQL)->query();
            }
            
            //debemos obtener el último valor de Speaking
            //Verificar cuantas preguntas tiene el examen
            //Mirar si las preguntas son fijas o aleatorias
            //Ver cuantas preguntas hay pendientes
            $modExamsUser = ExamsUser::model()->findByPk($examUserId);
            $muestraIntro = Licenses::model()->findByPk($modExamsUser->idLicenciaUsuario->idLicenciaCliente->id_licencia)->intro_secciones;
            $tipologo = Licenses::model()->findByPk($modExamsUser->idLicenciaUsuario->idLicenciaCliente->id_licencia)->logo;
            $examId = $modExamsUser->id_examen;
            $modExamen = Exams::model()->findByPk($modExamsUser->id_examen);
            $totalPreguntas = $modExamen->numero_preguntas;
            $totalContestadas = 0;
            $speakingId = $modUtil->getIdSpeaking();
            $roboFlag = 0;
            
            // si es un rater el que abre el examen, guardamos 
            if(Yii::app()->user->getState('id_perfil')=='6'){
            
                if($examId == 23 || $examId == 35 || $examId == 40 || $examId == $speakingId){
                    //almacenamos el seguimiento al que abre el speaking y no es rater
                    $roboFlag = 1;
                    $SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`) VALUES (NULL, '2', '".Yii::app()->user->getState('numero_id')."', NOW(), '".$license."', '".$examUserId."', 'start speaking exam by no tutor')";
                    $log= Yii::app()->db->createCommand($SQL)->query(); 
                    $this->redirect('index.php?r=Clients/default/login');
                    
                }
            }
            
            //llamamos a robocop para que revise y haga las notificaciones necesarias
            $modUtil->robocop($userId,$license,$examUserId,$roboFlag);
            //actualizamos terminos,
            $SQL = "UPDATE `licenses_user` SET `terms` = '1' WHERE `licenses_user`.`id_licencias_usuario` ='".$license."'";
            $list= Yii::app()->db->createCommand($SQL)->query();
            
            //actualizamos telefono si viene,
            
            $SQL = "UPDATE `licenses_user` SET `terms` = '1' WHERE `licenses_user`.`id_licencias_usuario` ='".$license."'";
            $list= Yii::app()->db->createCommand($SQL)->query();
            
            
            
            
            
            //consultamos el nombre del examen (primer palabra, Grammar or Speaking, or reading or listening)
            $eName = $modExamen->nombre;
            $ePreguntas = $modExamen->numero_preguntas;
            
            $nomExamen = explode(" ", $eName);
            $eName = strtolower($nomExamen[0]); 
            
            $globalModPeguntas;
            
            
            if(!isset($_POST['preguntas'])){ 
               
                //Empezar o Continuar con el Examen
                //Si las preguntas NO son aleatorias
                if($modExamen->preguntas_aleatorias==0){
                    
                    //Seleccionar todas las preguntas del examen
                    $modPeguntas = QuestionsExam::model()->findAllByAttributes(array('id_examen'=>$examId));
                    $modContestadas = AnswersExamUser::model()->findAllByAttributes(array('id_examen_usuario'=>$examUserId));
                    $totalContestadas = count($modContestadas);
                    if($totalContestadas>0){
                        //Continuar con el examen porque algo paso
                        $arregloContestadas =  array_keys(CHtml::listData(AnswersExamUser::model()->findAllByAttributes(array('id_examen_usuario'=>$examUserId)),'id_pregunta','id_respuesta'));
                        //Seleccionar todas las preguntas del examen que no se hayan contestado
                        $contestadas = "('".implode("','",$arregloContestadas)."')";
                        $criterio = new CDbCriteria;
                        $criterio->select = '*';
                        $criterio->with ="idPregunta";
                        $criterio->condition = 'id_examen = '.$examId.' and t.id_pregunta not in '.$contestadas;
                        if(!is_null(Yii::app()->user->getState('relacionada'))){
                            $criterio->condition.= ' and relacionada='.Yii::app()->user->getState('relacionada');
                            $modPeguntas = QuestionsExam::model()->findAll($criterio);
                            if(count($modPeguntas)<=0){ 
                                Yii::app()->user->setState('relacionada',NULL);
                                $criterio->condition = 'id_examen = '.$examId.' and t.id_pregunta not in '.$contestadas;
                                $modPeguntas = QuestionsExam::model()->findAll($criterio);
                            }
                        }
                        
                        if(count($modPeguntas)>0){
                            if($modExamen->vistaunoauno==1){
                                shuffle($modPeguntas);
                            } else {
                            }
                            $model = new AnswersExamUser;
                            
                            switch ($tipologo) {
                                case 1:
                                    $rutalogo = "images/TECS.png";
                                    break;
                                case 2:
                                    $rutalogo = "images/RPT.png";
                                    break;
                                case 3:
                                     $rutalogo = "images/EDE.png";
                                    break;
                                case 4:
                                     $rutalogo = "images/cedt.png";
                                    break;
                                default:
                                     $rutalogo = "images/EDE.png";
                            }
                            
                            Yii::app()->user->setState('rutalogoexam',$rutalogo);
                            if($muestraIntro == 1){
                                $muestraIntro = true;
                            } else {
                                $muestraIntro=false;
                            }
                            $this->render('askQuestion',array(
                            'model'=>$model,'unoauno'=>$modExamen->vistaunoauno,'totalP'=>$totalPreguntas,'totalC'=>$totalContestadas,
                                'modpreguntas'=>$modPeguntas,'titulo'=>$modExamen->nombre,'muestraIntro'=>$muestraIntro,
                                'bgColor'=>$modExamen->idSeccion->bg_color,'txtColor'=>$modExamen->idSeccion->txt_color
                            ));
                        } else {
                            //No hay mas preguntas
                            $this->actionFinishExam($userId,$license,$examUserId);
                        }                       
                    } else {
                        //Seleccionar la pregunta a realizar
                        //Empezar con el examen por primera vez.
                        if($modExamen->vistaunoauno==1){
                            shuffle($modPeguntas);
                        } 
                        
                        switch ($tipologo) {
                                case 1:
                                    $rutalogo = "images/TECS.png";
                                    break;
                                case 2:
                                    $rutalogo = "images/RPT.png";
                                    break;
                                case 3:
                                     $rutalogo = "images/EDE.png";
                                    break;
                                case 4:
                                     $rutalogo = "images/cedt.png";
                                    break;
                                default:
                                     $rutalogo = "images/EDE.png";
                            }
                        
                        Yii::app()->user->setState('rutalogoexam',$rutalogo);
                            if($muestraIntro == 1){
                                $muestraIntro = true;
                            } else {
                                $muestraIntro=false;
                            }
                        $this->render('askQuestion',array(
                        'model'=>$model,'unoauno'=>$modExamen->vistaunoauno,'totalP'=>$totalPreguntas,'totalC'=>$totalContestadas,
                            'modpreguntas'=>$modPeguntas,'titulo'=>$modExamen->nombre,
                            'bgColor'=>$modExamen->idSeccion->bg_color,'txtColor'=>$modExamen->idSeccion->txt_color,'muestraIntro'=>$muestraIntro
                        ));
                    }
                } else { 
                    
                    
                    //aqui cae speaking y reading
                    // si es un rater el que abre el examen, guardamos 
                    if(Yii::app()->user->getState('id_perfil')!='6'){
                        //almacenamos el seguimiento al tutor que ejecuta el speaking
                        //insertamos fecha de inicio examen, ip cliente y dns
                        // ACCESS con GET
                        $SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 4, '".Yii::app()->user->getState('usuario')."', NOW(), '".$license."', '".$examUserId."', 'Open Speaking test of test taker', '')";
                        $log= Yii::app()->db->createCommand($SQL)->query(); 
                        
                    }
                    
                    
                    //get Speaking Exam IDs
                    $sql='SELECT id_examen FROM `exams` WHERE `nombre` LIKE "%speaking%"';
                    $ids_query= Yii::app()->db->createCommand($sql)->queryAll();
                    
                    //save ids in a string to concatenate in SQL QUERY
                    foreach($ids_query as $id){
                        $ids .= $id["id_examen"].",";
                    }
                    
                    //REMOVE LAS CHARACTER OF THE STRING
                    $ids = substr($ids, 0, -1);
                    $id = $ids;
                    
                    //GET at least 1 answer for this speaking exam
                    $sql='SELECT COUNT(id_respuesta_examen_usuario) AS total FROM `answers_exam_user` 
                    INNER JOIN exams_user ON answers_exam_user.id_examen_usuario = exams_user.id_examen_usuario
                    WHERE answers_exam_user.`id_examen_usuario` = "'.$examUserId.'"
                    AND exams_user.id_examen IN ('.$id.')';  
                    $total= Yii::app()->db->createCommand($sql)->queryAll();
                    
                    // iF there are at least 1 question answered we close the exam
                    if($total[0]["total"] > 0){
                        $this->renderPartial('closeExam');
                    }else {
                           //Si las preguntas son aleatorias
                        //Seleccionar todas las preguntas del examen
                        $modPeguntas = QuestionsExam::model()->findAllByAttributes(array('id_examen'=>$examId));
                        
                        // Al consultar todas las preguntas, procedemos a sacar 6 randomicamente
                        // por nivel
                       //Validación randomica para examenes que comiencen con speaking en el nombre
                       
                        $prexNivel = intval($ePreguntas/5);
                        $finalArray = $this->randomQuestions($modPeguntas, $prexNivel);
                        $modPeguntas = $finalArray;
                        
                        $modContestadas = AnswersExamUser::model()->findAllByAttributes(array('id_examen_usuario'=>$examUserId));
                        
                       
                        $totalContestadas = count($modContestadas);
                        
                        if($totalContestadas>0){
                            
                            //revisarlo
                            //Continuar con el examen porque algo paso
                            $arregloContestadas =  array_keys(CHtml::listData(AnswersExamUser::model()->findAllByAttributes(array('id_examen_usuario'=>$examUserId)),'id_pregunta','id_respuesta'));
                            
                            //Seleccionar todas las preguntas del examen que no se hayan contestado
                            $contestadas = "('".implode("','",$arregloContestadas)."')";
                            
                            $criterio = new CDbCriteria;
                            $criterio->select = '*';
                            $criterio->with ="idPregunta";
                            $criterio->condition = 'id_examen = '.$examId.' and t.id_pregunta not in '.$contestadas;
                            
                            //preguntas relacionadas
                            if(!is_null(Yii::app()->user->getState('relacionada'))){
                                $criterio->condition.= ' and relacionada='.Yii::app()->user->getState('relacionada');
                                $modPeguntas = QuestionsExam::model()->findAll($criterio);
                                $texto=" es este: ".print_r($criterio,true);
                                Yii::app()->user->setState('txtquery',$texto);
                                if(count($modPeguntas)<=0){ 
                                    Yii::app()->user->setState('relacionada',NULL);
                                    $criterio->condition = 'id_examen = '.$examId.' and t.id_pregunta not in '.$contestadas;
                                    $modPeguntas = QuestionsExam::model()->findAll($criterio);
                                }
                            }
                            
                            
                            if(count($modPeguntas)>0){
                                
                                if($modExamen->vistaunoauno==1){
                                    
                                    //order del array ramdomico
                                    shuffle($modPeguntas);
                                } 
                                
                                
                                $model = new AnswersExamUser;
                                switch ($tipologo) {
                                    case 1:
                                        $rutalogo = "images/TECS.png";
                                        break;
                                    case 2:
                                        $rutalogo = "images/RPT.png";
                                        break;
                                    case 3:
                                         $rutalogo = "images/EDE.png";
                                        break;
                                    case 4:
                                         $rutalogo = "images/cedt.png";
                                        break;
                                    default:
                                         $rutalogo = "images/EDE.png";
                                }
                                Yii::app()->user->setState('rutalogoexam',$rutalogo);
                                if($muestraIntro == 1){
                                    $muestraIntro = true;
                                } else {
                                    $muestraIntro=false;
                                }
                                $this->render('askQuestion',array(
                                'model'=>$model,'unoauno'=>$modExamen->vistaunoauno,'totalP'=>$totalPreguntas,'totalC'=>$totalContestadas,
                                    'modpreguntas'=>$modPeguntas,'titulo'=>$modExamen->nombre,'muestraIntro'=>$muestraIntro,
                                    'bgColor'=>$modExamen->idSeccion->bg_color,'txtColor'=>$modExamen->idSeccion->txt_color
                                ));
                            } else {
                                //No hay mas preguntas
                                $this->actionFinishExam($userId,$license,$examUserId);
                            }                       
                        } else {
                            //Seleccionar la pregunta a realizar
                            //Empezar con el examen por primera vez.
                            if($modExamen->vistaunoauno==1){
                                shuffle($modPeguntas);
                            }
                            $model = new AnswersExamUser;
                                switch ($tipologo) {
                                    case 1:
                                        $rutalogo = "images/TECS.png";
                                        break;
                                    case 2:
                                        $rutalogo = "images/RPT.png";
                                        break;
                                    case 3:
                                         $rutalogo = "images/EDE.png";
                                        break;
                                    case 4:
                                         $rutalogo = "images/cedt.png";
                                        break;
                                    default:
                                         $rutalogo = "images/EDE.png";
                                }
                            Yii::app()->user->setState('rutalogoexam',$rutalogo);
                                if($muestraIntro == 1){
                                    $muestraIntro = true;
                                } else {
                                    $muestraIntro=false;
                                }
                            $this->render('askQuestion',array(
                            'model'=>$model,'unoauno'=>$modExamen->vistaunoauno,'totalP'=>$totalPreguntas,'totalC'=>$totalContestadas,
                                'modpreguntas'=>$modPeguntas,'titulo'=>$modExamen->nombre,
                                'bgColor'=>$modExamen->idSeccion->bg_color,'txtColor'=>$modExamen->idSeccion->txt_color,'muestraIntro'=>$muestraIntro
                            ));
                        }
                    } 
                    }
                    
                    
            } else { 
                
               /*  //aca guardamos el resultado de las respuestas speaking
               
                    echo "<pre>";
                var_dump($_GET);
                var_dump($_POST);die();*/
                //Guardar las respuestas
                foreach($_POST['preguntas'] as $pregunta=>$respuesta){
                    $modRespuestas = new AnswersExamUser;
                    $modRespuestas->id_pregunta = $pregunta;
                    $modRespuestas->date_answered = date('Y-m-d H:i:s');
                    if($modRespuestas->idPregunta->tipo_presentacion=="S"){
                        $modRespuestas->escala = $respuesta;
                    } else {
                        
                        if($respuesta == -1){
                            $modRespuestas->id_respuesta = NULL;
                        } else {
                            $modRespuestas->id_respuesta = $respuesta;
                        }
                    }
                    $modRespuestas->id_examen_usuario = $examUserId;
                    try{
                     $modRespuestas->save();
                    }
                    catch(Exception $e){}
                }
                if($modExamen->vistaunoauno==1){ 
                    //Viene una sola pregunta
                    //Continuar con la siguiente pregunta
                    
                    if($modExamen->preguntas_aleatorias==0){
                        //Preguntas contestadas
                        $arregloContestadas =  array_keys(CHtml::listData(AnswersExamUser::model()->findAllByAttributes(array('id_examen_usuario'=>$examUserId)),'id_pregunta','id_respuesta'));
                        //Seleccionar todas las preguntas del examen que no se hayan contestado
                        $contestadas = "('".implode("','",$arregloContestadas)."')";
                        $totalContestadas = count($arregloContestadas);
                        
                        $criterio = new CDbCriteria;
                        $criterio->select = '*';
                        $criterio->condition = 'id_examen = '.$examId.' and id_pregunta not in '.$contestadas;
                        $criterio->with ="idPregunta";
                        $criterio->condition = 'id_examen = '.$examId.' and t.id_pregunta not in '.$contestadas;
                        if(!is_null(Yii::app()->user->getState('relacionada'))){
                            $criterio->condition.= ' and relacionada='.Yii::app()->user->getState('relacionada');
                            $modPeguntas = QuestionsExam::model()->findAll($criterio);
                            $texto=" es este: ".print_r($criterio,true);
                            Yii::app()->user->setState('txtquery',$texto);
                            if(count($modPeguntas)<=0){ 
                                Yii::app()->user->setState('relacionada',NULL);
                                $criterio->condition = 'id_examen = '.$examId.' and t.id_pregunta not in '.$contestadas;
                                $modPeguntas = QuestionsExam::model()->findAll($criterio);
                            }
                        } else {
                            $modPeguntas = QuestionsExam::model()->findAll($criterio);
                        }
                        if(count($modPeguntas)>0){
                            if($modExamen->vistaunoauno==1){
                                shuffle($modPeguntas);
                            }
                            $model = new AnswersExamUser;
                            switch ($tipologo) {
                                case 1:
                                    $rutalogo = "images/TECS.png";
                                    break;
                                case 2:
                                    $rutalogo = "images/RPT.png";
                                    break;
                                case 3:
                                     $rutalogo = "images/EDE.png";
                                    break;
                                case 4:
                                     $rutalogo = "images/cedt.png";
                                    break;
                                default:
                                     $rutalogo = "images/EDE.png";
                            }
                            Yii::app()->user->setState('rutalogoexam',$rutalogo);
                            $this->render('askQuestion',array(
                            'model'=>$model,'unoauno'=>$modExamen->vistaunoauno,'totalP'=>$totalPreguntas,'totalC'=>$totalContestadas,
                                'modpreguntas'=>$modPeguntas,'titulo'=>$modExamen->nombre,
                                'bgColor'=>$modExamen->idSeccion->bg_color,'txtColor'=>$modExamen->idSeccion->txt_color,'muestraIntro'=>false
                            ));
                        } else {
                            //No hay mas preguntas
                            $this->actionFinishExam($userId,$license,$examUserId);
                        }
                    } else {
                        
                        
                        //Preguntas contestadas
                        $arregloContestadas =  array_keys(CHtml::listData(AnswersExamUser::model()->findAllByAttributes(array('id_examen_usuario'=>$examUserId)),'id_pregunta','id_respuesta'));
                        //Seleccionar todas las preguntas del examen que no se hayan contestado
                        
                        $contestadas = "('".implode("','",$arregloContestadas)."')";
                        $totalContestadas = count($arregloContestadas);
                        
                        
                        if($totalContestadas == intval($ePreguntas)){
                            //$this->actionFinishExam($userId,$license,$examUserId);
                        }
                        
                        $criterio = new CDbCriteria;
                        $criterio->select = '*';
                        $criterio->condition = 'id_examen = '.$examId.' and id_pregunta not in '.$contestadas;
                        $criterio->with ="idPregunta";
                        $criterio->condition = 'id_examen = '.$examId.' and t.id_pregunta not in '.$contestadas;
                        
                        if(!is_null(Yii::app()->user->getState('relacionada'))){
                            $criterio->condition.= ' and relacionada='.Yii::app()->user->getState('relacionada');
                            $modPeguntas = QuestionsExam::model()->findAll($criterio);
                            $texto=" es este: ".print_r($criterio,true);
                            Yii::app()->user->setState('txtquery',$texto);
                            if(count($modPeguntas)<=0){ 
                                Yii::app()->user->setState('relacionada',NULL);
                                $criterio->condition = 'id_examen = '.$examId.' and t.id_pregunta not in '.$contestadas;
                                $modPeguntas = QuestionsExam::model()->findAll($criterio);
                            }
                        } else {
                            $modPeguntas = QuestionsExam::model()->findAll($criterio);
                        }
                        if(count($modPeguntas)>0){
                            if($modExamen->vistaunoauno==1){
                                shuffle($modPeguntas);
                            }
                            $model = new AnswersExamUser;
                            switch ($tipologo) {
                                case 1:
                                    $rutalogo = "images/TECS.png";
                                    break;
                                case 2:
                                    $rutalogo = "images/RPT.png";
                                    break;
                                case 3:
                                     $rutalogo = "images/EDE.png";
                                    break;
                                case 4:
                                     $rutalogo = "images/cedt.png";
                                    break;
                                default:
                                     $rutalogo = "images/EDE.png";
                            }
                            Yii::app()->user->setState('rutalogoexam',$rutalogo);
                            $this->render('askQuestion',array(
                            'model'=>$model,'unoauno'=>$modExamen->vistaunoauno,'totalP'=>$totalPreguntas,'totalC'=>$totalContestadas,
                                'modpreguntas'=>$modPeguntas,'titulo'=>$modExamen->nombre,
                                'bgColor'=>$modExamen->idSeccion->bg_color,'txtColor'=>$modExamen->idSeccion->txt_color,'muestraIntro'=>false
                            ));
                        } else {
                            //No hay mas preguntas
                            $this->actionFinishExam($userId,$license,$examUserId);
                        }
                    }
                } else {
                    $this->actionFinishExam($userId,$license,$examUserId);
                }
            }
        }
	
	//funcion para quitar del array de ids de preguntas las que ya están contestadas
}

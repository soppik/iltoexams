<?php

class UsersCController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create', 'create_users', 'create_1','email', 'create_exam','create_exam_1','citiesCountry','update','admin','delete','transfer', 'userTransfer', 'csvUsers', 'csvUsersExamsFuaa', 'csvUsersExams', 'arrayFromCSV', 'UpdatePhotoTecs', 'check', 'checkList', 'reportCheckList', 'enableTaker'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        


        public function actionCitiesCountry($country){
            $retorno = CHtml::listData(Cities::model()->findAllByAttributes(array('id_pais'=>$country)), 'id_ciudad', 'nombre');
            echo json_encode($retorno);
        }

        /**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
	    //$model = UsersC::model()->findByPk('987987');
	    $sql='SELECT * FROM `users_c` WHERE `id_usuario_c` ="'.$id.'"';
        $model= Yii::app()->db->createCommand($sql)->queryAll();
        
	    $modUser = new UsersC;
	    $modUser->attributes = $model;
	    $this->render('view',array(
			'model'=>$this->loadModel($model),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($exam=NULL)
	{
		$model=new UsersC;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['UsersC']))
		{
           if($_POST["id_grupo_cliente"]=="" && $_POST["id_perfil"]=="6"){
    	       Yii::app()->user->setFlash('error', 'The Taker registration was unsuccessful, please fill the GROUP field.');
                $this->redirect(array('UsersC/create'));
    	   }
		                $modUtil = new Utilidades();
		                //removemos espacios en blanco
                        $_POST = array_map("trim", $_POST['UsersC']);
                        $model->attributes=$_POST;
                        $model->id_usuario_c = $model->numero_id;
                        $clave = $model->id_usuario_c;
                        
                        $model->id_cliente = Yii::app()->user->getState('cliente');
                        // Validar que el usuario no exista en otro cliente
                        $error=0;
                        $fechaLimite = date('Y-m-d H:i:s', strtotime("-180 days"));
                        $fechaActual = "0000-00-00 00:00:00";
                        $mensaje = "";
                        $modUsuarios = UsersC::model()->resetScope()->findAllByAttributes(array('id_usuario_c'=>$model->id_usuario_c));
                        if(count($modUsuarios)>0){
                            $modLicencias = LicensesUser::model()->findAllByAttributes(array('id_usuario'=>$model->id_usuario_c));
                            foreach($modLicencias as $rowLicencias){
                                if($rowLicencias->fecha_presentacion > $fechaActual && $rowLicencias->estado=="F"){
                                    $fechaActual = $rowLicencias->fecha_presentacion;
                                    $fechaPresenta = new DateTime($fechaActual);
                                    $fechaProxima = new DateTime($fechaActual);
                                    $fechaProxima->add(new DateInterval('P6M'));
                                    $mensaje = "The user has already took the TECS on (".$fechaPresenta->format('Y-m-d')."). To assure the reliability of the results, users may only take the TECS every six months. This user may take the test again on (".$fechaProxima->format('Y-m-d').")";
                                    $mensaje.= "<br>The communication skills score does not change if the candidate does not take the time to develop these skills. This takes time and to notice a slight change on the score a minimun of 6 months is required.";
                                }
                            }
                            $error=2; //El usuario está creado con otro cliente
                            foreach($modUsuarios as $usuario){
                                if($usuario->id_cliente==$model->id_cliente){
                                    $error=1; //El usuario ya está creado dentro del mismo cliente
                                }
                            }
                        }
                        if($error==0){
                        
                            
                            $ahora = time();
                            $ruta_foto=CUploadedFile::getInstance($model,'ruta_foto');
                            
                            if(!is_null($ruta_foto)){
                                $modUtil = new Utilidades();
                                $extension = ".".strtolower($ruta_foto->getExtensionName());
                                $nombreImagen = $model->id_cliente."_".$model->id_usuario_c."_".$ahora.$extension;
                                $model->ruta_foto=$nombreImagen;
                               // $model->save();
                                $ruta_foto->saveAs("images/users/".$nombreImagen);
                                $modUtil->generaThumb("images/users/",$nombreImagen);
                            }   
                          
                            if($model->id_perfil == 6){ 
                               /* if($nombreImagen==""){ 
                                    Yii::app()->user->setFlash('error', 'Taker photo is required.');
                                    $this->render('create',array('model'=>$model,));
                                }*/
                                
                                //guardamos grupo e email certificate
                                $SQL = "INSERT INTO `users_c` (`id_usuario_c`, `nombres`, `apellidos`, `email`, `id_cliente`, `ultimo_acceso`, `id_perfil`, `numero_id`, `ruta_foto`, `estado`, `clave`, `clave2`, `fecha_creado`, `id_grupo_cliente`, `id_pais`, `id_country`, `id_ciudad`, `email_certificate`, `isDemo`, `reference`) 
                                VALUES ('".$_POST['numero_id']."', '".$_POST['nombres']."', '".$_POST['apellidos']."', '".$_POST['email']."',  '".Yii::app()->user->getState('cliente')."', NOW(), '".$_POST['id_perfil']."', '".$_POST['numero_id']."', '".$nombreImagen."', 'A', MD5('".$clave."'), '".$clave."', NOW(), '".$_POST['id_grupo_cliente']."', 3, '".$_POST['id_pais']."', '".$_POST['id_ciudad']."', '".$_POST['email_certificate']."', 0, NULL)";
                                $list= Yii::app()->db->createCommand($SQL)->query();
                               
                            } else {
                                $SQL = "INSERT INTO `users_c` (`id_usuario_c`, `nombres`, `apellidos`, `email`, `id_cliente`, `id_perfil`, `numero_id`, `estado`, `clave`, `clave2`, `fecha_creado`, `id_country`, `id_ciudad`) 
                                VALUES ('".$_POST['numero_id']."', '".$_POST['nombres']."', '".$_POST['apellidos']."', '".$_POST['email']."', '".Yii::app()->user->getState('cliente')."','".$_POST['id_perfil']."', '".$_POST['numero_id']."', 'A', MD5('".$clave."'), '".$clave."',  NOW(), '".$_POST['id_pais']."', '".$_POST['id_ciudad']."')";
                              
                                $list= Yii::app()->db->createCommand($SQL)->query();
                            }
                            
                            // agregamos al log del usuario + last iD
            				$lastID = Yii::app()->db->getLastInsertID();
            				$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 1, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'Create user ".$lastID."', '')";
            				$log= Yii::app()->db->createCommand($SQL)->query(); 
                            
                            
                            if(is_null($exam)){
                                $this->redirect(array('admin'));
                            } else {
                                    $this->redirect(array('ExamsUser/create&id='.$model->id_usuario_c));
                            }
                            
                          
                            
                        } else {
                             $model->addError('id_usuario_c','This user ID is already in the system. Please verify the identity of the candidate.');
                        }
		}
                $modGroups = GroupsUsersClients::model()->findAll();
                $modRooms = ClassRooms::model()->findAll();
                if(count($modGroups)<=0){
                    $mensaje = 'There are no User Groups created yet. You have to create at least One User Group to create a TAKER.';
                }
                if(count($modRooms)<=0){
                    $mensaje .= 'There are no Test Rooms created yet. You have to create at least One Test Room to assign an exam to a TAKER';
                }
                if(strlen($mensaje)>0) Yii::app()->user->setFlash('error',$mensaje);
		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	public function actionCreate_users()
	{
		$model=new UsersC;

		$this->render('create_users',array(
			'model'=>$model,
		));
	}
	
	public function actionCreate_1($exam=NULL)
	{
		$model=new UsersC;
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['UsersC']))
		{
		    $modUtil = new Utilidades();
                        $model->attributes=$_POST['UsersC'];
                        $model->id_usuario_c=$model->numero_id;
                        $model->id_cliente = Yii::app()->user->getState('cliente');
                        $ruta_foto=CUploadedFile::getInstance($model,'ruta_foto');
                        $ahora = time();
                        $clave=$model->id_cliente;
                        $model->clave=md5($model->id_usuario_c);
                        $model->clave2=md5($model->id_usuario_c);
                        $model->estado="P";
			if($model->save(true,array('numero_id','id_cliente','id_grupo_cliente','id_usuario_c','nombres','apellidos','clave','email','id_perfil','tipo_id','numero_id','estado','id_pais','id_ciudad','email_certificate'))){
                            $modUtil = new Utilidades();
                            if(!is_null($ruta_foto)){
                                $extension = ".".strtolower($ruta_foto->getExtensionName());
                                $nombreImagen = $model->id_cliente."_".$model->id_usuario_c."_".$ahora.$extension;
                                $model->ruta_foto=$nombreImagen;
                                $model->save();
                                $ruta_foto->saveAs("images/users/".$nombreImagen);
                                $modUtil->generaThumb("images/users/",$nombreImagen);
                            }
                            if($model->id_perfil==6){
                            } else {
                               
                            }
                            if(is_null($exam)){
                                echo "admin";
                                return;
                                $this->redirect(array('admin'));
                            } else {
                                echo "create:".$model->id_usuario_c;
                                return;
                                $this->redirect(array('ExamsUser/create&id='.$model->id_usuario_c));
                            }
                        }
		}
                $modGroups = GroupsUsersClients::model()->findAll();
                $modRooms = ClassRooms::model()->findAll();
                if(count($modGroups)<=0){
                    $mensaje = 'There are no User Groups created yet. You have to create at least One User Group to create a TAKER.';
                }
                if(count($modRooms)<=0){
                    $mensaje .= 'There are no Test Rooms created yet. You have to create at least One Test Room to assign an exam to a TAKER';
                }
                if(strlen($mensaje)>0) Yii::app()->user->setFlash('error',$mensaje);
		$this->render('create_1',array(
			'model'=>$model,
		));
	}

	public function actionCreate_exam()
	{
	    $this->actionCreate("*");
	}
	public function actionCreate_exam_1()
	{
            $this->actionCreate_1("*");
	}
        
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
	    
		$model=  UsersC::model()->findByAttributes(array('id_usuario_c'=>$id));

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
        
		if(isset($_POST['UsersC']))
		{   
		    
		    $fileZise = $_FILES['UsersC']["size"]["ruta_foto"];
		    
		    
		    
		    if($fileZise > 999999){
                $fotoSql = '`ruta_foto`="'.$_POST['ruta_imagen_actual'].'",'; 
                Yii::app()->user->setFlash('error','This file exceed the maximum size allowed (1MB) for the picture.');
                $this->redirect(array('UsersC/update&id='.$id));
            }
                  
		    $anteClave = $model->clave;
            $model->attributes=$_POST['UsersC'];
             $modUtil = new Utilidades();
            if(!is_null($ruta_imagen)){
                $photoFlag = 1;
                $ruta_imagen=CUploadedFile::getInstance($model,'ruta_foto');
                $extension =  ".".strtolower($ruta_imagen->getExtensionName());
            
                switch ($extension) {
                    case '.jpg':
                        $photoFlag = 0;
                        
                    break;
                    case '.jpeg':
                        $photoFlag = 0;
                    break;
                    case '.png':
                        $photoFlag = 0;
                    break;
                    default:
                        $photoFlag = 1;
                }        
                
               if($photoFlag ==1){
                    $fotoSql = '`ruta_foto`="'.$_POST['ruta_imagen_actual'].'",'; 
                    Yii::app()->user->setFlash('error','The test taker picture was not uploaded, because you are using a '.$extension.' file. Please use a valid file such as .JPG, .JPEG or .PNG.');
                    $this->redirect(array('UsersC/update&id='.$id));
                }
                    
                //Si tenia una imagen, eliminarla
                if(isset($_POST['ruta_imagen_actual'])){
                    $modUtil->eliminarThumbs("images/users/",$_POST['ruta_imagen_actual']);
                }
                
                //Nombre de la nueva imagen
                //Corrección de la extensión en minusculac
                $ahora = time();
                $nombreImagen = $model->id_cliente."_".$model->id_usuario_c.$ahora.$extension;
                $model->ruta_foto=$nombreImagen;
                //$model->save(); 
                $ruta_imagen->saveAs("images/users/".$nombreImagen);
                $modUtil->generaThumb("images/users/",$nombreImagen);
            } elseif(strlen($_POST['ruta_imagen_actual'])>0){ //Si existia una imagen previa
                if(isset($_POST['delete_imagen'])){
                    $model->ruta_foto=NULL;
                    // $model->save(true,array('ruta_foto'));
                    $modUtil->eliminarThumbs("images/users/",$_POST['ruta_imagen_actual']);
                }
            }
                
            $fotoSql = '`ruta_foto`="'.$nombreImagen.'",';
            if($ruta_imagen==NULL) {
                $fotoSql = "";
            }            
            
            
            if($model->id_perfil==6){
                $SQL = "UPDATE `users_c` SET `nombres`='".$_POST['UsersC']['nombres']."', `apellidos`='".$_POST['UsersC']['apellidos']."', `email`='".$_POST['UsersC']['email']."', ".$fotoSql." `estado`='".$_POST['UsersC']['estado']."', `id_grupo_cliente`='".$model->id_grupo_cliente."', `id_country`='".$_POST['UsersC']["id_pais"]."', `id_ciudad`='".$_POST['UsersC']['id_ciudad']."', `email_certificate`='".$_POST['UsersC']['email_certificate']."' WHERE `id_usuario_c`='".$id."'";
                $list= Yii::app()->db->createCommand($SQL)->query();
                $this->redirect(array('admin'));
            }else{
                $SQL = "UPDATE `users_c` SET `nombres`='".$_POST['UsersC']['nombres']."', `apellidos`='".$_POST['UsersC']['apellidos']."', `email`='".$_POST['UsersC']['email']."', ".$fotoSql." `estado`='".$_POST['UsersC']['estado']."', `id_country`='".$_POST['UsersC']['id_country']."', `id_ciudad`='".$_POST['UsersC']["id_ciudad"]."', `email_certificate`='".$_POST['UsersC']['email_certificate']."' WHERE `id_usuario_c`='".$id."'";
                $list= Yii::app()->db->createCommand($SQL)->query();
                $this->redirect(array('admin'));
            }
            // agregamos al log del usuario + last iD
            $SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 1, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'Edit User ".$id."', '')";
            $log= Yii::app()->db->createCommand($SQL)->query(); 
            
        }
        
		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	public function actionUpdatePhotoTecs()
	{
		//$model=  UsersC::model()->findByAttributes(array('id_usuario_c'=>$id));

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
	
		if(isset($_POST['ExamsUser']))
		{
		    
            $userId = $_POST['ExamsUser']["id_user"];
		    $examUserId = $_POST['ExamsUser']["examUserId"];
		    $license = $_POST['ExamsUser']["license"];
		    //MODEL EXAM
            $model=new ExamsUser;
            $modExamsUser = ExamsUser::model()->findByPk($examUserId);
            $muestraIntro = Licenses::model()->findByPk($modExamsUser->idLicenciaUsuario->idLicenciaCliente->id_licencia)->intro_secciones;
            $examId = $modExamsUser->id_examen;
            $modUtil = new Utilidades();
            
            //Model USER
            $model=  UsersC::model()->findByAttributes(array('id_usuario_c'=>$userId));
            $model->attributes=$_POST['ExamsUser'];
            $ruta_foto = CUploadedFile::getInstance($model,'ruta_foto');
            
            $modUtil = new Utilidades();
            
            $isTecs = $modUtil->isTecs($modExamsUser->idLicenciaUsuario->id_licencias_usuario);
            
            if($isTecs == true){
                
                if(empty($ruta_foto)){
                    Yii::app()->user->setFlash('error', 'Photo is required.');
                    $this->redirect(array('default/index'));
                }
                if(is_null($ruta_foto)){
                    Yii::app()->user->setFlash('error', 'Photo is required.');
                    $this->redirect(array('default/index'));
                }
                $extension = ".".strtolower($ruta_foto->getExtensionName());
                $photoFlag = 1;
                switch ($extension) {
                    case '.jpg':
                        $photoFlag = 0;
                        break;
                    case '.jpeg':
                        $photoFlag = 0;
                        break;
                    case '.png':
                        $photoFlag = 0;
                        break;
                    default:
                        $photoFlag = 1;
                }
            
                if($photoFlag ==1){
                    Yii::app()->user->setFlash('error', 'The test taker picture was not uploaded, because you are using a invalid ('.$extension.') file. Please use a valid file such as .JPG, .JPEG or .PNG.');
                    $this->redirect(array('default/index'));
                }
                
                if(isset($_POST["phone"])){
                    //actualizamos terminos,
                    $SQL = "UPDATE `users_c` SET `telefono` = '".$_POST["phone"]."' WHERE `id_usuario_c`='".$userId."'";
                    $list= Yii::app()->db->createCommand($SQL)->query();
                    
                }
                
                if(!is_null($ruta_foto)){
                    
                    $modUsu = UsersC::model()->findByAttributes(array('id_usuario_c'=>$userId));
                    
                    $extension = ".".strtolower($ruta_foto->getExtensionName());
                    $ahora = time();
                    $nombreImagen = $modExamsUser->idLicenciaUsuario->idLicenciaCliente->id_cliente."_".$userId."_".$ahora.$extension;
                    
                    $modUsu->ruta_foto=$nombreImagen;
                    $modUsu->save(array('ruta_foto'),true);
                    $ruta_foto->saveAs("images/users/".$nombreImagen);
                    $modUtil->generaThumb("images/users/",$nombreImagen);
                }
            }
            
            if($photoFlag == 0){
                $this->redirect(array('AnswersExamUser/takeExam&y='.base64_encode($userId).'&p='.base64_encode($license).'&b='.base64_encode($examUserId).''));
            }
		}     
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
	    $modTmp = UsersC::model()->findByAttributes(array('id_usuario_c'=>$id,'id_cliente'=>Yii::app()->user->getState('cliente')));
        if(!is_null($modTmp)){
            // agregamos al log del usuario + last iD
            $SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 1, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'Delete User ".$id."', '')";
            $log= Yii::app()->db->createCommand($SQL)->query(); 
            $modTmp->delete();
            
            $this->render('admin',array(
			'model'=>$model,
		    ));	
        }

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			
		
	}
	
	
	public function actionEnableTaker($id)
	{
	    $SQL = "UPDATE `users_c` SET  `estado`='A' WHERE `id_usuario_c`='".$id."'";
        $list= Yii::app()->db->createCommand($SQL)->query();
        
        $SQL = "UPDATE `proctorv_request` SET  `status_request`= 4 WHERE `id_user`='".$id."'";
        $list= Yii::app()->db->createCommand($SQL)->query();
        
        $SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 1, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'Taker enabled', '".$id."')";
        $log= Yii::app()->db->createCommand($SQL)->query(); 
        
        $SQL = "DELETE FROM `fraud_report` WHERE document = '".$id."'";
		$log= Yii::app()->db->createCommand($SQL)->query(); 
        
        Yii::app()->user->setFlash('error','The test taker has been enabled in the system.  Enter the ID number of the test taker in the search box (USERNAME / ID). You can now assign a TECS or EDE license.');
        $this->redirect(array('admin'));
			
		
	}

    //ESTA FUNCION PARA QUE SE USA
    public function actionEmail($id,$login=0)
    {
        $modUtil = new Utilidades();
        //$modUtil->actionEnviarRegistro($id,$login);
        //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('UsersC');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
	 	$model=new UsersC('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['UsersC']))
			$model->attributes=$_GET['UsersC'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function actionCheck()
	{
	 	$model=new UsersC('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['UsersC']))
			$model->attributes=$_GET['UsersC'];

		$this->render('user-list',array(
			'model'=>$model,
		));
	}
	
	public function actionTransfer()
	{
	 	$model=new UsersC('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['UsersC']))
			$model->attributes=$_GET['UsersC'];

		$this->render('transfer',array(
			'model'=>$model,
		));
	}
	
	public function actionuserTransfer()
	{
	    
	    $SQL = "UPDATE `users_c` SET `id_cliente`='".$_POST['id_cliente']."', `id_grupo_cliente`='".$_POST['group_id']."'  WHERE `id_usuario_c`='".$_POST['id_usuario_c']."'";
        $list= Yii::app()->db->createCommand($SQL)->query();
        
        // agregamos al log del usuario + last iD
        $SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 1, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'Transfer test taker', '".$id."')";
        $log= Yii::app()->db->createCommand($SQL)->query(); 
        
        Yii::app()->user->setFlash('error','The test taker has already been enabled in the system.  Enter the ID number of the test taker ("'.$_POST['id_usuario_c'].'") in the search box (USERNAME / ID). Do not create the test taker again. You can now assign a TECS or EDE license.');
        $this->redirect(array('admin'));
	}
	
	public function actionCsvUsers()
	{
	    $model=new UsersC;
	    $client = $model->id_cliente = Yii::app()->user->getState('cliente');
	    $date = date("Y-m-d H:i:s");
	    
	    if ( isset($_POST["submit"]) ) {
            if ( isset($_FILES["UsersC"]["name"]["file"])) {
                
                // agregamos al log del usuario + last iD
                $SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 1, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'Create multiple test talkers using CSV file ".$_FILES["UsersC"]["name"]["file"]."', '')";
                $log= Yii::app()->db->createCommand($SQL)->query(); 
                
                //validamos si es un archivo .CSV
                $extension = explode(".", $_FILES["UsersC"]["name"]["file"]);
                
                if($extension[1]!="csv"){
                    Yii::app()->user->setFlash('error', 'Please use a .CSV file.  You are using a '.$extension[1].' extension file.');
                    $this->redirect(array('usersC/create_users'));
                }  
                
                $delimiter = "/n";
                //$file = fopen($_FILES["UsersC"]["tmp_name"]["file"],"r");
               if (($handle = fopen($_FILES["UsersC"]["tmp_name"]["file"], "r")) !== FALSE) { 
                    $i = 0; 
                    while (($lineArray = fgetcsv($handle, 4000, $delimiter)) !== FALSE) { 
                        for ($j=0; $j<count($lineArray); $j++) { 
                            $data2DArray[$i][$j] = $lineArray[$j]; 
                        } 
                        $i++; 
                    } 
                    fclose($handle); 
                } 
                
                $alreadyRegistered = array();
                $invalidId = array();
                
                foreach ($data2DArray as $item) {
                    
                    $value = explode(',', $item[0]);
                    // validamos que no exista el ID en el sistema
                    
                    $SQL = "SELECT COUNT(id_usuario_c) FROM `users_c` WHERE `id_usuario_c` ='".trim($value[0])."'";
                    $exist= Yii::app()->db->createCommand($SQL)->queryAll();
                    
                    if ($exist[0]["COUNT(id_usuario_c)"]==0) {
                        //validaciones de campos
                        $id_user = trim($value[0]);
                        $name = trim(strtolower(utf8_encode($value[1])));
                        $surename = trim(strtolower(utf8_encode($value[2])));
                        $email = trim(strtolower($value[3]));
                        $country = $value[4];
                        $city = $value[5];
                        
                        if($name ==""){
                            $name = "missing";
                        }
                        if($surename ==""){
                            $surename = "missing";
                        }
                        if($email ==""){
                            $email = "missing";
                        }
                        
                        if(is_numeric($id_user)){
                            $modUtil = new Utilidades();
                            $pass = $modUtil->generateRandomString(6);
                            $SQL = "INSERT INTO `users_c`(`id_usuario_c`, `nombres`, `apellidos`, `email`, `id_cliente`, `ultimo_acceso`, `id_perfil`, `numero_id`, `ruta_foto`, `estado`, `clave`, `clave2`, `fecha_creado`, `id_grupo_cliente`, `id_pais`, `id_country`, `id_ciudad`, `email_certificate`, `isDemo`, `reference`) 
                            VALUES ('".$id_user."','".$name."','".$surename."','".$email."','".$client."','".$date."','6','".$id_user."','','A','".md5($pass)."','".$pass."','".$date."','".$_POST["UsersC"]["id_grupo_cliente"]."','3','".$_POST["UsersC"]["id_pais"]."','".$_POST["UsersC"]["id_ciudad"]."','0','0',NULL)";
                            $exist= Yii::app()->db->createCommand($SQL)->query();
                        }else{
                            
                            array_push ( $invalidId , $value[0] );
                        }
                        
                        
                    }else{
                        array_push ( $alreadyRegistered , $value[0] );
                    }
                    
                }
            } else {
                Yii::app()->user->setFlash('error', 'No file Selected.');
                $this->redirect(array('usersC/create_users'));
            }
            if(empty($noRegTakerArray) && empty($invalidId)){
                Yii::app()->user->setFlash('error', 'The test takers have been successfully registered');
    		}
            $this->render('create_users',array('alreadyRegistered'=>$alreadyRegistered,'invalidId'=>$invalidId));
	    }
	}
	

	
	
	public function actionCsvUsersExams()
	{   
	    $model=new UsersC;
	    $client = $model->id_cliente = Yii::app()->user->getState('cliente');
	    $date = date("Y-m-d H:i:s");
	    if ( isset($_POST["submit"]) ) {
            if ( isset($_FILES["UsersC"]["name"]["file_2"])) {
                // agregamos al log del usuario + last iD
                $SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 1, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'Create multiple test talkers using CSV file ".$_FILES["UsersC"]["name"]["file_2"]."', '')";
                $log= Yii::app()->db->createCommand($SQL)->query(); 
                
                $datos = $_POST;
                //validamos si es un archivo .CSV
                $extension = explode(".", $_FILES["UsersC"]["name"]["file_2"]);
                
                if($extension[1]!="csv"){
                    Yii::app()->user->setFlash('error', 'Please use a .CSV file.  You are using a '.$extension[1].' extension file.');
                    $this->redirect(array('usersC/create_users'));
                }  
                
                //consulto que tipo de licencia se quiere crear
                $sql='SELECT id_licencia FROM licenses_client WHERE id_licencias_cliente = "'.$datos['license_client'].'"';
                $list= Yii::app()->db->createCommand($sql)->queryAll();
                $idLicencia = $list[0]["id_licencia"];
                
                $now = date('Y/m/d');
    		    $mDate = date('Y/m/d', strtotime($datos['date']));    
                $splitDate = explode("/", $datos['date']);
    		    $mm = $splitDate[0];
    		    $dd = $splitDate[1];
    		    $yy = $splitDate[2];
    		    $hr= $datos['hour'];
    		    
    		    $date = $yy."-".$mm."-".$dd." ".$hr;
    		    $datos['date'] = $date;
                
    		    //fin validaciones y variables
    		    $delimiter = "/n";
                //$file = fopen($_FILES["UsersC"]["tmp_name"]["file"],"r");
                if (($handle = fopen($_FILES["UsersC"]["tmp_name"]["file_2"], "r")) !== FALSE) { 
                    $i = 0; 
                    while (($lineArray = fgetcsv($handle, 4000, $delimiter)) !== FALSE) { 
                        for ($j=0; $j<count($lineArray); $j++) { 
                            $data2DArray[$i][$j] = $lineArray[$j]; 
                        } 
                        $i++; 
                    } 
                    fclose($handle); 
                } 
                
                //array para mostrar usuarios ya registrados     
                $alreadyRegistered = array();
                //array para mostrar Ids inválidos 
                $invalidId = array();
                //arrays para mostrar ids que no fueron asignados examenes
                $noExamAssing = array();
                    
                foreach ($data2DArray as $item) {
                
                $value = explode(',', $item[0]);
                $SQL = "SELECT COUNT(id_usuario_c) FROM `users_c` WHERE `id_usuario_c` ='".trim($value[0])."'";
                $exist= Yii::app()->db->createCommand($SQL)->queryAll();    
                
                /*
                $program = $value[4];
                // get the school
                $SQL = "SELECT program_school_city.id_school FROM `program_school_city` INNER JOIN school ON program_school_city.id_school = school.id_school WHERE `id_program` ='".trim($program)."' AND school.id_client = '8605173021' LIMIT 1";
                $query= Yii::app()->db->createCommand($SQL)->queryAll();
                
                
                $school = $query[0]["id_school"];
                $city = $value[5];
                // no tenemos variable period en csv, entonces por default va 1
                */
                $period = "10";
                $id_user = trim($value[0]);
                $modUtil = new Utilidades();
                
                $pass = $modUtil->generateRandomString(6); 
                
                //registramos los usuarios que no existen en el sistema
                if ($exist[0]["COUNT(id_usuario_c)"]==0) {
                            
                    //validaciones de campos
                    
                    $name = utf8_encode(trim(strtolower($value[1])));
                    $surename = utf8_encode(trim(strtolower($value[2])));
                    $email = utf8_encode(trim(strtolower($value[3])));
                    
                            
                    if($name ==""){
                        $name = "missing";
                    }
                    if($surename ==""){
                        $surename = "missing";
                    }
                    if($email ==""){
                        $email = "missing";
                    }
                    
                            
                    if(is_numeric($id_user)){
                        $SQL = "INSERT INTO `users_c`(`id_usuario_c`, `nombres`, `apellidos`, `email`, `id_cliente`, `ultimo_acceso`, `id_perfil`, `numero_id`, `ruta_foto`, `estado`, `clave`, `clave2`, `fecha_creado`, `id_grupo_cliente`, `id_pais`, `id_country`, `id_ciudad`, `email_certificate`, `isDemo`, `reference`) 
                        VALUES ('".$id_user."','".$name."','".$surename."','".$email."','".$client."','".$date."','6','".$id_user."','','A','".md5($pass)."','".$pass."','".$date."','24',3,'COL','".$city."','1','0',NULL)";
                        $exist= Yii::app()->db->createCommand($SQL)->query();
                        
                         // asignamos las licencias
                            //Asociar la licencia al usuario
                            $modLicUsu = new LicensesUser();
                            $modLicUsu->id_licencia_cliente = $datos['license_client'];
                            $modLicUsu->id_usuario=$id_user;
                            $modLicUsu->fecha_asignacion = new CDbExpression('NOW()');
                            $modLicUsu->fecha_presentacion = $date;
                            $modLicUsu->hora = $datos['hour'];
                            $modLicUsu->estado="A";
                            $modLicUsu->save();
                            
                            
                            //Gastar la licencia
                            $modUtil->gastarLicencia($datos['license_client']);
                            //Asignar los exámenes al usuario
                            $modLicenciasCliente = LicensesClient::model()->findByPk($modLicUsu->id_licencia_cliente);
                            $modExamenes = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>$modLicenciasCliente->id_licencia));
                                    
                            foreach($modExamenes as $examen){
                                $modExamenUsuario = new ExamsUser();
                                $modExamenUsuario->id_examen = $examen->id_examen;
                                $modExamenUsuario->id_licencia_usuario = $modLicUsu->id_licencias_usuario;
                                $modExamenUsuario->fecha_presentacion = $date;
                                $modExamenUsuario->hora = $datos['hour'];
                                $modExamenUsuario->salon = $datos['room'];
                                if(strlen($datos['tutor'])>0){
                                    $modExamenUsuario->tutor = $datos['tutor']; 
                                }
                                $modExamenUsuario->estado = "A";
                                $modExamenUsuario->save();    
                            }
                            
                            /*
                            // Guardamos el registro en la tabla de period
                            $SQL = "INSERT INTO `users_c_period`(`id_users_c_period`, `id_usuario_c`, `id_cliente`, `period`, `id_city`, `id_school`, `id_program`, `id_licencias_usuario`) 
                            VALUES ('', '".$id_user."', '8605173021', '".$period."', '0', '0', '0', '".$modLicUsu->id_licencias_usuario."')";
                            $list= Yii::app()->db->createCommand($SQL)->query();*/
                        
                    }else{
                            array_push ( $invalidId , $value[0] );
                        }
                }else{
                    
                       //consultamos si tiene licencias pendientes y las finalizamos
                    $SQL='SELECT id_licencias_usuario, id_usuario, id_licencia_cliente, id_licencia, licenses_user.fecha_presentacion, licenses_user.estado FROM `licenses_user`
                        INNER JOIN `licenses_client`
                        ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente
                        WHERE `id_usuario` = "'.trim($value[0]).'" AND licenses_user.estado != "F"
                        ORDER BY licenses_user.fecha_presentacion DESC';
                    $list= Yii::app()->db->createCommand($SQL)->queryAll();
                        
                    if(sizeof($list)>0){
                        foreach($list as $lic){
                            $SQL = "UPDATE `licenses_user` SET `estado`='F' WHERE `id_licencias_usuario`='".$lic["id_licencias_usuario"]."'";
                            $passUpdate= Yii::app()->db->createCommand($SQL)->query();
                        }    
                    } 
                        
                    //contraseña ramdom
                    $SQL = "UPDATE `users_c` SET `clave`='".md5($pass)."', `clave2`='".$pass."', `id_cliente`='".$client."',  `email_certificate`=1 WHERE `id_usuario_c`='".$id_user."'";
                    $passUpdate= Yii::app()->db->createCommand($SQL)->query();
                        
                    //validamos la disponibilidad de licencias
                    $SQL = "SELECT SUM(cantidad-utilizados) as total FROM `licenses_client` WHERE `id_licencias_cliente` ='".$datos["license_client"]."'";
                    $exist= Yii::app()->db->createCommand($SQL)->queryAll();
                                
                    if($exist[0]["total"]>0){
                            
                    // asignamos las licencias
                    //Asociar la licencia al usuario
                    $modLicUsu = new LicensesUser();
                    $modLicUsu->id_licencia_cliente = $datos['license_client'];
                    $modLicUsu->id_usuario=$id_user;
                    $modLicUsu->fecha_asignacion = new CDbExpression('NOW()');
                    $modLicUsu->fecha_presentacion = $date;
                    $modLicUsu->hora = $datos['hour'];
                    $modLicUsu->estado="A";
                    $modLicUsu->save();
                            
                    //Gastar la licencia
                    $modUtil->gastarLicencia($datos['license_client']);
                    //Asignar los exámenes al usuario
                    $modLicenciasCliente = LicensesClient::model()->findByPk($modLicUsu->id_licencia_cliente);
                    $modExamenes = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>$modLicenciasCliente->id_licencia));
                    
                    foreach($modExamenes as $examen){
                        $modExamenUsuario = new ExamsUser();
                        $modExamenUsuario->id_examen = $examen->id_examen;
                        $modExamenUsuario->id_licencia_usuario = $modLicUsu->id_licencias_usuario;
                        $modExamenUsuario->fecha_presentacion = $date;
                        $modExamenUsuario->hora = $datos['hour'];
                        $modExamenUsuario->salon = $datos['room'];
                        if(strlen($datos['tutor'])>0){
                            $modExamenUsuario->tutor = $datos['tutor']; 
                        }
                        $modExamenUsuario->estado = "A";
                        $modExamenUsuario->save();    
                    }
                    
                    /*        
                    // Guardamos el registro en la tabla de period
                    $SQL = "INSERT INTO `users_c_period`(`id_users_c_period`, `id_usuario_c`, `id_cliente`, `period`, `id_city`, `id_school`, `id_program`, `id_licencias_usuario`) 
                    VALUES ('', '".$id_user."', '8605173021', '".$period."', '".$city."', '".$school."', '".$program."', '".$modLicUsu->id_licencias_usuario."')";
                    $list= Yii::app()->db->createCommand($SQL)->query();*/
                }else{
                            
                            //PENDIENTE
                            //buscar licencias del mismo tipo y seguir asignando 
                            
                            array_push ( $noExamAssing , $value[0] );
                        }   
                    }
                    
                         
                    }
                    
            }
        } else {
            Yii::app()->user->setFlash('error', 'No file Selected.');
            $this->redirect(array('usersC/create_users'));
        }
    	if(empty($noRegTakerArray) && empty($invalidId)){
            Yii::app()->user->setFlash('error', 'The test takers have been successfully registered');
    	}
        $this->render('create_users',array('alreadyRegistered'=>$alreadyRegistered,'invalidId'=>$invalidId, 'noExamAssing'=>$noExamAssing));
}

public function actionCsvUsersExamsFuaa()
	{   
	    
	    $model=new UsersC;
	    $client = $model->id_cliente = Yii::app()->user->getState('cliente');
	    $date = date("Y-m-d H:i:s");
	    if ( isset($_POST["submit"]) ) {
            if ( isset($_FILES["UsersC"]["name"]["file_2"])) {
                
                // agregamos al log del usuario + last iD
                $SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 1, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'Create multiple test talkers using CSV file ".$_FILES["UsersC"]["name"]["file_2"]."', '')";
                $log= Yii::app()->db->createCommand($SQL)->query(); 
                
                $datos = $_POST;
                //validamos si es un archivo .CSV
                $extension = explode(".", $_FILES["UsersC"]["name"]["file_2"]);
                
                if($extension[1]!="csv"){
                    Yii::app()->user->setFlash('error', 'Please use a .CSV file.  You are using a '.$extension[1].' extension file.');
                    $this->redirect(array('usersC/create_users'));
                }  
                
                //consulto que tipo de licencia se quiere crear
                $sql='SELECT id_licencia FROM licenses_client WHERE id_licencias_cliente = "'.$datos['license_client'].'"';
                $list= Yii::app()->db->createCommand($sql)->queryAll();
                $idLicencia = $list[0]["id_licencia"];
                
                $now = date('Y/m/d');
    		    $mDate = date('Y/m/d', strtotime($datos['date']));    
                $splitDate = explode("/", $datos['date']);
    		    $mm = $splitDate[0];
    		    $dd = $splitDate[1];
    		    $yy = $splitDate[2];
    		    $hr= $datos['hour'];
    		    
    		   // $date = $yy."-".$mm."-".$dd." ".$hr;
    		    $datos['date'] = $date;
    		    //fin validaciones y variables
    		    $delimiter = "/n";
                //$file = fopen($_FILES["UsersC"]["tmp_name"]["file"],"r");
                if (($handle = fopen($_FILES["UsersC"]["tmp_name"]["file_2"], "r")) !== FALSE) { 
                    $i = 0; 
                    while (($lineArray = fgetcsv($handle, 4000, $delimiter)) !== FALSE) { 
                        for ($j=0; $j<count($lineArray); $j++) { 
                            $data2DArray[$i][$j] = $lineArray[$j]; 
                        } 
                        $i++; 
                    } 
                    fclose($handle); 
                } 
                
                //array para mostrar usuarios ya registrados     
                $alreadyRegistered = array();
                //array para mostrar Ids inválidos 
                $invalidId = array();
                //arrays para mostrar ids que no fueron asignados examenes
                $noExamAssing = array();
                    
                foreach ($data2DArray as $item) {
                
                $value = explode(',', $item[0]);
                $SQL = "SELECT COUNT(id_usuario_c) FROM `users_c` WHERE `id_usuario_c` ='".trim($value[0])."'";
                $exist= Yii::app()->db->createCommand($SQL)->queryAll();    
                $program = $value[4];
                
                // get the school
                $SQL = "SELECT program_school_city.id_school FROM `program_school_city` INNER JOIN school ON program_school_city.id_school = school.id_school WHERE `id_program` ='".trim($program)."' AND school.id_client = '8605173021' LIMIT 1";
                $query= Yii::app()->db->createCommand($SQL)->queryAll();
                
                
                $school = $query[0]["id_school"];
                $city = $value[5];
                // no tenemos variable period en csv, entonces por default va 1
                $period = $value[6];
               // $date = $value[7];
                $explode_hour = explode(" ", $date);
                $hour_separate = explode(":", $explode_hour[1]);
                
                $hr = $hour_separate[0].":00";
                
                
                
                $id_user = trim($value[0]);
                $modUtil = new Utilidades();
                
                $pass = $modUtil->generateRandomString(6); 
                
                //registramos los usuarios que no existen en el sistema
                if ($exist[0]["COUNT(id_usuario_c)"]==0) {
                            
                    //validaciones de campos
                    
                    $name = utf8_encode(trim(strtolower($value[1])));
                    $surename = utf8_encode(trim(strtolower($value[2])));
                    $email = utf8_encode(trim(strtolower($value[3])));
                    
                            
                    if($name ==""){
                        $name = "missing";
                    }
                    if($surename ==""){ 
                        $surename = "missing";
                    }
                    if($email ==""){
                        $email = "missing";
                    }
                    
                            
                    if(is_numeric($id_user)){
                        $SQL = "INSERT INTO `users_c`(`id_usuario_c`, `nombres`, `apellidos`, `email`, `id_cliente`, `ultimo_acceso`, `id_perfil`, `numero_id`, `ruta_foto`, `estado`, `clave`, `clave2`, `fecha_creado`, `id_grupo_cliente`, `id_pais`, `id_country`, `id_ciudad`, `email_certificate`, `isDemo`, `reference`) 
                        VALUES ('".$id_user."','".$name."','".$surename."','".$email."','".$client."','".$date."','6','".$id_user."','','A','".md5($pass)."','".$pass."','".$date."','24',3,'COL','".$city."','1','0',NULL)";
                        $exist= Yii::app()->db->createCommand($SQL)->query();
                        
                         // asignamos las licencias
                            //Asociar la licencia al usuario
                            $modLicUsu = new LicensesUser();
                            $modLicUsu->id_licencia_cliente = $datos['license_client'];
                            $modLicUsu->id_usuario=$id_user;
                            $modLicUsu->fecha_asignacion = new CDbExpression('NOW()');
                            $modLicUsu->fecha_presentacion = $date;
                            $modLicUsu->hora = $hr;
                            $modLicUsu->estado="A";
                            $modLicUsu->save();
                            
                            
                            //Gastar la licencia
                            $modUtil->gastarLicencia($datos['license_client']);
                            //Asignar los exámenes al usuario
                            $modLicenciasCliente = LicensesClient::model()->findByPk($modLicUsu->id_licencia_cliente);
                            $modExamenes = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>$modLicenciasCliente->id_licencia));
                                    
                            foreach($modExamenes as $examen){
                                $modExamenUsuario = new ExamsUser();
                                $modExamenUsuario->id_examen = $examen->id_examen;
                                $modExamenUsuario->id_licencia_usuario = $modLicUsu->id_licencias_usuario;
                                $modExamenUsuario->fecha_presentacion = $date;
                                $modExamenUsuario->hora = $hr;
                                $modExamenUsuario->salon = $datos['room'];
                                if(strlen($datos['tutor'])>0){
                                    $modExamenUsuario->tutor = $datos['tutor']; 
                                }
                                $modExamenUsuario->estado = "A";
                                $modExamenUsuario->save();    
                            }
                            
                            // Guardamos el registro en la tabla de period
                            $SQL = "INSERT INTO `users_c_period`(`id_users_c_period`, `id_usuario_c`, `id_cliente`, `period`, `id_city`, `id_school`, `id_program`, `id_licencias_usuario`) 
                            VALUES ('', '".$id_user."', '".$client."', '".$period."', '".$city."', '".$school."', '".$program."', '".$modLicUsu->id_licencias_usuario."')";
                            $list= Yii::app()->db->createCommand($SQL)->query();
                        
                    }else{
                            array_push ( $invalidId , $value[0] );
                        }
                }else{
                    
                       //consultamos si tiene licencias pendientes y las finalizamos
                    $SQL='SELECT id_licencias_usuario, id_usuario, id_licencia_cliente, id_licencia, licenses_user.fecha_presentacion, licenses_user.estado FROM `licenses_user`
                        INNER JOIN `licenses_client`
                        ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente
                        WHERE `id_usuario` = "'.trim($value[0]).'" AND licenses_user.estado != "F"
                        ORDER BY licenses_user.fecha_presentacion DESC';
                    $list= Yii::app()->db->createCommand($SQL)->queryAll();
                        
                    if(sizeof($list)>0){
                        foreach($list as $lic){
                            $SQL = "UPDATE `licenses_user` SET `estado`='F' WHERE `id_licencias_usuario`='".$lic["id_licencias_usuario"]."'";
                            $passUpdate= Yii::app()->db->createCommand($SQL)->query();
                        }    
                    } 
                        
                    //contraseña ramdom
                    $SQL = "UPDATE `users_c` SET `clave`='".md5($pass)."', `clave2`='".$pass."', `id_cliente`='".$client."',  `email_certificate`=1 WHERE `id_usuario_c`='".$id_user."'";
                    $passUpdate= Yii::app()->db->createCommand($SQL)->query();
                        
                    //validamos la disponibilidad de licencias
                    $SQL = "SELECT SUM(cantidad-utilizados) as total FROM `licenses_client` WHERE `id_licencias_cliente` ='".$datos["license_client"]."'";
                    $exist= Yii::app()->db->createCommand($SQL)->queryAll();
                                
                    if($exist[0]["total"]>0){
                            
                    // asignamos las licencias
                    //Asociar la licencia al usuario
                    $modLicUsu = new LicensesUser();
                    $modLicUsu->id_licencia_cliente = $datos['license_client'];
                    $modLicUsu->id_usuario=$id_user;
                    $modLicUsu->fecha_asignacion = new CDbExpression('NOW()');
                    $modLicUsu->fecha_presentacion = $date;
                    $modLicUsu->hora = $hr;
                    $modLicUsu->estado="A";
                    $modLicUsu->save();
                            
                    //Gastar la licencia
                    $modUtil->gastarLicencia($datos['license_client']);
                    //Asignar los exámenes al usuario
                    $modLicenciasCliente = LicensesClient::model()->findByPk($modLicUsu->id_licencia_cliente);
                    $modExamenes = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>$modLicenciasCliente->id_licencia));
                    
                    foreach($modExamenes as $examen){
                        $modExamenUsuario = new ExamsUser();
                        $modExamenUsuario->id_examen = $examen->id_examen;
                        $modExamenUsuario->id_licencia_usuario = $modLicUsu->id_licencias_usuario;
                        $modExamenUsuario->fecha_presentacion = $date;
                        $modExamenUsuario->hora = $hr;
                        $modExamenUsuario->salon = $datos['room'];
                        if(strlen($datos['tutor'])>0){
                            $modExamenUsuario->tutor = $datos['tutor']; 
                        }
                        $modExamenUsuario->estado = "A";
                        $modExamenUsuario->save();    
                    }
                            
                    // Guardamos el registro en la tabla de period
                    $SQL = "INSERT INTO `users_c_period`(`id_users_c_period`, `id_usuario_c`, `id_cliente`, `period`, `id_city`, `id_school`, `id_program`, `id_licencias_usuario`) 
                    VALUES ('', '".$id_user."', '".$client."', '".$period."', '".$city."', '".$school."', '".$program."', '".$modLicUsu->id_licencias_usuario."')";
                    $list= Yii::app()->db->createCommand($SQL)->query();
                }else{
                            
                            //PENDIENTE
                            //buscar licencias del mismo tipo y seguir asignando 
                            
                            array_push ( $noExamAssing , $value[0] );
                        }   
                    }
                    
                         
                    }
                    
            }
        } else {
            Yii::app()->user->setFlash('error', 'No file Selected.');
            $this->redirect(array('usersC/create_users'));
        }
    	if(empty($noRegTakerArray) && empty($invalidId)){
            Yii::app()->user->setFlash('error', 'The test takers have been successfully registered');
    	}
        $this->render('create_users',array('alreadyRegistered'=>$alreadyRegistered,'invalidId'=>$invalidId, 'noExamAssing'=>$noExamAssing));
    }
    
    public function actionCheckList()
	{   
	    
	    $model=new UsersC;
	    $client = $model->id_cliente = Yii::app()->user->getState('cliente');
	    $date = date("Y-m-d H:i:s");
	    if ( isset($_POST["submit"]) ) {
	        
            if ( isset($_FILES["UsersC"]["name"]["file"])) {
                
                // agregamos al log del usuario + last iD
                $SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 1, '".Yii::app()->user->getState('usuario')."', NOW(), '', '', 'check taker list using CSV file ".$_FILES["UsersC"]["name"]["file_2"]."', '')";
                $log= Yii::app()->db->createCommand($SQL)->query(); 
                
                $datos = $_POST;
                //validamos si es un archivo .CSV
                $extension = explode(".", $_FILES["UsersC"]["name"]["file"]);
                
                if($extension[1]!="csv"){
                    Yii::app()->user->setFlash('error', 'Please use a .CSV file.  You are using a '.$extension[1].' extension file.');
                    $this->redirect(array('usersC/create_users'));
                }  
                /*
                //consulto que tipo de licencia se quiere crear
                $sql='SELECT id_licencia FROM licenses_client WHERE id_licencias_cliente = "'.$datos['license_client'].'"';
                $list= Yii::app()->db->createCommand($sql)->queryAll();
                $idLicencia = $list[0]["id_licencia"];
                
                $now = date('Y/m/d');
    		    $mDate = date('Y/m/d', strtotime($datos['date']));    
                $splitDate = explode("/", $datos['date']);
    		    $mm = $splitDate[0];
    		    $dd = $splitDate[1];
    		    $yy = $splitDate[2];
    		    $hr= $datos['hour'];
    		    
    		   // $date = $yy."-".$mm."-".$dd." ".$hr;
    		    $datos['date'] = $date;
                */
    		    //fin validaciones y variables
    		    $delimiter = "/n";
                //$file = fopen($_FILES["UsersC"]["tmp_name"]["file"],"r");
                if (($handle = fopen($_FILES["UsersC"]["tmp_name"]["file"], "r")) !== FALSE) { 
                    $i = 0; 
                    while (($lineArray = fgetcsv($handle, 4000, $delimiter)) !== FALSE) { 
                        for ($j=0; $j<count($lineArray); $j++) { 
                            $data2DArray[$i][$j] = $lineArray[$j]; 
                        } 
                        $i++; 
                    } 
                    fclose($handle); 
                } 
                
                //array para mostrar usuarios ya registrados     
                $alreadyRegistered = array();
                    
                foreach ($data2DArray as $item) {
                    $value = explode(',', $item[0]);
                    $SQL = "SELECT * FROM `users_c` WHERE `id_usuario_c` ='".trim($value[0])."'";
                    $exist= Yii::app()->db->createCommand($SQL)->queryAll();    
                    
                    $fullname = $exist[0]["nombres"]." ".$exist[0]["apellidos"];
                    
                    $taker = array();
                    //registramos los usuarios que no existen en el sistema
                    if(isset($exist[0]["id_usuario_c"])){
                        
                        //get tecs2go info
                        $SQL = "SELECT * FROM `proctorv_request` WHERE `id_user` ='".$value[0]."'";
                        $tecs2go = Yii::app()->db->createCommand($SQL)->queryAll();
                        
                        $tecs2goReg = $tecs2go[0]["id_proctorv_request"];
                        
                        if($tecs2go[0]["id_proctorv_request"] == NULL){
                            $tecs2goReg = "NOT REGISTERED";
                        }
                        
                        if($tecs2go[0]["id_license"] == ""){
                            
                            $license = "NO";
                        }else{
                            $license = $tecs2go[0]["id_license"];
                            //get tecs2go info
                             $SQL = "SELECT licenses_user.estado, id_licencia_cliente, id_cliente FROM `licenses_user` INNER JOIN licenses_client ON licenses_client.id_licencias_cliente = licenses_user.id_licencia_cliente WHERE `id_licencias_usuario`  ='".$license."'";
                            $license_status = Yii::app()->db->createCommand($SQL)->queryAll();
                            
                            
                            
                            switch ($license_status[0]["estado"]) {
                                case 'A':
                                    $status = 'PENDING';
                                    break;
                                case 'F':
                                    $status = 'COMPLETED';
                                    break;
                                case 'B':
                                    $status = 'REVOKED';
                                    break;    
                                default:
                                    // code...
                                    break;
                            }
                        }
                        
                        array_push($taker, $value[0]);
                        array_push($taker, $fullname);
                        array_push($taker, $exist[0]["email"]);
                        array_push($taker, $exist[0]["telefono"]);
                        array_push($taker, "YES");
                        array_push($taker, $license);
                        array_push($taker, $license_status[0]["id_cliente"]);
                        array_push($taker, $license_status[0]["id_licencia_cliente"]);
                        array_push($taker, $status);
                       
                        
                        
                    }else{
                        
                        array_push($taker, $value[0]);
                        array_push($taker, $fullname);
                        array_push($taker, $exist[0]["email"]);
                        array_push($taker, $exist[0]["phone"]);
                        array_push($taker, "NO");
                        array_push($taker, "NO");
                        array_push($taker, "id_client");
                        array_push($taker, "id_license_cliente");
                        array_push($taker, "NO");
                        
                        
                    }
                    array_push ($alreadyRegistered, $taker);
                    
                         
                }
                
                
                    
            } 
        } else {
            Yii::app()->user->setFlash('error', 'No file Selected.');
            $this->redirect(array('usersC/user-list'));
        }
    	if(empty($noRegTakerArray) && empty($invalidId)){
            Yii::app()->user->setFlash('error', 'The taker list is displaying below');
    	}
        $this->render('user-list',array('alreadyRegistered'=>$alreadyRegistered));
    }
    
    
    public function actionReportCheckList()
	{   
	    //get the array from POST
        $data = json_decode(htmlspecialchars_decode($_POST['reportInfo']));
        
        // output headers so that the file is downloaded rather than displayed
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=list-check-report-'.date('Y_m_d_H_i_s').'.csv');
        
        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');	
		
		//CSV header
		fputcsv($output, array('ID_TAKER','FULL NAME','EMAIL','CONTACT','TECS2GO REGISTERED','TECS2GO LICENSE','TECS2GO STATUS'));
		
		//read the bidimensional array
		
		foreach ($data as $line) {
			    //get data from array
				$taker['id'] = $line[0];
            	$taker['name'] = utf8_decode($line[1]);
            	$taker['email'] = $line[2];
            	$taker['contact'] = $line[3];
            	$taker['tecs2go_reg'] = $line[4];
            	$taker['tecs2go_lic'] = $line[5];
            	$taker['tecs2go_sta'] = $line[6];
				fputcsv($output, $taker);
			}
			
        die();
    }



	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return UsersC the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
	    
		//$model=UsersC::model()->findByAttributes(array('id_usuario_c'=>$id));
		$sql='SELECT * FROM `users_c` WHERE `id_usuario_c` ="'.$idUsuario.'"';
        $model= Yii::app()->db->createCommand($sql)->queryAll();
		
		
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param UsersC $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='users-c-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}


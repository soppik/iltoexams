<?php

class DefaultController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}
    
    public function actionRememberPass(){
            $model=new RememberPass;
            if(isset($_POST['RememberPass'])){
                $datos=$_POST['RememberPass'];
               $modUtil = new Utilidades();
               if($modUtil->recoverPass($datos["username"],$datos["email"])){
                   $this->redirect(Yii::app()->createUrl('Clients/default/login'));
               }
            }
            $this->render('rememberPass',array('model'=>$model));
        }
        
        public function actionLogin()
	{
	    
	    //caemos al login
	    $model=new LoginForm;
	    
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
		    var_dump($model);die();
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
    
        
		// collect user input data when the user puts de info
		if(isset($_POST['LoginForm']))
		{
		    //admin email verification
		    
		    if($_POST["flag_econfirm"]==1){
		        $SQL = "UPDATE `password_change` SET `email_confirm`=1 WHERE id_usuario_c='".$_POST["LoginForm"]['username']."'";
                $eConfirm =  Yii::app()->db->createCommand($SQL)->query();
		    }
		    
		    
		    
		    $_POST['LoginForm'] = array_map("trim", $_POST['LoginForm']);
		    //validamos el estado del usuario antes de loggear
		    
		    //enlaces especiales Universidades
		    switch ($_POST["LoginForm"]['username']) {
		        case '8903990166':
		            	$this->redirect('tecs_udv.php');
		            break;
		        case '8600383741':
		            	$this->redirect('tecs_corpas.php');
		            break;
		        case '8605173021':
		            	$this->redirect('ede_aa.php');
		            break;
		        case '86051730211':
		            	$this->redirect('ede_areandina.php');
		            break;
		        case '81030525':
		            	$this->redirect('ede_uvpmx.php');
		            break;
		        case '8909854173':
		            	$this->redirect('ede_uam.php');
		            break;
		        case '8600560707':
		            	$this->redirect('ede_uan.php');
		            break;
		        case '8001162172':
		            	$this->redirect('ede_uniminuto.php');
		            break;
		        case 'utpanam':
		            	$this->redirect('ede_utp.php');
		            break;
		        case 'tecsdemo':
		            	$this->redirect('tecs_demo.php');
		        break;
		        default:
		        // code...
		            break;
		    }
		    
		    //check login type: TECS, ADMIN, EDE
            $type = $_POST["LoginForm"]['type'];
		    
		    //varlidamos que el usuario exista
		    $SQL = "SELECT id_usuario_c FROM `users_c` WHERE id_usuario_c='".$_POST["LoginForm"]['username']."'";
            $exist =  Yii::app()->db->createCommand($SQL)->queryAll();
            
            if(empty($exist)){
                switch ($type) {
                	case '1':
                	    $this->redirect('index.php?r=Clients/default/login&type=1&message=1');    
                	break;
                	case '2':
                	    $this->redirect('index.php?r=Clients/default/login&type=2&message=1');  
                	break;
                	case '3':
                	    $this->redirect('index.php?r=Clients/default/login&type=3&message=1');  
                	break;
                	default:
                	    // redireccionamos al login con mensaje de no hay licencia disponible
                		$this->redirect('index.php?r=Clients/default/login&type=1&message=1');  
                	break;
                			 }
            }
		    
		    //user status 
		    $SQL = "SELECT id_usuario_c, estado, id_perfil, id_cliente, connected, email, clave2 FROM `users_c` WHERE id_usuario_c='".$_POST["LoginForm"]['username']."'";
            $status =  Yii::app()->db->createCommand($SQL)->queryAll();
            
            //client status
            $SQL = "SELECT estado FROM `clients` WHERE id_cliente='".$status[0]['id_cliente']."'";
            $clientStatus =  Yii::app()->db->createCommand($SQL)->queryAll();
            
            //validamos si debe actualizar la contraseña SOLO ADMINS
            if($status[0]['id_perfil']!=6){
                $SQL = "SELECT date, email_confirm FROM `password_change` WHERE id_usuario_c='".$_POST["LoginForm"]['username']."'";
                $passUpdated =  Yii::app()->db->createCommand($SQL)->queryAll();
                
                if(empty($passUpdated[0]["date"])){
                    $email = $status[0]['email'];
                    $chain = explode('@', $email);
                    $email2 = $chain[1];
                    $email = substr($email,0,3);
                    $email .= '********@';
                    $email .= $chain[1];
                    Yii::app()->user->setFlash('error','Update your password to access the administrator dashboard.  An email will be sent to '.$email.'');
                    $this->redirect('index.php?r=Clients/default/changePass');
                }else{
                    if($passUpdated[0]["email_confirm"]!=1){
                        $this->redirect('index.php?r=Clients/default/login&type=2&message=9');
                    }
                }
            }
            
             
            if($clientStatus[0]['estado'] == 'A'){
                
                if($status[0]['estado']=='A' || $status[0]['estado']=='R'){
                    
                    
                    // get IP address and DNS
                    $ip= isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? 
                    $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
                    $dns = gethostbyaddr($_SERVER['REMOTE_ADDR']); 
                    $today = date("Y-m-d H:i:s");
                    
                    //generamos el Hash del login
                    $session_hash= md5($_POST["LoginForm"]['username'] . $ip . $dns);
                    
                    // revisamos si el usuario se encuentra logeado actualmente
                    // pudo haber cerrado el navegador y pretende volver a abrir la cuenta
                    
                    if($status[0]['connected'] == 1){
                        
                        //consultamos la ultima sesion iniciada por el usuario
            		    $SQL = "SELECT * FROM `user_auth` WHERE `id_usuario_c` = '".$_POST["LoginForm"]['username']."' ORDER BY id_user_auth DESC LIMIT 1";
                        $last_session_check =  Yii::app()->db->createCommand($SQL)->queryAll();
                        
                        // comparamos el hash de ingreso, si es el mismo que el registro "conectado", permitimos pasar
                        //porque es el mismo computador usado para iniciar sesion, se supone quedebe ser un solo usuario por pc
                        if($last_session_check[0]['hash'] != $session_hash){
                            if($last_session_check[0]['date_expire']>$today){
                                switch ($type) {
                                	case '1':
                                	    $this->redirect('index.php?r=Clients/default/login&type=1&message=11');    
                                	break;
                                	case '2':
                                	    $this->redirect('index.php?r=Clients/default/login&type=2&message=11');  
                                	break;
                                	case '3':
                                	    $this->redirect('index.php?r=Clients/default/login&type=3&message=11');  
                                	break;
                                	default:
                                	    // redireccionamos al login con mensaje de no hay licencia disponible
                                		$this->redirect('index.php?r=Clients/default/login&type=1&message=11');  
                                	break;
                                }  
                            }
                            
                        }
                        
                        //si pasa las validaciones anteriores iniciamos sesion normalmente
                        if($type == 3){
                            $_POST['LoginForm']['password'] = $status[0]["clave2"];
                        }
                        $model->attributes=$_POST['LoginForm'];
                        
                        
                        
        			    // validate user input and redirect to the previous page if valid
        			    if($model->validate() && $model->login()){
        			        //var_dump($_POST["flag"]);die();
        			       
        			        //ACTUALIZAMOS EL ESTADO DEL USUARIO EN LINEA EN LA PLATAFORMA
        			        $SQL = "UPDATE `users_c` SET `connected`=1 WHERE id_usuario_c='".$_POST["LoginForm"]['username']."'";
                            $eConfirm =  Yii::app()->db->createCommand($SQL)->query();
                            
                        
                            //iniciamos la sesion 
                            $session=new CHttpSession;
                            $session->open();
                            $session['user'] = $_POST["LoginForm"]['username'];
                            $session['hash'] = $session_hash;
                            $session['ip_address'] = $ip;
                            $session['date_login'] = $today;
                            $session['profile'] = $status[0]['id_perfil'];
                            // hay que sumar 2.5 horas 
                            $session['date_expire'] = date('Y-m-d H:i:s', strtotime("+60 minutes", strtotime($today)));
                            
                            //guardamos el registro de la tabla
                            $SQL = "INSERT INTO `user_auth` (`id_user_auth`, `id_usuario_c`, `id_session`, `hash`, `date_login`, `ip_address`, `date_expire`) VALUES (NULL, '".$_POST["LoginForm"]['username']."', '".$session_id."', '".$session_hash."', '".$session['date_login']."', '".$ip."', '".$session['date_expire']."')";
                            $log= Yii::app()->db->createCommand($SQL)->query(); 
                            
                            // agregamos al log del usuario
                            if($status[0]['id_perfil']!=6){
                                $SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`) VALUES (NULL, 0, '".$_POST["LoginForm"]['username']."', '".$session['date_login']."', '', '', 'login and start session')";
                                $log= Yii::app()->db->createCommand($SQL)->query(); 
                            }
                           
                            //verificamos 
                            switch ($type) {
                			     case '1':
                			        $license = $this->CheckPendingTecs($_POST["LoginForm"]['username']);
                			        $link = 'index.php?r=Clients/default/index&license='.base64_encode($license).'';
                			        if($license == "" || $license = NULL){
                			            // redireccionamos al login con mensaje de no hay licencia disponible
                			            $this->redirect('index.php?r=Clients/default/login&type=1&message=13');    
                			        }
                			        //si hay licencia tecs disponible para presentar, generamos el link de ingreso y dejamos pasar
                			        $this->redirect($link);
                			        
                			     break;
                			     case '2':
                			        
                			         //si un taker desea ingresar por la sección de administrador a un examen
                			       if($status[0]['id_perfil'] == 6){
                			           // redireccionamos al login con mensaje de no hay licencia disponible
                			            $this->redirect('index.php?r=Clients/default/login&type=2&message=12');  
                			       }    
                			       $license = 0;
                			        switch ($_POST["flag"]) {
                			            case '0':
                			                $this->redirect('index.php?r=Clients/default/index');
                			            break;
                			            case '1':
                			                $this->redirect('index.php?r=Clients/MyAccount/orderHistory');
                			            break;
                			            default:
                			              //  var_dump("mandamos el usuario al inde exam default");die();
                			                $this->redirect('index.php?r=Clients/default/index');
                			            break;
                			        }
                			     break;
                			     case '3':
                			        $license = $this->CheckPendingEde($_POST["LoginForm"]['username']);
                			        
                			        $link = 'index.php?r=Clients/default/index&license='.base64_encode($license).'';
                			        if($license == "" || $license = NULL){
                			            // redireccionamos al login con mensaje de no hay licencia disponible
                			            $this->redirect('index.php?r=Clients/default/login&type=1&message=14');    
                			        }
                			        $this->redirect($link);
                			     break;
                			     default:
                			        // redireccionamos al login con mensaje de no hay licencia disponible
                			            $this->redirect('index.php?r=Clients/default/login&type=1&message=1');  
                			      break;
                			 }
                            
        			        
        	       			
                		} else {
                		    switch ($type) {
                            	case '1':
                            	    $this->redirect('index.php?r=Clients/default/login&type=1&message=2');    
                            	break;
                            	case '2':
                            	    $this->redirect('index.php?r=Clients/default/login&type=2&message=2');  
                            	break;
                            	case '3':
                            	    $this->redirect('index.php?r=Clients/default/login&type=3&message=2');  
                            	break;
                            	default:
                            	    // redireccionamos al login con mensaje de no hay licencia disponible
                            		$this->redirect('index.php?r=Clients/default/login&type=1&message=2');  
                            	break;
                            }
                		    
                		} 
                        
                        
                        
                    }else{
                        
                       $model->attributes=$_POST['LoginForm'];
                       //si pasa las validaciones anteriores iniciamos sesion normalmente
                        if($type == 3){
                            $_POST['LoginForm']['password'] = $status[0]["clave2"];
                        }
                        $model->attributes=$_POST['LoginForm'];
        			    // validate user input and redirect to the previous page if valid
        			    if($model->validate() && $model->login()){
        			        //var_dump($_POST["flag"]);die();
        			        
        			        //usuario para presentar demo TECS
        			        if($status[0]['id_usuario_c'] == '1001'){
                                //ACTUALIZAMOS EL ESTADO DEL USUARIO DEMO TECS EN LINEA EN LA PLATAFORMA
            			        $SQL = "UPDATE `users_c` SET `connected`=0 WHERE id_usuario_c='".$_POST["LoginForm"]['username']."'";
                                $eConfirm =  Yii::app()->db->createCommand($SQL)->query(); 
                            }
                            
                            
                            //iniciamos la sesion 
                            $session=new CHttpSession;
                            $session->open();
                            $session_id = $session->getSessionID();
                            $session['user'] = $_POST["LoginForm"]['username'];
                            $session['hash'] = $session_hash;
                            $session['ip_address'] = $ip;
                            $session['date_login'] = $today;
                            $session['date_expire'] = date('Y-m-d H:i:s', strtotime("+1 hours", strtotime($today)));
                            
                            //guardamos el registro de la tabla
                            $SQL = "INSERT INTO `user_auth` ( `id_usuario_c`, `id_session`, `hash`, `date_login`, `ip_address`, `date_expire`) VALUES ('".$_POST["LoginForm"]['username']."', '".$session_id."', '".$session_hash."', '".$session['date_login']."', '".$ip."', '".$session['date_expire']."')";
                            $log= Yii::app()->db->createCommand($SQL)->query(); 
                            
                             // agregamos al log del usuario
                            if($status[0]['id_perfil']!=6){
                                $SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`) VALUES (NULL, 0, '".$_POST["LoginForm"]['username']."', '".$session['date_login']."', '', '', 'login and start session')";
                                $log= Yii::app()->db->createCommand($SQL)->query(); 
                            }
                            
                            //ACTUALIZAMOS EL ESTADO DEL USUARIO EN LINEA EN LA PLATAFORMA
        			        //tiene que escribir 1 hp! 
        			        $SQL = "UPDATE `users_c` SET `connected`=1 WHERE id_usuario_c='".$_POST["LoginForm"]['username']."'";
                            $eConfirm =  Yii::app()->db->createCommand($SQL)->query();
                            
        			         //check login type: TECS, ADMIN, EDE
                            $type = $_POST["LoginForm"]['type'];
                            
                            //verificamos 
                            switch ($type) {
                			     case '1':
                			        $license = $this->CheckPendingTecs($_POST["LoginForm"]['username']);
                			        $link = 'index.php?r=Clients/default/index&license='.base64_encode($license).'';
                			        if($license == "" || $license = NULL){
                			            
                			            //INGRESO NO EXITOSO SACAMOS DE LÍNEA EL USUARIO
                    			        $SQL = "UPDATE `users_c` SET `connected`=0 WHERE id_usuario_c='".$_POST["LoginForm"]['username']."'";
                                        $eConfirm =  Yii::app()->db->createCommand($SQL)->query();
                			            
                			            // redireccionamos al login con mensaje de no hay licencia disponible
                			            $this->redirect('index.php?r=Clients/default/login&type=1&message=13');    
                			        }
                			        //si hay licencia tecs disponible para presentar, generamos el link de ingreso y dejamos pasar
                			        $this->redirect($link);
                			        
                			     break;
                			     case '2':
                			         //si un taker desea ingresar por la sección de administrador a un examen
                			       if($status[0]['id_perfil'] == 6){
                			           //INGRESO NO EXITOSO SACAMOS DE LÍNEA EL USUARIO
                    			        $SQL = "UPDATE `users_c` SET `connected`=0 WHERE id_usuario_c='".$_POST["LoginForm"]['username']."'";
                                        $eConfirm =  Yii::app()->db->createCommand($SQL)->query();
                			           // redireccionamos al login con mensaje de no hay licencia disponible
                			            $this->redirect('index.php?r=Clients/default/login&type=2&message=12');  
                			       }    
                			       $license = 0;
                			        switch ($_POST["flag"]) {
                			            case '0':
                			                $this->redirect('index.php?r=Clients/default/index');
                			            break;
                			            case '1':
                			                $this->redirect('index.php?r=Clients/MyAccount/orderHistory');
                			            break;
                			            default:
                			              //  var_dump("mandamos el usuario al inde exam default");die();
                			                $this->redirect('index.php?r=Clients/default/index');
                			            break;
                			        }
                			     break;
                			     case '3':
                			        
                			        $license = $this->CheckPendingEde($_POST["LoginForm"]['username']);
                			        $link = 'index.php?r=Clients/default/index&license='.base64_encode($license).'';
                			        if($license == "" || $license = NULL){
                			            //INGRESO NO EXITOSO SACAMOS DE LÍNEA EL USUARIO
                    			        $SQL = "UPDATE `users_c` SET `connected`=0 WHERE id_usuario_c='".$_POST["LoginForm"]['username']."'";
                                        $eConfirm =  Yii::app()->db->createCommand($SQL)->query();
                			            // redireccionamos al login con mensaje de no hay licencia disponible
                			            $this->redirect('index.php?r=Clients/default/login&type=3&message=14');    
                			        }
                			        $this->redirect($link);
                			     break;
                			     default:
                			         //INGRESO NO EXITOSO SACAMOS DE LÍNEA EL USUARIO
                    			        $SQL = "UPDATE `users_c` SET `connected`=0 WHERE id_usuario_c='".$_POST["LoginForm"]['username']."'";
                                        $eConfirm =  Yii::app()->db->createCommand($SQL)->query();
                			        // redireccionamos al login con mensaje de no hay licencia disponible
                			            $this->redirect('index.php?r=Clients/default/login&type=1&message=1');  
                			      break;
                			 }
        			        
        	       			
                		} else {
                		    switch ($type) {
                            	case '1':
                            	    $this->redirect('index.php?r=Clients/default/login&type=1&message=2');    
                            	break;
                            	case '2':
                            	    $this->redirect('index.php?r=Clients/default/login&type=2&message=2');  
                            	break;
                            	case '3':
                            	    $this->redirect('index.php?r=Clients/default/login&type=3&message=2');  
                            	break;
                            	default:
                            	    // redireccionamos al login con mensaje de no hay licencia disponible
                            		$this->redirect('index.php?r=Clients/default/login&type=1&message=2');  
                            	break;
                            }
                		} 
                    }
                    
                }
            }
		} 
        
        if(isset($_GET['hash_ede'])){
        
            $datos = base64_decode($_GET['hash_ede']);


            $arreglo = explode('&',$datos);
            
            //$model->agent = $arreglo[0];
            $model->username = $arreglo[1];
            $model->password = $arreglo[2];
            
            $_POST['LoginForm']['agent']= $arreglo[0];
            $_POST['LoginForm']['username']= $arreglo[1];
            $_POST['LoginForm']['password']= $arreglo[2];
            $license = explode('=',$arreglo[3]);
            $id_license = $license[1];
            //iniciamos la sesion 
            $session=new CHttpSession;
            $session->open();
            $session['user'] = $_POST["LoginForm"]['username'];
            
             
            if($model->validate() && $model->login())
                $link = 'index.php?r=Clients/default/index&license='.base64_encode($id_license).'';
                $this->redirect($link);
            }
    	
    	
    	// display the login by LOGIN TYPE (Tecs, admin, ede)
        $taker = (strpos(Yii::app()->request->requestUri,'taker')>0) ? 1:0;
        $this->render('login',array('model'=>$model,'taker'=>$taker));
    		  
		
		    
		}
		
		
	function CheckPendingTecs($id)
	{
	    $modUtil = new Utilidades();
	    $ids_string = $modUtil->getIdsTecs();
	    
		 //consultamos la ultima sesion iniciada por el usuario
        $SQL = "SELECT `id_usuario`, `id_licencias_usuario` FROM `licenses_user` INNER JOIN licenses_client ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente WHERE `id_usuario` = '".$id."' AND licenses_user.estado = 'A' AND licenses_client.id_licencia IN (".$ids_string.") ";
        $result =  Yii::app()->db->createCommand($SQL)->queryAll();
        
        return $result[0]["id_licencias_usuario"];
	}
	
	function CheckPendingEde($id)
	{
	    
	    $modUtil = new Utilidades();
	    $ids_string = $modUtil->getIdsEde();
	    
		 //consultamos la ultima sesion iniciada por el usuario
        $SQL = "SELECT `id_usuario`, `id_licencias_usuario` FROM `licenses_user` INNER JOIN licenses_client ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente WHERE `id_usuario` = '".$id."' AND licenses_user.estado = 'A' AND licenses_client.id_licencia IN (".$ids_string.") ";
        $result =  Yii::app()->db->createCommand($SQL)->queryAll();
        
        return $result[0]["id_licencias_usuario"];
	}
	

        public function actionLogout()
	{
	    
	    Yii::app()->user->logout();
                Yii::app()->user->setState('usuario',NULL);
                Yii::app()->user->setState('cliente',NULL);
                Yii::app()->user->setState('nombre_cliente',NULL);
                Yii::app()->user->setState('logo_cliente',NULL);
                Yii::app()->user->setState('nombres',NULL);
                Yii::app()->user->setState('apellidos',NULL);
                Yii::app()->user->setState('id_perfil',NULL);
                Yii::app()->user->setState('nombre_perfil',NULL);
        
		$this->redirect('/');
	}
	
	public function actionLogin2(){
	  
	    
	}
	
	public function actionChangePass(){
            $model=new ChangePass;
            if(isset($_POST['ChangePass'])){
                $datos=$_POST['ChangePass'];
                $modUtil = new Utilidades();
                if($modUtil->changePass($datos["username"],$datos["oldPass"],$datos["newPass"])){
                    $this->redirect('index.php?r=Clients/default/login&type=2&message=8');
                }else{
                    Yii::app()->user->setFlash('error','Username or password entered are incorrect.  You cannot use a previously used password. Please try again..');
                    $this->redirect(Yii::app()->createUrl('Clients/default/changePass'));  
                }
            }
            $this->render('changePass',array('model'=>$model));
    }
    
    public function actionEmailConfirm(){
            $model=new EmailConfirm;
            if(isset($_POST['EmailConfirm'])){
                $datos=$_POST['ChangePass'];
                $modUtil = new Utilidades();
                if($modUtil->changePass($datos["username"],$datos["oldPass"],$datos["newPass"])){
                    $this->redirect('index.php?r=Clients/default/login&type=2&message=8');
                }else{
                    Yii::app()->user->setFlash('error','Username or password entered are incorrect.  You cannot use a previously used password. Please try again..');
                    $this->redirect(Yii::app()->createUrl('Clients/default/changePass'));  
                }
            }
            $this->render('changePass',array('model'=>$model));
        }

}
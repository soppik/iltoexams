<?php

/**
 * This is the model class for table "users_c".
 *
 * The followings are the available columns in table 'users_c':
 * @property string $id_usuario_c
 * @property string $nombres
 * @property string $apellidos
 * @property string $email
 * @property string $id_cliente
 * @property string $ultimo_acceso
 * @property integer $id_perfil
 * @property string $numero_id
 * @property string $ruta_foto
 * @property string $estado
 *
 * The followings are the available model relations:
 * @property LicensesUser[] $licensesUsers
 * @property Clients $idCliente
 * @property Profiles $idPerfil
 */
class UsersD extends UsersC
{
    //Eliminar el Default para que puedan ver todos los usuarios sin importar el cliente, esto se hace para los Reportes de Fraude
    public function defaultScope() {
        return array( 
                    'condition'=>"id_cliente>'0'",                    
                );
    }
    
}
<?php

/**
 * This is the model class for table "fraud_report".
 *
 * The followings are the available columns in table 'fraud_report':
 * @property integer $id
 * @property string $document
 * @property string $first_name
 * @property string $sure_name
 * @property string $email
 * @property string $id_client
 * @property string $picture
 * @property string $id_user_report
 * @property string $name_user_report
 * @property string $report_date
 * @property string $comments
 * @property integer $exam
 *
 * The followings are the available model relations:
 * @property Licenses $exam0
 * @property Clients $idClient
 * @property UsersC $document0
 */
class FraudReport extends CActiveRecord
{
    public $country;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'fraud_report';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('document, first_name, sure_name, email, id_user_report, name_user_report, report_date, comments, exam, type', 'required'),
			array('exam, type', 'numerical', 'integerOnly'=>true),
			array('document, id_client, id_user_report', 'length', 'max'=>20),
			array('first_name, sure_name', 'length', 'max'=>100),
			array('email, name_user_report', 'length', 'max'=>200),
			array('picture', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, document, type, country, first_name, sure_name, email, id_client, picture, id_user_report, name_user_report, report_date, comments, exam', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'exam0' => array(self::BELONGS_TO, 'Licenses', 'exam'),
			'idClient' => array(self::BELONGS_TO, 'Clients', 'id_client'),
			'document0' => array(self::BELONGS_TO, 'UsersC', 'document'),
			'document1' => array(self::BELONGS_TO, 'UsersD', 'document'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'document' => 'Taker',
			'first_name' => 'First Name',
			'sure_name' => 'Surname',
			'email' => 'Email',
			'id_client' => 'Id Client',
			'picture' => 'Picture',
			'id_user_report' => 'Id User Report',
			'name_user_report' => 'Name User Report',
			'report_date' => 'Report Date',
			'comments' => 'Comments',
			'exam' => 'Exam',
                    'type'=>'Fraud Type',
                    'country'=>'Country',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($buscar = NULL)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

            if(is_null($buscar)){
		$criteria=new CDbCriteria;
                $criteria->with = "document1";
		$criteria->compare('id',$this->id);
		$criteria->compare('document',$this->document,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('sure_name',$this->sure_name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('id_client',$this->id_client,true);
		$criteria->compare('picture',$this->picture,true);
		$criteria->compare('id_user_report',$this->id_user_report,true);
		$criteria->compare('name_user_report',$this->name_user_report,true);
		$criteria->compare('report_date',$this->report_date,true);
		$criteria->compare('comments',$this->comments,true);
		$criteria->compare('exam',$this->exam);
		$criteria->compare('type',$this->type);
                $criteria->compare('document1.id_pais',$this->country);

            } else {
                $criteria = new CDbCriteria(
                                array(
                                    'condition'=>" document like :busca or first_name like :busca or sure_name like :busca",
                                    'params' => array(':busca'=> "%$buscar%")
                                ));                
            }
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                'pagination'=>array('pageSize'=>50,),

            ));
	}

        public function nombrePais(){
            $modUsers= UsersC::model()->resetScope()->findByAttributes(array('id_usuario_c'=>$this->document));
            echo $modUsers->idPais->nombre;
        }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FraudReport the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function nombreTipo(){
            $modUtil = new Utilidades();
            $fraudes = $modUtil->arregloTiposFraude();
            echo $fraudes[$this->type];
            
        }
}

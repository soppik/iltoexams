<?php

/**
 * This is the model class for table "users_c".
 *
 * The followings are the available columns in table 'users_c':
 * @property string $id_usuario_c
 * @property string $nombres
 * @property string $apellidos
 * @property string $email
 * @property string $id_cliente
 * @property string $ultimo_acceso
 * @property integer $id_perfil
 * @property string $numero_id
 * @property string $ruta_foto
 * @property string $estado
 *
 * The followings are the available model relations:
 * @property LicensesUser[] $licensesUsers
 * @property Clients $idCliente
 * @property Profiles $idPerfil
 */
class UsersC extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
    
    public $clave2;
    public $calificacion;
    public $nivel;
    public $license;
	public function tableName()
	{
		return 'users_c';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombres, apellidos, email, id_cliente, id_perfil, numero_id, estado,id_pais, id_ciudad', 'required'),
			array('id_perfil, email_certificate', 'numerical', 'integerOnly'=>true),
			array('id_grupo_cliente, telefono', 'length', 'allowEmpty'=>true),
			array('id_usuario_c, id_cliente, numero_id', 'length', 'max'=>20),
			array('id_usuario_c', 'unique', 'message'=>'This user name already exists...'),
			array('nombres, apellidos', 'length', 'max'=>100),
			array('email', 'email', 'message'=>'This email address is not valid...'), //removimos la condición de que no podía repetirse el email
			array('ruta_foto', 'length', 'max'=>250), 
			array('estado', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_usuario_c, calificacion, nivel, id_grupo_cliente, nombres, apellidos, email, id_cliente, ultimo_acceso, id_perfil, numero_id, ruta_foto, estado, id_pais, id_ciudad, telefono', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'licensesUsers' => array(self::HAS_MANY, 'LicensesUser', 'id_usuario'),
			'idCliente' => array(self::BELONGS_TO, 'Clients', 'id_cliente'),
			'idPerfil' => array(self::BELONGS_TO, 'Profiles', 'id_perfil'),
                        'idGrupoCliente' => array(self::BELONGS_TO, 'GroupsUsersClients', 'id_grupo_cliente'),
                        'idPais' => array(self::BELONGS_TO, 'Countries', 'id_pais'),
                        'idCiudad' => array(self::BELONGS_TO, 'Cities', 'id_ciudad'),                    );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_usuario_c' => 'User ID (login)',
			'nombres' => 'First Name',
			'apellidos' => 'Surname',
			'email' => 'Email',
			'id_cliente' => 'Client',
			'ultimo_acceso' => 'Last Access',
			'id_perfil' => 'Profile',
                    'clave'=>'Password',
			'clave2' => 'Verify Password',
			'numero_id' => 'Id Number',
			'id_grupo_cliente' => 'User Group',
			'ruta_foto' => 'Picture',
			'estado' => 'Status',
                    'id_pais'=>'Country',
                    'id_ciudad'=>'City',
                    'calificacion'=>'Score',
                    'nivel'=>'Level',
                    'email_certificate'=> 'Email Certificate',
                    'telefono'=> 'contacto',
                    
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('id_usuario_c',$this->id_usuario_c,true);
		$criteria->compare('nombres',$this->nombres,true);
		$criteria->compare('apellidos',$this->apellidos,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('id_cliente',$this->id_cliente,true);
		$criteria->compare('id_pais',$this->id_pais);
		$criteria->compare('ultimo_acceso',$this->ultimo_acceso,true);
		$criteria->compare('id_perfil',$this->id_perfil);
		$criteria->compare('numero_id',$this->numero_id,true);
		$criteria->compare('ruta_foto',$this->ruta_foto,true);
		$criteria->compare('id_grupo_cliente',$this->id_grupo_cliente,true);
		$criteria->compare('email_certificate',$this->email_certificate,true);
		$criteria->compare('estado',$this->estado,true);
		$criteria->compare('telefono',$this->telefono,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'pagination'=>array('pageSize'=>50,),

		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UsersC the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

        public function defaultScope()
    {
            if(Yii::app()->user->isGuest){
                return array();
            } else {
                return array( 
                    'condition'=>"id_cliente='".Yii::app()->user->getState('cliente')."' and id_perfil >= ".Yii::app()->user->getState('id_perfil'),                    
                );
            }
    }
    public function scopes(){
        return array(
            'tutores'=>array(
                'condition'=>'id_perfil <= 4',
            ),
            'takers'=> array(
                'condition'=>'id_perfil = 6'),
        );
    }

    public function getNombresapellidos(){
        return $this->nombres." ".$this->apellidos;
    }
    public function getNombresapellidosID(){
        return $this->id_usuario_c." - ".$this->nombres." ".$this->apellidos;
    }
    public function traeCalifica(){
        $modLicUsu = LicensesUser::model()->findByAttributes(array('id_usuario'=>$this->id_usuario_c,'estado'=>'F'));
        $retorno['calificacion'] = $modLicUsu->calificacion;
        $retorno['nivel']=$modLicUsu->nivel;
        $retorno['licencia'] = $modLicUsu->idLicenciaCliente->idLicencia->nombre;
        return $retorno;
    }
    public function seElimina(){
        $perfilUs = Yii::app()->user->getState('id_perfil');
        if($this->id_perfil <= $perfilUs){
            return false;
        } else {
            if($this->id_perfil == 6){
                $totExamsUser = ExamsUser::model()->countByAttributes(array('tutor'=>$this->id_usuario_c));
                $totLicenciasUsuario = LicensesUser::model()->countByAttributes(array('id_usuario'=>$this->id_usuario_c));
                if($totLicenciasUsuario>0 || $totExamsUser>0 ){
                    return false;
                } else {
                    return true;
                }
            } else {
                if($this->id_perfil > 2 &&  $this->id_perfil<6 ){ return true;} else { return false;}
            }
        }
    }
    public function traeEstado(){ 
        $arregloEstados= array("A"=>"Enabled","P"=>"Pending","R"=>"Registered","I"=>"Disabled");
        return $arregloEstados[$this->estado];
    }
    
}
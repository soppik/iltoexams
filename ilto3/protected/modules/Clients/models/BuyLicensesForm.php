<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class BuyLicensesForm extends CFormModel
{
	public $client;
	public $amount;
	public $licensetype;
	public $value;


	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// username and password are required
			array('amount, licensetype', 'required'),
			// rememberMe needs to be a boolean
			array('amount, licensetype', 'numerical','integerOnly'=>true),
			// password needs to be authenticated
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
                    'amount'=>'Amount',
                    'licensetype'=>'Type',
		);
	}


}

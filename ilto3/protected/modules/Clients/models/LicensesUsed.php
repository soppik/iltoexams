<?php

class LicensesUsed extends LicensesUser{
    public function defaultScope() {
        return array();
    }

    	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                $criteria->with = array("idUsuario", "idLicenciaCliente");
    		$criteria->compare('t.id_licencias_usuario',$this->id_licencias_usuario);
		$criteria->compare('t.id_licencia_cliente',$this->id_licencia_cliente);
		$criteria->compare('t.id_usuario',$this->id_usuario,true);
		$criteria->compare('t.fecha_asignacion',$this->fecha_asignacion,true);
		$criteria->compare('t.estado',$this->estado,true);
		$criteria->compare('t.id_usuario',$this->usuario_id,true);
		$criteria->compare('t.nivel',$this->nivel,false);
		$criteria->compare('idUsuario.nombres',$this->usuario_nombre,true);
		$criteria->compare('idUsuario.id_grupo_cliente',$this->usuario_grupo,true);
		$criteria->compare('idUsuario.apellidos',$this->usuario_apellido,true);
		$criteria->compare('idLicenciaCliente.id_cliente',$this->elCliente,true);
		$criteria->compare('idLicenciaCliente.id_licencia',$this->laLicencia,true);
                

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'pagination'=>array('pageSize'=>50,),
                    'sort'=>array(
                            'attributes'=>array(
                                'usuario_id'=>array(
                                    'asc'=>'id_usuario',
                                    'desc'=>'id_usuario DESC',
                                ),
                                'usuario_nombre'=>array(
                                    'asc'=>'idUsuario.nombres',
                                    'desc'=>'idUsuario.nombres DESC',
                                ),
                                'usuario_apellido'=>array(
                                    'asc'=>'idUsuario.apellidos',
                                    'desc'=>'idUsuario.apellidos DESC',
                                ),
                                '*',
                            ),
                        ),
		));
	}

    
    
    }
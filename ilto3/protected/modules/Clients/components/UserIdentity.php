<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
            
                $this->username = $_POST['LoginForm']['username'];
                $this->password = $_POST['LoginForm']['password'];
                
               
                
                $users = UsersC::model()->findByAttributes(array('id_usuario_c'=>$this->username));
                    
               
                
                if($users !== NULL){
                    if(!isset($this->username)){
                            $this->errorCode=self::ERROR_USERNAME_INVALID;
                    }
                    elseif($users->clave!==md5($this->password)){
                            $this->errorCode=self::ERROR_PASSWORD_INVALID;
                    }
                    else{
                            Yii::app()->user->setState('numero_id', $users->numero_id);
                            Yii::app()->user->setState('usuario', $this->username);
                            Yii::app()->user->setState('nombres_apellidos', $users->nombres);
                            Yii::app()->user->setState('cliente', $users->id_cliente);
                            Yii::app()->user->setState('nombre_cliente', $users->idCliente->nombre_rsocial);
                            Yii::app()->user->setState('logo_cliente', $users->idCliente->ruta_logo);
                            Yii::app()->user->setState('logo_en_certificado', $users->idCliente->logo_en_certificado);
                            Yii::app()->user->setState('ruta_foto', $users->ruta_foto);
                            Yii::app()->user->setState('nombres', $users->nombres);
                            Yii::app()->user->setState('isDemo', $users->isDemo);
                            
                            Yii::app()->user->setState('email', $users->email);
                            
                            Yii::app()->user->setState('apellidos', $users->apellidos);
                            Yii::app()->user->setState('id_perfil', $users->id_perfil);
                            Yii::app()->user->setState('nombre_perfil', Profiles::model()->findByPk($users->id_perfil)->nombre);
                            $users->ultimo_acceso = new CDbExpression('NOW()');
                            $users->save(true,array('ultimo_acceso'));
                            $this->errorCode=self::ERROR_NONE;
                    }
                } else {
                    $this->errorCode=self::ERROR_USERNAME_INVALID;
                }
                return !$this->errorCode;
	}
}
<?php

class ExamsLicenseController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','moveUp','moveDown','addAll','delete'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        public function actionMoveUp($id){
            $modelo = $this->loadModel($id);
            $licencia = $modelo->id_licencia;
            $modelo = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>$licencia),array('order'=>'orden ASC'));
            $idAnt=NULL;
            foreach($modelo as $registro){
                if($registro->id_examenes_licencia == $id){
                    if(!is_null($idAnt)){//Solo Si no es el primer registro
                        $modTmp = ExamsLicense::model()->findByPk($idAnt);
                        $ordenAnterior = $modTmp->orden;
                        $txt="Encontre el id anterior $idAnt con el orden $ordenAnterior y le voy a asignar ".$registro->orden;
                        $modTmp->orden = $registro->orden;
                        $modTmp->save();
                        $registro->orden = $ordenAnterior;
                        $registro->save();
                    }
                }
                $idAnt = $registro->id_examenes_licencia;
            }
            return true;
        }
        public function actionMoveDown($id){
            $modelo = $this->loadModel($id);
            $licencia = $modelo->id_licencia;
            $modelo = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>$licencia),array('order'=>'orden DESC'));
            $idAnt=NULL;
            foreach($modelo as $registro){
                if($registro->id_examenes_licencia == $id){
                    if(!is_null($idAnt)){//Solo Si no es el primer registro
                        $modTmp = ExamsLicense::model()->findByPk($idAnt);
                        $ordenAnterior = $modTmp->orden;
                        $txt="Encontre el id anterior $idAnt con el orden $ordenAnterior y le voy a asignar ".$registro->orden;
                        $modTmp->orden = $registro->orden;
                        $modTmp->save();
                        $registro->orden = $ordenAnterior;
                        $registro->save();
                    }
                }
                $idAnt = $registro->id_examenes_licencia;
            }
            return true;
        }

	public function actionAddAll($license)
	{
            foreach($_POST['exams-grid_c0'] as $registro=>$valor){
                $modelo= new ExamsLicense();
                $modelo->id_licencia = $license;
                $modelo->id_examen = $valor;
                if(!$modelo->save()){print_r($modelo->getErrors());}
                
            }
            //Regenerar el orden de las preguntas
            ExamsLicense::model()->orden($license);
            $this->redirect(array('examsLicense/admin', 'license'=>$license));
	}

        /**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new ExamsLicense;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ExamsLicense']))
		{
			$model->attributes=$_POST['ExamsLicense'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ExamsLicense']))
		{
			$model->attributes=$_POST['ExamsLicense'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id,$license)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin','license'=>$license));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('ExamsLicense');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin($license)
	{
		$model=new ExamsLicense('search');
		$model->unsetAttributes();  // clear any default values
                $model->id_licencia=$license;
		if(isset($_GET['ExamsLicense']))
			$model->attributes=$_GET['ExamsLicense'];

		$this->render('admin',array(
			'model'=>$model,'license'=>$license,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ExamsLicense the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ExamsLicense::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ExamsLicense $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='exams-license-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

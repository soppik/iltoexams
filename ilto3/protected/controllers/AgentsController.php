<?php

class AgentsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('create','index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('update','admin','delete'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Agents;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Agents']))
		{
			$model->attributes=$_POST['Agents'];
                        $ruta_logo=CUploadedFile::getInstance($model,'logo');
                        $ahora = time();
			if($model->save()){
                            //Crear el cliente
                            $modUtil = new Utilidades();
                            $nombreImagen="";
                            $logo=0;
                            if(!is_null($ruta_logo)){
                                $extension = ".".$ruta_logo->getExtensionName();
                                $nombreImagen = $model->id_legal."_".$ahora.$extension;
                                $model->logo=$nombreImagen;
                                $model->save();
                                $ruta_logo->saveAs("images/clients/".$nombreImagen);
                                $modUtil->generaThumb("images/clients/",$nombreImagen);
                                $logo=1;
                            }
                            $modCliente = new Clients;
                            $modCliente->id_cliente = $model->id_legal;
                            $modCliente->estado="P";
                            $modCliente->logo_en_certificado=$logo;
                            $modCliente->contacto = $model->first_name." ".$model->last_name;
                            $modCliente->direccion = $model->address_1." ".$model->address_2." ".$model->address_3;
                            $modCliente->id_pais = $model->country;
                            $modCliente->id_ciudad = $model->city;
                            $modCliente->nombre_rsocial = $model->institution_name;
                            $modCliente->telefono = $model->phone;
                            $modCliente->nit_id_legal=$model->id_legal;
                            $modCliente->ruta_logo = $nombreImagen;
                            $idCliente = $model->id_legal;
                            if($modCliente->save()){
                                if($model->main_decision == 1){
                                //Crear El usuario TOAA
                                    $modUsuarios = new UsersC;
                                    $modUsuarios->apellidos = $model->last_name;
                                    $modUsuarios->nombres=$model->first_name;
                                    $modUsuarios->email = $model->email;
                                    $modUsuarios->id_cliente = $idCliente;
                                    $modUsuarios->estado = "P";
                                    $modUsuarios->id_usuario_c = $model->user_id_number;
                                    $modUsuarios->numero_id = $model->user_id_number;
                                    $modUsuarios->clave = md5($model->user_id_number);
                                    $modUsuarios->clave2 = md5($model->user_id_number);
                                    $modUsuarios->id_pais = $model->country;
                                    $modUsuarios->id_ciudad = $model->city;
                                    $modUsuarios->id_perfil = 2;
                                    $modUsuarios->save();
                                } else {
                                    if(!is_null($model->user_id_number2)){
                                        if(($model->email_1 != $model->email) && ($model->email_2 != $model->email_1)){
                                            $modUsuarios = new UsersC;
                                            $modUsuarios->apellidos = $model->last_name_1;
                                            $modUsuarios->nombres=$model->first_name_1;
                                            $modUsuarios->email = $model->email_1;
                                            $modUsuarios->id_cliente = $idCliente;
                                            $modUsuarios->estado = "P";
                                            $modUsuarios->id_perfil = 2;
                                            $modUsuarios->id_usuario_c = $model->user_id_number2;
                                            $modUsuarios->clave = md5($model->user_id_number2);
                                            $modUsuarios->clave2 = md5($model->user_id_number2);
                                            $modUsuarios->save();
                                        }
                                    }
                                }
                                if(!is_null($model->user_id_number3)){
                                    
                                    if(($model->email_2 != $model->email) && ($model->email_2 != $model->email_1)){
                                        $modUsuarios = new UsersC;
                                        $modUsuarios->apellidos = $model->last_name_2;
                                        $modUsuarios->nombres=$model->first_name_2;
                                        $modUsuarios->email = $model->email_2;
                                        $modUsuarios->id_cliente = $idCliente;
                                        $modUsuarios->estado = "P";
                                        $modUsuarios->id_perfil = 3;
                                        $modUsuarios->id_usuario_c = $model->user_id_number3;
                                        $modUsuarios->clave = md5($model->user_id_number2);
                                        $modUsuarios->clave2 = md5($model->user_id_number2);
                                        $modUsuarios->save();
                                    }
                                }
                            } else {
                                print_r($modCliente->getErrors());
                                die();
                            }
			$this->redirect(array('admin'));
                        }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Agents']))
		{
			$model->attributes=$_POST['Agents'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Agents');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Agents('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Agents']))
			$model->attributes=$_GET['Agents'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Agents the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Agents::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Agents $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='agents-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

        public function actionPrint_validation($v){
            $modUtil = new Utilidades();
            $tabla = $modUtil->generaValidacion($v);
            $mPDF1 = Yii::app()->ePdf->mpdf("","Letter");
            $mPDF1->WriteHTML($tabla);
            $time = time();
            $nombre = "validation_".$time.".pdf";
            $mPDF1->Output();        }
/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

 	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

	    		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		$session=new CHttpSession;
		$session->open();
		
		//ACTUALIZAMOS EL ESTADO DEL USUARIO A OFFLINE (0)
		$SQL = "UPDATE `users_c` SET `connected`=0 WHERE id_usuario_c='".$_GET['id']."'";
        $eConfirm =  Yii::app()->db->createCommand($SQL)->query();
        
        //consultamos la ultima sesion iniciada por el usuario
		$SQL = "SELECT id_perfil FROM `users_c` WHERE `id_usuario_c` = '".$_GET['id']."'";
        $userType =  Yii::app()->db->createCommand($SQL)->queryAll();
        
        
        // agregamos al log del usuario
		if($userType[0]['id_perfil']!=6){
        	$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`) VALUES (NULL, 3, '".$_GET['id']."', NOW(), '', '', 'logout and destroy session')";
            $log= Yii::app()->db->createCommand($SQL)->query(); 
        }
		
		$session->clear();
		$session->destroy();
		
		Yii::app()->user->logout();
                Yii::app()->user->setState('usuario',NULL);
                Yii::app()->user->setState('cliente',NULL);
                Yii::app()->user->setState('nombre_cliente',NULL);
                Yii::app()->user->setState('logo_cliente',NULL);
                Yii::app()->user->setState('nombres',NULL);
                Yii::app()->user->setState('apellidos',NULL);
                Yii::app()->user->setState('id_perfil',NULL);
                Yii::app()->user->setState('nombre_perfil',NULL);
                
        
		$this->redirect('/index.php');
	}
}
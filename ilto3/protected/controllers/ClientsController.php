<?php

class ClientsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','delete','delete2'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{

		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		//defined('YII_DEBUG_SHOW_PROFILER') or define('YII_DEBUG_SHOW_PROFILER',true);
	//	defined('YII_DEBUG') or define('YII_DEBUG',true );
		$model=new Clients;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Clients']))
		{
			$model->attributes=$_POST['Clients'];
                        $ruta_logo=CUploadedFile::getInstance($model,'ruta_logo');
                        $ahora = time();
                        $model->estado="A";
                        $model->id_cliente = $model->nit_id_legal;
                        if($_POST['Clients']['id_distributor']=='-1'){
                            $model->id_distributor = NULL;
                        }
                        
			if($model->save()){
                            $modUtil = new Utilidades();
                            if(!is_null($ruta_logo)){
                                $extension = ".".$ruta_logo->getExtensionName();
                                $nombreImagen = $model->id_cliente."_".$ahora.$extension;
                                $model->ruta_logo=$nombreImagen;
                                $model->save();
                                
                    
                                
                                $ruta_logo->saveAs("images/clients/".$nombreImagen);
                                $modUtil->generaThumb("images/clients/",$nombreImagen);
                            }
             
						
                            $this->redirect(array('admin'));
                        }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	//	defined('YII_DEBUG') or define('YII_DEBUG',false );
		
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['Clients']))
		{
			
            $modAnt = $this->loadModel($id);
			$model->attributes=$_POST['Clients'];
            $ruta_imagen=CUploadedFile::getInstance($model,'ruta_logo');
            $ahora = time();
				$modUtil = new Utilidades();
                if(!is_null($ruta_imagen)){
                //Si tenia una imagen, eliminarla
                if(isset($_POST['ruta_imagen_actual'])){
                	$modUtil->eliminarThumbs("images/clients/",$_POST['ruta_imagen_actual']);
                }
                //Nombre de la nueva imagen
                $extension = ".".$ruta_imagen->getExtensionName();
                $nombreImagen = $model->id_cliente."_".$ahora.$extension;
                $model->ruta_logo=$nombreImagen;
               // $model->save();
                $ruta_imagen->saveAs("images/clients/".$nombreImagen);
                $modUtil->generaThumb("images/clients/",$nombreImagen);
                } elseif(strlen($_POST['ruta_imagen_actual'])>0){ //Si existia una imagen previa
                	if(isset($_POST['delete_imagen'])){
                		$model->ruta_logo=NULL;
                        $model->save(true,array('ruta_logo'));
                		$modUtil->eliminarThumbs("images/clients/",$_POST['ruta_imagen_actual']);
                    }
                }
                $fotoSql = '`ruta_logo`="'.$nombreImagen.'",';    
                if($ruta_imagen==NULL) {
                        $fotoSql = "";
                }
                
            	$idDis = '`id_distributor`="'.$_POST['Clients']['id_distributor'].'",';
            	                //var_dump($_POST['Clients']['id_distributor']);die();
            	                
            	//usuario tooa
            	$modUsuario = UsersC::model()->findByAttributes(array('id_cliente'=>$_POST['Clients']['nit_id_legal'], 'id_perfil'=>2 ));
                          
                if($_POST['Clients']['is_distributor']=='2'){
	            	 $SQL = "UPDATE `clients` SET `nombre_rsocial`='".$_POST['Clients']['nombre_rsocial']."', `direccion`='".$_POST['Clients']['direccion']."',
	                `id_country`='".$_POST['Clients']['id_pais']."', `nit_id_legal`='".$_POST['Clients']['nit_id_legal']."',
	                `telefono`='".$_POST['Clients']['telefono']."', `contacto`='".$_POST['Clients']['contacto']."', ".$fotoSql." `estado`='".$_POST['Clients']['estado']."', `logo_en_certificado`='".$_POST['Clients']['logo_en_certificado']."', 
	                `is_distributor`='".$_POST['Clients']['is_distributor']."', `id_distributor`=NULL, `examen_asistido`='".$_POST['Clients']['examen_asistido']."' WHERE `id_cliente`='".$id."'";
	                $list= Yii::app()->db->createCommand($SQL)->query();
	                
	                
	                $SQL = "UPDATE `users_c` SET `isDemo`='".$_POST['Clients']['demo']."' WHERE `id_usuario_c`='".$modUsuario->id_usuario_c."'";
	                $list= Yii::app()->db->createCommand($SQL)->query();
	                
	                $this->redirect(array('admin'));
            	}else{
            		if($idDis=-1){
            			$idDis = '`id_distributor`= NULL,';
            		}
            		 $SQL = "UPDATE `clients` SET `nombre_rsocial`='".$_POST['Clients']['nombre_rsocial']."', `direccion`='".$_POST['Clients']['direccion']."',
	                `id_country`='".$_POST['Clients']['id_pais']."', `nit_id_legal`='".$_POST['Clients']['nit_id_legal']."',
	                `telefono`='".$_POST['Clients']['telefono']."', `contacto`='".$_POST['Clients']['contacto']."', ".$fotoSql." `estado`='".$_POST['Clients']['estado']."', `logo_en_certificado`='".$_POST['Clients']['logo_en_certificado']."', 
	                `is_distributor`='".$_POST['Clients']['is_distributor']."', ".$idDis." `examen_asistido`='".$_POST['Clients']['examen_asistido']."' WHERE `id_cliente`='".$id."'";
	                $list= Yii::app()->db->createCommand($SQL)->query();
	                
	                $SQL = "UPDATE `users_c` SET `isDemo`='".$_POST['Clients']['demo']."' WHERE `id_usuario_c`='".$modUsuario->id_usuario_c."'";
	                $list= Yii::app()->db->createCommand($SQL)->query();
	                $this->redirect(array('admin'));
            	}
                
               
				
        	}       
		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		
		//get licenses users for this client
		$SQL = "SELECT * FROM `licenses_client` WHERE `id_cliente` = '".$id."'";
	    $list= Yii::app()->db->createCommand($SQL)->queryAll();
	    //check is client is demo
	    $SQL = "SELECT isDemo FROM `users_c` WHERE `id_cliente` = '".$id."' AND id_perfil = 2";
	    $isDemo= Yii::app()->db->createCommand($SQL)->queryAll();
	    
	    //check client status
	    $SQL = "SELECT estado FROM `clients` WHERE `id_cliente` = '".$id."'";
	    $stateClient= Yii::app()->db->createCommand($SQL)->queryAll();
	    $status = $stateClient[0]["estado"];
	    //verificamos si es demo y tiene licencias demo
	    
	    $clientDemo = 0;
	    if($isDemo[0]["isDemo"]){
	    	$clientDemo = 1;
	    }
	    
	    //bandera de 2 años de antiguedad
	    $flag2years = 0; 
	    
	    //reviso si tiene licencias asignadas
	    if(!empty($list)){
	    	$licDemo = 0;
	    	$deleteConfirm = 0;
	    	
	    	// revisamos si todas las licencias son DEMO, si lo son borramos todo
	    	foreach ($list as $licGroup) {
		    	$idLic = $licGroup["id_licencias_cliente"];
		    	if($licGroup["demo"] == 1){
		    		$licDemo = 1;
		    	}else{
		    		$licDemo = 0;
		    	}
		    	
					
	    	}
	    	if($licDemo == 1){
	    		$this->deleteAll($id);
	    		
	    	}else{
	    		echo "no puedo borrar";die();
	    		foreach ($list as $licGroup) {
		    		
			    	$idLic = $licGroup["id_licencias_cliente"];
			    	if($licGroup["demo"] == 1){
			    		$licDemo = 1;
			    	}else{
			    		$licDemo = 0;
			    	}
			    	
			    	// reviso las fecha de asignacion y comparo con hoy
			    	$now = new Datetime("now");
			        $dateLic = new Datetime($licGroup["fecha_asignacion"]);
			        $diff=date_diff($dateLic,$now);
			        $numDays = $diff->format("%R%a");
			        var_dump($licDemo);die();
			        if($licDemo == 1){
			        	
			        }
			        if($numDays >= 730 ){
			        	$deleteConfirm = 1;
			        }
			        if($deleteConfirm == 1 && $clientDemo == 1){
			        	if($status == 'I'){
			        		$mensaje = 'An Email was sent to '.$model->email.' with the Login Information.';
	                        Yii::app()->user->setFlash('DENIED','The Client is DEMO, there is a confirmation licenses for delete but the Clients is still enabled.');
	                        $this->redirect(array('admin')); 
			        	}
			        }
			        die();
			    	$SQL = "SELECT * FROM `licenses_user` WHERE `id_licencia_cliente` LIKE '".$idLic."'";
			    	$listLic= Yii::app()->db->createCommand($SQL)->queryAll();
			    	
			    	foreach ($listLic as $idLicTaker) {
			    		$idLicExam = $idLicTaker["id_licencias_usuario"];
			    		//cambiar por delete para cada examen de cada licencia
			    		$SQL = "DELETE FROM `exams_user` WHERE `id_licencia_usuario` = '".$idLicExam."'";
			    		//$listExam= Yii::app()->db->createCommand($SQL)->query();
			    		
			    	}
			    	
			    	// cambiar por delete para cada licencia de usuario que tenga ese id
					$SQL = "DELETE FROM `licenses_user` WHERE `id_licencia_cliente` LIKE '".$idLic."'";
					//$listLic= Yii::app()->db->createCommand($SQL)->query();
						
		    	}
	    	}
	    	
	    
	    }else{
	    	
	    	$this->deleteAll($id);
	    
	    }
	    
	    	
	    //$this->loadModel($id)->delete();
		//die();
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	
	function deleteAll($client){
		
		//Delete client licenses
		$SQL = "DELETE FROM `licenses_client` WHERE `id_cliente` LIKE '".$client."'";
		$list= Yii::app()->db->createCommand($SQL)->query();
				    
		//Delete client class rooms
		$SQL = "DELETE FROM `class_rooms` WHERE `id_cliente` LIKE '".$client."'";
		$list= Yii::app()->db->createCommand($SQL)->query();
		
		//Borrar usuarios
		$SQL = "UPDATE `users_c` SET `id_grupo_cliente` = '252' WHERE `id_cliente` = '".$client."'";
		$list= Yii::app()->db->createCommand($SQL)->query();
				
		// Borrar grupos
		$SQL = "DELETE FROM `groups_users_clients` WHERE `id_cliente` LIKE '".$client."'";
		$list= Yii::app()->db->createCommand($SQL)->query();
		
		//Borrar usuarios
		$SQL = "DELETE FROM `users_c` WHERE `id_cliente` LIKE '".$client."'";
		$list= Yii::app()->db->createCommand($SQL)->query();
		
		
				    	
		
				    	
		//Borrar cliente
		$SQL = "DELETE FROM `clients` WHERE `id_cliente` LIKE '".$client."'";
		$list= Yii::app()->db->createCommand($SQL)->query();
	}
	

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Clients');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Clients('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Clients']))
			$model->attributes=$_GET['Clients'];
			
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Clients the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Clients::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Clients $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='clients-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

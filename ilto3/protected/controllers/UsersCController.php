<?php

class UsersCController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('areandina', 'areandina_new', 'areandina_admin', 'index','view', 'licences', 'udelvalle', 'ucorpas', 'ucorpas', 'uvpmx', 'utp', 'uamEde', 'uanEde', 'ftpEde', 'uniminuto', 'demo_tecs'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','citiesCountry','update','email','admin','delete', 'licences', 'certificate', 'email_certificate'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	/*
        public function actionAreandina(){
            
            
            $modUsu = UsersC::model()->findByAttributes(array('email'=>$_POST['email']));
            if(count($modUsu)>0){
                $hash_ede = base64_encode('8605173021&'.$modUsu->id_usuario_c.'&1234');
                $this->redirect('/ilto3/index.php?r=Clients/default/login&hash_ede='.$hash_ede);
            } else {
                
                
                //Registro en el area andina
                $modlicencia_cliente = LicensesClient::model()->disponibles()->findByAttributes(array('id_licencia'=>16,'estado'=>'A', 'id_cliente'=>'8605173021'));
                //Si hay licencias disponibles continuar
                $sql='SELECT SUM(cantidad - utilizados) as total FROM `licenses_client` WHERE `id_cliente` LIKE "8605173021" AND id_licencia = 16';
                $list= Yii::app()->db->createCommand($sql)->query();
                
                if($modlicencia_cliente){
                    $modUtil = new Utilidades();
                    $modArea = new EdeAreandina();
                    $modArea->email = $_POST['email'];
                    $modArea->firstName = $_POST['firstName'];
                    $modArea->lastName = $_POST['lastName'];
                  
                    if($modArea->save()){
                        //Registro unicamente en usuarios
                        $modUsu = new UsersC;
                        $modUsu->id_cliente = '8605173021';
                        $modUsu->email = $_POST['email'];
                        $modUsu->nombres = $_POST['firstName'];
                        $modUsu->apellidos = $_POST['lastName'];
                        $modUsu->id_grupo_cliente = $_POST['group'];
                        $modUsu->id_perfil=6;
                        $modUsu->estado='A';
                        $modUsu->numero_id='ede_'.$modArea->id;
                        $modUsu->id_usuario_c ='ede_'.$modArea->id;
                        $modUsu->clave = md5('1234');
                        $modUsu->clave2 = '1234';
                        $modUsu->id_pais=3;
                        $modUsu->id_ciudad=2;
                        //$modUsu->save();
                        
                        $SQL = "INSERT INTO `users_c` (`id_usuario_c`, `nombres`, `apellidos`, `email`, `id_cliente`, `ultimo_acceso`, `id_perfil`, `numero_id`, `estado`, `clave`, `clave2`, `fecha_creado`, `id_grupo_cliente`, `id_pais`, `id_country`, `id_ciudad`, `email_certificate`, `isDemo`, `reference`) 
                        VALUES ('".$modUsu->numero_id."', '".$modUsu->nombres."', '".$modUsu->apellidos."', '".$modUsu->email."',  '8605173021', NOW(), '".$modUsu->id_perfil."', '".$modUsu->numero_id."', 'A', '".$modUsu->clave."', '".$modUsu->clave2."', NOW(), '".$modUsu->id_grupo_cliente."', 3, 'COL', '2257', '0', 0, NULL)";
                        $list= Yii::app()->db->createCommand($SQL)->query();
                        //Asignar licencia y exámenes. Descontar licencia al cliente
                        //Asociar la licencia al usuario
                        Yii::import('application.modules.Clients.models.LicensesUser');
                        Yii::import('application.modules.Clients.models.ExamsUser');
                        $modLicUsu = new LicensesUser();
                        $modLicUsu->id_licencia_cliente = $modlicencia_cliente->id_licencias_cliente;
                        $modLicUsu->id_usuario=$modUsu->id_usuario_c;
                        $modLicUsu->fecha_asignacion = new CDbExpression('NOW()');
                        $modLicUsu->fecha_presentacion = new CDbExpression('NOW()');
                        $modLicUsu->hora =  '10:00';
                        $modLicUsu->estado="A";
                        $modLicUsu->save();
                        //Gastar la licencia
                        $modUtil->gastarLicencia($modLicUsu->id_licencia_cliente);
                        //Asignar los exámenes al usuario
                        $modExamenes = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>16));
                        foreach($modExamenes as $examen){
                            $modExamenUsuario = new ExamsUser();
                            $modExamenUsuario->id_examen = $examen->id_examen;
                            $modExamenUsuario->id_licencia_usuario = $modLicUsu->id_licencias_usuario;
                            $modExamenUsuario->fecha_presentacion = $modLicUsu->fecha_presentacion;
                            $modExamenUsuario->hora = '10:00';
                            $modExamenUsuario->salon = 16;
                            $modExamenUsuario->estado = "A";
                            $modExamenUsuario->save();
                        }
                        $hash_ede = base64_encode('8605173021&'.'ede_'.$modArea->id.'&1234');
                        $this->redirect('/ilto3/index.php?r=Clients/default/login&hash_ede='.$hash_ede);
                    } else {
                      $this->redirect('/ilto3/ede_areandina.php?err=2'); 
                    }
                } else {
                  $this->redirect('/ilto3/ede_areandina.php?err=2'); 
                }
            }
        }
        */
    // EDE UNIVERSIDAD DEL AREANDINA
    public function actionAreandina(){
            
            $modUsu = UsersC::model()->findByAttributes(array('id_usuario_c'=>$_POST['id_usuario_c']));
            
            //REVISAMOS SI EL USUARIO EXISTE Y TIENE LICENCIAS
            if(count($modUsu)>0){
                // VERIFICAMOS QUE LICENCIAS TIENE PENDIENTES
                $SQL='SELECT id_licencias_usuario, id_usuario, id_licencia_cliente, id_licencia, licenses_user.fecha_presentacion, licenses_user.estado FROM `licenses_user`
                INNER JOIN `licenses_client`
                ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente
                WHERE `id_usuario` = "'.$_POST['id_usuario_c'].'" AND licenses_user.estado != "F"
                AND licenses_client.id_licencia = 27
                ORDER BY licenses_user.fecha_presentacion DESC';
                $list= Yii::app()->db->createCommand($SQL)->queryAll();
                
                $sql='UPDATE `users_c` SET `id_cliente`="86051730212", `id_grupo_cliente`="'.$_POST['group'].'" WHERE `id_usuario_c` = "'.$_POST['id_usuario_c'].'"';
                $upd= Yii::app()->db->createCommand($sql)->query();
                
                //si la lista de licencias pendientes está vacía seguimos
                if(empty($list)){
                   
                   //pregunto si tiene alguna licencia ede presentada
                    $SQL='SELECT * FROM `licenses_user`
                    INNER JOIN `licenses_client`
                    ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente
                    WHERE `id_usuario` = "'.$_POST['id_usuario_c'].'" AND licenses_user.estado = "F"
                    AND licenses_client.id_licencia = 27
                    ORDER BY licenses_user.fecha_presentacion DESC LIMIT 1';
                    $list= Yii::app()->db->createCommand($SQL)->queryAll();
                   
                   /*
                    //revisar si hay una licencia al menos
                    if(count($list)>0){
                        //estimamos la diferencia de fechas para obtener los dias
                        $datetime1 = new Datetime($list[0]["fecha_presentacion"]);
                        $getNow = date("Y-m-d H:i:s");
                    	$today = new Datetime($getNow);
                    	$interval = date_diff($datetime1, $today);
                    	//obtenemos el valor en numero
                    	$days = explode("+",$interval->format('%R%a'));
                        //si lleva presentada la licencia menos de 10 días, no asignamos y mandamos error
                    	if($days[1]<10){
                    	    $ban_to_date =  date('Y-m-d H:i:s', strtotime("+10 days", strtotime($list[0]["fecha_presentacion"])));
                            $this->redirect('/ilto3/ede_aa.php?err=5&dat='.$ban_to_date.''); 
                        }
                    }
                    */
                    // asignemos licencia si la lista de licencias pendientes está vacia
                    $modlicencia_cliente = LicensesClient::model()->disponibles()->findByAttributes(array('id_licencia'=>27,'estado'=>'A', 'id_cliente'=>'86051730212'));
                    //Si hay licencias disponibles continuar
                    $sql='SELECT SUM(cantidad - utilizados) as total FROM `licenses_client` WHERE `id_cliente` LIKE "86051730212" AND id_licencia = 27';
                    $list= Yii::app()->db->createCommand($sql)->query();
                       
                    
                    if($modlicencia_cliente){
                        $modUtil = new Utilidades();
                        $modArea = new EdeAreandina();
                        $modArea->email = $_POST['email'];
                        $modArea->firstName = $_POST['firstName'];
                        $modArea->lastName = $_POST['lastName'];
                      
                        if($modArea->save()){
                            //Registro unicamente en usuarios
                            $modUsu = new UsersC;
                            $modUsu->id_cliente = '8605173021';
                            $modUsu->email = $_POST['email'];
                            $modUsu->nombres = $_POST['firstName'];
                            $modUsu->apellidos = $_POST['lastName'];
                            $modUsu->id_grupo_cliente = $_POST['group'];
                            $modUsu->id_perfil=6;
                            $modUsu->estado='A';
                            $modUsu->numero_id=$_POST['id_usuario_c'];
                            $modUsu->id_usuario_c =$_POST['id_usuario_c'];
                            $modUsu->clave = md5($_POST['id_usuario_c']);
                            $modUsu->clave2 = $_POST['id_usuario_c'];
                            $modUsu->id_pais=3;
                            $modUsu->id_ciudad=2;
                            //$modUsu->save();
                            
                            //Asignar licencia y exámenes. Descontar licencia al cliente
                            //Asociar la licencia al usuario
                            Yii::import('application.modules.Clients.models.LicensesUser');
                            Yii::import('application.modules.Clients.models.ExamsUser');
                            $modLicUsu = new LicensesUser();
                            $modLicUsu->id_licencia_cliente = $modlicencia_cliente->id_licencias_cliente;
                            $modLicUsu->id_usuario=$modUsu->id_usuario_c;
                            $modLicUsu->fecha_asignacion = new CDbExpression('NOW()');
                            $modLicUsu->fecha_presentacion = new CDbExpression('NOW()');
                            $modLicUsu->hora =  '10:00';
                            $modLicUsu->estado="A";
                            $modLicUsu->save();
                            
                            
                            // Guardamos el registro en la tabla de period
                            $SQL = "INSERT INTO `users_c_period`(`id_users_c_period`, `id_usuario_c`, `id_cliente`, `period`, `id_licencias_usuario`) 
                            VALUES ('', '".$modUsu->id_usuario_c."', '86051730212', '".$_POST["period"]."', '".$modLicUsu->id_licencias_usuario."')";
                            $list= Yii::app()->db->createCommand($SQL)->query();
                            
                            //obtenemos la contraseña del usuario
                            $sql="SELECT clave2 FROM `users_c` WHERE `id_usuario_c` = '".$_POST["id_usuario_c"]."' ";
                            $pass= Yii::app()->db->createCommand($sql)->queryAll();
                            
                            //Gastar la licencia
                            $modUtil->gastarLicencia($modLicUsu->id_licencia_cliente);
                            //Asignar los exámenes al usuario
                            $modExamenes = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>27));
                            foreach($modExamenes as $examen){
                                $modExamenUsuario = new ExamsUser();
                                $modExamenUsuario->id_examen = $examen->id_examen;
                                $modExamenUsuario->id_licencia_usuario = $modLicUsu->id_licencias_usuario;
                                $modExamenUsuario->fecha_presentacion = $modLicUsu->fecha_presentacion;
                                $modExamenUsuario->hora = '10:00';
                                $modExamenUsuario->salon = 16;
                                $modExamenUsuario->estado = "A";
                                $modExamenUsuario->save();
                            }
                           
                            $hash_ede = base64_encode('86051730212&'.$id.'&'.$pass[0]['clave2'].'&license='.$modLicUsu->id_licencias_usuario);
                            $this->redirect('/ilto3/index.php?r=Clients/default/login&hash_ede='.$hash_ede);
                        } else {
                          $this->redirect('/ilto3/ede_aa.php?err=2'); 
                        }
                    }
                
                }else{
                    
                    //cómo hay licencias pendientes
                    foreach ($list as $license) {
                        $this->redirect('index.php?r=Clients/default/login&type=3&message=3');
                        
                    }
                }
            } else {
                
                //Registro en el area andina
                $modlicencia_cliente = LicensesClient::model()->disponibles()->findByAttributes(array('id_licencia'=>27,'estado'=>'A', 'id_cliente'=>'86051730212'));
                //Si hay licencias disponibles continuar
                $sql='SELECT SUM(cantidad - utilizados) as total FROM `licenses_client` WHERE `id_cliente` LIKE "86051730212" AND id_licencia = 27';
                $list= Yii::app()->db->createCommand($sql)->query();
                
                if($modlicencia_cliente){
                    $modUtil = new Utilidades();
                    $modArea = new EdeAreandina();
                    $modArea->email = $_POST['email'];
                    $modArea->firstName = $_POST['firstName'];
                    $modArea->lastName = $_POST['lastName'];
                  
                    if($modArea->save()){
                        //Registro unicamente en usuarios
                        $modUsu = new UsersC;
                        $modUsu->id_cliente = '86051730212';
                        $modUsu->email = $_POST['email'];
                        $modUsu->nombres = $_POST['firstName'];
                        $modUsu->apellidos = $_POST['lastName'];
                        $modUsu->id_grupo_cliente = $_POST['group'];
                        $modUsu->id_perfil=6;
                        $modUsu->estado='A';
                        $modUsu->numero_id=$_POST['id_usuario_c'];
                        $modUsu->id_usuario_c =$_POST['id_usuario_c'];
                        $modUsu->clave = md5($_POST['id_usuario_c']);
                        $modUsu->clave2 = $_POST['id_usuario_c'];
                        $modUsu->id_pais=3;
                        $modUsu->id_ciudad=2;
                        //$modUsu->save();
                        
                        $SQL = "INSERT INTO `users_c` (`id_usuario_c`, `nombres`, `apellidos`, `email`, `id_cliente`, `ultimo_acceso`, `id_perfil`, `numero_id`, `ruta_foto`, `estado`, `clave`, `clave2`, `fecha_creado`, `id_grupo_cliente`, `id_pais`, `id_country`, `id_ciudad`, `email_certificate`, `isDemo`, `reference`) 
                        VALUES ('".$modUsu->numero_id."', '".$modUsu->nombres."', '".$modUsu->apellidos."', '".$modUsu->email."',  '86051730212', NOW(), '".$modUsu->id_perfil."', '".$modUsu->numero_id."', '', 'A', '".$modUsu->clave."', '".$modUsu->clave2."', NOW(), '".$modUsu->id_grupo_cliente."', 3, 'COL', '2257', '0', '0', NULL)";
                        $list= Yii::app()->db->createCommand($SQL)->query();
                        //Asignar licencia y exámenes. Descontar licencia al cliente
                        //Asociar la licencia al usuario
                        Yii::import('application.modules.Clients.models.LicensesUser');
                        Yii::import('application.modules.Clients.models.ExamsUser');
                        $modLicUsu = new LicensesUser();
                        $modLicUsu->id_licencia_cliente = $modlicencia_cliente->id_licencias_cliente;
                        $modLicUsu->id_usuario=$modUsu->id_usuario_c;
                        $modLicUsu->fecha_asignacion = new CDbExpression('NOW()');
                        $modLicUsu->fecha_presentacion = new CDbExpression('NOW()');
                        $modLicUsu->hora =  '10:00';
                        $modLicUsu->estado="A";
                        $modLicUsu->save();
                        //Gastar la licencia
                        
                        // Guardamos el registro en la tabla de period
                        $SQL = "INSERT INTO `users_c_period`(`id_users_c_period`, `id_usuario_c`, `id_cliente`, `period`, `id_licencias_usuario`) 
                        VALUES ('', '".$modUsu->id_usuario_c."', '86051730212', '".$_POST["period"]."', '".$modLicUsu->id_licencias_usuario."')";
                        $list= Yii::app()->db->createCommand($SQL)->query();
                        $modUtil->gastarLicencia($modLicUsu->id_licencia_cliente);
                        //Asignar los exámenes al usuario
                        $modExamenes = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>27));
                        foreach($modExamenes as $examen){
                            $modExamenUsuario = new ExamsUser();
                            $modExamenUsuario->id_examen = $examen->id_examen;
                            $modExamenUsuario->id_licencia_usuario = $modLicUsu->id_licencias_usuario;
                            $modExamenUsuario->fecha_presentacion = $modLicUsu->fecha_presentacion;
                            $modExamenUsuario->hora = '10:00';
                            $modExamenUsuario->salon = 16;
                            $modExamenUsuario->estado = "A";
                            $modExamenUsuario->save();
                        }
                        
                        $hash_ede = base64_encode('86051730212&'.$id.'&'.$pass[0]['clave2'].'&license='.$modLicUsu->id_licencias_usuario);
                        $this->redirect('/ilto3/index.php?r=Clients/default/login&hash_ede='.$hash_ede);
                    } else {
                      $this->redirect('/ilto3/ede_aa.php?err=2'); 
                    }
                } else {
                  $this->redirect('/ilto3/ede_aa.php?err=2'); 
                }
            }
        }
        
    // EDE UNIVERSIDAD DEL AREANDINA
    public function actionAreandina_new(){
            
            $modUsu = UsersC::model()->findByAttributes(array('id_usuario_c'=>$_POST['id_usuario_c']));
            
            if(empty($_POST["program"])){
                $this->redirect('/ilto3/ede_aa.php?err=6'); 
            }
            //consultamos el ID de la school
            $sql='SELECT id_school FROM `program_school_city` WHERE `id_program` = '.$_POST["program"].' ORDER BY `id_psc` DESC';
            $list= Yii::app()->db->createCommand($sql)->queryAll();
            $id_school = $list[0]["id_school"];
            
            
            //REVISAMOS SI EL USUARIO EXISTE Y TIENE LICENCIAS
            if(!is_null($modUsu)>0){
                
                // VERIFICAMOS QUE LICENCIAS TIENE PENDIENTES
                $SQL='SELECT id_licencias_usuario, id_usuario, id_licencia_cliente, id_licencia, licenses_user.fecha_presentacion, licenses_user.estado FROM `licenses_user`
                INNER JOIN `licenses_client`
                ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente
                WHERE `id_usuario` = "'.$_POST['id_usuario_c'].'" AND licenses_user.estado != "F"
                ORDER BY licenses_user.fecha_presentacion DESC';
                $list= Yii::app()->db->createCommand($SQL)->queryAll();
                
                
                
                //si la lista de licencias pendientes está vacía seguimos
                if(sizeof($list)==0){
                    
                    $sql='UPDATE `users_c` SET `id_cliente`="86051730212", `id_grupo_cliente`="726" WHERE `id_usuario_c` = "'.$_POST['id_usuario_c'].'"';
                    $upd= Yii::app()->db->createCommand($sql)->query();
                    
                   //pregunto si tiene alguna licencia ede presentada
                    $SQL='SELECT * FROM `licenses_user`
                    INNER JOIN `licenses_client`
                    ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente
                    WHERE `id_usuario` = "'.$_POST['id_usuario_c'].'" AND licenses_user.estado = "F"
                    AND licenses_client.id_licencia = 27
                    ORDER BY licenses_user.fecha_presentacion DESC LIMIT 1';
                    $list= Yii::app()->db->createCommand($SQL)->queryAll();
                   
                   
                    //revisar si hay una licencia al menos
                    if(!is_null($list)>0){
                        //estimamos la diferencia de fechas para obtener los dias
                        $datetime1 = new Datetime($list[0]["fecha_presentacion"]);
                        $getNow = date("Y-m-d H:i:s");
                    	$today = new Datetime($getNow);
                    	$interval = date_diff($datetime1, $today);
                    	//obtenemos el valor en numero
                    	$days = explode("+",$interval->format('%R%a'));
                        //si lleva presentada la licencia menos de 10 días, no asignamos y mandamos error
                    	if($days[1]<10){
                    	    $ban_to_date =  date('Y-m-d H:i:s', strtotime("+10 days", strtotime($list[0]["fecha_presentacion"])));
                           // $this->redirect('/ilto3/ede_aa.php?err=5&dat='.$ban_to_date.''); 
                        }
                    }
                    
                    // asignemos licencia si la lista de licencias pendientes está vacia
                    $modlicencia_cliente = LicensesClient::model()->disponibles()->findByAttributes(array('id_licencia'=>27,'estado'=>'A', 'id_cliente'=>'86051730212'));
                    //Si hay licencias disponibles continuar
                    $sql='SELECT SUM(cantidad - utilizados) as total FROM `licenses_client` WHERE `id_cliente` LIKE "86051730212" AND id_licencia = 27';
                    $list= Yii::app()->db->createCommand($sql)->query();
                       
                    
                    if($modlicencia_cliente){
                        $modUtil = new Utilidades();
                        $modArea = new EdeAreandina();
                        $modArea->email = $_POST['email'];
                        $modArea->firstName = $_POST['firstName'];
                        $modArea->lastName = $_POST['lastName'];
                      
                        if($modArea->save()){
                            //Registro unicamente en usuarios
                            $modUsu = new UsersC;
                            $modUsu->id_cliente = '86051730212';
                            $modUsu->email = $_POST['email'];
                            $modUsu->nombres = $_POST['firstName'];
                            $modUsu->apellidos = $_POST['lastName'];
                            $modUsu->id_grupo_cliente = $_POST['group'];
                            $modUsu->id_perfil=6;
                            $modUsu->estado='A';
                            $modUsu->numero_id=$_POST['id_usuario_c'];
                            $modUsu->id_usuario_c =$_POST['id_usuario_c'];
                            $modUsu->clave = md5($_POST['id_usuario_c']);
                            $modUsu->clave2 = $_POST['id_usuario_c'];
                            $modUsu->id_pais=3;
                            $modUsu->id_ciudad=2;
                            //$modUsu->save();
                            
                            //Asignar licencia y exámenes. Descontar licencia al cliente
                            //Asociar la licencia al usuario
                            Yii::import('application.modules.Clients.models.LicensesUser');
                            Yii::import('application.modules.Clients.models.ExamsUser');
                            $modLicUsu = new LicensesUser();
                            $modLicUsu->id_licencia_cliente = $modlicencia_cliente->id_licencias_cliente;
                            $modLicUsu->id_usuario=$modUsu->id_usuario_c;
                            $modLicUsu->fecha_asignacion = new CDbExpression('NOW()');
                            $modLicUsu->fecha_presentacion = new CDbExpression('DATE_SUB(now(), INTERVAL 1 HOUR)');
                            $modLicUsu->hora =  '10:00';
                            $modLicUsu->estado="A";
                            $modLicUsu->save();
                            
                            
                            // Guardamos el registro en la tabla de period
                            $SQL = "INSERT INTO `users_c_period`(`id_users_c_period`, `id_usuario_c`, `id_cliente`, `period`, `id_city`, `id_school`, `id_program`, `id_licencias_usuario`) 
                            VALUES ('', '".$modUsu->id_usuario_c."', '86051730212', '".$_POST["period"]."', '".$_POST["city"]."', '".$id_school."', '".$_POST["program"]."', '".$modLicUsu->id_licencias_usuario."')";
                            $list= Yii::app()->db->createCommand($SQL)->query();
                            
                            //obtenemos la contraseña del usuario
                            $sql="SELECT clave2 FROM `users_c` WHERE `id_usuario_c` = '".$_POST["id_usuario_c"]."' ";
                            $pass= Yii::app()->db->createCommand($sql)->queryAll();
                            
                            //Gastar la licencia
                            $modUtil->gastarLicencia($modLicUsu->id_licencia_cliente);
                            //Asignar los exámenes al usuario
                            $modExamenes = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>27));
                            foreach($modExamenes as $examen){
                                $modExamenUsuario = new ExamsUser();
                                $modExamenUsuario->id_examen = $examen->id_examen;
                                $modExamenUsuario->id_licencia_usuario = $modLicUsu->id_licencias_usuario;
                                $modExamenUsuario->fecha_presentacion = $modLicUsu->fecha_presentacion;
                                $modExamenUsuario->hora = '10:00';
                                $modExamenUsuario->salon = 16;
                                $modExamenUsuario->estado = "A";
                                $modExamenUsuario->save();
                            }
                            $hash_ede = base64_encode('86051730212&'.$_POST['id_usuario_c'].'&'.$pass[0]['clave2'].'&license='.$modLicUsu->id_licencias_usuario);
                            
                            $this->redirect('/ilto3/index.php?r=Clients/default/login&hash_ede='.$hash_ede);
                        } else {
                          $this->redirect('/ilto3/ede_aa.php?err=2'); 
                        }
                    }
                
                }else{
                    
                    //cómo hay licencias pendientes
                    foreach ($list as $license) {
                        $this->redirect('index.php?r=Clients/default/login&type=3&message=3');
                        
                    }
                }
            } else {
                
                //Registro en el area andina
                $modlicencia_cliente = LicensesClient::model()->disponibles()->findByAttributes(array('id_licencia'=>27,'estado'=>'A', 'id_cliente'=>'86051730212'));
                //Si hay licencias disponibles continuar
                $sql='SELECT SUM(cantidad - utilizados) as total FROM `licenses_client` WHERE `id_cliente` LIKE "86051730212" AND id_licencia = 27';
                $list= Yii::app()->db->createCommand($sql)->query();
                
                // Guardamos por defecto un grupo de la FUAA
                $sql='SELECT SUM(cantidad - utilizados) as total FROM `licenses_client` WHERE `id_cliente` LIKE "86051730212" AND id_licencia = 27';
                $list= Yii::app()->db->createCommand($sql)->query();
                
                if($modlicencia_cliente){
                    $modUtil = new Utilidades();
                    $modArea = new EdeAreandina();
                    $modArea->email = $_POST['email'];
                    $modArea->firstName = $_POST['firstName'];
                    $modArea->lastName = $_POST['lastName'];
                  
                    if($modArea->save()){
                        //Registro unicamente en usuarios
                        $modUsu = new UsersC;
                        $modUsu->id_cliente = '86051730212';
                        $modUsu->email = $_POST['email'];
                        $modUsu->nombres = $_POST['firstName'];
                        $modUsu->apellidos = $_POST['lastName'];
                        $modUsu->id_grupo_cliente = '577';
                        $modUsu->id_perfil=6;
                        $modUsu->estado='A';
                        $modUsu->numero_id=$_POST['id_usuario_c'];
                        $modUsu->id_usuario_c =$_POST['id_usuario_c'];
                        $modUsu->clave = md5($_POST['id_usuario_c']);
                        $modUsu->clave2 = $_POST['id_usuario_c'];
                        $modUsu->id_pais=3;
                        $modUsu->id_ciudad=2;
                        //$modUsu->save();
                        
                        $SQL = "INSERT INTO `users_c` (`id_usuario_c`, `nombres`, `apellidos`, `email`, `id_cliente`, `ultimo_acceso`, `id_perfil`, `numero_id`, `ruta_foto`, `estado`, `clave`, `clave2`, `fecha_creado`, `id_grupo_cliente`, `id_pais`, `id_country`, `id_ciudad`, `email_certificate`, `isDemo`, `reference`) 
                        VALUES ('".$modUsu->numero_id."', '".$modUsu->nombres."', '".$modUsu->apellidos."', '".$modUsu->email."',  '86051730212', NOW(), '".$modUsu->id_perfil."', '".$modUsu->numero_id."', '', 'A', '".$modUsu->clave."', '".$modUsu->clave2."', NOW(), '".$modUsu->id_grupo_cliente."', 3, 'COL', '2257', '0', '0', NULL)";
                        $list= Yii::app()->db->createCommand($SQL)->query();
                        //Asignar licencia y exámenes. Descontar licencia al cliente
                        //Asociar la licencia al usuario
                        Yii::import('application.modules.Clients.models.LicensesUser');
                        Yii::import('application.modules.Clients.models.ExamsUser');
                        $modLicUsu = new LicensesUser();
                        $modLicUsu->id_licencia_cliente = $modlicencia_cliente->id_licencias_cliente;
                        $modLicUsu->id_usuario=$modUsu->id_usuario_c;
                        $modLicUsu->fecha_asignacion = new CDbExpression('NOW()');
                        $modLicUsu->fecha_presentacion = new CDbExpression('DATE_SUB(now(), INTERVAL 1 HOUR)');
                        $modLicUsu->hora =  '10:00';
                        $modLicUsu->estado="A";
                        $modLicUsu->save();
                        //Gastar la licencia
                        
                        // Guardamos el registro en la tabla de period
                        $SQL = "INSERT INTO `users_c_period`(`id_users_c_period`, `id_usuario_c`, `id_cliente`, `period`, `id_city`, `id_school`, `id_program`, `id_licencias_usuario`) 
                        VALUES ('', '".$modUsu->id_usuario_c."', '86051730212', '".$_POST["period"]."', '".$_POST["city"]."', '".$id_school."', '".$_POST["program"]."', '".$modLicUsu->id_licencias_usuario."')";
                        $list= Yii::app()->db->createCommand($SQL)->query();
                        $modUtil->gastarLicencia($modLicUsu->id_licencia_cliente);
                        //Asignar los exámenes al usuario
                        $modExamenes = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>27));
                        foreach($modExamenes as $examen){
                            $modExamenUsuario = new ExamsUser();
                            $modExamenUsuario->id_examen = $examen->id_examen;
                            $modExamenUsuario->id_licencia_usuario = $modLicUsu->id_licencias_usuario;
                            $modExamenUsuario->fecha_presentacion = $modLicUsu->fecha_presentacion;
                            $modExamenUsuario->hora = '10:00';
                            $modExamenUsuario->salon = 16;
                            $modExamenUsuario->estado = "A";
                            $modExamenUsuario->save();
                        }
                        $hash_ede = base64_encode('86051730212&'.$modUsu->id_usuario_c.'&'.$modUsu->id_usuario_c.'&license='.$modExamenUsuario->id_licencia_usuario);
                        
                        $this->redirect('/ilto3/index.php?r=Clients/default/login&hash_ede='.$hash_ede);
                    } else {
                      $this->redirect('/ilto3/ede_aa.php?err=2'); 
                    }
                } else {
                  $this->redirect('/ilto3/ede_aa.php?err=2'); 
                }
            }
        }
        
    public function actionAreandina_admin(){
            
        $modUsu = UsersC::model()->findByAttributes(array('id_usuario_c'=>$_POST['id_usuario_c']));
        if(!isset($_POST["program"])){
            $program = 0;
         }else{
            $program = $_POST["program"];
         }
         
        //REVISAMOS SI EL USUARIO EXISTE Y TIENE LICENCIAS
        if(!is_null($modUsu)>0){
                // VERIFICAMOS QUE LICENCIAS TIENE PENDIENTES
                $SQL='SELECT id_licencias_usuario, id_usuario, id_licencia_cliente, id_licencia, licenses_user.fecha_presentacion, licenses_user.estado FROM `licenses_user`
                INNER JOIN `licenses_client`
                ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente
                WHERE `id_usuario` = "'.$_POST['id_usuario_c'].'" AND licenses_user.estado != "F"
                AND licenses_client.id_licencia = 27
                ORDER BY licenses_user.fecha_presentacion DESC';
                $list= Yii::app()->db->createCommand($SQL)->queryAll();
                
                $sql='UPDATE `users_c` SET `id_cliente`="86051730211", `id_grupo_cliente`="576" WHERE `id_usuario_c` = "'.$_POST['id_usuario_c'].'"';
                $upd= Yii::app()->db->createCommand($sql)->query();
                
                //si la lista de licencias pendientes está vacía seguimos
                if(empty($list)){
                   
                   //pregunto si tiene alguna licencia ede presentada
                    $SQL='SELECT * FROM `licenses_user`
                    INNER JOIN `licenses_client`
                    ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente
                    WHERE `id_usuario` = "'.$_POST['id_usuario_c'].'" AND licenses_user.estado = "F"
                    AND licenses_client.id_licencia = 27
                    ORDER BY licenses_user.fecha_presentacion DESC LIMIT 1';
                    $list= Yii::app()->db->createCommand($SQL)->queryAll();
                   
                    //revisar si hay una licencia al menos
                    if(!is_null($list)>0){
                        //estimamos la diferencia de fechas para obtener los dias
                        $datetime1 = new Datetime($list[0]["fecha_presentacion"]);
                        $getNow = date("Y-m-d H:i:s");
                    	$today = new Datetime($getNow);
                    	$interval = date_diff($datetime1, $today);
                    	//obtenemos el valor en numero
                    	$days = explode("+",$interval->format('%R%a'));
                        //si lleva presentada la licencia menos de 10 días, no asignamos y mandamos error
                    	if($days[1]<10){
                    	    $ban_to_date =  date('Y-m-d H:i:s', strtotime("+10 days", strtotime($list[0]["fecha_presentacion"])));
                            //$this->redirect('/ilto3/ede_areandina.php?err=5&dat='.$ban_to_date.''); 
                        }
                    }
                    
                    // asignemos licencia si la lista de licencias pendientes está vacia
                    $modlicencia_cliente = LicensesClient::model()->disponibles()->findByAttributes(array('id_licencia'=>27,'estado'=>'A', 'id_cliente'=>'86051730211'));
                    //Si hay licencias disponibles continuar
                    $sql='SELECT SUM(cantidad - utilizados) as total FROM `licenses_client` WHERE `id_cliente` = "86051730211" AND id_licencia = 27';
                    $list= Yii::app()->db->createCommand($sql)->query();
                       
                    
                    if($modlicencia_cliente){
                        $modUtil = new Utilidades();
                        $modArea = new EdeAreandina();
                        $modArea->email = $_POST['email'];
                        $modArea->firstName = $_POST['firstName'];
                        $modArea->lastName = $_POST['lastName'];
                      
                        if($modArea->save()){
                            //Registro unicamente en usuarios
                            $modUsu = new UsersC;
                            $modUsu->id_cliente = '86051730211';
                            $modUsu->email = $_POST['email'];
                            $modUsu->nombres = $_POST['firstName'];
                            $modUsu->apellidos = $_POST['lastName'];
                            $modUsu->id_grupo_cliente = '576';
                            $modUsu->id_perfil=6;
                            $modUsu->estado='A';
                            $modUsu->numero_id=$_POST['id_usuario_c'];
                            $modUsu->id_usuario_c =$_POST['id_usuario_c'];
                            $modUsu->clave = md5($_POST['id_usuario_c']);
                            $modUsu->clave2 = $_POST['id_usuario_c'];
                            $modUsu->id_pais=3;
                            $modUsu->id_ciudad=2;
                            //$modUsu->save();
                            
                            //Asignar licencia y exámenes. Descontar licencia al cliente
                            //Asociar la licencia al usuario
                            Yii::import('application.modules.Clients.models.LicensesUser');
                            Yii::import('application.modules.Clients.models.ExamsUser');
                            $modLicUsu = new LicensesUser();
                            $modLicUsu->id_licencia_cliente = $modlicencia_cliente->id_licencias_cliente;
                            $modLicUsu->id_usuario=$modUsu->id_usuario_c;
                            $modLicUsu->fecha_asignacion = new CDbExpression('NOW()');
                            $modLicUsu->fecha_presentacion = new CDbExpression('DATE_SUB(now(), INTERVAL 1 HOUR)');
                            $modLicUsu->hora =  '10:00';
                            $modLicUsu->estado="A";
                            $modLicUsu->save();
                            
                            
                            
                            // Guardamos el registro en la tabla de period
                            $SQL = "INSERT INTO `users_c_period`(`id_users_c_period`, `id_usuario_c`, `id_cliente`, `period`, `id_city`, `id_school`, `id_program`, `id_licencias_usuario`, `id_category`, `id_roll`) 
                            VALUES ('', '".$modUsu->id_usuario_c."', '86051730211', '0', '".$_POST["city"]."', '0', '".$program."', '".$modLicUsu->id_licencias_usuario."', '".$_POST["category"]."', '".$_POST["roll"]."')";
                            $list= Yii::app()->db->createCommand($SQL)->query();
                            
                            //obtenemos la contraseña del usuario
                            $sql="SELECT clave2 FROM `users_c` WHERE `id_usuario_c` = '".$_POST["id_usuario_c"]."' ";
                            $pass= Yii::app()->db->createCommand($sql)->queryAll();
                            
                            //Gastar la licencia
                            $modUtil->gastarLicencia($modLicUsu->id_licencia_cliente);
                            //Asignar los exámenes al usuario
                            $modExamenes = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>27));
                            foreach($modExamenes as $examen){
                                $modExamenUsuario = new ExamsUser();
                                $modExamenUsuario->id_examen = $examen->id_examen;
                                $modExamenUsuario->id_licencia_usuario = $modLicUsu->id_licencias_usuario;
                                $modExamenUsuario->fecha_presentacion = $modLicUsu->fecha_presentacion;
                                $modExamenUsuario->hora = '10:00';
                                $modExamenUsuario->salon = 389;
                                $modExamenUsuario->estado = "A";
                                $modExamenUsuario->save();
                            }
                            $hash_ede = base64_encode('86051730212&'.$_POST['id_usuario_c'].'&'.$pass[0]['clave2'].'&license='.$modLicUsu->id_licencias_usuario);
                            
                            $this->redirect('/ilto3/index.php?r=Clients/default/login&hash_ede='.$hash_ede);
                        } else {
                          $this->redirect('/ilto3/ede_areandina.php?err=2'); 
                        }
                    }
                
                }else{
                    
                    //cómo hay licencias pendientes
                    foreach ($list as $license) {
                        $this->redirect('index.php?r=Clients/default/login&type=3&message=3');
                        
                    }
                }
            } else {
                
                //Registro en el area andina
                $modlicencia_cliente = LicensesClient::model()->disponibles()->findByAttributes(array('id_licencia'=>27,'estado'=>'A', 'id_cliente'=>'86051730211'));
                //Si hay licencias disponibles continuar
                $sql='SELECT SUM(cantidad - utilizados) as total FROM `licenses_client` WHERE `id_cliente` = "86051730211" AND id_licencia = 27';
                $list= Yii::app()->db->createCommand($sql)->query();
                
                if($modlicencia_cliente){
                    $modUtil = new Utilidades();
                    $modArea = new EdeAreandina();
                    $modArea->email = $_POST['email'];
                    $modArea->firstName = $_POST['firstName'];
                    $modArea->lastName = $_POST['lastName'];
                  
                    if($modArea->save()){
                        //Registro unicamente en usuarios
                        $modUsu = new UsersC;
                        $modUsu->id_cliente = '86051730211';
                        $modUsu->email = $_POST['email'];
                        $modUsu->nombres = $_POST['firstName'];
                        $modUsu->apellidos = $_POST['lastName'];
                        $modUsu->id_perfil=6;
                        $modUsu->estado='A';
                        $modUsu->numero_id=$_POST['id_usuario_c'];
                        $modUsu->id_usuario_c =$_POST['id_usuario_c'];
                        $modUsu->clave = md5($_POST['id_usuario_c']);
                        $modUsu->clave2 = $_POST['id_usuario_c'];
                        $modUsu->id_pais=3;
                        $modUsu->id_ciudad=2;
                        if($_POST["roll"]== 1){
                            $modUsu->id_grupo_cliente = '612';
                        }else{
                            $modUsu->id_grupo_cliente = '613';
                        }
                        //$modUsu->save();
                         
                        $SQL = "INSERT INTO `users_c` (`id_usuario_c`, `nombres`, `apellidos`, `email`, `id_cliente`, `ultimo_acceso`, `id_perfil`, `numero_id`, `ruta_foto`, `estado`, `clave`, `clave2`, `fecha_creado`, `id_grupo_cliente`, `id_pais`, `id_country`, `id_ciudad`, `email_certificate`, `isDemo`, `reference`) 
                        VALUES ('".$modUsu->numero_id."', '".$modUsu->nombres."', '".$modUsu->apellidos."', '".$modUsu->email."',  '86051730211', NOW(), '".$modUsu->id_perfil."', '".$modUsu->numero_id."', '', 'A', '".$modUsu->clave."', '".$modUsu->clave2."', NOW(), '".$modUsu->id_grupo_cliente."', 3, 'COL', '2257', '0', '0', NULL)";
                        $list= Yii::app()->db->createCommand($SQL)->query();
                        //Asignar licencia y exámenes. Descontar licencia al cliente
                        //Asociar la licencia al usuario
                        Yii::import('application.modules.Clients.models.LicensesUser');
                        Yii::import('application.modules.Clients.models.ExamsUser');
                        $modLicUsu = new LicensesUser();
                        $modLicUsu->id_licencia_cliente = $modlicencia_cliente->id_licencias_cliente;
                        $modLicUsu->id_usuario=$modUsu->id_usuario_c;
                        $modLicUsu->fecha_asignacion = new CDbExpression('NOW()');
                        $modLicUsu->fecha_presentacion = new CDbExpression('DATE_SUB(now(), INTERVAL 1 HOUR)');
                        $modLicUsu->hora =  '10:00';
                        $modLicUsu->estado="A";
                        $modLicUsu->save();
                        //Gastar la licencia
                        
                        // Guardamos el registro en la tabla de period
                        // Guardamos el registro en la tabla de period
                        $SQL = "INSERT INTO `users_c_period`(`id_users_c_period`, `id_usuario_c`, `id_cliente`, `period`, `id_city`, `id_school`, `id_program`, `id_licencias_usuario`, `id_category`, `id_roll`) 
                        VALUES ('', '".$modUsu->id_usuario_c."', '86051730211', '0', '".$_POST["city"]."', '0', '".$program."', '".$modLicUsu->id_licencias_usuario."', '".$_POST["category"]."', '".$_POST["roll"]."')";
                        $list= Yii::app()->db->createCommand($SQL)->query();
                        $modUtil->gastarLicencia($modLicUsu->id_licencia_cliente);
                        //Asignar los exámenes al usuario
                        $modExamenes = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>27));
                        foreach($modExamenes as $examen){
                            $modExamenUsuario = new ExamsUser();
                            $modExamenUsuario->id_examen = $examen->id_examen;
                            $modExamenUsuario->id_licencia_usuario = $modLicUsu->id_licencias_usuario;
                            $modExamenUsuario->fecha_presentacion = $modLicUsu->fecha_presentacion;
                            $modExamenUsuario->hora = '10:00';
                            $modExamenUsuario->salon = 389;
                            $modExamenUsuario->estado = "A";
                            $modExamenUsuario->save();
                        }
                        $hash_ede = base64_encode('86051730212&'.$modUsu->id_usuario_c.'&'.$modUsu->id_usuario_c.'&license='.$modExamenUsuario->id_licencia_usuario);
                        
                        $this->redirect('/ilto3/index.php?r=Clients/default/login&hash_ede='.$hash_ede);
                    } else {
                      $this->redirect('/ilto3/ede_areandina.php?err=2'); 
                    }
                } else {
                  $this->redirect('/ilto3/ede_areandina.php?err=2'); 
                }
            }
    }//end areaandina_admin
    
    
    // EDE UNIVERSIDAD DEL AREANDINA
    public function actionDemo_tecs(){
            $id = '1001';
            $modUsu = UsersC::model()->findByAttributes(array('id_usuario_c'=>$id));
            
                //pregunto si tiene alguna licencia ede presentada
                $SQL='SELECT * FROM `licenses_user`
                INNER JOIN `licenses_client`
                ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente
                WHERE `id_usuario` = "'.$id.'" AND licenses_user.estado = "A"
                AND licenses_client.id_licencia = 5
                ORDER BY licenses_user.fecha_presentacion DESC LIMIT 1';
                $list= Yii::app()->db->createCommand($SQL)->queryAll();
                
                
                /*//revisar si hay una licencia al menos
                if(!empty($list)){
                    //actualizamos el estado  F  
                    $SQL = 'UPDATE `licenses_user` SET `estado`="F" WHERE `id_licencias_usuario` = "'.$list[0]['id_licencias_usuario'].'"';
                    $delete= Yii::app()->db->createCommand($SQL)->query();
                }*/
                
                 //revisar si hay una licencia al menos
                if(!empty($list)){
                    //eliminamos l   
                    $SQL = 'UPDATE `licenses_user` SET `estado`="F" WHERE `id_licencias_usuario` =  "'.$list[0]['id_licencias_usuario'].'"';
                    $update= Yii::app()->db->createCommand($SQL)->query();
                }
                
                 
                // asignemos licencia si la lista de licencias pendientes está vacia
                $modlicencia_cliente = LicensesClient::model()->disponibles()->findByAttributes(array('id_licencia'=>5,'estado'=>'A', 'id_cliente'=>'2030405060'));
                //Si hay licencias disponibles continuar
                $sql='SELECT SUM(cantidad - utilizados) as total FROM `licenses_client` WHERE `id_cliente` LIKE "2030405060" AND id_licencia = 5';
                $list= Yii::app()->db->createCommand($sql)->queryAll();
                
                if($modlicencia_cliente){
                   
                    $modUtil = new Utilidades();
                   
                    //Asignar licencia y exámenes. Descontar licencia al cliente
                    //Asociar la licencia al usuario
                    Yii::import('application.modules.Clients.models.LicensesUser');
                    Yii::import('application.modules.Clients.models.ExamsUser');
                    $modLicUsu = new LicensesUser();
                    $modLicUsu->id_licencia_cliente = $modlicencia_cliente->id_licencias_cliente;
                    $modLicUsu->id_usuario=$modUsu->id_usuario_c;
                    $modLicUsu->fecha_asignacion = new CDbExpression('NOW()');
                    $modLicUsu->fecha_presentacion = new CDbExpression('DATE_SUB(now(), INTERVAL 1 HOUR)');
                    $modLicUsu->hora =  '10:00';
                    $modLicUsu->estado="A";
                    $modLicUsu->save();
                            
                    //obtenemos la contraseña del usuario
                    $sql="SELECT clave2 FROM `users_c` WHERE `id_usuario_c` = '".$id."' ";
                    $pass= Yii::app()->db->createCommand($sql)->queryAll();
                    
                    
                    //Gastar la licencia
                    $modUtil->gastarLicencia($modLicUsu->id_licencia_cliente);
                    //Asignar los exámenes al usuario
                    $modExamenes = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>5));
                    foreach($modExamenes as $examen){
                        $modExamenUsuario = new ExamsUser();
                        $modExamenUsuario->id_examen = $examen->id_examen;
                        $modExamenUsuario->id_licencia_usuario = $modLicUsu->id_licencias_usuario;
                        $modExamenUsuario->fecha_presentacion = $modLicUsu->fecha_presentacion;
                        $modExamenUsuario->hora = '10:00';
                        $modExamenUsuario->salon = 291;
                        $modExamenUsuario->estado = "A";
                        $modExamenUsuario->save();
                    }
                    $sql="SELECT * FROM `exams_user` WHERE `id_licencia_usuario` = '".$modLicUsu->id_licencias_usuario."' ";
                    $exams= Yii::app()->db->createCommand($sql)->queryAll();
                    
                    
                    $hash_ede = base64_encode('2030405060&'.$id.'&'.$pass[0]['clave2'].'&license='.$modLicUsu->id_licencias_usuario);
                  
                    $this->redirect('/ilto3/index.php?r=Clients/default/login&hash_ede='.$hash_ede);
                       
                }else{
                     $this->redirect('https://www.iltoexams.com');
                }
                
    }
    
    // TECS LINK UNIVERSIDAD DEL VALLE
    public function actionUdelvalle(){
            
            $modUsu = UsersC::model()->findByAttributes(array('id_usuario_c'=>$_POST['id_usuario_c']));
            
            
            
            if(!empty($modUsu)){
                /*if($modUsu->id_cliente!='89039901661'){  PARA UDLV no se hacen transferencias en esta modalidad
                    $this->redirect('/ilto3/tecs_udv.php?err=4');    
                }*/
                $this->redirect('/ilto3/tecs_udv.php?err=3'); 
            } else {
                
                //Registro en el area andina
                $modlicencia_cliente = LicensesClient::model()->disponibles()->findByAttributes(array('id_licencia'=>18,'estado'=>'A', 'id_cliente'=>'8903990166'));
                //Si hay licencias disponibles continuar
                $sql='SELECT SUM(cantidad - utilizados) as total FROM `licenses_client` WHERE `id_cliente` LIKE "8903990166" AND id_licencia = 18';
                $list= Yii::app()->db->createCommand($sql)->query();
                
                if($modlicencia_cliente){
                    $modUtil = new Utilidades();
                    $modArea = new EdeAreandina();
                    $modArea->email = $_POST['email'];
                    $modArea->firstName = $_POST['firstName'];
                    $modArea->lastName = $_POST['lastName'];
                  
                    if($modArea->save()){
                        //Registro unicamente en usuarios
                        $modUsu = new UsersC;
                        $modUsu->id_cliente = '8903990166';
                        $modUsu->email = $_POST['email'];
                        $modUsu->nombres = $_POST['firstName'];
                        $modUsu->apellidos = $_POST['lastName'];
                        $modUsu->id_grupo_cliente = $_POST['group'];
                        $modUsu->id_perfil=6;
                        $modUsu->estado='A';
                        $modUsu->numero_id=$_POST['id_usuario_c'];
                        $modUsu->id_usuario_c =$_POST['id_usuario_c'];
                        $modUsu->clave = md5($_POST['id_usuario_c']);
                        $modUsu->clave2 = $_POST['id_usuario_c'];
                        $modUsu->id_pais=3;
                        $modUsu->id_ciudad=2;
                        //$modUsu->save();
                        
                        $SQL = "INSERT INTO `users_c` (`id_usuario_c`, `nombres`, `apellidos`, `email`, `id_cliente`, `ultimo_acceso`, `id_perfil`, `numero_id`, `ruta_foto`, `estado`, `clave`, `clave2`, `fecha_creado`, `id_grupo_cliente`, `id_pais`, `id_country`, `id_ciudad`, `email_certificate`, `isDemo`, `reference`) 
                        VALUES ('".$modUsu->numero_id."', '".$modUsu->nombres."', '".$modUsu->apellidos."', '".$modUsu->email."',  '8903990166', NOW(), '".$modUsu->id_perfil."', '".$modUsu->numero_id."', 'universidad del valle.gif', 'A', '".$modUsu->clave."', '".$modUsu->clave2."', NOW(), '".$modUsu->id_grupo_cliente."', 3, 'COL', '2257', '0', '0', NULL)";
                        $list= Yii::app()->db->createCommand($SQL)->query();
                        //Asignar licencia y exámenes. Descontar licencia al cliente
                        //Asociar la licencia al usuario
                        Yii::import('application.modules.Clients.models.LicensesUser');
                        Yii::import('application.modules.Clients.models.ExamsUser');
                        $modLicUsu = new LicensesUser();
                        $modLicUsu->id_licencia_cliente = $modlicencia_cliente->id_licencias_cliente;
                        $modLicUsu->id_usuario=$modUsu->id_usuario_c;
                        $modLicUsu->fecha_asignacion = new CDbExpression('NOW()');
                        $modLicUsu->fecha_presentacion = new CDbExpression('DATE_SUB(now(), INTERVAL 1 HOUR)');
                        $modLicUsu->hora =  '10:00';
                        $modLicUsu->estado="A";
                        $modLicUsu->save();
                        //Gastar la licencia
                        $modUtil->gastarLicencia($modLicUsu->id_licencia_cliente);
                        //Asignar los exámenes al usuario
                        $modExamenes = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>18));
                        foreach($modExamenes as $examen){
                            $modExamenUsuario = new ExamsUser();
                            $modExamenUsuario->id_examen = $examen->id_examen;
                            $modExamenUsuario->id_licencia_usuario = $modLicUsu->id_licencias_usuario;
                            $modExamenUsuario->fecha_presentacion = $modLicUsu->fecha_presentacion;
                            $modExamenUsuario->hora = '10:00';
                            $modExamenUsuario->salon = 224;
                            $modExamenUsuario->estado = "A";
                            $modExamenUsuario->save();
                        }
                        $hash_ede = base64_encode('8903990166&'.$_POST['id_usuario_c'].'&'.$_POST['id_usuario_c']);
                        
                        $this->redirect('/ilto3/index.php?r=Clients/default/login&hash_ede='.$hash_ede);
                    } else {
                      $this->redirect('/ilto3/tecs_udv.php?err=2'); 
                    }
                } else {
                  $this->redirect('/ilto3/tecs_udv.php?err=2'); 
                }
            }
        }
        
    // TECS LINK JUAN CORPAS
    public function actionUcorpas(){
            
            
            $modUsu = UsersC::model()->findByAttributes(array('id_usuario_c'=>$_POST['id_usuario_c']));
            
            if(count($modUsu)>0){
                /*if($modUsu->id_cliente!='89039901661'){  PARA UDLV no se hacen transferencias en esta modalidad
                    $this->redirect('/ilto3/tecs_udv.php?err=4');    
                }*/
                $this->redirect('/ilto3/ede_.php?err=3'); 
            } else {
                
                //Registro en el area andina
                $modlicencia_cliente = LicensesClient::model()->disponibles()->findByAttributes(array('id_licencia'=>15,'estado'=>'A', 'id_cliente'=>'8600383741'));
                
                
                //Si hay licencias disponibles continuar
                $sql='SELECT SUM(cantidad - utilizados) as total FROM `licenses_client` WHERE `id_cliente` LIKE "8600383741" AND id_licencia = 15';
                $list= Yii::app()->db->createCommand($sql)->query();
                
                if($modlicencia_cliente){
                    $modUtil = new Utilidades();
                    $modArea = new EdeAreandina();
                    $modArea->email = $_POST['email'];
                    $modArea->firstName = $_POST['firstName'];
                    $modArea->lastName = $_POST['lastName'];
                  
                    if($modArea->save()){
                        //Registro unicamente en usuarios
                        $modUsu = new UsersC;
                        $modUsu->id_cliente = '8600383741';
                        $modUsu->email = $_POST['email'];
                        $modUsu->nombres = $_POST['firstName'];
                        $modUsu->apellidos = $_POST['lastName'];
                        $modUsu->id_grupo_cliente = $_POST['group'];
                        $modUsu->id_perfil=6;
                        $modUsu->estado='A';
                        $modUsu->numero_id=$_POST['id_usuario_c'];
                        $modUsu->id_usuario_c =$_POST['id_usuario_c'];
                        $modUsu->clave = md5($_POST['id_usuario_c']);
                        $modUsu->clave2 = $_POST['id_usuario_c'];
                        $modUsu->id_pais=3;
                        $modUsu->id_ciudad=2;
                        //$modUsu->save();
                        
                        $SQL = "INSERT INTO `users_c` (`id_usuario_c`, `nombres`, `apellidos`, `email`, `id_cliente`, `ultimo_acceso`, `id_perfil`, `numero_id`, `ruta_foto`, `estado`, `clave`, `clave2`, `fecha_creado`, `id_grupo_cliente`, `id_pais`, `id_country`, `id_ciudad`, `email_certificate`, `isDemo`, `reference`) 
                        VALUES ('".$modUsu->numero_id."', '".$modUsu->nombres."', '".$modUsu->apellidos."', '".$modUsu->email."',  '8600383741', NOW(), '".$modUsu->id_perfil."', '".$modUsu->numero_id."', '8600383741_1462894687.jpg', 'A', '".$modUsu->clave."', '".$modUsu->clave2."', NOW(), '".$modUsu->id_grupo_cliente."', 3, 'COL', '2257', '0', '0', NULL)";
                        $list= Yii::app()->db->createCommand($SQL)->query();
                        //Asignar licencia y exámenes. Descontar licencia al cliente
                        //Asociar la licencia al usuario
                        Yii::import('application.modules.Clients.models.LicensesUser');
                        Yii::import('application.modules.Clients.models.ExamsUser');
                        $modLicUsu = new LicensesUser();
                        $modLicUsu->id_licencia_cliente = $modlicencia_cliente->id_licencias_cliente;
                        $modLicUsu->id_usuario=$modUsu->id_usuario_c;
                        $modLicUsu->fecha_asignacion = new CDbExpression('NOW()');
                        $modLicUsu->fecha_presentacion = new CDbExpression('NOW()');
                        $modLicUsu->hora =  '10:00';
                        $modLicUsu->estado="A";
                        $modLicUsu->save();
                        //Gastar la licencia
                        $modUtil->gastarLicencia($modLicUsu->id_licencia_cliente);
                        //Asignar los exámenes al usuario
                        $modExamenes = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>15));
                        foreach($modExamenes as $examen){
                            $modExamenUsuario = new ExamsUser();
                            $modExamenUsuario->id_examen = $examen->id_examen;
                            $modExamenUsuario->id_licencia_usuario = $modLicUsu->id_licencias_usuario;
                            $modExamenUsuario->fecha_presentacion = $modLicUsu->fecha_presentacion;
                            $modExamenUsuario->hora = '10:00';
                            $modExamenUsuario->salon = 138;
                            $modExamenUsuario->estado = "A";
                            if($examen->id_examen=='23'){
                                $modExamenUsuario->estado = "F";
                                $modExamenUsuario->calificacion = "0.0";
                            }
                            $modExamenUsuario->save();
                        }
                        
                        $hash_ede = base64_encode('8903990166&'.$_POST['id_usuario_c'].'&'.$_POST['id_usuario_c']);
                        
                        $this->redirect('/ilto3/index.php?r=Clients/default/login&hash_ede='.$hash_ede);
                    } else {
                      $this->redirect('/ilto3/tecs_corpas.php?err=2'); 
                    }
                } else {
                  $this->redirect('/ilto3/tecs_corpas.php?err=2'); 
                }
            }
        }
        
    public function actionUvpmx(){
            $modUsu = UsersC::model()->findByAttributes(array('id_usuario_c'=>$_POST['id_usuario_c']));
            
            if(empty($_POST["program"])){
                $this->redirect('/ilto3/ede_uvpmx.php?err=6'); 
            }
            
            $id_school = 8;
            
            //REVISAMOS SI EL USUARIO EXISTE Y TIENE LICENCIAS
            if(!is_null($modUsu)>0){
                // VERIFICAMOS QUE LICENCIAS TIENE PENDIENTES
                $SQL='SELECT id_licencias_usuario, id_usuario, id_licencia_cliente, id_licencia, licenses_user.fecha_presentacion, licenses_user.estado FROM `licenses_user`
                INNER JOIN `licenses_client`
                ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente
                WHERE `id_usuario` = "'.$_POST['id_usuario_c'].'" AND licenses_user.estado != "F"
                AND licenses_client.id_licencia = 27
                ORDER BY licenses_user.fecha_presentacion DESC';
                $list= Yii::app()->db->createCommand($SQL)->queryAll();
                
                //dejar grupo por Defecto
                $sql='UPDATE `users_c` SET `id_cliente`="81030525", `id_grupo_cliente`="614" WHERE `id_usuario_c` = "'.$_POST['id_usuario_c'].'"';
                $upd= Yii::app()->db->createCommand($sql)->query();
                
                //si la lista de licencias pendientes está vacía seguimos
                if(empty($list)){
                   
                   //pregunto si tiene alguna licencia ede presentada
                    $SQL='SELECT * FROM `licenses_user`
                    INNER JOIN `licenses_client`
                    ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente
                    WHERE `id_usuario` = "'.$_POST['id_usuario_c'].'" AND licenses_user.estado = "F"
                    AND licenses_client.id_licencia = 27
                    ORDER BY licenses_user.fecha_presentacion DESC LIMIT 1';
                    $list= Yii::app()->db->createCommand($SQL)->queryAll();
                   
                    //revisar si hay una licencia al menos
                    if(!is_null($list)>0){
                        //estimamos la diferencia de fechas para obtener los dias
                        $datetime1 = new Datetime($list[0]["fecha_presentacion"]);
                        $getNow = date("Y-m-d H:i:s");
                    	$today = new Datetime($getNow);
                    	$interval = date_diff($datetime1, $today);
                    	//obtenemos el valor en numero
                    	$days = explode("+",$interval->format('%R%a'));
                        //si lleva presentada la licencia menos de 10 días, no asignamos y mandamos error
                    	if($days[1]<10){
                    	    $ban_to_date =  date('Y-m-d H:i:s', strtotime("+10 days", strtotime($list[0]["fecha_presentacion"])));
                            //$this->redirect('/ilto3/ede_uvpmx.php?err=5&dat='.$ban_to_date.''); 
                        }
                    }
                    
                    // asignemos licencia si la lista de licencias pendientes está vacia
                    $modlicencia_cliente = LicensesClient::model()->disponibles()->findByAttributes(array('id_licencia'=>27,'estado'=>'A', 'id_cliente'=>'81030525'));
                    //Si hay licencias disponibles continuar
                    $sql='SELECT SUM(cantidad - utilizados) as total FROM `licenses_client` WHERE `id_cliente` LIKE "81030525" AND id_licencia = 27';
                    $list= Yii::app()->db->createCommand($sql)->query();
                       
                    
                    if($modlicencia_cliente){
                        $modUtil = new Utilidades();
                        $modArea = new EdeAreandina();
                        $modArea->email = $_POST['email'];
                        $modArea->firstName = $_POST['firstName'];
                        $modArea->lastName = $_POST['lastName'];
                      
                        if($modArea->save()){
                            //Registro unicamente en usuarios
                            $modUsu = new UsersC;
                            $modUsu->id_cliente = '81030525';
                            $modUsu->email = $_POST['email'];
                            $modUsu->nombres = $_POST['firstName'];
                            $modUsu->apellidos = $_POST['lastName'];
                            $modUsu->id_grupo_cliente = '614';
                            $modUsu->id_perfil=6;
                            $modUsu->estado='A';
                            $modUsu->numero_id=$_POST['id_usuario_c'];
                            $modUsu->id_usuario_c =$_POST['id_usuario_c'];
                            $modUsu->clave = md5($_POST['id_usuario_c']);
                            $modUsu->clave2 = $_POST['id_usuario_c'];
                            $modUsu->id_pais=3;
                            $modUsu->id_ciudad=2;
                            //$modUsu->save();
                            
                            //Asignar licencia y exámenes. Descontar licencia al cliente
                            //Asociar la licencia al usuario
                            Yii::import('application.modules.Clients.models.LicensesUser');
                            Yii::import('application.modules.Clients.models.ExamsUser');
                            $modLicUsu = new LicensesUser();
                            $modLicUsu->id_licencia_cliente = $modlicencia_cliente->id_licencias_cliente;
                            $modLicUsu->id_usuario=$modUsu->id_usuario_c;
                            $modLicUsu->fecha_asignacion = new CDbExpression('NOW()');
                            $modLicUsu->fecha_presentacion = new CDbExpression('DATE_SUB(now(), INTERVAL 1 HOUR)');
                            $modLicUsu->hora =  '10:00';
                            $modLicUsu->estado="A";
                            $modLicUsu->save();
                            
                            
                            
                            //Consultamos la carrera y ciudad donde actualmente esta
                            $SQL="SELECT id_city, id_program FROM `users_c_period` WHERE `id_usuario_c` = '".$modUsu->id_usuario_c."'";
                            $listUsp= Yii::app()->db->createCommand($SQL)->queryAll();
                            
                            $id_city = $listUsp[0]["id_city"];
                            $id_program = $listUsp[0]["id_program"];
                            
                            
                            if(empty($listUsp)){
                                $id_city = $_POST["city"];
                                $id_program = $_POST["program"];
                            }
                            
                            
                            // Guardamos el registro en la tabla de period
                            $SQL = "INSERT INTO `users_c_period`(`id_users_c_period`, `id_usuario_c`, `id_cliente`, `period`, `id_city`, `id_school`, `id_program`, `id_licencias_usuario`) 
                            VALUES ('', '".$modUsu->id_usuario_c."', '81030525', '2', '".$id_city."', '".$id_school."', '".$id_program."', '".$modLicUsu->id_licencias_usuario."')";
                            
                            $list= Yii::app()->db->createCommand($SQL)->query();
                            
                            //obtenemos la contraseña del usuario
                            $sql="SELECT clave2 FROM `users_c` WHERE `id_usuario_c` = '".$_POST["id_usuario_c"]."' ";
                            $pass= Yii::app()->db->createCommand($sql)->queryAll();
                            
                            //Gastar la licencia
                            $modUtil->gastarLicencia($modLicUsu->id_licencia_cliente);
                            //Asignar los exámenes al usuario
                            $modExamenes = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>27));
                            foreach($modExamenes as $examen){
                                $modExamenUsuario = new ExamsUser();
                                $modExamenUsuario->id_examen = $examen->id_examen;
                                $modExamenUsuario->id_licencia_usuario = $modLicUsu->id_licencias_usuario;
                                $modExamenUsuario->fecha_presentacion = $modLicUsu->fecha_presentacion;
                                $modExamenUsuario->hora = '10:00';
                                $modExamenUsuario->salon = 273;
                                $modExamenUsuario->estado = "A";
                                $modExamenUsuario->save();
                            }
                            $hash_ede = base64_encode('81030525&'.$_POST['id_usuario_c'].'&'.$pass[0]['clave2']);
                            
                            $this->redirect('/ilto3/index.php?r=Clients/default/login&hash_ede='.$hash_ede);
                        } else {
                          $this->redirect('/ilto3/ede_uvpmx.php?err=2'); 
                        }
                    }
                
                }else{
                    
                    //cómo hay licencias pendientes
                    foreach ($list as $license) {
                        $this->redirect('index.php?r=Clients/default/login&type=3&message=3');
                        
                    }
                }
            } else {
                
                //Registro en el area andina
                $modlicencia_cliente = LicensesClient::model()->disponibles()->findByAttributes(array('id_licencia'=>27,'estado'=>'A', 'id_cliente'=>'81030525'));
                //Si hay licencias disponibles continuar
                $sql='SELECT SUM(cantidad - utilizados) as total FROM `licenses_client` WHERE `id_cliente` LIKE "81030525" AND id_licencia = 27';
                $list= Yii::app()->db->createCommand($sql)->query();
                
                // Guardamos por defecto un grupo de la FUAA
                $sql='SELECT SUM(cantidad - utilizados) as total FROM `licenses_client` WHERE `id_cliente` LIKE "81030525" AND id_licencia = 27';
                $list= Yii::app()->db->createCommand($sql)->query();
                
                if($modlicencia_cliente){
                    $modUtil = new Utilidades();
                    $modArea = new EdeAreandina();
                    $modArea->email = $_POST['email'];
                    $modArea->firstName = $_POST['firstName'];
                    $modArea->lastName = $_POST['lastName'];
                  
                    if($modArea->save()){
                        //Registro unicamente en usuarios
                        $modUsu = new UsersC;
                        $modUsu->id_cliente = '81030525';
                        $modUsu->email = $_POST['email'];
                        $modUsu->nombres = $_POST['firstName'];
                        $modUsu->apellidos = $_POST['lastName'];
                        $modUsu->id_grupo_cliente = '614';
                        $modUsu->id_perfil=6;
                        $modUsu->estado='A';
                        $modUsu->numero_id=$_POST['id_usuario_c'];
                        $modUsu->id_usuario_c =$_POST['id_usuario_c'];
                        $modUsu->clave = md5($_POST['id_usuario_c']);
                        $modUsu->clave2 = $_POST['id_usuario_c'];
                        $modUsu->id_pais=3;
                        $modUsu->id_ciudad=2;
                        //$modUsu->save();
                        
                        $SQL = "INSERT INTO `users_c` (`id_usuario_c`, `nombres`, `apellidos`, `email`, `id_cliente`, `ultimo_acceso`, `id_perfil`, `numero_id`, `ruta_foto`, `estado`, `clave`, `clave2`, `fecha_creado`, `id_grupo_cliente`, `id_pais`, `id_country`, `id_ciudad`, `email_certificate`, `isDemo`, `reference`) 
                        VALUES ('".$modUsu->numero_id."', '".$modUsu->nombres."', '".$modUsu->apellidos."', '".$modUsu->email."',  '81030525', NOW(), '".$modUsu->id_perfil."', '".$modUsu->numero_id."', '', 'A', '".$modUsu->clave."', '".$modUsu->clave2."', NOW(), '".$modUsu->id_grupo_cliente."', 3, 'MEX', '".$_POST["city"]."', '0', '0', NULL)";
                        $list= Yii::app()->db->createCommand($SQL)->query();
                        //Asignar licencia y exámenes. Descontar licencia al cliente
                        //Asociar la licencia al usuario
                        Yii::import('application.modules.Clients.models.LicensesUser');
                        Yii::import('application.modules.Clients.models.ExamsUser');
                        $modLicUsu = new LicensesUser();
                        $modLicUsu->id_licencia_cliente = $modlicencia_cliente->id_licencias_cliente;
                        $modLicUsu->id_usuario=$modUsu->id_usuario_c;
                        $modLicUsu->fecha_asignacion = new CDbExpression('NOW()');
                        $modLicUsu->fecha_presentacion = new CDbExpression('DATE_SUB(now(), INTERVAL 1 HOUR)');
                        $modLicUsu->hora =  '10:00';
                        $modLicUsu->estado="A";
                        $modLicUsu->save();
                        //Gastar la licencia
                        
                        // Guardamos el registro en la tabla de period
                        $SQL = "INSERT INTO `users_c_period`(`id_users_c_period`, `id_usuario_c`, `id_cliente`, `period`, `id_city`, `id_school`, `id_program`, `id_licencias_usuario`) 
                        VALUES ('', '".$modUsu->id_usuario_c."', '81030525', '1', '".$_POST["city"]."', '".$id_school."', '".$_POST["program"]."', '".$modLicUsu->id_licencias_usuario."')";
                        $list= Yii::app()->db->createCommand($SQL)->query();
                        $modUtil->gastarLicencia($modLicUsu->id_licencia_cliente);
                        //Asignar los exámenes al usuario
                        $modExamenes = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>27));
                        foreach($modExamenes as $examen){
                            $modExamenUsuario = new ExamsUser();
                            $modExamenUsuario->id_examen = $examen->id_examen;
                            $modExamenUsuario->id_licencia_usuario = $modLicUsu->id_licencias_usuario;
                            $modExamenUsuario->fecha_presentacion = $modLicUsu->fecha_presentacion;
                            $modExamenUsuario->hora = '10:00';
                            $modExamenUsuario->salon = 273;
                            $modExamenUsuario->estado = "A";
                            $modExamenUsuario->save();
                        }
                        $hash_ede = base64_encode('81030525&'.$_POST['id_usuario_c'].'&'.$_POST['id_usuario_c']);
                        
                        $this->redirect('/ilto3/index.php?r=Clients/default/login&hash_ede='.$hash_ede);
                    } else {
                      $this->redirect('/ilto3/ede_uvpmx.php?err=2'); 
                    }
                } else {
                  $this->redirect('/ilto3/ede_uvpmx.php?err=2'); 
                }
            }
        }
        
    public function actionUtp(){
        $modUsu = UsersC::model()->findByAttributes(array('id_usuario_c'=>$_POST['id_usuario_c']));
        
        //REVISAMOS SI EL USUARIO EXISTE Y TIENE LICENCIAS
        if(!is_null($modUsu)>0){
            // VERIFICAMOS QUE LICENCIAS TIENE PENDIENTES
            $SQL='SELECT id_licencias_usuario, id_usuario, id_licencia_cliente, id_licencia, licenses_user.fecha_presentacion, licenses_user.estado FROM `licenses_user`
            INNER JOIN `licenses_client`
            ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente
            WHERE `id_usuario` = "'.$_POST['id_usuario_c'].'" AND licenses_user.estado != "F"
            AND licenses_client.id_licencia = 27
            ORDER BY licenses_user.fecha_presentacion DESC';
            $list= Yii::app()->db->createCommand($SQL)->queryAll();
                
            $sql='UPDATE `users_c` SET `id_cliente`="3007000211248DV50", `id_grupo_cliente`="'.$_POST['group'].'" WHERE `id_usuario_c` = "'.$_POST['id_usuario_c'].'"';
            $upd= Yii::app()->db->createCommand($sql)->query();
                
            //si la lista de licencias pendientes está vacía seguimos
            if(empty($list)){
                //pregunto si tiene alguna licencia ede presentada
                $SQL='SELECT * FROM `licenses_user`
                INNER JOIN `licenses_client`
                ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente
                WHERE `id_usuario` = "'.$_POST['id_usuario_c'].'" AND licenses_user.estado = "F"
                AND licenses_client.id_licencia = 27
                ORDER BY licenses_user.fecha_presentacion DESC LIMIT 1';
                $list= Yii::app()->db->createCommand($SQL)->queryAll();
                   
                //revisar si hay una licencia al menos
                if(!is_null($list)>0){
                    //estimamos la diferencia de fechas para obtener los dias
                    $datetime1 = new Datetime($list[0]["fecha_presentacion"]);
                    $getNow = date("Y-m-d H:i:s");
                    $today = new Datetime($getNow);
                    $interval = date_diff($datetime1, $today);
                    //obtenemos el valor en numero
                    $days = explode("+",$interval->format('%R%a'));
                    //si lleva presentada la licencia menos de 10 días, no asignamos y mandamos error
                    if($days[1]<10){
                        $ban_to_date =  date('Y-m-d H:i:s', strtotime("+10 days", strtotime($list[0]["fecha_presentacion"])));
                        //$this->redirect('/ilto3/ede_areandina.php?err=5&dat='.$ban_to_date.''); 
                    }
                }
                    
                // asignemos licencia si la lista de licencias pendientes está vacia
                $modlicencia_cliente = LicensesClient::model()->disponibles()->findByAttributes(array('id_licencia'=>27,'estado'=>'A', 'id_cliente'=>'3007000211248DV50'));
                //Si hay licencias disponibles continuar
                $sql='SELECT SUM(cantidad - utilizados) as total FROM `licenses_client` WHERE `id_cliente` = "3007000211248DV50" AND id_licencia = 27';
                $list= Yii::app()->db->createCommand($sql)->query();
                       
                    
                 if($modlicencia_cliente){
                    $modUtil = new Utilidades();
                    $modArea = new EdeAreandina();
                    $modArea->email = $_POST['email'];
                    $modArea->firstName = $_POST['firstName'];
                    $modArea->lastName = $_POST['lastName'];
                      
                    if($modArea->save()){
                        //Registro unicamente en usuarios
                        $modUsu = new UsersC;
                        $modUsu->id_cliente = '3007000211248DV50';
                        $modUsu->email = $_POST['email'];
                        $modUsu->nombres = $_POST['firstName'];
                        $modUsu->apellidos = $_POST['lastName'];
                        $modUsu->id_grupo_cliente = $_POST['group'];
                        $modUsu->id_perfil=6;
                        $modUsu->estado='A';
                        $modUsu->numero_id=$_POST['id_usuario_c'];
                        $modUsu->id_usuario_c =$_POST['id_usuario_c'];
                        $modUsu->clave = md5($_POST['id_usuario_c']);
                        $modUsu->clave2 = $_POST['id_usuario_c'];
                        $modUsu->id_pais=3;
                        $modUsu->id_ciudad=2;
                        //$modUsu->save();
                            
                        //Asignar licencia y exámenes. Descontar licencia al cliente
                        //Asociar la licencia al usuario
                        Yii::import('application.modules.Clients.models.LicensesUser');
                        Yii::import('application.modules.Clients.models.ExamsUser');
                        $modLicUsu = new LicensesUser();
                        $modLicUsu->id_licencia_cliente = $modlicencia_cliente->id_licencias_cliente;
                        $modLicUsu->id_usuario=$modUsu->id_usuario_c;
                        $modLicUsu->fecha_asignacion = new CDbExpression('NOW()');
                        $modLicUsu->fecha_presentacion = new CDbExpression('DATE_SUB(now(), INTERVAL 1 HOUR)');
                        $modLicUsu->hora =  '06:00';
                        $modLicUsu->estado="A";
                        $modLicUsu->save();
                            
                            
                        // Guardamos el registro en la tabla de period
                        $SQL = "INSERT INTO `users_c_period`(`id_users_c_period`, `id_usuario_c`, `id_cliente`, `period`, `id_city`, `id_school`, `id_program`, `id_licencias_usuario`, `id_category`, `id_roll`) 
                        VALUES ('', '".$modUsu->id_usuario_c."', '3007000211248DV50', '0', '".$_POST['city']."', '0', '0', '".$modLicUsu->id_licencias_usuario."', '0', '0')";
                        $list= Yii::app()->db->createCommand($SQL)->query();
                            
                            //obtenemos la contraseña del usuario
                            $sql="SELECT clave2 FROM `users_c` WHERE `id_usuario_c` = '".$_POST["id_usuario_c"]."' ";
                            $pass= Yii::app()->db->createCommand($sql)->queryAll();
                            
                            //Gastar la licencia
                            $modUtil->gastarLicencia($modLicUsu->id_licencia_cliente);
                            //Asignar los exámenes al usuario
                            $modExamenes = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>27));
                            foreach($modExamenes as $examen){
                                $modExamenUsuario = new ExamsUser();
                                $modExamenUsuario->id_examen = $examen->id_examen;
                                $modExamenUsuario->id_licencia_usuario = $modLicUsu->id_licencias_usuario;
                                $modExamenUsuario->fecha_presentacion = $modLicUsu->fecha_presentacion;
                                $modExamenUsuario->hora = '06:00';
                                $modExamenUsuario->salon = $_POST['room'];
                                $modExamenUsuario->estado = "A";
                                $modExamenUsuario->save();
                            }
                            $hash_ede = base64_encode('3007000211248DV50&'.$_POST['id_usuario_c'].'&'.$pass[0]['clave2']);
                            
                            $this->redirect('/ilto3/index.php?r=Clients/default/login&hash_ede='.$hash_ede);
                        } else {
                          $this->redirect('/ilto3/ede_utp.php?err=2'); 
                        }
                    }
                
                }else{
                    
                    //cómo hay licencias pendientes
                    foreach ($list as $license) {
                        $this->redirect('index.php?r=Clients/default/login&type=3&message=3');
                        
                    }
                }
            } else {
                
                //Registro en el sistema
                $modlicencia_cliente = LicensesClient::model()->disponibles()->findByAttributes(array('id_licencia'=>27,'estado'=>'A', 'id_cliente'=>'3007000211248DV50'));
                //Si hay licencias disponibles continuar
                $sql='SELECT SUM(cantidad - utilizados) as total FROM `licenses_client` WHERE `id_cliente` = "3007000211248DV50" AND id_licencia = 27';
                $list= Yii::app()->db->createCommand($sql)->query();
                
                if($modlicencia_cliente){
                    $modUtil = new Utilidades();
                    $modArea = new EdeAreandina();
                    $modArea->email = $_POST['email'];
                    $modArea->firstName = $_POST['firstName'];
                    $modArea->lastName = $_POST['lastName'];
                  
                    if($modArea->save()){
                        //Registro unicamente en usuarios
                        $modUsu = new UsersC;
                        $modUsu->id_cliente = '3007000211248DV50';
                        $modUsu->email = $_POST['email'];
                        $modUsu->nombres = $_POST['firstName'];
                        $modUsu->apellidos = $_POST['lastName'];
                        $modUsu->id_perfil=6;
                        $modUsu->estado='A';
                        $modUsu->numero_id=$_POST['id_usuario_c'];
                        $modUsu->id_usuario_c =$_POST['id_usuario_c'];
                        $modUsu->clave = md5($_POST['id_usuario_c']);
                        $modUsu->clave2 = $_POST['id_usuario_c'];
                        $modUsu->id_pais=3;
                        $modUsu->id_ciudad=2;
                        $modUsu->id_grupo_cliente = $_POST['group'];
                        
                        //$modUsu->save();
                         
                        $SQL = "INSERT INTO `users_c` (`id_usuario_c`, `nombres`, `apellidos`, `email`, `id_cliente`, `ultimo_acceso`, `id_perfil`, `numero_id`, `ruta_foto`, `estado`, `clave`, `clave2`, `fecha_creado`, `id_grupo_cliente`, `id_pais`, `id_country`, `id_ciudad`, `email_certificate`, `isDemo`, `reference`) 
                        VALUES ('".$modUsu->numero_id."', '".$modUsu->nombres."', '".$modUsu->apellidos."', '".$modUsu->email."',  '3007000211248DV50', NOW(), '".$modUsu->id_perfil."', '".$modUsu->numero_id."', '', 'A', '".$modUsu->clave."', '".$modUsu->clave2."', NOW(), '".$modUsu->id_grupo_cliente."', 3, 'PAN', '".$_POST['city']."', '1', '0', NULL)";
                        $list= Yii::app()->db->createCommand($SQL)->query();
                        //Asignar licencia y exámenes. Descontar licencia al cliente
                        //Asociar la licencia al usuario
                        Yii::import('application.modules.Clients.models.LicensesUser');
                        Yii::import('application.modules.Clients.models.ExamsUser');
                        $modLicUsu = new LicensesUser();
                        $modLicUsu->id_licencia_cliente = $modlicencia_cliente->id_licencias_cliente;
                        $modLicUsu->id_usuario=$modUsu->id_usuario_c;
                        $modLicUsu->fecha_asignacion = new CDbExpression('NOW()');
                        $modLicUsu->fecha_presentacion = new CDbExpression('DATE_SUB(now(), INTERVAL 1 HOUR)');
                        $modLicUsu->hora =  '06:00';
                        $modLicUsu->estado="A";
                        $modLicUsu->save();
                        //Gastar la licencia
                        
                        // Guardamos el registro en la tabla de period
                        // Guardamos el registro en la tabla de period
                        $SQL = "INSERT INTO `users_c_period`(`id_users_c_period`, `id_usuario_c`, `id_cliente`, `period`, `id_city`, `id_school`, `id_program`, `id_licencias_usuario`, `id_category`, `id_roll`) 
                        VALUES ('', '".$modUsu->id_usuario_c."', '3007000211248DV50', '0', '".$_POST['city']."', '0', '0', '".$modLicUsu->id_licencias_usuario."', '0', '0')";
                        $list= Yii::app()->db->createCommand($SQL)->query();
                        $modUtil->gastarLicencia($modLicUsu->id_licencia_cliente);
                        //Asignar los exámenes al usuario
                        $modExamenes = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>27));
                        foreach($modExamenes as $examen){
                            $modExamenUsuario = new ExamsUser();
                            $modExamenUsuario->id_examen = $examen->id_examen;
                            $modExamenUsuario->id_licencia_usuario = $modLicUsu->id_licencias_usuario;
                            $modExamenUsuario->fecha_presentacion = $modLicUsu->fecha_presentacion;
                            $modExamenUsuario->hora = '06:00';
                            $modExamenUsuario->salon = $_POST['room'];
                            $modExamenUsuario->estado = "A";
                            $modExamenUsuario->save();
                        }
                        $hash_ede = base64_encode('3007000211248DV50&'.$_POST['id_usuario_c'].'&'.$_POST['id_usuario_c']);
                        
                        $this->redirect('/ilto3/index.php?r=Clients/default/login&hash_ede='.$hash_ede);
                    } else {
                      $this->redirect('/ilto3/ede_utp.php?err=2'); 
                    }
                } else {
                  $this->redirect('/ilto3/ede_utp.php?err=2'); 
                }
            }    
        
    }
    
    public function actionUniminuto(){
        
        $modUsu = UsersC::model()->findByAttributes(array('id_usuario_c'=>$_POST['id_usuario_c']));
        
        //REVISAMOS SI EL USUARIO EXISTE Y TIENE LICENCIAS
        if(!is_null($modUsu)>0){
                // VERIFICAMOS QUE LICENCIAS TIENE PENDIENTES
                $SQL='SELECT id_licencias_usuario, id_usuario, id_licencia_cliente, id_licencia, licenses_user.fecha_presentacion, licenses_user.estado FROM `licenses_user`
                INNER JOIN `licenses_client`
                ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente
                WHERE `id_usuario` = "'.$_POST['id_usuario_c'].'" AND licenses_user.estado != "F"
                AND licenses_client.id_licencia = 27
                ORDER BY licenses_user.fecha_presentacion DESC';
                $list= Yii::app()->db->createCommand($SQL)->queryAll();
                
                $sql='UPDATE `users_c` SET `id_cliente`="8001162172", `id_grupo_cliente`="'.$_POST['group'].'" WHERE `id_usuario_c` = "'.$_POST['id_usuario_c'].'"';
                $upd= Yii::app()->db->createCommand($sql)->query();
                
                //si la lista de licencias pendientes está vacía seguimos
                if(empty($list)){
                   
                   //pregunto si tiene alguna licencia ede presentada
                    $SQL='SELECT * FROM `licenses_user`
                    INNER JOIN `licenses_client`
                    ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente
                    WHERE `id_usuario` = "'.$_POST['id_usuario_c'].'" AND licenses_user.estado = "F"
                    AND licenses_client.id_licencia = 27
                    ORDER BY licenses_user.fecha_presentacion DESC LIMIT 1';
                    $list= Yii::app()->db->createCommand($SQL)->queryAll();
                   
                    //revisar si hay una licencia al menos
                    if(!is_null($list)>0){
                        //estimamos la diferencia de fechas para obtener los dias
                        $datetime1 = new Datetime($list[0]["fecha_presentacion"]);
                        $getNow = date("Y-m-d H:i:s");
                    	$today = new Datetime($getNow);
                    	$interval = date_diff($datetime1, $today);
                    	//obtenemos el valor en numero
                    	$days = explode("+",$interval->format('%R%a'));
                        //si lleva presentada la licencia menos de 10 días, no asignamos y mandamos error
                    	if($days[1]<10){
                    	    $ban_to_date =  date('Y-m-d H:i:s', strtotime("+10 days", strtotime($list[0]["fecha_presentacion"])));
                            //$this->redirect('/ilto3/ede_areandina.php?err=5&dat='.$ban_to_date.''); 
                        }
                    }
                    
                    // asignemos licencia si la lista de licencias pendientes está vacia
                    $modlicencia_cliente = LicensesClient::model()->disponibles()->findByAttributes(array('id_licencia'=>27,'estado'=>'A', 'id_cliente'=>'8001162172'));
                    //Si hay licencias disponibles continuar
                    $sql='SELECT SUM(cantidad - utilizados) as total FROM `licenses_client` WHERE `id_cliente` = "8001162172" AND id_licencia = 27';
                    $list= Yii::app()->db->createCommand($sql)->query();
                       
                    
                    if($modlicencia_cliente){
                        $modUtil = new Utilidades();
                        $modArea = new EdeAreandina();
                        $modArea->email = $_POST['email'];
                        $modArea->firstName = $_POST['firstName'];
                        $modArea->lastName = $_POST['lastName'];
                      
                        if($modArea->save()){
                            //Registro unicamente en usuarios
                            $modUsu = new UsersC;
                            $modUsu->id_cliente = '8001162172';
                            $modUsu->email = $_POST['email'];
                            $modUsu->nombres = $_POST['firstName'];
                            $modUsu->apellidos = $_POST['lastName'];
                            $modUsu->id_grupo_cliente = $_POST['group'];
                            $modUsu->id_perfil=6;
                            $modUsu->estado='A';
                            $modUsu->numero_id=$_POST['id_usuario_c'];
                            $modUsu->id_usuario_c =$_POST['id_usuario_c'];
                            $modUsu->clave = md5($_POST['id_usuario_c']);
                            $modUsu->clave2 = $_POST['id_usuario_c'];
                            $modUsu->id_pais=3;
                            $modUsu->id_ciudad=2;
                            //$modUsu->save();
                            
                            //Asignar licencia y exámenes. Descontar licencia al cliente
                            //Asociar la licencia al usuario
                            Yii::import('application.modules.Clients.models.LicensesUser');
                            Yii::import('application.modules.Clients.models.ExamsUser');
                            $modLicUsu = new LicensesUser();
                            $modLicUsu->id_licencia_cliente = $modlicencia_cliente->id_licencias_cliente;
                            $modLicUsu->id_usuario=$modUsu->id_usuario_c;
                            $modLicUsu->fecha_asignacion = new CDbExpression('NOW()');
                            $modLicUsu->fecha_presentacion = new CDbExpression('DATE_SUB(now(), INTERVAL 1 HOUR)');
                            $modLicUsu->hora =  '06:00';
                            $modLicUsu->estado="A";
                            $modLicUsu->save();
                            
                            
                            
                            // Guardamos el registro en la tabla de period
                            $SQL = "INSERT INTO `users_c_period`(`id_users_c_period`, `id_usuario_c`, `id_cliente`, `period`, `id_city`, `id_school`, `id_program`, `id_licencias_usuario`, `id_category`, `id_roll`) 
                            VALUES ('', '".$modUsu->id_usuario_c."', '8001162172', '0', 2259, '0', '0', '".$modLicUsu->id_licencias_usuario."', '0', '0')";
                            $list= Yii::app()->db->createCommand($SQL)->query();
                            
                            //obtenemos la contraseña del usuario
                            $sql="SELECT clave2 FROM `users_c` WHERE `id_usuario_c` = '".$_POST["id_usuario_c"]."' ";
                            $pass= Yii::app()->db->createCommand($sql)->queryAll();
                            
                            //Gastar la licencia
                            $modUtil->gastarLicencia($modLicUsu->id_licencia_cliente);
                            //Asignar los exámenes al usuario
                            $modExamenes = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>27));
                            foreach($modExamenes as $examen){
                                $modExamenUsuario = new ExamsUser();
                                $modExamenUsuario->id_examen = $examen->id_examen;
                                $modExamenUsuario->id_licencia_usuario = $modLicUsu->id_licencias_usuario;
                                $modExamenUsuario->fecha_presentacion = $modLicUsu->fecha_presentacion;
                                $modExamenUsuario->hora = '06:00';
                                $modExamenUsuario->salon = $_POST['room'];
                                $modExamenUsuario->estado = "A";
                                $modExamenUsuario->save();
                            }
                            $hash_ede = base64_encode('8001162172&'.$_POST['id_usuario_c'].'&'.$pass[0]['clave2']);
                            
                            $this->redirect('/ilto3/index.php?r=Clients/default/login&hash_ede='.$hash_ede);
                        } else {
                          $this->redirect('/ilto3/ede_areandina.php?err=2'); 
                        }
                    }
                
                }else{
                    
                    //cómo hay licencias pendientes
                    foreach ($list as $license) {
                        $this->redirect('index.php?r=Clients/default/login&type=3&message=3');
                        
                    }
                }
            } else {
                
                //Registro en el sistema
                $modlicencia_cliente = LicensesClient::model()->disponibles()->findByAttributes(array('id_licencia'=>27,'estado'=>'A', 'id_cliente'=>'8001162172'));
                //Si hay licencias disponibles continuar
                $sql='SELECT SUM(cantidad - utilizados) as total FROM `licenses_client` WHERE `id_cliente` = "8001162172" AND id_licencia = 27';
                $list= Yii::app()->db->createCommand($sql)->query();
                
                if($modlicencia_cliente){
                    $modUtil = new Utilidades();
                    $modArea = new EdeAreandina();
                    $modArea->email = $_POST['email'];
                    $modArea->firstName = $_POST['firstName'];
                    $modArea->lastName = $_POST['lastName'];
                  
                    if($modArea->save()){
                        //Registro unicamente en usuarios
                        $modUsu = new UsersC;
                        $modUsu->id_cliente = '8001162172';
                        $modUsu->email = $_POST['email'];
                        $modUsu->nombres = $_POST['firstName'];
                        $modUsu->apellidos = $_POST['lastName'];
                        $modUsu->id_perfil=6;
                        $modUsu->estado='A';
                        $modUsu->numero_id=$_POST['id_usuario_c'];
                        $modUsu->id_usuario_c =$_POST['id_usuario_c'];
                        $modUsu->clave = md5($_POST['id_usuario_c']);
                        $modUsu->clave2 = $_POST['id_usuario_c'];
                        $modUsu->id_pais=3;
                        $modUsu->id_ciudad=2;
                        $modUsu->id_grupo_cliente = $_POST['group'];
                        
                        //$modUsu->save();
                         
                        $SQL = "INSERT INTO `users_c` (`id_usuario_c`, `nombres`, `apellidos`, `email`, `id_cliente`, `ultimo_acceso`, `id_perfil`, `numero_id`, `ruta_foto`, `estado`, `clave`, `clave2`, `fecha_creado`, `id_grupo_cliente`, `id_pais`, `id_country`, `id_ciudad`, `email_certificate`, `isDemo`, `reference`) 
                        VALUES ('".$modUsu->numero_id."', '".$modUsu->nombres."', '".$modUsu->apellidos."', '".$modUsu->email."',  '8001162172', NOW(), '".$modUsu->id_perfil."', '".$modUsu->numero_id."', '', 'A', '".$modUsu->clave."', '".$modUsu->clave2."', NOW(), '".$modUsu->id_grupo_cliente."', 3, 'COL', '2259', '1', '0', NULL)";
                        $list= Yii::app()->db->createCommand($SQL)->query();
                        //Asignar licencia y exámenes. Descontar licencia al cliente
                        //Asociar la licencia al usuario
                        Yii::import('application.modules.Clients.models.LicensesUser');
                        Yii::import('application.modules.Clients.models.ExamsUser');
                        $modLicUsu = new LicensesUser();
                        $modLicUsu->id_licencia_cliente = $modlicencia_cliente->id_licencias_cliente;
                        $modLicUsu->id_usuario=$modUsu->id_usuario_c;
                        $modLicUsu->fecha_asignacion = new CDbExpression('NOW()');
                        $modLicUsu->fecha_presentacion = new CDbExpression('DATE_SUB(now(), INTERVAL 1 HOUR)');
                        $modLicUsu->hora =  '06:00';
                        $modLicUsu->estado="A";
                        $modLicUsu->save();
                        //Gastar la licencia
                        
                        // Guardamos el registro en la tabla de period
                        // Guardamos el registro en la tabla de period
                        $SQL = "INSERT INTO `users_c_period`(`id_users_c_period`, `id_usuario_c`, `id_cliente`, `period`, `id_city`, `id_school`, `id_program`, `id_licencias_usuario`, `id_category`, `id_roll`) 
                        VALUES ('', '".$modUsu->id_usuario_c."', '8001162172', '0', '2259', '0', '0', '".$modLicUsu->id_licencias_usuario."', '0', '0')";
                        $list= Yii::app()->db->createCommand($SQL)->query();
                        $modUtil->gastarLicencia($modLicUsu->id_licencia_cliente);
                        //Asignar los exámenes al usuario
                        $modExamenes = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>27));
                        foreach($modExamenes as $examen){
                            $modExamenUsuario = new ExamsUser();
                            $modExamenUsuario->id_examen = $examen->id_examen;
                            $modExamenUsuario->id_licencia_usuario = $modLicUsu->id_licencias_usuario;
                            $modExamenUsuario->fecha_presentacion = $modLicUsu->fecha_presentacion;
                            $modExamenUsuario->hora = '06:00';
                            $modExamenUsuario->salon = $_POST['room'];
                            $modExamenUsuario->estado = "A";
                            $modExamenUsuario->save();
                        }
                        $hash_ede = base64_encode('8001162172&'.$_POST['id_usuario_c'].'&'.$_POST['id_usuario_c']);
                        
                        $this->redirect('/ilto3/index.php?r=Clients/default/login&hash_ede='.$hash_ede);
                    } else {
                      $this->redirect('/ilto3/ede_areandina.php?err=2'); 
                    }
                } else {
                  $this->redirect('/ilto3/ede_areandina.php?err=2'); 
                }
            }
    }//end uniminuto
    

        /**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionLicences($id)
	{   
	    
	    $model=new UsersC;
	    
	   	$this->render('licences',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new UsersC;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['UsersC']))
		{
            $modUtil = new Utilidades();
            
            
            //removemos espacios en blanco
            //$_POST = array_map("trim", $_POST['UsersC']);
			$model->attributes=$_POST['UsersC'];
			$model->id_usuario_c = $_POST['UsersC']['numero_id'];
			$clave = $_POST['UsersC']['clave'];
            $idTipo = $_POST['UsersC']['id_perfil'];
            $idUsuario = trim($_POST['UsersC']['numero_id']);
            $clave = trim($_POST['UsersC']['clave']);
            switch ($idTipo) {
                case 2:
                   $type="";
                    break;
                case 3:
                   $type="-PA";
                    break;
                case 4:
                   $type="-TU";
                    break;
                case 5:
                   $type="-RE";
                    break;
                case 6:
                     $clave = trim($idUsuario);
                    break;
                
                default:
                    // code...
                    break;
            }
            $id = $idUsuario;
            $ruta_foto=CUploadedFile::getInstance($model,'ruta_foto');
            
            $model->id_usuario_c = $id;
            
            //validamos que el username o id number no exista
            $modUsuarios = UsersC::model()->resetScope()->findAllByAttributes(array('id_usuario_c'=>$id));
            $mensaje = 'This Id Number is already on the system';
			if(count($modUsuarios)>0){
			    Yii::app()->user->setFlash('error',$mensaje);
		          $this->redirect(array('admin', $mensaje));  
    		}
    		
    		//validamos que el correo no exista
    		$modUsuarios = UsersC::model()->resetScope()->findAllByAttributes(array('email'=>$_POST['UsersC']['email']));
            $mensaje = 'This email is already on the system.';
			if($idTipo != 6 && count($modUsuarios)>0){
			      Yii::app()->user->setFlash('error',$mensaje);
		           $this->redirect(array('admin'));
    		}
    		
    		
    	    $modUtil = new Utilidades();
           
            if(!is_null($ruta_foto)){
                    $extension = ".".$ruta_foto->getExtensionName();
                    $nombreImagen = $_POST['UsersC']['id_cliente']."_".$id."_".$ahora.$extension;
                    $model->ruta_foto=$nombreImagen;
                    $model->save();
                    $ruta_foto->saveAs("images/users/".$nombreImagen);
                    $modUtil->generaThumb("images/users/",$nombreImagen);
            
                }
            
            $SQL = "INSERT INTO `users_c` (`numero_id`, `id_cliente`, `id_usuario_c`, `nombres`, `apellidos`, `clave`, `email`, `id_perfil`, `estado`, `id_pais`, `id_ciudad`) 
            VALUES ('".$_POST['UsersC']['numero_id']."', '".$_POST['UsersC']['id_cliente']."', '".$id."', '".$_POST['UsersC']['nombres']."', '".$_POST['UsersC']['apellidos']."', MD5('".$clave."'), '".$_POST['UsersC']['email']."', '".$_POST['UsersC']['id_perfil']."', 'A', '".$_POST['UsersC']['id_pais']."', '".$_POST['UsersC']['id_ciudad']."')  
            ON DUPLICATE KEY UPDATE id_cliente = id_cliente";
          
            $list= Yii::app()->db->createCommand($SQL)->query();
            
            //$modUtil->enviarCuenta($id, 1);
   
            $this->redirect(array('admin'));
            /*
            if($model->save(true,array('numero_id' ,'id_cliente','id_usuario_c','nombres','apellidos','clave','email','id_perfil','tipo_id','numero_id','estado','id_pais','id_ciudad'))){
                $modUtil = new Utilidades();

                   
               
            }*/
		}
		$this->render('create',array(
			'model'=>$model,
		));
	}

        public function actionEmail($id){
            $modUtil = new Utilidades();
            $modUtil->enviarCuenta($id,1);
            $this->redirect(array('admin'));
            
        }
        
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=  UsersC::model()->findByAttributes(array('id_usuario_c'=>$id));
        $anteClave = $model->clave;
        $nuevaClave = "";
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		
		if(isset($_POST['UsersC']))
		{ 
		    
		            $model->attributes=$_POST['UsersC'];
                    $nuevaClave = $_POST['UsersC']['clave'];
                    $clave2 = $model->clave2;
                    
                    if($nuevaClave==""){
                        $model->clave=$anteClave;
                        $SQL = "SELECT clave2 FROM `users_c` WHERE `id_usuario_c` LIKE '".$model->id_usuario_c."'";
                        $list= Yii::app()->db->createCommand($SQL)->query();
                         
                          foreach ($list as $var) {
                            $clave2 = $var['clave2'];
                          }
                        $model->clave2 =$clave2;
                        
                    }else{
                        $model->clave=md5($nuevaClave);
                    }
                    
                    //$ruta_imagen=CUploadedFile::getInstance($model,'ruta_foto');
                    $modUtil = new Utilidades();
                    if($model->update(true,array('nombres', 'apellidos', 'clave', 'clave2', 'email', 'id_cliente', 'id_perfil', 'tipo_id', 'numero_id', 'estado'))){
                  
                  
                    //enviamos correo
                    if($_POST['UsersC']['estado']=='A' && $_POST['UsersC']['id_perfil']=='2' && $model->isDemo =='1'){
                         
                        $mensaje = 'An email was sent with the account detail to the enabled user.'; 
                        
                        $SQL = "SELECT * FROM `users_c` WHERE `id_usuario_c` LIKE '".$model->id_usuario_c."'";
                        $list= Yii::app()->db->createCommand($SQL)->query();
                         
                        foreach ($list as $var) {
                            $name = $var['nombres'];
                            $userId = $var['id_usuario_c'];
                            $pass = $var['clave2'];
                        }
                                                                         //send Agent email
                        $to = $model->email;
                        $subject = 'Demo account enabled - '.$model->idCliente->nombre_rsocial.'';
                        $tipo_perfil = $model->idPerfil->nombre;
                        $cc = 'info@iltoexams.com';
                        $htmlBody = '<p><h2>Hi '.$name.',</h2></p>
                        <p>
                        
                        </p>
                        <h3>Your Demo account created as TOTAL (TOAA) is now enable. To use your new account please follow the steps bellow:</h3>
                        <br>
                        Click the following link <a href="https://www.iltoexams.com">https://www.iltoexams.com</a>. Once on the site, click the "TEST ADMINISTRATOR" link and use the following information:</p>
                        <table>
                            <tr><td style="font-size:16px"><span style="color:#008a17; font-size:16px">Username:</span></td><td> '.$userId.'</td></tr>
                            <tr><td style="font-size:16px"><span style="color:#008a17; font-size:16px">Password:</span></td><td> '.$pass.'</td></tr>
                        </table>
                        </p>
                        <p>
                            Best regards,
                        </p>
                        <p>
                            Support Team - ILTO<br>
                            <a href="https://www.iltoexams.com/"><img src="https://iltoexams.com/logo_ILTO.png" alt="iltoexams"></p></a>
                        </p>';
                        
                        
                        $dat = "{From: 'noreply@iltoexams.com', To: '$to', Cc: 'iltoexams@outlook.com', Subject: '$subject', HtmlBody: '$htmlBody'}";//array('From' => 'noreply@iltoexams.com', 'To'=>'dokho_02@hotmail.com', 'Subject'=>'Hola Chris Fer', 'HtmlBody'=>'<strong>Hello</strong> dear Postmark user.');
                        $modUtil = new Utilidades();
                        $modUtil->sendEmail($dat);
                        
                        $mensaje = 'An Email was sent to '.$model->email.' with the Login Information.';
                        Yii::app()->user->setFlash('success','An Email was sent to '.$model->email.' with the Login Information.');
                        $this->redirect(array('admin', $mensaje)); 
                    }
                  
                       /* if(!is_null($ruta_imagen)){
                            //Si tenia una imagen, eliminarla
                            if(isset($_POST['ruta_imagen_actual'])){
                                $modUtil->eliminarThumbs("images/users/",$_POST['ruta_imagen_actual']);
                            }
                            //Nombre de la nueva imagen
                            $extension = ".".$ruta_imagen->getExtensionName();
                            $ahora = time();
                            $nombreImagen = $model->id_cliente."_".$model->id_usuario_c.$ahora.$extension;
                            $model->ruta_foto=$nombreImagen;
                            $model->save();
                            $ruta_imagen->saveAs("images/users/".$nombreImagen);
                            $modUtil->generaThumb("images/users/",$nombreImagen);
                        } elseif(strlen($_POST['ruta_imagen_actual'])>0){ //Si existia una imagen previa
                            if(isset($_POST['delete_imagen'])){
                                $model->ruta_foto=NULL;
                                $model->save(true,array('ruta_foto'));
                                $modUtil->eliminarThumbs("images/users/",$_POST['ruta_imagen_actual']);
                            }
                        }*/
                        $this->redirect(array('admin'));
                    }
		}
		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id,$client)
	{
            Yii::import('application.modules.Clients.models.LicensesUser');
            Yii::import('application.modules.Clients.models.ExamsUser');
            LicensesUser::model()->deleteAllByAttributes(array('id_usuario'=>$id));
            UsersC::model()->findByAttributes(array('id_usuario_c'=>$id,'id_cliente'=>$client))->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('UsersC');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new UsersC('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['UsersC']))
			$model->attributes=$_GET['UsersC'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return UsersC the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=UsersC::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param UsersC $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='users-c-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        public function actionCitiesCountry($country){
            $retorno = CHtml::listData(Cities::model()->findAllByAttributes(array('id_pais'=>$country)), 'id_ciudad', 'nombre');
            echo json_encode($retorno);
        }
        
   
   public function actionCertificate($id){
                $modUtil = new Utilidades;
                $modExamsUser = ExamsUser::model()->findAllByAttributes(array('id_licencia_usuario'=>$id));
                $idExamen= $modExamsUser[0]->id_examen;
                if($modExamsUser){
                    foreach($modExamsUser as $examen){
                        if($examen->estado!="F"){
                            Yii::app()->user->setFlash('error','This user still have pending exams ');
                            $this->redirect(array('LicensesUser/admin/id/'.$modExamsUser[0]->idLicenciaUsuario->id_usuario));
                        }
                    }
                    if($idExamen == 23){
                        $informe = $modUtil->generaCertificado($id);
                		$mPDF1 = Yii::app()->ePdf->mpdf("","Letter");
                    	$mPDF1->WriteHTML($informe);
                    	$nombre = "cert_".$modExamsUser[0]->idLicenciaUsuario->id_usuario.".pdf";
                    	$mPDF1->Output();   
                    }
                    
                    if($idExamen == 15){
                        $informe = $modUtil->generaCertificado($id);
                		$mPDF1 = Yii::app()->ePdf->mpdf("","Letter");
                    	$mPDF1->WriteHTML($informe);
                    	$nombre = "cert_".$modExamsUser[0]->idLicenciaUsuario->id_usuario.".pdf";
                    	$mPDF1->Output();   
                    }
                    if($idExamen == 13){
                        $informe = $modUtil->generaCertificado($id);
                		$mPDF1 = Yii::app()->ePdf->mpdf("","Letter");
                    	$mPDF1->WriteHTML($informe);
                    	$nombre = "cert_".$modExamsUser[0]->idLicenciaUsuario->id_usuario.".pdf";
                    	$mPDF1->Output();   
                    }
                   
                   
                   		$informe = $modUtil->generaCertificadoEde($id);
	                    $mPDF1 = Yii::app()->ePdf->mpdf("","Letter");
	                    $mPDF1->WriteHTML($informe);
	                    $nombre = "cert_".$modExamsUser[0]->idLicenciaUsuario->id_usuario.".pdf";
	                    $mPDF1->Output();
                }
        }

        public function actionEmailcertificate($id){
				$modExamsUser = ExamsUser::model()->findAllByAttributes(array('id_licencia_usuario'=>$id));
                $idExamen= $modExamsUser[0]->id_examen;
				$modUtil = new Utilidades; 
                $modLicencias = LicensesUser::model()->findByPk($id);
                $modUtil->enviarCertificado($modLicencias->id_usuario,$id);
                $this->redirect(array('LicensesUser/admin/id/'.$modLicencias->id_usuario));        
                
        }
        

    
}

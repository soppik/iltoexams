<?php


class ApiController extends CController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	//public $layout='//layouts/column2';
    protected $connection = false;
	/**
	 * @return array action filters
	 */
	 public function __construct(){
	    
         if(str_replace('www.', '', $_SERVER['SERVER_NAME']) == 'iltoexams.com'){
            	    $this->connection=new CDbConnection('mysql:host=localhost;dbname=iltoexam_newversion','iltoexam_newvers','Ilto.2015');
                    $this->connection->active=true;             
         }else{
             	   //$this->connection=new CDbConnection('mysql:host=localhost;dbname=iltoexam_newversion','iltoexam_newvers','Ilto.2015');
             	    $this->connection=new CDbConnection('mysql:host=localhost;dbname=thetec_iltoexam_newversion','thetec_newvers','Ilto.2015');
                    $this->connection->active=true;
         }
         
	
	 }
	 
	public function filters()
	{
	    	return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	
	public function accessRules()
	{
	    
	    return array(
            array('allow',
                'actions'=>array('create', 'edit', 'update', 'delete'),
                'users'=>array('*'),
            )
        );
        
	}
	
	public function actionReportExport(){
        
          
        // output headers so that the file is downloaded rather than displayed
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=list-report-'.date('Y_m_d_H_i_s').'.csv');
        
        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');
        
        
        /*$SQL = "SELECT id_licencia, id_grupo_cliente, id_usuario_c, email, nombres, apellidos, idLicenciaCliente.fecha_asignacion, calificacion, nivel, idLicenciaCliente.estado  FROM `licenses_user` `t`  LEFT OUTER JOIN `users_c` `idUsuario` ON (`t`.`id_usuario`=`idUsuario`.`id_usuario_c`) AND (id_cliente='".$_REQUEST['id_cliente']."' and id_perfil >= 2) 
        LEFT OUTER JOIN `licenses_client` `idLicenciaCliente` ON (`t`.`id_licencia_cliente`=`idLicenciaCliente`.`id_licencias_cliente`) WHERE (idLicenciaCliente.id_cliente LIKE '%".$_REQUEST['id_cliente']."%') ORDER BY t.fecha_presentacion DESC LIMIT 100"";
        */
        $SQL = "SELECT * FROM `users_c_period` WHERE `id_cliente` = '".$_REQUEST['id_cliente']."'";
        $period = $this->connection->createCommand($SQL)->queryAll();
        
        
        
        if(!empty($period)){
            $SQL = "SELECT id_licencia, t.id_licencias_usuario, id_grupo_cliente, id_usuario, email, nombres, apellidos, t.fecha_presentacion as fecha_presentacion, t.calificacion as calificacion, t.nivel as nivel, t.estado as estado, id_city, id_school, id_program, period FROM `licenses_user` `t` 
	            LEFT OUTER JOIN `users_c` `idUsuario` ON (`t`.`id_usuario`=`idUsuario`.`id_usuario_c`) AND (id_perfil >= 2) 
	            LEFT OUTER JOIN `licenses_client` `idLicenciaCliente` ON (`t`.`id_licencia_cliente`=`idLicenciaCliente`.`id_licencias_cliente`) 
	            LEFT OUTER JOIN `users_c_period` `idLicenciaPeriod` ON (`t`.`id_licencias_usuario`=`idLicenciaPeriod`.`id_licencias_usuario`)
	            WHERE (idLicenciaCliente.id_cliente = '".$_REQUEST['id_cliente']."') ORDER BY t.fecha_presentacion DESC"; 
            fputcsv($output, array('LICENSE_TYPE', 'LICENSE ID', 'GROUP', 'USER ID','EMAIL','NAME','SURNAME','TEST DATE','SCORE','CEFR LEVEL', 'STATUS', 'CITY', 'SCHOOL', 'PROGRAM', 'PERIOD'));
        }else{
            $SQL = "SELECT id_licencia, t.id_licencias_usuario, id_grupo_cliente, id_usuario_c, email, nombres, apellidos, t.fecha_presentacion as fecha_presentacion, t.calificacion as calificacion, t.nivel as nivel, t.estado as estado FROM `licenses_user` `t`  LEFT OUTER JOIN `users_c` `idUsuario` ON (`t`.`id_usuario`=`idUsuario`.`id_usuario_c`) AND (id_perfil >= 2) 
            LEFT OUTER JOIN `licenses_client` `idLicenciaCliente` ON (`t`.`id_licencia_cliente`=`idLicenciaCliente`.`id_licencias_cliente`) WHERE (idLicenciaCliente.id_cliente = '".$_REQUEST['id_cliente']."') ORDER BY t.fecha_presentacion DESC ";
            fputcsv($output, array('LICENSE', 'LICENSE ID','GROUP', 'USER ID','EMAIL','NAME','SURNAME','TEST DATE','SCORE','CEFR LEVEL', 'STATUS'));
        }
        $list_l= $this->connection->createCommand($SQL)->queryAll();
        
        $SQL = "SELECT * FROM `groups_users_clients` `t` WHERE id_cliente='".$_REQUEST['id_cliente']."' ";
        $list_g= $this->connection->createCommand($SQL)->queryAll();
        
        foreach($list_g as $g){
            $grupos[$g['id']] = $g['nombre'];
        }
        
        $SQL = "SELECT * FROM `licenses` `t`";
        $list_t= $this->connection->createCommand($SQL)->queryAll();
        
        foreach($list_t as $t){
            $lics[$t['id_licencia']] = $t['nombre'];
        }
        
        $SQL = "SELECT ID, Name FROM `city` ";
	   	$cities= Yii::app()->db->createCommand($SQL)->queryAll();
	        
	    foreach($cities as $c){
	       $cities[$c['ID']] = $c['Name'];
	    }
	    
	    $SQL = "SELECT id_school, name FROM `school` ";
	    $schools= Yii::app()->db->createCommand($SQL)->queryAll();
	        
	    foreach($schools as $sc){
	       $schools[$sc['id_school']] = $sc['name'];
	    }
	    
	    $SQL = "SELECT id_program, name FROM `programs` ";
	    $programs= Yii::app()->db->createCommand($SQL)->queryAll();
	        
	    foreach($programs as $pr){
	       $programs[$pr['id_program']] = $pr['name'];
	    }
	        
        
        foreach($list_l as $rooms){
            
            $rooms['id_licencia'] = utf8_encode($lics[$rooms['id_licencia']]);
            $rooms['id_grupo_cliente'] = $grupos[$rooms['id_grupo_cliente']];
                        
            $rooms['nombres'] = $rooms['nombres'];
            $rooms['apellidos'] = $rooms['apellidos'];
                               
            $rooms['nivel'] = utf8_encode($rooms['nivel']);
            $rooms['estado'] = ($rooms['estado']=="A" ? "Pending" : ($rooms['estado']=="F" ? "Completed" : ""));
            $rooms['id_city'] = utf8_decode($cities[$rooms['id_city']]);
            $rooms['id_school'] = utf8_decode($schools[$rooms['id_school']]);
            $rooms['id_program'] = utf8_decode($programs[$rooms['id_program']]);
            fputcsv($output, $rooms);
        }
        die();
	    
	}
	
	public function actionReportExportFuaa(){
        
          
       // output headers so that the file is downloaded rather than displayed
	        header('Content-Type: text/csv; charset=utf-8');
	        header('Content-Disposition: attachment; filename=list-report-'.date('Y_m_d_H_i_s').'.csv');
	        
	        // create a file pointer connected to the output stream
	        $output = fopen('php://output', 'w');
        
       if(isset($_POST['level']) && !empty($_POST['level']>0)){
				foreach($_POST['level'] as $check) {
					$level .= "'".$check."',";
				}	
				$level = rtrim($level,",");
			}
			
			if(isset($_POST['period']) && !empty($_POST['period']>0)){
				foreach($_POST['period'] as $number) {
					$periods .= "'".$number."',";
				}	
				$periods = rtrim($periods,",");
			}
			
			if(isset($_POST['city']) && !empty($_POST['city']>0)){
				foreach($_POST['city'] as $number) {
					$cities .= "'".$number."',";
				}	
				$cities = rtrim($cities,",");
			}
			
			
			$IdCliente = $_POST['LicensesUser']['id_cliente'];
	    	$where = 'WHERE (idLicenciaCliente.id_cliente = "'.$IdCliente.'")';
		 
					    
		    if(isset($_POST['LicensesUser']['id_user']) && strlen($_POST['LicensesUser']['id_user'])>0){
		        $where .= ' and id_usuario like "'.$_POST['LicensesUser']['id_user'].'" ';
		    }
		
		    if(isset($_POST['LicensesUser']['name']) && strlen($_POST['LicensesUser']['name'])>0){
		        $where .= ' and nombres like "%'.$_POST['LicensesUser']['name'].'%" ';
		    }
		    
		    
		    if(isset($_POST['LicensesUser']['surname']) && strlen($_POST['LicensesUser']['surname'])>0){
		        $where .= ' and apellidos like "%'.$_POST['LicensesUser']['surname'].'%" ';
		    }
	
	        
		    if(isset($_POST['LicensesUser']['id_grupo_cliente']) && strlen($_POST['LicensesUser']['id_grupo_cliente'])>0){
		        $where .= ' and id_grupo_cliente = "'.$_POST['LicensesUser']['id_grupo_cliente'].'" ';
		    }	    
		    
		    if(isset($level) && strlen($level)>0){
		        $where .= ' and nivel IN ('.$level.') ';
		    }
		    
		    if(isset($periods) && strlen($periods)>0){
		        $where .= ' and period IN ('.$periods.') ';
		    }
		    
		    if(isset($_POST['LicensesUser']['status']) && strlen($_POST['LicensesUser']['status'])>0){
		        $where .= ' and t.estado = "'.$_POST['LicensesUser']['status'].'" ';
		    }
		    
		    if(isset($_POST['LicensesUser']['id_license']) && strlen($_POST['LicensesUser']['id_license']>0)){
		        $where .= ' and id_licencia = "'.$_POST['LicensesUser']['id_license'].'" ';
		    }
		    
		    if(isset($_POST['LicensesUser']['startDate']) && strlen($_POST['LicensesUser']['startDate']>0)){
		        $where .= ' and t.fecha_presentacion >= "'.$_POST['LicensesUser']['startDate'].'" ';
		        if(isset($_POST['LicensesUser']['endDate']) && strlen($_POST['LicensesUser']['endDate']>0)){
		            $where .= ' and t.fecha_presentacion BETWEEN "'.$_POST['LicensesUser']['startDate'].'" AND "'.$_POST['LicensesUser']['endDate'].'"';
		        }
		    }
		    
		    if(isset($_POST['LicensesUser']['id_program']) && strlen($_POST['LicensesUser']['id_program'])>0){
		        $where .= ' and id_program = "'.$_POST['LicensesUser']['id_program'].'" ';
		    }
		    
		    if(isset($_POST['LicensesUser']['score']) && strlen($_POST['LicensesUser']['score']>0)){
		        $where .= ' and calificacion >= "'.$_POST['LicensesUser']['score'].'" ';
		    }
		    
		    $IdCliente = $_POST['LicensesUser']['id_cliente'];
	        
	        //Si el cliente usa el periodo
	        
	        $SQL = "SELECT id_cliente FROM `users_c_period` WHERE `id_cliente` = '".$IdCliente."' GROUP BY id_cliente";
	        $period = Yii::app()->db->createCommand($SQL)->queryAll();
	       
    	   $SQL = "SELECT id_licencia, t.id_licencias_usuario, id_grupo_cliente, id_usuario, email, nombres, apellidos, t.fecha_presentacion as fecha_presentacion, t.calificacion as calificacion, t.nivel as nivel, t.estado as estado, id_city, id_school, id_program, period FROM `licenses_user` `t` 
    	       LEFT OUTER JOIN `users_c` `idUsuario` ON (`t`.`id_usuario`=`idUsuario`.`id_usuario_c`) AND (id_perfil >= 2) 
    	       LEFT OUTER JOIN `licenses_client` `idLicenciaCliente` ON (`t`.`id_licencia_cliente`=`idLicenciaCliente`.`id_licencias_cliente`) 
	            LEFT OUTER JOIN `users_c_period` `idLicenciaPeriod` ON (`t`.`id_licencias_usuario`=`idLicenciaPeriod`.`id_licencias_usuario`)
	            $where ORDER BY t.fecha_presentacion DESC";
	            
	       fputcsv($output, array('LICENSE_TYPE', 'LICENSE ID', 'GROUP', 'USER ID','EMAIL','NAME','SURNAME','TEST DATE','SCORE','CEFR LEVEL', 'STATUS', 'CITY', 'SCHOOL', 'PROGRAM', 'PERIOD'));
	        
	        
	        $list= $this->connection->createCommand($SQL)->queryAll();
			
			$SQL = "SELECT * FROM `groups_users_clients` `t` WHERE id_cliente='".$IdCliente."' ";
	        $list_g= $this->connection->createCommand($SQL)->queryAll();
	        
	        foreach($list_g as $g){
	            $grupos[$g['id']] = $g['nombre'];
	        }
	        
	        
	        $SQL = "SELECT * FROM `licenses` `t`";
	        $list_t= $this->connection->createCommand($SQL)->queryAll();
	        
	        foreach($list_t as $t){
	            $lics[$t['id_licencia']] = $t['nombre'];
	        } 
	        
	        $SQL = "SELECT ID, Name FROM `city` ";
		    $cities= $this->connection->createCommand($SQL)->queryAll();
		        
		    foreach($cities as $c){
		       $cities[$c['ID']] = $c['Name'];
		    }
		    
		    $SQL = "SELECT id_school, name FROM `school` ";
		    $schools= $this->connection->createCommand($SQL)->queryAll();
		        
		    foreach($schools as $sc){
		       $schools[$sc['id_school']] = $sc['name'];
		    }
	    
			$SQL = "SELECT id_program, name FROM `programs` ";
		    $programs= $this->connection->createCommand($SQL)->queryAll();
		        
		    foreach($programs as $pr){
		       $programs[$pr['id_program']] = $pr['name'];
		    }
			
			foreach($list as $rooms){
            
	            $rooms['id_licencia'] = utf8_encode($lics[$rooms['id_licencia']]);
	            $rooms['id_grupo_cliente'] = utf8_decode($grupos[$rooms['id_grupo_cliente']]);
	                        
	            $rooms['nombres'] = utf8_decode($rooms['nombres']);
	            $rooms['apellidos'] = utf8_decode($rooms['apellidos']);
	                               
	            $rooms['nivel'] = utf8_encode($rooms['nivel']);
	            
	            $rooms['estado'] = ($rooms['estado']=="A" ? "Pending" : ($rooms['estado']=="F" ? "Completed" : ""));
	            $rooms['id_city'] = utf8_decode($cities[$rooms['id_city']]);
	            $rooms['id_school'] = utf8_decode($schools[$rooms['id_school']]);
	            $rooms['id_program'] = utf8_decode($programs[$rooms['id_program']]);
	        	fputcsv($output, $rooms);    
	        }
	        
			die();
	    
	}
	
	public function actionFuuaAdReportExport(){
        	    
        // output headers so that the file is downloaded rather than displayed
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=list-report-'.date('Y_m_d_H_i_s').'.csv');
        
        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');
        
        
        $SQL = "SELECT id_licencia, t.id_licencias_usuario, id_usuario, email, nombres, apellidos, t.fecha_presentacion as fecha_presentacion, t.calificacion as calificacion, t.nivel as nivel, t.estado as estado, id_city, id_category,  id_roll FROM `licenses_user` `t` LEFT OUTER JOIN `users_c` `idUsuario` ON (`t`.`id_usuario`=`idUsuario`.`id_usuario_c`) AND (id_perfil >= 2) LEFT OUTER JOIN `licenses_client` `idLicenciaCliente` ON (`t`.`id_licencia_cliente`=`idLicenciaCliente`.`id_licencias_cliente`) LEFT OUTER JOIN `users_c_period` `idLicenciaPeriod` ON (`t`.`id_licencias_usuario`=`idLicenciaPeriod`.`id_licencias_usuario`) WHERE (idLicenciaCliente.id_cliente LIKE '86051730211') ORDER BY t.fecha_presentacion DESC";
        fputcsv($output, array('LICENSE_TYPE', 'LICENSE ID', 'USER ID','EMAIL','NAME','SURNAME','TEST DATE','SCORE','CEFR LEVEL', 'STATUS', 'CITY', 'CATEGORY', 'ROLL'));
        $list_l= $this->connection->createCommand($SQL)->queryAll();
        
        $SQL = "SELECT * FROM `licenses` `t`";
        $list_t= $this->connection->createCommand($SQL)->queryAll();
        
        foreach($list_t as $t){
            $lics[$t['id_licencia']] = $t['nombre'];
        }
        
        
        foreach($list_l as $rooms){
            
            $rooms['id_licencia'] = utf8_encode($lics[$rooms['id_licencia']]);
                        
            $rooms['nombres'] = $rooms['nombres'];
            $rooms['apellidos'] = $rooms['apellidos'];
                               
            $rooms['nivel'] = utf8_encode($rooms['nivel']);
            $rooms['estado'] = ($rooms['estado']=="A" ? "Pending" : ($rooms['estado']=="F" ? "Completed" : ""));
            $rooms['id_city'] = ($rooms['id_city']=="2257" ? "Bogota" : ($rooms['id_city']=="2259" ? "Medellin" : ($rooms['id_city']=="2265" ? "Pereira" : ($rooms['id_city']=="2265" ? "Valledupar" : ""))));
            $rooms['id_category'] = ($rooms['id_category']=="1" ? "Aspirante" : ($rooms['id_category']=="2" ? "Empleado" : ""));
            $rooms['id_roll'] = ($rooms['id_roll']=="1" ? "Administrativo" : ($rooms['id_roll']=="2" ? "Docente" : ""));
            fputcsv($output, $rooms);
        }
        die();
	    
	}
	
	
	public function actionResultReportExport($sql){
        	    
        
        // output headers so that the file is downloaded rather than displayed
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=list-report-'.date('Y_m_d_H_i_s').'.csv');
        
        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');
        
        
        /*$SQL = "SELECT id_licencia, id_grupo_cliente, id_usuario_c, email, nombres, apellidos, idLicenciaCliente.fecha_asignacion, calificacion, nivel, idLicenciaCliente.estado  FROM `licenses_user` `t`  LEFT OUTER JOIN `users_c` `idUsuario` ON (`t`.`id_usuario`=`idUsuario`.`id_usuario_c`) AND (id_cliente='".$_REQUEST['id_cliente']."' and id_perfil >= 2) 
        LEFT OUTER JOIN `licenses_client` `idLicenciaCliente` ON (`t`.`id_licencia_cliente`=`idLicenciaCliente`.`id_licencias_cliente`) WHERE (idLicenciaCliente.id_cliente LIKE '%".$_REQUEST['id_cliente']."%') ORDER BY t.fecha_presentacion DESC LIMIT 100"";
        */
        $SQL = "SELECT * FROM `users_c_period` WHERE `id_cliente` = '".$_REQUEST['id_cliente']."'";
        $period = $this->connection->createCommand($SQL)->queryAll();
        
        if(!empty($period)){
            $SQL = "SELECT id_licencia, t.id_licencias_usuario, id_grupo_cliente, id_usuario, email, nombres, apellidos, t.fecha_presentacion as fecha_presentacion, t.calificacion as calificacion, t.nivel as nivel, t.estado as estado, period FROM `licenses_user` `t` LEFT OUTER JOIN `users_c` `idUsuario` ON (`t`.`id_usuario`=`idUsuario`.`id_usuario_c`) AND (id_cliente='".$_REQUEST['id_cliente']."' and id_perfil >= 2) LEFT OUTER JOIN `licenses_client` `idLicenciaCliente` ON (`t`.`id_licencia_cliente`=`idLicenciaCliente`.`id_licencias_cliente`) LEFT OUTER JOIN `users_c_period` `idLicenciaPeriod` ON (`t`.`id_licencias_usuario`=`idLicenciaPeriod`.`id_licencias_usuario`) WHERE (idLicenciaCliente.id_cliente LIKE '8605173021') ORDER BY t.fecha_presentacion DESC";
            fputcsv($output, array('LICENSE_TYPE', 'LICENSE ID', 'GROUP', 'USER ID','EMAIL','NAME','SURNAME','TEST DATE','SCORE','CEFR LEVEL', 'STATUS', 'PERIOD'));
        }else{
            $SQL = "SELECT id_licencia, t.id_licencias_usuario, id_grupo_cliente, id_usuario_c, email, nombres, apellidos, t.fecha_presentacion as fecha_presentacion, t.calificacion as calificacion, t.nivel as nivel, t.estado as estado FROM `licenses_user` `t`  LEFT OUTER JOIN `users_c` `idUsuario` ON (`t`.`id_usuario`=`idUsuario`.`id_usuario_c`) AND (id_cliente='".$_REQUEST['id_cliente']."' and id_perfil >= 2) 
            LEFT OUTER JOIN `licenses_client` `idLicenciaCliente` ON (`t`.`id_licencia_cliente`=`idLicenciaCliente`.`id_licencias_cliente`) WHERE (idLicenciaCliente.id_cliente = '".$_REQUEST['id_cliente']."') ORDER BY t.fecha_presentacion DESC ";
            fputcsv($output, array('LICENSE', 'LICENSE ID','GROUP', 'USER ID','EMAIL','NAME','SURNAME','TEST DATE','SCORE','CEFR LEVEL', 'STATUS'));
        }
        $list_l= $this->connection->createCommand($SQL)->queryAll();
        
        $SQL = "SELECT * FROM `groups_users_clients` `t` WHERE id_cliente='".$_REQUEST['id_cliente']."' ";
        $list_g= $this->connection->createCommand($SQL)->queryAll();
        
        foreach($list_g as $g){
            $grupos[$g['id']] = $g['nombre'];
        }
        
        $SQL = "SELECT * FROM `licenses` `t`";
        $list_t= $this->connection->createCommand($SQL)->queryAll();
        
        foreach($list_t as $t){
            $lics[$t['id_licencia']] = $t['nombre'];
        }
        
        
        foreach($list_l as $rooms){
            
            $rooms['id_licencia'] = utf8_encode($lics[$rooms['id_licencia']]);
            $rooms['id_grupo_cliente'] = $grupos[$rooms['id_grupo_cliente']];
                        
            $rooms['nombres'] = utf8_encode($rooms['nombres']);
            $rooms['apellidos'] = utf8_encode($rooms['apellidos']);
                               
            $rooms['nivel'] = utf8_encode($rooms['nivel']);
            $rooms['estado'] = ($rooms['estado']=="A" ? "Pending" : ($rooms['estado']=="F" ? "Completed" : ""));
            
            fputcsv($output, $rooms);
        }
        die();
	    
	}
	
	public function actionOrdersExport(){
        	    
        	    // output headers so that the file is downloaded rather than displayed
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=list-report-'.date('Y_m_d_H_i_s').'.csv');
        
        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');
        
        
        /*$SQL = "SELECT id_licencia, id_grupo_cliente, id_usuario_c, email, nombres, apellidos, idLicenciaCliente.fecha_asignacion, calificacion, nivel, idLicenciaCliente.estado  FROM `licenses_user` `t`  LEFT OUTER JOIN `users_c` `idUsuario` ON (`t`.`id_usuario`=`idUsuario`.`id_usuario_c`) AND (id_cliente='".$_REQUEST['id_cliente']."' and id_perfil >= 2) 
        LEFT OUTER JOIN `licenses_client` `idLicenciaCliente` ON (`t`.`id_licencia_cliente`=`idLicenciaCliente`.`id_licencias_cliente`) WHERE (idLicenciaCliente.id_cliente LIKE '%".$_REQUEST['id_cliente']."%') ORDER BY t.fecha_presentacion DESC LIMIT 100"";
        */
        $SQL = "SELECT id_order, id_client, nombre_rsocial, date_expedition, date_expire, subtotal, discount, total, orders.id_distributor, status FROM `orders` INNER JOIN `clients` ON orders.id_client = clients.id_cliente";
        
        $list_l= $this->connection->createCommand($SQL)->queryAll();
        
        // output the column headings
        fputcsv($output, array('id_order', 'id_cliente', 'nombre_rsocial','date_expedition','date_expire','subtotal','discount','total','id_distributor', 'status'));
        
        foreach($list_l as $item){
            
            $rooms['id_licencia'] = utf8_encode($lics[$rooms['id_licencia']]);
            $rooms['id_grupo_cliente'] = utf8_encode($grupos[$rooms['id_grupo_cliente']]);
                        
            $rooms['nombres'] = utf8_encode($rooms['nombres']);
            $rooms['apellidos'] = utf8_encode($rooms['apellidos']);
                               
            $rooms['nivel'] = utf8_encode($rooms['nivel']);
            
            fputcsv($output, $item);
        }
        die();
	    
	}
	
	
	
	public function actionPieExport(){
        	    
        	    // output headers so that the file is downloaded rather than displayed
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=tecsPie-report-'.date('Y_m_d_H_i_s').'.csv');
        
        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');
        
       
        $SQL= "SELECT count(nivel) as totales, nivel FROM `licenses_user` INNER JOIN licenses_client ON id_licencias_cliente = id_licencia_cliente and id_licencia IN (7,15) INNER JOIN users_c ON licenses_user.id_usuario = users_c.id_usuario_c WHERE users_c.id_cliente = '".$_REQUEST['id_cliente']."' group by nivel";
        //$SQL = "SELECT *, count(nivel) as totales FROM `licenses_user` INNER JOIN licenses_client ON id_licencias_cliente = id_licencia_cliente and id_licencia IN (7,15)  INNER JOIN users_c ON licenses_user.id_usuario = users_c.id_usuario_c  WHERE users_c.id_cliente = '".$_REQUEST['id_cliente']."' group by nivel";
        $list_l= $this->connection->createCommand($SQL)->queryAll();
        
        $SQL = "SELECT * FROM `groups_users_clients` `t` WHERE id_cliente='".$_REQUEST['id_cliente']."' ";
        $list_g= $this->connection->createCommand($SQL)->queryAll();
        
        $SQL = "SELECT * FROM `licenses` `t` WHERE estado='A' ";
        $list_t= $this->connection->createCommand($SQL)->queryAll();
        
        foreach($list_t as $t){
            $lics[$t['id_licencia']] = $t['nombre'];
        }
        
        foreach($list_g as $g){
            $grupos[$g['id']] = $g['nombre'];
        }
        
        // output the column headings
        fputcsv($output, array('TOTAL', 'CEFR LEVEL'));
        foreach($list_l as $rooms){
            
            fputcsv($output, $rooms);
        }
        die();
	    
	}
	
	
		public function actionEdePieExport(){
        	    
        	    // output headers so that the file is downloaded rather than displayed
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=edePie-report-'.date('Y_m_d_H_i_s').'.csv');
        
        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');
        
       
        $SQL= "SELECT count(nivel) as totales, nivel FROM `licenses_user` INNER JOIN licenses_client ON id_licencias_cliente = id_licencia_cliente and id_licencia IN (6,16) INNER JOIN users_c ON licenses_user.id_usuario = users_c.id_usuario_c WHERE users_c.id_cliente = '".$_REQUEST['id_cliente']."' group by nivel";
        //$SQL = "SELECT *, count(nivel) as totales FROM `licenses_user` INNER JOIN licenses_client ON id_licencias_cliente = id_licencia_cliente and id_licencia IN (7,15)  INNER JOIN users_c ON licenses_user.id_usuario = users_c.id_usuario_c  WHERE users_c.id_cliente = '".$_REQUEST['id_cliente']."' group by nivel";
        $list_l= $this->connection->createCommand($SQL)->queryAll();
        
        $SQL = "SELECT * FROM `groups_users_clients` `t` WHERE id_cliente='".$_REQUEST['id_cliente']."' ";
        $list_g= $this->connection->createCommand($SQL)->queryAll();
        
        $SQL = "SELECT * FROM `licenses` `t` WHERE estado='A' ";
        $list_t= $this->connection->createCommand($SQL)->queryAll();
        
        foreach($list_t as $t){
            $lics[$t['id_licencia']] = $t['nombre'];
        }
        
        foreach($list_g as $g){
            $grupos[$g['id']] = $g['nombre'];
        }
        
        // output the column headings
        fputcsv($output, array('TOTAL', 'CEFR LEVEL'));
        foreach($list_l as $rooms){
            
            fputcsv($output, $rooms);
        }
        die();
	    
	}
	
	
	
    public function actionIndex()
	{
	    var_dump($_REQUEST);
	    die('index!!');
	}
	
	public function actionUser()
	{
	    
	    ob_start();
        $this->loadTemplateUser();  
        $result =  ob_get_contents();
        ob_end_clean();
	    $Result['html'] = $result;
	    header('Content-Type: application/json');
	    die(json_encode($Result));
	}
	
	
	public function loadTemplateUser(){
	    
	    
	    $IdCliente = $_POST['id_cliente'];
	    
        $where = '';
	    
	    if(isset($_REQUEST['id_usuario_c']) && strlen($_REQUEST['id_usuario_c'])>0){
	        $where .= ' and id_usuario_c like "'.$_REQUEST['id_usuario_c'].'%" ';
	    }
	    
	    
	     
	    if(isset($_REQUEST['id_grupo_cliente']) && strlen($_REQUEST['id_grupo_cliente'])>0){
	        $where .= ' and id_grupo_cliente like "'.$_REQUEST['id_grupo_cliente'].'%" ';
	    }
	    
	    
	    if(isset($_REQUEST['id_perfil']) && strlen($_REQUEST['id_perfil'])>0){
	        $where .= ' and id_perfil like "'.$_REQUEST['id_perfil'].'%" ';
	    }
	    
	    
	    if(isset($_REQUEST['nombres']) && strlen($_REQUEST['nombres'])>0){
	        $where .= ' and nombres like "'.$_REQUEST['nombres'].'%" ';
	    }
	
	
	    if(isset($_REQUEST['apellidos']) && strlen($_REQUEST['apellidos'])>0){
	        $where .= ' and apellidos like "'.$_REQUEST['apellidos'].'%" ';
	    }
	    
	    
	    if(isset($_REQUEST['status']) && strlen($_REQUEST['status'])>0){
	        $where .= ' and estado = "'.$_REQUEST['status'].'" ';
	    }
	    
        
        $SQL = "SELECT * FROM `users_c` `t` WHERE id_cliente='".$IdCliente."' $where order by id_cliente DESC LIMIT 50";
        //var_dump($SQL );
        $list_u= $this->connection->createCommand($SQL)->queryAll();
        
        $SQL = "SELECT * FROM `groups_users_clients` `t` WHERE id_cliente='".$IdCliente."' ";
        $list_g= $this->connection->createCommand($SQL)->queryAll();
   
        $SQL = "SELECT * FROM `profiles` `t` WHERE estado='A' ";
        $list_t= $this->connection->createCommand($SQL)->queryAll();
        
        $Status = array("A"=>"Enabled", "I"=>"Disabled");
        $result = '';
	    foreach($list_u as $rooms){
                                                    
                $SQL = "SELECT * FROM `profiles` WHERE `id_perfil` = ".(int)$rooms['id_perfil']." ";
                $userData= $this->connection->createCommand($SQL)->queryAll();
                
                
                $SQL = "SELECT * FROM `groups_users_clients` WHERE `id` = ".(int)$rooms['id_grupo_cliente']." ORDER BY `id` DESC ";
                $userGroup= $this->connection->createCommand($SQL)->queryAll();
                
	    ?>
                    <tr>
                        <td><?php echo utf8_encode($userData[0]['nombre'])?></td>
                        <td><?php echo utf8_encode($userGroup[0]['nombre'])?></td>
                        <td><?php echo $rooms['id_usuario_c']?></td>
                        <td><?php echo utf8_encode($rooms['nombres'])?></td>
                        <td><?php echo utf8_encode($rooms['apellidos'])?></td>
                        
                        <td class="center "><?php 
                        	 switch ($rooms['estado']) {
                               case 'A':
                            	$status = 'ENABLED';
                               break;
                               case 'B':
                            	$status = 'DISABLED';
                               break;
                               default:
	                        	$status = 'ENABLED';
                               break;
                                                                }
                                                                
                            echo $status;
                        
                        ?></td>
                        <td>
                            
                            <?php 
                                if($rooms['id_perfil'] == 2){
                                    ?>
                                        <a href="/ilto3/index.php?r=Clients/UsersC/view&id=<?php echo $rooms['id_usuario_c']?>" class="btn btn-warning btn-xs edit"><i class="fa fa-edit"></i> View</a>
                                    <?php
                                }
                                
                                if($rooms['id_perfil'] == 6){
                                    ?>
                                        <?php 
                                            $SQL = "SELECT COUNT(*) as total FROM `licenses_user` `t` WHERE `t`.`id_usuario`='".$rooms['id_usuario_c']."' ";
                                            //var_dump($SQL);
                                            $LicensesCount = $this->connection->createCommand($SQL)->queryAll();
                                   
                                            ?>
                                                <a href="/ilto3/index.php?r=Clients/LicensesUser/admin&id=<?php echo $rooms['id_usuario_c']?>" class="btn btn-success btn-xs edit"><i class="fa fa-check"></i> Licenses</a>
                                                <a href="/ilto3/index.php?r=Clients/UsersC/view&id=<?php echo $rooms['id_usuario_c']?>" class="btn btn-warning btn-xs edit"><i class="fa fa-edit"></i> View</a>
                                                
                                    
                                            
                                            <?php
                                            if($LicensesCount[0]['total']==0){
                                                ?>
                                                    <a href="/ilto3/index.php?r=Clients/UsersC/delete&id=<?php echo $rooms['id_usuario_c']?>" class="btn btn-danger btn-xs delete"><i class="fa fa-trash-o"></i> Delete</a>
                                                    <a href="/ilto3/index.php?r=Clients/UsersC/update&id=<?php echo $rooms['id_usuario_c']?>" class="btn btn-info btn-xs edit"><i class="fa fa-edit"></i> Edit</a>
                                            <?php
                                                }
                                            
                                            if($rooms['estado'] == 'B'){
                                            	?>
                                            	<a href="/ilto3/index.php?r=Clients/UsersC/enableTaker&id=<?php echo $rooms['id_usuario_c']?>" class="btn btn-primary btn-xs edit"><i class="fa fa-check"></i> Enable Taker</a>	
                                           <?php
                                            }    
                                                
                                        ?>
                                        <?php
                                }
                                
                                if($rooms['id_perfil'] == 3){
                                    ?>
                                        <a href="/ilto3/index.php?r=Clients/UsersC/view&id=<?php echo $rooms['id_usuario_c']?>" class="btn btn-warning btn-xs edit"><i class="fa fa-edit"></i> View</a>
                                        <a href="/ilto3/index.php?r=Clients/UsersC/update&id=<?php echo $rooms['id_usuario_c']?>" class="btn btn-info btn-xs edit"><i class="fa fa-edit"></i> Edit</a>
                                    <?php
                                        $SQL = "SELECT COUNT(*) as total FROM `exams_user` `t` WHERE `t`.`tutor`='".$rooms['id_usuario_c']."' ";
                                        $LicensesCount = $this->connection->createCommand($SQL)->queryAll();
                                        if($LicensesCount[0]['total']==0){
                                             ?>
                                                <a href="/ilto3/index.php?r=Clients/UsersC/delete&id=<?php echo $rooms['id_usuario_c']?>" class="btn btn-danger btn-xs delete"><i class="fa fa-trash-o"></i>Delete</a>   
                                            <?php
                                        }
                                }
                                
                                if($rooms['id_perfil'] == 4){
                                    ?>
                                        <a href="/ilto3/index.php?r=Clients/UsersC/view&id=<?php echo $rooms['id_usuario_c']?>" class="btn btn-warning btn-xs edit"><i class="fa fa-edit"></i> View</a>
                                        <a href="/ilto3/index.php?r=Clients/UsersC/update&id=<?php echo $rooms['id_usuario_c']?>" class="btn btn-info btn-xs edit"><i class="fa fa-edit"></i> Edit</a>
                                     <?php
                                        $SQL = "SELECT COUNT(*) as total FROM `exams_user` `t` WHERE `t`.`tutor`='".$rooms['id_usuario_c']."' ";
                                        $LicensesCount = $this->connection->createCommand($SQL)->queryAll();
                                        if($LicensesCount[0]['total']==0){
                                             ?>
                                                <a href="/ilto3/index.php?r=Clients/UsersC/delete&id=<?php echo $rooms['id_usuario_c']?>" class="btn btn-danger btn-xs delete"><i class="fa fa-trash-o"></i>Delete</a>   
                                            <?php
                                        }
                                }
                                
                                if($rooms['id_perfil'] == 5){
                                    ?>
                                        <a href="/ilto3/index.php?r=Clients/UsersC/view&id=<?php echo $rooms['id_usuario_c']?>" class="btn btn-warning btn-xs edit"><i class="fa fa-edit"></i> View</a>
                                        <a href="/ilto3/index.php?r=Clients/UsersC/update&id=<?php echo $rooms['id_usuario_c']?>" class="btn btn-info btn-xs edit"><i class="fa fa-edit"></i> Edit</a>
                                        <?php
                                        $SQL = "SELECT COUNT(*) as total FROM `exams_user` `t` WHERE `t`.`tutor`='".$rooms['id_usuario_c']."' ";
                                        $LicensesCount = $this->connection->createCommand($SQL)->queryAll();
                                        if($LicensesCount[0]['total']==0){
                                             ?>
                                                <a href="/ilto3/index.php?r=Clients/UsersC/delete&id=<?php echo $rooms['id_usuario_c']?>" class="btn btn-danger btn-xs delete"><i class="fa fa-trash-o"></i>Delete</a>   
                                            <?php
                                        }
                                }
                            ?>
                            
                            
                        </td>
                    </tr>
                <?php
                
        }
        
	}
	
	
	public function actionUsername()
	{
	    
	    $SQL = "SELECT * FROM `users_c` `t` WHERE id_usuario_c='".$_REQUEST['id']."'";
	    $list= $this->connection->createCommand($SQL)->queryAll();
	    
	    $SQL = "SELECT nombre_rsocial FROM `clients` `t` WHERE id_cliente='".$list[0]['id_cliente']."'";
	    $client= $this->connection->createCommand($SQL)->queryAll();
	    
	    
	    if($list[0]['id_usuario_c']==""){
	        $Result = '<p id="alertG_lbl">Username or ID number available.</p>';
	    }else{
	        $Result = '<p id="alertR_lbl">This user has already taken the TECS with '.$client[0]['nombre_rsocial'].'. Please click here to enable the user in your system.</p>';
	    }
	   
	   header('Content-Type: application/json');
	   die($Result);
	}
	
	public function actionGetUserInfo()
	{
	    
	    
	    $SQL = "SELECT * FROM `users_c` `t` WHERE id_usuario_c='".$_REQUEST['id']."'";
	    $list= $this->connection->createCommand($SQL)->queryAll();
	   
	    $SQL = "SELECT nombre_rsocial FROM `clients` `t` WHERE id_cliente='".$list[0]['id_cliente']."'";
	    $client= $this->connection->createCommand($SQL)->queryAll();
	    
	    $SQL = "select * from groups_users_clients where id_cliente = '".$_REQUEST['id_cliente']."' and estado='A'";
	    $groups= $this->connection->createCommand($SQL)->queryAll();
	    
	    if($list[0]['id_usuario_c']==""){
	        $Result = '<td colspan="5"><p id="alertR_lbl">ID number not registered, unsucesful transfer.</p></td>';
	    }else{
	        if($list[0]['id_perfil']=="6"){
    	        
    	        if($_REQUEST['id_cliente']!=$list[0]['id_cliente']){
    	            $Result = '<td>'.$list[0]['id_usuario_c'].'</td>
        	        <td>'.utf8_encode($list[0]['nombres']).'</td>
        	        <td>'.utf8_encode($list[0]['apellidos']).'</td>
        	        <td>'.$list[0]['email'].'</td>
        	        <td>'.utf8_encode($client[0]['nombre_rsocial']).'</td>
        	        <td><select name="group_id" id="group_id" style="max-width:100px;">';
        	        foreach ($groups as $group) {
        	            $Result .= '<option value="'.$group['id'].'">'.utf8_encode($group['nombre']).'</option>';
        	        }
        	        $Result .= '</select></td><td><input name="id_usuario_c" type="hidden" value="'.$list[0]['id_usuario_c'].'">
        	        <input name="btnSearch" class="btn btn-success btn-xs" id="send" type="submit" value="Enable"></td>';
    	        }else{
        	        $Result = '<td>'.$list[0]['id_usuario_c'].'</td>
        	        <td>'.utf8_encode($list[0]['nombres']).'</td>
        	        <td>'.utf8_encode($list[0]['apellidos']).'</td>
        	        <td>'.$list[0]['email'].'</td>
        	        <td>'.utf8_encode($client[0]['nombre_rsocial']).'</td>
        	        <td>'.utf8_encode($groups[0]['nombre']).'</td>
        	        <td>This taker is already enabled in your system</td>';  
    	        }	        
	            
	        }else {
	            $Result = '<td colspan="5"><p id="alertR_lbl">Only TAKERS can be transfered.</p></td>';
	        }
	   }
	   
	   header('Content-Type: application/json');
	   die($Result);
	}
	
	public function loadUserInfo()
	{
	    
	    $SQL = "SELECT * FROM `users_c` `t` WHERE id_usuario_c='".$_REQUEST['id']."'";
	    $list= $this->connection->createCommand($SQL)->queryAll();
	    
	    $SQL = "SELECT nombre_rsocial FROM `clients` `t` WHERE id_cliente='".$list[0]['id_cliente']."'";
	    $client= $this->connection->createCommand($SQL)->queryAll();
	    
	    
	    if($list[0]['id_usuario_c']==""){
	        $Result = '<p id="alertG_lbl">Username or ID number available.</p>';
	    }else{
	        $Result = '<p id="alertR_lbl">This user has already taken the TECS with '.$client[0]['nombre_rsocial'].'.  Please write an email to <b>info@iltoexams.com</b> with the user name and ID number to enable the user in your system. The user will be enabled to take TECS in 24 hours.</p>';
	    }
	   
	   header('Content-Type: application/json');
	   die($Result);
	} 
	
		public function actionUserToReport()
	{
	    
	    $SQL = "SELECT id_usuario_c, nombres, apellidos FROM `users_c` `t` WHERE id_usuario_c='".$_REQUEST['id']."'";
	    $list= $this->connection->createCommand($SQL)->queryAll();
	    
	    $Result = '<span id="alertG_lbl">'.$list[0]['nombres'].'  '.$list[0]['apellidos'].'</span>';
	    
	    if($list[0]['id_usuario_c']==""){
	        $Result = 
	            '
                <div class="form-group has-feedback">
	                <span class="btn btn-success btn-xs edit">NEW USER REGISTRATION</span>
	                <label class="col-lg-4" style="text-align:right;">NAME: *</label>
                    <div class="col-lg-6">
                        <input required="required" value="1" type="hidden" class="form-control" name="FraudReport[new_user]" id="FraudReport_new_user" data-bv-field="FraudReport[new_user]">
                        <input required="required" maxlength="200" size="50" type="text" class="form-control" name="FraudReport[nombres]" id="FraudReport_nombres" data-bv-field="FraudReport[nombres]">
                    </div>
                </div>
                <div class="form-group has-feedback">
	                
	                <label class="col-lg-4"  style="text-align:right;">SURENAME: *</label>
                    <div class="col-lg-6">
                        <input required="required" maxlength="200" size="50" type="text" class="form-control" name="FraudReport[apellidos]" id="FraudReport_apellidos" data-bv-field="FraudReport[apellidos]">
                    </div>
                </div>';
	    }
	        
	    
	   header('Content-Type: application/json');
	   die($Result);
	}  
	
	public function actionGroup()
	{
	    $where = '';
	    
	    if(isset($_REQUEST['nombre']) && strlen($_REQUEST['nombre'])>0){
	        $where .= ' and nombre like "'.$_REQUEST['nombre'].'%" ';
	    }
	    
	    if(isset($_REQUEST['id']) && strlen($_REQUEST['id'])>0){
	        $where .= ' and id like "'.$_REQUEST['id'].'%" ';
	    }
	
	    
	    if(isset($_REQUEST['estado']) && strlen($_REQUEST['estado'])>0){
	        $where .= ' and estado = "'.$_REQUEST['estado'].'" ';
	    }
	    
	    
        $IdCliente = $_POST['id_cliente'];
        $SQL = "SELECT * FROM `groups_users_clients` `t` WHERE id_cliente='".$IdCliente."' $where order by id DESC LIMIT 50";
        
        $list= $this->connection->createCommand($SQL)->queryAll();
        //var_dump($list);
	    //var_dump($_REQUEST);
	    $Result = false;
	    $result = '';
	    $Status = array('A'=>'ENABLE', 'I'=>'DISABLE');
	    foreach($list as $l){
	        
	        $SQL = "SELECT COUNT(*) as total FROM `users_c` WHERE `id_grupo_cliente`='".$l['id']."' ";
            $LicensesCount = $this->connection->createCommand($SQL)->queryAll();
            if($LicensesCount[0]['total']==0){
             $result .= ' <tr>
                            <td>'.$l['nombre'].'</td>
                            <td class="center ">'.$Status[$l['estado']].'</td>
                            <td>
                                <a href="/ilto3/index.php?r=Clients/groupsUsersClients/update&id='.$l['id'].'" class="btn btn-info btn-xs edit"><i class="fa fa-edit"></i> Edit</a>
                                <a href="/ilto3/index.php?r=Clients/groupsUsersClients/delete&id='.$l['id'].'" class="btn btn-danger btn-xs delete"><i class="fa fa-trash-o"></i> Delete</a>
                            </td>
                        </tr>';
            }else {
                 $result .= ' <tr>
                            <td>'.$l['nombre'].'</td>
                            <td class="center ">'.$Status[$l['estado']].'</td>
                            <td>
                                <a href="/ilto3/index.php?r=Clients/groupsUsersClients/update&id='.$l['id'].'" class="btn btn-info btn-xs edit"><i class="fa fa-edit"></i> Edit</a>
                                 </td>
                        </tr>';
            } 
	    }
	   
	   $Result['html'] = $result;
	   header('Content-Type: application/json');
	   die(json_encode($Result));
	}
	
	public function actionReports()
	{
	    $IdCliente = $_POST['id_cliente'];
	    
	    $where = '(idLicenciaCliente.id_cliente = "'.$IdCliente.'")';
	 
	    
	    if(isset($_REQUEST['id_usuario_c']) && strlen($_REQUEST['id_usuario_c'])>0){
	        $where .= ' and id_usuario like "'.$_REQUEST['id_usuario_c'].'%" ';
	    }
	
	    if(isset($_REQUEST['nombres']) && strlen($_REQUEST['nombres'])>0){
	        $where .= ' and nombres like "'.$_REQUEST['nombres'].'%" ';
	    }
	    
	    
	    if(isset($_REQUEST['apellidos']) && strlen($_REQUEST['apellidos'])>0){
	        $where .= ' and apellidos like "'.$_REQUEST['apellidos'].'%" ';
	    }

        
	    if(isset($_REQUEST['id_grupo_cliente']) && strlen($_REQUEST['id_grupo_cliente'])>0){
	        $where .= ' and id_grupo_cliente = "'.$_REQUEST['id_grupo_cliente'].'" ';
	    }	    
	    
	    if(isset($_REQUEST['nivel']) && strlen($_REQUEST['nivel'])>0){
	        $where .= ' and nivel = "'.$_REQUEST['nivel'].'" ';
	    }
	    
	    if(isset($_REQUEST['estado']) && strlen($_REQUEST['estado'])>0){
	        $where .= ' and t.estado = "'.$_REQUEST['estado'].'" ';
	    }
	    
	    if(isset($_REQUEST['id_licencia']) && strlen($_REQUEST['id_licencia']>0)){
	        
	        $modUtil = new Utilidades();
	        $tecs_ids = $modUtil->getIdsTecs();
	        $ede_ids = $modUtil->getIdsEde();
	        
	        
	        switch ($_REQUEST['id_licencia']) {
	           case '1':
	               $where .= 'and id_licencia IN ('.$tecs_ids.')';
	           break;
	           case '2':
	               $where .= 'and id_licencia IN ('.$ede_ids.')';
	           break;
	            default:
	               $where .= 'and id_licencia IN ('.$ede_ids.')';
	                break;
	        }
	        
	        
	    }
	    
	    if(isset($_REQUEST['startDate']) && strlen($_REQUEST['startDate']>0)){
	        $where .= ' and t.fecha_presentacion >= "'.$_REQUEST['startDate'].'" ';
	        if(isset($_REQUEST['endDate']) && strlen($_REQUEST['endDate']>0)){
	            $where .= ' and t.fecha_presentacion BETWEEN "'.$_REQUEST['startDate'].'" AND "'.$_REQUEST['endDate'].'"';
	        }
	    }
	    
	    if(isset($_REQUEST['calificacion']) && strlen($_REQUEST['calificacion']>0)){
	        $where .= ' and calificacion >= "'.$_REQUEST['calificacion'].'" ';
	    }
	    
	    $IdCliente = $_POST['id_cliente'];
        
        $SQL = "SELECT t.id_licencias_usuario, t.id_licencia_cliente, t.id_usuario, t.fecha_asignacion, t.estado as status, t.fecha_presentacion, t.hora, t.calificacion, t.nivel, id_usuario_c, nombres, apellidos, email, id_grupo_cliente, id_licencia FROM `licenses_user` `t`  LEFT OUTER JOIN `users_c` `idUsuario` ON (`t`.`id_usuario`=`idUsuario`.`id_usuario_c`) 
        LEFT OUTER JOIN `licenses_client` `idLicenciaCliente` ON (`t`.`id_licencia_cliente`=`idLicenciaCliente`.`id_licencias_cliente`) WHERE $where ORDER BY t.fecha_presentacion DESC LIMIT 150";
        $list= $this->connection->createCommand($SQL)->queryAll();
        
        $SQL = "SELECT * FROM `groups_users_clients` `t` WHERE id_cliente ='".$IdCliente."' ";
        $list_g= $this->connection->createCommand($SQL)->queryAll();
        
        $SQL = "SELECT * FROM `licenses` `t`";
        $list_t= $this->connection->createCommand($SQL)->queryAll();
        
        foreach($list_t as $t){
            $lics[$t['id_licencia']] = $t['nombre'];
        }
        
        foreach($list_g as $g){
            $grupos[$g['id']] = $g['nombre'];
        }
        
	    $result = false;
	    $result = '';
	    foreach($list as $l){
	        if($l['status']=="A"){
	            $status =  "Pending";    
	        }elseif($l['status']=="F"){
	            $status =  "Completed";
	        }
	            $license = explode("V", $lics[$l['id_licencia']]);
	         $result .=   '<tr>
	                        <td>'.utf8_encode($license[0]).'</td>
                            <td><span style="font-size:11px;">'.utf8_encode($grupos[$l['id_grupo_cliente']]).'<span style="font-size:11px;"></td>
                            <td>'.$l['id_usuario'].'</td>
                            <td>'.utf8_encode($l['nombres']).'</td>
                            <td>'.utf8_encode($l['apellidos']).'</td>
                            <td>'.substr($l['fecha_presentacion'],0,10).'</td>
                            <td>'.round($l['calificacion'],0).'</td>
                            <td>'.$l['nivel'].'</td>
                            <td>'.$status.'</td>
                            <td><a title="View_Certificate" target="_blank" style="font-size:10px;" href="/ilto3/index.php?r=Clients/licensesUser/certificate&amp;id='.$l['id_licencias_usuario'].'"class="btn btn-warning btn-xs edit"><i class="fa fa-check"></i>Certificate</a>
                                                        
                            </tr>';
	        }
	   $Result['html'] = $result;
	   header('Content-Type: application/json');
	   //die(json_encode(array('html'=>implode('',$result))));
	   die(json_encode($Result));
	}
	
		public function actionTecs2goAdmin()
	{
		
		 ob_start();
	    
	    $IdCliente = $_POST['id_cliente'];
	    
	    
	    $where = '(users_c.id_cliente = "'.$IdCliente.'")';
	    
	    
	    if(isset($_REQUEST['id_user']) && strlen($_REQUEST['id_user'])>0){
	    	$where .= 'AND id_user like "'.$_REQUEST['id_user'].'%" ';
	    }
	    
	    if(isset($_REQUEST['status_request']) && strlen($_REQUEST['status_request'])>0){
	    	$where .= 'AND status_request = "'.$_REQUEST['status_request'].'" ';
	    }
	    
	    
	    $SQL = "SELECT id_proctorv_request, date_request, step_request, id_license, id_user, questions_step, photo_step, card_step, status_request, nombres, apellidos FROM `proctorv_request` INNER JOIN users_c ON proctorv_request.id_user = users_c.id_usuario_c WHERE $where  ORDER BY date_request DESC LIMIT 50";
        $proctorv_list= $this->connection->createCommand($SQL)->queryAll();
		
		
		$result = false;
	    $result = '';
	    //get the info result
	    foreach($proctorv_list as $request){
	    		
	    		$dat = explode(" ", $request["date_request"]); 
	    		
	    		//questions
                if($request["questions_step"]==1){
                	$questions_step = '<img src="/ilto3/images/iconos/verified.png" title="Done!" width="16">';
                }else{
                	$questions_step = '<img src="/ilto3/images/iconos/alert-warning.png" title="Pending!" width="16">'; 
                }
                
                //photo
                switch ($request["photo_step"]) {
                case 0:
                	$photo_step = '<img src="/ilto3/images/iconos/alert-warning.png" title="Pending Validation!" width="16">'; 
                break;
                case 1:
                	$photo_step = '<img src="/ilto3/images/iconos/verified.png" title="Done and Match!" width="16">&nbsp;<img src="/ilto3/images/iconos/verified.png" title="Done and Match!" width="16">';
                break;
                case 2:
                	$photo_step = '<img src="/ilto3/images/iconos/alert-danger.png" title="Pending Photo and Validation!" width="16">'; 
                break;
                case 3:
                	$photo_step = '<img src="/ilto3/images/iconos/verified.png" title="Validation Done" width="16"><img src="/ilto3/images/iconos/alert-warning.png" title="Pending Match!" width="16">';
                break;
                case 4:
                	$photo_step = '<img src="/ilto3/images/iconos/alert-danger.png" title="Match Failed" width="16">'; 
                break;
                default:
                // code...
                break;
                }
                
                //card_step
                switch ($request["card_step"]) {
                case 0:
                	$card_step = '<img src="/ilto3/images/iconos/alert-warning.png" title="Pending Validation!" width="16">'; 
                break;
                case 1:
                	$card_step =  '<img src="/ilto3/images/iconos/verified.png" title="Done" width="16">';
                break;
                case 2:
                	$card_step =  '<img src="/ilto3/images/iconos/alert-danger.png" title="Pending Photo and Validation!" width="16">'; 
                break;
                default:
                // code...
                break;
                }
                
                
                switch ($request["status_request"]) {
	               case 0:
                       $status_request = "PENDING VALIDATIONS";
                    break;
                    case 1:
                    	$status_request = "READY TO TAKE SPEAKING TEST";
                    break;
                    case 2:
                    	$status_request = "READY TO TAKE SPEAKING TEST";
                    break;
                    case 3:
                    	$status_request = "READY TO TAKE ONLINE SECTIONS";
                    break;
                    case 4:
                    	$status_request = "DONE";
                    break;
                    case 5:
                    	$status_request = "REVOKED";
                    break;
                    default:
                    // code...
                    break;
                }
                
                 //license
                if($request["id_license"] != NULL){
                	$license =  '<img src="/ilto3/images/iconos/verified.png" title="Allocation Done" width="16">'; 
                }else{
                	$license = '<img src="/ilto3/images/iconos/alert-warning.png" title="Pending Allocation" width="16">';
                }
                
            	//Speaking
                if($request["id_license"] != NULL){
                	$SQL = "SELECT * FROM exams_user WHERE id_licencia_usuario = '".$request["id_license"]."' ORDER BY id_examen_usuario ASC LIMIT 1";
                    $speaking = $this->connection->createCommand($SQL)->queryAll();
                                                        
                    switch($speaking[0]["estado"]){
                    	case 'A':
                        	$speaking_section = '<img src="/ilto3/images/iconos/alert-warning.png" title="Pending Speaking!" width="16">';
                        break;
                        case 'F':
                        	$speaking_section = '<img src="/ilto3/images/iconos/verified.png" title="Speaking Done" width="16">';
                        break;
                        case 'B':
                        	$speaking_section ='<img src="/ilto3/images/iconos/alert-danger.png" title="License REVOKED" width="16">'; 
                        break;
                	}
                }else {
                	$speaking_section = '<img src="/ilto3/images/iconos/alert-danger.png" title="Pending License allocation!" width="16">'; 
                }
                
                $result .='<tr>
                	<td>'.$dat[0].'</td>
                    <td>'.$request["id_user"].'</td>
                    <td>'.$request["nombres"].' '.$request["apellidos"].'</td>
                    <td>'.$questions_step.'</td>
                    <td>'.$photo_step.'</td>
                    <td>'.$card_step.'</td>
                    <td>'.$license.'</td>
                    <td>'.$speaking_section.'</td>
                    <td>'.$status_request.'</td>
                    <td><a href="/ilto3/index.php?r=Clients/LicensesUser/proctorvRequest/id/'.base64_encode($request["id_proctorv_request"]).'" class="btn btn-success" id="allocate">Admin</a>
                    </td>
                </tr>';
            }//foreach request
            
    	$result =  ob_get_contents();
        ob_end_clean();
	    $Result['html'] = $result;
	    header('Content-Type: application/json');
	    die(json_encode($Result));
	}
	
		public function actionTecs2goIltoAdmin()
	{
	    $IdCliente = $_POST['id_cliente'];
	    
	    $result = false;
	    $result = '';
	    if($idClient != '90043026471'){
	    	$where = ' id_cliente = "'.$IdCliente.'"';
	    	
	    	if(isset($_REQUEST['id_usuario_c']) && strlen($_REQUEST['id_usuario_c'])>0){
	        	$where .= 'AND id_user like "'.$_REQUEST['id_usuario_c'].'%" ';
	    	}
	    	
	    	$SQL = "SELECT id_proctorv_request, date_request, step_request, id_license, id_user, questions_step, photo_step, card_step, status_request, nombres, apellidos FROM `proctorv_request` INNER JOIN users_c ON proctorv_request.id_user = users_c.id_usuario_c WHERE $where ORDER BY date_request DESC";
        	$proctorv_list= $this->connection->createCommand($SQL)->queryAll();
			var_dump($proctorv_list);die();
        	//get the info result
	    	foreach($proctorv_list as $request){
	    		//date
	    		$dat = explode(" ", $request["date_request"]); 
	    		
	    		//questions
                if($request["questions_step"]==1){
                	$questions_step = '<img src="/ilto3/images/iconos/verified.png" title="Done!" width="16">';
                }else{
                	$questions_step = '<img src="/ilto3/images/iconos/alert-warning.png" title="Pending!" width="16">'; 
                }
                
                //photo
                switch ($request["photo_step"]) {
                case 0:
                	$photo_step = '<img src="/ilto3/images/iconos/alert-warning.png" title="Pending Validation!" width="16">'; 
                break;
                case 1:
                	$photo_step = '<img src="/ilto3/images/iconos/verified.png" title="Done and Match!" width="16">&nbsp;<img src="/ilto3/images/iconos/verified.png" title="Done and Match!" width="16">';
                break;
                case 2:
                	$photo_step = '<img src="/ilto3/images/iconos/alert-danger.png" title="Pending Photo and Validation!" width="16">'; 
                break;
                case 3:
                	$photo_step = '<img src="/ilto3/images/iconos/verified.png" title="Validation Done" width="16"><img src="/ilto3/images/iconos/alert-warning.png" title="Pending Match!" width="16">';
                break;
                case 4:
                	$photo_step = '<img src="/ilto3/images/iconos/alert-danger.png" title="Match Failed" width="16">'; 
                break;
                default:
                // code...
                break;
                }
                
                //card_step
                switch ($request["card_step"]) {
                case 0:
                	$card_step = '<img src="/ilto3/images/iconos/alert-warning.png" title="Pending Validation!" width="16">'; 
                break;
                case 1:
                	$card_step =  '<img src="/ilto3/images/iconos/verified.png" title="Done" width="16">';
                break;
                case 2:
                	$card_step =  '<img src="/ilto3/images/iconos/alert-danger.png" title="Pending Photo and Validation!" width="16">'; 
                break;
                default:
                // code...
                break;
                }
                
                
                switch ($request["status_request"]) {
                case 0:
                	$status_request =  "PENDING";
                break;
                case 1:
                	$status_request = "PENDING";
                break;
                case 2:
                	$status_request = "READY TO TAKE TEST";
                break;
                case 3:
                	$status_request = "DONE";
                break;
                case 4:
                	$status_request = "REVOKED";
                break;
                default:
                // code...
                break;
                }
                
                 //license
                if($request["id_license"] != NULL){
                	$license =  '<img src="/ilto3/images/iconos/verified.png" title="Allocation Done" width="16">'; 
                }else{
                	$license = '<img src="/ilto3/images/iconos/alert-warning.png" title="Pending Allocation" width="16">';
                }
                
            	//Speaking
                if($request["id_license"] != NULL){
                	$SQL = "SELECT * FROM exams_user WHERE id_licencia_usuario = '".$request["id_license"]."' ORDER BY id_examen_usuario ASC LIMIT 1";
                    $speaking = $this->connection->createCommand($SQL)->queryAll();
                                                        
                    switch($speaking[0]["estado"]){
                    	case 'A':
                        	$speaking_section = '<img src="/ilto3/images/iconos/alert-warning.png" title="Pending Speaking!" width="16">';
                        break;
                        case 'F':
                        	$speaking_section = '<img src="/ilto3/images/iconos/verified.png" title="Speaking Done" width="16">';
                        break;
                        case 'B':
                        	$speaking_section ='<img src="/ilto3/images/iconos/alert-danger.png" title="License REVOKED" width="16">'; 
                        break;
                	}
                }else {
                	$speaking_section = '<img src="/ilto3/images/iconos/alert-danger.png" title="Pending License allocation!" width="16">'; 
                }
                
                $result .= '<tr role="row">
                	<th>'.$dat[0].'</th>
                    <th>'.$request["id_user"].'</th>
                    <th>'.$request["nombres"].'</th>
                    <th>'.$request["apellidos"].'</th>
                    <th>'.$questions_step.'</th>
                    <th>'.$photo_step.'</th>
                    <th>'.$card_step.'</th>
                    <th>'.$license.'</th>
                    <th>'.$speaking_section.'</th>
                    <th>'.$status_request.'</th>
                    <th><a href="/ilto3/index.php?r=Clients/LicensesUser/proctorvRequest/id/'.base64_encode($request["id_proctorv_request"]).'" class="btn btn-success" id="allocate">Admin</a>
                    </th>
                </tr>';
	    	}
	    	
	    	
	    }else{
	    	
	    	if(isset($_REQUEST['id_usuario_c']) && strlen($_REQUEST['id_usuario_c'])>0){
	        	$where .= 'AND id_user like "'.$_REQUEST['id_usuario_c'].'%" ';
	    	}
	    	
	    	
	    	$SQL = "SELECT id_proctorv_request, date_request, step_request, id_license, id_user, questions_step, photo_step, card_step, status_request, nombres, apellidos FROM `proctorv_request` INNER JOIN users_c ON proctorv_request.id_user = users_c.id_usuario_c WHERE $where ORDER BY date_request DESC";
        	$proctorv_list= $this->connection->createCommand($SQL)->queryAll();

	    	//get the info result
	    	foreach($proctorv_list as $request){
	    		//date
	    		$dat = explode(" ", $request["date_request"]); 
	    		
	    		//questions
                if($request["questions_step"]==1){
                	$questions_step = '<img src="/ilto3/images/iconos/verified.png" title="Done!" width="16">';
                }else{
                	$questions_step = '<img src="/ilto3/images/iconos/alert-warning.png" title="Pending!" width="16">'; 
                }
                
                //photo
                switch ($request["photo_step"]) {
                case 0:
                	$photo_step = '<img src="/ilto3/images/iconos/alert-warning.png" title="Pending Validation!" width="16">'; 
                break;
                case 1:
                	$photo_step = '<img src="/ilto3/images/iconos/verified.png" title="Done and Match!" width="16">&nbsp;<img src="/ilto3/images/iconos/verified.png" title="Done and Match!" width="16">';
                break;
                case 2:
                	$photo_step = '<img src="/ilto3/images/iconos/alert-danger.png" title="Pending Photo and Validation!" width="16">'; 
                break;
                case 3:
                	$photo_step = '<img src="/ilto3/images/iconos/verified.png" title="Validation Done" width="16"><img src="/ilto3/images/iconos/alert-warning.png" title="Pending Match!" width="16">';
                break;
                case 4:
                	$photo_step = '<img src="/ilto3/images/iconos/alert-danger.png" title="Match Failed" width="16">'; 
                break;
                default:
                // code...
                break;
                }
                
                //card_step
                switch ($request["card_step"]) {
                case 0:
                	$card_step = '<img src="/ilto3/images/iconos/alert-warning.png" title="Pending Validation!" width="16">'; 
                break;
                case 1:
                	$card_step =  '<img src="/ilto3/images/iconos/verified.png" title="Done" width="16">';
                break;
                case 2:
                	$card_step =  '<img src="/ilto3/images/iconos/alert-danger.png" title="Pending Photo and Validation!" width="16">'; 
                break;
                default:
                // code...
                break;
                }
                
               
                
                switch ($request["status_request"]) {
                case 0:
                	$status_request =  "PENDING";
                break;
                case 1:
                	$status_request = "PENDING";
                break;
                case 2:
                	$status_request = "READY TO TAKE TEST";
                break;
                case 3:
                	$status_request = "DONE";
                break;
                case 4:
                	$status_request = "REVOKED";
                break;
                default:
                // code...
                break;
                }
                
	    		
                $result .= '<tr role="row">
                	<th>'.$dat[0].'</th>
                    <th>'.$request["id_user"].'</th>
                    <th>'.$request["nombres"].'</th>
                    <th>'.$request["apellidos"].'</th>
                    <th>'.$questions_step.'</th>
                    <th>'.$photo_step.'</th>
                    <th>'.$card_step.'</th>
                    <th>'.$request["nombre_rsocial"].'</th>
                    <th>'.$status_request.'</th>
                    <th><a href="/ilto3/index.php?r=Clients/LicensesUser/proctorvRequest/id/'.base64_encode($request["id_proctorv_request"]).'" class="btn btn-success" id="allocate">Admin</a>
                    </th>
                </tr>';
            }//foreach request
            
	    }//else
	    
	   $Result['html'] = $result;
	   header('Content-Type: application/json');
	   //die(json_encode(array('html'=>implode('',$result))));
	   die(json_encode($Result));
	}
	

    public function actionOldReport(){
    
        $IdCliente = $_POST['id_cliente'];

        if(isset($_REQUEST['code']) && strlen($_REQUEST['code'])>0){
	        $where .= ' and code like "'.$_REQUEST['code'].'%" ';
	    }
	    
	    
        if(isset($_REQUEST['name']) && strlen($_REQUEST['name'])>0){
	        $where .= ' and name like "'.$_REQUEST['name'].'%" ';
	    }
	    
	    if(isset($_REQUEST['cef']) && strlen($_REQUEST['cef'])>0){
	        $where .= ' and cef like "'.$_REQUEST['cef'].'%" ';
	    }
	    
    	    
        $SQL = "SELECT * FROM `old_platform` WHERE `client` LIKE '$IdCliente' $where LIMIT 100";
        $list_old= $this->connection->createCommand($SQL)->queryAll();
	    
	    $result = null;
	
	    foreach($list_old as $reg){
        $pdf = ".pdf";
        $code = $reg['code'].$pdf;   
        $result[] = '<tr>
                <td>'.$reg['code'].'</td>
                <td>'.$reg['name'].'</td>
                <td>'.$reg['exam_date'].'</td>
                <td>'.$reg['score'].'</td>
                <td>'.$reg['cef'].'</td>
                <td><a title="View_Certificate" target="_blank" style="font-size:10px;" href="/ilto3/../newtest2/examspdf/results'.$code.'" class="btn btn-warning btn-xs edit"><i class="fa fa-check"></i>Certificate</a>
                </td>      
            </tr>';
        }
    
    header('Content-Type: application/json');
	die(json_encode(array('html'=>implode('',$result))));
	    
    }
    
    
	public function actionCountries()
	{
	    var_dump($_REQUEST);
	    die('countries!!');
	}
	
	public function actionCities()
	{
	    var_dump($_REQUEST);
	    die('cities!!');
	}
	
	public function actionRoom()
	{
	    
	    $where = '';
	    
	    if(isset($_REQUEST['description']) && strlen($_REQUEST['description'])>0){
	        $where .= ' and nombre like "'.$_REQUEST['description'].'%" ';
	    }
	    
	    if(isset($_REQUEST['ubicacion']) && strlen($_REQUEST['ubicacion'])>0){
	        $where .= ' and ubicacion like "'.$_REQUEST['ubicacion'].'%" ';
	    }
	    
	    if(isset($_REQUEST['id']) && strlen($_REQUEST['id'])>0){
	        $where .= ' and id like "'.$_REQUEST['id'].'%" ';
	    }
	    
	    if(isset($_REQUEST['capacidad']) && strlen($_REQUEST['capacidad'])>0){
	        $where .= ' and capacidad like "'.$_REQUEST['capacidad'].'%" ';
	    }
	    
	    if(isset($_REQUEST['estado']) && strlen($_REQUEST['estado'])>0){
	        $where .= ' and estado = "'.$_REQUEST['estado'].'" ';
	    }
	    
	    
        $IdCliente = $_POST['id_cliente'];
        $SQL = "SELECT * FROM `class_rooms` `t` WHERE id_cliente='".$IdCliente."' $where order by id DESC LIMIT 50";
        // var_dump($SQL);
        $list= $this->connection->createCommand($SQL)->queryAll();
        //var_dump($list);
	    //var_dump($_REQUEST);
	    $Result = false;
	    $result = '';
	    $Status = array('A'=>'ENABLE', 'I'=>'DISABLE');
	    foreach($list as $l){
	        $SQL = "SELECT COUNT(*) as total FROM `exams_user` WHERE `salon`='".$l['id']."' ";
            $LicensesCount = $this->connection->createCommand($SQL)->queryAll();   
            //var_dump($LicensesCount[0]['total']);
            if($LicensesCount[0]['total']==0){
                $result .= ' <tr>
                            <td>'.$l['nombre'].'</td>
                            <td>'.$l['ubicacion'].'</td>
                            <td class="center ">'.$l['capacidad'].'</td>
                            <td class="center ">'.$Status[$l['estado']].'</td>
                            <td>
                                <a href="/ilto3/index.php?r=Clients/classRooms/update&id='.$l['id'].'" class="btn btn-info btn-xs edit"><i class="fa fa-edit"></i> Edit</a>
                                <a href="/ilto3/index.php?r=Clients/classRooms/delete&id='.$l['id'].'" class="btn btn-danger btn-xs delete"><i class="fa fa-trash-o"></i> Delete</a>
                            </td>
                        </tr>';                    
            }else {
                   
	         $result .= ' <tr>
                            <td>'.$l['nombre'].'</td>
                            <td>'.$l['ubicacion'].'</td>
                            <td class="center ">'.$l['capacidad'].'</td>
                            <td class="center ">'.$Status[$l['estado']].'</td>
                            <td>
                                <a href="/ilto3/index.php?r=Clients/classRooms/update&id='.$l['id'].'" class="btn btn-info btn-xs edit"><i class="fa fa-edit"></i> Edit</a>
                            </td>
                        </tr>';
                
            }
	    }
	   
	   $Result['html'] = $result;
	   header('Content-Type: application/json');
	   die(json_encode($Result));
	}
	
	public function actionGetIdExam(){
        
        
        $IdCliente = $_POST['id_cliente'];

        if(isset($_REQUEST['code']) && strlen($_REQUEST['code'])>0){
	        $where .= ' and code like "'.$_REQUEST['code'].'%" ';
	    }
	    
	    
        if(isset($_REQUEST['name']) && strlen($_REQUEST['name'])>0){
	        $where .= ' and name like "'.$_REQUEST['name'].'%" ';
	    }
	    
	    if(isset($_REQUEST['cef']) && strlen($_REQUEST['cef'])>0){
	        $where .= ' and cef like "'.$_REQUEST['cef'].'%" ';
	    }
	    
    	    
        $SQL = "SELECT * FROM `old_platform` WHERE `client` LIKE '$IdCliente' $where LIMIT 100";
        $list_old= $this->connection->createCommand($SQL)->queryAll();
	    
	    $result = null;
	
	    foreach($list_old as $reg){
        $pdf = ".pdf";
        $code = $reg['code'].$pdf;   
        $result[] = '<tr>
                <td>'.$reg['code'].'</td>
                <td>'.$reg['name'].'</td>
                <td>'.$reg['exam_date'].'</td>
                <td>'.$reg['score'].'</td>
                <td>'.$reg['cef'].'</td>
                <td><a title="View_Certificate" target="_blank" style="font-size:10px;" href="/ilto3/../newtest2/examspdf/results'.$code.'" class="btn btn-warning btn-xs edit"><i class="fa fa-check"></i>Certificate</a>
                </td>      
            </tr>';
        }
    
    header('Content-Type: application/json');
	die(json_encode(array('html'=>implode('',$result))));
	    
    }
    
    public function actionGetPrograms()
	{
	    $SQL = "SELECT programs.id_program, programs.name, program_school_city.id_school 
                FROM `programs` 
                INNER JOIN program_school_city
                ON programs.id_program = program_school_city.id_program
                WHERE program_school_city.id_city = '".$_REQUEST['id']."' ORDER BY name ASC";
	    $programs= $this->connection->createCommand($SQL)->queryAll();
	    
	    $Result = '<select required="required" style="width:100%" class="form-control" id="program" name="program">
                    <option value="">Choose your School</option>';
                                            
	    foreach ($programs as $pg) {
            $Result .= '<option value="'.$pg['id_program'].'">'.ucfirst(utf8_encode($pg['name'])).'</option>';
        }
        
        $Result .= '</select>';
	   header('Content-Type: application/json');
	   die($Result);
	}
	
	public function actionGetProgramsPse()
	{
	    $SQL = "SELECT programs.id_program, programs.name, program_school_city.id_school 
                FROM `programs` 
                INNER JOIN program_school_city
                ON programs.id_program = program_school_city.id_program
                WHERE program_school_city.id_city = '".$_REQUEST['id']."' ORDER BY name ASC";
	    $programs= $this->connection->createCommand($SQL)->queryAll();
	    
	    $Result = '<select required style="width:100%" class="form-control input-lg" id="referenceCode" name="referenceCode">
                    <option value=""></option>';
                                            
	    foreach ($programs as $pg) {
            $Result .= '<option value="'.$_REQUEST['id_number']."_".$pg['id_program']."_".date("h:i:s").'">'.ucfirst(utf8_encode($pg['name'])).'</option>';
        }
        
        $Result .= '</select>';
	   header('Content-Type: application/json');
	   die($Result);
	}
	
	public function actiongetMd5TecsPayment()
	{
	    $ApiKey = $_REQUEST['apiKey'];
	    $merchantId = $_REQUEST['merchantId'];
	    $referenceCode = $_REQUEST['referenceCode'];
	    $amount = $_REQUEST['amount'];
	    $currency = $_REQUEST['currency'];
	    
	    $firma = "$ApiKey~$merchantId~$referenceCode~$amount~$currency";
		
		$firmaMd5 = md5($firma);
	    
	    $Result = $firmaMd5;
	    
	   header('Content-Type: application/json');
	   die($Result);
	}
	
	
	
	public function actionGetProgramsByRole()
	{
	    $SQL = "SELECT programs.id_program, programs.name, program_school_city.id_school 
                FROM `programs` 
                INNER JOIN program_school_city
                ON programs.id_program = program_school_city.id_program
                WHERE program_school_city.id_city = '".$_REQUEST['id']."'";
	    $programs= $this->connection->createCommand($SQL)->queryAll();
	    
	    $Result = '<select required="required" style="width:100%" class="form-control" id="program" name="program">
                    <option value="">Choose your School</option>';
                                            
	    foreach ($programs as $pg) {
            $Result .= '<option value="'.$pg['id_program'].'">'.ucfirst(utf8_encode($pg['name'])).'</option>';
        }
        
        $Result .= '</select>';
	   header('Content-Type: application/json');
	   die($Result);
	}
	
	public function actionGetSalesbyDate()
	{
	    $currentMonth = date('n');
	    
	    
	    // revisamo los requerimientos del Ajax y validamos, si viene la variable concatenamos la condicion a la consulta
	    if(isset($_REQUEST['status']) && strlen($_REQUEST['status'])>0){
	        $where .= ' and licenses_client.estado = "'.$_REQUEST['status'].'" ';
	    }
	    
	    
	    if(isset($_REQUEST['client']) && strlen($_REQUEST['client'])>0){
	        $where .= ' AND licenses_client.id_cliente = "'.$_REQUEST['client'].'" ';
	    }
	    
	    
	    $SQL = "SELECT licenses_client.id_cliente, licenses_client.cantidad, licenses_client.utilizados, licenses_client.estado, nombre, nombre_rsocial, fecha_asignacion, fecha_final  FROM `licenses_client` 
        	INNER JOIN clients
        	ON licenses_client.id_cliente = clients.id_cliente 
        	INNER JOIN licenses
        	ON licenses_client.id_licencia = licenses.id_licencia
        	WHERE demo = 0
        	$where 
        	ORDER BY fecha_final DESC, nombre DESC";
        
       
	    $list= $this->connection->createCommand($SQL)->queryAll();
	    
	   
	    
	    //Sacar el total de licencias por la consulta
	    switch ($total) {
            case 0:
                $SQL = "SELECT SUM(licenses_client.cantidad), nombre  FROM `licenses_client` 
                INNER JOIN clients
                ON licenses_client.id_cliente = clients.id_cliente 
                INNER JOIN licenses
                ON licenses_client.id_licencia = licenses.id_licencia
                WHERE (fecha_asignacion BETWEEN '2016-01-01' AND NOW())
                ".$andClient."
                AND demo = 0
                GROUP BY nombre
                ORDER BY fecha_asignacion DESC";
                $totaLic= $this->connection->createCommand($SQL)->queryAll();
                break;
            case 1:
                $SQL = "SELECT  SUM(licenses_client.cantidad), nombre FROM `licenses_client` 
                INNER JOIN clients
                ON licenses_client.id_cliente = clients.id_cliente 
                INNER JOIN licenses
                ON licenses_client.id_licencia = licenses.id_licencia
                WHERE (fecha_asignacion BETWEEN '".$from."' AND NOW())
                ".$andClient."
                AND demo = 0
                GROUP BY nombre
                ORDER BY fecha_asignacion DESC";
                $totaLic= $this->connection->createCommand($SQL)->queryAll();
                break;
            case 2:
                $SQL = "SUM(licenses_client.cantidad), nombre  FROM `licenses_client` 
                INNER JOIN clients
                ON licenses_client.id_cliente = clients.id_cliente 
                INNER JOIN licenses
                ON licenses_client.id_licencia = licenses.id_licencia
                WHERE (fecha_asignacion BETWEEN '2016-01-01' AND '".$to."')
                ".$andClient."
                AND demo = 0
                GROUP BY nombre
                ORDER BY fecha_asignacion DESC";
                $totaLic= $this->connection->createCommand($SQL)->queryAll();
                break;
            case 3:
                $SQL = "SELECT  SUM(licenses_client.cantidad), nombre  FROM `licenses_client` 
                INNER JOIN clients
                ON licenses_client.id_cliente = clients.id_cliente 
                INNER JOIN licenses
                ON licenses_client.id_licencia = licenses.id_licencia
                WHERE (fecha_asignacion BETWEEN '".$from."' AND '".$to."')
                ".$andClient."
                AND demo = 0
                GROUP BY nombre
                ORDER BY fecha_asignacion DESC";
                $totaLic= $this->connection->createCommand($SQL)->queryAll();
                break;
            default:
                echo "There is not records for total.";
        }
	    
	    $Result = '<table class="tbl_admin" id="month">
		<thead>
			<tr>
				<th>
			    	License
			    </th>
			    <th>
			    	Quantity
			    </th>
			    <th>
			    	Used
			    </th>
			    <th>
			    	Available
			    </th>
			    <th>
			    	Status
			    </th>
			    <th>
			    	Allocation date
			    </th>
			    <th>
			    	Available before at
			    </th>
		   	</tr>
		</thead>
		<tbody >';
	    
	    foreach($list as $item){
	        switch ($item['estado']) {
                case 'A':
                    $status = '<span style="font-weight:bold;color:#39b54a;">AVAILABLE</span>';
                    break;
                 case 'F':
                    $status = '<span style="font-weight:bold;color:black;">FINISHED</span>';
                    break;
                 case 'E':
                    $status = '<span style="font-weight:bold;color:red;">EXPIRED</span>';
                    break;
                default:
                    echo "There is not records for total.";
            }
	        
	        $Result .='<tr><td>'.$item['nombre'].'</td>
			                <td>'.$item['cantidad'].'</td>
			                <td>'.$item['utilizados'].'</td>
			                <td>'.($item['cantidad'] -$item['utilizados']) .'</td>
			                <td>'.$status.'</td>
			                <td>'.substr($item['fecha_asignacion'],0,10).'</td>
			                <td>'.substr($item['fecha_final'],0,10).'</td>
		           </tr>';
	    }
	    
	    foreach($totaLic as $tot){
	        $Result .= '<tr><td colspan="2"><b>'.$tot["nombre"].'</b></td><td colspan="2"><b>TOTAL: '.$tot["SUM(licenses_client.cantidad)"].'</b></td></tr>';
	    }
	    
	    $Result .= '</tbody></table>';
	    
	    if(empty($list)){
	         $Result = "There is not results.";
	    }
	    
		            
        
	   
	   header('Content-Type: application/json');
	   die($Result);
	}
	
	// TECS report
	public function actionTecsReport(){
        
          
        // output headers so that the file is downloaded rather than displayed
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=list-report-'.date('Y_m_d_H_i_s').'.csv');
        
        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');
        
        $modUtil = new Utilidades();
        $tecs_id = $modUtil->getIdTecs();
        
        // TECS PREGUNTAS
        $SQL = "SELECT exams_license.id_examen, nombre FROM `exams_license` INNER JOIN exams ON exams_license.id_examen = exams.id_examen WHERE `id_licencia` = '15'";
        $tecs_exams = $this->connection->createCommand($SQL)->queryAll();
        fputcsv($output, array('RESPUESTA','CANTIDAD','ES_VERDADERA','PREGUNTA','ID_PREGUNTA','NULL','TOTAL','CANTIDAD','EXAMEN'));
        
        foreach($tecs_exams as $exam){
            // TECS PREGUNTAS
            $SQL = "SELECT questions.id_pregunta, questions.nivel, questions.texto, exams.nombre FROM `questions_exam` INNER JOIN questions ON questions_exam.id_pregunta = questions.id_pregunta INNER JOIN exams ON questions_exam.id_examen = exams.id_examen WHERE questions_exam.id_examen = '".$exam["id_examen"]."' ";
            $questions = $this->connection->createCommand($SQL)->queryAll();
            
            //TECS RESPUESTAS 
            //respuestas speaking
            if($exam["id_examen"] == '23'){
                
                foreach ($questions as $question) {
                    //$SQL = "SELECT id_pregunta, COUNT(escala) AS cantidad, escala FROM `answers_exam_user` WHERE `id_pregunta` = '".$question["id_pregunta"]."' GROUP BY escala";
                    //$answers = $this->connection->createCommand($SQL)->queryAll();
                }
                
            }else{
                foreach ($questions as $question) {
                    $question["id_pregunta"] =  $question["id_pregunta"];
                    $question["nivel"] = $question["nivel"];
                    $question["nivel"] = $question["nivel"];
                    $question["texto"] = $question["texto"];
                    $question["nombre"] = $question["nombre"];
                    
                    $SQL = "SELECT texto, COUNT(answers_exam_user.id_respuesta) as CANTIDAD, es_verdadera FROM `answers_exam_user` INNER JOIN answers ON answers_exam_user.id_respuesta = answers.id_respuesta WHERE answers_exam_user.id_pregunta = '".$question["id_pregunta"]."' GROUP BY answers_exam_user.id_respuesta";
                    $answers = $this->connection->createCommand($SQL)->queryAll();
                    
                    
                    $SQL = "SELECT COUNT(id_respuesta_examen_usuario) as blank FROM `answers_exam_user` WHERE `id_pregunta` = '".$question["id_pregunta"]."' AND `id_respuesta` IS NULL";
                    $null = $this->connection->createCommand($SQL)->queryAll();
                    
                    $SQL = "SELECT COUNT(id_respuesta_examen_usuario) as total FROM `answers_exam_user` WHERE `id_pregunta` = '".$question["id_pregunta"]."'";
                    $total = $this->connection->createCommand($SQL)->queryAll();
                    
                    foreach ($answers as $answer) {
                        
                        $answer["pregunta"] = $question["texto"];
                        $answer["id_pregunta"] = $question["id_pregunta"];
                        $answer["blank"] = $null[0]["blank"];
                        $answer["total"] = $total[0]["total"];
                        $answer["texto"] = trim(preg_replace('/\s+/', ' ', $answer["texto"]));
                        $answer["cantidad"] = $answer["CANTIDAD"];
                        $answer["es_verdadera"] = $answer["es_verdadera"];
                        $answer["examen"] = $exam["nombre"];
                        //var_dump($answer);
                        fputcsv($output, $answer);
                    }
                }
                
            }
        }
    
	}
	
	// TECS report
	public function actionTecsUnivalle_1(){
     
        // output headers so that the file is downloaded rather than displayed
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=list-report-'.date('Y_m_d_H_i_s').'.csv');
        
        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');
        $modUtil = new Utilidades();
        $tecs_id = $modUtil->getIdTecs();
        
            // TECS PREGUNTAS
        $SQL = "SELECT exams_license.id_examen, nombre, exams_user.id_examen_usuario FROM `exams_license` INNER JOIN exams ON exams_license.id_examen = exams.id_examen INNER JOIN exams_user ON exams_license.id_examen = exams_user.id_examen WHERE `id_licencia` = '15' AND exams_user.id_licencia_usuario = '35273'";
        $tecs_exams = $this->connection->createCommand($SQL)->queryAll();
        fputcsv($output, array('RESPUESTA','ES_VERDADERA','ID_RESPUESTA','PREGUNTA','ID_PREGUNTA','EXAMEN'));
        
        foreach($tecs_exams as $exam){
            // TECS PREGUNTAS
            $SQL = "SELECT questions.id_pregunta, questions.nivel, questions.texto, exams.nombre FROM `questions_exam` INNER JOIN questions ON questions_exam.id_pregunta = questions.id_pregunta INNER JOIN exams ON questions_exam.id_examen = exams.id_examen INNER JOIN answers_exam_user ON questions_exam.id_pregunta = answers_exam_user.id_pregunta WHERE answers_exam_user.id_examen_usuario = '".$exam["id_examen_usuario"]."' ";
            $questions = $this->connection->createCommand($SQL)->queryAll();
            
            //TECS RESPUESTAS 
            //respuestas speaking
            if($exam["id_examen"] == '23'){
                
                foreach ($questions as $question) {
                    //$SQL = "SELECT id_pregunta, COUNT(escala) AS cantidad, escala FROM `answers_exam_user` WHERE `id_pregunta` = '".$question["id_pregunta"]."' GROUP BY escala";
                    //$answers = $this->connection->createCommand($SQL)->queryAll();
                }
                
            }else{
                foreach ($questions as $question) {
                    $question["id_pregunta"] =  $question["id_pregunta"];
                    $question["nivel"] = $question["nivel"];
                    $question["nivel"] = $question["nivel"];
                    $question["texto"] = $question["texto"];
                    $question["nombre"] = $question["nombre"];
                    
                    $SQL = "SELECT texto, es_verdadera, answers_exam_user.id_respuesta FROM `answers_exam_user` INNER JOIN answers ON answers_exam_user.id_respuesta = answers.id_respuesta WHERE answers_exam_user.id_pregunta = '".$question["id_pregunta"]."' GROUP BY answers_exam_user.id_respuesta";
                    $answers = $this->connection->createCommand($SQL)->queryAll();
                    
                    
                    $SQL = "SELECT COUNT(id_respuesta_examen_usuario) as blank FROM `answers_exam_user` WHERE `id_pregunta` = '".$question["id_pregunta"]."' AND `id_respuesta` IS NULL";
                    $null = $this->connection->createCommand($SQL)->queryAll();
                    
                    $SQL = "SELECT COUNT(id_respuesta_examen_usuario) as total FROM `answers_exam_user` WHERE `id_pregunta` = '".$question["id_pregunta"]."'";
                    $total = $this->connection->createCommand($SQL)->queryAll();
                    
                    foreach ($answers as $answer) {
                        
                        $answer["pregunta"] = $question["texto"];
                        $answer["id_pregunta"] = $question["id_pregunta"];
                        $answer["id_respuesta"] = $answer["id_respuesta"];
                        $answer["texto"] = trim(preg_replace('/\s+/', ' ', $answer["texto"]));
                        $answer["es_verdadera"] = $answer["es_verdadera"];
                        $answer["examen"] = $exam["nombre"];
                        //var_dump($answer);
                        fputcsv($output, $answer);
                    }
                }
                
            }
        }
    
	}
	
	// TECS report
	public function actionTecsByExams(){
     
        // output headers so that the file is downloaded rather than displayed
       /*header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=list-report-'.date('Y_m_d_H_i_s').'.csv');
        
        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');
        */$modUtil = new Utilidades();
        $tecs_id = $modUtil->getIdTecs();
        
            // TECS PREGUNTAS
        $SQL = "SELECT id_licencias_usuario, id_usuario, nombres, apellidos, licenses_user.fecha_asignacion, licenses_user.fecha_presentacion, calificacion, nivel, licenses_user.estado FROM `licenses_user` INNER JOIN users_c ON licenses_user.id_usuario = users_c.id_usuario_c INNER JOIN licenses_client ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente WHERE licenses_client.id_cliente = '8605173021' AND licenses_client.id_licencia IN (7,15,19)";
        $tecs_exams = $this->connection->createCommand($SQL)->queryAll();
        //fputcsv($output, array('ID_LICENSE','ID_USER','NAME','SURENAME','ALLOCATE_DATE','PRESENTATION_DATE', 'SCORE', 'CEFR', 'STATUS', 'SPEAKING', 'GRAMMAR', 'READING', 'LISTENING'));
        
        foreach($tecs_exams as $item){
            
            $item['id_licencias_usuario'] = $item["id_licencias_usuario"];
            $item['id_usuario'] = $item["id_usuario"];
            $item['nombres'] =  strtolower(utf8_encode($item['nombres']));
            $item['apellidos'] =  strtolower(utf8_encode($item['apellidos']));
            $item['fecha_asignacion'] =  $item['fecha_asignacion'];
            $item['fecha_presentacion'] =  $item['fecha_presentacion'];
            $item['calificacion'] =  $item['calificacion'];
            $item['nivel'] =  $item['nivel'];
            $item['estado'] =  $item['estado'];
            
            // TECS PREGUNTAS
            $SQL = "SELECT calificacion FROM `exams_user`  WHERE id_licencia_usuario = '".$item["id_licencias_usuario"]."' ORDER BY id_examen ASC";
            $exams = $this->connection->createCommand($SQL)->queryAll();
            
            echo $item['id_licencias_usuario']. ", " .$item['id_usuario']. ", " .$item['nombres']. ", " .$item['apellidos']. ", " .$item['fecha_asignacion']. ", " .$item['fecha_asignacion']. ", " .$item['calificacion']. ", " .$item['nivel']. ", " .$item['estado'];
            foreach($exams as $exam){
                echo ", ".$exam["calificacion"];
            }
            
            /*$item['speaking'] = $item($exam[0]["calificacion"]);
            $item['grammar'] = $exam[1]["calificacion"];
            $item['reading'] = $exam[2]["calificacion"];
            $item['listening'] = $exam[3]["calificacion"];
            //fputcsv($output, $item); */
            echo "<br/>";
        }
       
        //die();
	}
	
	// TECS report
	public function actionTecsUnivalle(){
     
        // output headers so that the file is downloaded rather than displayed
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=list-report-'.date('Y_m_d_H_i_s').'.csv');
        
        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');
        $modUtil = new Utilidades();
        
        // Seleccionamos las licencias terminadas y que sean tecs 4.0
        $SQL = "SELECT `id_usuario`, `id_licencias_usuario` 
                FROM `licenses_user`
                INNER JOIN licenses_client 
                ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente
                 WHERE licenses_user.`estado` = 'F'
                AND licenses_client.id_licencia = 15;";
        $list_l= $this->connection->createCommand($SQL)->queryAll();
        
        
        $questions = array("Examinee");
        // armar el encabezado
        $SQL = "SELECT id_pregunta FROM `questions_exam` WHERE `id_examen` IN (26,27,28) ORDER BY id_examen ASC;";
        $list_p= $this->connection->createCommand($SQL)->queryAll();
        
        foreach($list_p as $pregunta){
            array_push($questions, "item-".$pregunta["id_pregunta"]);
        }
	
        
        fputcsv($output, $questions);
        
        foreach ($list_l as $licencia) {
            // por cada licencia nos traemoss los examenes que tienen
            $SQL = "SELECT * FROM `exams_user` WHERE `id_licencia_usuario` = '".$licencia["id_licencias_usuario"]."'  AND id_examen != 23 ORDER BY id_examen_usuario ASC ";
            $list_e= $this->connection->createCommand($SQL)->queryAll();
            $data = array($licencia["id_usuario"]);
            
            foreach ($list_e as $exams) {
                // por cada licencia nos traemoss los examenes que tienen
                $SQL = "SELECT id_pregunta, id_respuesta FROM `answers_exam_user` WHERE `id_examen_usuario` = '".$exams["id_examen_usuario"]."'  ORDER BY id_pregunta ASC";
                $list_a= $this->connection->createCommand($SQL)->queryAll();
                
                foreach ($list_a as $answer) {
                    array_push($data,$answer["id_respuesta"]);
                }
            }
            fputcsv($output, $data);
        }    
    }
    
    public function actionCertificateValidate()
	{
	    $id = $_REQUEST['id'];
	    $fraudDecrypt = $_REQUEST['code'];
        $fraudEncrypt = $_REQUEST['code'];
        $superscorePass = $_REQUEST['id'];
    	$superscore = $_REQUEST['superscore'];
	    $fraudDecrypt =  "1".substr($fraudDecrypt,1);
	    
	    
	    //SUPERSCORE QUERY
	    if($superscore== 1){
	    	
	    	if($id != $superscorePass){
	    		//si no se ha generado un superScore o no tiene 
		            $result = '<div class="row" style="padding:10px; text-align:left;">
		                            <div class="col-sm-2">
		                                
		                            </div>
		                            <div class="col-sm-2">
		                                <img src="/img/error.png" width="64" style="display:inline;"/>
		                            </div>
		                            <div class="col-sm-8">
		                               <h2>THIS IS NOT A VALID CERTIFICATE.</h2> 
		                            </div>
		                        </div>';
	    	}else{
	    		// get taker and licenses information from Fraud Prevention Code
		        $SQL = "SELECT id_examen_usuario FROM `exams_superscore` WHERE `id_user` LIKE '".$id."'";
		        $exams_list= $this->connection->createCommand($SQL)->queryAll();
		        
		        //si es SAN MATEO no permitimos
		    	$SQL = "SELECT id_cliente FROM `users_c` WHERE `id_usuario_c` LIKE '".$id."'";
		    	$client = $this->connection->createCommand($SQL)->queryAll();
		    	if($client[0]["id_cliente"]=='800040295'){
		    		$exams_list = array();
		    	}
		       
		        if(empty($exams_list)){
		        	
		        	//si no se ha generado un superScore o no tiene 
		            $result = '<div class="row" style="padding:10px; text-align:left;">
		                            <div class="col-sm-2">
		                                
		                            </div>
		                            <div class="col-sm-2">
		                                <img src="/img/error.png" width="64" style="display:inline;"/>
		                            </div>
		                            <div class="col-sm-8">
		                               <h2>THIS IS NOT A VALID CERTIFICATE.</h2> 
		                            </div>
		                        </div>';
		        }else{
		        	
		            $SQL = "SELECT nombres, apellidos  
		                FROM `users_c` 
		                WHERE `id_usuario_c` = '".$id."'";
		            	$list= $this->connection->createCommand($SQL)->queryAll();
		            
		            
		            
		            foreach($exams_list as $exam){
		            	$CONT=$CONT+1;
		            	$SQL = "SELECT nombre, calificacion, exams_user.nivel, fecha_presentacion  
		                FROM `exams_user` 
		                INNER JOIN exams ON exams_user.id_examen = exams.id_examen
		                WHERE `id_examen_usuario` = '".$exam["id_examen_usuario"]."'";
		            	$exam_info= $this->connection->createCommand($SQL)->queryAll();
		            	$tot+=$exam_info[0]["calificacion"];
		                $nameE = explode("V",$exam_info[0]['nombre']);
		                $result .= '<div class="row" style="text-aling:center;padding:0px 20px;text-align:center;">
		                                <div class="col-sm-3">
		                                    <h5>'.$exam_info[0]['fecha_presentacion'].'</h5>
		                                </div>
		                                <div class="col-sm-3">
		                                    <h5>'.utf8_decode($nameE[0]).'</h5>
		                                </div>
		                                <div class="col-sm-3">
		                                   <h5>'.$exam_info[0]['calificacion'].'</h5> 
		                                </div>
		                                <div class="col-sm-3">
		                                   <h5>'.$exam_info[0]['nivel'].'</h5> 
		                                </div>
		                        </div>';
		            }
		            
		            $totALPROMEDIO=round($tot/$CONT,0);
	                  if(((($lisT[0]['CALIficacion'])>=0)&&(($totALPROMEDIO)<=12)))
                     {
                          $TECSPROFICIENCY ="1";
                          $CEFBYTECS ="Pre A1";
                          $cefrMess = 'Entry User';
                     }
                  if(((($totALPROMEDIO)>12)&&(($totALPROMEDIO)<=27)))
                     {
                          $TECSPROFICIENCY ="2";
                          $CEFBYTECS ="A1";
                          $cefrMess = 'Entry User';

                     }
                  if(((($totALPROMEDIO)>27)&&(($totALPROMEDIO)<=38)))
                     {
                          $TECSPROFICIENCY ="3";
                          $CEFBYTECS ="A2";
                          $cefrMess = 'Basic User';
                     }
                  if(((($totALPROMEDIO)>38)&&(($totALPROMEDIO)<=50)))
                     {
                          $TECSPROFICIENCY ="4";
                          $CEFBYTECS ="A2+";
                          $cefrMess = 'Basic User';
                     }
                  if(((($totALPROMEDIO)>50)&&(($totALPROMEDIO)<=62)))
                     {
                          $TECSPROFICIENCY ="5";
                          $CEFBYTECS ="B1";
                          $cefrMess = 'Intermediate User';
                     }
                  if(((($totALPROMEDIO)>62)&&(($totALPROMEDIO)<=72)))
                     {
                          $TECSPROFICIENCY ="6";
                          $CEFBYTECS ="B1+";
                          $cefrMess = 'Intermediate User';
                     }
                    if(((($totALPROMEDIO)>72)&&(($totALPROMEDIO)<=80)))
                     {
                          $TECSPROFICIENCY ="7";
                          $CEFBYTECS ="B2";
                          $cefrMess = 'Independent User';
                     }
                     if(((($totALPROMEDIO)>80)&&(($totALPROMEDIO)<=87)))
                     {
                          $TECSPROFICIENCY ="8";
                          $CEFBYTECS ="B2+";
                          $cefrMess = 'Independent User';
                     }
                     if(((($totALPROMEDIO)>87)&&(($totALPROMEDIO)<=100)))
                     {
                          $TECSPROFICIENCY ="9";
                          $CEFBYTECS ="C1";
                          $cefrMess = 'Proficient User';
                     }
	              $total_scoremail=round($tot/$CONT,0);
		            $result .=   '<div class="row" style="padding:0px 20px;">
		                        <div class="col-sm-5">
		                            <h3><b>'.$list[0]['nombres'].'</b></h3>
		                            <h5>Name</h5>
		                            <h3><b>'.$total_scoremail.'</b></h3>
		                            <h5>Test Score</h5>
		                            <h3><b>SUPERSCORE VALIDATION</b></h3>
		                        </div>
		                        <div class="col-sm-5">
		                           <h3><b>'.$list[0]['apellidos'].'</b></h3>
		                            <h5>Surname</h5>
		                            <h3><b>'.$CEFBYTECS.'</b></h3>
		                            <h5>CEFR Level</h5>
		                            <h3>'.$id.'</h3>
		                            <h5>Fraud Prevention Code</h5>
		                        </div>
		                        <div class="col-sm-2">
		                           </br><img width="70px" src="/ilto3/images/TECS.png"></br></br>
		                           <img src="/img/check.png" width="64"/>
		                        </div>
		                        </div>'; 
		        }
	    	}
	    	
	    
	    }else{
	    	
	    	//NOT A SUPERSCORE QUERY
	    	if(is_numeric($fraudDecrypt)){
            	$fraudDecryptF = $fraudDecrypt - 1000000; 
	        }else{
	            $fraudDecryptF = "NULL";
	        }
        	
        	// get taker and licenses information from Fraud Prevention Code
	        $SQL = "SELECT id_licencias_usuario, id_licencia_cliente, licenses_user.id_usuario, fecha_asignacion, licenses_user.estado, fecha_presentacion, hora, calificacion, nivel, nombres, apellidos, id_cliente FROM `licenses_user` 
	        INNER JOIN users_c on licenses_user.id_usuario = users_c.id_usuario_c WHERE `id_licencias_usuario` = '".$fraudDecryptF."' AND licenses_user.`id_usuario` LIKE '".$id."' AND licenses_user.estado = 'F'";
	        $list= $this->connection->createCommand($SQL)->queryAll();
	        
	        
	         //get id exams to set DATE ANSWERED
		        $sql='SELECT id_examen_usuario  FROM `exams_user` WHERE `id_licencia_usuario` = "'.$fraudDecryptF.'"';
		        $exams = Yii::app()->db->createCommand($sql)->queryAll();
		        
		        
		        //get answered speaking to set Date
		        $sql='SELECT date_answered  FROM `answers_exam_user` WHERE `id_examen_usuario` = "'.$exams[1]["id_examen_usuario"].'" ORDER BY date_answered ASC LIMIT 1';
		        $date_answered = Yii::app()->db->createCommand($sql)->queryAll();
		        
		        //set the date
		        if($date_answered[0]["date_answered"]==""){
		            //if there is no date record answered exam
		            $fecha = Yii::app()->dateFormatter->formatDateTime($list[0]["fecha_presentacion"],"long",NULL)."&nbsp;&nbsp;".$modExamsUser[0]->hora;
		        }else {
		            //if there is date record 
		           $fecha = Yii::app()->dateFormatter->formatDateTime($date_answered[0]["date_answered"],"long","short");
		        }
	        
	         //var_dump($imagen);die();
	        
	        //get ID CLIENT AND LOGO
	        $idCliente = $list[0]["id_cliente"];
	
	        $flag_cefr_anterior = 0;
	        
	        if($list[0]["id_licencia_cliente"] == 728 && $idCliente == '800040295'){
	
	            $flag_cefr_anterior = 1;
	        } 
	        
	      
	        if(empty($list)){
	            $result = '<div class="row" style="padding:10px; text-align:left;">
	                            <div class="col-sm-2">
	                                
	                            </div>
	                            <div class="col-sm-2">
	                                <img src="/img/error.png" width="64" style="display:inline;"/>
	                            </div>
	                            <div class="col-sm-8">
	                               <h2>THIS IS NOT A VALID CERTIFICATE.</h2> 
	                            </div>
	                        </div>';
	        }else{
	        		
	           $SQL = "SELECT nombre, calificacion, exams_user.nivel  
	                FROM `exams_user` 
	                INNER JOIN exams ON exams_user.id_examen = exams.id_examen
	                WHERE `id_licencia_usuario` = '".$fraudDecryptF."'";
	            $exams= $this->connection->createCommand($SQL)->queryAll();
	            
	            
	            
	            if($flag_cefr_anterior == 1){
                    if(((($list[0]['calificacion'])>=0)&&(($list[0]['calificacion'])<=12)))
                   {
                        $CEFBYTECS ="- A1";
                   }
                if(((($list[0]['calificacion'])>12)&&(($list[0]['calificacion'])<=31)))
                   {
                        $CEFBYTECS ="A1";
    
                   }
                if(((($list[0]['calificacion'])>31)&&(($list[0]['calificacion'])<=55)))
                   {
                        $CEFBYTECS ="A2";
                   }
                if(((($list[0]['calificacion'])>55)&&(($list[0]['calificacion'])<=74)))
                   {
                        $CEFBYTECS ="B1";
                   }
                if(((($list[0]['calificacion'])>74)&&(($list[0]['calificacion'])<=89)))
                   {
                        $CEFBYTECS ="B2";
                   }
                if(((($list[0]['calificacion'])>89)&&(($list[0]['calificacion'])<=100)))
                   {
                        $CEFBYTECS ="C1";
                   }
            }else{
            	if(((($list[0]['calificacion'])>=0)&&(($list[0]['calificacion'])<=12)))
                     {
                          $TECSPROFICIENCY ="1";
                          $CEFBYTECS ="Pre A1";
                          $cefrMess = 'Entry User';
                     }
                  if(((($list[0]['calificacion'])>12)&&(($list[0]['calificacion'])<=27)))
                     {
                          $TECSPROFICIENCY ="2";
                          $CEFBYTECS ="A1";
                          $cefrMess = 'Entry User';

                     }
                  if(((($list[0]['calificacion'])>27)&&(($list[0]['calificacion'])<=38)))
                     {
                          $TECSPROFICIENCY ="3";
                          $CEFBYTECS ="A2";
                          $cefrMess = 'Basic User';
                     }
                  if(((($list[0]['calificacion'])>38)&&(($list[0]['calificacion'])<=50)))
                     {
                          $TECSPROFICIENCY ="4";
                          $CEFBYTECS ="A2+";
                          $cefrMess = 'Basic User';
                     }
                  if(((($list[0]['calificacion'])>50)&&(($list[0]['calificacion'])<=62)))
                     {
                          $TECSPROFICIENCY ="5";
                          $CEFBYTECS ="B1";
                          $cefrMess = 'Intermediate User';
                     }
                  if(((($list[0]['calificacion'])>62)&&(($list[0]['calificacion'])<=72)))
                     {
                          $TECSPROFICIENCY ="6";
                          $CEFBYTECS ="B1+";
                          $cefrMess = 'Intermediate User';
                     }
                    if(((($list[0]['calificacion'])>72)&&(($list[0]['calificacion'])<=80)))
                     {
                          $TECSPROFICIENCY ="7";
                          $CEFBYTECS ="B2";
                          $cefrMess = 'Independent User';
                     }
                     if(((($list[0]['calificacion'])>80)&&(($list[0]['calificacion'])<=87)))
                     {
                          $TECSPROFICIENCY ="8";
                          $CEFBYTECS ="B2+";
                          $cefrMess = 'Independent User';
                     }
                     if(((($list[0]['calificacion'])>87)&&(($list[0]['calificacion'])<=100)))
                     {
                          $TECSPROFICIENCY ="9";
                          $CEFBYTECS ="C1";
                          $cefrMess = 'Proficient User';
                     }
            }
	            
	            
	            
	            $result =   '<div class="row" style="padding:0px 20px;">
	                        <div class="col-sm-5">
	                            <h3>'.$list[0]['nombres'].'</h3>
	                            <h5>Name</h5>
	                            <h3>'.$list[0]['calificacion'].'</h3>
	                            <h5>Test Score</h5>
	                            <h3>'.$fecha.'</h3>
	                            <h5>Test date</h5>
	                        </div>
	                        <div class="col-sm-5">
	                           <h3>'.$list[0]['apellidos'].'</h3>
	                            <h5>Surname</h5>
	                            <h3>'.$CEFBYTECS.'</h3>
	                            <h5>CEFR Level</h5>
	                            <h3>'.$_REQUEST['code'].'</h3>
	                            <h5>Fraud Prevention Code</h5>
	                        </div>
	                        <div class="col-sm-2">
	                           </br><img width="70px" src="/ilto3/images/TECS.png"></br></br>
	                           <img src="/img/check.png" width="64"/>
	                        </div>
	                        </div>'; 
	            $result .= '<div class="row" style="padding:0px 20px;background: #31b744; text-align:center;color:#fff;">
	                            <div class="col-sm-4">
	                                <h4><b>COMMUNICATION SKILL</b></h4>
	                            </div>
	                            <div class="col-sm-4">
	                               <h4><b>SCORE</b></h4> 
	                            </div>
	                            <div class="col-sm-4">
	                               <h4><b>CEFR Level</b></h4> 
	                            </div>
	                        </div>';
	                        
	            foreach($exams as $exam){
	                $nameE = explode("V",$exam['nombre']);
	                $result .= '<div class="row" style="text-aling:center;padding:0px 20px;text-align:center;">
	                                <div class="col-sm-4">
	                                    <h5>'.$nameE[0].'</h5>
	                                </div>
	                                <div class="col-sm-4">
	                                   <h5>'.$exam['calificacion'].'</h5> 
	                                </div>
	                                <div class="col-sm-4">
	                                   <h5>'.$exam['nivel'].'</h5> 
	                                </div>
	                        </div>';
	            }
	        }
	    }
       
        
        
        header('Content-Type: application/json');
	    die($result);
	}
	
	public function actionLog()
	{
	    $IdCliente = $_POST['id_cliente'];
	    //idClient
	    $where = 'users_c.id_cliente = "'.$IdCliente.'"';
	    
	    //logtype
	    if(isset($_REQUEST['log_type']) && strlen($_REQUEST['log_type']>=0)){
	        
	        if($_REQUEST['log_type']!=""){
	            $where .= ' and log_type = "'.$_REQUEST['log_type'].'" ';
	        }
	        
	    }
	 
	    //id_adm
	    if(isset($_REQUEST['id_adm']) && strlen($_REQUEST['id_adm'])>0){
	        $where .= ' and id_adm like "'.$_REQUEST['id_adm'].'%" ';
	    }
	    // comment
	    if(isset($_REQUEST['comment']) && strlen($_REQUEST['comment'])>0){
	        $where .= ' and comment like "%'.$_REQUEST['comment'].'%" ';
	    }
	    
	    //id_usuario_c
	    if(isset($_REQUEST['id_user']) && strlen($_REQUEST['id_user'])>0){
	        $where .= ' and id_user like "'.$_REQUEST['id_user'].'%" ';
	    }

        //id_license
	    if(isset($_REQUEST['id_license']) && strlen($_REQUEST['id_license'])>0){
	        $where .= ' and id_license LIKE "'.$_REQUEST['id_license'].'%" ';
	    }	    
	    
	   if(isset($_REQUEST['startDate']) && strlen($_REQUEST['startDate']>0)){
	        $where .= ' and date_time >= "'.$_REQUEST['startDate'].'" ';
	        if(isset($_REQUEST['endDate']) && strlen($_REQUEST['endDate']>0)){
	            $where .= ' and date_time BETWEEN "'.$_REQUEST['startDate'].'" AND "'.$_REQUEST['endDate'].'"';
	        }
	    }
	    
	    
        
        $SQL = "SELECT `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user` FROM `adm_log` INNER JOIN `users_c` ON `users_c`.id_usuario_c = `adm_log`.id_adm WHERE $where ORDER BY date_time DESC LIMIT 100";
        $list= $this->connection->createCommand($SQL)->queryAll();
       // var_dump($SQL);
        foreach($list as $log){
            switch($log['log_type']){
                case 0:
                    $type = "Login"; 
                break;
                case 1:
                    $type = "Action"; 
                break;
                case 3:
                    $type = "Logout"; 
                break;
                case 4:
                    $type = "Access"; 
                break;
            }
            
            
            $result .=    '<tr>
                <td>'.$type.'</td>
                <td>'.$log['date_time'].'</td>
                <td>'.$log['id_adm'].'</td>
                <td>'.utf8_encode($log['comment']).'</td>
                <td>'.$log['id_user'].'</td>
                <td>'.$log['id_license'].'</td>   
                <td>'.$log['id_exam'].'</td>   
                </tr>';
        }
        
	   $Result['html'] = $result;
	   header('Content-Type: application/json');
	   //die(json_encode(array('html'=>implode('',$result))));
	   die(json_encode($Result));
	}
	
	public function actionGetPassByAuthorizationCode()
	{
	    //consultamos si hay un cliente con este codigo de seguridad
	    $SQL = "SELECT id_cliente FROM `clients` WHERE authorization_code='".$_REQUEST['code']."'";
	    $success= $this->connection->createCommand($SQL)->queryAll();
	   
	    //validamos la consulta
	    if($success[0]['id_cliente']==""){
	        //si no hay cliente con dicho código, devolvemos un mensaje de código inválido
	        $Result = 'A';
	    }else{
	        if($_REQUEST['id']==""){
	            //Si no viene un ID en la petición, devolvemos mensaje de ID
	            $Result = 'B';
	        }else{
	            //obtenemos los ids de los TECS    	        
	            $modUtil = new Utilidades();
	            $ids_string = $modUtil->getIdsTecs();
	            
	            //consultamos si el usuario tiene un examen tecs pendiente
	            $SQL = "SELECT id_licencias_usuario FROM `licenses_user` INNER JOIN licenses_client ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente WHERE id_licencia IN (".$ids_string.") AND licenses_user.estado = 'A' AND id_usuario = '".$_REQUEST['id']."' ";
	            $tecs= $this->connection->createCommand($SQL)->queryAll();
	            
	            //si no hay licencias pendientes NO DEVOLVEMOS LA PASSWORD
	            
	            if($tecs[0]["id_licencias_usuario"] == ""){
	                $Result = 'C';
	            }else{
	                //SI HAY LICENCIAS PENDIENTES POR PRESENTAR DEVOLVEMOS LA PASSWORD
	                $SQL = "SELECT clave2 FROM `users_c` WHERE id_usuario_c='".$_REQUEST['id']."'";
        	        $password= $this->connection->createCommand($SQL)->queryAll();
        	        $Result = $password[0]["clave2"];
	                
	            }
	            
	            
	        }
	        
	    }
	   
	   
	   header('Content-Type: application/json');
	   die($Result);
	}
	
	public function actionVerifyPicture(){
		//import utilities for send Email
		$modUtil = new Utilidades();
		//vars
		$id_request = $_REQUEST["request"];
		$flag = $_REQUEST["flag"];
		$action = $_REQUEST["action"];
		$id_admin = $_REQUEST["id_admin"];
		
		$sql_header = "UPDATE proctorv_request SET";  
		
		$SQL = "SELECT  id_user, photo_route, card_route FROM `proctorv_request` WHERE id_proctorv_request = '".$id_request."'";
		$proctorv_request =  $this->connection->createCommand($SQL)->queryAll();
		
		
		//flag 1 SET PROFILE PICTURE
		if($flag == 1){
			if($action == 1){
				$action_field = "photo_step = 3";
				$Result = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Not Done!" width="32"> 
                            <span class="yellow_text">ACCEPTED. PENDING FACE MATCH</span><h4>
                            <h4>3. Face Photo</h4><br/>
                        <p>Does the person on the camera match the photo?</p>
                         <input id="matchPhotoFace" type="button" class="btn btn-success" value="MATCH"/> &nbsp; <input id="NotMatchPhotoFace" type="button" class="btn btn-danger"  value="DO NOT MATCH"/><br><br>
                        <img class="requirement-picture" src="/ilto3/images/proctorv_users/'.$proctorv_request[0]['photo_route'].'" title="taker face photo">';
                $comment = 'Validate face Photo';
                
                //get id client
				$SQL =  "SELECT id_cliente, id_ciudad FROM users_c  WHERE id_usuario_c = '$id_admin' ";  
				$id_cliente= $this->connection->createCommand($SQL)->queryAll();
                
                //get request info
				$SQL =  "SELECT id_program, date_picked, id_license FROM proctorv_request 
						INNER JOIN proctorv_user_dates ON proctorv_request.id_proctorv_request = proctorv_user_dates.id_request
						WHERE id_proctorv_request = '$id_request' 
						AND speaking_date = 1";  
				$request_info= $this->connection->createCommand($SQL)->queryAll();
				
					//IF THERE IS NOT LICENSE FOR THIS TECS2GO, WE ALLOCATE ONE
					if($request_info[0]["id_license"] == null){
						//get ID TECS2GO ROOM of CLIENT
						$SQL =  "SELECT id FROM class_rooms  WHERE id_cliente = '".$id_cliente[0]['id_cliente']."' AND nombre LIKE '%TECS2GO%'";  
						$id_room = $this->connection->createCommand($SQL)->queryAll();
						
						//get IDS from TECS licenses Available from client
						$modUtil = new Utilidades();
						$tecs_ids = array();
						$tecs_ids = $modUtil->getAvailableTecsId($id_cliente[0]["id_cliente"]);
						shuffle($tecs_ids);
						$ramdon_license_id = $tecs_ids[0];
						
						//get ID_license_client by License_type
						$SQL =  "SELECT id_licencias_cliente FROM licenses_client  WHERE id_cliente = '".$id_cliente[0]['id_cliente']."' AND id_licencia = '".$ramdon_license_id."' AND estado = 'A'";  
						$id_licencias_cliente = $this->connection->createCommand($SQL)->queryAll();
						
						$data = array();
						//tratar de llamar la asignación de licencia
						//variables para pasar por data
						$data['id_cliente'] = $id_cliente[0]['id_cliente'];
						$data["period"] = 10;
						$data["city"] = $id_cliente[0]["id_ciudad"];
						$data["school"] = 0;
						$data["program"] = $request_info[0]["id_program"];
						$data['id_licencias_cliente'] = $id_licencias_cliente[0]["id_licencias_cliente"];
						
						
						$data['fecha_presentacion'] = $request_info[0]["date_picked"];
						$explode_hour = explode(" ", $request_info[0]["date_picked"]);
				        $hour_separate = explode(":", $explode_hour[1]);
				        $data['hora'] = $hour_separate[0].":00";
						
				        $data['salon'] = $id_room[0]["id"];
				        $data['tutor'] = $id_admin;
				        $this->allocateLicense($proctorv_request[0]['id_user'], $data, $id_request, $id_admin);
				        
					}//end IF CHECK LICENSE
				
			}else{
				$action_field = "photo_step = 2, photo_route = ''";
				$Result = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Not Done!" width="32"> 
                            <span class="yellow_text">PENDING VALIDATION</span><h4>
                            <h4>3. Face Photo</h4><br/>
                 <img src="/ilto3/images/no-profile-photo-ilto.jpg" title="No profile picture found">';
                 $comment = 'Deny face Photo';
                 
			}
		
		}
		//flag 2 SET ID CARD PICTURE
		if($flag == 2){
			//action 1 OK, 2 DENY AND DELETE PICTURE
			if($action == 1){
				$action_field = "card_step = 1";
					$Result = '<h4><img src="/ilto3/images/iconos/verified.png" title="Done!" width="32">
                        <span class="green_text">DONE</span></h4>
                        <h4>4. ID Card Photo - '.$proctorv_request[0]["id_user"].'</h4><br/>
                        <img class="requirement-picture" src="/ilto3/images/proctorv_users/'.$proctorv_request[0]['card_route'].'" title="taker Id card photo">';
                        $comment = 'Validate ID Card Photo';
			}else{
				$action_field = "card_step = 2, card_route = ''";
				$Result = '<h4><img src="/ilto3/images/iconos/verified.png" title="Done!" width="32">
                        <span class="green_text">DONE</span></h4>
                        <h4>4. ID Card Photo - '.$proctorv_request[0]["id_user"].'</h4><br/>
                 <img src="/ilto3/images/no-profile-photo-ilto.jpg" title="No profile picture found">';
                 $comment = 'Deny ID Card Photo';
			}
		}
		
		// agregamos al log del usuario
		$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 5, '$id_admin', NOW(), '', '$id_request', '$comment', '".$proctorv_request[0]["id_user"]."')";
		$log= $this->connection->createCommand($SQL)->query();
		
		//update face / id card photo status
		$sql_final = "WHERE id_proctorv_request = '$id_request'";
		$SQL = $sql_header." ".$action_field." ".$sql_final;
		$success= $this->connection->createCommand($SQL)->query();
		
		//get the face/id card status updated
		$SQL = "SELECT id_user, photo_step, card_step, email FROM `proctorv_request` INNER JOIN users_c ON proctorv_request.id_user = users_c.id_usuario_c WHERE id_proctorv_request = '".$id_request."'";
		$notification =  $this->connection->createCommand($SQL)->queryAll();
		
	
		if($notification[0]["photo_step"] != 0 && $notification[0]["card_step"] !=0){
		
			//photo step
			switch ($notification[0]["photo_step"]) {
				case 2:
					$photo_step_not = 'Tu fotografía del rostro fue <b>RECHAZADA</b>, por favor vuelve a tomar la fotografía.';
					break;
				case 3:
					$photo_step_not = 'Tu fotografía del rostro fue <b>ACEPTADA</b>.';
					break;
				default:
					// code...
					break;
			}
			
			//ID CARD step
			switch ($notification[0]["card_step"]) {
				case 1:
					$card_step_not = 'Tu fotografía de CEDULA/DNI/RFC fue <b>ACEPTADA</b>.';
					break;
				case 2:
					$card_step_not = 'Tu fotografía de CEDULA/DNI/RFC fue <b>RECHAZADA</b>, por favor vuelve a tomar la fotografía.';
					break;
				default:
					// code...
					break;
			}
			
			if($notification[0]["photo_step"] == 3 && $notification[0]["card_step"] == 1){
				$adition = '<p><strong>IMPORTANTE LEA CUIDADOSAMENTE</strong><br>
				Las fechas seleccionadas por usted están <strong>confirmadas</strong>. Además del correo de confirmación de fechas, NO ENVIAREMOS OTRO CORREO, NI LO CONTACTAREMOS TELEFONICAMENTE. <br><br>
				
				Recuerde que si ya pagó, al hacer el registro y selección de fechas y horas de presentación del examen, usted confirma el uso de la licencia del examen y nosotros reservaremos un profesor y un guarda virtual para su examen. <br>Esto significa que los recursos han sido asignados y si no asiste al examen, deberá pagar nuevamente. <br><br>				
				
				Examen de comunicación conversacional:<br>
				Por favor esté atent@ en la franja horaria, el dia que seleccionó al correo con el asunto TECS2GO SPEAKING TEST, este contiene el enlace para que ingrese a la videoconferencia. Este correo llegará entre 30 y 60 minutos antes de el examen. <strong>Por favor revise su correo spam o no deseado</strong>.<br><br>  
				
				
				<strong>Examen Online (Lectura, Uso del Idioma, Escucha)</strong><br>
				Recibira 2 correos de ProctorExam (nuestra unidad de guardas virtuales). Un correo inicial para que realice la verificación técnica de su computador (sonido, audio, internet), igualmente debe descargar una aplicación en su teléfono celular y en google Chrome. Al finalizar esta verificación, recibirá otro correo con el acceso al examen, el cual está programado para el dia y la hora que seleccionó.  Por favor revise su correo spam o no deseado. La dirección de email de donde se origina el correo para el examen es: 
				exams@proctorexam.com</p>';
			}else{
				$adition = '&nbps;';
			}
			
			//send email notifications
			$to = $notification[0]["email"];
	        $subject = "Actualización de estado en tu registro TECS2GO";
	        $htmlBody = '<p>Tienes las siguientes notificaciones de tu registro TECS2GO.  Accede al siguiente enlace para verificar <a href="https://iltoexams.com/ilto3/proctorv/view/login.php">TECS2GO</a>.<br/>
				<ul>
				  <li>'.$photo_step_not.'</li>
				  <li>'.$card_step_not.'</li>
				</ul>  
        		</p><br/>
        		<p>'.$adition.'</p><br/><br/>
        		Atentamente,<br/><br/>
                    <a href="http://www.iltoexams.com/"><img style="float:left;top:20px;" alt="ILTO Exams" src="iltoexams.com/logo_ILTO.png" /></a>
        		
                ';
                
            
            //send email    
	        $dat = "{From: 'noreply@iltoexams.com', To: '$to', Subject: '$subject', HtmlBody: '$htmlBody'}";//array('From' => 'noreply@iltoexams.com', 'To'=>'dokho_02@hotmail.com', 'Subject'=>'Hola Chris Fer', 'HtmlBody'=>'<strong>Hello</strong> dear Postmark user.');
	        $modUtil->sendEmail($dat);
			
		}
		
		
		
		
		header('Content-Type: application/json');
	    die($Result);
	}
	
	public function allocateLicense($id_taker, $data, $id_request, $id_admin){
		
		$modUtil = new Utilidades();
        //Asociar la licencia al usuario
        $modLicUsu = new LicensesUser();
        $modLicUsu->id_licencia_cliente = $data['id_licencias_cliente'];
        $modLicUsu->id_usuario=$id_taker;
        $modLicUsu->fecha_asignacion = new CDbExpression('NOW()');
        $modLicUsu->fecha_presentacion = $data['fecha_presentacion'];
        $modLicUsu->hora = $data['hora'];
        $modLicUsu->estado="A";
        
        $SQL = "INSERT INTO `licenses_user` (`id_licencias_usuario`, `id_licencia_cliente`, `id_usuario`, `fecha_asignacion`, `estado`, `fecha_presentacion`, `hora`, `calificacion`, `nivel`, `terms`) 
        VALUES (NULL, '".$data['id_licencias_cliente']."', '".$id_taker."', NOW(), 'A', '".$data['fecha_presentacion']."', '".$data['hora']."', '0.0', '', '0')";
       
        $insert_license= Yii::app()->db->createCommand($SQL)->query();
        
        //$modLicUsu->save();
        $id_license = Yii::app()->db->getLastInsertID();
        //Gastar la licencia
        
        $modUtil->gastarLicencia($data['id_licencias_cliente']);
        //Asignar los exámenes al usuario
        $modLicenciasCliente = LicensesClient::model()->findByPk($modLicUsu->id_licencia_cliente);
        $modExamenes = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>$modLicenciasCliente->id_licencia));
        
        foreach($modExamenes as $examen){
	        $modExamenUsuario = new ExamsUser();
	        $modExamenUsuario->id_examen = $examen->id_examen;
            $modExamenUsuario->id_licencia_usuario = $modLicUsu->id_licencias_usuario;
            $modExamenUsuario->fecha_presentacion = $data['fecha_presentacion'];
            $modExamenUsuario->hora = $data['hora'];
            $modExamenUsuario->salon = $data['salon'];
            if(strlen($datos['tutor'])>0){
            	$modExamenUsuario->tutor = $data['tutor']; 
            }
            	$modExamenUsuario->estado = "A";
                //$modExamenUsuario->save();
                 $SQL = "INSERT INTO `exams_user` 
                 (`id_examen_usuario`, `id_licencia_usuario`, `id_examen`, `fecha_presentacion`, `calificacion`, `nivel`, `estado`, `hora`, `tutor`, `salon`, `a1_level`, `a2_level`, `b1_level`, `b2_level`, `c1_level`) 
                 VALUES (NULL, '".$id_license."', '".$examen->id_examen."', '".$data['fecha_presentacion']."', NULL, NULL, 'A', '".$data['hora']."', '".$id_admin."', '".$data['salon']."', '0', '0', '0', '0', '0')"; 
				$insert_exams= Yii::app()->db->createCommand($SQL)->query();
            }
                                
            
            if($data['id_cliente'] == '8605173021' || $data['id_cliente'] = '86051730212'){
            	// Guardamos el registro en la tabla de period
                $SQL = "INSERT INTO `users_c_period`(`id_users_c_period`, `id_usuario_c`, `id_cliente`, `period`, `id_city`, `id_school`, `id_program`, `id_licencias_usuario`) 
                	VALUES ('', '".$id_taker."', '".$data['id_cliente']."', '".$data["period"]."', '".$data["city"]."', '".$data["school"]."', '".$data["program"]."', '".$id_license."')";
                    $list= Yii::app()->db->createCommand($SQL)->query();   
            }
                                
			// agregamos al log del usuario
            $SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 1, '".$id_admin."', NOW(), '$id_license', '', 'Assign a license to a test taker', '$id_taker')";
            $log= Yii::app()->db->createCommand($SQL)->query(); 
                                
            //generate ramdon pass and update the user password FOR ONLINE SECTION
            $pass = $modUtil->generateRandomString(6);
            $SQL = "UPDATE `users_c` SET `clave`='".md5($pass)."', `clave2`='".$pass."' WHERE `id_usuario_c`='".$id_taker."'";
            $passUpdate= Yii::app()->db->createCommand($SQL)->query();
            // si ha pasado todas las validaciones y restricciones,
            
            //link created license to TECS2GO REQUEST
            $SQL = "UPDATE `proctorv_request` SET `id_license`='".$id_license."', `date_allocate`= NOW() WHERE `id_proctorv_request`='".$id_request."'";
            $passUpdate= Yii::app()->db->createCommand($SQL)->query();
            // si ha pasado todas las validaciones y restricciones,
                                          
    }
	
	
	public function actionEnableIdentityValidation(){
	
		//vars
		$id_request = $_REQUEST["request"];
		
		$SQL =  "UPDATE proctorv_request SET status_request = 1 WHERE id_proctorv_request = '$id_request' ";  
		$success= $this->connection->createCommand($SQL)->query();

		$Result = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Pending for check it!" width="32"> <span class="red_text">PENDING</span><br/>
		TECS2GO Speaking Test</h4><br>
        <p>Waiting Identity Validation</p>';
	
	
		header('Content-Type: application/json');
		die($Result);
	}
	
	public function actionValidateStatusRequest(){
		//vars
		$id_request = $_REQUEST["request"];
		
		$SQL =  "SELECT * FROM proctorv_request  WHERE id_proctorv_request = '$id_request' ";  
		$proctorv_request= $this->connection->createCommand($SQL)->queryAll();
		
		switch ($proctorv_request[0]["status_request"]) {
		    case 0:
		        $status = '<img src="/ilto3/images/iconos/alert-warning.png" title="Pending for check it!" width="32"> <span class="red_text">PENDING</span>';
		        $available_at = '<input id="enableValidation" type="button" class="btn btn-success" value="Enable Validation"/>';
		        break;
		    case 1:
		        $status = '<img src="/ilto3/images/iconos/alert-warning.png" title="Pending for check it!" width="32"> <span class="red_text">PENDING</span>
		        <p>Waiting Identity Validation</p>';
		        break;
		    case 2:
		        $status = '<img src="/ilto3/images/iconos/verified.png" title="Pending for check it!" width="32"> <span class="green_text">READY TO TAKE TEST</span>
		        '.$btn_speaking.'';
		        ;
		    break;
		    case 3:
		       $status = '<img src="/ilto3/images/iconos/alert-danger.png" title="Pending for check it!" width="32"> <span class="green_text">REVOKED</span>';
		       $available_at = '<p>Please contact an administrator.</p>';
		    break;
		    case 4:
		       $status = '<img src="/ilto3/images/iconos/alert-danger.png" title="Pending for check it!" width="32"> <span class="green_text">REVOKED</span>';
		       $available_at = '<p>Please contact an administrator.</p>';
		    break;
		    default:
		        // code...
		        break;
		}
	}
	
	public function actionValidateMatchFace(){
		
		//vars
		$id_request = $_REQUEST["request"];
		$flag = $_REQUEST["flag"];
		$id_admin = $_REQUEST["id_admin"];

		//MATCH PROCCESS
		$SQL =  "SELECT photo_route, id_user FROM proctorv_request  WHERE id_proctorv_request = '$id_request' ";  
		$proctorv_request= $this->connection->createCommand($SQL)->queryAll();
	
		
		//face and camera match 
		if($flag==1){
			$SQL =  "UPDATE proctorv_request SET photo_step = 1 WHERE id_proctorv_request = '$id_request' ";  
			$success= $this->connection->createCommand($SQL)->query();
			
			$SQL =  'UPDATE users_c SET ruta_foto = "'.$proctorv_request[0]['photo_route'].'" WHERE id_usuario_c = "'.$proctorv_request[0]['id_user'].'"';  
			$success= $this->connection->createCommand($SQL)->query();
		
			
			$Result = '<h4><img src="/ilto3/images/iconos/verified.png" title="Not Done!" width="32"> 
                            <span class="green_text">DONE AND MATCH</span><h4>
                            <h4>3. Face Photo</h4><br/>
                        <img class="requirement-picture" src="/ilto3/images/proctorv_users/'.$proctorv_request[0]['photo_route'].'" title="taker face photo">';
            $comment = 'Match Face Photo APPROVED';
            
            
		//face and camera NOT match	
		}else{
			$SQL =  "UPDATE proctorv_request SET photo_step = 4 WHERE id_proctorv_request = '$id_request' ";  
			$success= $this->connection->createCommand($SQL)->query();
			
			$Result = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Not Done!" width="32"> 
                            <span class="yellow_text">MATCH FAILED</span><h4>
                            <h4>3. Face Photo</h4><br/>
                        <img class="requirement-picture" src="/ilto3/images/proctorv_users/'.$proctorv_request[0]['photo_route'].'" title="taker face photo">';
            $comment = 'Match Face Photo DISAPPROVED ';
		}
		
		// agregamos al log del usuario
		$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 5, '$id_admin', NOW(), '', '$id_request', '$comment', '".$proctorv_request[0]["id_user"]."')";
		$log= $this->connection->createCommand($SQL)->query();
		
		header('Content-Type: application/json');
		die($Result);
		
	}
	
	
	public function actionGetRequestStatus(){
		
		$SQL =  "SELECT id_license, photo_step, status_request FROM proctorv_request  WHERE id_proctorv_request = '$id_request' ";  
		$proctorv_request= $this->connection->createCommand($SQL)->queryAll();
		
		if($proctorv_request["id_license"] != NULL){
		    $SQL = "SELECT * FROM `exams_user` WHERE `id_licencia_usuario` = '".$proctorv_request["id_license"]."' ORDER BY id_examen_usuario ASC LIMIT 1 ";
		    $speaking= $connection->createCommand($SQL)->queryAll();
		    
		    $btn_speaking = '<a href="/ilto3/index.php?r=Clients/AnswersExamUser/takeExam&userId='.$speaking[0]["id_examen_usuario"].'">TAKE EXAM</a>';
		}else{
		
		    $btn_speaking = '<p>License pending</p>';
		}
		
		//identity btn status
		if($proctorv_request[0]["photo_step"]==1){
		        $identity_button = "";
		    }else{
		        $identity_button = "disabled";
		    }
		
		//STATUS SWITCH
		switch ($proctorv_request[0]["status_request"]) {
		    
		    case 0:
		        $status = '<img src="/ilto3/images/iconos/alert-warning.png" title="Pending for check it!" width="32"> <span class="red_text">PENDING</span>
		        <input id="enableValidation" type="button" class="btn btn-success" value="Enable Validation" '.$identity_button.'/>';
		        break;
		    case 1:
		        $status = '<img src="/ilto3/images/iconos/alert-warning.png" title="Pending for check it!" width="32"> <span class="red_text">PENDING</span>
		        <p>Waiting Identity Validation</p>';
		        break;
		    case 2:
		        $status = '<img src="/ilto3/images/iconos/verified.png" title="Pending for check it!" width="32"> <span class="green_text">READY TO TAKE TEST</span>
		        '.$btn_speaking.'';
		        ;
		    break;
		    case 3:
		       $status = '<img src="/ilto3/images/iconos/alert-danger.png" title="Pending for check it!" width="32"> <span class="green_text">REVOKED</span>';
		    break;
		    case 4:
		       $status = '<img src="/ilto3/images/iconos/alert-danger.png" title="Pending for check it!" width="32"> <span class="green_text">REVOKED</span>';
		    break;
		    default:
		        // code...
		        break;
		}
			
				header('Content-Type: application/json');
				die($status);
	}
	
	public function actionTakerCheckIdentityVal(){
		
		//vars
		$id_request = $_REQUEST["request"];
		$SQL =  "SELECT * FROM proctorv_request  WHERE id_proctorv_request = '$id_request' ";  
		$proctorv_request= $this->connection->createCommand($SQL)->queryAll();
		
		if($proctorv_request[0]["status_request"]==1){
			
			switch ($proctorv_request[0]["identity_verified"]) {
		    case 0:
		        $Result = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Not Done!" width="32"> 
		        <span class="yellow_text">PENDIENTE</span><br/>
		        Validación de Identidad</h4><br>
		        <a href="/ilto3/proctorv/view/identity-validation.php?id='.base64_encode($id_request).'" class="btn btn-warning" id="allocate">Verificar Identidad</a><br/><br/>
		        <input type="hidden" id="sr" value="'.$proctorv_request["status_request"].'" /> ';
		        break;
		    case 1:
		        $Result = '<h4><img src="/ilto3/images/iconos/verified.png" title="Done!" width="32">
		        <span class="green_text">APROBADA</h4> 
		        Validación de Identidad</h4><br>';
		        break;
		    case 2:
		        $Result = '<h4><img src="/ilto3/images/iconos/alert-danger.png" title="failed!" width="32">
		        <span class="red_text">NO APROBADA</h4>
		        Validación de Identidad</h4><br>
		        Validation not sucessful. Contact tecs2go@iltoexams.com</h4>';
		        break;
		    default:
		        // code...
		        break;
		}
	    }else{
	    	$Result = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Not Done!" width="32"> 
	        <span class="yellow_text">PENDIENTE</span><br/>
	        Validación de Identidad</h4><br> 
	        <input type="hidden" id="sr" value="'.$proctorv_request[0]["status_request"].'" />';
	    }
		header('Content-Type: application/json');
		die($Result);	
		
	}
	
	public function actionRevokeLicense(){
		
		//vars
		$id_request = $_REQUEST["id_request"];
		$id_admin = $_REQUEST["id_admin"];
		$id_license =$_REQUEST["id_license"];
		$id_user = $_REQUEST["id_user"];
		$comment = 'TECS2GO REVOKED license by Proctor.';
			
		//REVOKE LICENSE
		$SQL =  "UPDATE licenses_user SET estado = 'B' WHERE id_licencias_usuario = '".$id_license."' ";  
		$success= $this->connection->createCommand($SQL)->query();
		
		//REVOKE LICENSE'S EXAMS
		$SQL =  "UPDATE exams_user SET estado = 'B' WHERE id_licencia_usuario = '".$id_license."' ";  
		$success= $this->connection->createCommand($SQL)->query();
		
		//SET USER STATUS TO BLOCKED
		//REVOKE LICENSE'S EXAMS
		$SQL =  "UPDATE users_c SET estado = 'B' WHERE id_usuario_c = '".$id_user."' ";  
		$success= $this->connection->createCommand($SQL)->query();
		
		
		//UPDATE REVOKE STATUS TECS2GO REQUEST
		$SQL =  "UPDATE proctorv_request SET status_request = '5' WHERE id_proctorv_request = '".$id_request."' ";  
		$success= $this->connection->createCommand($SQL)->query();
		
		
		// agregamos al log del usuario
		$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 5, '$id_admin', NOW(), '', '$id_request', '$comment', '".$proctorv_request[0]["id_user"]."')";
		$log= $this->connection->createCommand($SQL)->query();
		
		$Result = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Pending!" width="32"><span class="red_text">THIS LICENSE HAS BEEN REVOKED.</span></h4>';
		header('Content-Type: application/json');
		die($Result);	
	}
	
	public function actionUndoRevokeLicense(){
		
		//vars
		$id_request = $_REQUEST["id_request"];
		$id_admin = $_REQUEST["id_admin"];
		$id_license =$_REQUEST["id_license"];
		$id_user = $_REQUEST["id_user"];
		$comment = 'TECS2GO UNDO REVOKED license by Proctor.';
		
		//SET USER STATUS TO BLOCKED
		//REVOKE LICENSE'S EXAMS
		$SQL =  "UPDATE users_c SET estado = 'A' WHERE id_usuario_c = '".$id_user."' ";  
		$success= $this->connection->createCommand($SQL)->query();
		
		
		$SQL = "DELETE FROM `fraud_report` WHERE document = '".$id_user."'";
		$success= $this->connection->createCommand($SQL)->query();
		
		$SQL =  "SELECT * FROM licenses_user  WHERE id_licencias_usuario = '$id_license' ";  
		$license_query= $this->connection->createCommand($SQL)->queryAll();
		
		if($license_query[0]["calificacion"] > 0.0){
			//SET LICENSE STATUS TO F
			$SQL =  "UPDATE licenses_user SET estado = 'F' WHERE id_licencias_usuario = '".$id_license."' ";  
			$success= $this->connection->createCommand($SQL)->query();
			
			//SET EXAMS STATUS TO F
			$SQL =  "UPDATE exams_user SET estado = 'F' WHERE id_licencia_usuario = '".$id_license."' ";  
			$success= $this->connection->createCommand($SQL)->query();
			
				//UPDATE REVOKE STATUS TECS2GO REQUEST
			$SQL =  "UPDATE proctorv_request SET status_request = '4' WHERE id_proctorv_request = '".$id_request."' ";  
			$success= $this->connection->createCommand($SQL)->query();
		}else{
			//SET LICENSE STATUS TO F
			$SQL =  "UPDATE licenses_user SET estado = 'A' WHERE id_licencias_usuario = '".$id_license."' ";  
			$success= $this->connection->createCommand($SQL)->query();
			
			$SQL =  "SELECT * FROM exams_user WHERE id_licencia_usuario = '$id_license' ";  
			$exams = $this->connection->createCommand($SQL)->queryAll();
			
			foreach ($exams as $exam) {
				if($exam["calificacion"] != NULL){
					//SET EXAMS STATUS TO F
					$SQL =  "UPDATE exams_user SET estado = 'F' WHERE id_examen_usuario = '".$exam["id_examen_usuario"]."' ";  
					$success= $this->connection->createCommand($SQL)->query();
				}else{
					$SQL =  "UPDATE exams_user SET estado = 'A' WHERE id_examen_usuario = '".$exam["id_examen_usuario"]."' ";  
					$success= $this->connection->createCommand($SQL)->query();
				}
			}
			
				//UPDATE REVOKE STATUS TECS2GO REQUEST
			$SQL =  "UPDATE proctorv_request SET status_request = '3' WHERE id_proctorv_request = '".$id_request."' ";  
			$success= $this->connection->createCommand($SQL)->query();
		}
		die();
		
		
		
		
		
		// agregamos al log del usuario
		$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 5, '$id_admin', NOW(), '', '$id_request', '$comment', '".$proctorv_request[0]["id_user"]."')";
		$log= $this->connection->createCommand($SQL)->query();
		
		$Result = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Pending!" width="32"><span class="red_text">LICENSE UPDATED.</span></h4>';
		header('Content-Type: application/json');
		die($Result);	
	}
	
	//revoke license from Speaking	
	public function actionRevokeLicenseFromSpeaking(){
	
		//vars
		$id_admin = $_REQUEST["id_admin"];
		$id_license =$_REQUEST["id_license"];
		$id_user = $_REQUEST["id_user"];
		$id_exam = $_REQUEST["id_exam"];
		
		$comment = 'REVOKED license FROM SPEAKING by Proctor.';
			
		//REVOKE LICENSE
		$SQL =  "UPDATE licenses_user SET estado = 'B' WHERE id_licencias_usuario = '".$id_license."' ";  
		$success= $this->connection->createCommand($SQL)->query();
		
		//REVOKE LICENSE'S EXAMS
		$SQL =  "UPDATE exams_user SET estado = 'B' WHERE id_licencia_usuario = '".$id_license."' ";  
		$success= $this->connection->createCommand($SQL)->query();
		
		//SET USER STATUS TO BLOCKED
		//REVOKE LICENSE'S EXAMS
		$SQL =  "UPDATE users_c SET estado = 'B' WHERE id_usuario_c = '".$id_user."' ";  
		$success= $this->connection->createCommand($SQL)->query();
		
		//check if there is a tecs2go linked to this license
		$SQL =  "SELECT * FROM proctorv_request  WHERE id_license = '$id_license' ";  
		$proctorv_request= $this->connection->createCommand($SQL)->queryAll();
		
		//get user info
		$SQL =  "SELECT * FROM users_c  WHERE id_usuario_c = '$id_user' ";  
		$user= $this->connection->createCommand($SQL)->queryAll();
		
		
		if($proctorv_request[0]["id_proctorv_request"] != "" ){
			//UPDATE REVOKE STATUS TECS2GO REQUEST
			$SQL =  "UPDATE proctorv_request SET status_request = '5' WHERE id_proctorv_request = '".$proctorv_request[0]["id_proctorv_request"]."' ";  
			$success= $this->connection->createCommand($SQL)->query();
		}
		
		
		// agregamos al log del usuario
		$SQL = 'INSERT INTO `fraud_report` (`document`, `first_name`, `sure_name`, `email`, `id_client`, `picture`, `id_user_report`, `name_user_report`, report_date, `comments`, `exam`, `type`)
	            VALUES ("'.$id_user.'", "'.$user[0]['nombres'].'", "'.$user[0]['apellidos'].'", "'.$user[0]['email'].'", "'.$user[0]['id_cliente'].'", "'.$user[0]['ruta_foto'].'",
	            "'.$id_admin.'", "'.$id_admin.'", NOW(), "'.$comment.'", 19, 1)';
	    $log= $this->connection->createCommand($SQL)->query();
		
		// agregamos al fraud Alert 
		$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 5, '$id_admin', NOW(), '$id_license', '$id_exam', '$comment', '".$id_user."')";
		$log= $this->connection->createCommand($SQL)->query();
		
	}
	
	
	//get available hours to take speaking by day
	public function actionVerifySpeakingHours(){
		//vars
		$day = $_REQUEST["newDate"];
		$result = '';
		
		//revisar cantidad de examenes por hora desde las 08:00 hasta las 12
		$SQL = "SELECT COUNT(id_proctorv_user_date) as dates_count FROM `proctorv_user_dates` WHERE `date_picked` BETWEEN '".$day." 08:00:00.000000' AND '".$day." 12:00:00.000000' AND `speaking_date` = 1";
		$counter_am = $this->connection->createCommand($SQL)->queryAll();
		
		//if there is 50 or less we can add one more to this schedule
		if($counter_am[0]["dates_count"] < 50){
			$result .= '<option value="am">AM</option>';
		}
		
		//revisar cantidad de examenes por hora desde las 14:00 hasta las 18
		$SQL = "SELECT COUNT(id_proctorv_user_date) as dates_count FROM `proctorv_user_dates` WHERE `date_picked` BETWEEN '".$day." 14:00:00.000000' AND '".$day." 18:00:00.000000' AND `speaking_date` = 1";
		$counter_pm = $this->connection->createCommand($SQL)->queryAll();
		
		//if there is 50 or less we can add one more to this schedule
		if($counter_pm[0]["dates_count"] < 50){
			
			$result .= '<option value="pm">PM</option>';
		}
		
		
		if($result == ""){
			$result .= '<option value="" selected="selected"></option>';
		}else{
				$result .= '<option value="" selected="selected"></option>';
		}
		
		header('Content-Type: application/json');
		die($result);
	}
	
	//get available hours to take speaking by day
	public function actionVerifyOlineHours(){
		//vars
		$day = $_REQUEST["today"];
		$exploded_day = explode(" ", $day);
		$day = $exploded_day[0];
		
		$counter_by_hour = 0;
		$result = '';
		//revisar cantidad de examenes por hora desde las 08:00 hasta las 16:00
		//DEJAR 20 EXAMENES GENERAL POR HORA
		for($i = 8; $i <=18; $i += 2){
			//set hour to sql
			if(strlen($i) == 1){
			$day = $day." 0".$i.":00:00";	
			}else{
				$day = $day." ".$i.":00:00";	
			}
			$SQL =  "SELECT id_proctorv_user_date FROM proctorv_user_dates WHERE date_picked LIKE '%".$day."%' AND speaking_date = 0";  
			$counter_by_hour = $this->connection->createCommand($SQL)->queryAll();
			
			$count = count($counter_by_hour);
			$day = $_REQUEST["today"];
			
			//actualizado a 30 por hora
			if($count < 30){
				$result .= '<option value="'.$i.'">'.$i.':00 </option> ';	
			}
		}
		
		
		if($result == ""){
			$result = "<h4>No hay horas disponibles para éste día, por favor selecciona otro día.</h4>
			<input type='hidden' name='no_hour' value='0' />";
		}
		
		header('Content-Type: application/json');
		die($result);
	}
	
	public function actionResetDates(){
		//vars
		$id_request = $_REQUEST["id_request"];
		$id_admin = $_REQUEST["id_admin"];
		$id_license =$_REQUEST["id_license"];
		$id_user = $_REQUEST["id_user"];
		
		if(isset($id_request)){
			if($id_license !="" ){
				
				//GET THE ID_LICENSES_CLIENT AND RESTORE IT TO THE STACK
				$SQL =  "SELECT * FROM licenses_user  WHERE id_licencias_usuario = '$id_license' ";  
				$license_client= $this->connection->createCommand($SQL)->queryAll();
	
				 if($license_client[0]["estado"] == 'A'){
			    	//get speaking status
			    	$SQL = "SELECT * FROM exams_user WHERE id_licencia_usuario = '".$id_license."' ORDER BY id_examen_usuario ASC ";
			        $online = $this->connection->createCommand($SQL)->queryAll();
			        
			        //DELETE LICENSE AND EXAMS ONLY IF SPEAKING IS NOT DONE
			        if($online[1]["estado"] == 'A'){
			        	
			        	$comment = 'RESET ONLINE DATE BY '.$id_admin.'  FROM ID REQUEST: '.$id_request.'  ';
			        	//UPDATE TO NULL LICENSE ID, ALLOCATION DATE AND SET PROFILE = 0 TO PROCTORV_REQUEST
						
						//DELETE SPEAKING DATE
						$SQL = "DELETE FROM `proctorv_user_dates` WHERE `id_request` = '".$id_request."' AND speaking_date = 0";
						
						$success= $this->connection->createCommand($SQL)->query();
						
						$Result = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Pending!" width="32"><span class="red_text">RESET DATES SUCCESFUL.</span></h4>';
					}else{ // Speaking Exam STATUS DONE
						$Result = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Pending!" width="32"><span class="red_text">The ONLINE Exam is DONE, the Date can not be Reset.</span></h4>';	
					}
			    }else{ // License STATUS DONE
						$Result = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Pending!" width="32"><span class="red_text">The License is already DONE, the Date can not be Reset.</span></h4>';	
				}
				
			}else{
						$comment = 'RESET ONLINE DATE BY '.$id_admin.'  FROM ID REQUEST: '.$id_request.'  ';
			        	//UPDATE TO NULL LICENSE ID, ALLOCATION DATE AND SET PROFILE = 0 TO PROCTORV_REQUEST
						
						//DELETE SPEAKING DATE
						$SQL = "DELETE FROM `proctorv_user_dates` WHERE `id_request` = '".$id_request."' AND speaking_date = 0";
						$success= $this->connection->createCommand($SQL)->query();
			}
		}
		
		// agregamos al log del usuario
		$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 5, '$id_admin', NOW(),  '$id_license', '', '$comment', '".$id_user."')";
		$log= $this->connection->createCommand($SQL)->query();
	
	
		header('Content-Type: application/json');
		die($Result);
	}
	
		public function actionResetSpeakingDate(){
		//vars
		$id_request = $_REQUEST["id_request"];
		$id_admin = $_REQUEST["id_admin"];
		$id_license =$_REQUEST["id_license"];
		$id_user = $_REQUEST["id_user"];
		
		if(isset($id_request)){
			if($id_license !="" ){
				
				//GET THE ID_LICENSES_CLIENT AND RESTORE IT TO THE STACK
				$SQL =  "SELECT * FROM licenses_user  WHERE id_licencias_usuario = '$id_license' ";  
				$license_client= $this->connection->createCommand($SQL)->queryAll();
	
				 if($license_client[0]["estado"] == 'A'){
			    	//get speaking status
			    	$SQL = "SELECT * FROM exams_user WHERE id_licencia_usuario = '".$id_license."' ORDER BY id_examen_usuario ASC LIMIT 1";
			        $speaking = $this->connection->createCommand($SQL)->queryAll();
			        
			        //DELETE LICENSE AND EXAMS ONLY IF SPEAKING IS NOT DONE
			        if($speaking[0]["estado"] == 'A'){
			        	
			        	$comment = 'RESET SPEAKING DATE BY '.$id_admin.'  FROM ID REQUEST: '.$id_request.'  ';
			        	//UPDATE TO NULL LICENSE ID, ALLOCATION DATE AND SET PROFILE = 0 TO PROCTORV_REQUEST
						
						//DELETE SPEAKING DATE
						$SQL = "DELETE FROM `proctorv_user_dates` WHERE `id_request` = '".$id_request."' AND speaking_date = 1";
						
						$success= $this->connection->createCommand($SQL)->query();
						
						$Result = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Pending!" width="32"><span class="red_text">RESET DATES SUCCESFUL.</span></h4>';
					}else{ // Speaking Exam STATUS DONE
						$Result = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Pending!" width="32"><span class="red_text">The Speaking Exam is DONE, the Date can not be Reset.</span></h4>';	
					}
			    }else{ // License STATUS DONE
						$Result = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Pending!" width="32"><span class="red_text">The License is already DONE, the Date can not be Reset.</span></h4>';	
				}
				
			}else{
						$comment = 'RESET SPEAKING DATE BY '.$id_admin.'  FROM ID REQUEST: '.$id_request.'  ';
			        	//UPDATE TO NULL LICENSE ID, ALLOCATION DATE AND SET PROFILE = 0 TO PROCTORV_REQUEST
						
						//DELETE SPEAKING DATE
						$SQL = "DELETE FROM `proctorv_user_dates` WHERE `id_request` = '".$id_request."' AND speaking_date = 1";
						$success= $this->connection->createCommand($SQL)->query();
			}
		}
		
		// agregamos al log del usuario
		$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 5, '$id_admin', NOW(),  '$id_license', '', '$comment', '".$id_user."')";
		$log= $this->connection->createCommand($SQL)->query();
	
	
		header('Content-Type: application/json');
		die($Result);	
	}
	
	public function actionTransferAdmin(){
		$id_client = $_REQUEST["id_client"];
		$id_admin = $_REQUEST["id_admin"];
		
		//REVOKE LICENSE'S EXAMS
		$SQL =  "UPDATE users_c SET id_cliente = '".$id_client."' WHERE id_usuario_c = '".$id_admin."' ";  
		$success= $this->connection->createCommand($SQL)->query();
		
		
	}
	
	public function actionSaveUniversityPayment(){
		$id_request = $_REQUEST["id_request"];
		$id_user = $_REQUEST["id_user"];
		$payment_number = $_REQUEST["payment_number"];
		$payment_status = $_REQUEST["payment_status"];
		
		
		if($payment_status == 2){
			$payment_description = 'UNIVERSITY TEST';
		}
		
		if($payment_status == 3){
			$payment_description = 'UNIVERSITY COURSE';
		}
		
		
		// agregamos al log del usuario
		$SQL = "INSERT INTO `proctorv_user_payment` (`id_request`, `id_user`, `date_generate`, `payment_number`, `sell_reference`) VALUES ('$id_request', '$id_user', NOW(), '$payment_number', '$payment_description')";
		$query= $this->connection->createCommand($SQL)->query();
		
		$Result = '<h4>Pago TECS2GO registrado.</h4>
		<p><b>Referencia de de pago: </b>'.$payment_number.'<br/>
		<b>Descripción: </b>'.$payment_description.'<br/>
		<a href="/ilto3/proctorv/view/date-tecs2go.php?id='.base64_encode($id_request).'"  class="btn btn-success" id="date_select">Seleccionar Fechas</a>  <img src="/ilto3/proctorv/img/nuevo.png" title="Ir a Selección de Fechas" width="32">	
		</p>';
		header('Content-Type: application/json');
		die($Result);	
	}
	
		public function actionNewTecs2goRequest(){
		$id_taker = $_REQUEST["id_taker"];
		
		$SQL =  "SELECT password_request FROM proctorv_request  WHERE id_user = '$id_taker' ";  
		$password= $this->connection->createCommand($SQL)->queryAll();
		$pss = $password[0]["password_request"];
		
		
		//create PROCTORV REQUEST
        $SQL = "INSERT INTO `proctorv_request` (`id_user`, `date_request`, `date_allocate`, `id_license`, `password_request`, `photo_step`,  `card_step`, `questions_step`, `status_request`, `step_request`) 
                        VALUES ('$id_taker', NOW(), NULL, NULL, '$pss', 2, 2, 1, 0, 0) ";
		$query= $this->connection->createCommand($SQL)->query();
		
	
	}
	
	//notify date pick SPEAKING
	public function actionRememberDatePickSpeaking(){
		$id_taker = $_REQUEST["id_user"];


		$SQL =  "SELECT email FROM users_c  WHERE id_usuario_c = '$id_taker' ";  
		$notification= $this->connection->createCommand($SQL)->queryAll();
		$modUtil = new Utilidades();

		//send email notifications
		$to = $notification[0]["email"];
	    $subject = "Recordatorio de selección de Fecha de Sección Speaking para tu examen TECS2GO";
	    $htmlBody = '<p>Tienes pendiente seleccionar la fecha de la sección Conversacional de tu examen TECS2GO.  Inicia sesión y completa tu registro TECS2GO haciendo clic en el siguiente <a href="https://iltoexams.com/ilto3/proctorv/view/login.php">enlace</a>.<br/>
				
        		Atentamente,<br/><br/>
                    <a href="http://www.iltoexams.com/"><img style="float:left;top:20px;" alt="ILTO Exams" src="iltoexams.com/logo_ILTO.png" /></a>
        		
        ';
                
            
        //send email    
	    $dat = "{From: 'noreply@iltoexams.com', To: '$to', Subject: '$subject', HtmlBody: '$htmlBody'}";//array('From' => 'noreply@iltoexams.com', 'To'=>'dokho_02@hotmail.com', 'Subject'=>'Hola Chris Fer', 'HtmlBody'=>'<strong>Hello</strong> dear Postmark user.');
	    $modUtil->sendEmail($dat);
		
		$Result = "A notification email was sent to ".$to;
		header('Content-Type: application/json');
		die($Result);
	
	}
	
	//notify date pick ONLINE
	public function actionRememberDatePick(){
		$id_taker = $_REQUEST["id_user"];


		$SQL =  "SELECT email FROM users_c  WHERE id_usuario_c = '$id_taker' ";  
		$notification= $this->connection->createCommand($SQL)->queryAll();
		$modUtil = new Utilidades();

		//send email notifications
		$to = $notification[0]["email"];
	    $subject = "Recordatorio de selección de Fecha de Sección ONLINE para tu examen TECS2GO";
	    $htmlBody = '<p>Tienes pendiente seleccionar la fecha de la sección Online de tu examen TECS2GO.  Inicia sesión y completa tu registro TECS2GO haciendo clic en el siguiente <a href="https://iltoexams.com/ilto3/proctorv/view/login.php">enlace</a>.<br/>
				
        		Atentamente,<br/><br/>
                    <a href="http://www.iltoexams.com/"><img style="float:left;top:20px;" alt="ILTO Exams" src="iltoexams.com/logo_ILTO.png" /></a>
        		
        ';
                
            
        //send email    
	    $dat = "{From: 'noreply@iltoexams.com', To: '$to', Subject: '$subject', HtmlBody: '$htmlBody'}";//array('From' => 'noreply@iltoexams.com', 'To'=>'dokho_02@hotmail.com', 'Subject'=>'Hola Chris Fer', 'HtmlBody'=>'<strong>Hello</strong> dear Postmark user.');
	    $modUtil->sendEmail($dat);
		
		$Result = "A notification email was sent to ".$to;
		header('Content-Type: application/json');
		die($Result);
	
	}
	
	public function actionRememberPhotoPick(){
		$id_taker = $_REQUEST["id_user"];


		$SQL =  "SELECT email FROM users_c  WHERE id_usuario_c = '$id_taker' ";  
		$notification= $this->connection->createCommand($SQL)->queryAll();
		$modUtil = new Utilidades();

		//send email notifications
		$to = $notification[0]["email"];
	    $subject = "Recordatorio de toma de Foto de Cara para tu examen TECS2GO";
	    $htmlBody = '<p>Tienes pendiente la toma de foto de cara de tu examen TECS2GO.  Inicia sesión y completa tu registro TECS2GO haciendo clic en el siguiente <a href="https://iltoexams.com/ilto3/proctorv/view/login.php">enlace</a>.<br/>
				
        		Atentamente,<br/><br/>
                    <a href="http://www.iltoexams.com/"><img style="float:left;top:20px;" alt="ILTO Exams" src="iltoexams.com/logo_ILTO.png" /></a>
        		
        ';
                
            
        //send email    
	    $dat = "{From: 'noreply@iltoexams.com', To: '$to', Subject: '$subject', HtmlBody: '$htmlBody'}";//array('From' => 'noreply@iltoexams.com', 'To'=>'dokho_02@hotmail.com', 'Subject'=>'Hola Chris Fer', 'HtmlBody'=>'<strong>Hello</strong> dear Postmark user.');
	    $modUtil->sendEmail($dat);
		
		$Result = "A notification email was sent to ".$to;
		header('Content-Type: application/json');
		die($Result);
	
	}
	
	public function actionRememberCardPick(){
		$id_taker = $_REQUEST["id_user"];


		$SQL =  "SELECT email FROM users_c  WHERE id_usuario_c = '$id_taker' ";  
		$notification= $this->connection->createCommand($SQL)->queryAll();
		$modUtil = new Utilidades();

		//send email notifications
		$to = $notification[0]["email"];
	    $subject = "Recordatorio de toma de Foto de tu Documento para tu examen TECS2GO";
	    $htmlBody = '<p>Tienes pendiente la toma de foto de tu documento para tu examen TECS2GO.  Inicia sesión y completa tu registro TECS2GO haciendo clic en el siguiente <a href="https://iltoexams.com/ilto3/proctorv/view/login.php">enlace</a>.<br/>
				
        		Atentamente,<br/><br/>
                    <a href="http://www.iltoexams.com/"><img style="float:left;top:20px;" alt="ILTO Exams" src="iltoexams.com/logo_ILTO.png" /></a>
        		
        ';
                
            
        //send email    
	    $dat = "{From: 'noreply@iltoexams.com', To: '$to', Subject: '$subject', HtmlBody: '$htmlBody'}";//array('From' => 'noreply@iltoexams.com', 'To'=>'dokho_02@hotmail.com', 'Subject'=>'Hola Chris Fer', 'HtmlBody'=>'<strong>Hello</strong> dear Postmark user.');
	    $modUtil->sendEmail($dat);
		
		$Result = "A notification email was sent to ".$to;
		header('Content-Type: application/json');
		die($Result);
	
	}
	
	
	//new UPDATE TAKER CONTACT AT EXAM START
	public function actionupdateContactTaker(){
		$id_user = $_REQUEST["id_user"];
		$phone = $_REQUEST["phone"];
		
		//REVOKE LICENSE'S EXAMS
		$SQL =  "UPDATE users_c SET telefono = '".$phone."' WHERE id_usuario_c = '".$id_user."' ";  
		$success= $this->connection->createCommand($SQL)->query();
		
		
	}
	
	// set PSE payment to OK
	public function actionConfirmPayment(){
		$id_payment = $_REQUEST["id_payment"];
		$id_admin = $_REQUEST["id_admin"];
		$id_request = $_REQUEST["id_request"];
		$id_user = $_REQUEST["id_user"];
		$status = $_REQUEST["flag"];
		
		//set the payment state 4 as OK
		$SQL = "UPDATE `proctorv_user_payment` SET `status`= $status WHERE `id_proctorv_user_payment`='".$id_payment."'";
        $updatePayment= Yii::app()->db->createCommand($SQL)->query();
        if($status == 4){
        	$comment = "PAYMENT Verified and Approved by Admin Request ID: $id_request ";
        	$Result = "The Taker can Now select the Dates, please Refresh the page.";
        }else{
        	$comment = "PAYMENT Reject by Admin Request ID: $id_request ";
        	$Result = "The Payment was Rejected, please Refresh the page.";
        }
        
        
        // agregamos al log del usuario
		$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 5, '$id_admin', NOW(),  '$id_request', '', '$comment', '".$id_user."')";
		$log= $this->connection->createCommand($SQL)->query();
		
		
		
		header('Content-Type: application/json');
		die($Result);
	
	}
	
	// set PSE payment to OK
	public function actionConfirmNewPayment(){
		
		$id_admin = $_REQUEST["id_admin"];
		$id_request = $_REQUEST["id_request"];
		$id_user = $_REQUEST["id_user"];
		
		//register the new payment
		$SQL = "INSERT INTO `proctorv_user_payment`(`id_proctorv_user_payment`, `id_request`, `id_user`, `date_generate`, `payment_number`) VALUES (NULL, '$id_request', '$id_user', NOW(), 'Approved by ILTO')";
		$payment= $this->connection->createCommand($SQL)->query();
		
		
		$comment = "PAYMENT Verified and Approved by Admin Request ID: $id_request ";
        // agregamos al log del usuario
		$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 5, '$id_admin', NOW(),  '$id_request', '', '$comment', '".$id_user."')";
		$log= $this->connection->createCommand($SQL)->query();
		
		
		$Result = "The Taker can Now select the Dates";
		header('Content-Type: application/json');
		die($Result);
	
	}
	
	// set PSE payment to OK
	public function actionRejectUniPayment(){
		$id_payment = $_REQUEST["id_payment"];
		$id_admin = $_REQUEST["id_admin"];
		$id_request = $_REQUEST["id_request"];
		$id_user = $_REQUEST["id_user"];
		$status = $_REQUEST["flag"];
		
		if(isset($id_request) && $id_request != ""){
			//DELETE UNIVERSITY PAYMENT
			$SQL = "DELETE FROM `proctorv_user_payment` WHERE `id_request` = '".$id_request."' AND id_user = '".$id_user."' ";
			$success= $this->connection->createCommand($SQL)->query();
		}
		
		
		$comment = "UNIVERSITY PAYMENT DELETED by Admin Request ID: $id_request ";
        $Result = "Taker PAYMENT ENABLED, please reload the page.";
    
        
        // agregamos al log del usuario
		$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 5, '$id_admin', NOW(),  '$id_request', '', '$comment', '".$id_user."')";
		$log= $this->connection->createCommand($SQL)->query();
		
		
		
		header('Content-Type: application/json');
		die($Result);
	
	}
	
	// R Qualify License
	public function actionQualifyLicense(){
		
		$id_user = $_REQUEST["id_user"];
		$id_license = $_REQUEST["id_license"];
		$id_admin = $_REQUEST["id_admin"];
		$speaking = $_REQUEST["spe"];
		$grammar = $_REQUEST["gra"];
		$reading = $_REQUEST["rea"];
		$lisening = $_REQUEST["lis"];
		
		$notes = array($speaking, $grammar, $reading, $lisening);
		$i = 0;
		if(isset($id_license) && $id_license != ""){
			//DELETE UNIVERSITY PAYMENT
			
			
			$SQL = "SELECT id_examen_usuario, calificacion FROM exams_user WHERE id_licencia_usuario ='".$id_license."'";
			$exams = $this->connection->createCommand($SQL)->queryAll();
	
			$lastNotes = array();
			foreach($exams as $exam){
				 array_push($lastNotes, $exam["calificacion"]);
				if(((($notes[$i])>=0)&&(($notes[$i])<=12)))
                     {
                          $TECSPROFICIENCY ="1";
                          $CEFBYTECS ="PA1";
                          $cefrMess = 'Entry User';
                     }
                  if(((($notes[$i])>12)&&(($notes[$i])<=27)))
                     {
                          $TECSPROFICIENCY ="2";
                          $CEFBYTECS ="A1";
                          $cefrMess = 'Entry User';

                     }
                  if(((($notes[$i])>27)&&(($notes[$i])<=38)))
                     {
                          $TECSPROFICIENCY ="3";
                          $CEFBYTECS ="A2";
                          $cefrMess = 'Basic User';
                     }
                  if(((($notes[$i])>38)&&(($notes[$i])<=50)))
                     {
                          $TECSPROFICIENCY ="4";
                          $CEFBYTECS ="A2+";
                          $cefrMess = 'Basic User';
                     }
                  if(((($notes[$i])>50)&&(($notes[$i])<=62)))
                     {
                          $TECSPROFICIENCY ="5";
                          $CEFBYTECS ="B1";
                          $cefrMess = 'Intermediate User';
                     }
                  if(((($notes[$i])>62)&&(($notes[$i])<=72)))
                     {
                          $TECSPROFICIENCY ="6";
                          $CEFBYTECS ="B1+";
                          $cefrMess = 'Intermediate User';
                     }
                    if(((($notes[$i])>72)&&(($notes[$i])<=80)))
                     {
                          $TECSPROFICIENCY ="7";
                          $CEFBYTECS ="B2";
                          $cefrMess = 'Independent User';
                     }
                     if(((($notes[$i])>80)&&(($notes[$i])<=87)))
                     {
                          $TECSPROFICIENCY ="8";
                          $CEFBYTECS ="B2+";
                          $cefrMess = 'Independent User';
                     }
                     if(((($notes[$i])>87)&&(($notes[$i])<=100)))
                     {
                          $TECSPROFICIENCY ="9";
                          $CEFBYTECS ="C1";
                          $cefrMess = 'Proficient User';
                     }
                
                //update score state and LVL exams by id     
				$SQL = "UPDATE `exams_user` SET `calificacion`='".$notes[$i]."', `nivel`= '".$CEFBYTECS."', estado='F' WHERE `id_examen_usuario`='".$exam["id_examen_usuario"]."'";
				$success= $this->connection->createCommand($SQL)->query();
				$i++;
			}
			
			
			
			//update user status
			$SQL = "UPDATE `users_c` SET estado='A' WHERE `id_usuario_c`='".$id_user."'";
			$success= $this->connection->createCommand($SQL)->query();
			$modUtil = new Utilidades();
			$license_results= $modUtil->calificaLicencia($id_license);
			
			//update License Status
			$SQL = "UPDATE `licenses_user` SET `calificacion`='".$license_results["calificacion"]."', `nivel`= '".$license_results["nivel"]."', `estado`='F' WHERE `id_licencias_usuario`='".$id_license."'";
			$success= $this->connection->createCommand($SQL)->query();
			
		}
			
		
	
		
		$comment = "UPDATE SCORE EXAMS : ".$lastNotes[0]." ".$lastNotes[1]." ".$lastNotes[2]." ".$lastNotes[3]." BY ".$notes[0]." ".$notes[1]." ".$notes[2]." ".$notes[3]." ";
        $Result = "Scores updated.";
        
        // agregamos al log del usuario
		$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 5, '$id_admin', NOW(),  '$id_license', '', '$comment', '".$id_user."')";
		$log= $this->connection->createCommand($SQL)->query();
		
		
		
		header('Content-Type: application/json');
		die($Result);
	
	}
	
	// set PSE payment to OK
	public function actionValidateVote(){
		$id_vote_user_v = $_REQUEST["id_vote_user_v"];
		
		$SQL = "UPDATE `vote_user_v` SET `validated`=1 WHERE `id_vote_user_v`='".$id_vote_user_v."'";
		$log= $this->connection->createCommand($SQL)->query();	
		$Result = "<h3>Voto Verificado</h3>";
		
		header('Content-Type: application/json');
		die($Result);
	
	}
	
	// set PSE payment to OK
	public function actionNullVote(){
		$id_vote_user_v = $_REQUEST["id_vote_user_v"];
		
		$SQL = "UPDATE `vote_user_v` SET `validated`=2 WHERE `id_vote_user_v`='".$id_vote_user_v."'";
		$log= $this->connection->createCommand($SQL)->query();	
		$Result = "<h3>Voto Verificado</h3>";
		
		header('Content-Type: application/json');
		die($Result);
	
	}
	
	
	//get available hours to take speaking by day
	public function actionVerifySisbenHours(){
		//vars
		$day = $_REQUEST["today"];
		$exploded_day = explode(" ", $day);
		$day = $exploded_day[0];
		
		$counter_by_hour = 0;
		$result = '';
		//revisar cantidad de examenes por hora desde las 08:00 hasta las 16:00
		//DEJAR 20 EXAMENES GENERAL POR HORA
		for($i = 8; $i <=17; $i ++){
			//set hour to sql
			if(strlen($i) == 1){
			$day = $day." 0".$i.":00:00";	
			}else{
				$day = $day." ".$i.":00:00";	
			}
			$SQL =  "SELECT id_users_sis_dates FROM users_sis_dates WHERE date_picked LIKE '%".$day."%' ";  
			$counter_by_hour = $this->connection->createCommand($SQL)->queryAll();
			
			$count = count($counter_by_hour);
			$day = $_REQUEST["today"];
			
			//actualizado a 30 por hora
			if($count < 24){
				$result .= '<option value="'.$i.'">'.$i.':00 ( '.$count.' )</option> ';	
			}
		}
		
		
		if($result == ""){
			$result = "<h4>No hay horas disponibles para éste día, por favor selecciona otro día.</h4>
			<input type='hidden' name='no_hour' value='0' />";
		}
		
		header('Content-Type: application/json');
		die($result);
	}
	
    /** --- END TECS2GO ADMIN METHODS ---- **/
    	
	/** 
	TECS ACADEMY METHODS 
	**/
	
	// set PSE payment to OK
	public function actionConfirmNewAcademyPayment(){
		
		$id_admin = $_REQUEST["id_admin"];
		$id_request = $_REQUEST["id_request"];
		$id_user = $_REQUEST["id_user"];
		$reference = $_REQUEST["reference"];
		$referenceCode = $id_user."_".$id_request."_".date("h:i:s");
		
		//register the new payment
		$SQL = "INSERT INTO `proctorv_user_payment`(`id_proctorv_user_payment`, `id_request`, `id_user`, `date_generate`, `payment_number`, `status`, `sell_reference`, `transaction_reference`) VALUES (NULL, '$id_request', '$id_user', NOW(), 'Approved by ILTO', 4, '".$reference."', '".$referenceCode."')";
		$payment= $this->connection->createCommand($SQL)->query();
		
		$comment = "PAYMENT Verified and Approved by Admin Request ID: $id_request ";
        // agregamos al log del usuario
		$SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 5, '$id_admin', NOW(),  '$id_request', '', '$comment', '".$id_user."')";
		$log= $this->connection->createCommand($SQL)->query();
		
		//update consultan history request status info
		$SQL = "UPDATE `consultant_history` SET payment_status = 4, description = '".$reference."' WHERE `id_product`='".$id_request."' AND description ='TECS-ACADEMY' ";
		$success= $this->connection->createCommand($SQL)->query();
		
		//update TECS ACADEMY REGISTER TO ENABLED TECS INTRO
		if($reference == 'TECS-INTRO'){
		    $SQL = "UPDATE `tecs_academy` SET tecs_intro = 1 WHERE `id_tecs_academy`='".$id_request."' ";
		}
		
		if($reference == 'TECS-MASTER'){
		    $SQL = "UPDATE `tecs_academy` SET tecs_master = 1 WHERE `id_tecs_academy`='".$id_request."' ";
		}
		$success= $this->connection->createCommand($SQL)->query();
		
		
		$Result = "The Taker can Now select the Dates";
		header('Content-Type: application/json');
		die($Result);
	
	}
	
	/**
	 * TECS ACADEMY FUNCTIONS
	 * 
	 */
	
	public function actionAcademyAdmin()
	{
	   $SQL = "SELECT tecs_academy.id_tecs_academy, tecs_academy.id_user, nombres, apellidos, tecs_intro, tecs_master, id_license, date_request, telefono, email FROM `tecs_academy` 
        INNER JOIN users_c ON tecs_academy.id_user = users_c.id_usuario_c 
        ORDER BY `tecs_academy`.`date_request` DESC";
        $list_u= $this->connection->createCommand($SQL)->queryAll();
        
        $html = '<div class="widget-header">
                    <span class="widget-caption"><b>TECS MASTER REQUESTS</b></span>
                        <a href="/ilto3/index.php?r=Clients/usersC/create" class="btn btn-success">Create Users</a>
                    </div>
                    <div class="widget-body">
                        <form class="search-form" action="/ilto3/index.php?r=Api/userAcademy" onsubmit="return false">
                            <table class="table table-striped table-hover table-bordered" id="editabledatatable">
                                <thead>
                                    <tr role="row">
                                        <th>
                                        DATE REQUEST
                                        <br>
                                        </th>
                                        <th>
                                        USERNAME / ID
                                        <br>
                                        <input type="text" name="id_usuario_c" class="search form-control" data-related="users_c.id_usuario_c"/>
                                        </select>
                                        </th>
                                        <th>
                                        NAME
                                        <br>
                                        <input type="text" name="nombres" class="search form-control" data-related="users_c.nombres"/>
                                        </th>
                                        <th>
                                        SURNAME
                                        <br>
                                        <input type="text" name="apellidos" class="search form-control" data-related="users_c.apellidos"/>
                                        </th>
                                        <th>
                                        CONTACT
                                        <br>
                                        </th>
                                        <th>
                                        EMAIL
                                        <br>
                                        </th>
                                        <th>
                                        CONSULTANT
                                        <br>
                                        </th>
                                        <th>
                                        TECS INTRO
                                        <br>
                                        </th>
                                        <th>
                                        TECS MASTER
                                        <br>
                                        </th>
                                        <th>
                                        EDE LVL
                                        <br>
                                        <select class="form-control search" name="status" data-related="users_c.estado">
                                            <option value=""></option>
                                            <option value="PA1">PRE A1</option>
                                            <option value="A1">A1</option>
                                            <option value="A2">A2</option>
                                            <option value="A2+">A2+</option>
                                            <option value="B1">B1</option>
                                            <option value="B1+">B1+</option>
                                            <option value="B2">B2</option>
                                            <option value="B2+">B2+</option>
                                            <option value="C1">C1</option>
                                        </select>
                                        </th>
                                        <th>
                                        ACTIONS
                                        </th>
                                         
                                        </tr>
                                </thead>
                            <tbody class="search-rows">';
                                         
                            foreach($list_u as $user){
                                $SQL = "SELECT * FROM `tecs_academy_dates` WHERE `id_tecs_academy` = ".$user['id_tecs_academy']." and type = 0 ";
                                $date= $this->connection->createCommand($SQL)->queryAll();
                                
                                //get consultant info
                                $SQL = "SELECT id_consultant, nombres FROM `consultant_history` 
                                    INNER JOIN users_c ON consultant_history.id_consultant = users_c.id_usuario_c
                                    WHERE `id_product` = ".$user['id_tecs_academy']." ";
                                $consultant = $this->connection->createCommand($SQL)->queryAll();
                                                    
                                //ede license
                                if(isset($user['id_license']) && $user['id_license'] != NULL){
                                    //get consultant 
                                    $SQL = "SELECT nivel FROM `licenses_user` `t` WHERE id_licencias_usuario= '".$user['id_license']."' ";
                                    $ede_lvl= $this->connection->createCommand($SQL)->queryAll();
                                    $lvl = $ede_lvl[0]["nivel"];
                                    if($lvl == ""){
                                        $lvl = "PENDING PLACEMENT";
                                    }
                                }else{
                                    $lvl = "PENDING ALLOCATING";
                                }
                                                    
                                $html .= '<tr>
                                    <td>'.$user['date_request'].'</td>
                                    <td>'.$user['id_user'].'</td>
                                    <td>'.utf8_encode($user['nombres']).'</td>
                                    <td>'.utf8_encode($user['apellidos']).'</td>
                                    <td>'.utf8_encode($user['telefono']).'</td>
                                    <td>'.utf8_encode($user['email']).'</td>
                                    <td>'.utf8_encode($consultant[0]['nombres']).'</td>';
                                    if($user['tecs_intro']==1){
                                        $html .= '<td><img src="/ilto3/images/iconos/verified.png" title="Pending Speaking!" width="16"></td>';
                                    }else{
                                        $html .= '<td></td>';
                                    }
                                    if($user['tecs_master']==1){
                                        $html .= '<td><img src="/ilto3/images/iconos/verified.png" title="Pending Speaking!" width="16"></td>';
                                    }else{
                                        $html .= '<td></td>';
                                    }
                                    
                                    $html .='<td>'.$lvl.'</td>
                                    <td>
                                    <a href="/ilto3/index.php?r=Clients/TecsAcademy/RequestDetail/id/'.base64_encode($user["id_tecs_academy"]).'" class="btn btn-primary" id="allocate">Details</a>
                                    </td>
                                    </tr>';
                                }
                            $html .= '</tbody>
                                </table>
                                </form>
                                </div>';            

	    $Result = $html;
	   
	   header('Content-Type: application/json');
	   die($Result);
	}
	
	
	public function actionIntroAdmin()
	{
	    
	    $SQL = "SELECT tecs_academy.id_tecs_academy, tecs_academy.id_user, nombres, apellidos, tecs_intro, tecs_master, id_license, date_request, telefono, email FROM `tecs_academy` 
        INNER JOIN users_c ON tecs_academy.id_user = users_c.id_usuario_c 
        WHERE tecs_academy.tecs_intro = 1
        ORDER BY `tecs_academy`.`date_request` DESC";
        $list_u= $this->connection->createCommand($SQL)->queryAll();
        
        $html = '<div class="widget-header">
                    <span class="widget-caption"><b>TECS INTRO REQUESTS</b></span>
                        <a href="/ilto3/index.php?r=Clients/usersC/create" class="btn btn-success">Create Users</a>
                    </div>
                    <div class="widget-body">
                        <form class="search-form" action="/ilto3/index.php?r=Api/userAcademy" onsubmit="return false">
                            <table class="table table-striped table-hover table-bordered" id="editabledatatable">
                                <thead>
                                    <tr role="row">
                                        <th>
                                        DATE SCHEDULE
                                        <br>
                                        </th>
                                        <th>
                                        USERNAME / ID
                                        <br>
                                        <input type="text" name="id_usuario_c" class="search form-control" data-related="users_c.id_usuario_c"/>
                                        </select>
                                        </th>
                                        <th>
                                        NAME
                                        <br>
                                        <input type="text" name="nombres" class="search form-control" data-related="users_c.nombres"/>
                                        </th>
                                        <th>
                                        SURNAME
                                        <br>
                                        <input type="text" name="apellidos" class="search form-control" data-related="users_c.apellidos"/>
                                        </th>
                                        <th>
                                        CONTACT
                                        <br>
                                        </th>
                                        <th>
                                        EMAIL
                                        <br>
                                        </th>
                                        <th>
                                        CONSULTANT
                                        <br>
                                        </th>
                                        <th>
                                        EDE LVL
                                        <br>
                                        <select class="form-control search" name="status" data-related="users_c.estado">
                                            <option value=""></option>
                                            <option value="PA1">PRE A1</option>
                                            <option value="A1">A1</option>
                                            <option value="A2">A2</option>
                                            <option value="A2+">A2+</option>
                                            <option value="B1">B1</option>
                                            <option value="B1+">B1+</option>
                                            <option value="B2">B2</option>
                                            <option value="B2+">B2+</option>
                                            <option value="C1">C1</option>
                                        </select>
                                        </th>
                                        <th>
                                        ACTIONS
                                        </th>
                                    </tr>
                                </thead>
                            <tbody class="search-rows">';
                                         
                            foreach($list_u as $user){
                                $SQL = "SELECT * FROM `tecs_academy_dates` WHERE `id_tecs_academy` = ".$user['id_tecs_academy']." and type = 0 ";
                                $date= $this->connection->createCommand($SQL)->queryAll();
                                
                                $timezone = explode(" ", $date[0]['date_selected']);
                                
                                //get consultant info
                                $SQL = "SELECT id_consultant, nombres FROM `consultant_history` 
                                    INNER JOIN users_c ON consultant_history.id_consultant = users_c.id_usuario_c
                                    WHERE `id_product` = ".$user['id_tecs_academy']." ";
                                $consultant = $this->connection->createCommand($SQL)->queryAll();
                                                    
                                //ede license
                                if(isset($user['id_license']) && $user['id_license'] != NULL){
                                    //get consultant 
                                    $SQL = "SELECT nivel FROM `licenses_user` `t` WHERE id_licencias_usuario= '".$user['id_license']."' ";
                                    $ede_lvl= $this->connection->createCommand($SQL)->queryAll();
                                    $lvl = $ede_lvl[0]["nivel"];
                                    if($lvl == ""){
                                        $lvl = "PENDING PLACEMENT";
                                    }
                                }else{
                                    $lvl = "PENDING ALLOCATING";
                                }
                                                    
                                $html .= '<tr>
                                    <td>'.$timezone[1].'</td>
                                    <td>'.$user['id_user'].'</td>
                                    <td>'.utf8_encode($user['nombres']).'</td>
                                    <td>'.utf8_encode($user['apellidos']).'</td>
                                    <td>'.utf8_encode($user['telefono']).'</td>
                                    <td>'.utf8_encode($user['email']).'</td>
                                    <td>'.utf8_encode($consultant[0]['nombres']).'</td>
                                    <td>'.$lvl.'</td>
                                    <td>
                                    <a href="/ilto3/index.php?r=Clients/TecsAcademy/RequestDetail/id/'.base64_encode($user["id_tecs_academy"]).'" class="btn btn-primary" id="allocate">Details</a>
                                    </td>
                                    </tr>';
                                }
                            $html .= '</tbody>
                                </table>
                                </form>
                                </div>';            

	    $Result = $html;
	   
	   header('Content-Type: application/json');
	   die($Result);
	}
	
	public function actionMasterAdmin()
	{
	   $SQL = "SELECT tecs_academy.id_tecs_academy, tecs_academy.id_user, nombres, apellidos, tecs_intro, tecs_master, id_license, date_request, telefono, email FROM `tecs_academy` 
        INNER JOIN users_c ON tecs_academy.id_user = users_c.id_usuario_c 
        WHERE tecs_academy.tecs_master = 1
        ORDER BY `tecs_academy`.`date_request` DESC";
        $list_u= $this->connection->createCommand($SQL)->queryAll();
        
        $html = '<div class="widget-header">
                    <span class="widget-caption"><b>TECS MASTER REQUESTS</b></span>
                        <a href="/ilto3/index.php?r=Clients/usersC/create" class="btn btn-success">Create Users</a>
                    </div>
                    <div class="widget-body">
                        <form class="search-form" action="/ilto3/index.php?r=Api/userAcademy" onsubmit="return false">
                            <table class="table table-striped table-hover table-bordered" id="editabledatatable">
                                <thead>
                                    <tr role="row">
                                        <th>
                                        DATE SCHEDULE
                                        <br>
                                        </th>
                                        <th>
                                        USERNAME / ID
                                        <br>
                                        <input type="text" name="id_usuario_c" class="search form-control" data-related="users_c.id_usuario_c"/>
                                        </select>
                                        </th>
                                        <th>
                                        NAME
                                        <br>
                                        <input type="text" name="nombres" class="search form-control" data-related="users_c.nombres"/>
                                        </th>
                                        <th>
                                        SURNAME
                                        <br>
                                        <input type="text" name="apellidos" class="search form-control" data-related="users_c.apellidos"/>
                                        </th>
                                        <th>
                                        CONTACT
                                        <br>
                                        </th>
                                        <th>
                                        EMAIL
                                        <br>
                                        </th>
                                        <th>
                                        CONSULTANT
                                        <br>
                                        </th>
                                        <th>
                                        EDE LVL
                                        <br>
                                        <select class="form-control search" name="status" data-related="users_c.estado">
                                            <option value=""></option>
                                            <option value="PA1">PRE A1</option>
                                            <option value="A1">A1</option>
                                            <option value="A2">A2</option>
                                            <option value="A2+">A2+</option>
                                            <option value="B1">B1</option>
                                            <option value="B1+">B1+</option>
                                            <option value="B2">B2</option>
                                            <option value="B2+">B2+</option>
                                            <option value="C1">C1</option>
                                        </select>
                                        </th>
                                        <th>
                                        ACTIONS
                                        </th>
                                    </tr>
                                </thead>
                            <tbody class="search-rows">';
                                         
                            foreach($list_u as $user){
                                $SQL = "SELECT * FROM `tecs_academy_dates` WHERE `id_tecs_academy` = ".$user['id_tecs_academy']." and type = 0 ";
                                $date= $this->connection->createCommand($SQL)->queryAll();
                                
                                //get consultant info
                                $SQL = "SELECT id_consultant, nombres FROM `consultant_history` 
                                    INNER JOIN users_c ON consultant_history.id_consultant = users_c.id_usuario_c
                                    WHERE `id_product` = ".$user['id_tecs_academy']." ";
                                $consultant = $this->connection->createCommand($SQL)->queryAll();
                                                    
                                //ede license
                                if(isset($user['id_license']) && $user['id_license'] != NULL){
                                    //get consultant 
                                    $SQL = "SELECT nivel FROM `licenses_user` `t` WHERE id_licencias_usuario= '".$user['id_license']."' ";
                                    $ede_lvl= $this->connection->createCommand($SQL)->queryAll();
                                    $lvl = $ede_lvl[0]["nivel"];
                                    if($lvl == ""){
                                        $lvl = "PENDING PLACEMENT";
                                    }
                                }else{
                                    $lvl = "PENDING ALLOCATING";
                                }
                                                    
                                $html .= '<tr>
                                    <td>'.$date[0]['date_selected'].'</td>
                                    <td>'.$user['id_user'].'</td>
                                    <td>'.utf8_encode($user['nombres']).'</td>
                                    <td>'.utf8_encode($user['apellidos']).'</td>
                                    <td>'.utf8_encode($user['telefono']).'</td>
                                    <td>'.utf8_encode($user['email']).'</td>
                                    <td>'.utf8_encode($consultant[0]['nombres']).'</td>
                                    <td>'.$lvl.'</td>
                                    <td>
                                    <a href="/ilto3/index.php?r=Clients/TecsAcademy/RequestDetail/id/'.base64_encode($user["id_tecs_academy"]).'" class="btn btn-primary" id="allocate">Details</a>
                                    </td>
                                    </tr>';
                                }
                            $html .= '</tbody>
                                </table>
                                </form>
                                </div>';            

	    $Result = $html;
	   
	   header('Content-Type: application/json');
	   die($Result);
	}

	
}
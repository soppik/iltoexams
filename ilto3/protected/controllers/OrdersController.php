<?php

class OrdersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin', 'updateOrder'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

    /**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Orders;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Orders']))
		{
			$model->attributes=$_POST['Orders'];
                        $model->fecha_asignacion = new CDbExpression('NOW()');
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Update View a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Orders']))
		{
			$model->attributes=$_POST['Orders'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Update Method a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdateOrder(){
		
		$modUtil = new Utilidades();
		$id_ede = $modUtil->getIdEde();
		$id_tecs = $modUtil->getIdTecs();
		$subtotal_order = 0;
		
		if(is_null($_POST["Order"]["free_ede"])){
			$free = "off";
		}else{
			$free = "on";
		}
		$nofree = "off";
		
		if(isset($_POST[$id_tecs]) && !empty($_POST[$id_tecs])){
			$subtotal_order_lic = $modUtil->updateSingleLicence($_POST[$id_tecs]["id_order_license"],$_POST[$id_tecs]["amount"],$_POST[$id_tecs]["price"],$noFree,$_POST["currency_value"]);
		}else{
			$subtotal_order_lic = 0;
		}
		$sutotal_order = $sutotal_order + $subtotal_order_lic;
		if(isset($_POST[$id_ede]) && !empty($_POST[$id_ede])){
			$subtotal_order_lic = $modUtil->updateSingleLicence($_POST[$id_ede]["id_order_license"],$_POST[$id_ede]["amount"],$_POST[$id_ede]["price"],$free,$_POST["currency_value"]);
		}else{
			$subtotal_order_lic = 0;
		}
		$subtotal_order = $sutotal_order + $subtotal_order_lic;
		if(isset($_POST[17]) && !empty($_POST[17])){
			$subtotal_order_lic = $modUtil->updateSingleLicence($_POST[17]["id_order_license"],$_POST[17]["amount"],$_POST[17]["price"],$noFree,$_POST["currency_value"]);
		}else{
			$subtotal_order_lic = 0;
		}
		$sutotal_order = $sutotal_order + $subtotal_order_lic;
		if(isset($_POST[18]) && !empty($_POST[18])){
			$subtotal_order_lic = $modUtil->updateSingleLicence($_POST[18]["id_order_license"],$_POST[18]["amount"],$_POST[18]["price"],$noFree,$_POST["currency_value"]);
		}else{
			$subtotal_order_lic = 0;
		}
		
		$subtotal_order = $subtotal_order + $subtotal_order_lic;
        $discount = ($subtotal_order*$_POST["Order"]["discount"])/100;
		$total_order = $subtotal_order - $discount;
		
		switch ($_POST["Order"]["due"]) {
        	case '0':
        		$date_expire = $_POST["Order"]["date_due"];
        		break;
        	case '1':
        		$date_expire = date('Y-m-d H:i:s', strtotime($_POST["Order"]["date_due"]. ' + 30 days'));;
        		break;
        	case '2':
        		$date_expire = date('Y-m-d H:i:s', strtotime($_POST["Order"]["date_due"]. ' + 60 days'));;
        		break;
        	case '3':
        		$date_expire = date('Y-m-d H:i:s', strtotime($_POST["Order"]["date_due"]. ' + 90 days'));;
        		break;
        	default:
        		// code...
        		break;
        }
        
		$SQL = "UPDATE `orders` SET `subtotal`='".$subtotal_order."',`discount`='".$_POST["Order"]["discount"]."',`total`='".$total_order."',`status`='".$_POST["Order"]["status"]."',`check_ilto`='1', `date_expire`='".$date_expire."', terms = '".$_POST["Order"]["due"]."'  WHERE `id_order`='".$_POST["Order"]["id_order"]."'";
        $update_order = Yii::app()->db->createCommand($SQL)->query();
            
        // SEND EMAIL 
        $SQL = "SELECT nombres, apellidos, email FROM `users_c` WHERE id_cliente='".$_POST[0]["id_client"]."' AND id_perfil='2'";
        $toaa= Yii::app()->db->createCommand($SQL)->queryAll();
            
        //Verificar si tiene distribuidor
        $id_order= $_POST["Order"]["id_order"];
            
        $tooaEmail = $_POST["Order"]["email"];       
        
        // Se envia correo de la orden de compra al cliente y a iltoexams
        $to = $tooaEmail;
        $subject = 'Quotation No. '.$id_order.' Checked';
		$msg = '<form action="https://iltoexams.com/ilto3/index.php?r=Clients/default/login" method="post">
		<p style="text-aling:justify;color:#5d5c5c;">
		Your quote number No. '.$id_order.' has been approved by ILTO. Click on the link to now further details and confirm the information.<br/><br/>
		<input type="hidden" name="flag" value="1">
		<input type="submit" value="View Details" style="border-radius: 10px;padding:15px; margin:20px auto;display:block;width:200px;background-color:#31b744;color:#FFF;font-size:14px;font-weight:bold;margin:auto;">
		</p>
		</form>';
        $data = "{From: 'noreply@iltoexams.com', To: '$to', Subject: '$subject', HtmlBody: '$msg'}";//array('From' => 'noreply@iltoexams.com', 'To'=>'dokho_02@hotmail.com', 'Subject'=>'Hola Chris Fer', 'HtmlBody'=>'<strong>Hello</strong> dear Postmark user.');
          
        $modUtil = new Utilidades();
        $modUtil->sendEmail($data); //habilitamos
        
        $this->render('admin',array(
			'model'=>$model,
		));
		}
		
	
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Orders');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new LicensesClient('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Orders']))
			$model->attributes=$_GET['Orders'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return LicensesClient the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Orders::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param LicensesClient $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='licenses-client-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

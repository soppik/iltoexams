<?php

class LicensesUserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('admin','certficate','emailcertficate'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
        public function actionCertificate($id){
		$modUtil = new Utilidades;
                Yii::import('Clients.models.*');
                $modExamsUser = ExamsUser::model()->findAllByAttributes(array('id_licencia_usuario'=>$id));
               
                if($modExamsUser){
                    foreach($modExamsUser as $examen){
                        if($examen->estado!="F"){
                            Yii::app()->user->setFlash('error','This user still have pending exams ');
                            $this->redirect(array('LicensesUser/admin/id/'.$modExamsUser[0]->idLicenciaUsuario->id_usuario));
                        }
                    }
                   
                    $informe = $modUtil->generaCertificado($id,$modExamsUser);
                    $mPDF1 = Yii::app()->ePdf->mpdf("","Letter");
                    $mPDF1->WriteHTML($informe);
                    $nombre = "cert_".$modExamsUser[0]->idLicenciaUsuario->id_usuario.".pdf";
                    $mPDF1->Output();
                }
        }

        public function actionEmailcertificate($id){
		$modUtil = new Utilidades; 
                $modLicencias = LicensesUser::model()->findByPk($id);
                $modUtil->enviarCertificado($modLicencias->id_usuario,$id);
                $this->redirect(array('LicensesUser/admin/id/'.$modLicencias->id_usuario));        
                
        }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new LicensesUser('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['LicensesUser']))
			$model->attributes=$_GET['LicensesUser'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return LicensesUser the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=LicensesUser::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param LicensesUser $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='licenses-user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

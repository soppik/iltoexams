<?php
/* @var $this ExamsLicenseController */
/* @var $data ExamsLicense */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_examenes_licencia')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_examenes_licencia), array('view', 'id'=>$data->id_examenes_licencia)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_licencia')); ?>:</b>
	<?php echo CHtml::encode($data->id_licencia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_examen')); ?>:</b>
	<?php echo CHtml::encode($data->id_examen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('es_placement')); ?>:</b>
	<?php echo CHtml::encode($data->es_placement); ?>
	<br />


</div>
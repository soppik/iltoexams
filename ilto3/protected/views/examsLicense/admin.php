<?php
/* @var $this ExamsLicenseController */
/* @var $model ExamsLicense */
$this->layout="column1";
$modUtil = new Utilidades();
$this->breadcrumbs=array(
	'Back to Licenses'=>array('licenses/admin'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#exams-license-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
$modLicense = Licenses::model()->findByPk($license);
?>

<h1>Manage Exams Licenses for <?php echo $modLicense->nombre;?></h1>

<a class="fancy" href="index.php?r=exams/add&license=<?php echo $license;?>">Add Exams to this License</a>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'exams-license-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
            array(
                'header'=>'Exam',
                'value'=>'$data->idExamen->nombre'
            ),
            array(
                'header'=>'Section',
                'value'=>'$data->idExamen->idSeccion->nombre'
            ),
            array(
                'name'=>'orden',
                'filter'=>'',
            ),    
            array(
                'header'=>'Move Question',
                'filter'=>'',
                'type'=>'raw',
                'value'=>'CHtml::link("Up&nbsp;","javascript:moveUp($data->id_examenes_licencia);"). CHtml::link("Down&nbsp;","javascript:moveDown($data->id_examenes_licencia);")'
            ),    
		array(
			'class'=>'CButtonColumn',
                    'template'=>'{delete}',
                    'buttons'=>array(
                        'delete'=>array(
                            'label'=>'Delete',
                            'url'=> 'Yii::app()->createUrl("examsLicense/delete",array("id"=>$data->id_examenes_licencia,"license"=>$data->id_licencia))',
                            'imageUrl'=>NULL
                        )
                    )
		),
	),
)); 

$config = array( 
    'scrolling' => 'yes', 
    'titleShow' => false,
    'overlayColor' => '#000',
    'padding' => '10',
    'showCloseButton' => true,
       'width'=>'500px',
        // change this as you need
    );
    $this->widget('application.extensions.fancybox.EFancyBox', array('target'=>'.fancy', 'config'=>$config));
?>
<script>
    function moveUp(id){
        $.ajax({
            url: 'index.php?r=ExamsLicense/moveUp/id/'+id,
            async: true,
            success: function(result){
                $.fn.yiiGridView.update('exams-license-grid');
            }
        });
    }
    function moveDown(id){
        $.ajax({
            url: 'index.php?r=ExamsLicense/moveDown/id/'+id,
            async: true,
            success: function(result){
                $.fn.yiiGridView.update('exams-license-grid');
            }
        });
    }
    </script>
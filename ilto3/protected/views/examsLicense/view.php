<?php
/* @var $this ExamsLicenseController */
/* @var $model ExamsLicense */

$this->breadcrumbs=array(
	'Exams Licenses'=>array('index'),
	$model->id_examenes_licencia,
);

$this->menu=array(
	array('label'=>'List ExamsLicense', 'url'=>array('index')),
	array('label'=>'Create ExamsLicense', 'url'=>array('create')),
	array('label'=>'Update ExamsLicense', 'url'=>array('update', 'id'=>$model->id_examenes_licencia)),
	array('label'=>'Delete ExamsLicense', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_examenes_licencia),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ExamsLicense', 'url'=>array('admin')),
);
?>

<h1>View ExamsLicense #<?php echo $model->id_examenes_licencia; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_examenes_licencia',
		'id_licencia',
		'id_examen',
		'es_placement',
	),
)); ?>

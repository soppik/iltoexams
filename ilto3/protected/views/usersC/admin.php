<?php
/* @var $this UsersCController */
/* @var $model UsersC */

$this->breadcrumbs=array(
	'Users Cs'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create Client Users', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#users-c-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Client Users </h1>
<h3 style="color:#EE5959;"><?php echo Yii::app()->user->getFlash('error'); ?></h3>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-c-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
            array(
                'name'=>'id_cliente',
                'value'=>'$data->idCliente->nombre_rsocial',
               'filter'=>CHtml::listData(Clients::model()->findAll(),"id_cliente","nombre_rsocial"),
                ),
            array(
                'name'=>'id_perfil',
                'value'=> '$data->idPerfil->nombre',
               'filter'=>CHtml::listData(Profiles::model()->findAll(),"id_perfil","nombre"),
            ),
		'id_usuario_c',
		'nombres',
		'apellidos',
		'email',
                array(
                    'name'=>'estado',
                    'value'=>'($data->estado=="A" ? "Enabled" : "Disabled")',
                    'filter'=>array("A"=>"Enable","I"=>"Disable"),
                    ),
		/*
		'id_perfil',
		'tipo_id',
		'numero_id',
		'ruta_foto',
		'estado',
		*/
		array(
			'class'=>'CButtonColumn',
                    //'template'=>'{update}{delete}{registration}{licenses}',
                    'template'=>'{update}{registration}{delete}{licenses}',
                    'buttons'=>array(
                        'update'=>array(
                            'label'=>'Edit&nbsp;',
                            'imageUrl'=>NULL,
                            'url'=> 'Yii::app()->createUrl("UsersC/update",array("id"=>$data->id_usuario_c))',
                        ),
                        'registration'=>array(
                            'label'=>'Email&nbsp;',
                            'imageUrl'=>NULL,
                            'url'=> 'Yii::app()->createUrl("UsersC/email",array("id"=>$data->id_usuario_c))',
                        ),
                        'delete'=>array(
                            'label'=>'Delete&nbsp;',
                            'imageUrl'=>NULL,
                            'url'=> 'Yii::app()->createUrl("UsersC/delete",array("id"=>$data->id_usuario_c,"client"=>$data->id_cliente))',
                        ),
                        'licenses'=>array(
                            //'url'=> 'Yii::app()->createUrl("Clients/LicensesUser/admin2",array("id"=>$data->id_usuario_c))',
                            'url'=> 'Yii::app()->createUrl("UsersC/licences",array("id"=>$data->id_usuario_c))',
                            'visible'=>'(Yii::app()->user->getState("id_perfil") <= 4  && $data->id_perfil == 6) ? true:false',
                            'imageUrl'=>NULL,
                            'label'=>'Licenses'
                        ),                    )
		),
	),
)); ?>

<?php
/* @var $this UsersCController */
/* @var $model UsersC */

$this->breadcrumbs=array(
	'Client Users'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Client Users', 'url'=>array('admin')),
);
$baseUrl = Yii::app()->baseUrl; 
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/js/picEditMaster/picedit.min.js');
$cs->registerCssFile($baseUrl.'/js/picEditMaster/picedit.min.css');
?>

<h1>Create Client Users</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
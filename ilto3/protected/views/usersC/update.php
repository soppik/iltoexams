<?php
/* @var $this UsersCController */
/* @var $model UsersC */

$this->breadcrumbs=array(
	'Client Users'=>array('index'),
	$model->id_usuario_c=>array('view','id'=>$model->id_usuario_c),
	'Update',
);

$this->menu=array(
	array('label'=>'Create Client Users', 'url'=>array('create')),
	array('label'=>'Manage Client Users', 'url'=>array('admin')),
);
$baseUrl = Yii::app()->baseUrl; 
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/js/picEditMaster/picedit.min.js');
$cs->registerCssFile($baseUrl.'/js/picEditMaster/picedit.min.css');
?>

<h1>Update UsersC <?php echo $model->id_usuario_c; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
/* @var $this UsersCController */
/* @var $model UsersC */

$this->breadcrumbs=array(
	'Users Cs'=>array('index'),
	$model->id_usuario_c,
);

$this->menu=array(
	array('label'=>'List UsersC', 'url'=>array('index')),
	array('label'=>'Create UsersC', 'url'=>array('create')),
	array('label'=>'Update UsersC', 'url'=>array('update', 'id'=>$model->id_usuario_c)),
	array('label'=>'Delete UsersC', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_usuario_c),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage UsersC', 'url'=>array('admin')),
);
?>

<h1>View User #<?php echo $model->id_usuario_c; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_usuario_c',
		'nombres',
		'apellidos',
		'email',
		'id_cliente',
		'ultimo_acceso',
		'id_perfil',
		'tipo_id',
		'numero_id',
		'ruta_foto',
		'estado',
	),
)); ?>

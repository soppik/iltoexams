<?php
/* @var $this UsersCController */
/* @var $model UsersC */

$config = Yii::app()->getComponents(false);
$connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
// establish connection. You may try...catch possible exceptions
$connection->active=true;

$model=  UsersC::model()->findByAttributes(array('id_usuario_c'=>$_GET['id']));

// Get Licence
$SQL = "SELECT * FROM `licenses_user` `t` WHERE `t`.`id_usuario` LIKE '".$model->id_usuario_c."'";
$list_u= $connection->createCommand($SQL)->queryAll();


$this->breadcrumbs=array(
	'Users Cs'=>array('index'),
	'licences',
);

$this->menu=array(
	array('label'=>'Manage Client Users', 'url'=>array('admin')),
);


?>

<h1>Manage Licences Taker</h1>
<h3 style="color:#EE5959;"><?php echo Yii::app()->user->getFlash('error'); ?></h3>


    <?php 
    foreach($list_u as $rooms){
        ?> 
        <table class="table table-striped table-hover table-bordered" id="editabledatatable">
    <thead>
        <tr role="row">
            <th>
                CLIENT LICENSE
            </th>
            <th>
                ALLOCATION DATE (YY-MM-DD)
            </th>
            <th>
                STATUS
            </th>
            <th>
            </th>
        </tr>
    </thead>
    <tbody>
        
        <?php 
        //echo '<pre>';
        $SQL = "SELECT * FROM `groups_users_clients` WHERE `id` = ".(int)$rooms['id_grupo_cliente']." ORDER BY `id` DESC ";
        $userGroup= $connection->createCommand($SQL)->queryAll();
        //var_dump($userGroup);
        $SQL =  "SELECT * FROM `licenses_client`, licenses WHERE `id_licencias_cliente` = ".$rooms['id_licencia_cliente']." and licenses.id_licencia=licenses_client.id_licencia ";
        $LicenceClient= $connection->createCommand($SQL)->queryAll();
        //var_dump($LicenceClient);
        $SQL = "SELECT * FROM licenses_user WHERE id_licencias_usuario = ".(int)$rooms['id_licencias_usuario']." ";
        $LicenceUser = $connection->createCommand($SQL)->queryAll();
        //var_dump($SQL);
        //echo '</pre>';
        $date = date($LicenceUser[0]['fecha_asignacion']);
        
        ?>
        <tr>
            <td>
               <h4> <?php echo $LicenceClient[0]['nombre'];?></h4>
                    
            </td>
            <td><?php echo substr($date, 0,10);?></td>
            <td class="center "><?php echo ($rooms['estado']=='F')?'COMPLETED':'PENDING' ?></td>
            <td>
                <?php 
                $SQL = "SELECT COUNT(DISTINCT `t`.`id_licencias_usuario`) as total FROM `licenses_user` `t`  
                LEFT OUTER JOIN `users_c` `idUsuario` ON (`t`.`id_usuario`=`idUsuario`.`id_usuario_c`) AND (id_cliente='".(int)Yii::app()->user->getState('cliente')."' and id_perfil >= 2) 
                LEFT OUTER JOIN `licenses_client` `idLicenciaCliente` ON (`t`.`id_licencia_cliente`=`idLicenciaCliente`.`id_licencias_cliente`) 
                WHERE (t.id_usuario LIKE '%".(int)$rooms['id_usuario_c']."%')";
                
                $LicencesByUser = $connection->createCommand($SQL)->queryAll();
                if($rooms['estado']=='F'){
                    ?>
                    <a title="View_Certificate" href="/ilto3/index.php?r=UsersC/certificate&amp;id=<?php echo $rooms['id_licencias_usuario']?>"class="btn btn-warning btn-xs edit"><i class="fa fa-check"></i>View_Certificate</a>
                    <a title="Email_Certificate" href="/ilto3/index.php?r=UsersC/emailcertificate&amp;id=<?php echo $rooms['id_licencias_usuario']?>" class="btn btn-info btn-xs cd-popup-trigger"><i class="fa fa-edit"></i>Send certificate to the test taker</a>
                    <?php
                }else {
                    ?>
                    <a href="/ilto3/index.php?r=Clients/ExamsUser/admin&id=<?php echo $rooms['id_licencias_usuario']?>" class="btn btn-success btn-xs edit"><i class="fa fa-check"></i>Results</a>
                    <?php
                }
                ?>
            </td>
        </tr>
    </tbody>
</table>
<table class="table table-striped table-hover table-bordered" id="editabledatatable">
                        <thead>
                            <tr role="row">
                                <th>
                                    Exam
                                </th>
                                <th>
                                    Score
                                </th>
                                <th>
                                    CEFR Level
                                </th>
                                <th>
                                    Status
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                    <?php
                    //Get exams by Licence
                    $SQL = "SELECT exams.nombre, exams_user.calificacion, exams_user.nivel, exams_user.estado FROM `exams_user` INNER JOIN `exams` ON exams_user.id_examen=exams.id_examen WHERE exams_user.id_licencia_usuario LIKE '".$rooms['id_licencias_usuario']."'";
                    $list_e= $connection->createCommand($SQL)->queryAll();
                    foreach($list_e as $exams){
                        echo '<tr><td>'.$exams['nombre'].'</td><td>'.$exams['calificacion'].'</td><td>'.$exams['nivel'].'</td></td><td>'.$exams['estado'].'</td></tr>';
                    }
                ?>
                    </tbody>
                </table>
    <?php
    }
?>


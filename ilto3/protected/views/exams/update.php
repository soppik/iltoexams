<?php
/* @var $this ExamsController */
/* @var $model Exams */

$this->breadcrumbs=array(
	'Exams'=>array('index'),
	$model->id_examen=>array('view','id'=>$model->id_examen),
	'Update',
);

$this->menu=array(
	array('label'=>'Create Exams', 'url'=>array('create')),
	array('label'=>'Manage Exams', 'url'=>array('admin')),
);
?>

<h1>Update Exams <?php echo $model->id_examen; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
/* @var $this ExamsController */
/* @var $model Exams */

$this->breadcrumbs=array(
	'Exams'=>array('index'),
	$model->id_examen,
);

$this->menu=array(
	array('label'=>'List Exams', 'url'=>array('index')),
	array('label'=>'Create Exams', 'url'=>array('create')),
	array('label'=>'Update Exams', 'url'=>array('update', 'id'=>$model->id_examen)),
	array('label'=>'Delete Exams', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_examen),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Exams', 'url'=>array('admin')),
);
?>

<h1>View Exams #<?php echo $model->id_examen; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_examen',
		'nombre',
		'nivel',
		'preguntas_aleatorias',
		'id_seccion',
		'estado',
		'porcentaje_aprobado',
	),
)); ?>

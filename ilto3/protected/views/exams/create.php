<?php
/* @var $this ExamsController */
/* @var $model Exams */

$this->breadcrumbs=array(
	'Exams'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Exams', 'url'=>array('admin')),
);
?>

<h1>Create Exams</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
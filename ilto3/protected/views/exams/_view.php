<?php
/* @var $this ExamsController */
/* @var $data Exams */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_examen')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_examen), array('view', 'id'=>$data->id_examen)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nivel')); ?>:</b>
	<?php echo CHtml::encode($data->nivel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('preguntas_aleatorias')); ?>:</b>
	<?php echo CHtml::encode($data->preguntas_aleatorias); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_seccion')); ?>:</b>
	<?php echo CHtml::encode($data->id_seccion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado')); ?>:</b>
	<?php echo CHtml::encode($data->estado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('porcentaje_aprobado')); ?>:</b>
	<?php echo CHtml::encode($data->porcentaje_aprobado); ?>
	<br />


</div>
<?php
/* @var $this PerfilesController */
/* @var $model Perfiles */


$this->menu=array(
	array('label'=>'Back to Profile Administrator', 'url'=>array('admin')),
);
?>

<h1>Create user's Profile</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
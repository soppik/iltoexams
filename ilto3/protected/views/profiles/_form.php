<?php
/* @var $this PerfilesController */
/* @var $model Perfiles */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'profiles-form',
	'enableAjaxValidation'=>false, 
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>
    
    <?php
/* @var $this ProveedoresController */
/* @var $model Proveedores */
	
	
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/checkboxtree-last/library/jquery-1.4.4.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/checkboxtree-last/library/jquery-ui-1.8.12.custom/js/jquery-ui-1.8.12.custom.min.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/checkboxtree-last/jquery.checkboxtree.js');
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/checkboxtree-last/library/jquery-ui-1.8.12.custom/css/smoothness/jquery-ui-1.8.12.custom.css');
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/checkboxtree-last/jquery.checkboxtree.css');

    $criteria = new CDbCriteria();
	$criteria->addCondition("nodo=0");
	$modelofuera= Menu::model()->findAll($criteria);
	$configact=explode(',',$model->configuracion);
	
	
    ?>

		<?php echo $form->labelEx($model,'configuracion'); ?>
   <ul id="tree1">
        <?php
        foreach($modelofuera as $nodo){
			if($nodo->url=='#'){
				$criteria = new CDbCriteria();
				$criteria->addCondition("nodo=".$nodo->opcion);
				$modelodentro = Menu::model()->findAll($criteria);
				$opcionactual=$nodo->nodo.'-'.$nodo->opcion;
				$checked=""; 
				foreach($configact as $activos){
					if($activos==$opcionactual){
					$checked='checked="checked"';}
				}
                                echo "<li><input name='opcionesperfil[]' type='checkbox' value='".$opcionactual."' ".$checked."><label>".$nodo->nombre."</label>";
                                echo "<ul>";
                                foreach($modelodentro as $opcion){
                                    if($opcion->url=='#'){
                                        $criteria = new CDbCriteria();
                                        $criteria->addCondition("nodo=".$opcion->opcion);
                                        $modelotres = Menu::model()->findAll($criteria);
                                        $opcionactual3=$opcion->nodo.'-'.$opcion->opcion;
                                        $checked2="";
                                        foreach($configact as $activos){
                                                if($activos==$opcionactual3){
                                                $checked2='checked="checked"';}
                                        }
                                        echo "<li><input name='opcionesperfil[]' type='checkbox' value='".$opcionactual3."' ".$checked2."><label>".$opcion->nombre."</label>";
                                        echo "<ul>";
                                        foreach($modelotres as $opcion3){
                                            $opcionactual3=$opcion3->nodo.'-'.$opcion3->opcion;
                                            $checked="";
                                            foreach($configact as $activos){
                                                    if($activos==$opcionactual3){
                                                    $checked='checked="checked"';}
                                            }
                                            echo "<li><input name='opcionesperfil[]' value='".$opcionactual3."' type='checkbox' ".$checked."><label>".$opcion3->nombre."</label>";
                                       }
                                       echo "</ul>";
                                    } else {
                                        $opcionactual=$opcion->nodo.'-'.$opcion->opcion;
                                        $checked="";
                                                foreach($configact as $activos){
                                                        if($activos==$opcionactual){
                                                        $checked='checked="checked"';}
                                                }
                                                        echo "<li><input name='opcionesperfil[]' value='".$opcionactual."' type='checkbox' ".$checked."><label>".$opcion->nombre."</label>";
                                    }
                                }
                                echo "</ul>";
                        }else{
				$opcionactual=$nodo->nodo.'-'.$nodo->opcion;
				$checked="";
				foreach($configact as $activos){
					if($activos==$opcionactual){
					$checked='checked="checked"';}
				}
				echo "<li><input name='opcionesperfil[]' type='checkbox' value='".$opcionactual."' ".$checked."><label>".$nodo->nombre."</label>";
				}
        }?>
    </ul>

<script type="text/javascript">
    $('#tree1').checkboxTree({initializeUnchecked:'collapsed'});
</script>
  	
    <div class="row">
		<?php echo $form->labelEx($model,'es_cliente'); ?>
           <?php 
        	$dataestado=array('1'=>'Yes','0'=>'No');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'Profiles[es_cliente]',
            'id'=> 'combo_escte',
            'data'=>$dataestado,
                'value'=>$model->es_cliente,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'220px',
            ),
          ));?>
		<?php echo $form->error($model,'es_cliente'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'estado'); ?>
           <?php 
        	$dataestado=array('A'=>'Enabled','I'=>'Disabled');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'Profiles[estado]',
            'id'=> 'combo1',
            'data'=>$dataestado,
                'value'=>$model->estado,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'220px !important;',
            ),
          ));?>
		<?php echo $form->error($model,'estado'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget();
	$config = array( 
    'scrolling' => 'yes', 
    'titleShow' => false,
    'overlayColor' => '#000',
    'padding' => '10',
    'showCloseButton' => true,
       'width'=>'500px',
        // change this as you need
    );
    $this->widget('application.extensions.fancybox.EFancyBox', array('target'=>'#fancybox-trigger', 'config'=>$config));
 ?>

</div><!-- form -->
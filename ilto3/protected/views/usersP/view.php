<?php
/* @var $this UsersPController */
/* @var $model UsersP */

$this->breadcrumbs=array(
	'Users Ps'=>array('index'),
	$model->id_usuario_p,
);

$this->menu=array(
	array('label'=>'List UsersP', 'url'=>array('index')),
	array('label'=>'Create UsersP', 'url'=>array('create')),
	array('label'=>'Update UsersP', 'url'=>array('update', 'id'=>$model->id_usuario_p)),
	array('label'=>'Delete UsersP', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_usuario_p),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage UsersP', 'url'=>array('admin')),
);
?>

<h1>View UsersP #<?php echo $model->id_usuario_p; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_usuario_p',
		'nombre',
		'email',
		'id_perfil',
		'ultimo_acceso',
		'clave',
		'estado',
	),
)); ?>

<?php
/* @var $this UsersPController */
/* @var $model UsersP */

$this->breadcrumbs=array(
	'Users Ps'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create UsersP', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#users-p-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage ILTO Users</h1>



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-p-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_usuario_p',
		'nombre',
		'email',
		'id_perfil',
		'ultimo_acceso',
                array(
                    'name'=>'estado',
                    'value'=>'($data->estado=="A" ? "Enabled" : "Disabled")',
                    'filter'=>array("A"=>"Enable","I"=>"Disable"),
                    ),
		array(
			'class'=>'CButtonColumn',
                    'template'=>'{update}{delete}',
                    'buttons'=>array(
                        'delete'=>array(
                            'label'=>'Delete&nbsp;',
                            'imageUrl'=>NULL
                        ),
                        'update'=>array(
                            'label'=>'Edit&nbsp;',
                            'imageUrl'=>NULL
                        )
                    )
		),
	),
)); ?>

<?php
/* @var $this UsersPController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Users Ps',
);

$this->menu=array(
	array('label'=>'Create UsersP', 'url'=>array('create')),
	array('label'=>'Manage UsersP', 'url'=>array('admin')),
);
?>

<h1>Users Ps</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

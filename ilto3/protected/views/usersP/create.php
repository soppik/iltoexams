<?php
/* @var $this UsersPController */
/* @var $model UsersP */

$this->breadcrumbs=array(
	'Users Ps'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage UsersP', 'url'=>array('admin')),
);
?>

<h1>Create ILTO Users</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
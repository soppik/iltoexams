<?php
/* @var $this UsersPController */
/* @var $model UsersP */

$this->breadcrumbs=array(
	'Users Ps'=>array('index'),
	$model->id_usuario_p=>array('view','id'=>$model->id_usuario_p),
	'Update',
);

$this->menu=array(
	array('label'=>'Create UsersP', 'url'=>array('create')),
	array('label'=>'Manage UsersP', 'url'=>array('admin')),
);
?>

<h1>Update ILTO Users <?php echo $model->nombre; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
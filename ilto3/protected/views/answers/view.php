<?php
/* @var $this AnswersController */
/* @var $model Answers */

$this->breadcrumbs=array(
	'Answers'=>array('index'),
	$model->id_respuesta,
);

$this->menu=array(
	array('label'=>'List Answers', 'url'=>array('index')),
	array('label'=>'Create Answers', 'url'=>array('create')),
	array('label'=>'Update Answers', 'url'=>array('update', 'id'=>$model->id_respuesta)),
	array('label'=>'Delete Answers', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_respuesta),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Answers', 'url'=>array('admin')),
);
?>

<h1>View Answers #<?php echo $model->id_respuesta; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_respuesta',
		'id_pregunta',
		'ruta_imagen',
		'ruta_video',
		'ruta_audio',
		'texto',
		'es_verdadera',
	),
)); ?>

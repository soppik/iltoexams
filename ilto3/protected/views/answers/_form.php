<?php
/* @var $this AnswersController */
/* @var $model Answers */
/* @var $form CActiveForm */
$modUtil= new Utilidades();

?>

<div class="form">

<?php 
if($model->isNewRecord){
    $model->estado="A";
}
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'answers-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

       	<div class="row">
                <?php echo $form->labelEx($model,'texto'); ?>
		<?php echo $form->textArea($model,'texto',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'texto'); ?>
	</div>

	<?php echo $form->errorSummary($model); ?>
	<div class="row">
		<?php echo $form->labelEx($model,'ruta_imagen'); ?>
		<?php echo $form->fileField($model,'ruta_imagen'); ?>
		<?php echo $form->error($model,'ruta_imagen'); ?>
            <?php if(!$model->isNewRecord){
                if(strlen($model->ruta_imagen)>0){
                    echo "<img src='images/answers/tn2_$model->ruta_imagen' style='width:200px;'>";
                    echo "<input type='hidden' name='ruta_imagen_actual' value='$model->ruta_imagen'>";
                    echo "&nbsp;&nbsp;<input type='checkbox' name='delete_imagen' value='1'>&nbsp;Delete Image on Save";
                }
            } ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ruta_video'); ?>
		<?php echo $form->textField($model,'ruta_video',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'ruta_video'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ruta_audio'); ?>
		<?php echo $form->fileField($model,'ruta_audio'); ?>
		<?php echo $form->error($model,'ruta_audio'); ?>
            <?php if(!$model->isNewRecord){
                if(strlen($model->ruta_audio)>0){
                    echo "<audio controls><source src='$model->ruta_audio' type='audio/wav'></audio>";
                    echo "<input type='hidden' name='ruta_audio_actual' value='$model->ruta_audio'>";
                    echo "&nbsp;&nbsp;<input type='checkbox' name='delete_audio' value='1'>&nbsp;Delete Audio on Save";
                }
            } ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'es_verdadera'); ?>
		<?php echo $form->checkBox($model,'es_verdadera'); ?>
		<?php echo $form->error($model,'es_verdadera'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'estado'); ?>
		<?php
            $dataCombo=array('A'=>'Enabled','I'=>'Disabled');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'Answers[estado]',
            'id'=> 'combo_estado',
            'data'=>$dataCombo,
                'value'=>$model->estado,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'120px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'estado'); ?>
	</div>

	<div class="row buttons">
		<?php 
                //$model->id_pregunta = $question;
                echo $form->hiddenField($model,'id_pregunta',array('value'=>$question)); 
                echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
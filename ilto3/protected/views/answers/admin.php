<?php
/* @var $this AnswersController */
/* @var $model Answers */

$this->breadcrumbs=array(
	'Answers'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Go Back to Questions', 'url'=>array('questions/admin')),
	array('label'=>'Create Answers', 'url'=>array('create','question'=>$question)),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#answers-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Answers for....ID:<?php echo $question;?></h1>
<div>
    <?php
        echo Questions::model()->findByPk($question)->texto;
    ?>
</div>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'answers-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
            'id_respuesta',
            'id_pregunta',
            array(
                'name'=>'texto',
                'type'=>'raw'
            ),
            array(
                'name'=>'ruta_imagen',
                'filter'=>'',
                'value'=>'(!empty($data->ruta_imagen))?CHtml::image(Yii::app()->baseUrl."/images/answers/tn1_".$data->ruta_imagen,"",array("class"=>"imagen", "href"=>Yii::app()->baseUrl."/images/answers/tn3_$data->ruta_imagen","style"=>"width:150px !important; max-width:100% !important;")):"no image"',
                'type'=>'raw'
                ),
            array(
                'name'=>'ruta_audio',
                'filter'=>'',
                'type'=>'raw',
                'value'=>'(!empty($data->ruta_audio))? "<audio controls><source src=\'$data->ruta_audio\' type=\'audio/wav\'></audio>" : "No Audio"',
            ),
                array(
                    'name'=>'estado',
                    'value'=>'($data->estado=="A" ? "Enabled" : "Disabled")',
                    'filter'=>array("A"=>"Enable","I"=>"Disable"),
                    ),
                array(
                    'name'=>'es_verdadera',
                    'value'=>'(($data->es_verdadera))?CHtml::image(Yii::app()->baseUrl."/images/iconos/ok.png","",array("style"=>"max-width:20px;")):CHtml::image(Yii::app()->baseUrl."/images/iconos/delete.png","",array("style"=>"max-width:20px;"))',                    
                    'filter'=>'',
                    'type'=>'raw',
                    ),
		array(
			'class'=>'CButtonColumn',
                    'template'=>'{update}{delete}',
                    'buttons'=>array(
                        'update'=>array(
                            'label'=>'Edit&nbsp;',
                            'url'=> 'Yii::app()->createUrl("answers/update",array("id"=>$data->id_respuesta,"question"=>$data->id_pregunta))',
                            'imageUrl' => NULL,
                    ),
                        'delete'=>array(
                            'label'=>'Delete',
                            'imageUrl'=>NULL, 
                        ))
		),
	),
)); ?>

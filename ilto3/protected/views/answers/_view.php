<?php
/* @var $this AnswersController */
/* @var $data Answers */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_respuesta')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_respuesta), array('view', 'id'=>$data->id_respuesta)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_pregunta')); ?>:</b>
	<?php echo CHtml::encode($data->id_pregunta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ruta_imagen')); ?>:</b>
	<?php echo CHtml::encode($data->ruta_imagen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ruta_video')); ?>:</b>
	<?php echo CHtml::encode($data->ruta_video); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ruta_audio')); ?>:</b>
	<?php echo CHtml::encode($data->ruta_audio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('texto')); ?>:</b>
	<?php echo CHtml::encode($data->texto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('es_verdadera')); ?>:</b>
	<?php echo CHtml::encode($data->es_verdadera); ?>
	<br />


</div>
<?php
/* @var $this LicensesController */
/* @var $model Licenses */

$this->breadcrumbs=array(
	'Licenses'=>array('index'),
	$model->id_licencia=>array('view','id'=>$model->id_licencia),
	'Update',
);

$this->menu=array(
	array('label'=>'Create Licenses', 'url'=>array('create')),
	array('label'=>'Manage Licenses', 'url'=>array('admin')),
);
?>

<h1>Update Licenses <?php echo $model->id_licencia; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
/* @var $this LicensesController */
/* @var $data Licenses */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_licencia')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_licencia), array('view', 'id'=>$data->id_licencia)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado')); ?>:</b>
	<?php echo CHtml::encode($data->estado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('genera_certificado')); ?>:</b>
	<?php echo CHtml::encode($data->genera_certificado); ?>
	<br />


</div>
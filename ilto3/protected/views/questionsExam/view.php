<?php
/* @var $this QuestionsExamController */
/* @var $model QuestionsExam */

$this->breadcrumbs=array(
	'Questions Exams'=>array('index'),
	$model->id_preguntas_examen,
);

$this->menu=array(
	array('label'=>'List QuestionsExam', 'url'=>array('index')),
	array('label'=>'Create QuestionsExam', 'url'=>array('create')),
	array('label'=>'Update QuestionsExam', 'url'=>array('update', 'id'=>$model->id_preguntas_examen)),
	array('label'=>'Delete QuestionsExam', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_preguntas_examen),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage QuestionsExam', 'url'=>array('admin')),
);
?>

<h1>View QuestionsExam #<?php echo $model->id_preguntas_examen; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_preguntas_examen',
		'id_examen',
		'id_pregunta',
	),
)); ?>

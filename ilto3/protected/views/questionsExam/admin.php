<?php
/* @var $this QuestionsExamController */
/* @var $model QuestionsExam */
$modUtil = new Utilidades();
$this->breadcrumbs=array(
	'Back to Exams'=>array('exams/admin'),
	'Manage',
);

$this->layout="column1";
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#questions-exam-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Questions for Exam <?php echo Exams::model()->findByPk($exam)->nombre;?></h1>

<a class="fancy" href="index.php?r=questions/add&exam=<?php echo $exam;?>"><img src="images/iconos/add.png"></a>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'questions-exam-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
            array(
                'name'=>'level',
                'value'=>'$data->idPregunta->nivel',
                'filter'=>$modUtil->arregloNiveles(),
                ),
            array(
		'header'=>'Show As',
                'filter'=>$modUtil->arregloPresentaciones(),
                'value'=>'$data->idPregunta->nombrePresenta',
                ),
            array(
                'header'=>'Question',
                'value'=>'$data->idPregunta->texto',
                'type'=>'raw'
            ),
            array(
                'header'=>'Image',
                'value'=>'(!empty($data->idPregunta->ruta_imagen))?CHtml::image(Yii::app()->baseUrl."/images/questions/tn1_".$data->idPregunta->ruta_imagen,"",array("class"=>"imagen", "href"=>Yii::app()->baseUrl."/images/questions/tn3_".$data->idPregunta->ruta_imagen,"style"=>"width:150px !important; max-width:100% !important;")):"no image"',
                'type'=>'raw'
                ),
 /*           array(
                'header'=>'Audio',
                'type'=>'raw',
                'value'=>'(!empty($data->idPregunta->ruta_audio))? "<audio controls><source src=\''.Yii::app()->baseUrl.'/images/questions/$data->idPregunta->ruta_audio\' type=\'audio/wav\'></audio>" : "No Audio"',
            ),*/
		array( 
			'class'=>'CButtonColumn',
                    'template'=>'{delete}',
                    'buttons'=>array(
                        'delete'=>array(
                            'url'=> 'Yii::app()->createUrl("questionsExam/delete",array("id"=>$data->id_preguntas_examen,"exam"=>$data->id_examen))',
                            'label'=>'Delete',
                            'imageUrl'=>NULL,
                        )
                    )
		),
	),
)); 
$config = array( 
    'scrolling' => 'yes', 
    'titleShow' => false,
    'overlayColor' => '#000',
    'padding' => '10',
    'showCloseButton' => true,
       'width'=>'500px',
        // change this as you need
    );
    $this->widget('application.extensions.fancybox.EFancyBox', array('target'=>'.fancy', 'config'=>$config));

?>

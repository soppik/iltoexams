<?php
/* @var $this QuestionsExamController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Questions Exams',
);

$this->menu=array(
	array('label'=>'Create QuestionsExam', 'url'=>array('create')),
	array('label'=>'Manage QuestionsExam', 'url'=>array('admin')),
);
?>

<h1>Questions Exams</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

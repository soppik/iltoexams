<?php
/* @var $this QuestionsExamController */
/* @var $data QuestionsExam */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_preguntas_examen')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_preguntas_examen), array('view', 'id'=>$data->id_preguntas_examen)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_examen')); ?>:</b>
	<?php echo CHtml::encode($data->id_examen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_pregunta')); ?>:</b>
	<?php echo CHtml::encode($data->id_pregunta); ?>
	<br />


</div>
<?php
/* @var $this LicensesClientController */
/* @var $model LicensesClient */
/* @var $form CActiveForm */
$config = Yii::app()->getComponents(false);
$connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
//$connection=new CDbConnection('mysql:host=localhost;dbname=iltoexam_newversion','iltoexam_newvers','Ilto.2015');
//$connection=new CDbConnection($config["db"]["connectionString"],$config["db"]["username"],$config["db"]["password"]);
$connection->active=true;

// GET ORDER GENERAL INFO
$SQL = "SELECT * FROM `orders` WHERE id_order ='".$_GET["id"]."'";
$order=  Yii::app()->db->createCommand($SQL)->queryAll();

// Get ORDER DETAIL INFO
$SQL = "SELECT * FROM `order_licenses` WHERE id_order ='".$order[0]["id_order"]."' ORDER BY id_licencia ASC";
$order_list=  Yii::app()->db->createCommand($SQL)->queryAll();

//GET CLIENT INFO
$SQL = "SELECT * FROM `clients` WHERE id_cliente ='".$order[0]["id_client"]."'";
$client=  Yii::app()->db->createCommand($SQL)->queryAll();

$SQL = "SELECT city.name as city, country.name as country FROM `city` INNER JOIN country ON city.CountryCode = country.Code WHERE city.ID ='".$client[0]["id_ciudad"]."'";
$country=  Yii::app()->db->createCommand($SQL)->queryAll();

$SQL = "SELECT * FROM `users_c` WHERE id_cliente ='".$client[0]["nit_id_legal"]."' AND id_perfil='2'";
$toaa=  Yii::app()->db->createCommand($SQL)->queryAll();

if(is_null($client[0]["id_distributor"])){
                $distributor = '<span><b>Agent:</b>ILTOEXAMS
                                <input type="hidden" value="NULL" class="form-control" name="Order[id_distributor]" id="order[id_distributor]"/>'; 
            }else{
                $distributor = '<span><b>Agent: </b>'.$client[0]["id_distributor"].'<br/>
                <input type="hidden" value="'.$client[0]["id_distributor"].'" class="form-control" name="Order[id_distributor]" id="order[id_distributor]"/>'; 
            }
            
	switch ($order[0]['status']) {
		case 'P':
		    $status= "pending";
		break;
		case 'R':
		    $status= "approved";
		break;
		case 'A':
		    $status= "confirmed";
		break;
		case 'C':
		    $status= "canceled";
		break;
		default:
		    $status= "pending";
	}
	
	switch ($order[0]['terms']) {
		case '0':
		    $terms= "Due on Recipt";
		break;
		case '1':
		    $terms= "30 Days";
		break;
		case '2':
		    $terms= "60 Days";
		break;
		case '3':
		    $terms= "90 Days";
		break;
		default:
		    $terms= "Due on Recipt";
	}


?>

<div class="form"> UPDATE ORDER <?php echo $_GET["id"]; ?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'UpdateOrder',
	'action'=>'/ilto3/index.php?r=Orders/updateOrder',
	
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
<style type="text/css">
    .amount {
        width:100px;
        text-align:center;
        font-weight:bold;
    }
    .prices {
        width:50px;
        text-align:center;
        font-weight:bold;
    }
</style>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
         <div class="widget">
            <div class="widget-body">
                <div class="row" style="padding:15px;"><!--ORDER Header-->
                    <table>
                        <tr>
                            <td style="width:33%;">
                                    <img src="/ilto3/images/logo_ilto_original.png" width="200" style="">
                            </td>
                            <td style="width:33%;text-align:center;font-size:12px;">International Language Testing Organization Inc.
                                        8300 NW 53rd Street Suite 350 Doral FL 33166
                                        305-853-8088
                                        www.iltoexams.com
                            </td>
                            <td style="width:33%;">
                                <div class="col-lg-4 col-sm-4 col-xs-4">
                                    <table>
                                        <tr>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                                Terms
                                            </td>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                                Order No.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #0f5418; text-align:center;font-size:12px;">
                                                <select id="combo_pago" name="Order[due]" class="form-control">
                        					        <option value="0">Due on Recipt</option>
                        							<option value="1">30 Days</option>
                        							<option value="2">60 Days</option>
                        							<option value="3">90 Days</option>
                        						</select>
                        						<script>
                                            	 jQuery(document).ready(function(){
                                            	     var terms= "<?php echo $terms; ?>";
                                            	     $("select option").filter(function() {
                                                    //may want to use $.trim in here
                                                    return $(this).text() == terms; 
                                                    }).prop('selected', true);
                                                 });
                                                </script>
                                            </td>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #0f5418; text-align:center;font-size:12px;">
                                                <?php echo $order[0]["id_order"]; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                                Date
                                            </td>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                                Due Date
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #0f5418; text-align:center;font-size:12px;">
                                                <span id="date_expedition"><?php echo substr($order[0]["date_expedition"],0,10); ?></span>
                                            </td>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #0f5418; text-align:center;font-size:12px;">
                                                <?php 
                                                    if($order[0]["check_ilto"]==1){
                                                        echo substr($order[0]["date_expire"],0,10);
                                                    }
                                                ?>
                                                <input type="hidden" name="Order[date_due]" id="date_due" value="<?php echo $order[0]['date_expedition'];?>" >
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <!-- END ORDER Header-->
                    <div class="row" style="padding:15px;"><!--BILL TO-->
                    <table>
                        <tr>
                            <td style="width:33%;padding:15px;text-align:left;background-color:#dedcdc;;min-height:140px;color:black;font-size:12px;">
                                <span><b>Customer: </b><?php echo utf8_decode($client[0]["nombre_rsocial"]); ?></span><input type="hidden" name="Order[id_client]" class="prices" value="<?php echo $client[0]["nit_id_legal"]; ?>"><br/>
                                <span><b>Name: </b><?php echo utf8_decode($toaa[0]["nombres"]." ".$toaa[0]["apellidos"]); ?><br/>
                                <span><b>Address: </b><?php echo utf8_decode($client[0]["direccion"]).', '.utf8_decode($country[0]["country"]).', '.utf8_decode($country[0]["city"]); ?></span><br/>
                                <span><b>Phone Number: </b><?php echo $client[0]["telefono"]; ?></span><br/>
                                <span><b>Email: </b><?php echo $toaa[0]["email"]; ?><input type="hidden" name="Order[email]" class="prices" value="<?php echo $toaa[0]["email"]; ?>"></span>
                            </td>
                            <td style="width:33%;padding:15px;text-align:center;background-color:#e8e6e6;min-height:140px;color:black;font-size:12px;">
                                <?php echo $distributor; ?>
                                
                                <input type="hidden" name="Order[id_order]" class="id_order_license" value="<?php echo $order[0]["id_order"]; ?>"><br/>
				                     <div id="status_order">
				                        <b>Status: </b>
				                        <?php if($status!="confirmed"){
                                            echo '<select id="combo_estado" name="Order[status]" class="form-control">
                					        <option value=""></option>
                							<option value="C">canceled</option>
                							<option value="P">pending</option>
                							<option value="R">approved</option>
                						</select>';
                                        }else{
                                            echo $status;
                                        }
                                        ?>
                		              	<h5><b>CURRENCY: </b><?php echo $order[0]["currency_type"];?><h5/>
                                        <h5><b></b>$<?php echo $order[0]["currency_value"];?><h5/>
                                        <input type="hidden" name="currency_value" id="currency_value" class="id_order_license" value="<?php echo $order[0]["currency_value"];?>">
                        				</div>
                		    	<script>
                            	 jQuery(document).ready(function(){
                            	     var status = "<?php echo $status; ?>";
                            	     $("select option").filter(function() {
                                    //may want to use $.trim in here
                                    return $(this).text() == status; 
                                    }).prop('selected', true);
                                 });
                                </script>
                            </td>
                            <td style="width:33%;padding:15px;text-align:center;background-color:#dedcdc;;min-height:140px;color:black;font-size:12px;">
                               <br/>If you have any questions about your order, please contact info@iltoexams.com<br/><br/>
                                This is not a Tax Invoice!
                            </td>
                        </tr>
                    </table>
                </div><!-- END BILL TO-->
                     <div class="row" id="body_order" style="min-height:300px; padding:15px;"><!--BODY -->
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <table style="width:100%;">
                            <tr>
                                <td style="width:padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                    Licenses valid thru
                                </td>
                                <td style="width:350px;padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                    Description
                                </td>
                                <td style="width:70px;padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                    Quantity
                                </td>
                                <td style="width:70px;padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                     Unit price
                                </td>
                                <td style="width:100px;padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                    Amount
                                </td>
                            </tr> 
                            <?php
                            $consecutivo = 0;
                            foreach($order_list as $item){
                                $consecutivo++;
                                $SQL = "SELECT nombre FROM `licenses` WHERE `id_licencia` =  '".$item["id_licencia"]."'";
                                $licenseName =  Yii::app()->db->createCommand($SQL)->queryAll();
                              ?>
                                <tr style="border:1px solid black;">
                                    <td style="padding:5px;border:1px solid #0f5418;font-weight:800;text-align:center;font-size:12px;border-right:1px solid black;">
                                       <?php echo date("Y-m-d", strtotime($order[0]["date_expedition"]. ' + 1 year')); ?> 
                                    </td>
                                    <td style="padding:5px;border:1px solid #0f5418;font-weight:800;text-align:left;font-size:12px;border-right:1px solid black;">
                                        <?php echo $licenseName[0]["nombre"]; 
                                        if($item["id_licencia"]=='21'){
                                            echo '<br/><input type="checkbox" class="form-control" name="Order[free_ede]" id="free_ede"/><span style="color:red;">FREE</span>';
                                        }
                                        ?> 
                                    </td>
                                    <td style="padding:5px;border:1px solid #0f5418;font-weight:800;text-align:center;font-size:12px;border-right:1px solid black;">
                                        <?php echo '<input type="text" name="'.$item["id_licencia"].'[amount]" id="'.$item["id_licencia"].'_amount" class="amount" value="'.$item["quantity"].'">
                                        <input type="hidden" name="'.$item["id_licencia"].'[id_order_license]" class="id_order_license" value="'.$item["id_order_license"].'">'; ?>
                                    </td>
                                    <td style="padding:5px;border:1px solid #0f5418;font-weight:800;text-align:center;font-size:12px;border-right:1px solid black;">$
                                        <?php echo '<input type="text" name="'.$item["id_licencia"].'[price]" id="'.$item["id_licencia"].'_price" class="prices" value="'.$item["price"].'">'?>
                                    </td>
                                    <td style="padding:5px;border:1px solid #0f5418;font-weight:800;text-align:right;font-size:12px;border-right:1px solid black;">$
                                        <?php echo '<span id="'.$item["id_licencia"].'_total">'.$item["total"].'</td>'?>
                                    </td>
                                </tr>
                              <?php  
                              }
                               echo '<input type="hidden" name="Order[consecutivo]" class="amount" value="'.$consecutivo.'">';
                              ?>
                              <tr>
                                  <td colspan="3" style="padding:10px;background-color:#dedcdc;">
                                  </td>
                                   
                                  <td  style="padding:10px;border:1px solid #0f5418; background-color: #77e286;">
                                      <b>SUBTOTAL</b>
                                  </td>
                                  <td style="padding:10px;border:1px solid #0f5418; text-align:right;background-color: #77e286;">$
                                    <span id="subtotal_span"> <?php echo$order[0]["subtotal"];?></span>
                                      <?php echo '<input type="hidden" name="Order[subtotal]" class="prices" id="subtotal" value="'.$order[0]["subtotal"].'">'?>
                                  </td>
                              </tr>
                              <tr>
                                  <td colspan="3" style="padding:10px;background-color:#dedcdc;">
                                      XXXXXXXXXXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXXXXXXXX
                                  </td>
                                   
                                  <td style="padding:10px;border:1px solid #0f5418;">
                                      DISCOUNT
                                  </td>
                                  <td style="padding:10px;border:1px solid #0f5418; text-align:right;">
                                    <?php echo '<input type="text" name="Order[discount]" class="prices" id="points" value="'.$order[0]["discount"].'">'?>
                                  %</td>
                              </tr>
                              <tr>
                                  <td colspan="3" style="padding:10px;background-color:#dedcdc;">
                                  
                                  </td>
                                  <td style="padding:10px;border:1px solid #0f5418;">
                                      <b>TOTAL</b>
                                  </td>
                                  <td style="padding:10px;border:1px solid #0f5418; text-align:right;background-color:#39b54a;color:white;font-weight:800;">$
                                     <span id="total"> <?php echo number_format($order[0]["total"]);?> </span>
                                  </td>
                              </tr>
                              <tr>
                                    <td class="col-lg-8 col-sm-8 col-xs-8">
                                        
                                    </td>
                                    <td class="col-lg-4col-sm-4 col-xs-4" style="padding: 5px 0px;">
                                        <?php if($status!="confirmed"){
                                            echo '<button type="submit" class="btn btn-warning btn-xs edit" id="send"  style="width: 100%;min-height: 30px; color:black; font-weight:bold;">SEND</button>';
                                        }
                                        ?> 
                                       </td>
                                </tr>
                          <!-- END BODY-->    
                  </table>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
//
$( document ).ready(function() {
    
    
    sub_ilt19 = parseFloat($('#19_total').text());;
    if(isNaN( sub_ilt19 )){
        sub_ilt19 = 0;
    }
    sub_ilt21 = parseFloat($('#21_total').text());;
    if(isNaN( sub_ilt21 )){
        sub_ilt21 = 0;
    }
    sub_ilt17 = parseFloat($('#17_total').text());;
    if(isNaN( sub_ilt17 )){
        sub_ilt17 = 0;
    }
    sub_ilt18 = parseFloat($('#18_total').text());;
    if(isNaN( sub_ilt18 )){
        sub_ilt18 = 0;
    }
    
    currency = parseFloat($('#currency_value').val());
    price19 = parseFloat($('#19_price').val());
    price21 = parseFloat($('#21_price').val());
    price17 = parseFloat($('#17_price').val());
    price18 = parseFloat($('#18_price').val());
    
});
//tecs
$( "#19_amount" ).keyup(function() {
    ilt19_quantity =  parseFloat($('#19_amount').val());
    price19_mod = parseFloat($('#19_price').val());
    console.log(currency);
    if(price19 != price19_mod){
        price19 = price19_mod;
    }else{
        switch(true) {
            case ilt19_quantity < 199:
                price19 = 28*currency;
            break;
            case ilt19_quantity >= 200 && ilt19_quantity < 500:
                price19= 18*currency;
            break;
            case ilt19_quantity >=500 && ilt19_quantity < 1000:
                price19= 16*currency;
            break;
            case ilt19_quantity >= 1000:
                price19= 11*currency;
            break;
            default:
                price19 = 28*currency;
        }
        $('#19_price').val(price19);
    }
    
    sub_ilt19 = (ilt19_quantity*price19).toFixed(2);
    $('#19_total').text(sub_ilt19);
    total = (parseFloat(sub_ilt19) + parseFloat(sub_ilt21) + parseFloat(sub_ilt17) + parseFloat(sub_ilt18)).toFixed(2);
    //total = new Intl.NumberFormat("de-DE").format(total);
    $('#subtotal_span').text(total);
    $('#subtotal').text(total);
    $('#total').text(total);
    
});

$( "#19_price" ).keyup(function() {
    ilt19_quantity =  parseInt($('#19_amount').val());
    price19 =  parseFloat($('#19_price').val());
   
	sub_ilt19 = (ilt19_quantity*price19).toFixed(2);
    $('#19_total').text(sub_ilt19);
    total = (parseFloat(sub_ilt19) + parseFloat(sub_ilt21) + parseFloat(sub_ilt17) + parseFloat(sub_ilt18)).toFixed(2);
    $('#subtotal_span').text(total);
    $('#subtotal').text(total);
    $('#total').text(total);

});     
//EDE
$( "#21_amount" ).keyup(function() {
    ilt21_quantity =  parseInt($('#21_amount').val());
    price21_mod = parseFloat($('#21_price').val());
    
    if(price21 != price21_mod){
        price21 = price21_mod;
    }else{
        switch(true) {
            case ilt21_quantity < 999:
                price21 = 6*currency;
                break;
            case ilt21_quantity >= 1000:
                price21 = 3*currency;
                break;
            default:
                price21 = 6*currency;
        }
        $('#21_price').val(price21);
    }
    
    sub_ilt21 = parseFloat(ilt21_quantity*price21).toFixed(2);
    $('#21_total').text(sub_ilt21);
    total = (parseFloat(sub_ilt19) + parseFloat(sub_ilt21) + parseFloat(sub_ilt17) + parseFloat(sub_ilt18)).toFixed(2);
    $('#subtotal_span').text(total);
    $('#subtotal').text(total);
    $('#total').text(total);
});

$( "#21_price" ).keyup(function() {
    ilt21_quantity =  parseFloat($('#21_amount').val());
    price21 =  parseFloat($('#21_price').val());
   
	sub_ilt21 = (ilt21_quantity*price21).toFixed(2);
    $('#21_total').text(sub_ilt21);
    total = (parseFloat(sub_ilt19) + parseFloat(sub_ilt21) + parseFloat(sub_ilt17) + parseFloat(sub_ilt18)).toFixed(2);
    $('#subtotal_span').text(total);
    $('#subtotal').text(total);
    $('#total').text(total);

});

$("#free_ede").click(function() { 
    if($("#free_ede").is(':checked')) {  
        price2 = 0; 
        $('#2_price').val(price2);
        sub_ilt21 = ilt21_quantity*price2;
        $('#2_total').text(sub_ilt21);
        total = (parseFloat(sub_ilt19) + parseFloat(sub_ilt21) + parseFloat(sub_ilt17) + parseFloat(sub_ilt18)).toFixed(2);
        $('#subtotal_span').text(total);
        $('#subtotal').text(total);
        $('#total').text(total);
    } else {  
        ilt21_quantity =  parseInt($('#2_amount').val());
        switch(true) {
            case ilt21_quantity < 999:
                price2 = 6*currency;
            break;
            case ilt21_quantity >= 1000:
                price2 = 3*currency;
            break;
            default:
                price2 = 6*currency;
        }
        sub_ilt21 = (ilt21_quantity*price2).toFixed(2);
        $('#2_total').text(sub_ilt21);
        total = (parseFloat(sub_ilt19) + parseFloat(sub_ilt21) + parseFloat(sub_ilt17) + parseFloat(sub_ilt18)).toFixed(2);
        $('#subtotal_span').text(total);
        $('#subtotal').text(total);
        $('#total').text(total);
    }  
        
    }); 
    //TEC ECLUV
    $( "#18_amount" ).keyup(function() {
    ilt18_quantity =  parseInt($('#18_amount').val());
    price18_mod = parseInt($('#18_price').val());
    
    if(price18 != price18_mod){
        price18 = price18_mod;
    }else{
        switch(true) {
            case ilt18_quantity < 199:
                price18 = 28*currency;
            break;
            case ilt18_quantity >= 200 && ilt18_quantity < 500:
                price18= 18*currency;
            break;
            case ilt18_quantity >=500 && ilt18_quantity < 1000:
                price18= 16*currency;
            break;
            case ilt18_quantity >= 1000:
                price18= 11*currency;
            break;
            default:
                price18 = 28*currency;
        }
        $('#18_price').val(price18);
    }
    
    sub_ilt18 = (ilt18_quantity*price18).toFixed(2);;
    $('#18_total').text(sub_ilt18);
    total = (parseFloat(sub_ilt19) + parseFloat(sub_ilt21) + parseFloat(sub_ilt17) + parseFloat(sub_ilt18)).toFixed(2);
    $('#subtotal_span').text(total);
    $('#subtotal').text(total);
    $('#total').text(total);
    });
    
    $( "#18_price" ).keyup(function() {
        ilt18_quantity =  parseInt($('#18_amount').val());
        price18 =  parseInt($('#18_price').val());
       
    	sub_ilt18 = ilt18_quantity*price18;
        $('#18_total').text(sub_ilt18);
        total = (parseFloat(sub_ilt19) + parseFloat(sub_ilt21) + parseFloat(sub_ilt17) + parseFloat(sub_ilt18)).toFixed(2);
        $('#subtotal_span').text(total);
        $('#subtotal').text(total);
        $('#total').text(total);
    
    });    
    $( "#points" ).keyup(function() {
        points =  parseInt($('#points').val());
        subtotal = parseFloat(sub_ilt19) + parseFloat(sub_ilt21) + parseFloat(sub_ilt17) + parseFloat(sub_ilt18);
        discount = parseFloat(Math.round(subtotal*points)/100).toFixed(2);
        total = subtotal - discount;
        $('#percent').text(points);
        $('#discount').text(discount);
        $('#total').text(total);
    });

</script>
<?php
/* @var $this LicensesClientController */
/* @var $model OrdersClient */

$config = Yii::app()->getComponents(false);
$connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
//$connection=new CDbConnection('mysql:host=localhost;dbname=iltoexam_newversion','iltoexam_newvers','Ilto.2015');
//$connection=new CDbConnection($config["db"]["connectionString"],$config["db"]["username"],$config["db"]["password"]);
$connection->active=true;


$SQL = "SELECT * FROM `orders` ORDER BY `id_order` DESC";
$list= $connection->createCommand($SQL)->queryAll();

$this->breadcrumbs=array(
	'Licenses Clients'=>array('index'),
	'Manage',
);
/*
$this->menu=array(
	array('label'=>'Assign new License', 'url'=>array('create')),
);*/

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#licenses-client-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<link rel="stylesheet" type="text/css" href="/ilto3/assets/563dc7d4/gridview/styles.css" />
<link rel="stylesheet" type="text/css" href="/ilto3/assets/952e6968/pager.css" />
<h1>Orders per Client</h1><a class="sort-link" href="/ilto3/index.php?r=Api/OrdersExport">Export DATA</a>
<div id="clients-grid" class="grid-view">
	<table class="items">
		<thead>
			<tr>
				<th id="orders-grid_c0">
			    	<a class="sort-link" href="#">ID Order</a>
			    </th>
			    <th id="orders-grid_c0">
			    	<a class="sort-link" href="#">Client</a>
			    </th>
			    <th id="orders-grid_c0">
			    	<a class="sort-link" href="#">Date Expedition</a>
			    </th>
			    <th id="orders-grid_c1">
			    	<a class="sort-link" href="#">Date Expire</a>
			    </th>
			    	<th id="orders-grid_c0">
			    	<a class="sort-link" href="#">subtotal</a>
			    </th>
			    <th id="orders-grid_c0">
			    	<a class="sort-link" href="#">discount</a>
			    </th>
			    <th id="orders-grid_c1">
			    	<a class="sort-link" href="#">total</a>
			    </th>
			     <th id="orders-grid_c1">
			    	<a class="sort-link" href="#">Status</a>
			    </th>
			    <th id="orders-grid_c1">
			    	<a class="sort-link" href="#">Distributor</a>
			    </th>
			    <th id="orders-grid_c1">
			    	<a class="sort-link" href="#">Currency</a>
			    </th>
			    <th id="orders-grid_c1">
			    	<a class="sort-link" href="#"></a>
			    </th>
		   	</tr>
		   	<tr class="filters">
			</tr>
		</thead>
		<tbody>
		<?php
			foreach($list as $value){
				$SQL = "SELECT nombre_rsocial FROM `clients` WHERE id_cliente='".$value['id_client']."'";
				$client= $connection->createCommand($SQL)->queryAll();
				
				if($value["id_distributor"]!='NULL'){
					$SQL = "SELECT nombre_rsocial FROM `clients` WHERE id_cliente='".$value['id_distributor']."'";
					$distributor= $connection->createCommand($SQL)->queryAll();	
					
					$id_distr = $distributor[0]["nombre_rsocial"];
				}else{
					$id_distr = "ILTOEXAMS";
				}
				
				switch ($value['status']) {
				    case 'P':
				        $status= "Pending";
				        break;
				    case 'R':
				        $status= "Approved";
				        break;
				    case 'A':
				        $status= "Confirm";
				        break;
				    case 'C':
				        $status= "Cancel";
				        break;
				    default:
				        $status= "Pending";
				}
				
				
				echo	'<tr>
		                    <td>'.$value['id_order'].'</td>
		                    <td>'.$client[0]['nombre_rsocial'].'</td>
		                    <td>'.substr($value['date_expedition'], 0, 10).'</td>
		                    <td>'.substr($value['date_expire'], 0, 10).'</td>
		                    <td>'.$value['subtotal'].'</td>
		                    <td>'.$value['discount'].'%</td>
		                    <td>'.$value['total'].'</td>
		                    <td>'.$status.'</td>
		                    <td>'.$id_distr.'</td>
		                    <td>'.$value['currency_type'].'</td>
		                    <td><a href="/ilto3/index.php?r=Orders/update&id='.$value['id_order'].'">EDIT</a></td>
	                        </tr>';	
			}
			?>
			
		</tbody>
	</table>
	<div class="keys" style="display:none" title="/ilto3/index.php?r=clients/admin"><span>2030405060</span><span>8000785388</span><span>8001528404</span><span>8300026341</span><span>830033825</span><span>8600112851</span><span>8600137985</span><span>860029924</span><span>8600383741</span><span>8600667896</span><span>8600705365</span><span>8604045797</span><span>860506140-6</span><span>8605079033</span><span>8605127804</span><span>8605130779</span><span>8605173021</span><span>8605310815</span><span>8802214435</span><span>8903074001</span><span>890310903</span><span>8903990166</span><span>89039901661</span><span>8907043821</span><span>8907045629</span><span>8909851899</span><span>8909854173</span><span>8914008037</span><span>900</span><span>9000059366</span><span>9000260140</span><span>9002120741</span><span>9002963855</span><span>900430260</span><span>9004302647</span><span>9004845446</span><span>900493991</span><span>9008752661</span><span>test</span></div>
</div>




<?php
/* @var $this AgentsController */
/* @var $model Agents */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'prefix'); ?>
		<?php echo $form->textField($model,'prefix',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'first_name'); ?>
		<?php echo $form->textField($model,'first_name',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'last_name'); ?>
		<?php echo $form->textField($model,'last_name',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'job_title'); ?>
		<?php echo $form->textField($model,'job_title',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'department'); ?>
		<?php echo $form->textField($model,'department',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'phone'); ?>
		<?php echo $form->textField($model,'phone',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'prefix_1'); ?>
		<?php echo $form->textField($model,'prefix_1',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'first_name_1'); ?>
		<?php echo $form->textField($model,'first_name_1',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'last_name_1'); ?>
		<?php echo $form->textField($model,'last_name_1',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'job_title_1'); ?>
		<?php echo $form->textField($model,'job_title_1',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'department_1'); ?>
		<?php echo $form->textField($model,'department_1',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'email_1'); ?>
		<?php echo $form->textField($model,'email_1',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'phone_1'); ?>
		<?php echo $form->textField($model,'phone_1',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'prefix_2'); ?>
		<?php echo $form->textField($model,'prefix_2',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'first_name_2'); ?>
		<?php echo $form->textField($model,'first_name_2',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'last_name_2'); ?>
		<?php echo $form->textField($model,'last_name_2',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'job_title_2'); ?>
		<?php echo $form->textField($model,'job_title_2',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'department_2'); ?>
		<?php echo $form->textField($model,'department_2',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'email_2'); ?>
		<?php echo $form->textField($model,'email_2',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'phone_2'); ?>
		<?php echo $form->textField($model,'phone_2',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'country'); ?>
		<?php echo $form->textField($model,'country'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'city'); ?>
		<?php echo $form->textField($model,'city'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'institution_name'); ?>
		<?php echo $form->textField($model,'institution_name',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'address_1'); ?>
		<?php echo $form->textField($model,'address_1',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'address_2'); ?>
		<?php echo $form->textField($model,'address_2',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'address_3'); ?>
		<?php echo $form->textField($model,'address_3',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'state'); ?>
		<?php echo $form->textField($model,'state',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'zip'); ?>
		<?php echo $form->textField($model,'zip',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'organization_type'); ?>
		<?php echo $form->textField($model,'organization_type',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alter_country'); ?>
		<?php echo $form->textField($model,'alter_country'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alter_city'); ?>
		<?php echo $form->textField($model,'alter_city'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alter_iinstitution_name'); ?>
		<?php echo $form->textField($model,'alter_iinstitution_name',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alter_address_1'); ?>
		<?php echo $form->textField($model,'alter_address_1',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alter_address_2'); ?>
		<?php echo $form->textField($model,'alter_address_2',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alter_address_3'); ?>
		<?php echo $form->textField($model,'alter_address_3',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alter_state'); ?>
		<?php echo $form->textField($model,'alter_state',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alter_zip'); ?>
		<?php echo $form->textField($model,'alter_zip',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'is_other_agent'); ?>
		<?php echo $form->textField($model,'is_other_agent'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'organization_tests'); ?>
		<?php echo $form->textArea($model,'organization_tests',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'organization_references'); ?>
		<?php echo $form->textArea($model,'organization_references',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'years_operating'); ?>
		<?php echo $form->textField($model,'years_operating'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'years_location'); ?>
		<?php echo $form->textField($model,'years_location'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'secure_rooms'); ?>
		<?php echo $form->textField($model,'secure_rooms'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'computers_room1'); ?>
		<?php echo $form->textField($model,'computers_room1'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'computers_room2'); ?>
		<?php echo $form->textField($model,'computers_room2'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'computers_room3'); ?>
		<?php echo $form->textField($model,'computers_room3'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'net_type_room1'); ?>
		<?php echo $form->textField($model,'net_type_room1'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'net_type_room2'); ?>
		<?php echo $form->textField($model,'net_type_room2'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'net_type_room3'); ?>
		<?php echo $form->textField($model,'net_type_room3'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'partitions'); ?>
		<?php echo $form->textField($model,'partitions'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
<?php
/* @var $this AgentsController */
/* @var $model Agents */

$this->breadcrumbs=array(
	'Agents'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Agents', 'url'=>array('admin')),
);

?>

<h1>Agents Registration Form</h1>
<?php 
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'agents-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>
	<?php echo $form->errorSummary($model); ?>

<?php
$this->widget('bootstrap.widgets.TbTabs', array(
                "id" => "tabs",
                "type" => "tabs",
                'tabs'=>array(
                array(
                        'active'=>true,
                        'label'=>'Personal Information',
                        'content'=>$this->renderPartial("agent_form1", array('model' => $model,'form'=>$form),
                        true),
                ),
                array(
                        'label'=>'Company Information',
                        'content'=>$this->renderPartial("agent_form2",
                                array(
                                        'model' => $model,
                                        'form'=>$form,
                                ),
                        true),
                ),
)));
?>

<?php $this->endWidget(); ?>

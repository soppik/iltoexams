<?php
/* @var $this AgentsController */
/* @var $model Agents */

$this->breadcrumbs=array(
	'Agents'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Agents', 'url'=>array('index')),
	array('label'=>'Create Agents', 'url'=>array('create')),
	array('label'=>'Update Agents', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Agents', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Agents', 'url'=>array('admin')),
);
?>

<h1>View Agents #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'prefix',
		'first_name',
		'last_name',
		'job_title',
		'department',
		'email',
		'phone',
		'prefix_1',
		'first_name_1',
		'last_name_1',
		'job_title_1',
		'department_1',
		'email_1',
		'phone_1',
		'prefix_2',
		'first_name_2',
		'last_name_2',
		'job_title_2',
		'department_2',
		'email_2',
		'phone_2',
		'country',
		'city',
		'institution_name',
		'address_1',
		'address_2',
		'address_3',
		'state',
		'zip',
		'organization_type',
		'alter_country',
		'alter_city',
		'alter_iinstitution_name',
		'alter_address_1',
		'alter_address_2',
		'alter_address_3',
		'alter_state',
		'alter_zip',
		'is_other_agent',
		'organization_tests',
		'organization_references',
		'years_operating',
		'years_location',
		'secure_rooms',
		'computers_room1',
		'computers_room2',
		'computers_room3',
		'net_type_room1',
		'net_type_room2',
		'net_type_room3',
		'partitions',
	),
)); ?>

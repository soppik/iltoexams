 
        <div class="row"><p><strong>Test Site Location</strong><br><br>Please fill out the following boxes regarding the organization location </p></div>
        
	<div class="row">
		<?php echo $form->labelEx($model,'country'); ?>
		<?php
            $dataCombo=CHtml::listData(Countries::model()->findAllByAttributes(array('estado'=>'A')),'id_pais','nombre');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'Agents[country]',
            'id'=> 'combo_paises',
            'data'=>$dataCombo,
                'value'=>$model->country,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'200px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'country'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'city'); ?>
            <select name="Agents[city]" id="Agents_city"></select>
		<?php echo $form->error($model,'city'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'institution_name'); ?>
		<?php echo $form->textField($model,'institution_name',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'institution_name'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'id_legal'); ?>
		<?php echo $form->textField($model,'id_legal',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'id_legal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address_1'); ?>
		<?php echo $form->textField($model,'address_1',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'address_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address_2'); ?>
		<?php echo $form->textField($model,'address_2',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'address_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address_3'); ?>
		<?php echo $form->textField($model,'address_3',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'address_3'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'state'); ?>
		<?php echo $form->textField($model,'state',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'state'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'zip'); ?>
		<?php echo $form->textField($model,'zip',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'zip'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'organization_type'); ?>
		<?php echo $form->textField($model,'organization_type',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'organization_type'); ?>
	</div>
        <div class="row">
            Is the shipping address similar to the Test Site Location?
        </div>
	<div class="row">
		<?php echo $form->labelEx($model,'same_address'); ?>
		<?php echo $form->dropDownList($model,'same_address', array('1'=> 'Yes', '2'=> 'No')); ?>
		<?php echo $form->error($model,'same_address'); ?>
	</div>
        <div id="alter_location" style="display:none; margin-left: 0px !important">
	<div class="row">
		<?php echo $form->labelEx($model,'alter_country'); ?>
		<?php echo $form->textField($model,'alter_country'); ?>
		<?php echo $form->error($model,'alter_country'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alter_city'); ?>
		<?php echo $form->textField($model,'alter_city'); ?>
		<?php echo $form->error($model,'alter_city'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alter_iinstitution_name'); ?>
		<?php echo $form->textField($model,'alter_iinstitution_name',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'alter_iinstitution_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alter_address_1'); ?>
		<?php echo $form->textField($model,'alter_address_1',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'alter_address_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alter_address_2'); ?>
		<?php echo $form->textField($model,'alter_address_2',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'alter_address_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alter_address_3'); ?>
		<?php echo $form->textField($model,'alter_address_3',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'alter_address_3'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alter_state'); ?>
		<?php echo $form->textField($model,'alter_state',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'alter_state'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alter_zip'); ?>
		<?php echo $form->textField($model,'alter_zip',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'alter_zip'); ?>
	</div>
</div>
	<div class="row">
		<?php echo $form->labelEx($model,'is_other_agent'); ?>
		<?php echo $form->dropDownList($model,'is_other_agent', array('1'=> 'Yes', '2'=> 'No')); ?>
		<?php echo $form->error($model,'is_other_agent'); ?>
	</div>
<div id="other_tests" class="row" style="display:none; margin-left: 0px !important" >
	<div class="row">
		<?php echo $form->labelEx($model,'organization_tests'); ?>
		<?php echo $form->textArea($model,'organization_tests',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'organization_tests'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'organization_references'); ?>
		<?php echo $form->textArea($model,'organization_references',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'organization_references'); ?>
	</div>
</div>
	<div class="row">
		<?php echo $form->labelEx($model,'years_operating'); ?>
		<?php echo $form->textField($model,'years_operating'); ?>
		<?php echo $form->error($model,'years_operating'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'years_location'); ?>
		<?php echo $form->textField($model,'years_location'); ?>
		<?php echo $form->error($model,'years_location'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'secure_rooms'); ?>
		<?php echo $form->textField($model,'secure_rooms'); ?>
		<?php echo $form->error($model,'secure_rooms'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'computers_room1'); ?>
		<?php echo $form->textField($model,'computers_room1'); ?>
		<?php echo $form->error($model,'computers_room1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'computers_room2'); ?>
		<?php echo $form->textField($model,'computers_room2'); ?>
		<?php echo $form->error($model,'computers_room2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'computers_room3'); ?>
		<?php echo $form->textField($model,'computers_room3'); ?>
		<?php echo $form->error($model,'computers_room3'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'net_type_room1'); ?>
		<?php echo $form->textField($model,'net_type_room1'); ?>
		<?php echo $form->error($model,'net_type_room1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'net_type_room2'); ?>
		<?php echo $form->textField($model,'net_type_room2'); ?>
		<?php echo $form->error($model,'net_type_room2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'net_type_room3'); ?>
		<?php echo $form->textField($model,'net_type_room3'); ?>
		<?php echo $form->error($model,'net_type_room3'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'partitions'); ?>
		<?php echo $form->textField($model,'partitions'); ?>
		<?php echo $form->error($model,'partitions'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>
        <script type="text/javascript">
            $('#combo_paises').change( function() {
        debugger;
        var valor = $('#combo_paises').val();
       var url = "/ilto3/index.php?r=cities/citiesCountry/country/"+valor;
    $.getJSON( url, {
        format: "json"
      }).done(function( data ) {
          debugger;
          $('#Agents_city').length=0;
          var salida= [];
          $.each( data, function( i, item ) {
              salida.push('<option value="'+i+'">'+ item+"</option>");
          });
          $('#Agents_city').html(salida.join(''));
        }); 
    });
    $('#Agents_same_address').on('click',function(){
        if($('#Agents_same_address').val()==2){
            $('#alter_location').show();
        } else {
            $('#alter_location').hide();
        }
    });
    $('#Agents_is_other_agent').on('click',function(){
        if($('#Agents_is_other_agent').val()==1){
            $('#other_tests').show();
        } else {
            $('#other_tests').hide();
        }
    });
    
        </script>

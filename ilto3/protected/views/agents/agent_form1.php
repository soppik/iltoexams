<?php 
$modUtil= new Utilidades();
$prefixes = $modUtil->arregloPrefijos();

if($model->isnewrecord){
$model->main_decision = 1;
$model->same_address = 1;
$model->is_other_agent = 2;
}
?>
	<p class="note">Fields with <span class="required">*</span> are required.</p>


        <p><br><br><strong>YOUR INFORMATION</strong></p>
	<div class="row">
		<?php echo $form->labelEx($model,'prefix'); ?>
                <?php echo $form->dropDownList($model,'prefix', $prefixes, array('prompt'=>'Select prefix')); ?>
		<?php echo $form->error($model,'prefix'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_id_number'); ?>
		<?php echo $form->textField($model,'user_id_number',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'user_id_number'); ?>
	</div>

        <div class="row">
		<?php echo $form->labelEx($model,'first_name'); ?>
		<?php echo $form->textField($model,'first_name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'first_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'last_name'); ?>
		<?php echo $form->textField($model,'last_name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'last_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'job_title'); ?>
		<?php echo $form->textField($model,'job_title',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'job_title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'department'); ?>
		<?php echo $form->textField($model,'department',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'department'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model,'phone',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>
        <div class="row">
            Are you the main decision maker in determining if your organization wants to become a TECS Authorized Agent?
        </div>
	<div class="row">
		<?php echo $form->labelEx($model,'main_decision'); ?>
		<?php echo $form->dropDownList($model,'main_decision', array('1'=> 'Yes', '2'=> 'No')); ?>
		<?php echo $form->error($model,'main_decision'); ?>
	</div>
            

<div class="row" id="main_person" style="display:none; margin-left: 0px !important">        
    <p><strong>Please complete the information below:</strong></p>
	<div class="row">
		<?php echo $form->labelEx($model,'prefix_1'); ?>
                <?php echo $form->dropDownList($model,'prefix_1', $prefixes, array('prompt'=>'Select prefix')); ?>
		<?php echo $form->error($model,'prefix_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_id_number1'); ?>
		<?php echo $form->textField($model,'user_id_number1',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'user_id_number1'); ?>
	</div>

    <div class="row">
		<?php echo $form->labelEx($model,'first_name_1'); ?>
		<?php echo $form->textField($model,'first_name_1',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'first_name_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'last_name_1'); ?>
		<?php echo $form->textField($model,'last_name_1',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'last_name_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'job_title_1'); ?>
		<?php echo $form->textField($model,'job_title_1',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'job_title_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'department_1'); ?>
		<?php echo $form->textField($model,'department_1',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'department_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email_1'); ?>
		<?php echo $form->textField($model,'email_1',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'email_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phone_1'); ?>
		<?php echo $form->textField($model,'phone_1',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'phone_1'); ?>
	</div>
</div>
        
<div id="additional_user" style="clear:both;">        
    <p><strong>Please list an alternate contact for your institution:</strong></p>
	<div class="row">
		<?php echo $form->labelEx($model,'prefix_2'); ?>
                <?php echo $form->dropDownList($model,'prefix_2', $prefixes, array('prompt'=>'Select prefix')); ?>
		<?php echo $form->error($model,'prefix_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_id_number2'); ?>
		<?php echo $form->textField($model,'user_id_number2',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'user_id_number2'); ?>
	</div>

    <div class="row">
		<?php echo $form->labelEx($model,'first_name_2'); ?>
		<?php echo $form->textField($model,'first_name_2',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'first_name_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'last_name_2'); ?>
		<?php echo $form->textField($model,'last_name_2',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'last_name_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'job_title_2'); ?>
		<?php echo $form->textField($model,'job_title_2',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'job_title_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'department_2'); ?>
		<?php echo $form->textField($model,'department_2',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'department_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email_2'); ?>
		<?php echo $form->textField($model,'email_2',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'email_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phone_2'); ?>
		<?php echo $form->textField($model,'phone_2',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'phone_2'); ?>
	</div>

    <div class="row">
		<?php echo $form->labelEx($model,'logo'); ?>
		<?php echo $form->fileField($model,'logo'); ?>
		<?php echo $form->error($model,'logo'); ?>
	</div>
    
</div>
        <script type="text/javascript">
    $('#Agents_main_decision').on('click',function(){
        if($('#Agents_main_decision').val()==2){
            $('#main_person').show();
        } else {
            $('#main_person').hide();
        }
    });
            </script>
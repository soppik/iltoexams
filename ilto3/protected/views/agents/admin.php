<?php
/* @var $this AgentsController */
/* @var $model Agents */

$this->breadcrumbs=array(
	'Agents'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create Agents', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#agents-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Agents</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'agents-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'prefix',
		'first_name',
		'last_name',
		'job_title',
		'department',
		/*
		'email',
		'phone',
		'prefix_1',
		'first_name_1',
		'last_name_1',
		'job_title_1',
		'department_1',
		'email_1',
		'phone_1',
		'prefix_2',
		'first_name_2',
		'last_name_2',
		'job_title_2',
		'department_2',
		'email_2',
		'phone_2',
		'country',
		'city',
		'institution_name',
		'address_1',
		'address_2',
		'address_3',
		'state',
		'zip',
		'organization_type',
		'alter_country',
		'alter_city',
		'alter_iinstitution_name',
		'alter_address_1',
		'alter_address_2',
		'alter_address_3',
		'alter_state',
		'alter_zip',
		'is_other_agent',
		'organization_tests',
		'organization_references',
		'years_operating',
		'years_location',
		'secure_rooms',
		'computers_room1',
		'computers_room2',
		'computers_room3',
		'net_type_room1',
		'net_type_room2',
		'net_type_room3',
		'partitions',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

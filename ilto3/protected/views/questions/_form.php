<?php
/* @var $this QuestionsController */
/* @var $model Questions */
/* @var $form CActiveForm */
$modUtil= new Utilidades();
$SQL = "SELECT id_examen, nombre FROM `exams`ORDER BY `exams`.`id_examen` DESC";
$exams= Yii::app()->db->createCommand($SQL)->queryAll();

$SQL = "SELECT * FROM `question_header`";
$headers= Yii::app()->db->createCommand($SQL)->queryAll();


?>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'questions-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
	<div class="row">
		<?php echo $form->labelEx($model,'id_seccion'); ?>
		<?php
            $dataCombo=CHtml::listData(Sections::model()->findAllByAttributes(array('estado'=>'A')),'id_seccion','nombre');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'Questions[id_seccion]',
            'id'=> 'combo_seccion',
            'data'=>$dataCombo,
                'value'=>$model->id_seccion,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'220px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'id_seccion'); ?>
	</div>
	<div class="row">
	  <label for="Questions_nivel" class="required">EXAM <span class="required">*</span></label>
  	  <div class="select2-container" id="s2id_combo_exams" style="width: 220px">   
  	    <select name="Questions[id_exam]" id="combo_exam" style="width:100%">
  	       <?php foreach($exams as $exam){
  	         ?>
  	        <option value="<?php echo $exam['id_examen']?>"><?php echo utf8_encode($exam['nombre'])?></option>
  	        <?php
  	       }?>   
	      </select>
	   </div>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'nivel'); ?>
		<?php
            $dataCombo=$modUtil->arregloNiveles();
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'Questions[nivel]',
            'id'=> 'combo_nivel',
            'data'=>$dataCombo,
                'value'=>$model->nivel,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'220px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'nivel'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'tipo_presentacion'); ?>
		<?php
            $dataCombo=$modUtil->arregloPresentaciones();
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'Questions[tipo_presentacion]',
            'id'=> 'combo_tipo',
            'data'=>$dataCombo,
                'value'=>$model->tipo_presentacion,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'220px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'tipo_presentacion'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'template'); ?>
		<?php
            $dataCombo=$modUtil->arregloTemplates();
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'Questions[template]',
            'id'=> 'combo_template',
            'data'=>$dataCombo,
                'value'=>$model->template,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'320px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'template'); ?>
	</div>
  <div class="row">
    	<?php echo $form->labelEx($model,'Header'); ?>
		<div class="select2-container" id="s2id_combo_header" style="width: 320px"> 
		<select class="select2-container" name="Questions[id_question_header]" id="s2id_combo_header" >
      <option value="">
      </option>
      <?php
      foreach($headers as $value){
        echo "</option><option value='".$value["id_question_header"]."'>".$value["description"]."</option>";;
      }
      ?>
      </select>
		  <?php echo $form->error($model,'template'); ?>
	  </div>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'texto'); ?>
		<?php echo $form->textArea($model,'texto',array('rows'=>6, 'cols'=>30)); ?>
		<?php echo $form->error($model,'texto'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'titulo_respuesta'); ?>
		<?php echo $form->textField($model,'titulo_respuesta'); ?>
		<?php echo $form->error($model,'titulo_respuesta'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'ruta_imagen'); ?>
		<?php echo $form->fileField($model,'ruta_imagen'); ?>
		<?php echo $form->error($model,'ruta_imagen'); ?>
            <?php if(!$model->isNewRecord){
                if(strlen($model->ruta_imagen)>0){
                    echo "<img src='images/questions/tn2_$model->ruta_imagen' style='width:200px;'>";
                    echo "<input type='hidden' name='ruta_imagen_actual' value='$model->ruta_imagen'>";
                    echo "&nbsp;&nbsp;<input type='checkbox' name='delete_imagen' value='1'>&nbsp;Delete Image on Save";
                }
            } ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'time'); ?>
		<?php echo $form->textField($model,'time'); ?>
		<?php echo $form->error($model,'time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ruta_audio'); ?>
		<?php echo $form->fileField($model,'ruta_audio'); ?>
		<?php echo $form->error($model,'ruta_audio'); ?>
            <?php if(!$model->isNewRecord){
                if(strlen($model->ruta_audio)>0){
                         $this->widget('ext.jouele.jouele.Jouele', array(
                            'file' => Yii::app()->baseUrl."/".$model->ruta_audio,
                            'name' => 'Question',
                            'htmlOptions' => array(
                                'class' => 'jouele-skin-silver',
                                'style'=>'width:180px; background-color:#f2f2f2',
                             )
                ));
                    echo "<input type='hidden' name='ruta_audio_actual' value='$model->ruta_audio'>";
                    echo "&nbsp;&nbsp;<input type='checkbox' name='delete_audio' value='1'>&nbsp;Delete Audio on Save";
                }
            } ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'relacionada'); ?>
            <select name="Questions[relacionada]" id="Questions_relacionada" value=""></select>
		<?php echo $form->error($model,'relacionada'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estado'); ?>
		<?php
            $dataCombo=array('A'=>'Enabled','I'=>'Disabled');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'Questions[estado]',
            'id'=> 'combo_estado',
            'data'=>$dataCombo,
                'value'=>$model->estado,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'120px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'estado'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>
<?php $this->widget('application.extensions.tinymce.SladekTinyMce'); ?>
 
<script>
    $('#combo_seccion').change( function() {
        debugger;
        var valor = $('#combo_seccion').val();
        var url = "index.php?r=Questions/related/section/"+valor;
    $.getJSON( url, {
        format: "json"
      }).done(function( data ) {
          debugger;
          $('#Questions_relacionada').length=0;
          var salida= [];
          salida.push('<option value="">'+"</option>");
          $.each( data, function( i, item ) {
              salida.push('<option value="'+i+'">'+ item+"</option>");
          });
          $('#Questions_relacionada').html(salida.join(''));
          <?php if(!is_null($model->relacionada)){?>
          $("#Questions_relacionada option[value='<?php echo $model->relacionada;?>']").attr('selected', true);
          <?php }?>
        }); 
    });
    <?php 
    if(!$model->isNewrecord){
        echo "$('#combo_seccion').trigger('change');";
    }
    ?>
        
    tinymce.init({selector:'#Questions_texto',theme: "modern",
    width: 900,
    height: 300,
    plugins:"textcolor",
    toolbar1: "styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor",
    toolbar2: "formatselect fontselect fontsizeselect | cut copy paste pastetext pasteword, code"    
    });
  
$(document).ready(function(){
	$('#combo_seccion').change(function() {
		GetIdExam($(this).find(':selected').val())
	})
})

function GetIdExam(countryId){
	$("#city-select").children().remove()
		$.ajax({
			type: "POST",
			url: "/ilto3/protected/controllers/ApiController.php",
			data: "get=city&countryId=" + countryId
		}).done(function( result ) {
			$(result).each(function(i,j){
				$("#city-select").append($('<option>', {
					value: j.ID,
					text: j.Name,
			}));
		})
	});
}
</script>
<?php $this->endWidget(); ?>

</div><!-- form -->
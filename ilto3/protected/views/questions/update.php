<?php
/* @var $this QuestionsController */
/* @var $model Questions */

$this->breadcrumbs=array(
	'Questions'=>array('index'),
	$model->id_pregunta=>array('view','id'=>$model->id_pregunta),
	'Update',
);

$this->menu=array(
	array('label'=>'Create Questions', 'url'=>array('create')),
	array('label'=>'Manage Questions', 'url'=>array('admin')),
);
?>

<h1>Update Questions <?php echo $model->id_pregunta; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
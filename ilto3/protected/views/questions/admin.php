<?php
/* @var $this QuestionsController */
/* @var $model Questions */
$modUtil=new Utilidades();
$this->breadcrumbs=array(
	'Questions'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create Questions', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#questions-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

?>
<style>
    audio{
        width:150px; 
    }
</style>
<h1>Manage Questions</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'questions-grid',
    'afterAjaxUpdate'=>"function(){ $('.imagen').fancybox(); }",
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'enableHistory'=>true,
	'columns'=>array(
            'id_pregunta',
            array(
		'name'=>'nivel',
                'filter'=>$modUtil->arregloNiveles(),
                ),
            array(
		'name'=>'tipo_presentacion',
                'filter'=>$modUtil->arregloPresentaciones(),
                'value'=>'$data->nombrePresenta',
                ),
            array(
		'name'=>'template',
                'filter'=>$modUtil->arregloTemplates(),
                ),
	   
            array(
                'name'=>'texto',
                'type'=>'raw'
            ),
            'titulo_respuesta',
                array(
                    'name'=>'id_seccion',
                    'value'=>'$data->idSeccion->nombre',
                    'filter'=>CHtml::listData(Sections::model()->findAll(),"id_seccion","nombre"),
                ),
            array(
                'name'=>'ruta_imagen',
                'filter'=>'',
                'value'=>'(!empty($data->ruta_imagen))?CHtml::image(Yii::app()->baseUrl."/images/questions/tn1_".$data->ruta_imagen,"",array("class"=>"imagen", "href"=>Yii::app()->baseUrl."/images/questions/tn3_$data->ruta_imagen","style"=>"width:150px !important; max-width:100% !important;")):"no image"',
                'type'=>'raw'
                ),
                array(
                    'name'=>'estado',
                    'value'=>'($data->estado=="A" ? "Enabled" : "Disabled")',
                    'filter'=>array("A"=>"Enable","I"=>"Disable"),
                    ),
            array('name'=>'relacionada'),
		array(
			'class'=>'CButtonColumn',
                    'template'=>'{update}{delete}{answers}',
                    'buttons'=>array(
                        'answers'=>array(
                            'url'=> 'Yii::app()->createUrl("answers/admin",array("question"=>$data->id_pregunta))',
                            'imageUrl' =>NULL,
                            'label'=>'Answers',
                            'visible'=>'($data->tipo_presentacion=="S")?false:true',
                            
                        ),
                        'delete'=>array(
                            'label'=>'Delete_',
                            'imageUrl'=>NULL
                        ),
                        'update'=>array(
                            'label'=>'Edit_',
                            'imageUrl'=>NULL
                        )
                    )
		),
	),
)); 

$config = array( 
    'scrolling' => 'yes', 
    'titleShow' => false,
    'overlayColor' => '#000',
    'padding' => '10',
    'showCloseButton' => true,
       'width'=>'500px',
        // change this as you need
    );
    $this->widget('application.extensions.fancybox.EFancyBox', array('target'=>'.imagen', 'config'=>$config));

?>

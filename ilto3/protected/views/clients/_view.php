<?php
/* @var $this ClientsController */
/* @var $data Clients */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_cliente')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_cliente), array('view', 'id'=>$data->id_cliente)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre_rsocial')); ?>:</b>
	<?php echo CHtml::encode($data->nombre_rsocial); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('direccion')); ?>:</b>
	<?php echo CHtml::encode($data->direccion); ?>
	<br />

	<b><?php //echo CHtml::encode($data->getAttributeLabel('id_pais')); ?>:</b>
	<?php //echo CHtml::encode($data->id_pais); ?>
	<br />

	<b><?php //echo CHtml::encode($data->getAttributeLabel('id_ciudad')); ?>:</b>
	<?php //echo CHtml::encode($data->id_ciudad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nit_id_legal')); ?>:</b>
	<?php echo CHtml::encode($data->nit_id_legal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefono')); ?>:</b>
	<?php echo CHtml::encode($data->telefono); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('contacto')); ?>:</b>
	<?php echo CHtml::encode($data->contacto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ruta_logo')); ?>:</b>
	<?php echo CHtml::encode($data->ruta_logo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado')); ?>:</b>
	<?php echo CHtml::encode($data->estado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('logo_en_certificado')); ?>:</b>
	<?php echo CHtml::encode($data->logo_en_certificado); ?>
	<br />

	*/ ?>

</div>
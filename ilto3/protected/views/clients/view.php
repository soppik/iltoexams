<?php
/* @var $this ClientsController */
/* @var $model Clients */

$this->breadcrumbs=array(
	'Clients'=>array('index'),
	$model->id_cliente,
);

$this->menu=array(
	array('label'=>'Back to Manage Clients', 'url'=>array('admin')),
);
?>

<h1>View Clients #<?php echo $model->id_cliente; ?></h1>

<?php 
$imagen=(!empty($model->ruta_logo))? CHtml::image(Yii::app()->baseUrl."/images/clients/tn1_".$model->ruta_logo,"",array("class"=>"imagen", "href"=>Yii::app()->baseUrl."/images/clients/tn3_$model->ruta_logo","style"=>"width:150px !important; max-width:100% !important;")) :"no image";
$modLicencias = LicensesClient::model()->findAllByAttributes(array('id_cliente'=>$model->id_cliente));
$licencias ="<table><tr class='center'><th>LICENSE</th><th>Purchase Date  (YYYY - MM - DD)</th><th>Purchase</th><th>Used</th><th>Remain</th></tr>";
foreach($modLicencias as $rowLicencias){
    $licencias.="<tr><td>".$rowLicencias->idLicencia->nombre."</td>";
    $licencias.="<td>".$rowLicencias->fecha_asignacion."</td>";
    $licencias.="<td style='text-align:center;'>".$rowLicencias->cantidad."</td>";
    $licencias.="<td style='text-align:center;'>".$rowLicencias->utilizados."</td>";
    $licencias.="<td style='text-align:center;'>".($rowLicencias->cantidad - $rowLicencias->utilizados)."</td>";
}
$licencias.="</table>";
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_cliente',
		'nombre_rsocial',
		'direccion',
            /*array(
              'label'=>'Country',
                'value'=>$model->idPais->nombre
            ),
            array(
              'label'=>'City',
                'value'=>$model->idCiudad->nombre
            ),*/
		'nit_id_legal',
		'telefono',
		'contacto',
		'email',
            array(
                'name'=>'ruta_logo',
                'type'=>'raw',
                'value'=> $imagen,
            ),
            array('label'=>'Status','value'=>($model->estado=='A')?"Enabled":"Disabled"),
            array('name'=>'logo_en_certificado','value'=>($model->logo_en_certificado==0)?"NO":"YES"),
            array(
                'label'=>'',
                'type'=>'raw',
                'value'=> $licencias
            ),
	),
)); ?>

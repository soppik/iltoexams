<?php 
$tipo_perfil = $model->idPerfil->nombre;
$config = Yii::app()->getComponents(false);
$connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
// establish connection. You may try...catch possible exceptions
$connection->active=true;

$SQL = "SELECT nombre_rsocial as cliente FROM `clients` WHERE `id` = ".Yii::app()->user->getState('cliente')." ";
$list= $connection->createCommand($sql)->queryAll();

if($tipo_perfil == "TOAA"){
    $descripcion_perfil = '<p><h2><span color="#008a17">TOAA</span> (Total Access Administrator), This user is able to:</h2>
        <ul>
          <li>Manage user accounts (create, delete, modify),</li>
          <li>Allocate test licenses.</li>
          <li>Manage groups or test rooms (Create, delete, modify)</li>
          <li>Purchase test licenses</li>
          <li>Tutor access (Perform the speaking test and assign test licenses)</li>
          <li>Manage the test agenda (set and modify exam dates,approve or reject test takers)</li>
          </ul>
          </p>';
}
if($tipo_perfil == "PAA"){
    $descripcion_perfil = '<p>,h2><span color="#008a17">PAA</span> (Partial Access Administrator), This user is able to:</h2>
<ul>
       <li>Allocate test licenses.</li>
       <li>Manage groups or test rooms (Create, delete, modify).</li>
       <li>Tutor access (Perform the speaking test and assign test licenses).</li>
       <li>Manage the test agenda (set and modify exam dates, approve or reject test takers).</li>
          </ul>
          </p>';
}
if($tipo_perfil == "TUAA"){
    $descripcion_perfil = '<p><h2><span color="#008a17">TUAA</span> (Tutor Access Administrator), This user is able to:</h2>
<ul>
<li>Allocate test licenses.</li>
<li>See groups or test romos.</li>
<li>Tutor access (Perform the speaking test and assign test licenses).</li>
<li>See the test schedule.</li>
          </ul>
          </p>';
}
if($tipo_perfil == "RAA"){
    $descripcion_perfil = '<p><h2><span color="#008a17">RAA</span> (Tutor Access Administrator), This user is able to see the test reports.</h2>';
}
?>
<p>
   <h2>Hi <?php echo $model->nombres;?></h2>
</p>
<p>
You have a new account in www.iltoexams.com,  your account has been created as <span color="#008a17"><?php echo $tipo_perfil;?> for <?php echo $list['cliente']; ?></span>:
</p>
<p>
<?php echo $descripcion_perfil;?>
</p>
<p>
<h3> TO USE YOUR NEW ACCOUNT PLEASE FOLLOW THE STEPS BELLOW:</h3>
<br>
Click the following link <a href="http://www.iltoexams.com">http://www.iltoexams.com</a>. Once on the site, click the "TEST ADMINISTRATOR" link and use the following information:</p>
<table>
    <tr><td style="font-size:16px"><span style="color:#008a17; font-size:16px">Username:</span></td><td><?php echo $model->id_usuario_c;?></td></tr>
    <tr><td style="font-size:16px"><span style="color:#008a17; font-size:16px">Password:</span></td><td><?php echo $model->clave2;?></td></tr>
</table>
</p>
<p>
    Best regards,
</p>
<p>
    Support Team - ILTO<br>
    <a href="http://www.iltoexams.com/"><img style="float:left;" alt="ILTO Exams" src="logo_ILTO.png" /></a>
</p>


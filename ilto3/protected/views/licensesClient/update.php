<?php
/* @var $this LicensesClientController */
/* @var $model LicensesClient */

$this->breadcrumbs=array(
	'Licenses Clients'=>array('index'),
	$model->id_licencias_cliente=>array('view','id'=>$model->id_licencias_cliente),
	'Update',
);

$this->menu=array(
	array('label'=>'Assign new Licenses', 'url'=>array('create')),
	array('label'=>'Manage Licenses per Client', 'url'=>array('admin')),
);
?>

<h1>Update LicensesClient <?php echo $model->id_licencias_cliente; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
<?php

$config = Yii::app()->getComponents(false);
//$connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
$connection=new CDbConnection('mysql:host=localhost;dbname=iltoexam_newversion','iltoexam_newvers','Ilto.2015');
//$connection=new CDbConnection($config["db"]["connectionString"],$config["db"]["username"],$config["db"]["password"]);
$connection->active=true;


// consulto el ultimo ID del TECS y EDE
$modUtil = new Utilidades();
$id_tecs = $modUtil->getIdTecs();
$id_ede =  $modUtil->getIdEde();

/* @var $this LicensesClientController */
/* @var $model LicensesClient */

$this->breadcrumbs=array(
	'Licenses Clients'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Assign new License', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#licenses-client-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

//tecs by year
$SQL = "SELECT SUM(cantidad) FROM `licenses_client` WHERE (fecha_asignacion BETWEEN '2018-01-01' AND NOW()) AND id_cliente != '2030405060' AND id_licencia = '$id_tecs' AND demo = 0 LIMIT 1";
$tecsByYear = $connection->createCommand($SQL)->queryAll();

//ede by year
$SQL = "SELECT SUM(cantidad) FROM `licenses_client` WHERE (fecha_asignacion BETWEEN '2018-01-01' AND NOW()) AND id_cliente != '2030405060' AND id_licencia = '$id_ede' AND demo = 0 LIMIT 1";
$edeByYear = $connection->createCommand($SQL)->queryAll();

$otherByYear = $licByYear[0]["SUM(cantidad)"] - ($tecsByYear[0]["SUM(cantidad)"] + $edeByYear[0]["SUM(cantidad)"]);
// last Month
$currentMonth = date('n');

//licenses last Month
$SQL = "SELECT id_cliente, nombre_rsocial FROM `clients`";
$listClients = $connection->createCommand($SQL)->queryAll();


if(isset($_GET["id"])){
	$SQL = "SELECT licenses_client.id_cliente, licenses_client.cantidad, licenses_client.utilizados, licenses_client.estado, nombre, nombre_rsocial, fecha_asignacion, fecha_final  FROM `licenses_client` 
	INNER JOIN clients
	ON licenses_client.id_cliente = clients.id_cliente 
	INNER JOIN licenses
	ON licenses_client.id_licencia = licenses.id_licencia
	WHERE demo = 0
	AND licenses_client.id_cliente = '".$_GET["id"]."'
	ORDER BY fecha_final DESC, nombre DESC ";
}else{
	$SQL = "SELECT licenses_client.id_cliente, licenses_client.cantidad, licenses_client.utilizados, licenses_client.estado, nombre, nombre_rsocial, fecha_asignacion, fecha_final  FROM `licenses_client` 
	INNER JOIN clients
	ON licenses_client.id_cliente = clients.id_cliente 
	INNER JOIN licenses
	ON licenses_client.id_licencia = licenses.id_licencia
	WHERE licenses_client.estado = 'A'
	AND demo = 0
	ORDER BY nombre";
}


//
$clients = $connection->createCommand($SQL)->queryAll();


//summary licences by client GET
$SQL = "SELECT SUM(licenses_client.cantidad), nombre  FROM `licenses_client` 
INNER JOIN clients
ON licenses_client.id_cliente = clients.id_cliente 
INNER JOIN licenses
ON licenses_client.id_licencia = licenses.id_licencia
WHERE licenses_client.id_cliente = '".$_GET["id"]."'
AND demo = 0
GROUP BY nombre
ORDER BY fecha_asignacion DESC";
$totaLic= $connection->createCommand($SQL)->queryAll();


$lastM = $licLastM[0]["SUM(cantidad)"];
if(empty($licLastM[0]["SUM(cantidad)"]) || is_null($licLastM[0]["SUM(cantidad)"])){
	$lastM = 0;
}

$tecsLastM = $tecsLastM[0]["SUM(cantidad)"];
if(empty($tecsLastM[0]["SUM(cantidad)"]) || is_null($tecsLastM[0]["SUM(cantidad)"])){
	$tecsLastM  = 0;
}

$edeLastM = $edeLastM[0]["SUM(cantidad)"];
if(empty($edeLastM[0]["SUM(cantidad)"]) || is_null($edeLastM[0]["SUM(cantidad)"])){
	$edeLastM = 0;
}



$otherLastM = $lastM - ($tecsLastM + $edeLastM);

?>
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

th {
	color: white;
    background: url(bg.gif) repeat-x scroll left top white;
    text-align: center;
    background-color:#4d9ac6;
    border:1px solid white;
}

th a {
    color: #EEE;
    font-weight: bold;
    text-decoration: none;
    text-align:center;
}

td, th {
   	text-align: left;
    padding: 8px;
}

td {
	    font-size: 0.9em;
    border: 1px white solid;
    padding: 0.3em;
}

tr:nth-child(even) {
    background: #E5F1F4;
}

body {
	background-image:none !important;
}
.datepicker{top:0;left:0;padding:4px;margin-top:1px;z-index:9999 !important}.datepicker:before{content:'';display:inline-block;border-left:7px solid transparent;border-right:7px solid transparent;border-bottom:7px solid #ccc;border-bottom-color:rgba(0,0,0,.2);position:absolute;top:-7px;left:6px}.datepicker:after{content:'';display:inline-block;border-left:6px solid transparent;border-right:6px solid transparent;border-bottom:6px solid #fff;position:absolute;top:-6px;left:7px}.datepicker>div{display:none}.datepicker table{width:100%;margin:0}.datepicker td,.datepicker th{text-align:center;font-size:12px;height:20px;width:20px}.datepicker td.day:hover{background:#eee;cursor:pointer}.datepicker td.day.disabled{color:#eee}.datepicker td.old,.datepicker td.new{color:#999}.datepicker td.active,.datepicker td.active:hover{background-color:#2dc3e8;color:#fff;-webkit-text-shadow:0 -1px 0 rgba(0,0,0,.25);text-shadow:0 -1px 0 rgba(0,0,0,.25)}.datepicker td span{display:block;width:47px;height:54px;line-height:54px;float:left;margin:2px;cursor:pointer}.datepicker td span:hover{background:#eee}.datepicker td span.active{background-color:#2dc3e8;color:#fff;-webkit-text-shadow:0 -1px 0 rgba(0,0,0,.25);text-shadow:0 -1px 0 rgba(0,0,0,.25)}.datepicker td span.old{color:#999}.datepicker th.switch{width:175px}.datepicker th.next,.datepicker th.prev{font-size:12px}.datepicker thead tr:first-child th{cursor:pointer}.datepicker thead tr:first-child th:hover{background:#eee}.input-append.date .add-on i,.input-prepend.date .add-on i{display:block;cursor:pointer;width:16px;height:16px}.daterangepicker{position:absolute;background:#fff;top:100px;left:20px;padding:4px;margin-top:1px;font-size:12px;width:278px}.daterangepicker.opensleft:before{position:absolute;top:-7px;right:9px;display:inline-block;border-right:7px solid transparent;border-bottom:7px solid #ccc;border-left:7px solid transparent;border-bottom-color:rgba(0,0,0,.2);content:''}.daterangepicker.opensleft:after{position:absolute;top:-6px;right:10px;display:inline-block;border-right:6px solid transparent;border-bottom:6px solid #fff;border-left:6px solid transparent;content:''}.daterangepicker.openscenter:before{position:absolute;top:-7px;left:0;right:0;width:0;margin-left:auto;margin-right:auto;display:inline-block;border-right:7px solid transparent;border-bottom:7px solid #ccc;border-left:7px solid transparent;border-bottom-color:rgba(0,0,0,.2);content:''}.daterangepicker.openscenter:after{position:absolute;top:-6px;left:0;right:0;width:0;margin-left:auto;margin-right:auto;display:inline-block;border-right:6px solid transparent;border-bottom:6px solid #fff;border-left:6px solid transparent;content:''}.daterangepicker.opensright:before{position:absolute;top:-7px;left:9px;display:inline-block;border-right:7px solid transparent;border-bottom:7px solid #ccc;border-left:7px solid transparent;border-bottom-color:rgba(0,0,0,.2);content:''}.daterangepicker.opensright:after{position:absolute;top:-6px;left:10px;display:inline-block;border-right:6px solid transparent;border-bottom:6px solid #fff;border-left:6px solid transparent;content:''}.daterangepicker.dropup{margin-top:-5px}.daterangepicker.dropup:before{top:initial;bottom:-7px;border-bottom:initial;border-top:7px solid #ccc}.daterangepicker.dropup:after{top:initial;bottom:-6px;border-bottom:initial;border-top:6px solid #fff}.daterangepicker.dropdown-menu{max-width:none;z-index:3000}.daterangepicker.single .ranges,.daterangepicker.single .calendar{float:none}.daterangepicker .calendar{display:none;max-width:270px;margin:4px}.daterangepicker.show-calendar .calendar{display:block}.daterangepicker .calendar.single .calendar-table{border:none}.daterangepicker .calendar th,.daterangepicker .calendar td{white-space:nowrap;text-align:center;min-width:32px}.daterangepicker .calendar-table{border:1px solid #ddd;padding:4px;border-radius:4px;background:#fff}.daterangepicker table{width:100%;margin:0}.daterangepicker td,.daterangepicker th{text-align:center;width:20px;height:20px;white-space:nowrap;cursor:pointer}.daterangepicker td.off,.daterangepicker td.off.in-range,.daterangepicker td.off.start-date,.daterangepicker td.off.end-date{color:#999;background:#fff}.daterangepicker td.disabled,.daterangepicker option.disabled{color:#999;cursor:not-allowed;text-decoration:line-through}.daterangepicker td.available:hover,.daterangepicker th.available:hover{background:#eee}.daterangepicker td.in-range{background:#d0e7fc;-webkit-border-radius:0;-webkit-background-clip:padding-box;-moz-border-radius:0;-moz-background-clip:padding;border-radius:0;background-clip:padding-box}.daterangepicker td.start-date{border-radius:4px 0 0 4px}.daterangepicker td.end-date{border-radius:0 4px 4px 0}.daterangepicker td.start-date.end-date{border-radius:4px}.daterangepicker td.active,.daterangepicker td.active:hover{background-color:#2dc3e8;border-color:#2dc3e8;color:#fff}.daterangepicker td.week,.daterangepicker th.week{font-size:80%;color:#ccc}.daterangepicker select.monthselect,.daterangepicker select.yearselect{font-size:12px;padding:1px;height:auto;margin:0;cursor:default}.daterangepicker select.monthselect{margin-right:2%;width:56%}.daterangepicker select.yearselect{width:40%}.daterangepicker select.hourselect,.daterangepicker select.minuteselect,.daterangepicker select.secondselect,.daterangepicker select.ampmselect{width:50px;margin-bottom:0}.daterangepicker th.month{width:auto}.daterangepicker .input-mini{border:1px solid #ccc;border-radius:4px;color:#555;display:block;height:30px;line-height:30px;vertical-align:middle;margin:0 0 5px 0;padding:0 6px 0 28px;width:100%}.daterangepicker .input-mini.active{border:1px solid #357ebd}.daterangepicker .daterangepicker_input i{position:absolute;left:8px;top:8px}.daterangepicker .daterangepicker_input{position:relative}.daterangepicker .calendar-time{text-align:center;margin:5px auto;line-height:30px;position:relative;padding-left:28px}.daterangepicker .calendar-time select.disabled{color:#ccc;cursor:not

</style>

<div class="row" style="padding:20px;">
	<h1>FILTER REPORT BY DATE</h1> 
	<div id="clients-grid" class="grid-view">
	<table class="tbl_admin" >
		<tbody>
			<tr >
				<td style="border:0px;">
			    	
			    </td>
			    <td style="border:0px;">
			    	
			    </td>
			    <td style="border:0px;">
			    	<label>Client</label></br>
			    	<select id="id_client" style="max-width:250px;">
			    		<option value=""></option>
			    		<?php
							foreach($listClients as $cl){
								echo '<option value="'.$cl["id_cliente"].'">'.utf8_encode($cl["nombre_rsocial"]).'</option>';
							}
						?>	
			    	</select>
			    </td>
			     <td style="border:0px;">
			     	<label>Status</label></br>
			    	<select id="status" name="status" style="max-width:250px;">
			    		<option value=""></option>
			    		<option value="A">Available</option>
			    		<option value="F">Finished</option>
			    		<option value="E">Expired</option>
			    	</select>
			    </td>
			    <td style="border:0px;">
			    	<div class="input-group"style="border:0px;">
						<input name="btnSearch" id="btnSearch" class="btn btn-warning btn-xs edit" type="submit" value="Search">
					</div>
			    </td>
		</tbody>
	</table>
	</div>

<div id="clients-grid" class="grid-view">
	<?php echo '<h1>'.utf8_encode($clients[0]['nombre_rsocial']).'</h1>';?>
	<table class="tbl_admin" id="month">
		<thead>
			<tr>
			    <th>
			    	License
			    </th>
			    <th>
			    	Quantity
			    </th>
			    <th>
			    	Used
			    </th>
			    <th>
			    	Available
			    </th>
			    <th>
			    	Status
			    </th>
			    <th>
			    	Allocation date
			    </th>
			    <th>
			    	Available before at
			    </th>
			    
		   	</tr>
		</thead>
		<tbody >
		<?php
			
			foreach($clients as $client){
				switch ($client['estado']) {
	                case 'A':
	                    $status = '<span style="font-weight:bold;color:#39b54a;">AVAILABLE</span>';
	                    break;
	                 case 'F':
	                    $status = '<span style="font-weight:bold;color:black;">FINISHED</span>';
	                    break;
	                 case 'E':
	                    $status = '<span style="font-weight:bold;color:red;">EXPIRED</span>';
	                    break;
	                default:
	                    echo "There is not records for total.";
	            }
				echo	'
						 <tr>
			            	<td>'.$client['nombre'].'</td>
			                <td>'.$client['cantidad'].'</td>
			                <td>'.$client['utilizados'].'</td>
			                <td>'.($client['cantidad'] -$client['utilizados']) .'</td>
			                <td>'.$status.'</td>
			                <td>'.substr($client['fecha_asignacion'],0,10).'</td>
			                <td>'.substr($client['fecha_final'],0,10).'</td>
		                 </tr>';	
			}
			?>
		</tbody>
	</table>
</div>

<!--Page Related Scripts-->
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/jquery.dataTables.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/ZeroClipboard.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.tableTools.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/dataTables.bootstrap.min.js"></script>
    <script src="/ilto3/themes/ilto/html/assets/js/datatable/datatables-init.js"></script>
    <!--Bootstrap Date Picker-->
    <script src="/ilto3/themes/ilto/html/assets/js/datetime/bootstrap-datepicker.js"></script>
    
    
    <script>
        InitiateSimpleDataTable.init();
        InitiateExpandableDataTable.init();
        InitiateSearchableDataTable.init();
       
    </script>
    <script>
    
            var startDate = new Date();
			var endDate = new Date();
			$('#dp4').datepicker({
				format: 'yyyy-mm-dd'
			});
			$('#dp5').datepicker({
				format: 'yyyy-mm-dd'
			});
			
     $('#dp4').datepicker()
				.on('changeDate', function(ev){
					if (ev.date.valueOf() > endDate.valueOf()){
						$('#alert').show().find('strong').text('The final date cannot be previous to  the starting date.');
					} else {
						$('#alert').hide();
						startDate = new Date(ev.date);
						$('#startDate').text($('#dp4').data('date'));
					}
					$('#dp4').datepicker('hide');
				});
			$('#dp5').datepicker()
				.on('changeDate', function(ev){
					if (ev.date.valueOf() < startDate.valueOf()){
						$('#alert').show().find('strong').text('The final date cannot be previous to  the starting date.');
					} else {
						$('#alert').hide();
						endDate = new Date(ev.date);
						$('#endDate').text($('#dp5').data('date'));
					}
					$('#dp5').datepicker('hide');
				});
				$(function(){
											  $('#calificacion').keypress(function(e){
											    if(e.which == 101){
											    	return false;
											    }
											  });
											});
	
	$("#btnSearch").click(function() {
		var from= $("#dp4").val();
		var to= $("#dp5").val();
		var client = $('#id_client').find(":selected").val();
		
		var status = $('#status').find(":selected").val();
		$.ajax({
			url : "/ilto3/index.php?r=Api/getSalesbyDate",
			type: "POST",
			dataType: "text",
			data: {
			    from: from,
			    to: to,
			    status: status,
			    client: client
			},
			success: function(data) {
				$("#month").html(data);
			}
		});
	});										
    </script>

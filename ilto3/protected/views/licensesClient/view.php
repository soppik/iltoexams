<?php
/* @var $this LicensesClientController */
/* @var $model LicensesClient */

$this->breadcrumbs=array(
	'Licenses Clients'=>array('index'),
	$model->id_licencias_cliente,
);

$this->menu=array(
	array('label'=>'List LicensesClient', 'url'=>array('index')),
	array('label'=>'Create LicensesClient', 'url'=>array('create')),
	array('label'=>'Update LicensesClient', 'url'=>array('update', 'id'=>$model->id_licencias_cliente)),
	array('label'=>'Delete LicensesClient', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_licencias_cliente),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage LicensesClient', 'url'=>array('admin')),
);
?>

<h1>View LicensesClient #<?php echo $model->id_licencias_cliente; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_licencias_cliente',
		'id_cliente',
		'id_licencia',
		'cantidad',
		'utilizados',
		'demo',
		'fecha_inicial',
		'fecha_final',
		'estado',
	),
)); ?>

<?php
/* @var $this LicensesClientController */
/* @var $model LicensesClient */

$this->breadcrumbs=array(
	'Licenses Clients'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Licenses per Client', 'url'=>array('admin')),
);
?>

<h1>Create LicensesClient</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
/* @var $this LicensesClientController */
/* @var $model LicensesClient */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'licenses-client-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_cliente'); ?>
		<?php
            $dataCombo=CHtml::listData(Clients::model()->findAllByAttributes(array('estado'=>'A')),'id_cliente','nombre_rsocial');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'LicensesClient[id_cliente]',
            'id'=> 'combo1',
            'data'=>$dataCombo,
                'value'=>$model->id_cliente,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'220px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'id_cliente'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_licencia'); ?>
		<?php
            $dataCombo=CHtml::listData(Licenses::model()->findAllByAttributes(array('estado'=>'A')),'id_licencia','nombre');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'LicensesClient[id_licencia]',
            'id'=> 'combo_licencias',
            'data'=>$dataCombo,
                'value'=>$model->id_licencia,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'220px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'id_licencia'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cantidad'); ?>
		<?php echo $form->numberField($model,'cantidad', array('required'=>'required')); ?>
		<?php echo $form->error($model,'cantidad'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'demo'); ?>
		<?php echo $form->checkBox($model,'demo'); ?>
		<?php echo $form->error($model,'demo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_final'); ?>
	<?php 
            echo CHtml::image(Yii::app()->request->baseUrl.'/images/iconos/calendar.png','Calendar',array('style'=>'height:20px; vertical-align:top;opacity: 0.4; filter: alpha(opacity=40);'));
        echo $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'name'=>'LicensesClient[fecha_final]',
            'value'=>$model->fecha_final,
                        // 'i18nScriptFile' => 'jquery.ui.datepicker-ja.js', (#2)
                        'htmlOptions' => array(
                            'id' => 'LicensesClient_fecha_final',
                                'size' => '12',
                                'style'=>'width:185px;'
                        ),
                        'options' => array(  // (#3)
                            'showOn' => 'focus', 
                            'dateFormat' => 'yy-mm-dd',
                            'showOtherMonths' => true,
                            'selectOtherMonths' => true,
                            'changeMonth' => true,
                            'changeYear' => true,
                            'showButtonPanel' => false,
                        )
                    ), 
                    true);
                    ?>
		<?php echo $form->error($model,'fecha_final'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estado'); ?>
		<?php
            $dataCombo=array('A'=>'Enabled','I'=>'Disabled');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'LicensesClient[estado]',
            'id'=> 'combo2',
            'data'=>$dataCombo,
                'value'=>$model->estado,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'220px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'estado'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php
/* @var $this SectionsController */
/* @var $model Sections */
/* @var $form CActiveForm */
$modUtil = new Utilidades;
if($model->isNewRecord){
    $model->intro="Type here the Section Intro...";
}
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'sections-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bg_color'); ?>
		<?php
            $dataCombo=$modUtil->arregloColores();
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'Sections[bg_color]',
            'id'=> 'combo_bg',
            'data'=>$dataCombo,
                'value'=>$model->bg_color,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'220px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'bg_color'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'txt_color'); ?>
		<?php
            $dataCombo=$modUtil->arregloColores();
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'Sections[txt_color]',
            'id'=> 'combo_txt',
            'data'=>$dataCombo,
                'value'=>$model->txt_color,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'220px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'txt_color'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'intro'); ?>
		<?php echo $form->textArea($model,'intro',array('rows'=>6, 'cols'=>30)); ?>
		<?php echo $form->error($model,'intro'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'estado'); ?>
		<?php
            $dataCombo=array('A'=>'Enabled','I'=>'Disabled');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'Sections[estado]',
            'id'=> 'combo_estado',
            'data'=>$dataCombo,
                'value'=>$model->estado,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'220px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'estado'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>
<?php $this->widget('application.extensions.tinymce.SladekTinyMce'); ?>
 
<script>
        tinymce.init({selector:'#Sections_intro',theme: "modern",
    width: 900,
    height: 300});
</script>
<?php $this->endWidget(); ?>

</div><!-- form -->
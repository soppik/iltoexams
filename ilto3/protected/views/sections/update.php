<?php
/* @var $this SectionsController */
/* @var $model Sections */

$this->breadcrumbs=array(
	'Sections'=>array('index'),
	$model->id_seccion=>array('view','id'=>$model->id_seccion),
	'Update',
);

$this->menu=array(
	array('label'=>'Create Sections', 'url'=>array('create')),
	array('label'=>'Manage Sections', 'url'=>array('admin')),
);
?>

<h1>Update Sections <?php echo $model->id_seccion; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
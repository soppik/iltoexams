<?php
/* @var $this LicensesUserController */
/* @var $model LicensesUser */
$this->layout = 'column1';
$this->breadcrumbs=array(
	'Licenses Users'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create LicensesUser', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#licenses-user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Certificates</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'licenses-user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
            array(
                'name'=>'id_licencia_cliente',
                'value'=>'$data->idLicenciaCliente->idLicencia->nombre',
                'filter'=>CHtml::listData(Licenses::model()->findAll('genera_certificado=1'),"id_licencia","nombre"),
                'header'=> 'License'
                ),
            array(
                'name'=>'cliente',
                'value'=>'$data->idLicenciaCliente->idCliente->nombre_rsocial',
                'filter'=>CHtml::listData(Clients::model()->findAll(),"id_cliente","nombre_rsocial"),
            ),
		'id_usuario',
            array(
                'name'=>'nombre_usuario',
                'value'=>'$data->idUsuario->nombres'
            ),
            array(
                'name'=>'apellido_usuario',
                'value'=>'$data->idUsuario->apellidos'
            ),
            
		'fecha_asignacion',
		'fecha_presentacion',
		/*
		'hora',
		'calificacion',
		'nivel',
		*/
		array(
			'class'=>'CButtonColumn',
                    'template'=>'{certificate}',
                    'buttons'=>array(
                        'certificate'=>array(
                            'label'=>'Certificate',
                            'visible'=>'($data->estado=="F") ? true : false',
                            'url'=>'Yii::app()->createUrl("LicensesUser/certificate",array("id"=>$data->id_licencias_usuario))',
                            'options'=>array("target"=>"_blank"),
                        )
                        
                    )
		),
	),
)); ?>

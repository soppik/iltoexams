<?php

/**
 * This is the model class for table "licenses_user".
 *
 * The followings are the available columns in table 'licenses_user':
 * @property integer $id_licencias_usuario
 * @property integer $id_licencia_cliente
 * @property string $id_usuario
 * @property string $fecha_asignacion
 * @property string $estado
 * @property string $fecha_presentacion
 * @property string $hora
 * @property double $calificacion
 * @property string $nivel
 *
 * The followings are the available model relations:
 * @property ExamsUser[] $examsUsers
 * @property LicensesClient $idLicenciaCliente
 * @property UsersC $idUsuario
 */
class LicensesUser extends CActiveRecord
{
    public $nombre_usuario;
    public $apellido_usuario;
    public $cliente;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'licenses_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_licencia_cliente, id_usuario, fecha_asignacion, estado, calificacion, nivel', 'required'),
			array('id_licencia_cliente', 'numerical', 'integerOnly'=>true),
			array('calificacion', 'numerical'),
			array('id_usuario', 'length', 'max'=>20),
			array('estado', 'length', 'max'=>1),
			array('hora, nivel', 'length', 'max'=>4),
			array('fecha_presentacion', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_licencias_usuario, cliente,  nombre_usuario, apellido_usuario, id_licencia_cliente, id_usuario, fecha_asignacion, estado, fecha_presentacion, hora, calificacion, nivel', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'examsUsers' => array(self::HAS_MANY, 'ExamsUser', 'id_licencia_usuario'),
			'idLicenciaCliente' => array(self::BELONGS_TO, 'LicensesClient', 'id_licencia_cliente'),
			'idUsuario' => array(self::BELONGS_TO, 'UsersC', 'id_usuario'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_licencias_usuario' => 'Id',
			'id_licencia_cliente' => 'Client License',
			'id_usuario' => 'User',
			'fecha_asignacion' => 'Assignation Date',
			'estado' => 'Status',
			'fecha_presentacion' => 'Exam Date',
			'hora' => 'Hora',
			'calificacion' => 'Calificacion',
			'nivel' => 'Nivel',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
        $criteria->with = array("idUsuario", "idLicenciaCliente.idLicencia", "idLicenciaCliente.idCliente");
		$criteria->compare('idCliente.id_cliente',$this->cliente,true);
		$criteria->compare('idLicencia.id_licencia',$this->id_licencia_cliente);
		$criteria->compare('id_usuario',$this->id_usuario,true);
		$criteria->compare('fecha_asignacion',$this->fecha_asignacion,true);
		$criteria->compare('estado',$this->estado,true);
		$criteria->compare('fecha_presentacion',$this->fecha_presentacion,true);
		$criteria->compare('hora',$this->hora,true);
		$criteria->compare('calificacion',$this->calificacion);
		$criteria->compare('nivel',$this->nivel,true);
		$criteria->compare('idUsuario.nombres',$this->nombre_usuario,true);
		$criteria->compare('idUsuario.apellidos',$this->apellido_usuario,true);
                

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'pagination'=>array('pageSize'=>50,),

		));
	}
        
        public function defaultScope()
        {
            return array( 
                'with'=>'idLicenciaCliente.idLicencia',
                'condition'=>"idLicencia.genera_certificado = 1",                    
            );
        }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LicensesUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

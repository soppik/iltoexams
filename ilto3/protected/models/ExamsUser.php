<?php

/**
 * This is the model class for table "exams_user".
 *
 * The followings are the available columns in table 'exams_user':
 * @property integer $id_examen_usuario
 * @property integer $id_licencia_usuario
 * @property integer $id_examen
 * @property string $fecha_presentacion
 * @property double $calificacion
 * @property integer $nivel
 * @property string $estado
 * @property string $hora
 * @property string $tutor
 * @property integer $salon
 *
 * The followings are the available model relations:
 * @property AnswersExamUser[] $answersExamUsers
 * @property LicensesUser $idLicenciaUsuario
 * @property Exams $idExamen
 * @property UsersC $tutor0
 * @property ClassRooms $salon0
 */
class ExamsUser extends CActiveRecord
{
    public $ruta_foto;
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'exams_user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id_licencia_usuario, id_examen, fecha_presentacion, estado, hora, salon', 'required'),
            array('id_licencia_usuario, id_examen, salon', 'numerical', 'integerOnly'=>true),
	array('ruta_foto', 'length', 'max'=>250),
            array('calificacion', 'numerical'),
            array('estado', 'length', 'max'=>1),
            array('nivel', 'length', 'max'=>4),
            array('hora', 'length', 'max'=>5),
            array('tutor', 'length', 'max'=>20),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_examen_usuario, id_licencia_usuario, id_examen, fecha_presentacion, calificacion, nivel, estado, hora, salon', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'answersExamUsers' => array(self::HAS_MANY, 'AnswersExamUser', 'id_examen_usuario'),
            'idLicenciaUsuario' => array(self::BELONGS_TO, 'LicensesUser', 'id_licencia_usuario'),
            'idExamen' => array(self::BELONGS_TO, 'Exams', 'id_examen'),
            'tutor0' => array(self::BELONGS_TO, 'UsersC', 'tutor'),
            'salon0' => array(self::BELONGS_TO, 'ClassRooms', 'salon'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id_examen_usuario' => 'Id',
            'id_licencia_usuario' => 'License',
            'id_examen' => 'Test',
            'fecha_presentacion' => 'Test Date',
            'calificacion' => 'Score',
            'nivel' => 'CEFR Level',
            'estado' => 'Status',
            'hora' => 'Time',
            'ruta_foto' => 'Picture',
            'tutor' => 'Tutor',
            'salon' => 'Classroom',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id_examen_usuario',$this->id_examen_usuario);
        $criteria->compare('id_licencia_usuario',$this->id_licencia_usuario);
        $criteria->compare('id_examen',$this->id_examen);
        $criteria->compare('fecha_presentacion',$this->fecha_presentacion,true);
        $criteria->compare('calificacion',$this->calificacion);
        $criteria->compare('nivel',$this->nivel, true);
        $criteria->compare('estado',$this->estado,true);
        $criteria->compare('hora',$this->hora,true);
        $criteria->compare('tutor',$this->tutor,true);
        $criteria->compare('salon',$this->salon);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
                    'pagination'=>array('pageSize'=>50,),

        ));
    }
    
    public function scopes(){
        return array(
            'esteCliente'=>array(
                'with'=>'idLicenciaUsuario.',
                'condition'=>"idLicenciaUsuario.idLicenciaCliente.id_cliente = '".Yii::app()->user->getState('cliente')."'",
            ),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ExamsUser the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
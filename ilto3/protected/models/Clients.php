<?php

/**
 * This is the model class for table "clients".
 *
 * The followings are the available columns in table 'clients':
 * @property string $id_cliente
 * @property string $nombre_rsocial
 * @property string $direccion
 * @property integer $id_pais
 * @property integer $id_ciudad
 * @property string $nit_id_legal
 * @property string $telefono
 * @property string $contacto
 * @property string $email
 * @property string $ruta_logo
 * @property string $estado
 * @property integer $logo_en_certificado
 *
 * The followings are the available model relations:
 * @property Cities $idCiudad
 * @property Countries $idPais
 * @property LicensesClient[] $licensesClients
 * @property UsersC[] $usersCs
 */
class Clients extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'clients';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_cliente,examen_asistido, is_distributor, nombre_rsocial, direccion, id_country, id_ciudad, nit_id_legal, telefono, contacto, estado, logo_en_certificado', 'required'),
			array('id_ciudad, logo_en_certificado', 'numerical', 'integerOnly'=>true),
			array('id_country, id_cliente, id_distributor, nit_id_legal', 'length', 'max'=>20),
                        array('ruta_logo', 'file', 'allowEmpty'=>true, 'types'=>'jpg, gif, png'),
			array('nombre_rsocial, direccion, contacto', 'length', 'max'=>200),
			array('telefono', 'length', 'max'=>30),
			array('ruta_logo', 'length', 'max'=>250),
			array('estado', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_cliente, nombre_rsocial, direccion, id_country, id_ciudad, nit_id_legal, telefono, contacto, ruta_logo, estado, logo_en_certificado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'classRooms' => array(self::HAS_MANY, 'ClassRooms', 'id_cliente'),
			'idCiudad' => array(self::BELONGS_TO, 'City', 'ID'),
			'idPais' => array(self::BELONGS_TO, 'Countries', 'id_country'),
			'idDistributor' => array(self::BELONGS_TO, 'Clients', 'id_distributor'),
			'clients' => array(self::HAS_MANY, 'Clients', 'id_distributor'),
			'groupsUsersClients' => array(self::HAS_MANY, 'GroupsUsersClients', 'id_cliente'),
			'licensesClients' => array(self::HAS_MANY, 'LicensesClient', 'id_cliente'),
			'usersCs' => array(self::HAS_MANY, 'UsersC', 'id_cliente'),);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_cliente' => 'Id',
			'nombre_rsocial' => 'Company Name',
			'direccion' => 'Address',
			'id_country' => 'Code',
			'id_ciudad' => 'City',
			'nit_id_legal' => 'NIT // EID',
			'telefono' => 'Phone',
			'contacto' => 'Contact Name',
			'ruta_logo' => 'Logo',
			'estado' => 'Status',
                    'examen_asistido'=> 'Test Helper',
                    'is_distributor'=> 'Distributor?', 
                    'id_distributor'=> 'Master Distributor',
			'logo_en_certificado' => 'Include Logo in Certificate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_cliente',$this->id_cliente,true);
		$criteria->compare('nombre_rsocial',$this->nombre_rsocial,true);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('Code',$this->id_country);
		$criteria->compare('ID',$this->id_ciudad);
		$criteria->compare('nit_id_legal',$this->nit_id_legal,true);
		$criteria->compare('telefono',$this->telefono,true);
		$criteria->compare('contacto',$this->contacto,true);
		$criteria->compare('ruta_logo',$this->ruta_logo,true);
		$criteria->compare('estado',$this->estado,true);
		$criteria->compare('id_distributor',$this->id_distributor,true);
		$criteria->compare('logo_en_certificado',$this->logo_en_certificado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'pagination'=>array('pageSize'=>50,),

		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Clients the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

        public function scopes(){
            return array(
                'distributors'=>array(
                    'condition'=>"is_distributor = 1",
                ),
            );
        }

        
        
        }

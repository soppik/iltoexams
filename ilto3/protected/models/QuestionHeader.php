<?php

/**
 * This is the model class for table "questions_exam".
 *
 * The followings are the available columns in table 'questions_exam':
 * @property integer $id_preguntas_examen
 * @property integer $id_examen
 * @property integer $id_pregunta
 *
 * The followings are the available model relations:
 * @property Exams $idExamen
 */
class QuestionHeader extends CActiveRecord
{
    public $level;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'question_header';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_question_heaader, description', 'required'),
			array('id_question_header', 'numerical', 'integerOnly'=>true),
		);
	}

	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_question_header' => 'Id',
			'description' => 'text',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                $criteria->with="idPregunta";
		$criteria->compare('id_question_header',$this->id_question_header);
		$criteria->compare('description',$this->description);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'pagination'=>array('pageSize'=>50,),

		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QuestionsExam the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

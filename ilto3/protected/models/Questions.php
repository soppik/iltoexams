<?php

/**
 * This is the model class for table "questions".
 *
 * The followings are the available columns in table 'questions':
 * @property integer $id_pregunta
 * @property integer $nivel
 * @property string $tipo_presentacion
 * @property string $texto
 * @property integer $id_seccion
 * @property string $ruta_imagen
 * @property string $ruta_video
 * @property string $ruta_audio
 * @property string $estado
 *
 * The followings are the available model relations:
 * @property Answers[] $answers
 * @property AnswersExamUser[] $answersExamUsers
 * @property Sections $idSeccion
 */
class Questions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'questions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nivel, tipo_presentacion, time, id_seccion,template, estado', 'required'),
			array('id_seccion, time, template, relacionada, id_question_header', 'numerical', 'integerOnly'=>true),
			array('tipo_presentacion, estado', 'length', 'max'=>1),
			array('ruta_video, titulo_respuesta, id_question_header', 'length', 'max'=>250),
                        array('ruta_imagen', 'file', 'allowEmpty'=>true, 'types'=>'jpg, gif, png'),
                        array('ruta_audio', 'file', 'allowEmpty'=>true, 'types'=>'mp3, mp4, ogg'),
			array('texto', 'safe'), 
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_pregunta, nivel, tipo_presentacion, texto, id_seccion, ruta_imagen, ruta_video, ruta_audio, estado, id_question_header', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'answers' => array(self::HAS_MANY, 'Answers', 'id_pregunta'),
			'answersExamUsers' => array(self::HAS_MANY, 'AnswersExamUser', 'id_pregunta'),
			'idSeccion' => array(self::BELONGS_TO, 'Sections', 'id_seccion'),
			'idQuestionHeadder' => array(self::BELONGS_TO, 'question_header', 'id_question_header'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_pregunta' => 'Id',
			'nivel' => 'Level',
			'tipo_presentacion' => 'Show as',
			'texto' => 'Text',
			'id_seccion' => 'Section',
			'ruta_imagen' => 'Image',
			'ruta_video' => 'Video',
			'time' => 'Seconds to answer this Question',
			'ruta_audio' => 'Audio',
			'titulo_respuesta' => 'Answers Title',
			'template' => 'Template',
			'estado' => 'Status',
                    'relacionada'=>'Related Question',
            'id_question_header' => 'Header',
                   
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_pregunta',$this->id_pregunta);
		$criteria->compare('nivel',$this->nivel);
		$criteria->compare('tipo_presentacion',$this->tipo_presentacion,true);
		$criteria->compare('texto',$this->texto,true);
		$criteria->compare('id_seccion',$this->id_seccion);
		$criteria->compare('ruta_imagen',$this->ruta_imagen,true);
		$criteria->compare('ruta_video',$this->ruta_video,true);
		$criteria->compare('ruta_audio',$this->ruta_audio,true);
		$criteria->compare('template',$this->template,true);
		$criteria->compare('estado',$this->estado,true);
		$criteria->compare('id_question_header',$this->id_question_header,true);
                $criteria->order = "id_pregunta desc";

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'pagination'=>array('pageSize'=>50,),
                    

		));
	}

	public function search_exam($exam)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                
		$criteria->compare('id_pregunta',$this->id_pregunta);
		$criteria->compare('nivel',$this->nivel);
		$criteria->compare('tipo_presentacion',$this->tipo_presentacion,true);
		$criteria->compare('texto',$this->texto,true);
		$criteria->compare('id_seccion',$this->id_seccion);
		$criteria->compare('ruta_imagen',$this->ruta_imagen,true);
		$criteria->compare('ruta_video',$this->ruta_video,true);
		$criteria->compare('ruta_audio',$this->ruta_audio,true);
		$criteria->compare('template',$this->template,true);
		$criteria->compare('estado',$this->estado,true);
		$criteria->compare('relacionada',$this->relacionada,true);
		$criteria->compare('id_question_header',$this->id_question_header,true);
                //Limitar las preguntas unicamente a la seccion del Examen
                $modExam = Exams::model()->findByPk($exam);
                $criteria->addCondition("id_seccion = ".$modExam->id_seccion);
                //Excluir las preguntas que ya fueron seleccionadas en este examen
                $preguntasSeleccionadas = QuestionsExam::model()->findAllByAttributes(array('id_examen'=>$exam));
                $arregloPreguntas = array();
                foreach($preguntasSeleccionadas as $pregunta){
                    array_push($arregloPreguntas, "'".$pregunta->id_pregunta."'");
                }
                if(count($arregloPreguntas)>0){
        		$criteria->addCondition("id_pregunta not in (".implode(",",$arregloPreguntas).")");
                }
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>array('pageSize'=>50),

		));
	}
        /*eduardo romero
        eromero_hotmail.com*/
                
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Questions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getNombrePresenta(){
            $modUtil = new Utilidades();
            $arreglo = $modUtil->arregloPresentaciones();
            echo $arreglo[$this->tipo_presentacion];
        }
        public function getNombreTemplate(){
            $modUtil = new Utilidades();
            $arreglo = $modUtil->arregloTemplates();
            echo $arreglo[$this->template];
        }
}

<?php

/**
 * This is the model class for table "invoices".
 *
 * The followings are the available columns in table 'invoices':
 * @property string $id_usuario_c
 * @property string $nombres
 * @property string $apellidos
 * @property string $email
 * @property string $id_cliente
 * @property string $ultimo_acceso
 * @property integer $id_perfil
 * @property string $numero_id
 * @property string $ruta_foto
 * @property string $estado
 *
 * The followings are the available model relations:
 * @property LicensesUser[] $licensesUsers
 * @property Clients $idCliente
 * @property Profiles $idPerfil
 */
class Invoice extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
    
    public function tableName()
	{
		return 'invoices';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_invoice, id_order, date_expedition, status', 'required'),
			array('id_invoice, id_order', 'numerical', 'integerOnly'=>true),
			array('id_invoice', 'unique'),
			array('estado', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_invoice, id_order, date_expedition, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
        Yii::import('application.modules.Clients.models.GroupsUsersClients');
		return array(
			'idOrder' => array(self::BELONGS_TO, 'orders', 'id_order'));
			}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_invoice' => 'ID Invoice',
			'id_order' => 'ID order',
			'date_expedition' => 'Date Expedition',
			'estado' => 'Status'
			);

    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
 	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_invoice',$this->id_order,true);
		$criteria->compare('id_order',$this->id_client,true);
		$criteria->compare('date_expedition',$this->date_expedition,true);
		$criteria->compare('status',$this->status,true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'pagination'=>array('pageSize'=>50,),

		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UsersC the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

<?php
class Utilidades extends CFormModel
{

    public function crearThumbNail($name,$filename,$new_w,$new_h){
        ini_set ('gd.jpeg_ignore_warning', 1);
    	$system=explode(".",$name);
        if($system[1]=="pdf"){
            return false;
        }
    	if (preg_match("/jpg|jpeg/",$system[1])){$src_img=imagecreatefromjpeg($name);}
    	if (preg_match("/png/",$system[1])){$src_img=imagecreatefrompng($name);}
    	
    	
    	$old_x=imagesx($src_img);
    	$old_y=imagesy($src_img);
    	if ($old_x > $old_y) 
    	{
    		$thumb_w=$new_w;
    		$thumb_h=$old_y*($new_h/$old_x);
    	}
    	if ($old_x < $old_y) 
    	{
    		$thumb_w=$old_x*($new_w/$old_y);
    		$thumb_h=$new_h;
    	}
    	if ($old_x == $old_y) 
    	{
    		$thumb_w=$new_w;
    		$thumb_h=$new_h;
    	}
    	$dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);
    	imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y); 
    	if (preg_match("/png/",$system[1]))
    	{
    		imagepng($dst_img,$filename); 
    	} else {
    		imagejpeg($dst_img,$filename); 
    	}
    	imagedestroy($dst_img); 
    	imagedestroy($src_img); 
        
        
        if(error_reporting() === 0)
       {
            $abc = 0;
            return false;
       }
    }
    
    public function generaThumb($ruta,$name){
        //A partir de la imagen generar las 4 imágenes 
        $nombre = $ruta."/".$name;
        $tn1 = $ruta."/tn1_".$name;
        $tn2 = $ruta."/tn2_".$name;
        $tn3 = $ruta."/tn3_".$name;
        $this->crearThumbNail($nombre, $tn1, 100, 100);
        $this->crearThumbNail($nombre, $tn2, 250, 250);
        $this->crearThumbNail($nombre, $tn3, 600, 600);
        //Eliminar el archivo original
        @unlink($nombre);
    }
    public function eliminarThumbs($ruta,$name){
        //A partir de la imagen generar las 4 imágenes 
        $tn1 = $ruta."/tn1_".$name;
        $tn2 = $ruta."/tn2_".$name;
        $tn3 = $ruta."/tn3_".$name;
        @unlink($tn1);
        @unlink($tn2);
        @unlink($tn3);
    }
     public function arregloPrefijos(){
        return array('Ms'=>'Ms','Miss'=>'Miss','Mrs'=>'Mrs','Mr'=>'Mr','Dr'=>'Dr');
    }
    public function arregloNiveles(){
        return array('-A1'=>'-A1','A1'=>'A1','A2'=>'A2','B1'=>'B1','B2'=>'B2','C1'=>'C1','C2'=>'C2');
    }
    public function arregloPresentaciones(){
        return array('M'=>'Multiple Selection','D'=>'DropDown','S'=>'Scale');
    } 
    public function arregloMeses(){
        return array('1'=>'Jan','2'=>'Feb','3'=>'Mar','4'=>'Apr','5'=>'May','6'=>'Jun','7'=>'Jul','8'=>'Aug','9'=>'Sep','10'=>'Oct','11'=>'Nov','12'=>'Dec');
    }
    public function arregloLogoExamen(){
        return array('1'=>'TECS','2'=>'RPT','3'=>'EDE');
    }
    public function arregloTiposDocumento(){
        return array('C'=>'Cedula','T'=>'Tarjeta de Identidad','N'=>'Nit');
    }
    public function arregloTiposFraude(){
        return array('1'=>'Identity Theft','2'=>'Counterfeit Certificate','3'=>'Incoherent Skills Score','4'=>'Translation during Test','5'=>'Other','6'=>'Impersonation','7'=>'Use of HDMI Cable','8'=>'Use of notes during the test','9'=>'Assistance by a third party','10'=>'incoherent scores not supported');
    }
    public function arregloColores(){
        return array('#31B744'=>'Green','#FFDA74'=>'Yellow','#1363DC'=>'Blue','#9055CF'=>'Purple','#EE5959'=>'Red','#FFFFFF'=>'White');
    }
    public function arregloTemplates(){
        return array('1'=>'Question Left Fixed // Answers Right','2'=>'Question Top // Answers Below','3'=>'In Line (Scale Questions)','4'=>'Media on Top // Answers Below (Dropdown Questions)','5'=>'Question Left Fluid // Answers Right','6'=>'Question Left // Answers Right 1 column');
    }
    public function tieneTutor($id){ //Saber si la licencia asignada al usuario tiene examenes que exijan tutor
        $modLicenciasCliente = LicensesClient::model()->findByPk($id);
        $retorno = array('estutor'=>'0','nombre'=>'','certificado'=>'0');
        if(!is_null($modLicenciasCliente)){
            $retorno['certificado']=$modLicenciasCliente->idLicencia->genera_certificado;
            $modExamenes = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>$modLicenciasCliente->id_licencia));
            if(!is_null($modExamenes)){
                foreach($modExamenes as $examen){
                    if($examen->idExamen->tutor == 1){
                        $retorno['estutor']="1";
                        $retorno['nombre']=$examen->idExamen->nombre;
                    }
                }
            }
        }
        return json_encode($retorno);
    }
    public function generaPassword($modelo){
        //Se recibe el modelo del Usuario al que se le quiere generar la cave
        //Tomar las 2 primeras letras del Nombre
        $clave = substr($modelo->nombres,1,2);
        $clave .= $this->randomString(6);
        $clave .= substr($modelo->apellidos,1,2);
        return $clave;
    }
    function randomString($numero) {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789.!-*";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $numero; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
    return implode($pass); //turn the array into a string
    }
    
    //REVISAR ESTA FUNCION PUEDE SER EL PROBLEMA
   /* public function actionEnviarRegistro($userId,$login=0,$tipo=0){
        $model= UsersC::model()->findByAttributes(array('id_usuario_c'=>$userId)); 
        //La clave es el ID del usuario, generalmente la cédula
        $clave=$userId;
        $model->clave=md5($clave);
        $model->clave2=md5($clave);
        $model->save(true,array('clave'));
        if($tipo==0){ //Taker con info del Examen
            //Enviar correo al usuario 
            $licenciaUsuario = LicensesUser::model()->findByAttributes(array('id_usuario'=>$userId,'estado'=>'A'));
            $examsUser = ExamsUser::model()->findAllByAttributes(array('id_licencia_usuario'=>$licenciaUsuario->id_licencias_usuario));
            $mail = new YiiMailer('registration', array('login'=>$login, 'model' => $model,'description'=>'Registration process','header_title'=>'ILTO - ADMINISTRATOR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ACCOUNT INFORMATION','clave'=>$clave,'examsUser'=>$examsUser));
            //set properties
            $mail->setFrom("registration@iltoexams.com", "ILTO Exams");
            $mail->setSubject("Registration complete");
            $mail->setTo($model->email);
            $mail->setCc('info@iltoexams.com');
        }
        if($tipo==1){  //Registro a un usuario normal con login
            $mail = new YiiMailer('registration', array('login'=>$login, 'model' => $model,'description'=>'Registration process','header_title'=>'ILTO - ADMINISTRATOR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ACCOUNT INFORMATION','clave'=>$clave,));
            //set properties
            $mail->setFrom("registration@iltoexams.com", "ILTO Exams");
            $mail->setSubject("Registration process");
            $mail->setTo($model->email);
            $mail->setCc('info@iltoexams.com');
        }
        if($tipo>1){
            $mail = new YiiMailer('passwordchange', array('login'=>1, 'model' => $model,'description'=>'Reset Password','clave'=>$clave));
            //set properties
            $mail->setFrom("support@iltoexams.com", "ILTO Exams");
            $mail->setSubject("You have requested a new password");
            $mail->setTo($model->email);
            $mail->setCc('info@iltoexams.com');
        }
        //send
        if ($mail->send()) {
                Yii::app()->user->setFlash('success','An Email was sent to '.$model->email.' with the Login Information.');
        } else {
                Yii::app()->user->setFlash('error','Error while sending email: '.$mail->getError());
        }
    }*/

    public function enviarCuenta($userId,$login=0){
        
        $model= UsersC::model()->findByAttributes(array('id_usuario_c'=>$userId)); 
        
        $perfil = $model->idPerfil->nombre;
        
        switch ($perfil) {
            case 'TOAA':
                $descripcion_perfil = '<p><h2><span color="#008a17">TOAA</span> (Total Access Administrator), This user is able to:</h2>
                <ul>
                  <li>Manage user accounts (create, delete, modify),</li>
                  <li>Allocate test licenses.</li>
                  <li>Manage groups or test rooms (Create, delete, modify)</li>
                  <li>Purchase test licenses</li>
                  <li>Tutor access (Perform the speaking test and assign test licenses)</li>
                  <li>Manage the test agenda (set and modify exam dates,approve or reject test takers)</li>
                </ul>
                  </p>';
                break;
            case 'PAA':
                $descripcion_perfil = '<p>,h2><span color="#008a17">PAA</span> (Partial Access Administrator), This user is able to:</h2>
                <ul>
                   <li>Allocate test licenses.</li>
                   <li>Manage groups or test rooms (Create, delete, modify).</li>
                   <li>Tutor access (Perform the speaking test and assign test licenses).</li>
                   <li>Manage the test agenda (set and modify exam dates, approve or reject test takers).</li>
                </ul>
                </p>';
                break;    
            case 'TUAA':
                $descripcion_perfil = '<p><h2><span color="#008a17">TUAA</span> (Tutor Access Administrator), This user is able to:</h2>
                <ul>
                    <li>Allocate test licenses.</li>
                    <li>See groups or test romos.</li>
                    <li>Tutor access (Perform the speaking test and assign test licenses).</li>
                    <li>See the test schedule.</li>
                </ul>
                </p>';
                break;
            case 'RAA':
                $descripcion_perfil = '<p><h2><span color="#008a17">RAA</span> (Tutor Access Administrator), This user is able to see the test reports.</h2>';
                break;
            default:
                // code...
                break;
        } //
        //
        $to = $model->email;
        $subject = "New Account Information.";
        $htmlBody = '<p><h2>Hi '.$model->nombres.'</h2></p>
            <p>You have a new account in www.iltoexams.com,  your account has been created as <span color="#008a17">'.$perfil.' for '.$model->idCliente->nombre_rsocial.'</span>:</p>
            <p>'.$descripcion_perfil.'</p>
            <p><h3> TO USE YOUR NEW ACCOUNT PLEASE FOLLOW THE STEPS BELLOW:</h3>
            <br>
            Click the following link <a href="https://www.iltoexams.com">https://www.iltoexams.com</a>. Once on the site, click the "TEST ADMINISTRATOR" link and use the following information:</p>
            <table>
                <tr><td style="font-size:16px"><span style="color:#008a17; font-size:16px">Username:</span></td><td> '.$model->id_usuario_c.'</td></tr>
                <tr><td style="font-size:16px"><span style="color:#008a17; font-size:16px">Password:</span></td><td> '.$model->clave2.'</td></tr>
            </table>
            </p>
            <p>
                Best regards,
            </p>
            <p>
                Support Team - ILTO<br>
                <a href="https://www.iltoexams.com/"><img style="float:left;" alt="ILTO Exams" src="iltoexams.com/logo_ILTO.png" /></a>
            </p>
        ';
        
        $dat = "{From: 'noreply@iltoexams.com', To: '$to', Cc: 'info@iltoexams.com', Subject: '$subject', HtmlBody: '$htmlBody'}";//array('From' => 'noreply@iltoexams.com', 'To'=>'dokho_02@hotmail.com', 'Subject'=>'Hola Chris Fer', 'HtmlBody'=>'<strong>Hello</strong> dear Postmark user.');
        $this->sendEmail($dat);
        
        /*$clave=$model->id_usuario_c;
        $model->clave=md5($clave);
        $model->clave2=md5($clave);
        $model->save(true,array('clave'));*/
        /*$mail = new YiiMailer('registration', array('login'=>1, 'model' => $model,'header_title'=>'ILTO - ADMINISTRATOR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ACCOUNT INFORMATION','clave'=>$clave));
        //set properties
        $mail->setFrom("support@iltoexams.com", "ILTO Exams");
        $mail->setSubject("New Account Information");
        $mail->setTo($model->email);
        $mail->setCc('info@iltoexams.com');
        //send test
        if ($mail->send()) {
                Yii::app()->user->setFlash('success','An Email was sent to '.$model->email.' with the Login Information.');
        } else {
                Yii::app()->user->setFlash('error','Error while sending email: '.$mail->getError());
        }*/
    }
    
    public function enviarPDF($to,$subject,$htmlBody,$pdf){
        
        //var_dump($to, $subject, $htmlBody, $pdf);die();
        
        $dat = "{   From: 'noreply@iltoexams.com', 
                    To: '$to', Cc: 'info@iltoexams.com', 
                    Subject: '$subject', 
                    HtmlBody: '$htmlBody'
                    Attachments: [
                        {
                          'Name': '$pdf',
                          'Content': 'dGVzdCBjb250ZW50',
                          'ContentType': 'application/octet-stream'
                        }
                      ]
                }";
        $this->sendEmail($dat);
    }
    
    
    public function enviarPass($userId,$login=0){
        $model= UsersC::model()->findByAttributes(array('id_usuario_c'=>$userId)); 
        
        $perfil = $model->idPerfil->nombre;
        
        switch ($perfil) {
            case 'TOAA':
                $descripcion_perfil = '<p><h2><span color="#008a17">TOAA</span> (Total Access Administrator), This user is able to:</h2>
                <ul>
                  <li>Manage user accounts (create, delete, modify),</li>
                  <li>Allocate test licenses.</li>
                  <li>Manage groups or test rooms (Create, delete, modify)</li>
                  <li>Purchase test licenses</li>
                  <li>Tutor access (Perform the speaking test and assign test licenses)</li>
                  <li>Manage the test agenda (set and modify exam dates,approve or reject test takers)</li>
                </ul>
                  </p>';
                break;
            case 'PAA':
                $descripcion_perfil = '<p>,h2><span color="#008a17">PAA</span> (Partial Access Administrator), This user is able to:</h2>
                <ul>
                   <li>Allocate test licenses.</li>
                   <li>Manage groups or test rooms (Create, delete, modify).</li>
                   <li>Tutor access (Perform the speaking test and assign test licenses).</li>
                   <li>Manage the test agenda (set and modify exam dates, approve or reject test takers).</li>
                </ul>
                </p>';
                break;    
            case 'TUAA':
                $descripcion_perfil = '<p><h2><span color="#008a17">TUAA</span> (Tutor Access Administrator), This user is able to:</h2>
                <ul>
                    <li>Allocate test licenses.</li>
                    <li>See groups or test romos.</li>
                    <li>Tutor access (Perform the speaking test and assign test licenses).</li>
                    <li>See the test schedule.</li>
                </ul>
                </p>';
                break;
            case 'RAA':
                $descripcion_perfil = '<p><h2><span color="#008a17">RAA</span> (Tutor Access Administrator), This user is able to see the test reports.</h2>';
                break;
             case 'RAA':
                $descripcion_perfil = '<p><h2><span color="#008a17">RAA</span> (Tutor Access Administrator), This user is able to see the test reports.</h2>';
                break;
            default:
                // code...
                break;
        }
        $to = $model->email;
        $subject = "New Account Information.";
        $htmlBody = '<p><h2>Hi '.$model->nombres.'</h2></p>
            <p>You have a new account in www.iltoexams.com,  your account has been created as <span color="#008a17">'.$perfil.' for '.$model->idCliente->nombre_rsocial.'</span>:</p>
            <p>'.$descripcion_perfil.'</p>
            <p><h3> TO USE YOUR NEW ACCOUNT PLEASE FOLLOW THE STEPS BELLOW:</h3>
            <br>
            Click the following link <a href="http://www.iltoexams.com">http://www.iltoexams.com</a>. Once on the site, click the "TEST ADMINISTRATOR" link and use the following information:</p>
            <table>
                <tr><td style="font-size:16px"><span style="color:#008a17; font-size:16px">Username:</span></td><td> '.$model->id_usuario_c.'</td></tr>
                <tr><td style="font-size:16px"><span style="color:#008a17; font-size:16px">Password:</span></td><td> '.$model->clave2.'</td></tr>
            </table>
            </p>
            <p>
                Best regards,
            </p>
            <p>
                Support Team - ILTO<br>
                <a href="http://www.iltoexams.com/"><img style="float:left;" alt="ILTO Exams" src="iltoexams.com/logo_ILTO.png" /></a>
            </p>
        ';
        
        $dat = "{From: 'noreply@iltoexams.com', To: '$to', Cc: 'info@iltoexams.com', Subject: '$subject', HtmlBody: '$htmlBody'}";//array('From' => 'noreply@iltoexams.com', 'To'=>'dokho_02@hotmail.com', 'Subject'=>'Hola Chris Fer', 'HtmlBody'=>'<strong>Hello</strong> dear Postmark user.');
        $this->sendEmail($dat);
    }
    
    
    
    public function fotoUsuario(){
        $foto=Yii::app()->basePath."/../images/users/tn1_".Yii::app()->user->getState('ruta_foto');
        if(file_exists("$foto")){
              echo '<img class="imgUser" src="images/users/tn1_'.Yii::app()->user->getState("ruta_foto").'"/>';
         } else {
              echo '<img class="imgUser" src="images/no_photo.png"/>';
        } 
    }
  
    function draw_calendar($month,$year){

	/* draw table */
	$calendar = '<table cellpadding="0" cellspacing="0" class="calendar table table-striped">';

	/* table headings */
	$headings = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
	$calendar.= '<tr class="calendar-row"><td class="calendar-day-head">'.implode('</td><td class="calendar-day-head">',$headings).'</td></tr>';

	/* days and weeks vars now ... */
	$running_day = date('w',mktime(0,0,0,$month,1,$year));
	$days_in_month = date('t',mktime(0,0,0,$month,1,$year));
	$days_in_this_week = 1;
	$day_counter = 0;
	$dates_array = array();

	/* row for week one */
	$calendar.= '<tr class="calendar-row">';

	/* print "blank" days until the first of the current week */
	for($x = 0; $x < $running_day; $x++):
		$calendar.= '<td class="calendar-day-np"> </td>';
		$days_in_this_week++;
	endfor;

	/* keep going with days.... */
	for($list_day = 1; $list_day <= $days_in_month; $list_day++):
		$calendar.= '<td class="calendar-day">';
			/* add in the day number */
			$calendar.= '<div class="day-number">'.$list_day.'</div>';
			$calendar.= '<div id="divday_'.(($list_day<10)?"0".$list_day : $list_day).'" class="day-content"></div>';
			
		$calendar.= '</td>';
		if($running_day == 6):
			$calendar.= '</tr>';
			if(($day_counter+1) != $days_in_month):
				$calendar.= '<tr class="calendar-row">';
			endif;
			$running_day = -1;
			$days_in_this_week = 0;
		endif;
		$days_in_this_week++; $running_day++; $day_counter++;
	endfor;

	/* finish the rest of the days in the week */
	if($days_in_this_week < 8):
		for($x = 1; $x <= (8 - $days_in_this_week); $x++):
			$calendar.= '<td class="calendar-day-np"> </td>';
		endfor;
	endif;

	/* final row */
	$calendar.= '</tr>';

	/* end the table */
	$calendar.= '</table>';
	
	/* all done, return result */
	return $calendar;
    }
    
    public function nivelExamen($totALPROMEDIO){
        if(((($totALPROMEDIO)>=0)&&(($totALPROMEDIO)<=12)))
                    if(((($totALPROMEDIO)>=0)&&(($totALPROMEDIO)<=12)))
                     {
                          $TECSPROFICIENCY ="1";
                          $CEFBYTECS ="PA1";
                          $cefrMess = 'Entry User';
                     }
                  if(((($totALPROMEDIO)>12)&&(($totALPROMEDIO)<=27)))
                     {
                          $TECSPROFICIENCY ="2";
                          $CEFBYTECS ="A1";
                          $cefrMess = 'Entry User';

                     }
                  if(((($totALPROMEDIO)>27)&&(($totALPROMEDIO)<=38)))
                     {
                          $TECSPROFICIENCY ="3";
                          $CEFBYTECS ="A2";
                          $cefrMess = 'Basic User';
                     }
                  if(((($totALPROMEDIO)>38)&&(($totALPROMEDIO)<=50)))
                     {
                          $TECSPROFICIENCY ="4";
                          $CEFBYTECS ="A2+";
                          $cefrMess = 'Basic User';
                     }
                  if(((($totALPROMEDIO)>50)&&(($totALPROMEDIO)<=62)))
                     {
                          $TECSPROFICIENCY ="5";
                          $CEFBYTECS ="B1";
                          $cefrMess = 'Intermediate User';
                     }
                  if(((($totALPROMEDIO)>62)&&(($totALPROMEDIO)<=72)))
                     {
                          $TECSPROFICIENCY ="6";
                          $CEFBYTECS ="B1+";
                          $cefrMess = 'Intermediate User';
                     }
                    if(((($totALPROMEDIO)>72)&&(($totALPROMEDIO)<=80)))
                     {
                          $TECSPROFICIENCY ="7";
                          $CEFBYTECS ="B2";
                          $cefrMess = 'Independent User';
                     }
                     if(((($totALPROMEDIO)>80)&&(($totALPROMEDIO)<=87)))
                     {
                          $TECSPROFICIENCY ="8";
                          $CEFBYTECS ="B2+";
                          $cefrMess = 'Independent User';
                     }
                     if(((($totALPROMEDIO)>87)&&(($totALPROMEDIO)<=100)))
                     {
                          $TECSPROFICIENCY ="9";
                          $CEFBYTECS ="C1";
                          $cefrMess = 'Proficient User';
                     }
        return $CEFBYTECS;
    }

        function levelExplain($id){
                $texto="";
                switch($id){
                    case 'Pre A1':
                        $texto="Can understand and use familiar everyday expressions and very basic phrases aimed at the satisfaction of needs of a concrete type. Can introduce him/herself and others and can ask and answer questions about personal details such as where he/she lives, people he/she knows and things he/she has. Can interact in a simple way provided the other person talks slowly and clearly and is prepared to help.";
                        break;
                    case 'A1':
                        $texto="Can understand and use familiar everyday expressions and very basic phrases aimed at the satisfaction of needs of a concrete type. Can introduce him/herself and others and can ask and answer questions about personal details such as where he/she lives, people he/she knows and things he/she has. Can interact in a simple way provided the other person talks slowly and clearly and is prepared to help.";
                        break;
                    case 'A2':
                        $texto="Can understand sentences and frequently used expressions related to areas of most immediate relevance (e.g. very basic personal and family information, shopping, local geography, employment). Can communicate in simple and routine tasks requiring a simple and direct exchange of information on familiar and routine matters. Can describe in simple terms aspects of his/her background, immediate environment and matters in areas of immediate need.";
                        break;
                    case 'A2+':
                        $texto="Can understand sentences and frequently used expressions related to areas of most immediate relevance (e.g. very basic personal and family information, shopping, local geography, employment). Can communicate in simple and routine tasks requiring a simple and direct exchange of information on familiar and routine matters. Can describe in simple terms aspects of his/her background, immediate environment and matters in areas of immediate need.";
                        break;
                    case 'B1':
                        $texto="Can understand the main points of clear standard input on familiar matters
        regularly encountered in work, school, leisure, etc. Can deal with most
        situations likely to arise whilst travelling in an area where the language is
        spoken. Can produce simple connected text on topics which are familiar or of
        personal interest. Can describe experiences and events, dreams, hopes and
        ambitions and briefly give reasons and explanations for opinions and plans";
                        break;
                    case 'B1+':
                        $texto="Can understand the main points of clear standard input on familiar matters
        regularly encountered in work, school, leisure, etc. Can deal with most
        situations likely to arise whilst travelling in an area where the language is
        spoken. Can produce simple connected text on topics which are familiar or of
        personal interest. Can describe experiences and events, dreams, hopes and
        ambitions and briefly give reasons and explanations for opinions and plans";
                        break;
                    case 'B2':
                        $texto="Can understand the main ideas of complex text on both concrete and
        abstract topics, including technical discussions in his/her field of
        specialisation. Can interact with a degree of fluency and spontaneity that
        makes regular interaction with native speakers quite possible without strain
        for either party. Can produce clear, detailed text on a wide range of subjects
        and explain a viewpoint on a topical issue giving the advantages and
        disadvantages of various options.";
                    case 'B2+':
                        $texto="Can understand the main ideas of complex text on both concrete and
        abstract topics, including technical discussions in his/her field of
        specialisation. Can interact with a degree of fluency and spontaneity that
        makes regular interaction with native speakers quite possible without strain
        for either party. Can produce clear, detailed text on a wide range of subjects
        and explain a viewpoint on a topical issue giving the advantages and
        disadvantages of various options.";
                        break;
                    case 'C1':
                        $texto="Can understand a wide range of demanding, longer texts, and recognise
        implicit meaning. Can express him/herself fluently and spontaneously
        without much obvious searching for expressions. Can use language flexibly
        and effectively for social, academic and professional purposes. Can produce
        clear, well-structured, detailed text on complex subjects, showing controlled
        use of organisational patterns, connectors and cohesive devices.";
                        break;
                    case 'C2':
                        $texto=" Can understand with ease virtually everything heard or read. Can summarise
        information from different spoken and written sources, reconstructing
        arguments and accounts in a coherent presentation. Can express him/herself
        spontaneously, very fluently and precisely, differentiating finer shades of
        meaning even in more complex situations.";
                        break;

                }
                return $texto;
    }    
    
    function generaValidacion($hash){
        $datos= explode("|", base64_decode($hash));
        $name=$datos[0];
        $datefech = $datos[1];
        $score = $datos[2];
        $CEF = $datos[3];
        $retorno = '<table width="90%" border="0" cellpadding="0" cellspacing="0" style="border:#FC0 2px solid" align="center">
            <tr border="1" style="border:#FC0 1px solid">
              <td  width="50%" bgcolor="#F2F2F2" style="text-align:left"><img src="'.Yii::app()->getBaseUrl(true).'/images/TECS.png" width="120" style="margin:5px"></td>
              <td  height="95" bgcolor="#F2F2F2" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size:24px; font-weight:bold" >VALIDATION CODE</td>
            </tr>
          </table>';
        $retorno.='<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td><img src="/CH.png" width="187" height="245"></td>
                  <td>
              <strong style=" font-size:25px; color:#069">This is a valid test</strong>. <br>
              This test was taken <br>
              by <b style="color: #069; font-size:30px">'.$name.'</b> <br>
              on <b style="color:#069; font-size:30px">'.$datefech.'</b>.<br>
              Total Score <b style="color:#069; font-size:30px">'.$score.'</b><br>
              CEF <b style="color:#069; font-size:30px">'.$CEF.'</b></td>
                </tr>
              </table>';
        return $retorno;
        
    }
    
    function generaCertificadoAa($id, $modExamsUser = NULL){
   
        // solicitar como parametro el id_cliente, para fortalecer y asegurar la consulta.
        /**************************************************0*/
        if(is_null($modExamsUser)){
            
            $modExamsUser = ExamsUser::model()->findAllByAttributes(array('id_licencia_usuario'=>$id));
            $logoEnCertificado=Yii::app()->user->getState('logo_en_certificado');
            $idCliente = $modExamsUser[0]->idLicenciaUsuario->idLicenciaCliente->id_cliente;
            $idUsuario = $modExamsUser[0]->idLicenciaUsuario->id_usuario;
            $sql='SELECT * FROM `users_c` WHERE `id_usuario_c` = "'.$idUsuario.'"';
            $user= Yii::app()->db->createCommand($sql)->queryAll();
            $nombreUsuario = ucwords($user[0]["nombres"])."&nbsp;".ucwords($user[0]["apellidos"]);
            $imagen = Yii::app()->getBaseUrl(true)."/images/users/tn2_".$user[0]["ruta_foto"];
            $grupo = $modExamsUser[0]->idLicenciaUsuario->idUsuario->id_grupo_cliente;
            $id_licencia = $modExamsUser[0]->idLicenciaUsuario->idLicenciaCliente->id_licencia;
            
        } else {
            $logoEnCertificado = $modExamsUser[0]->idLicenciaUsuario->idLicenciaCliente->idCliente->logo_en_certificado;
            $idUsuario = $modExamsUser[0]->idLicenciaUsuario->id_usuario;
            $modUsu = UsersC::model()->findAllByAttributes(array('id_usuario_c'=>$idUsuario));
            $modExamsUser = ExamsUser::model()->findAllByAttributes(array('id_licencia_usuario'=>$id));
            $sql='SELECT *FROM `users_c` WHERE `id_usuario_c` = "'.$idUsuario.'"';
            $user= Yii::app()->db->createCommand($sql)->queryAll();
            $nombreUsuario = ucwords($user[0]["nombres"])."&nbsp;".ucwords($user[0]["apellidos"]);
            $grupo = $modUsu->id_grupo_cliente;
            $imagen = Yii::app()->getBaseUrl(true)."/images/users/tn2_".$user[0]["ruta_foto"];
        }
        
        
        //var_dump($imagen);die();
        $SQL = 'SELECT * FROM `proctorv_request` WHERE `id_user` = "'.$idUsuario.'" LIMIT 1'; 
        $tecs2go = Yii::app()->db->createCommand($SQL)->queryAll();
       
        if(count($tecs2go)>0){
            $imagen = Yii::app()->getBaseUrl(true)."/images/proctorv_users/".$tecs2go[0]["photo_route"];
        }
         //var_dump($imagen);die();
        
        //get ID CLIENT AND LOGO
        $idCliente = $modExamsUser[0]->idLicenciaUsuario->idLicenciaCliente->id_cliente;

        $flag_cefr_anterior = 0;
        if($modExamsUser[0]->idLicenciaUsuario->idLicenciaCliente->id_licencias_cliente == 728 && $modExamsUser[0]->idLicenciaUsuario->idLicenciaCliente->id_cliente == '800040295'){

            $flag_cefr_anterior = 1;
        } 
        

        $sql='SELECT id_cliente, nombre_rsocial, ruta_logo  FROM `clients` WHERE `id_cliente` = "'.$idCliente.'"';
        $list= Yii::app()->db->createCommand($sql)->queryAll();
        
        $nombreCliente = $list[0]["nombre_rsocial"];
        $logo = Yii::app()->getBaseUrl(true)."/images/clients/tn2_".$list[0]["ruta_logo"];
        //var_dump($logo);die();
        
        $isDemo = "DEMO: ";
        $sql='SELECT isDemo, id_country, id_grupo_cliente FROM `users_c` WHERE `id_cliente` LIKE "'.$idCliente.'" AND `id_perfil` = 2';
        $list= Yii::app()->db->createCommand($sql)->queryAll();
        
        if($grupo == '607'){ 
                $textoGrupo = '<span style="color:red;">This certificate is only valid for work purposes, This is not valid for academic organizations or graduation requirements.</span>';
        }else{
             $textoGrupo = "";
        }
        
        //logo tipo certificado 
    
        if($id_licencia == '22'){
            $logoLicencia = Yii::app()->getBaseUrl(true)."/images/relang.png";    
        }else {
            $logoLicencia = Yii::app()->getBaseUrl(true)."/images/tecs-2-go.png";
        }
        
        //verifico si es demo
        $isDemo = "DEMO: ";
        $sql='SELECT isDemo, id_country, id_grupo_cliente FROM `users_c` WHERE `id_cliente` LIKE "'.$idCliente.'" AND `id_perfil` = 2';
        $list= Yii::app()->db->createCommand($sql)->queryAll();
        
        $sql='SELECT Name FROM `country` WHERE `Code` LIKE "'.$list[0]['id_country'].'"';
        $list_c= Yii::app()->db->createCommand($sql)->queryAll();
        $pais = $list_c[0]['Name'];
       
        if($list[0]['isDemo']==1){
            $nombreCliente = $isDemo.$nombreCliente;
        }
        
        $KEYUSERexam=(1000000 + $modExamsUser[0]->id_licencia_usuario);
        
        $KEYUSERexam = "L".substr($KEYUSERexam,1);
        
        
        //get id exams to set DATE ANSWERED
        $sql='SELECT id_examen_usuario  FROM `exams_user` WHERE `id_licencia_usuario` = "'.$modExamsUser[0]->id_licencia_usuario.'"';
        $exams = Yii::app()->db->createCommand($sql)->queryAll();
        
        
        //get answered speaking to set Date
        $sql='SELECT date_answered  FROM `answers_exam_user` WHERE `id_examen_usuario` = "'.$exams[0]["id_examen_usuario"].'" ORDER BY date_answered ASC LIMIT 1';
        $date_answered = Yii::app()->db->createCommand($sql)->queryAll();
        
        //set the date
        if($date_answered[0]["date_answered"]==""){
            //if there is no date record answered exam
            $fecha = Yii::app()->dateFormatter->formatDateTime($modExamsUser[0]->fecha_presentacion,"long",NULL)."&nbsp;&nbsp;".$modExamsUser[0]->hora;
        }else {
            //if there is date record 
           $fecha = Yii::app()->dateFormatter->formatDateTime($date_answered[0]["date_answered"],"long","short");
        }
        
        
        $TRRESULTADOS='';
        $TABLARESULTADOS='
        <!--Basic Styles-->
        <link href="/ilto3/themes/ilto/html/assets/css/bootstrap.min.css" rel="stylesheet" />
        <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
        <link href="/ilto3/themes/ilto/html/assets/css/font-awesome.min.css" rel="stylesheet" />
        <link href="/ilto3/themes/ilto/html/assets/css/weather-icons.min.css" rel="stylesheet" />
        
        <!--Fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

        <style>
        h3,h4,h5 {
            font-style: normal;
            font-weight: bold;
            line-height: 1.54;
            font-family: "Montserrat", sans-serif;
            color:#333;
        }
        
        </style>
        ';
            $CONT=0;
            $tot=0;
            foreach($modExamsUser as $examen){
                $CONT=$CONT+1;
                $examName = explode(" ",$examen->idExamen->nombre);
                    
                    $TRRESULTADOS .='
                    <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:5px;margin-bottom:5px;size:10px;">
                        <div class="col-xs-1" style="text-align:center;padding:5px;height:25px;padding:0px;margin-left:75px;background:#E4E4E4;">
                            <strong>'.$CONT.'</strong>
                        </div>   
                        <div class="col-xs-4" style="text-align:left;padding:5px;height:25px;padding:0px;margin-left:5px;background:#E4E4E4;size:10px;"><span style="left:10px;">&nbsp;'.$date_ans[0].'</span>
                            '.strtoupper($examName[0]).'
                        </div>  
                        <div class="col-xs-4" style="text-align:center;padding:5px;height:25px;padding:0px;margin-left:5px;background:#AEE1B6;size:10px;">
                            <strong>'.number_format($examen->calificacion,2).'</strong>
                        </div>  
                    </div>';
                    
                    
                $tot+=$examen->calificacion;
            }
            $totALPROMEDIO=round($tot/$CONT,2);
            
                if($flag_cefr_anterior == 1){
                    if(((($totALPROMEDIO)>=0)&&(($totALPROMEDIO)<=12)))
                   {
                        $CEFBYTECS ="- A1";
                   }
                if(((($totALPROMEDIO)>12)&&(($totALPROMEDIO)<=31)))
                   {
                        $CEFBYTECS ="A1";
    
                   }
                if(((($totALPROMEDIO)>31)&&(($totALPROMEDIO)<=55)))
                   {
                        $CEFBYTECS ="A2";
                   }
                if(((($totALPROMEDIO)>55)&&(($totALPROMEDIO)<=74)))
                   {
                        $CEFBYTECS ="B1";
                   }
                if(((($totALPROMEDIO)>74)&&(($totALPROMEDIO)<=89)))
                   {
                        $CEFBYTECS ="B2";
                   }
                if(((($totALPROMEDIO)>89)&&(($totALPROMEDIO)<=100)))
                   {
                        $CEFBYTECS ="C1";
                   }
            }else{
                
                if(((($totALPROMEDIO)>=0)&&(($totALPROMEDIO)<=12)))
                     {
                          $TECSPROFICIENCY ="1";
                          $CEFBYTECS ="Pre A1";
                          $cefrMess = 'Entry User';
                     }
                  if(((($totALPROMEDIO)>12)&&(($totALPROMEDIO)<=27)))
                     {
                          $TECSPROFICIENCY ="2";
                          $CEFBYTECS ="A1";
                          $cefrMess = 'Entry User';

                     }
                  if(((($totALPROMEDIO)>27)&&(($totALPROMEDIO)<=38)))
                     {
                          $TECSPROFICIENCY ="3";
                          $CEFBYTECS ="A2";
                          $cefrMess = 'Basic User';
                     }
                  if(((($totALPROMEDIO)>38)&&(($totALPROMEDIO)<=50)))
                     {
                          $TECSPROFICIENCY ="4";
                          $CEFBYTECS ="A2+";
                          $cefrMess = 'Basic User';
                     }
                  if(((($totALPROMEDIO)>50)&&(($totALPROMEDIO)<=62)))
                     {
                          $TECSPROFICIENCY ="5";
                          $CEFBYTECS ="B1";
                          $cefrMess = 'Intermediate User';
                     }
                  if(((($totALPROMEDIO)>62)&&(($totALPROMEDIO)<=72)))
                     {
                          $TECSPROFICIENCY ="6";
                          $CEFBYTECS ="B1+";
                          $cefrMess = 'Intermediate User';
                     }
                    if(((($totALPROMEDIO)>72)&&(($totALPROMEDIO)<=80)))
                     {
                          $TECSPROFICIENCY ="7";
                          $CEFBYTECS ="B2";
                          $cefrMess = 'Independent User';
                     }
                     if(((($totALPROMEDIO)>80)&&(($totALPROMEDIO)<=87)))
                     {
                          $TECSPROFICIENCY ="8";
                          $CEFBYTECS ="B2+";
                          $cefrMess = 'Independent User';
                     }
                     if(((($totALPROMEDIO)>87)&&(($totALPROMEDIO)<=100)))
                     {
                          $TECSPROFICIENCY ="9";
                          $CEFBYTECS ="C1";
                          $cefrMess = 'Proficient User';
                     }
            }
                  
                     
                     
              $total_scoremail=number_format($tot/$CONT,2);
            if($flag_cefr_anterior == 1){
                $TABLARESULTADOSTOTALES='
                <div class="row" style="padding:0px;margin:0px;">
                    <div class="col-xs-6" style="text-align:center;padding:5px;">
                        <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:10px;margin-bottom:5px;">
                            <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;border-radius: 15px 0px 0px 0px;">
                                <h6>TECS PROFICIENCY STAGE</h6> 
                            </div>  
                            <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;border-radius: 0px 15px 0px 0px;">
                                <h6>CEFR SCORE</h6> 
                            </div>  
                        </div>
                        <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:5px;margin-bottom:5px;">
                            <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                                0 
                            </div>   
                            <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;">
                                - A1
                            </div>  
                        </div>
                        <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:5px;margin-bottom:5px;">
                            <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                                1 
                            </div>   
                            <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;">
                                A1
                            </div>  
                        </div>
                        <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:5px;margin-bottom:5px;">
                            <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                                2 
                            </div>  
                            <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;">
                                A2
                            </div>  
                        </div>
                        <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:5px;margin-bottom:5px;">
                            <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                                3 
                            </div> 
                            <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;">
                                B1
                            </div>  
                        </div>
                        <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:5px;margin-bottom:5px;">
                            <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                                4 
                            </div>  
                            <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;">
                                B2
                            </div>  
                        </div>
                        <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:5px;margin-bottom:5px;">
                            <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;border-radius:0px 0px 0px 15px;">
                                5 
                            </div>  
                            <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;border-radius:0px 0px 15px 0px;">
                                C1
                            </div>  
                        </div>        
                    </div>  
                    <div class="col-xs-5" style="text-align:center;padding:5px;">
                        <h6 style="text-align:center;">IMPORTANT INFORMATION</h6>
                        <p style="text-align:left;font-size:10px;">According to The Common European Framework of Reference for Languages your level corresponds to:<br>
                          <strong>'.$CEFBYTECS.':&nbsp;</strong>&quot;<i>'.$this->levelExplain($CEFBYTECS).'</i>&quot;
                                '.$textoGrupo.'</p>
                    </div> 
                    
                    
                </div>';   
            }else{
                 $TABLARESULTADOSTOTALES='
            <div class="row" style="padding:0px;margin:0px;">
                <div class="col-xs-6" style="text-align:center;padding:5px;">
                    <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:5px;margin-bottom:5px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;border-radius: 15px 0px 0px 0px;">
                            <h6>TECS PROFICIENCY STAGE</h6> 
                        </div>  
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;border-radius: 0px 15px 0px 0px;">
                            <h6>CEFR SCORE</h6> 
                        </div>  
                    </div>
                    <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:3px;margin-bottom:3px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                            <p style="size:8px;margin:auto;padding:0px;">1</p> 
                        </div>   
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;size:8px !important;">
                            <p style="size:8px;margin:auto;padding:0px;">Pre A1</p>
                        </div>  
                    </div>
                     <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:3px;margin-bottom:3px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                            <p style="size:8px;margin:auto;padding:0px;">2</p> 
                        </div>   
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;size:8px !important;">
                            <p style="size:8px;margin:auto;padding:0px;">A1</p>
                        </div>  
                    </div>
                     <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:3px;margin-bottom:3px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                            <p style="size:8px;margin:auto;padding:0px;">3</p> 
                        </div>   
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;size:8px !important;">
                            <p style="size:8px;margin:auto;padding:0px;">A2</p>
                        </div>  
                    </div>
                     <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:3px;margin-bottom:3px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                            <p style="size:8px;margin:auto;padding:0px;">4</p> 
                        </div>   
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;size:8px !important;">
                            <p style="size:8px;margin:auto;padding:0px;">A2+</p>
                        </div>  
                    </div>
                     <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:3px;margin-bottom:3px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                            <p style="size:8px;margin:auto;padding:0px;">5</p> 
                        </div>   
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;size:8px !important;">
                            <p style="size:8px;margin:auto;padding:0px;">B1</p>
                        </div>  
                    </div>
                     <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:3px;margin-bottom:3px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                            <p style="size:8px;margin:auto;padding:0px;">6</p> 
                        </div>   
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;size:8px !important;">
                            <p style="size:8px;margin:auto;padding:0px;">B1+</p>
                        </div>  
                    </div>
                     <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:3px;margin-bottom:3px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                            <p style="size:8px;margin:auto;padding:0px;">7</p> 
                        </div>   
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;size:8px !important;">
                            <p style="size:8px;margin:auto;padding:0px;">B2</p>
                        </div>  
                    </div>
                     <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:3px;margin-bottom:3px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                            <p style="size:8px;margin:auto;padding:0px;">8</p> 
                        </div>   
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;size:8px !important;">
                            <p style="size:8px;margin:auto;padding:0px;">B2+</p>
                        </div>  
                    </div>
                     <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:3px;margin-bottom:3px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;border-radius: 0px 0px 0px 15px;">
                            <p style="size:8px;margin:auto;padding:0px;">9</p> 
                        </div>   
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;size:8px !important;border-radius: 0px 0px 15px 0px;">
                            <p style="size:8px;margin:auto;padding:0px;">C1</p>
                        </div>  
                    </div>
                    
                    
                </div>  
                <div class="col-xs-5" style="text-align:center;padding:5px;">
                    <h5 style="text-align:center;">LEVEL DESCRIPTION</h5>
                    <p style="text-align:left;font-size:10px;">According to The Common European Framework of Reference for Languages your level corresponds to:<br>
                      <strong>'.$CEFBYTECS.':&nbsp;</strong>&quot;<i>'.$this->levelExplain($CEFBYTECS).'</i>&quot;
                            '.$textoGrupo.'</p>
                </div> 
                
                
            </div>'; 
            }
          
                
            $TABLARESULTADOSTOTALCOMPLETA='<div class="row" style="border-right:2px solid #AEE1B6;border-bottom:2px solid #AEE1B6;text-align:center;">
                <div class="row" style="padding:0px;margin:0px;text-align:center;">
                    <div class="col-xs-3" style="height:50px;padding:0px;margin:0px;">
                        <img src="'.$logoLicencia.'" height="90">
                    </div>
                    <div class="col-xs-5" style="text-align:center;padding:0px;height:50px;padding:0px;margin:0px;padding-top:5px;">
                        <h4>PERFORMANCE REPORT</h4>
                    </div>
                    <div class="col-xs-3" style="text-align:right;height:50px;padding:0px;margin:0px;">
                        <img src="'.Yii::app()->getBaseUrl(true).'/images/logo_ilto_original.png" width="200">
                    </div>
                </div>';
               if($logoEnCertificado==1){
                  $TABLARESULTADOSTOTALCOMPLETA.= '
                <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:15px;">
                    <div class="col-xs-3" style="height:120px;padding:0px;margin:0px;">
                        <img style="max-height:110px;" src="'.Yii::app()->getBaseUrl(true).'/images/cefr-flag.jpg" style="margin-top:1px;">
                        <h2 style="padding:0px;margin:5px auto;">'.$CEFBYTECS.'</h2>
                        <h4 style="padding:0px;margin:5px auto;">'.$cefrMess.'</h4>
                    </div>
                    <div class="col-xs-8" style="text-align:center;padding:0px;height:150px;padding:0px;margin:0px;border-radius: 0px 15px 0px 0px;background:#AEE1B6;font-size:12px;">
                        <h4 style="padding:0px;margin:5px auto;">TEST TAKER INFORMATION</h4>
                        <div class="row" style="padding:0px 10px;margin:0px;text-align:left;">
                            <div class="col-xs-7" style="height:50px;padding:10px;margin:0px;">
                                <label>Name</label><br/>
                                <div style="background:#FEFEFE;padding:5px;border-radius: 10px;text-align:center;">'.$nombreUsuario.'</div>
                                <label>Id Number:</label>
                                <div style="display: inline;background:#FEFEFE;padding:5px;border-radius: 10px;text-align:center;">'.$idUsuario.'</div>
                                <label>First Language:</label>
                                <div style="display: inline;background:#FEFEFE;padding:5px;border-radius: 10px;text-align:center;">Spanish</div>
                            </div>
                            <div class="col-xs-4" style="height:50px;padding:5px;margin:0px;text-align:center;">
                                <h6 style="font-weight:600;padding:0px;margin:5px auto;">Fraud Prevention Code</h6>
                                <h4 style="padding:0px;margin:10px auto;"><b>'.$KEYUSERexam.'</b></h4>
                                <p style="text-align:center;font-size:10px;">To verify this is a valid test, please enter the code in the fraud prevention code section in the website www.iltoexams.com</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:5px;font-size:12px;">
                    <div class="col-xs-3" style="height:150px;padding:0px;margin:0px;">
                    <img style="max-height:110px;"  src="'.$imagen.'" >
                    
                    </div>
                    <div class="col-xs-8" style="text-align:center;padding:5px;height:50px;padding:0px;margin:0px;border-radius:0px 0px 15px 15px;background:#AEE1B6;">
                        <div class="row" style="padding:5px;margin:0px;text-align:left;">
                            <div class="col-xs-6" style="height:50px;padding:10px;margin:0px;">
                                <label>Test Administrator</label><br/>
                                <div style="background:#FEFEFE;padding:5px;border-radius: 10px;text-align:center;">'.$nombreCliente.'</div>
                                <label>Id Test date:</label>
                                <div style="display: inline;background:#FEFEFE;padding:5px;border-radius: 10px;text-align:center;">'.$fecha.'</div>
                            </div>
                            <div class="col-xs-5" style="height:50px;padding:5px;margin:0px;text-align:center;">
                                <label>Adm. ID</label><br/>
                                <div style="background:#FEFEFE;padding:5px;border-radius: 10px;">'.$idCliente.'</div>
                                <label>Country</label>
                                <div style="display: inline;background:#FEFEFE;padding:5px;border-radius: 10px;">'.$pais.'</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:0px;">
                    
                    <div class="col-xs-5" style="text-align:center;padding:5px;height:30px;padding:0px;margin-left:50px;border-radius:15px 15px 0px 0px;background:#AEE1B6;">
                        <h5>TOTAL PERCENTILE SCORE</h5>
                    </div>   
                    <div class="col-xs-5" style="text-align:center;padding:5px;height:30px;padding:0px;margin-left:25px;border-radius:15px 15px 0px 0px;background:#AEE1B6;">
                        <h5>CEFR LEVEL</h5>
                    </div>  
                </div>
                <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:5px;margin-bottom:10px;">
                    <div class="col-xs-5" style="text-align:center;padding:5px;height:30px;padding:0px;margin-left:50px;border-radius:0px 0px 15px 15px;background:#E4E4E4;">
                    '.$total_scoremail.'
                    </div>   
                    <div class="col-xs-5" style="text-align:center;padding:5px;height:30px;padding:0px;margin-left:25px;border-radius:0px 0px 15px 15px;background:#E4E4E4;">
                    '.$CEFBYTECS.'
                    </div>  
                </div>
                
                <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:10px;margin-bottom:5px;">
                <h4 style="margin:auto;">INDIVIDUAL COMMUNICATION SKILL PERFORMANCE</h4>
                    <div class="col-xs-1" style="text-align:center;padding:5px;height:32px;padding:0px;margin-left:75px;border-radius:15 0px 0px 0px;background:#E4E4E4;">
                    </div>   
                    <div class="col-xs-4" style="text-align:center;padding:5px;height:30px;padding:0px;margin-left:5px;background:#E4E4E4;">
                        <h6>MODULE</h6>
                    </div>  
                    <div class="col-xs-4" style="text-align:center;padding:5px;height:30px;padding:0px;margin-left:5px;border-radius:0px 15px 0px 0px;background:#AEE1B6;">
                        <h6>SCORE</h6>
                    </div>  
                </div>
                ';
                $TABLARESULTADOSTOTALCOMPLETA .= $TRRESULTADOS; 
               }   
               $TABLARESULTADOSTOTALCOMPLETA .= ' 
              
          '.$TABLARESULTADOS.'   
              '.$TABLARESULTADOSTOTALES;
             
             $TABLARESULTADOSTOTALCOMPLETA.= '<p style="font-size:10px;text-align:center;">The TECS – Test of English Communication Skills is a product of ILTO – International Language Testing Organization. All rights Reserved &copy; </p>';
            if($list[0]['isDemo']==1){
        
                $TABLARESULTADOSTOTALCOMPLETA.= '<div id="isDemo" style="position:absolute;top:50px;left:5px;"><img src="'.Yii::app()->getBaseUrl(true).'/images/isDemo.png" width="1500"/></div>';
            } 
             
          '</div>';
        /***************************************************/
        return $TABLARESULTADOSTOTALCOMPLETA;          
    
        
    }
    
    function generaCertificado($id, $modExamsUser = NULL){
        
        // solicitar como parametro el id_cliente, para fortalecer y asegurar la consulta.
        /**************************************************0*/
        if(is_null($modExamsUser)){
            
            $modExamsUser = ExamsUser::model()->findAllByAttributes(array('id_licencia_usuario'=>$id));
            $logoEnCertificado=Yii::app()->user->getState('logo_en_certificado');
            $idCliente = $modExamsUser[0]->idLicenciaUsuario->idLicenciaCliente->id_cliente;
            $idUsuario = $modExamsUser[0]->idLicenciaUsuario->id_usuario;
            $sql='SELECT nombres, apellidos FROM `users_c` WHERE `id_usuario_c` = "'.$idUsuario.'"';
            $user= Yii::app()->db->createCommand($sql)->queryAll();
            $nombreUsuario = ucwords($user[0]["nombres"])."&nbsp;".ucwords($user[0]["apellidos"])
            ;$imagen = Yii::app()->getBaseUrl(true)."/images/users/tn2_".$modExamsUser[0]->idLicenciaUsuario->idUsuario->ruta_foto;
            $grupo = $modExamsUser[0]->idLicenciaUsuario->idUsuario->id_grupo_cliente;
            $id_licencia = $modExamsUser[0]->idLicenciaUsuario->idLicenciaCliente->id_licencia;
            
        } else {
            $logoEnCertificado = $modExamsUser[0]->idLicenciaUsuario->idLicenciaCliente->idCliente->logo_en_certificado;
            $idUsuario = $modExamsUser[0]->idLicenciaUsuario->id_usuario;
            $modUsu = UsersC::model()->resetScope()->findByAttributes(array('id_usuario_c'=>$idUsuario),array('limit'=>1));
            $sql='SELECT nombres, apellidos FROM `users_c` WHERE `id_usuario_c` = "'.$idUsuario.'"';
            $user= Yii::app()->db->createCommand($sql)->queryAll();
            $nombreUsuario = ucwords($user[0]["nombres"])."&nbsp;".ucwords($user[0]["apellidos"]);
            $grupo = $modUsu->id_grupo_cliente;
            $imagen = Yii::app()->getBaseUrl(true)."/images/users/tn2_".$modUsu->ruta_foto;
        }
        
        //get ID CLIENT AND LOGO
        $idCliente = $modExamsUser[0]->idLicenciaUsuario->idLicenciaCliente->id_cliente;
        $sql='SELECT id_cliente, nombre_rsocial, ruta_logo  FROM `clients` WHERE `id_cliente` = "'.$idCliente.'"';
        $list= Yii::app()->db->createCommand($sql)->queryAll();
        
        $nombreCliente = $list[0]["nombre_rsocial"];
        $logo = Yii::app()->getBaseUrl(true)."/images/clients/tn2_".$list[0]["ruta_logo"];
        //var_dump($logo);die();
        
        $isDemo = "DEMO: ";
        $sql='SELECT isDemo, id_country, id_grupo_cliente FROM `users_c` WHERE `id_cliente` LIKE "'.$idCliente.'" AND `id_perfil` = 2';
        $list= Yii::app()->db->createCommand($sql)->queryAll();
        
        if($grupo == '607'){ 
                $textoGrupo = '<span style="color:red;">This certificate is only valid for work purposes, This is not valid for academic organizations or graduation requirements.</span>';
        }else{
             $textoGrupo = "";
        }
        
        //logo tipo certificado 
    
        if($id_licencia == '22'){
            $logoLicencia = Yii::app()->getBaseUrl(true)."/images/relang.png";    
        }else {
            $logoLicencia = Yii::app()->getBaseUrl(true)."/images/TECS.png";
        }
        
        //verifico si es demo
        $isDemo = "DEMO: ";
        $sql='SELECT isDemo, id_country, id_grupo_cliente FROM `users_c` WHERE `id_cliente` LIKE "'.$idCliente.'" AND `id_perfil` = 2';
        $list= Yii::app()->db->createCommand($sql)->queryAll();
        
        $sql='SELECT Name FROM `country` WHERE `Code` LIKE "'.$list[0]['id_country'].'"';
        $list_c= Yii::app()->db->createCommand($sql)->queryAll();
        $pais = $list_c[0]['Name'];
       
        if($list[0]['isDemo']==1){
            $nombreCliente = $isDemo.$nombreCliente;
        }
        
        $KEYUSERexam=(1000000 + $modExamsUser[0]->id_licencia_usuario);
        
        $KEYUSERexam = "L".substr($KEYUSERexam,1);
        
        //get id exams to set DATE ANSWERED
        $sql='SELECT id_examen_usuario  FROM `exams_user` WHERE `id_licencia_usuario` = "'.$modExamsUser[0]->id_licencia_usuario.'"';
        $exams = Yii::app()->db->createCommand($sql)->queryAll();
        
        
        //get answered speaking to set Date
        $sql='SELECT date_answered  FROM `answers_exam_user` WHERE `id_examen_usuario` = "'.$exams[1]["id_examen_usuario"].'" ORDER BY date_answered ASC LIMIT 1';
        $date_answered = Yii::app()->db->createCommand($sql)->queryAll();
        
        //set the date
        if($date_answered[0]["date_answered"]==""){
            //if there is no date record answered exam
            $fecha = Yii::app()->dateFormatter->formatDateTime($modExamsUser[0]->fecha_presentacion,"long",NULL)."&nbsp;&nbsp;".$modExamsUser[0]->hora;
        }else {
            //if there is date record 
           $fecha = Yii::app()->dateFormatter->formatDateTime($date_answered[0]["date_answered"],"long","short");
        }
        
        $TRRESULTADOS='';
        $TABLARESULTADOS='<style></style><table width="100%" style="border:#FC0 0px solid;font-family: Arial, Helvetica, sans-serif;"><tr><td width="50%" style="border-right:#FC0 0px solid"  bgcolor="#E6E6E6"><table align="center"  border="0" cellpadding="0" cellspacing="0"  >
            <tr>
              <td colspan="3"  align="center"><strong>MODULE</strong></td>
              <td  >&nbsp;</td>
              <td ><strong>SCORE</strong></td>
              </tr>';
                  $CONT=0;
                  $tot=0;
                  foreach($modExamsUser as $examen){
                          $CONT=$CONT+1;
                          $examName = explode(" ",$examen->idExamen->nombre);
                          $TRRESULTADOS .=' <tr>
                          <td width="6%"  align="center"><strong>'.$CONT.'</strong></td>
                          <td width="6%"  >&nbsp;</td>
                          <td width="60%" style="text-align:left;">'.strtoupper($examName[0]).'</td>
                          <td width="5%"  >&nbsp;</td>
                          <td width="18%" align="center"><strong>'.number_format($examen->calificacion,2).'</strong></td>
                          </tr>';					
                          $tot+=$examen->calificacion;
                  }
                  $totALPROMEDIO=round($tot/$CONT,1);
                  if(((($totALPROMEDIO)>=0)&&(($totALPROMEDIO)<=12)))
                     {
                          $TECSPROFICIENCY ="1";
                          $CEFBYTECS ="Pre A1";
                          $cefrMess = 'Entry User';
                     }
                  if(((($totALPROMEDIO)>12)&&(($totALPROMEDIO)<=27)))
                     {
                          $TECSPROFICIENCY ="2";
                          $CEFBYTECS ="A1";
                          $cefrMess = 'Entry User';

                     }
                  if(((($totALPROMEDIO)>27)&&(($totALPROMEDIO)<=38)))
                     {
                          $TECSPROFICIENCY ="3";
                          $CEFBYTECS ="A2";
                          $cefrMess = 'Basic User';
                     }
                  if(((($totALPROMEDIO)>38)&&(($totALPROMEDIO)<=50)))
                     {
                          $TECSPROFICIENCY ="4";
                          $CEFBYTECS ="A2+";
                          $cefrMess = 'Basic User';
                     }
                  if(((($totALPROMEDIO)>50)&&(($totALPROMEDIO)<=62)))
                     {
                          $TECSPROFICIENCY ="5";
                          $CEFBYTECS ="B1";
                          $cefrMess = 'Intermediate User';
                     }
                  if(((($totALPROMEDIO)>62)&&(($totALPROMEDIO)<=72)))
                     {
                          $TECSPROFICIENCY ="6";
                          $CEFBYTECS ="B1+";
                          $cefrMess = 'Intermediate User';
                     }
                    if(((($totALPROMEDIO)>72)&&(($totALPROMEDIO)<=80)))
                     {
                          $TECSPROFICIENCY ="7";
                          $CEFBYTECS ="B2";
                          $cefrMess = 'Independent User';
                     }
                     if(((($totALPROMEDIO)>80)&&(($totALPROMEDIO)<=87)))
                     {
                          $TECSPROFICIENCY ="8";
                          $CEFBYTECS ="B2+";
                          $cefrMess = 'Independent User';
                     }
                     if(((($totALPROMEDIO)>87)&&(($totALPROMEDIO)<=100)))
                     {
                          $TECSPROFICIENCY ="9";
                          $CEFBYTECS ="C1";
                          $cefrMess = 'Proficient User';
                     }
              $total_scoremail=number_format($tot/$CONT,2);
              //aca
                  $TABLARESULTADOS.=$TRRESULTADOS.'</TABLE></td><td  style="background-color:#80F291;"><table width="100%"  height="96" border="0" align="center" cellpadding="0" cellspacing="0" >
            <tr>
              <td align="center">TOTAL PERCENTILE SCORE</td>
              <td align="center"><strong>'.$total_scoremail.'</strong></td>
              </tr>
              <tr><td>&nbsp;</td></tr>
         
            <tr>
              <td align="center">CEFR LEVEL</td>
              <td align="center"><strong>'.$CEFBYTECS.'</strong></td>
            </tr>
              </table></td></table>';
              $TABLARESULTADOSTOTALES='<table><tr><td width="50%">
<table  height="200" border="0" align="center" cellpadding="0" cellspacing="0" style="border:#E6E6E6 2px solid;font-size:14px;font-family: Arial, Helvetica, sans-serif;">
  <tr>
    <td height="80" align="center"  bgcolor="#E6E6E6"><strong>TECS PROFICIENCY STAGE</strong></td>
    <td  bgcolor="#E6E6E6" align="center"><strong>TECS<br>
      SCORE<br>
    </strong></td>
    <td  bgcolor="#EAEAEA" align="center"><strong>	CEFR<br>
      SCORE
    </strong></td>
    </tr>
  <tr>
    <td width="45%"  bgcolor="#E6E6E6" align="center">0<br>
      1<br>
      2<br>
      3<br>
      4<br>
      5</td>
    <td width="29%" bgcolor="#E6E6E6" align="center">0-12<br>
      13-31<br>
      32-55<br>
      56-74<br>
      75-89<br>
      90-100</td>
    <td width="26%" height="47"  bgcolor="#E6E6E6" align="center">- A1<br>
      A1<br>
      A2<br>
      B1<br>
      B2<br>
      C1</td>
    </tr>
    </table></td><td style="vertical-align:top"><table border="0" style="font-size:12px; text-align: justify;" ><tr style="background-color:#31B744;"><td width="100%"><h2 style="Color:#fff;font-family: Arial, Helvetica, sans-serif;font-size:14px;"><center>IMPORTANT INFORMATION</center></h2></td></tr><tr><td  bgcolor="#F2F2F2">
              According to The Common European Framework of Reference for Languages your level corresponds to:<br>
              <strong>'.$CEFBYTECS.':&nbsp;</strong>&quot;<i>'.$this->levelExplain($CEFBYTECS).'</i>&quot;
                    '.$textoGrupo.'
                  </td></tr></table></td></tr>
                  </table>              
<p style="font-family:Arial, Helvetica, sans-serif; font-size:10px;"></p>          ';
          
                
                          $TABLARESULTADOSTOTALCOMPLETA=
                          '
                          <table width="100%" border="0" style="border:#FC0 0px solid; background-position:center; background-repeat:no-repeat; background-position:" background="backgroundtecs.jpg">
            <tr>
              <td height="900" valign="top">

              <table width="98%" border="0" cellpadding="0" cellspacing="0" style="border:#FC0 0px solid";align="center">
            <tr border="1" style="border:#FC0 0px solid">
              <td  width="25%" bgcolor="#F2F2F2"><img src="'.$logoLicencia.'" width="120" style="margin:5px"></td>
              <td  height="95" bgcolor="#F2F2F2" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size:24px; font-weight:bold" >PERFORMANCE REPORT</td>
                 <td  width="25%" bgcolor="#F2F2F2" style="text-align:right"><img src="'.Yii::app()->getBaseUrl(true).'/images/logo_ILTO.png" width="120" style="margin:5px"></td>
            
            </tr>
          </table>
              <br>';
               if($logoEnCertificado==1){
                  $TABLARESULTADOSTOTALCOMPLETA.= '
                      <table width="98%" border="0" cellpadding="0" cellspacing="0" style="border-bottom:#D6D6D6 1px solid;font-size:13px;font-family: Arial, Helvetica, sans-serif;padding-bottom:5px;" align="center">
            <tr>
              <td width="15%"  bgcolor="#FFFFFF"><strong>Test Administrator</strong></td>
              <td width="1%"  bgcolor="#F2F2F2">&nbsp;</td>
              <td width="30%"  bgcolor="#F2F2F2">'.$nombreCliente.'</td>
              <td width="2%"  bgcolor="#FFFFFF">&nbsp;</td>
              <td width="15%"  bgcolor="#FFFFFF"><strong>Adm. ID</strong></td>
              <td width="2%"  bgcolor="#F2F2F2">&nbsp;</td>
              <td width="15%"  bgcolor="#F2F2F2" text-align:center;>'.$idCliente.'</td>   
              <td width="15%" rowspan="2" bgcolor="#F2F2F2"><img src="'.$logo.'" width="100"></td> 
            </tr>
            <tr>
              <td  bgcolor="#FFFFFF"><strong>Test date</strong></td>
              <td  bgcolor="#F2F2F2">&nbsp;</td>
              <td  bgcolor="#F2F2F2">'.$fecha.'</td>
              <td  bgcolor="#FFFFFF">&nbsp;</td>
              <td  bgcolor="#FFFFFF"><strong>Country</strong></td>
              <td  bgcolor="#F2F2F2">&nbsp;</td>
              <td  bgcolor="#F2F2F2">'.$pais.'</td>
            </tr>
              </table>';
               } else {
                  $TABLARESULTADOSTOTALCOMPLETA.= '<table width="98%" border="0" cellpadding="0" cellspacing="0" style="border:#FC0 0px solid;font-size:13px" align="center">
            <tr>
              <td width="23%"  bgcolor="#FFFFFF"><strong>Test Administrator</strong></td>
              <td width="1%"  bgcolor="#F2F2F2">&nbsp;</td>
              <td width="43%"  bgcolor="#F2F2F2">'.$nombreCliente.'</td>
              <td width="2%"  bgcolor="#FFFFFF">&nbsp;</td>
              <td width="13%"  bgcolor="#FFFFFF"><strong>Adm. ID</strong></td>
              <td width="1%"  bgcolor="#F2F2F2">&nbsp;</td>
              <td width="17%"  bgcolor="#F2F2F2">'.$idCliente.'</td>
            </tr>
            <tr>
              <td  bgcolor="#FFFFFF"><strong>Test date</strong></td>
              <td  bgcolor="#F2F2F2">&nbsp;</td>
              <td  bgcolor="#F2F2F2">'.$fecha.'</td>
              <td  bgcolor="#FFFFFF">&nbsp;</td>
              <td  bgcolor="#FFFFFF"><strong>Country</strong></td>
              <td  bgcolor="#F2F2F2">&nbsp;</td>
              <td  bgcolor="#F2F2F2">'.$pais.'</td>
            </tr>
              </table>';
                   
               }   
               $TABLARESULTADOSTOTALCOMPLETA .= ' 
              <br>
              <div style="width:200px; margin:auto;">
              <center>
                <strong style="font-family: Arial, Helvetica, sans-serif; font-size:16px; font-weight:bold">TEST TAKER INFORMATION </strong>
              </center></div><br>
          <table width="98%" border="0" cellpadding="0"  cellspacing="0" style="border-bottom:#D6D6D6 1px solid;font-size:13px;font-family: Arial, Helvetica, sans-serif;padding-bottom:5px;" align="center">
            <tr>
              <td width="30%" style="text-align:center;"><img style="max-height:110px;" src="'.$imagen.'" ></td> 
              <td width="33%" bgcolor="#E6E6E6">
              <table ><tr>    
              <td>&nbsp;</td>
              <td rowspan="1" ><strong>Full Name</strong></td>
              <td >&nbsp;</td>
              <td rowspan="1" >'.$nombreUsuario.'</td>
            </tr>
             <tr>
              <td>&nbsp;</td>
              <td><strong>Id Number</strong></td>
              <td>&nbsp;</td>
              <td>'.$idUsuario.'</td>
            </tr>
            <tr>
              <td >&nbsp;</td>
              <td ><strong>First Language</strong></td>
              <td >&nbsp;</td>
              <td >Spanish</td>
            </tr></table></td>
            <td width="33%"><table   border="0" align="center" cellpadding="2" cellspacing="0" style="font-size:14px">
            <tr>
              <td height="60" align="center"  bgcolor="#FFFFFF"><strong style="font-size:20px; font-family:Arial, Helvetica, sans-serif; font-weight:bold">Fraud Prevention Code</strong></td>
              </tr>
            <tr>
              <td  height="40" align="center"  bgcolor="#FFFFFF" style="font-size:20px; font-family:Arial, Helvetica, sans-serif; font-weight:bold">'.$KEYUSERexam.'</td>
              </tr>
              <tr><td style="font-size:10px; font-weight:bold;"  bgcolor="#FFFFFF">To verify this is a valid test, please enter the code in the fraud prevention code section in the website www.iltoexams.com</td></tr>
              </table></td>
            </tr>
              </table>          <br>              
<center>
            <strong><span style="font-family:Arial, Helvetica, sans-serif;">TEST RESULTS<span>
            </strong>
          </center>
          <br>
          '.$TABLARESULTADOS.'   
              '.$TABLARESULTADOSTOTALES.'
           </td>
              </tr>
             </table><p style=" font-size:10px; ">';
             
             $TABLARESULTADOSTOTALCOMPLETA.= 'The TECS &ndash; Test of English Communication Skills is a product of ILTO &ndash; International Language Testing Organization. All rights Reserved&copy; </p>';
            if($list[0]['isDemo']==1){
        
                $TABLARESULTADOSTOTALCOMPLETA.= '<div id="isDemo" style="position:absolute;top:50px;left:5px;"><img src="'.Yii::app()->getBaseUrl(true).'/images/isDemo.png" width="1500"/></div>';
            } 
             
          '</table></span>';
        /***************************************************/
        return $TABLARESULTADOSTOTALCOMPLETA;        
    }
    
    function generaCertificadoEde($id, $modExamsUser = NULL){
        // solicitar como parametro el id_cliente, para fortalecer y asegurar la consulta.
        /***************************************************/
        if(is_null($modExamsUser)){
            $modExamsUser = ExamsUser::model()->findAllByAttributes(array('id_licencia_usuario'=>$id));
            $logoEnCertificado=Yii::app()->user->getState('logo_en_certificado');
            $idUsuario = $modExamsUser[0]->idLicenciaUsuario->id_usuario;
            $sql='SELECT nombres, apellidos FROM `users_c` WHERE `id_usuario_c` = "'.$idUsuario.'"';
            $user= Yii::app()->db->createCommand($sql)->queryAll();
            $nombreUsuario = ucwords($user[0]["nombres"])."&nbsp;".ucwords($user[0]["apellidos"]);
            $imagen = Yii::app()->getBaseUrl(true)."/images/users/tn2_".$modExamsUser[0]->idLicenciaUsuario->idUsuario->ruta_foto;
        } else {
            $logoEnCertificado = $modExamsUser[0]->idLicenciaUsuario->idLicenciaCliente->idCliente->logo_en_certificado;
            $idUsuario = $modExamsUser[0]->idLicenciaUsuario->id_usuario;
            $modUsu = UsersC::model()->resetScope()->findByAttributes(array('id_usuario_c'=>$idUsuario),array('limit'=>1));
            //get country name
            $sql='SELECT nombres, apellidos FROM `users_c` WHERE `id_usuario_c` = "'.$idUsuario.'"';
            $user= Yii::app()->db->createCommand($sql)->queryAll();
            $nombreUsuario = ucwords($user[0]["nombres"])."&nbsp;".ucwords($user[0]["apellidos"]);
            $imagen = Yii::app()->getBaseUrl(true)."/images/users/tn2_".$modUsu->ruta_foto;
        }
        $KEYUSERexam=(1000000 + $modExamsUser[0]->id_licencia_usuario);
        
        //get ID CLIENT AND LOGO
        $idCliente = $modExamsUser[0]->idLicenciaUsuario->idLicenciaCliente->id_cliente;
        $sql='SELECT id_cliente, nombre_rsocial, ruta_logo  FROM `clients` WHERE `id_cliente` = "'.$idCliente.'"';
        $list= Yii::app()->db->createCommand($sql)->queryAll();
        
        //client name
        $nombreCliente = $list[0]["nombre_rsocial"];
        $logo = Yii::app()->getBaseUrl(true)."/images/clients/tn2_".$list[0]["ruta_logo"];
        
        //if_demo and country_id
        $isDemo = "DEMO: ";
        $sql='SELECT isDemo, id_country FROM `users_c` WHERE `id_cliente` LIKE "'.$idCliente.'" AND `id_perfil` = 2';
        $list= Yii::app()->db->createCommand($sql)->queryAll();
        
        
        //get country name
        $sql='SELECT Name FROM `country` WHERE `Code` LIKE "'.$list[0]['id_country'].'"';
        $list_c= Yii::app()->db->createCommand($sql)->queryAll();
        $pais = $list_c[0]['Name'];
        
        if($list[0]['isDemo']==1){
            $nombreCliente = $isDemo.$nombreCliente;
        }
       
        //security key
        $KEYUSERexam = "L".substr($KEYUSERexam,1);
        
        //get id exams to set DATE ANSWERED
        $sql='SELECT id_examen_usuario  FROM `exams_user` WHERE `id_licencia_usuario` = "'.$modExamsUser[0]->id_licencia_usuario.'"';
        $exams = Yii::app()->db->createCommand($sql)->queryAll();
        
        
        //get answered speaking to set Date
        $sql='SELECT date_answered  FROM `answers_exam_user` WHERE `id_examen_usuario` = "'.$exams[1]["id_examen_usuario"].'" ORDER BY date_answered ASC LIMIT 1';
        $date_answered = Yii::app()->db->createCommand($sql)->queryAll();
        
        //set the date
        if($date_answered[0]["date_answered"]==""){
            //if there is no date record answered exam
            $fecha = Yii::app()->dateFormatter->formatDateTime($modExamsUser[0]->fecha_presentacion,"long",NULL)."&nbsp;&nbsp;".$modExamsUser[0]->hora;
        }else {
            //if there is date record 
           $fecha = Yii::app()->dateFormatter->formatDateTime($date_answered[0]["date_answered"],"long","short");
        }
        
        $TRRESULTADOS='';
        $TABLARESULTADOS='<style></style><table width="100%" style="border:#FC0 0px solid"><tr><td width="50%" style="border-right:#FC0 0px solid"  bgcolor="#F2F2F2"><table align="center"  border="0" cellpadding="0" cellspacing="0"  >
            <tr>
              <td colspan="3"  align="center"><strong>MODULE</strong></td>
              <td  >&nbsp;</td>
              <td ><strong>SCORE</strong></td>
              </tr>';
                  $CONT=0;
                  $tot=0;
                  foreach($modExamsUser as $examen){
                          $CONT=$CONT+1;
                          $examName = explode(" ",$examen->idExamen->nombre);
                          $TRRESULTADOS .=' <tr>
                          <td width="6%"  bgcolor="#F2F2F2" align="center"><strong>'.$CONT.'</strong></td>
                          <td width="6%"  bgcolor="#F2F2F2">&nbsp;</td>
                          <td width="60%"  bgcolor="#F2F2F2" style="text-align:left;">'.strtoupper($examName[0]).'</td>
                          <td width="5%"  bgcolor="#F2F2F2">&nbsp;</td>
                          <td width="18%" align="center"  bgcolor="#F2F2F2"><strong>'.number_format($examen->calificacion,2).'</strong></td>
                          </tr>';					
                          $tot+=$examen->calificacion;
                  }
                  $totALPROMEDIO=round($tot/$CONT,1);
                  if(((($totALPROMEDIO)>=0)&&(($totALPROMEDIO)<=12)))
                     {
                          $TECSPROFICIENCY ="1";
                          $CEFBYTECS ="Pre A1";
                          $cefrMess = 'Entry User';
                     }
                  if(((($totALPROMEDIO)>12)&&(($totALPROMEDIO)<=27)))
                     {
                          $TECSPROFICIENCY ="2";
                          $CEFBYTECS ="A1";
                          $cefrMess = 'Entry User';

                     }
                  if(((($totALPROMEDIO)>27)&&(($totALPROMEDIO)<=38)))
                     {
                          $TECSPROFICIENCY ="3";
                          $CEFBYTECS ="A2";
                          $cefrMess = 'Basic User';
                     }
                  if(((($totALPROMEDIO)>38)&&(($totALPROMEDIO)<=50)))
                     {
                          $TECSPROFICIENCY ="4";
                          $CEFBYTECS ="A2+";
                          $cefrMess = 'Basic User';
                     }
                  if(((($totALPROMEDIO)>50)&&(($totALPROMEDIO)<=62)))
                     {
                          $TECSPROFICIENCY ="5";
                          $CEFBYTECS ="B1";
                          $cefrMess = 'Intermediate User';
                     }
                  if(((($totALPROMEDIO)>62)&&(($totALPROMEDIO)<=72)))
                     {
                          $TECSPROFICIENCY ="6";
                          $CEFBYTECS ="B1+";
                          $cefrMess = 'Intermediate User';
                     }
                    if(((($totALPROMEDIO)>72)&&(($totALPROMEDIO)<=80)))
                     {
                          $TECSPROFICIENCY ="7";
                          $CEFBYTECS ="B2";
                          $cefrMess = 'Independent User';
                     }
                     if(((($totALPROMEDIO)>80)&&(($totALPROMEDIO)<=87)))
                     {
                          $TECSPROFICIENCY ="8";
                          $CEFBYTECS ="B2+";
                          $cefrMess = 'Independent User';
                     }
                     if(((($totALPROMEDIO)>87)&&(($totALPROMEDIO)<=100)))
                     {
                          $TECSPROFICIENCY ="9";
                          $CEFBYTECS ="C1";
                          $cefrMess = 'Proficient User';
                     }
              $total_scoremail=number_format($tot/$CONT,2);
                  $TABLARESULTADOS.=$TRRESULTADOS.'</TABLE></td><td  style="background-color:#80F291;"><table width="100%"  height="96" border="0" align="center" cellpadding="0" cellspacing="0" >
            <tr>
              <td align="center">TOTAL PERCENTILE SCORE</td>
              <td align="center"><strong>'.$total_scoremail.'</strong></td>
              </tr>
              <tr><td>&nbsp;</td></tr>
         
            <tr>
              <td align="center">CEFR LEVEL</td>
              <td align="center"><strong>'.$CEFBYTECS.'</strong></td>
            </tr>
              </table></td></table>';
              $TABLARESULTADOSTOTALES='<table><tr><td width="50%">
    <table  height="175" border="0" align="center" cellpadding="0" cellspacing="0" style="border:#FC0 0px solid;font-size:14px">
        <tr>
            <td height="59" align="center" bgcolor="#EAEAEA"> <p style="text-align:justify;">This is the result of a diagnosis test that measures the level of English of the taker in three basic skills in a non-secure environment. This is not an international certification and thus, ILTO is not accountable for the real result of this test. For a more comprehensive international standardized test certificate, ILTO suggests the TECS (Test of English Communication Skills) which is taken in a secure environment, with a fraud prevention code that prevents any kind of plagiarism or abnormal situation and issues an official certificate supported by all the development in English testing by ILTO. </p>
            </td>
        </tr>
    </table>
</td>
    </tr>
    </table>              
<p style="font-family:Arial, Helvetica, sans-serif; font-size:10px;"></p>
          ';
                          $TABLARESULTADOSTOTALCOMPLETA=
                          '
                          <table width="100%" border="0" style="border:#FC0 0px solid; background-position:center; background-repeat:no-repeat; background-position:" background="backgroundtecs.jpg">
            <tr>
              <td height="900" valign="top">

              <table width="98%" border="0" cellpadding="0" cellspacing="0" style="border:#FC0 0px solid";align="center">
            <tr border="1" style="border:#FC0 0px solid">
              <td  width="25%" bgcolor="#F2F2F2"><img src="'.Yii::app()->getBaseUrl(true).'/images/EDE.png" width="120" style="margin:5px"></td>
              <td  height="95" bgcolor="#F2F2F2" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size:24px; font-weight:bold" >English Diagnosis Exam Results</td>
                 <td  width="25%" bgcolor="#F2F2F2" style="text-align:right"><img src="'.Yii::app()->getBaseUrl(true).'/images/logo_ILTO.png" width="120" style="margin:5px"></td>
            
            </tr>
          </table>
              <br>';
               if($logoEnCertificado==1){
                  $TABLARESULTADOSTOTALCOMPLETA.= '
                      <table width="98%" border="0" cellpadding="0" cellspacing="0" style="border:#FC0 0px solid;font-size:13px" align="center">
            <tr>
              <td width="15%"  bgcolor="#FFFFFF"><strong>Test Administrator</strong></td>
              <td width="1%"  bgcolor="#F2F2F2">&nbsp;</td>
              <td width="30%"  bgcolor="#F2F2F2">'.$nombreCliente.'</td>
              <td width="2%"  bgcolor="#FFFFFF">&nbsp;</td>
              <td width="15%"  bgcolor="#FFFFFF"><strong>Adm. ID</strong></td>
              <td width="2%"  bgcolor="#F2F2F2">&nbsp;</td>
              <td width="15%"  bgcolor="#F2F2F2">'.$idCliente.'</td>   
            </tr>
            <tr>
              <td  bgcolor="#FFFFFF"><strong>Test date</strong></td>
              <td  bgcolor="#F2F2F2">&nbsp;</td>
              <td  bgcolor="#F2F2F2">'.$fecha.'</td>
              <td  bgcolor="#FFFFFF">&nbsp;</td>
              <td  bgcolor="#FFFFFF"><strong>Country</strong></td>
              <td  bgcolor="#F2F2F2">&nbsp;</td>
              <td  bgcolor="#F2F2F2">'.$pais.'</td>
            </tr>
              </table>';
               } else {
                  $TABLARESULTADOSTOTALCOMPLETA.= '<table width="98%" border="0" cellpadding="0" cellspacing="0" style="border:#FC0 0px solid;font-size:13px" align="center">
            <tr>
              <td width="23%"  bgcolor="#FFFFFF"><strong>Test Administrator</strong></td>
              <td width="1%"  bgcolor="#F2F2F2">&nbsp;</td>
              <td width="43%"  bgcolor="#F2F2F2">'.$nombreCliente.'</td>
              <td width="2%"  bgcolor="#FFFFFF">&nbsp;</td>
              <td width="13%"  bgcolor="#FFFFFF"><strong>Adm. ID</strong></td>
              <td width="1%"  bgcolor="#F2F2F2">&nbsp;</td>
              <td width="17%"  bgcolor="#F2F2F2">'.$idCliente.'</td>
            </tr>
            <tr>
              <td  bgcolor="#FFFFFF"><strong>Test date</strong></td>
              <td  bgcolor="#F2F2F2">&nbsp;</td>
              <td  bgcolor="#F2F2F2">'.$fecha.'</td>
              <td  bgcolor="#FFFFFF">&nbsp;</td>
              <td  bgcolor="#FFFFFF"><strong>Country</strong></td>
              <td  bgcolor="#F2F2F2">&nbsp;</td>
              <td  bgcolor="#F2F2F2">'.$pais.'C/td>
            </tr>
              </table>';
                   
               }   
               $TABLARESULTADOSTOTALCOMPLETA .= ' 
              <br>
              <div style="width:200px; margin:auto;">
              <center>
                <strong>TEST TAKER INFORMATION </strong>
              </center></div><br>
          <table width="98%" border="0" cellpadding="0"  cellspacing="0" style="border:#FC0 0px solid;font-size:15px" align="center">
            <tr>
              <td width="100%" bgcolor="#F2F2F2">
              <table border="0" width="100%"><tr>    
              <td rowspan="2"><img src="'.$logo.'" width="150"> </td>
              <td rowspan="1" ><strong>Full Name</strong></td>
              <td >&nbsp;</td>
              <td rowspan="1" >'.$nombreUsuario.'</td>
            </tr>
            <tr>
              <td ><strong>First Language</strong></td>
              <td >&nbsp;</td>
              <td >Spanish</td>
            </tr></table></td>
            </tr>
              </table>          <br>              
<center>
            <strong>TEST RESULTS
            </strong>
          </center>
          <br>
          '.$TABLARESULTADOS.'    <br>
              '.$TABLARESULTADOSTOTALES.'
           </td>
              </tr>
             </table><p style=" font-size:10px; ">The EDE &ndash; English Diagnosis Exam is a product of ILTO &ndash; International Language Testing Organization. All rights Reserved&copy; </p>';
            if($list[0]['isDemo']==1){
        
                $TABLARESULTADOSTOTALCOMPLETA.= '<div id="isDemo" style="position:absolute;top:50px;left:5px;"><img src="'.Yii::app()->getBaseUrl(true).'/images/isDemo.png" width="1500"/></div>';
            } 
             
          '</table></span>';
        /***************************************************/
        return $TABLARESULTADOSTOTALCOMPLETA;        
    }
    
    function generaCertificadoEcluv($id, $modExamsUser = NULL){
        // solicitar como parametro el id_cliente, para fortalecer y asegurar la consulta.
        //Certificado para las licencias TECS ECLUV
        /***************************************************/
        
        if(is_null($modExamsUser)){
            $modExamsUser = ExamsUser::model()->findAllByAttributes(array('id_licencia_usuario'=>$id));
            $logoEnCertificado=Yii::app()->user->getState('logo_en_certificado');
            $logo= Yii::app()->getBaseUrl(true)."/images/clients/tn2_".Yii::app()->user->getState('logo_cliente');
            $nombreCliente = Yii::app()->user->getState('nombre_cliente'); 
            $idCliente = Yii::app()->user->getState('cliente');
            $idUsuario = $modExamsUser[0]->idLicenciaUsuario->id_usuario;
            $nombreUsuario = $modExamsUser[0]->idLicenciaUsuario->idUsuario->nombres."&nbsp;".$modExamsUser[0]->idLicenciaUsuario->idUsuario->apellidos;
            $imagen = Yii::app()->getBaseUrl(true)."/images/users/tn2_".$modExamsUser[0]->idLicenciaUsuario->idUsuario->ruta_foto;
        } else {
            $logoEnCertificado = $modExamsUser[0]->idLicenciaUsuario->idLicenciaCliente->idCliente->logo_en_certificado;
            $logo= Yii::app()->getBaseUrl(true)."/images/clients/tn1_".$modExamsUser[0]->idLicenciaUsuario->idLicenciaCliente->idCliente->ruta_logo;
            $nombreCliente = $modExamsUser[0]->idLicenciaUsuario->idLicenciaCliente->idCliente->nombre_rsocial;
            $idCliente = $modExamsUser[0]->idLicenciaUsuario->idLicenciaCliente->id_cliente;
            $idUsuario = $modExamsUser[0]->idLicenciaUsuario->id_usuario;
            $modUsu = UsersC::model()->resetScope()->findByAttributes(array('id_usuario_c'=>$idUsuario),array('limit'=>1));
            $nombreUsuario = $modUsu->nombres."&nbsp;".$modUsu->apellidos;
            $imagen = Yii::app()->getBaseUrl(true)."/images/users/tn2_".$modUsu->ruta_foto;
        }
        //verifico si es demo
        $isDemo = "DEMO: ";
        $sql='SELECT isDemo, id_country FROM `users_c` WHERE `id_cliente` LIKE "'.$idCliente.'" AND `id_perfil` = 2';
        $list= Yii::app()->db->createCommand($sql)->queryAll();
        
        $sql='SELECT Name FROM `country` WHERE `Code` LIKE "'.$list[0]['id_country'].'"';
        $list_c= Yii::app()->db->createCommand($sql)->queryAll();
        $pais = $list_c[0]['Name'];
        
        if($list[0]['isDemo']==1){
            $nombreCliente = $isDemo.$nombreCliente;
        }
        $KEYUSERexam=$idUsuario;
       
        $KEYUSERexam = "L".substr($KEYUSERexam,1);
        $fecha = Yii::app()->dateFormatter->formatDateTime($modExamsUser[0]->fecha_presentacion,"long",NULL)."&nbsp;&nbsp;".$modExamsUser[0]->hora;
        $TRRESULTADOS='';
        $TABLARESULTADOS='<style></style><table width="100%" style="border:#FC0 0px solid;font-family: Arial, Helvetica, sans-serif;"><tr><td width="50%" style="border-right:#FC0 0px solid"  bgcolor="#E6E6E6"><table align="center"  border="0" cellpadding="0" cellspacing="0"  >
            <tr>
              <td colspan="3"  align="center"><strong>MODULE</strong></td>
              <td  >&nbsp;</td>
              <td ><strong>SCORE</strong></td>
              </tr>';
                  $CONT=0;
                  $tot=0;
                  foreach($modExamsUser as $examen){
                          $CONT=$CONT+1;
                          $examName = explode(" ",$examen->idExamen->nombre);
                          $TRRESULTADOS .=' <tr>
                          <td width="6%"  align="center"><strong>'.$CONT.'</strong></td>
                          <td width="6%"  >&nbsp;</td>
                          <td width="60%" style="text-align:left;">'.strtoupper($examName[0]).'</td>
                          <td width="5%"  >&nbsp;</td>
                          <td width="18%" align="center"><strong>'.number_format($examen->calificacion,2).'</strong></td>
                          </tr>';					
                          $tot+=$examen->calificacion;
                  }
                  $totALPROMEDIO=round($tot/$CONT,1);
                  if(((($totALPROMEDIO)>=0)&&(($totALPROMEDIO)<=12)))
                     {
                          $TECSPROFICIENCY ="1";
                          $CEFBYTECS ="Pre A1";
                          $cefrMess = 'Entry User';
                     }
                  if(((($totALPROMEDIO)>12)&&(($totALPROMEDIO)<=27)))
                     {
                          $TECSPROFICIENCY ="2";
                          $CEFBYTECS ="A1";
                          $cefrMess = 'Entry User';

                     }
                  if(((($totALPROMEDIO)>27)&&(($totALPROMEDIO)<=38)))
                     {
                          $TECSPROFICIENCY ="3";
                          $CEFBYTECS ="A2";
                          $cefrMess = 'Basic User';
                     }
                  if(((($totALPROMEDIO)>38)&&(($totALPROMEDIO)<=50)))
                     {
                          $TECSPROFICIENCY ="4";
                          $CEFBYTECS ="A2+";
                          $cefrMess = 'Basic User';
                     }
                  if(((($totALPROMEDIO)>50)&&(($totALPROMEDIO)<=62)))
                     {
                          $TECSPROFICIENCY ="5";
                          $CEFBYTECS ="B1";
                          $cefrMess = 'Intermediate User';
                     }
                  if(((($totALPROMEDIO)>62)&&(($totALPROMEDIO)<=72)))
                     {
                          $TECSPROFICIENCY ="6";
                          $CEFBYTECS ="B1+";
                          $cefrMess = 'Intermediate User';
                     }
                    if(((($totALPROMEDIO)>72)&&(($totALPROMEDIO)<=80)))
                     {
                          $TECSPROFICIENCY ="7";
                          $CEFBYTECS ="B2";
                          $cefrMess = 'Independent User';
                     }
                     if(((($totALPROMEDIO)>80)&&(($totALPROMEDIO)<=87)))
                     {
                          $TECSPROFICIENCY ="8";
                          $CEFBYTECS ="B2+";
                          $cefrMess = 'Independent User';
                     }
                     if(((($totALPROMEDIO)>87)&&(($totALPROMEDIO)<=100)))
                     {
                          $TECSPROFICIENCY ="9";
                          $CEFBYTECS ="C1";
                          $cefrMess = 'Proficient User';
                     }
              $total_scoremail=number_format($tot/$CONT,2);
              //aca
                  $TABLARESULTADOS.=$TRRESULTADOS.'</TABLE></td><td  style="background-color:#80F291;"><table width="100%"  height="96" border="0" align="center" cellpadding="0" cellspacing="0" >
            <tr>
              <td align="center">TOTAL PERCENTILE SCORE</td>
              <td align="center"><strong>'.$total_scoremail.'</strong></td>
              </tr>
              <tr><td>&nbsp;</td></tr>
         
            <tr>
              <td align="center">CEFR LEVEL</td>
              <td align="center"><strong>'.$CEFBYTECS.'</strong></td>
            </tr>
              </table></td></table>';
              $TABLARESULTADOSTOTALES='
            <div class="row" style="padding:0px;margin:0px;">
                <div class="col-xs-6" style="text-align:center;padding:5px;">
                    <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:5px;margin-bottom:5px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;border-radius: 15px 0px 0px 0px;">
                            <h6>TECS PROFICIENCY STAGE</h6> 
                        </div>  
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;border-radius: 0px 15px 0px 0px;">
                            <h6>CEFR SCORE</h6> 
                        </div>  
                    </div>
                    <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:3px;margin-bottom:3px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                            <p style="size:8px;margin:auto;padding:0px;">1</p> 
                        </div>   
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;size:8px !important;">
                            <p style="size:8px;margin:auto;padding:0px;">Pre A1</p>
                        </div>  
                    </div>
                     <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:3px;margin-bottom:3px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                            <p style="size:8px;margin:auto;padding:0px;">2</p> 
                        </div>   
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;size:8px !important;">
                            <p style="size:8px;margin:auto;padding:0px;">A1</p>
                        </div>  
                    </div>
                     <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:3px;margin-bottom:3px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                            <p style="size:8px;margin:auto;padding:0px;">3</p> 
                        </div>   
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;size:8px !important;">
                            <p style="size:8px;margin:auto;padding:0px;">A2</p>
                        </div>  
                    </div>
                     <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:3px;margin-bottom:3px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                            <p style="size:8px;margin:auto;padding:0px;">4</p> 
                        </div>   
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;size:8px !important;">
                            <p style="size:8px;margin:auto;padding:0px;">A2+</p>
                        </div>  
                    </div>
                     <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:3px;margin-bottom:3px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                            <p style="size:8px;margin:auto;padding:0px;">5</p> 
                        </div>   
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;size:8px !important;">
                            <p style="size:8px;margin:auto;padding:0px;">B1</p>
                        </div>  
                    </div>
                     <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:3px;margin-bottom:3px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                            <p style="size:8px;margin:auto;padding:0px;">6</p> 
                        </div>   
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;size:8px !important;">
                            <p style="size:8px;margin:auto;padding:0px;">B1+</p>
                        </div>  
                    </div>
                     <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:3px;margin-bottom:3px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                            <p style="size:8px;margin:auto;padding:0px;">7</p> 
                        </div>   
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;size:8px !important;">
                            <p style="size:8px;margin:auto;padding:0px;">B2</p>
                        </div>  
                    </div>
                     <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:3px;margin-bottom:3px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                            <p style="size:8px;margin:auto;padding:0px;">8</p> 
                        </div>   
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;size:8px !important;">
                            <p style="size:8px;margin:auto;padding:0px;">B2+</p>
                        </div>  
                    </div>
                     <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:3px;margin-bottom:3px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;border-radius: 0px 0px 0px 15px;">
                            <p style="size:8px;margin:auto;padding:0px;">9</p> 
                        </div>   
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;size:8px !important;border-radius: 0px 0px 15px 0px;">
                            <p style="size:8px;margin:auto;padding:0px;">C1</p>
                        </div>  
                    </div>
                    
                    
                </div>  
                <div class="col-xs-5" style="text-align:center;padding:5px;">
                    <h5 style="text-align:center;">LEVEL DESCRIPTION</h5>
                    <p style="text-align:left;font-size:10px;">According to The Common European Framework of Reference for Languages your level corresponds to:<br>
                      <strong>'.$CEFBYTECS.':&nbsp;</strong>&quot;<i>'.$this->levelExplain($CEFBYTECS).'</i>&quot;
                            '.$textoGrupo.'</p>
                </div> 
                
                
            </div>';
                          $TABLARESULTADOSTOTALCOMPLETA=
                          '
                          <table width="100%" border="0" style="border:#FC0 0px solid; background-position:center; background-repeat:no-repeat; background-position:" background="backgroundtecs.jpg">
            <tr>
              <td height="900" valign="top">

              <table width="98%" border="0" cellpadding="0" cellspacing="0" style="border:#FC0 0px solid";align="center">
            <tr border="1" style="border:#FC0 0px solid">
              <td  width="25%" bgcolor="#F2F2F2"><img src="'.Yii::app()->getBaseUrl(true).'/images/tecs-ecluv.png" width="120" style="margin:5px"></td>
              <td  height="95" bgcolor="#F2F2F2" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size:24px; font-weight:bold" >PERFORMANCE REPORT</td>
                 <td  width="25%" bgcolor="#F2F2F2" style="text-align:right"><img src="'.Yii::app()->getBaseUrl(true).'/images/logo_ILTO.png" width="120" style="margin:5px"></td>
            
            </tr>
          </table>
              <br>';
               if($logoEnCertificado==1){
                  $TABLARESULTADOSTOTALCOMPLETA.= '
                      <table width="98%" border="0" cellpadding="0" cellspacing="0" style="border-bottom:#D6D6D6 1px solid;font-size:13px;font-family: Arial, Helvetica, sans-serif;padding-bottom:5px;" align="center">
            <tr>
              <td width="15%"  bgcolor="#FFFFFF"><strong>Test Administrator</strong></td>
              <td width="1%"  bgcolor="#F2F2F2">&nbsp;</td>
              <td width="30%"  bgcolor="#F2F2F2">'.$nombreCliente.'</td>
              <td width="2%"  bgcolor="#FFFFFF">&nbsp;</td>
              <td width="15%"  bgcolor="#FFFFFF"><strong>Adm. ID</strong></td>
              <td width="2%"  bgcolor="#F2F2F2">&nbsp;</td>
              <td width="15%"  bgcolor="#F2F2F2" text-align:center;>'.$idCliente.'</td>   
              <td width="15%" rowspan="2" bgcolor="#F2F2F2"><img src="'.$logo.'" width="100"></td> 
            </tr>
            <tr>
              <td  bgcolor="#FFFFFF"><strong>Test date</strong></td>
              <td  bgcolor="#F2F2F2">&nbsp;</td>
              <td  bgcolor="#F2F2F2">'.$fecha.'</td>
              <td  bgcolor="#FFFFFF">&nbsp;</td>
              <td  bgcolor="#FFFFFF"><strong>Country</strong></td>
              <td  bgcolor="#F2F2F2">&nbsp;</td>
              <td  bgcolor="#F2F2F2">'.$pais.'</td>
            </tr>
              </table>';
               } else {
                  $TABLARESULTADOSTOTALCOMPLETA.= '<table width="98%" border="0" cellpadding="0" cellspacing="0" style="border:#FC0 0px solid;font-size:13px" align="center">
            <tr>
              <td width="23%"  bgcolor="#FFFFFF"><strong>Test Administrator</strong></td>
              <td width="1%"  bgcolor="#F2F2F2">&nbsp;</td>
              <td width="43%"  bgcolor="#F2F2F2">'.$nombreCliente.'</td>
              <td width="2%"  bgcolor="#FFFFFF">&nbsp;</td>
              <td width="13%"  bgcolor="#FFFFFF"><strong>Adm. ID</strong></td>
              <td width="1%"  bgcolor="#F2F2F2">&nbsp;</td>
              <td width="17%"  bgcolor="#F2F2F2">'.$idCliente.'</td>
            </tr>
            <tr>
              <td  bgcolor="#FFFFFF"><strong>Test date</strong></td>
              <td  bgcolor="#F2F2F2">&nbsp;</td>
              <td  bgcolor="#F2F2F2">'.$fecha.'</td>
              <td  bgcolor="#FFFFFF">&nbsp;</td>
              <td  bgcolor="#FFFFFF"><strong>Country</strong></td>
              <td  bgcolor="#F2F2F2">&nbsp;</td>
              <td  bgcolor="#F2F2F2">'.$pais.'</td>
            </tr>
              </table>';
                   
               }   
               $TABLARESULTADOSTOTALCOMPLETA .= ' 
              <br>
              <div style="width:200px; margin:auto;">
              <center>
                <strong style="font-family: Arial, Helvetica, sans-serif; font-size:16px; font-weight:bold">TEST TAKER INFORMATION </strong>
              </center></div><br>
          <table width="98%" border="0" cellpadding="0"  cellspacing="0" style="border-bottom:#D6D6D6 1px solid;font-size:13px;font-family: Arial, Helvetica, sans-serif;padding-bottom:5px;" align="center">
            <tr>
              <td width="30%" style="text-align:center;"><img style="max-height:110px;" src="'.$imagen.'" ></td> 
              <td width="33%" bgcolor="#E6E6E6">
              <table ><tr>    
              <td>&nbsp;</td>
              <td rowspan="1" ><strong>Full Name</strong></td>
              <td >&nbsp;</td>
              <td rowspan="1" >'.$nombreUsuario.'</td>
            </tr>
             <tr>
              <td>&nbsp;</td>
              <td><strong>Id Number</strong></td>
              <td>&nbsp;</td>
              <td>'.$idUsuario.'</td>
            </tr>
            <tr>
              <td >&nbsp;</td>
              <td ><strong>First Language</strong></td>
              <td >&nbsp;</td>
              <td >Spanish</td>
            </tr></table></td>
            <td width="33%"><table   border="0" align="center" cellpadding="2" cellspacing="0" style="font-size:14px">
            <tr>
              <td height="60" align="center"  bgcolor="#FFFFFF"><strong style="font-size:20px; font-family:Arial, Helvetica, sans-serif; font-weight:bold">Fraud Prevention Code</strong></td>
              </tr>
            <tr>
              <td  height="40" align="center"  bgcolor="#FFFFFF" style="font-size:20px; font-family:Arial, Helvetica, sans-serif; font-weight:bold">'.$KEYUSERexam.'</td>
              </tr>
              <tr><td style="font-size:10px; font-weight:bold;"  bgcolor="#FFFFFF">To verify this is a valid test, please enter the code in the fraud prevention code section in the website www.iltoexams.com</td></tr>
              </table></td>
            </tr>
              </table>          <br>              
<center>
            <strong><span style="font-family:Arial, Helvetica, sans-serif;">TEST RESULTS<span>
            </strong>
          </center>
          <br>
          '.$TABLARESULTADOS.'    <br>
              '.$TABLARESULTADOSTOTALES.'
           </td>
              </tr>
             </table><p style=" font-size:10px; ">The TECS &ndash; Test of English Communication Skills is a product of ILTO &ndash; International Language Testing Organization. All rights Reserved&copy; </p>';
            if($list[0]['isDemo']==1){
        
                $TABLARESULTADOSTOTALCOMPLETA.= '<div id="isDemo" style="position:absolute;top:50px;left:5px;"><img src="'.Yii::app()->getBaseUrl(true).'/images/isDemo.png" width="1500"/></div>';
            } 
             
          '</table></span>';
        /***************************************************/
        return $TABLARESULTADOSTOTALCOMPLETA;        
    }
    
    
    function getSuperScoreCertificate($id, $exams, $modExamsUser = NULL){
        	
        
        // solicitar como parametro el id_cliente, para fortalecer y asegurar la consulta.
        /**************************************************0*/
        
        if(is_null($modExamsUser)){
        $modExamsUser = array();    
            foreach ($exams as $exam) {
                $SQL = "SELECT * FROM `exams_user` WHERE `id_examen_usuario` = '".$exam[2]."'";
        	    $exams_list = Yii::app()->db->createCommand($SQL)->queryAll();
        	    array_push($modExamsUser,$exams_list[0]);   
            }
            	
            //$modExamsUser = ExamsUser::model()->findAllByAttributes(array('id_licencia_usuario'=>$id));
            
            $logoEnCertificado=Yii::app()->user->getState('logo_en_certificado');
            $idCliente = Yii::app()->user->getState('cliente');
            $idUsuario = $id;
            $sql='SELECT nombres, apellidos, ruta_foto FROM `users_c` WHERE `id_usuario_c` = "'.$idUsuario.'"';
            $user= Yii::app()->db->createCommand($sql)->queryAll();
            $nombreUsuario = ucwords($user[0]["nombres"])."&nbsp;".ucwords($user[0]["apellidos"]);
            
            $grupo = $modExamsUser[0]->idLicenciaUsuario->idUsuario->id_grupo_cliente;
            $id_licencia = $modExamsUser[0]->idLicenciaUsuario->idLicenciaCliente->id_licencia;
            
            
        } else {
            $logoEnCertificado = $modExamsUser[0]->idLicenciaUsuario->idLicenciaCliente->idCliente->logo_en_certificado;
            $idUsuario = $id;
            $modUsu = UsersC::model()->resetScope()->findByAttributes(array('id_usuario_c'=>$idUsuario),array('limit'=>1));
            $sql='SELECT nombres, apellidos, ruta_foto FROM `users_c` WHERE `id_usuario_c` = "'.$idUsuario.'"';
            $user= Yii::app()->db->createCommand($sql)->queryAll();
            $nombreUsuario = ucwords($user[0]["nombres"])."&nbsp;".ucwords($user[0]["apellidos"]);
            $grupo = $modUsu->id_grupo_cliente;
            $imagen = Yii::app()->getBaseUrl(true)."/images/users/tn2_".$modUsu->ruta_foto;
        }
        
        //fotografía del usuario
        $SQL = "SELECT ruta_foto FROM `users_c` WHERE `id_usuario_c` = '".$id."'";
        $imgt = Yii::app()->db->createCommand($SQL)->queryAll();
        $imagen = Yii::app()->getBaseUrl(true)."/images/users/tn2_".$img[0]["ruta_foto"];
        
        
        //get ID CLIENT AND LOGO
        $idCliente = Yii::app()->user->getState('cliente');
        $sql='SELECT id_cliente, nombre_rsocial, ruta_logo  FROM `clients` WHERE `id_cliente` = "'.$idCliente.'"';
        $list= Yii::app()->db->createCommand($sql)->queryAll();
        
        $nombreCliente = $list[0]["nombre_rsocial"];
        $logo = Yii::app()->getBaseUrl(true)."/images/clients/tn2_".$list[0]["ruta_logo"];
        //var_dump($logo);die();
        
        $isDemo = "DEMO: ";
        $sql='SELECT isDemo, id_country, id_grupo_cliente FROM `users_c` WHERE `id_cliente` LIKE "'.$idCliente.'" AND `id_perfil` = 2';
        $list= Yii::app()->db->createCommand($sql)->queryAll();
        
        if($grupo == '607'){ 
                $textoGrupo = '<span style="color:red;">This certificate is only valid for work purposes, This is not valid for academic organizations or graduation requirements.</span>';
        }else{
             $textoGrupo = "";
        }
        
        $SQL = 'SELECT * FROM `proctorv_request` WHERE `id_user` = "'.$idUsuario.'"';
        
        $tecs2go = Yii::app()->db->createCommand($SQL)->queryAll();
       
        if(count($tecs2go)>0){
            $imagen = Yii::app()->getBaseUrl(true)."/images/proctorv_users/".$user[0]["ruta_foto"];
        }
        
        //logo tipo certificado 
        $logoLicencia = Yii::app()->getBaseUrl(true)."/images/tecs.png";
        
        //verifico si es demo
        $isDemo = "DEMO: ";
        $sql='SELECT isDemo, id_country, id_grupo_cliente FROM `users_c` WHERE `id_cliente` LIKE "'.$idCliente.'" AND `id_perfil` = 2';
        $list= Yii::app()->db->createCommand($sql)->queryAll();
        
        $sql='SELECT Name FROM `country` WHERE `Code` LIKE "'.$list[0]['id_country'].'"';
        $list_c= Yii::app()->db->createCommand($sql)->queryAll();
        $pais = $list_c[0]['Name'];
       
        if($list[0]['isDemo']==1){
            $nombreCliente = $isDemo.$nombreCliente;
        }
       
        
        $KEYUSERexam=(1000000 + $modExamsUser[0]["id_licencia_usuario"]);
        
        $KEYUSERexam = "L".substr($KEYUSERexam,1);
        
        
        $fecha = Yii::app()->dateFormatter->formatDateTime($fecha = date("Y-m-d h:i"),"long","short");
        
        
        
        $TRRESULTADOS='';
        $TABLARESULTADOS='
        <!--Basic Styles-->
        <link href="/ilto3/themes/ilto/html/assets/css/bootstrap.min.css" rel="stylesheet" />
        <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
        <link href="/ilto3/themes/ilto/html/assets/css/font-awesome.min.css" rel="stylesheet" />
        <link href="/ilto3/themes/ilto/html/assets/css/weather-icons.min.css" rel="stylesheet" />
        
        <!--Fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

        <style>
        h3,h4,h5 {
            font-style: normal;
            font-weight: bold;
            line-height: 1.54;
            font-family: "Montserrat", sans-serif;
            color:#333;
        }
        
        </style>
        ';
            $CONT=0;
            $tot=0;
            foreach($modExamsUser as $examen){
                $CONT=$CONT+1;
                $sql='SELECT nombre FROM `exams` WHERE `id_examen` LIKE "'.$examen["id_examen"].'"';
                    
                $examName= Yii::app()->db->createCommand($sql)->queryAll();
                $examName = explode(" ",$examName[0]["nombre"]);
                $date_ans = explode(" ",$examen["fecha_presentacion"]);
                $TRRESULTADOS .='
                    <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:5px;margin-bottom:5px;size:10px;">
                        <div class="col-xs-1" style="text-align:center;padding:5px;height:25px;padding:0px;margin-left:75px;background:#E4E4E4;">
                            <strong>'.$CONT.'</strong>
                        </div>   
                        <div class="col-xs-4" style="text-align:left;padding:5px;height:25px;padding:0px;margin-left:5px;background:#E4E4E4;size:10px;"><span style="left:10px;">&nbsp;'.$date_ans[0].'</span>
                            '.strtoupper($examName[0]).' - '.$examen["nivel"].'
                        </div>  
                        <div class="col-xs-4" style="text-align:center;padding:5px;height:25px;padding:0px;margin-left:5px;background:#AEE1B6;size:10px;">
                            <strong>'.number_format($examen["calificacion"],2).'</strong>
                        </div>  
                    </div>';					
                $tot+=$examen["calificacion"];
            }
            $totALPROMEDIO=round($tot/$CONT,2);
            
                  if(((($totALPROMEDIO)>=0)&&(($totALPROMEDIO)<=12)))
                     {
                          $TECSPROFICIENCY ="1";
                          $CEFBYTECS ="Pre A1";
                          $cefrMess = 'Entry User';
                     }
                  if(((($totALPROMEDIO)>12)&&(($totALPROMEDIO)<=27)))
                     {
                          $TECSPROFICIENCY ="2";
                          $CEFBYTECS ="A1";
                          $cefrMess = 'Entry User';

                     }
                  if(((($totALPROMEDIO)>27)&&(($totALPROMEDIO)<=38)))
                     {
                          $TECSPROFICIENCY ="3";
                          $CEFBYTECS ="A2";
                          $cefrMess = 'Basic User';
                     }
                  if(((($totALPROMEDIO)>38)&&(($totALPROMEDIO)<=50)))
                     {
                          $TECSPROFICIENCY ="4";
                          $CEFBYTECS ="A2+";
                          $cefrMess = 'Basic User';
                     }
                  if(((($totALPROMEDIO)>50)&&(($totALPROMEDIO)<=62)))
                     {
                          $TECSPROFICIENCY ="5";
                          $CEFBYTECS ="B1";
                          $cefrMess = 'Intermediate User';
                     }
                  if(((($totALPROMEDIO)>62)&&(($totALPROMEDIO)<=72)))
                     {
                          $TECSPROFICIENCY ="6";
                          $CEFBYTECS ="B1+";
                          $cefrMess = 'Intermediate User';
                     }
                    if(((($totALPROMEDIO)>72)&&(($totALPROMEDIO)<=80)))
                     {
                          $TECSPROFICIENCY ="7";
                          $CEFBYTECS ="B2";
                          $cefrMess = 'Independent User';
                     }
                     if(((($totALPROMEDIO)>80)&&(($totALPROMEDIO)<=87)))
                     {
                          $TECSPROFICIENCY ="8";
                          $CEFBYTECS ="B2+";
                          $cefrMess = 'Independent User';
                     }
                     if(((($totALPROMEDIO)>87)&&(($totALPROMEDIO)<=100)))
                     {
                          $TECSPROFICIENCY ="9";
                          $CEFBYTECS ="C1";
                          $cefrMess = 'Proficient User';
                     }
              $total_scoremail=number_format($tot/$CONT,2);
              
                
            $TABLARESULTADOSTOTALES='
            <div class="row" style="padding:0px;margin:0px;">
                <div class="col-xs-6" style="text-align:center;padding:5px;">
                    <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:5px;margin-bottom:5px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;border-radius: 15px 0px 0px 0px;">
                            <h6>TECS PROFICIENCY STAGE</h6> 
                        </div>  
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;border-radius: 0px 15px 0px 0px;">
                            <h6>CEFR SCORE</h6> 
                        </div>  
                    </div>
                    <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:3px;margin-bottom:3px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                            <p style="size:8px;margin:auto;padding:0px;">1</p> 
                        </div>   
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;size:8px !important;">
                            <p style="size:8px;margin:auto;padding:0px;">Pre A1</p>
                        </div>  
                    </div>
                     <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:3px;margin-bottom:3px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                            <p style="size:8px;margin:auto;padding:0px;">2</p> 
                        </div>   
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;size:8px !important;">
                            <p style="size:8px;margin:auto;padding:0px;">A1</p>
                        </div>  
                    </div>
                     <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:3px;margin-bottom:3px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                            <p style="size:8px;margin:auto;padding:0px;">3</p> 
                        </div>   
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;size:8px !important;">
                            <p style="size:8px;margin:auto;padding:0px;">A2</p>
                        </div>  
                    </div>
                     <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:3px;margin-bottom:3px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                            <p style="size:8px;margin:auto;padding:0px;">4</p> 
                        </div>   
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;size:8px !important;">
                            <p style="size:8px;margin:auto;padding:0px;">A2+</p>
                        </div>  
                    </div>
                     <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:3px;margin-bottom:3px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                            <p style="size:8px;margin:auto;padding:0px;">5</p> 
                        </div>   
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;size:8px !important;">
                            <p style="size:8px;margin:auto;padding:0px;">B1</p>
                        </div>  
                    </div>
                     <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:3px;margin-bottom:3px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                            <p style="size:8px;margin:auto;padding:0px;">6</p> 
                        </div>   
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;size:8px !important;">
                            <p style="size:8px;margin:auto;padding:0px;">B1+</p>
                        </div>  
                    </div>
                     <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:3px;margin-bottom:3px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                            <p style="size:8px;margin:auto;padding:0px;">7</p> 
                        </div>   
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;size:8px !important;">
                            <p style="size:8px;margin:auto;padding:0px;">B2</p>
                        </div>  
                    </div>
                     <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:3px;margin-bottom:3px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;">
                            <p style="size:8px;margin:auto;padding:0px;">8</p> 
                        </div>   
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;size:8px !important;">
                            <p style="size:8px;margin:auto;padding:0px;">B2+</p>
                        </div>  
                    </div>
                     <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:3px;margin-bottom:3px;">
                        <div class="col-xs-6" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:75px;background:#E4E4E4;border-radius: 0px 0px 0px 15px;">
                            <p style="size:8px;margin:auto;padding:0px;">9</p> 
                        </div>   
                        <div class="col-xs-3" style="text-align:center;padding:5px;height:15px;padding:0px;margin-left:5px;background:#AEE1B6;size:8px !important;border-radius: 0px 0px 15px 0px;">
                            <p style="size:8px;margin:auto;padding:0px;">C1</p>
                        </div>  
                    </div>
                    
                    
                </div>  
                <div class="col-xs-5" style="text-align:center;padding:5px;">
                    <h5 style="text-align:center;">LEVEL DESCRIPTION</h5>
                    <p style="text-align:left;font-size:10px;">According to The Common European Framework of Reference for Languages your level corresponds to:<br>
                      <strong>'.$CEFBYTECS.':&nbsp;</strong>&quot;<i>'.$this->levelExplain($CEFBYTECS).'</i>&quot;
                            '.$textoGrupo.'</p>
                </div> 
                
                
            </div>';
          
                
            $TABLARESULTADOSTOTALCOMPLETA='<div class="row" style="border-right:2px solid #AEE1B6;border-bottom:2px solid #AEE1B6;text-align:center;">
                <div class="row" style="padding:0px;margin:0px;text-align:center;">
                    <div class="col-xs-3" style="height:50px;padding:0px;margin:0px;">
                        <img src="'.$logoLicencia.'" >
                    </div>
                    <div class="col-xs-5" style="text-align:center;padding:0px;height:50px;padding:0px;margin:0px;padding-top:5px;">
                        <h3 style="padding-bottom:2px;margin-bottom:0px;">SUPERSCORE</h3>
                         <p>PERFORMANCE REPORT</p>
                    </div>
                    <div class="col-xs-3" style="text-align:right;height:50px;padding:0px;margin:0px;">
                        <img src="'.Yii::app()->getBaseUrl(true).'/images/logo_ilto_original.png" width="200">
                    </div>
                </div>';
               if($logoEnCertificado==1){
                  $TABLARESULTADOSTOTALCOMPLETA.= '
                <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:15px;">
                    <div class="col-xs-3" style="height:120px;padding:0px;margin:0px;">
                        <img style="max-height:110px;" src="'.Yii::app()->getBaseUrl(true).'/images/cefr-flag.jpg" style="margin-top:1px;">
                        <h2 style="padding:0px;margin:5px auto;">'.$CEFBYTECS.'</h2>
                        <h4 style="padding:0px;margin:5px auto;">'.$cefrMess.'</h4>
                    </div>
                    <div class="col-xs-8" style="text-align:center;padding:0px;height:150px;padding:0px;margin:0px;border-radius: 0px 15px 0px 0px;background:#AEE1B6;font-size:12px;">
                        <h5 style="padding:0px;margin:5px auto;">TEST TAKER INFORMATION</h5>
                        <div class="row" style="padding:0px 10px;margin:0px;text-align:left;">
                            <div class="col-xs-7" style="height:50px;padding:10px;margin:0px;">
                                <label>Name</label><br/>
                                <div style="background:#FEFEFE;padding:5px;border-radius: 10px;text-align:center;">'.$nombreUsuario.'</div>
                                <label>Id Number:</label>
                                <div style="display: inline;background:#FEFEFE;padding:5px;border-radius: 10px;text-align:center;">'.$idUsuario.'</div>
                                <label>First Language:</label>
                                <div style="display: inline;background:#FEFEFE;padding:5px;border-radius: 10px;text-align:center;">Spanish</div>
                            </div>
                            <div class="col-xs-4" style="height:50px;padding:5px;margin:0px;text-align:center;">
                                <h6 style="font-weight:600;padding:0px;margin:5px auto;">Fraud Prevention Code</h6>
                                <h4 style="padding:0px;margin:10px auto;"><b>'.$KEYUSERexam.'</b></h4>
                                <p style="text-align:center;font-size:10px;">To verify this is a valid test, please enter the code in the fraud prevention code section in the website www.iltoexams.com</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:5px;font-size:12px;">
                    <div class="col-xs-3" style="height:150px;padding:0px;margin:0px;">
                    <img style="max-height:110px;" src="'.$imagen.'" >
                    
                    </div>
                    <div class="col-xs-8" style="text-align:center;padding:5px;height:50px;padding:0px;margin:0px;border-radius:0px 0px 15px 15px;background:#AEE1B6;">
                        <div class="row" style="padding:5px;margin:0px;text-align:left;">
                            <div class="col-xs-6" style="height:50px;padding:10px;margin:0px;">
                                <label>Test Administrator</label><br/>
                                <div style="background:#FEFEFE;padding:5px;border-radius: 10px;text-align:center;">'.$nombreCliente.'</div>
                                <label>Id Test date:</label>
                                <div style="display: inline;background:#FEFEFE;padding:5px;border-radius: 10px;text-align:center;">'.$fecha.'</div>
                            </div>
                            <div class="col-xs-5" style="height:50px;padding:5px;margin:0px;text-align:center;">
                                <label>Adm. ID</label><br/>
                                <div style="background:#FEFEFE;padding:5px;border-radius: 10px;">'.$idCliente.'</div>
                                <label>Country</label>
                                <div style="display: inline;background:#FEFEFE;padding:5px;border-radius: 10px;">'.$pais.'</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:0px;">
                    
                    <div class="col-xs-5" style="text-align:center;padding:5px;height:20px;padding:0px;margin-left:50px;border-radius:15px 15px 0px 0px;background:#AEE1B6;">
                        <h6 style="font-weight:bold;">TOTAL PERCENTILE SCORE</h6>
                    </div>   
                    <div class="col-xs-5" style="text-align:center;padding:5px;height:20px;padding:0px;margin-left:25px;border-radius:15px 15px 0px 0px;background:#AEE1B6;">
                        <h6 style="font-weight:bold;">CEFR LEVEL</h6>
                    </div>  
                </div>
                <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:5px;margin-bottom:10px;">
                    <div class="col-xs-5" style="text-align:center;padding:5px;height:30px;padding:0px;margin-left:50px;border-radius:0px 0px 15px 15px;background:#E4E4E4;">
                    '.$total_scoremail.'
                    </div>   
                    <div class="col-xs-5" style="text-align:center;padding:5px;height:30px;padding:0px;margin-left:25px;border-radius:0px 0px 15px 15px;background:#E4E4E4;">
                    '.$CEFBYTECS.'
                    </div>  
                </div>
                
                <div class="row" style="padding:0px;margin:0px;text-align:center;margin-top:10px;margin-bottom:5px;">
                <h4 style="margin:auto;">INDIVIDUAL COMMUNICATION SKILL PERFORMANCE</h4>
                    <div class="col-xs-1" style="text-align:center;padding:5px;height:32px;padding:0px;margin-left:75px;border-radius:15 0px 0px 0px;background:#E4E4E4;">
                    </div>   
                    <div class="col-xs-4" style="text-align:center;padding:5px;height:30px;padding:0px;margin-left:5px;background:#E4E4E4;">
                        <h6>MODULE</h6>
                    </div>  
                    <div class="col-xs-4" style="text-align:center;padding:5px;height:30px;padding:0px;margin-left:5px;border-radius:0px 15px 0px 0px;background:#AEE1B6;">
                        <h6>SCORE</h6>
                    </div>  
                </div>
                ';
                $TABLARESULTADOSTOTALCOMPLETA .= $TRRESULTADOS; 
               }   
               $TABLARESULTADOSTOTALCOMPLETA .= ' 
              
          '.$TABLARESULTADOS.'   
              '.$TABLARESULTADOSTOTALES;
             
             $TABLARESULTADOSTOTALCOMPLETA.= '<p style="font-size:10px;text-align:center;">The TECS – Test of English Communication Skills is a product of ILTO – International Language Testing Organization. All rights Reserved &copy; </p>';
            if($list[0]['isDemo']==1){
        
                $TABLARESULTADOSTOTALCOMPLETA.= '<div id="isDemo" style="position:absolute;top:50px;left:5px;"><img src="'.Yii::app()->getBaseUrl(true).'/images/isDemo.png" width="1500"/></div>';
            } 
             
          '</div>';
          
        /***************************************************/
        return $TABLARESULTADOSTOTALCOMPLETA;          
    
        
    }
    
    
    public function enviarCertificado($idUsuario,$id){
        
        $modUsuario = UsersC::model()->findByAttributes(array('id_cliente'=>Yii::app()->user->getState('cliente'), 'id_usuario_c'=>$idUsuario));
        $sql='SELECT * FROM `users_c` WHERE `id_usuario_c` ="'.$idUsuario.'"';
        $modUsuario= Yii::app()->db->createCommand($sql)->queryAll();
        
        $sql='SELECT id_licencia_cliente FROM `licenses_user` WHERE `id_licencias_usuario` ="'.$id.'"';
        $list= Yii::app()->db->createCommand($sql)->queryAll();
        
        $sql='SELECT id_licencia FROM `licenses_client` WHERE `id_licencias_cliente` ="'.$list[0]["id_licencia_cliente"].'"';
        $licencia_tipo= Yii::app()->db->createCommand($sql)->queryAll();
        $idLicenciaTipo = $licencia_tipo[0]["id_licencia"];
        
        switch ($idLicenciaTipo) {
            case 6:
                $informe = $this->generaCertificadoEde($id);
                break;
            case 7:
                $informe = $this->generaCertificado($id);
                break;
            case 15:
               $informe = $this->generaCertificadoAa($id);
                break;
            case 16:
                $informe = $this->generaCertificadoEde($id);
                break;
            case 18:
                $informe = $this->generaCertificado($id);
                break;
            case 19:
                $informe = $this->generaCertificadoAa($id);
                break;
            case 21:
                $informe = $this->generaCertificadoEde($id);
                break;
            case 23:
                $informe = $this->generaCertificadoAa($id);
                break;
                //new license
            case 25:
                $informe = $this->generaCertificadoAa($id);
                break;
            case 27:
                $informe = $this->generaCertificadoEde($id);
                break;
            default:
                $informe = $this->generaCertificadoEde($id);
        }
       
        
        //require '/home/thetec/public_html/vendor/autoload.php';
		require '/home/iltoexam/public_html/vendor/autoload.php';
					
					
					
		$mpdf = new \Mpdf\Mpdf(['tempDir' => __DIR__ . '/../temp']);
		$mpdf->WriteHTML($informe);	
		
        $nombre = "cert_".$idUsuario.".pdf";
        $content = $mpdf->Output('','S');
        $content = chunk_split(base64_encode($content));
        $mailto = $modUsuario[0]["email"]; //Mailto here
        $from_name = 'ILTO'; //Name of sender mail
        $from_mail = 'noreply@iltoexams.com'; //Mailfrom here
        $subject = 'Test Certificate Attachment'; 
        $message = '<p><h1 style="color:black;">Dear '.$modUsuario[0]["nombres"].'</h1></p><p style="font-size:20px;">Thank you for taking the <span style="color:#F69A40; font-weight:bold;">TECS</span> (Test of English Communication Skills).
            We are glad you are one of the thousands of candidates who take this test in order to know their English communication level from a reliable source.<br>
            Please contact your test administrator for a copy of the test certificate with your results</p>
            <p style="font-size:18px;"><br><br><br>Best regards,<br><br>Matt Kettering<br>Head of Academic Support<br><img src="http://www.iltoexams.com/ilto3/images/logo_ILTO.png"/></p>';
           
        $filename = "cert_".$idUsuario.".pdf";
        //Headers of PDF and e-mail
        $boundary = "XYZ-" . date("dmYis") . "-ZYX"; 
        $header .= "Content-Transfer-Encoding: 8bits\r\n"; 
        $header .= "Content-Type: text/html; charset=ISO-8859-1\r\n\r\n"; // or utf-8
        $header .= "$message\r\n";
        $header .= "--$boundary\r\n";
        $header .= "Content-Type: application/pdf; name=\"".$filename."\"\r\n";
        $header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n";
        $header .= "Content-Transfer-Encoding: base64\r\n\r\n";
        $header .= "$content\r\n"; 
        $header .= "--$boundary--\r\n";
        $header2 = "MIME-Version: 1.0\r\n";
        $header2 .= "From: ".$from_name." <".$from_mail."> \r\n"; 
        $header2 .= "Return-Path: $from_mail\r\n";
        $header2 .= "Content-type: multipart/mixed; boundary=\"$boundary\"\r\n";
        $header2 .= "$boundary\r\n";
        
        if(mail($mailto,$subject,$header,$header2, "-r".$from_mail)){
            Yii::app()->user->setFlash('success','An Email was sent to '.$modUsuario[0]["email"].' with the certificate');
            return true;
        } else {
            $error = print_r(error_get_last(),true);
            Yii::app()->user->setFlash('error',$error);
            return false;
        }
        
    } 
    
    public function calificaLicencia($license){
        $CONT=0;
        $tot=0;
        $totalPROMEDIO=0;
        $CEFBYTECS="";
        $modExamsUser = ExamsUser::model()->findAllByAttributes(array('id_licencia_usuario'=>$license));
        
        
        
        $spekingScore = floatval($modExamsUser[0]["calificacion"]);
        $listeningScore = floatval($modExamsUser[3]["calificacion"]); 
        $diff = $spekingScore - $listeningScore;
        $status = 'F';
        if($this->isTecs($license) == true){
            //get difference between exams with absolute values   
            $diff =  abs(floatval($modExamsUser[0]["calificacion"]) - floatval($modExamsUser[3]["calificacion"]));
            $diff2 = abs(floatval($modExamsUser[0]["calificacion"]) - floatval($modExamsUser[1]["calificacion"]));
            $diff3 = abs(floatval($modExamsUser[0]["calificacion"]) - floatval($modExamsUser[2]["calificacion"]));
            $diff4 = abs(floatval($modExamsUser[1]["calificacion"]) - floatval($modExamsUser[2]["calificacion"]));        
            $diff5 = abs(floatval($modExamsUser[1]["calificacion"]) - floatval($modExamsUser[3]["calificacion"])); 
            $diff6 = abs(floatval($modExamsUser[2]["calificacion"]) - floatval($modExamsUser[3]["calificacion"])); 
        
            // flag check under revision
            $under_revision = 0;
            
            //if some diff is 40 or more set flag to 1    
            if($diff >= 40 || $diff2 >= 40 || $diff3 >= 40){
                $under_revision = 1;
            }
        
            //if some diff is 40 or more set flag to 1       
            if($diff4 >= 40 || $diff5 >= 40 || $diff6 >= 40){
                $under_revision = 1;
            }
        
         //if flag is 1, set the license to under revision    
            if($under_revision  == 1){
                $status = 'U';
                // revoke exams
                $SQL = "UPDATE exams_user SET estado = 'U' WHERE id_licencia_usuario='".$license."'";
                $update = Yii::app()->db->createCommand($SQL)->query();
                
                //revoke user
                //$SQL = "UPDATE users_c SET email_certificate = 0, estado = 'B' WHERE id_usuario_c='".$userId."'";
                //$update = Yii::app()->db->createCommand($SQL)->query();
                    
                //revoke license
                $SQL = "UPDATE licenses_user SET estado = 'U' WHERE id_licencias_usuario='".$license."'";
                $update = Yii::app()->db->createCommand($SQL)->query();
                        
                //insert log to REVOKED user
                $SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 4, '".$userId."', NOW(), '".$license."', '".$examUserId."', 'REVOKED BY FRAUD: HIGH DIFERENCE BETWEEN SPEAKING AND LISTENING SCORES', '".$userId."')";
                $log= Yii::app()->db->createCommand($SQL)->query();
                        
                //verify DEMO ID
                //$SQL = "SELECT * FROM users_c  WHERE id_usuario_c ='".$userId."'";
                //$usr= Yii::app()->db->createCommand($SQL)->queryAll();
                        
                //$SQL = "INSERT INTO `fraud_report`(`id`, `document`, `first_name`, `sure_name`, `email`, `id_client`, `picture`, `id_user_report`, `name_user_report`, `report_date`, `comments`, `exam`, `type`) VALUES (NULL,'".$userId."','".$usr[0]["nombres"]."','".$usr[0]["apellidos"]."','".$usr[0]["email"]."','".$usr[0]["id_cliente"]."',NULL,'SYSTEM', 'SYSTEM',NOW(),'REVOKED BY FRAUD: HIGH DIFERENCE BETWEEN SPEAKING AND LISTENING SCORES',25,5)";
                //$fraud = Yii::app()->db->createCommand($SQL)->query();
                        
                        
            }
        }
        
                
        
       
        //consultar informacion del cliente
        $SQL = "SELECT id_usuario, nivel, calificacion  FROM `licenses_user` WHERE `id_licencias_usuario` = '".$license."'";
        $user= Yii::app()->db->createCommand($SQL)->queryAll();
        
        foreach($modExamsUser as $examen){
                $CONT=$CONT+1;
                $tot+=$examen->calificacion;
        }
       
        if($CONT>0){
            $totALPROMEDIO=round($tot/$CONT,2);
            if(((($totALPROMEDIO)>=0)&&(($totALPROMEDIO)<=12)))
                     {
                          $TECSPROFICIENCY ="1";
                          $CEFBYTECS ="PA1";
                          $cefrMess = 'Entry User';
                     }
                  if(((($totALPROMEDIO)>12)&&(($totALPROMEDIO)<=27)))
                     {
                          $TECSPROFICIENCY ="2";
                          $CEFBYTECS ="A1";
                          $cefrMess = 'Entry User';

                     }
                  if(((($totALPROMEDIO)>27)&&(($totALPROMEDIO)<=38)))
                     {
                          $TECSPROFICIENCY ="3";
                          $CEFBYTECS ="A2";
                          $cefrMess = 'Basic User';
                     }
                  if(((($totALPROMEDIO)>38)&&(($totALPROMEDIO)<=50)))
                     {
                          $TECSPROFICIENCY ="4";
                          $CEFBYTECS ="A2+";
                          $cefrMess = 'Basic User';
                     }
                  if(((($totALPROMEDIO)>50)&&(($totALPROMEDIO)<=62)))
                     {
                          $TECSPROFICIENCY ="5";
                          $CEFBYTECS ="B1";
                          $cefrMess = 'Intermediate User';
                     }
                  if(((($totALPROMEDIO)>62)&&(($totALPROMEDIO)<=72)))
                     {
                          $TECSPROFICIENCY ="6";
                          $CEFBYTECS ="B1+";
                          $cefrMess = 'Intermediate User';
                     }
                    if(((($totALPROMEDIO)>72)&&(($totALPROMEDIO)<=80)))
                     {
                          $TECSPROFICIENCY ="7";
                          $CEFBYTECS ="B2";
                          $cefrMess = 'Independent User';
                     }
                     if(((($totALPROMEDIO)>80)&&(($totALPROMEDIO)<=87)))
                     {
                          $TECSPROFICIENCY ="8";
                          $CEFBYTECS ="B2+";
                          $cefrMess = 'Independent User';
                     }
                     if(((($totALPROMEDIO)>87)&&(($totALPROMEDIO)<=100)))
                     {
                          $TECSPROFICIENCY ="9";
                          $CEFBYTECS ="C1";
                          $cefrMess = 'Proficient User';
                     }
            $total_scoremail=round($tot/$CONT,2);
        }
        
       
        return array("calificacion"=>$totALPROMEDIO,"nivel"=>$CEFBYTECS,"estado"=>$status);
        
    }
    

    
    
    public function sendEmailNotification($user, $license){
        //consultar informacion del cliente
        
        
        $SQL = "SELECT nombres, apellidos, users_c.id_cliente, users_c.telefono, nombre_rsocial FROM `users_c` INNER JOIN clients ON users_c.id_cliente = clients.id_cliente WHERE id_usuario_c = '".$user."'";
        $usr= Yii::app()->db->createCommand($SQL)->queryAll();
        
        
         //consultar el correo del total
        $SQL = "SELECT email FROM `users_c` WHERE `id_cliente` LIKE '".$usr[0]["id_cliente"]."' AND `id_perfil` = 2";
        $toaa= Yii::app()->db->createCommand($SQL)->queryAll();
        
        //get exams by license
        $SQL = "SELECT id_examen_usuario, exams.nombre as test, calificacion as score, exams_user.nivel as CERF FROM `exams_user` 
        INNER JOIN exams ON exams.id_examen = exams_user.id_examen WHERE `id_licencia_usuario` = '".$license."'";
        
        $exams = Yii::app()->db->createCommand($SQL)->queryAll();
                
                $to = $toaa[0]["email"];
                $name = $usr[0]["nombres"].' '.$usr[0]["apellidos"];
                $phone = $usr[0]["telefono"];
                $admin = $usr[0]["nombre_rsocial"];
                
                $subject = "Certificate Anomaly";
                $htmlBody = '<p>The following TECS certificate has been placed on hold, due to an anomaly detected:<br/><br/>
                    <b>Name: </b>'.ucfirst($name).'<br/><br/>
                    <b>ID: </b>'.$user.'<br/><br/>
                    <b>Contact number: </b>'.$phone.'<br/><br/>
                    <b>Test Administrator:</b> '.$admin.'<br/><br/>
                    <b>Certificate Anomaly: </b>
            		The results of the speaking section and the listening section of the test are not coherent, as the difference between their scores is 50% or more.<br/><br/>
            		The speaking section assesses the skills of a person to communicate in a verbal manner, where demonstration of listening and speaking proficiency must be coherent. It is evident the relevance to understand a spoken idea in order to be able to deliver an answer in a normal communication process.<br/><br/>
                    According to the terms and conditions accepted by the test taker before taking the TECS, ILTO reserves the right to contact the test taker to confirm the speaking level as well as to revoke the certificate if there is evidence of fraud or the scores inconsistency remains. <br/><br/>
            		
        		</p>
        		<style>
        		table{border-collapse:collapse}caption{padding-top:.75rem;padding-bottom:.75rem;color:#6c757d;text-align:left;caption-side:bottom}th{text-align:inherit}label{display:inline-block;margin-bottom:.5rem}
        		</style>
        		<table class="table table" style="text-align:center; >
        		        <thead style="padding:5px;" border="1">
        		            <tr>
        		                <th>
        		                    Test
        		                </th>
        		                <th>
        		                    Score
        		                </th>
        		                <th>
        		                    CERF
        		                </th>
        		                <th>
        		                    Date Start
        		                </th>
        		                <th>
        		                    Date End
        		                </th>
        		                <th>
        		                    Time spent
        		                </th>
        		            </tr>
        		        </thead>
        		        <tbody>';
        		
        		foreach($exams as $exam){
        		    //GET DATE START BY EXAM
                    $SQL = "SELECT date_answered FROM `answers_exam_user` WHERE `id_examen_usuario` = '".$exam["id_examen_usuario"]."' ORDER BY `answers_exam_user`.`date_answered` ASC LIMIT 1";
                    $date_start= Yii::app()->db->createCommand($SQL)->queryAll();
                    
                    //GET DATE END BY EXAM
                    $SQL = "SELECT date_answered FROM `answers_exam_user` WHERE `id_examen_usuario` = '".$exam["id_examen_usuario"]."' ORDER BY `answers_exam_user`.`date_answered` DESC LIMIT 1";
                    $date_end= Yii::app()->db->createCommand($SQL)->queryAll();
                    
                    //cast date_strings to DateTime
                    $dt_start = new Datetime($date_start[0]["date_answered"]);
                    $dt_end = new Datetime($date_end[0]["date_answered"]);
                    
                    //get the interval between the both dates                
                    $interval = date_diff($dt_start, $dt_end);
                	//get the minutes interval
                	$minutes = explode("+",$interval->format('%i'));
        		    $htmlBody .= "
        		        <tr>
        		            <td>
        		                ".$exam["test"]."
        		            </td>
        		            <td>
        		                ".$exam["score"]."
        		            </td>
        		            <td>
        		                ".$exam["CERF"]."
        		            </td>
        		            <td>
        		                ".$date_start[0]["date_answered"]."
        		            </td>
        		            <td>
        		                ".$date_end[0]["date_answered"]."
        		            </td>
        		            <td>
        		                ".$minutes[0]."
        		            </td>";
        		            
        		       $htmlBody .= "</tr>";
        		}
        		$htmlBody .= ' 
        		        </tbody>
        		    </table>
        		    <p>
        		        Sincerely,<br/><br/>
                    <a href="http://www.iltoexams.com/"><img style="float:left;top:20px;" alt="ILTO Exams" src="iltoexams.com/logo_ILTO.png" /></a>
        		    </p>';
                
                
                $dat = "{From: 'noreply@iltoexams.com', To: '$to', Cc: 'ing.christian.velandia@gmail.com', Subject: '$subject', HtmlBody: '$htmlBody'}";//array('From' => 'noreply@iltoexams.com', 'To'=>'dokho_02@hotmail.com', 'Subject'=>'Hola Chris Fer', 'HtmlBody'=>'<strong>Hello</strong> dear Postmark user.');
                
                //$dat = "{From: 'noreply@iltoexams.com', To: '$to', Cc: 'info@iltoexams.com,osdeisa@gmail.com', Subject: '$subject', HtmlBody: '$htmlBody'}";//array('From' => 'noreply@iltoexams.com', 'To'=>'dokho_02@hotmail.com', 'Subject'=>'Hola Chris Fer', 'HtmlBody'=>'<strong>Hello</strong> dear Postmark user.');
                //$this->sendEmail($dat);
        //Comparo
    }
    
    
    public function recoverPass($user, $email){
        $model = UsersC::model()->findByAttributes(array("id_usuario_c"=>$user,"email"=>$email));
        
        $SQL = "SELECT id_perfil FROM `users_c` WHERE id_usuario_c='".$user."'";
        $id= Yii::app()->db->createCommand($SQL)->queryAll();
        if($id[0]["id_perfil"]!=6){
            if($model->email == $email){
                $this->enviarPass($user, 1);
                return true;
            } else {
                return false;
            }
        }    
    }
   
    public function gastarLicencia($idLicenciaCliente){
        $modLicenciasCliente = LicensesClient::model()->findByPk($idLicenciaCliente);
        $modLicenciasCliente->utilizados += 1;
        $modLicenciasCliente->save(true,array('utilizados'));
        //Si se acaban las licencias cambiar el estado
        if($modLicenciasCliente->utilizados == $modLicenciasCliente->cantidad){
            $modLicenciasCliente->estado = 'F';
            $modLicenciasCliente->save(true,array('estado'));
        }
        //Aplicar control de Alertas
        $modLicCteTodas = LicensesClient::model()->findAllByAttributes(array('id_cliente'=>$modLicenciasCliente->id_cliente, 'id_licencia'=>$modLicenciasCliente->id_licencia));
        $totalRestante = 0;
        $revisando = 0;
        foreach($modLicCteTodas as $modelo){
            $revisando = 1;
            $totalRestante += ($modelo->cantidad - $modelo->utilizados);
        }
        if(($totalRestante == 25 || $totalRestante == 0 ) && $revisando==1){
           $this->enviarAlertaLicencias($totalRestante,$modLicenciasCliente->id_licencia,$modLicenciasCliente->id_cliente);
        }
    }
    
    public function generateRandomString($length) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
    
    public function enviarAlertaLicencias($cantidad,$licencia,$cliente){
        $nomLicencia = Licenses::model()->findByPk($licencia)->nombre;
        $modToaa = UsersC::model()->findByAttributes(array('id_cliente'=>$cliente, 'id_perfil'=>2));
        if($modToaa){
            $mail = new YiiMailer('licensesAlert', array('model'=>$modToaa,'licencia'=>$nomLicencia, 'cantidadLic'=> $cantidad, 'header_title'=>'ILTO - ADMINISTRATOR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; LICENSES ALERT'));
            //set properties
            $mail->setFrom("noreply@iltoexams.com", "ILTO Exams");
            $mail->setSubject("LICENSES ALERT");
            $mail->setTo($modToaa->email);
            $mail->setCc('info@iltoexams.com');
            //send
            $mail->send();
        }
    }
    
    public function shuffle_2($arreglo){
        $nuevoArreglo = array();
        $offset=  intval(count($arreglo)/5);
        for($i=0;$i<=$offset-1;$i++){
            $parte = array_slice($arreglo, 5*$i,$offset);
            shuffle($parte);
            array_push($nuevoArreglo, $parte);
        }
        return $nuevoArreglo;
    }
    
    public function sixMonthValidate($effectiveDate) {
            
        $currentDate = new DateTime('now');
        //Si CurrentDate es mayor o igual a la fecha efectiva, devuelve un TRUE permitiendo crear licencia
        //Por el contrario devuelve false, no permitiendo crear licencia
        
        if($currentDate >= $effectiveDate) {
            return true;
            
        }else
        {
            return false;
        }
    }
    
    public function saveImage($file,$target_dir){
    
    $target_file = $target_dir . basename($file["name"]);
    $target_file = strtolower($target_file);
   
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    // Check if image file is a actual image or fake image
    if(isset($_POST["submit"])) {
        $check = getimagesize($file["tmp_name"]);
        if($check !== false) {
          //  echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            echo "File is not an image.<br>";
            $uploadOk = 0;
        }
    }
    // Check if file already exists
    if (file_exists($target_file)) {
       // echo "Sorry, file already exists.";
       // $uploadOk = 0;
    }
    // Check file size
    if ($file["size"] > 500000) {
        echo "Sorry, your file is too large.<br>";
        $uploadOk = 0;
    }
    // Allow certain file formats
    //var_dump($imageFileType);
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.<br>";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($file["tmp_name"], $target_file)) {
         //   echo "The file ". basename( $file["name"]). " has been uploaded.";
        } else {
           // echo "Sorry, there was an error uploading your file.<br>";
        }
    }
    
    return basename( $file["name"]);
}

public function sendEmail($data){
    
   
    $postmarkApiKey = '93cc172f-fc14-4c2f-8c4c-6dd0a647ac68';
    
    //NOTE: The top level key in the array is important, as some apis will insist that it is 'file'.
    //$data = "{From: 'noreply@iltoexams.com', To: 'dokho_02@hotmail.com', Cc: 'ing.christian.velandia@gmail.com', Subject: 'Hello from Postmark', HtmlBody: '<strong>Hello</strong> dear Postmark user.'}";//array('From' => 'noreply@iltoexams.com', 'To'=>'dokho_02@hotmail.com', 'Subject'=>'Hola Chris Fer', 'HtmlBody'=>'<strong>Hello</strong> dear Postmark user.');
    
    $Api = "https://api.postmarkapp.com/email";
    
    $ch = curl_init();
    $options = array(CURLOPT_URL => $Api,
                 CURLOPT_HTTPHEADER => array('Accept: application/json', 'Content-Type: application/json', 'X-Postmark-Server-Token: '.$postmarkApiKey),
                 CURLOPT_RETURNTRANSFER => true,
                 CURLINFO_HEADER_OUT => false, //Request header
                 CURLOPT_HEADER => false, //Return header
                 CURLOPT_SSL_VERIFYPEER => false, //Don't veryify server certificate
                 CURLOPT_POST => true,
                 CURLOPT_POSTFIELDS => $data
                );
    
    curl_setopt_array($ch, $options);
    $result = curl_exec($ch);
    $header_info = curl_getinfo($ch,CURLINFO_HEADER_OUT);
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $header = substr($result, 0, $header_size);
    $body = substr($result, $header_size);
    curl_close($ch);
    /*
    var_dump($result);
    var_dump($header_info);
    var_dump($header);
    var_dump($body);*/
}

public function SendOrderDetail($id){
		// CONSTRUCCIÓN DEL PDF
            
            $SQL = "SELECT * FROM `clients` WHERE id_cliente='".Yii::app()->user->getState('cliente')."'";
            $client= Yii::app()->db->createCommand($SQL)->queryAll();
            
            $SQL = "SELECT city.name as city, country.name as country FROM `city` INNER JOIN country ON city.CountryCode = country.Code WHERE city.ID ='".$client[0]["id_ciudad"]."'";
            $country= Yii::app()->db->createCommand($SQL)->queryAll();
            
            $SQL = "SELECT nombres, apellidos, email FROM `users_c` WHERE id_cliente='".Yii::app()->user->getState('cliente')."' AND id_perfil='2'";
            $toaa= Yii::app()->db->createCommand($SQL)->queryAll();
            
            $SQL = "SELECT * FROM `orders` WHERE id_client =  '".Yii::app()->user->getState('cliente')."' AND id_order='".$id."' ORDER BY id_order DESC";
            $order = Yii::app()->db->createCommand($SQL)->queryAll();
            
            $SQL = "SELECT * FROM `order_licenses` WHERE `id_order` =  '".$order[0]["id_order"]."'";
            $order_items = Yii::app()->db->createCommand($SQL)->queryAll();
             
            
            if(is_null($client[0]["id_distributor"])){
                $distributor = '<span><b>Agent:</b>ILTOEXAMS'; 
            }else{
                $distributor = '<span><b>Agent: </b>'.$client[0]["id_distributor"].'<br/>'; 
            }
                        
        
            $informe = '
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
         <div class="widget">
            <div class="widget-body">
                <div class="row" style="padding:15px;"><!--ORDER Header-->
                    <table>
                        <tr>
                            <td style="width:33%;">
                                    <img src="/ilto3/images/logo_ilto_original.png" width="200" style="">
                            </td>
                            <td style="width:33%;text-align:center;font-size:12px;">International Language Testing Organization Inc.
                                        8300 NW 53rd Street Suite 350 Doral FL 33166
                                        305-853-8088
                                        www.iltoexams.com
                            </td>
                            <td style="width:33%;">
                                <div class="col-lg-4 col-sm-4 col-xs-4">
                                    <table>
                                        <tr>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                                Terms
                                            </td>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                                Order No.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #0f5418; text-align:center;font-size:12px;">
                                                '.$order[0]["date_expedition"].'
                                            </td>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #0f5418; text-align:center;font-size:12px;">
                                                '.$order[0]["id_order"].'
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                                Date
                                            </td>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                                Due Date
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #0f5418; text-align:center;font-size:12px;">
                                                '.$order[0]["date_expedition"].'
                                            </td>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #0f5418; text-align:center;font-size:12px;">
                                                '.$order[0]["date_expire"].'
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <!-- END ORDER Header-->
                    <div class="row" style="padding:15px;"><!--BILL TO-->
                    <table>
                        <tr>
                            <td style="width:33%;padding:15px;text-align:left;background-color:#dedcdc;;min-height:140px;color:black;font-size:12px;">
                                <span><b>Customer: </b>'.utf8_decode($client[0]["nombre_rsocial"]).'</span><br/>
                                <span><b>Name: </b>'.utf8_decode($toaa[0]["nombres"]." ".$toaa[0]["apellidos"]).'<br/>
                                <span><b>Address: </b>'.utf8_decode($client[0]["direccion"]).', '.utf8_decode($country[0]["country"]).', '.utf8_decode($country[0]["city"]).'</span><br/>
                                <span><b>Phone Number: </b> '.utf8_decode($client[0]["telefono"]).'</span><br/>
                                <span><b>Email: </b>'.$toaa[0]["email"].'</span>
                            </td>
                            <td style="width:33%;padding:15px;text-align:center;background-color:#e8e6e6;min-height:140px;color:black;font-size:12px;">
                                '.$distributor.'
                            </td>
                            <td style="width:33%;padding:15px;text-align:center;background-color:#dedcdc;;min-height:140px;color:black;font-size:12px;">
                               <br/>If you have any questions about your order, please contact info@iltoexams.com<br/><br/>
                                This is not a Tax Invoice!
                            </td>
                        </tr>
                    </table>
                </div><!-- END BILL TO-->
                     <div class="row" id="body_order" style="min-height:300px; padding:15px;"><!--BODY -->
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <table style="width:100%;">
                            <tr>
                                <td style="width:padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                    Licenses valid thru
                                </td>
                                <td style="width:350px;padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                    Description
                                </td>
                                <td style="width:70px;padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                    Quantity
                                </td>
                                <td style="width:70px;padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                     Unit price
                                </td>
                                <td style="width:100px;padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                    Amount
                                </td>
                            </tr>';
                        
                        // LICENSES
                        
                            foreach($order_items as $item){
                                
                                $SQL = "SELECT nombre FROM `licenses` WHERE `id_licencia` =  '".$item["id_licencia"]."'";
                                $licenseName = Yii::app()->db->createCommand($SQL)->queryAll();
             
                                $informe .= '
                                <tr style="border:1px solid black;">
                                    <td style="padding:5px;border:1px solid #0f5418;font-weight:800;text-align:center;font-size:12px;border-right:1px solid black;">
                                        '.date("Y-m-d", strtotime(date('Y-m-d'). ' + 1 year')).' 
                                    </td>
                                    <td style="padding:5px;border:1px solid #0f5418;font-weight:800;text-align:left;font-size:12px;border-right:1px solid black;">
                                        '.$licenseName[0]["nombre"].' 
                                    </td>
                                    <td style="padding:5px;border:1px solid #0f5418;font-weight:800;text-align:center;font-size:12px;border-right:1px solid black;">
                                        '.$item["quantity"].'
                                    </td>
                                    <td style="padding:5px;border:1px solid #0f5418;font-weight:800;text-align:center;font-size:12px;border-right:1px solid black;">$
                                        '.$item["price"].'
                                    </td>
                                    <td style="padding:5px;border:1px solid #0f5418;font-weight:800;text-align:right;font-size:12px;border-right:1px solid black;">$
                                        '.$item["total"].'
                                    </td>
                                </tr>';
                            }
                            
                            
                            
                            $informe .= '
                            <tr>
                                <td colspan="3" style="padding:10px;background-color:#dedcdc;">
                                    XXXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXXXXXXXXXX XXXXXXXXXXXX
                                </td>
                                 
                                <td  style="padding:10px;border:1px solid #0f5418; background-color: #77e286;">
                                    <b>SUBTOTAL</b>
                                </td>
                                <td style="padding:10px;border:1px solid #0f5418; text-align:right;background-color: #77e286;">$
                                   '.$order[0]["subtotal"].' 
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="padding:10px;background-color:#dedcdc;">
                                    XXXXXXXXXXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXXXXXXXX
                                </td>
                                 
                                <td style="padding:10px;border:1px solid #0f5418;">
                                    DISCOUNT
                                </td>
                                <td style="padding:10px;border:1px solid #0f5418; text-align:right;">
                                   '.$order[0]["discount"].' 
                                %</td>
                            </tr>
                            <tr>
                                <td colspan="3" style="padding:10px;background-color:#dedcdc;">
                                
                                </td>
                                <td style="padding:10px;border:1px solid #0f5418;">
                                    <b>TOTAL</b>
                                </td>
                                <td style="padding:10px;border:1px solid #0f5418; text-align:right;background-color:#39b54a;color:white;font-weight:800;">$
                                    '.$order[0]["total"].' 
                                </td>
                            </tr>
                        <!-- END BODY-->    
                </table>';
                                 
            $mPDF1 = Yii::app()->ePdf->mpdf("","Letter");
            $mPDF1->WriteHTML($informe);
            $nombre = "order.pdf";
            //$mPDF1->Output();
            
            $tooaEmail = $toaa[0]["email"];       
            if($_POST['order']['id_distributor']!='NULL'){
                $SQL = "SELECT email FROM `users_c` WHERE id_cliente='".$_POST['order']['id_distributor']."' AND id_perfil='2' LIMIT 1";
                $distributor= Yii::app()->db->createCommand($SQL)->queryAll();
            
                var_dump("envio al distribuidor");
                $to = $tooaEmail.', '.$distributor[0]['email'].', info@iltoexams.com';
                $message = 'Orden de compra confirmada, se envia correo al distribuidor, al cliente y copia a ilto.  El distribuidor le entregará la factura al cliente.';
                
                
            }else{
                var_dump("NO envio al distribuidor");
                $to = $tooaEmail.', info@iltoexams.com';
                $message = 'Orden de compra confirmada, se envia correo al cliente y copia a ilto.  Recibirá otro correo con la factura generada.';
            
            }
            
            $modUtil = new Utilidades();
            $content = $mPDF1->Output('','S');
            $content = chunk_split(base64_encode($content));
            $mailto = $to; //Mailto here
            $from_name = 'ILTO'; //Name of sender mail
            $from_mail = 'info@iltoexams.com'; //Mailfrom here
            $subject = 'Paso 3 - Orden de compra CONFIRMADA POR EL CLIENTE'; 
            $filename = $nombre;
            //Headers of PDF and e-mail
            $boundary = "XYZ-" . date("dmYis") . "-ZYX"; 
            $header .= "Content-Transfer-Encoding: 8bits\r\n"; 
            $header .= "Content-Type: text/html; charset=ISO-8859-1\r\n\r\n"; // or utf-8
            $header .= "$message\r\n";
            $header .= "--$boundary\r\n";
            $header .= "Content-Type: application/pdf; name=\"".$filename."\"\r\n";
            $header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n";
            $header .= "Content-Transfer-Encoding: base64\r\n\r\n";
            $header .= "$content\r\n"; 
            $header .= "--$boundary--\r\n";
            $header2 = "MIME-Version: 1.0\r\n";
            $header2 .= "From: ".$from_name." <".$from_mail."> \r\n"; 
            $header2 .= "Return-Path: $from_mail\r\n";
            $header2 .= "Content-type: multipart/mixed; boundary=\"$boundary\"\r\n";
            $header2 .= "$boundary\r\n";
            
            if(mail($mailto,$subject,$header,$header2, "-r".$from_mail)){
                Yii::app()->user->setFlash('success','An Email was sent to '.$modUsuario->email.' with the certificate');
                return true;
            } else {
                $error = print_r(error_get_last(),true);
                Yii::app()->user->setFlash('error',$error);
                return false;
            }
	}
	
	public function cuoteDetail($id){
		// CONSTRUCCIÓN DEL PDF
            
            $SQL = "SELECT * FROM `clients` WHERE id_cliente='".Yii::app()->user->getState('cliente')."'";
            $client= Yii::app()->db->createCommand($SQL)->queryAll();
            
            $SQL = "SELECT city.name as city, country.name as country FROM `city` INNER JOIN country ON city.CountryCode = country.Code WHERE city.ID ='".$client[0]["id_ciudad"]."'";
            $country= Yii::app()->db->createCommand($SQL)->queryAll();
            
            $SQL = "SELECT nombres, apellidos, email FROM `users_c` WHERE id_cliente='".Yii::app()->user->getState('cliente')."' AND id_perfil='2'";
            $toaa= Yii::app()->db->createCommand($SQL)->queryAll();
            
            $SQL = "SELECT * FROM `orders` WHERE id_client =  '".Yii::app()->user->getState('cliente')."' AND id_order='".$id."' ORDER BY id_order DESC";
            $order = Yii::app()->db->createCommand($SQL)->queryAll();
            
            
            $SQL = "SELECT * FROM `order_licenses` WHERE `id_order` =  '".$order[0]["id_order"]."'";
            $order_items = Yii::app()->db->createCommand($SQL)->queryAll();
            
            
            if(is_null($client[0]["id_distributor"])){
                $distributor = '<span><b>Agent:</b>ILTOEXAMS'; 
            }else{
                $distributor = '<span><b>Agent: </b>'.$client[0]["id_distributor"].'<br/>'; 
            }
            
            if($order[0]["currency_type"] == "COP"){
                $ilto_header = 'ILTO COLOMBIA<br/>Nit: 900430264_7<br/>
                Contact : 312 515 3017<br/>Calle 25 D No 36-07, Bogotá - Colombia<br/>';
                
                $bank = '<p style="font-size:14px;">
                <b>BANCO:</b> BANCO DAVIVIENDA<br/> 
                <b>TIPO DE CUENTA:</b> CUENTA DE AHOORROS No. 4502 0005 8995<br/> 
                <b>TITULAR:</b> ILTO COLOMBIA S.A.S.<br/>';
                $waring = 'Este documento es una cotización, NO UNA FACTURA.  Este documento será procesado por un representante de ventas quién te contactará para confirmar los términos y condiciones de compra.  
                </p> ';
                
                $warning_2 = '<br/>Si tienes alguna inquietud acerca de tu cotización, por favor contáctanos a info@iltoexams.com<br/><br/>';
                
            }else{
                $ilto_header = 'International Language Testing Organization Inc.
                8300 NW 53rd Street Suite 350 Doral FL 33166<br/><br/><br/>';    
                    
                $waring = 'This document is a quotation, NOT AN INVOICE. This quotation will be processed by a sales rep who will contact you to confirm the purchase terms and conditions. Please check the status of this quotation in <b>MY LICENSES</b> section. ';
                $warning_2 = '<br/>If you have any questions about your order, please contact info@iltoexams.com<br/><br/>
                                This is not a Tax Invoice!';
            }
            
            
            switch($order[0]["terms"]) {
            case '0':
                $due= "Due on Recipt";
            break;
            case '1':
                $due= "30 days";
            break;
            case '2':
                $due= "60 days";
            break;
            case '3':
                $due= "90 days";
            break;
            default:
            $due= "Due on recipt";
        } 
        $nombres = $toaa[0]["nombres"]." ".$toaa[0]["apellidos"];
        $addr = $client[0]["direccion"]." ".$country[0]["country"]." ".$country[0]["city"];
        setlocale(LC_ALL, "en_US.utf8"); 
        $nombre = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $nombres);
        $agent = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE',$client[0]["nombre_rsocial"]);
        $addres = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $addr);
        
            $informe = '
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
         <div class="widget">
            <div class="widget-body">
                <div class="row" style="padding:15px;"><!--ORDER Header-->
                    <table>
                        <tr>
                            <td style="width:33%;text-align:center;" >
                                    <img src="/ilto3/images/logo_ilto_original.png" width="200" style="">
                                    <h3>QUOTATION</h3>
                            </td>
                            <td style="width:33%;text-align:center;font-size:12px;">'.$ilto_header.'
                                        www.iltoexams.com
                            </td>
                            <td style="width:33%;">
                                <div class="col-lg-4 col-sm-4 col-xs-4">
                                    <table>
                                        <tr>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                                Terms
                                            </td>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                                Quotation No.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #0f5418; text-align:center;font-size:12px;">
                                                '.$due.'
                                            </td>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #0f5418; text-align:center;font-size:12px;">
                                                '.$order[0]["id_order"].'
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                                Date
                                            </td>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                                Due Date
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #0f5418; text-align:center;font-size:12px;">
                                                '.substr($order[0]["date_expedition"], 0, 10).'
                                            </td>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #0f5418; text-align:center;font-size:12px;">
                                                '.substr($order[0]["date_expire"], 0, 10).'
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                <!-- END ORDER Header-->
                </div>    
                <div class="row" style="padding:15px;"><!--BILL TO-->
                    <table>
                        <tr>
                            <td style="width:33%;padding:15px;text-align:left;background-color:#dedcdc;;min-height:140px;color:black;font-size:12px;">
                                <span><b>Customer: </b>'.$agent.'</span><br/>
                                <span><b>Company ID: </b>'.$client[0]["nit_id_legal"].'<br/>
                                <span><b>Address: </b>'.$addres.'</span><br/>
                                <span><b>Phone Number: </b> '.utf8_decode($client[0]["telefono"]).'</span><br/>
                                <span><b>Email: </b>'.$toaa[0]["email"].'</span>
                            </td>
                            <td style="width:33%;padding:15px;text-align:center;background-color:#e8e6e6;min-height:140px;color:black;font-size:12px;">
                                '.$distributor.'
                            </td>
                            <td style="width:33%;padding:15px;text-align:center;background-color:#dedcdc;;min-height:140px;color:black;font-size:12px;">
                               '.$warning_2.'
                            </td>
                        </tr>
                    </table>
                </div><!-- END BILL TO-->
                <div class="row" id="body_order" style="min-height:500px; padding:15px;"><!--BODY -->
                    <div class="col-lg-12 col-sm-12 col-xs-12">
                        <table style="width:100%;">
                            <tr>
                                <td style="width:padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                    Licenses valid thru
                                </td>
                                <td style="width:350px;padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                    Description
                                </td>
                                <td style="width:70px;padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                    Quantity
                                </td>
                                <td style="width:70px;padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                    Unit price
                                </td>
                                <td style="width:100px;padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                    Amount
                                </td>
                            </tr>';
                        
                        // LICENSES
                        
                            foreach($order_items as $item){
                                
                                $SQL = "SELECT nombre FROM `licenses` WHERE `id_licencia` =  '".$item["id_licencia"]."'";
                                $licenseName = Yii::app()->db->createCommand($SQL)->queryAll();
             
                                $informe .= '
                            <tr style="border:1px solid black;">
                                <td style="padding:5px;border:1px solid #0f5418;font-weight:800;text-align:center;font-size:12px;border-right:1px solid black;">
                                    '.date("Y-m-d", strtotime(date('Y-m-d'). ' + 1 year')).' 
                                </td>
                                <td style="padding:5px;border:1px solid #0f5418;font-weight:800;text-align:left;font-size:12px;border-right:1px solid black;">
                                    '.$licenseName[0]["nombre"].' 
                                </td>
                                <td style="padding:5px;border:1px solid #0f5418;font-weight:800;text-align:center;font-size:12px;border-right:1px solid black;">
                                    '.$item["quantity"].'
                                </td>
                                <td style="padding:5px;border:1px solid #0f5418;font-weight:800;text-align:center;font-size:12px;border-right:1px solid black;">$
                                    '.number_format($item["price"],2).'
                                </td>
                                <td style="padding:5px;border:1px solid #0f5418;font-weight:800;text-align:right;font-size:12px;border-right:1px solid black;">$
                                    '.number_format($item["total"]).'
                                </td>
                            </tr>';
                            }
                            
                            
                            
                            $informe .= '
                            <tr style="border:1px solid black;">
                                <td style="padding:5px;border:1px solid #0f5418;font-weight:800;text-align:center;font-size:12px;border-right:1px solid black;">
                                    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>  
                                </td>
                                </td>
                                <td style="padding:5px;border:1px solid #0f5418;font-weight:800;text-align:left;font-size:12px;border-right:1px solid black;min-height:300px;">
                                   &nbsp;
                                </td>
                                <td style="padding:5px;border:1px solid #0f5418;font-weight:800;text-align:center;font-size:12px;border-right:1px solid black;min-height:300px;">
                                   &nbsp; 
                                </td>
                                <td style="padding:5px;border:1px solid #0f5418;font-weight:800;text-align:center;font-size:12px;border-right:1px solid black;min-height:300px;">
                                  &nbsp;  
                                </td>
                                <td style="padding:5px;border:1px solid #0f5418;font-weight:800;text-align:right;font-size:12px;border-right:1px solid black;min-height:300px;">
                                   &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="padding:10px;">
                                    '.$bank.'
                                </td>
                                <td  style="padding:10px;border:1px solid #0f5418; background-color: #77e286;">
                                    <b>SUBTOTAL</b>
                                </td>
                                <td style="padding:10px;border:1px solid #0f5418; text-align:right;background-color: #77e286;">$
                                    '.number_format($order[0]["subtotal"]).' 
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="padding:10px;">
                                   '.$waring.'
                                </td>
                                <td style="padding:10px;border:1px solid #0f5418;">
                                    DISCOUNT
                                </td>
                                <td style="padding:10px;border:1px solid #0f5418; text-align:right;">
                                    '.$order[0]["discount"].' 
                                %</td>
                            </tr>
                            <tr>
                                <td colspan="3" style="padding:10px;">
                                </td>
                                <td style="padding:10px;border:1px solid #0f5418;">
                                    <b>TOTAL</b>
                                </td>
                                <td style="padding:10px;border:1px solid #0f5418; text-align:right;background-color:#39b54a;color:white;font-weight:800;">$
                                    '.number_format($order[0]["total"]).' 
                                </td>
                            </tr>
                        </table>
                    </div>
                </div><!-- END BODY-->   
            </div>
        </div>
    </div>
</div>
<table style="width:100%;margin-top:50px;">
    <tr>
        <td style="text-align:center;">
            <img src="/ilto3/images/ede.png" width="200" style="">    
        </td>
        <td style="text-align:center;">
            <img src="/ilto3/images/tecs.png" width="200" style="">       
        </td>
    </tr>
</table>';
          
            require '/home/iltoexam/public_html/vendor/autoload.php';
			$mpdf = new \Mpdf\Mpdf(['tempDir' => __DIR__ . '/../temp']);
			$mpdf->WriteHTML($informe);
			$nombre = "order_".$order[0]["id_order"].".pdf";
			$mpdf->Output();  
            
	}
	
	public function InvoiceGenerate($id){
		// CONSTRUCCIÓN DEL PDF
            $SQL = "SELECT * FROM `invoices` WHERE id_invoice='".$id."'";
            $invoice = Yii::app()->db->createCommand($SQL)->queryAll();
            
            $SQL = "SELECT * FROM `orders` WHERE id_order='".$invoice[0]["id_order"]."'";
            $order = Yii::app()->db->createCommand($SQL)->queryAll();
            
            $SQL = "SELECT * FROM `order_licenses` WHERE `id_order` =  '".$order[0]["id_order"]."'";
            $order_items = Yii::app()->db->createCommand($SQL)->queryAll();
            
            $SQL = "SELECT * FROM `clients` WHERE id_cliente='".$order[0]["id_client"]."'";
            $client= Yii::app()->db->createCommand($SQL)->queryAll();
            
            $SQL = "SELECT city.name as city, country.name as country FROM `city` INNER JOIN country ON city.CountryCode = country.Code WHERE city.ID ='".$client[0]["id_ciudad"]."'";
            $country= Yii::app()->db->createCommand($SQL)->queryAll();
            
            $SQL = "SELECT nombres, apellidos, email FROM `users_c` WHERE id_cliente='".$client[0]["id_cliente"]."' AND id_perfil='2'";
            $toaa= Yii::app()->db->createCommand($SQL)->queryAll();
            
            $distributor = '<span><b>Agent:</b>ILTOEXAMS'; 
            
            if( $invoice[0]["id_invoice"] < 100){
                $inv = str_pad ($invoice[0]["id_invoice"], 4,"0", STR_PAD_LEFT);
            }
            
            if( ($invoice[0]["id_invoice"] > 100) && ($invoice[0]["id_invoice"] < 1000)){
                $inv = str_pad ($invoice[0]["id_invoice"], 3,"0", STR_PAD_LEFT);
            }
            
            if($order[0]["currency_type"] == "COP"){
                $ilto_header = 'ILTO COLOMBIA<br/>Nit: 900430264_7<br/>
                Contact:312 515 3017<br/>Calle 25 D No 36-07, Bogotá - Colombia<br/>';
                
                $bank = '<p style="font-size:14px;">
                BANCO DAVIVIENDA<br/> 
                SAVING ACCOUNT # 4502 0005 8995
                </p> ';
                
            }else{
                $ilto_header = 'International Language Testing Organization Inc.
                8300 NW 53rd Street Suite 350 Doral FL 33166<br/><br/><br/>';    
                    
                $bank = '<p style="font-size:14px;">
                BANK OF AMERICA<br/> SWIFT ADDRESS: BOFAUS3N<br/> RECEIVING BANK: ABA # is 026009593<br/> BENEFICIARY NAME: international Language Testing Organization Inc.<br/>
                BENEFICIARY ACCOUNT#: 898062667428<br/> ADDITIONAL REFERENCE: Bank of America, FL7-573-01-01<br/> 3025 NW 87 Ave, Doral, FL 33172
                </p> ';
            }
            
            
            switch ($order[0]['terms']) {
        		case '0':
        		    $terms= "Due on recipt";
        		break;
        		case '1':
        		    $terms= "30 days";
        		break;
        		case '2':
        		    $terms= "60 days";
        		break;
        		case '3':
        		    $terms= "90 days";
        		break;
        		default:
        		    $terms= "Due on recipt";
        	}
            $nombres = $toaa[0]["nombres"]." ".$toaa[0]["apellidos"];
            $addr = $client[0]["direccion"]." ".$country[0]["country"]." ".$country[0]["city"];
            setlocale(LC_ALL, "en_US.utf8"); 
            $nombre = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $nombres);
            $agent = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE',$client[0]["nombre_rsocial"]);
            $addres = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $addr);
            $informe = '
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
         <div class="widget">
            <div class="widget-body">
                <div class="row" style="padding:10px;"><!--ORDER Header-->
                    <table>
                        <tr>
                            <td style="width:33%;">
                                    <img src="/ilto3/images/logo_ilto_original.png" width="200" style="">
                            </td>
                            <td style="width:33%;text-align:left;font-size:12px;">'.$ilto_header.'
                                        www.iltoexams.com
                            </td>
                            <td style="width:33%;">
                            
                                <div class="col-lg-4 col-sm-4 col-xs-4">
                                    <table style="padding:0px;margin:0px;">
                                        <tr>
                                            <td colspan="2" style="text-align:center;">
                                                <h2 style="left:30px;color:#f4b400;">COMMERCIAL INVOICE</h2>
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #333;background-color:#39b54a;color:white;font-weight:bold;text-align:center;font-size:12px;">
                                                Terms
                                            </td>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #333;background-color:#39b54a;color:white;font-weight:bold;text-align:center;font-size:12px;">
                                                Invoice No. 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #333; text-align:center;font-size:12px;">
                                             '.$terms.' 
                                            </td>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #333; text-align:center;font-size:12px;">
                                                0'.$inv.'
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #333;background-color:#f4b400;color:white;font-weight:bold;text-align:center;font-size:12px;">
                                                Date
                                            </td>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #333;background-color:#f4b400;color:white;font-weight:bold;text-align:center;font-size:12px;">
                                                Due Date
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #333; text-align:center;font-size:12px;">
                                               '.date("Y-m-d", strtotime($invoice[0]["date_expedition"])).' 
                                            </td>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #333; text-align:center;font-size:12px;">
                                                '.date("Y-m-d", strtotime($order[0]["date_expire"])).'
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <!-- END ORDER Header-->
                    <div class="row" style="padding:15px;"><!--BILL TO-->
                    <table style="width:100%;">
                        <tr>
                            <td style="width:60%;padding:5px;text-align:left;border:1px solid #333;background-color:#39b54a;min-height:20px;color:#fff;font-size:12px;font-weight:bold;">
                                Bill to
                            </td>
                            <td style="width:10%;padding:5px;text-align:center;background-color:#fff;min-height:20px;color:black;font-size:12px;">
                                
                            </td>
                            <td style="width:30%;padding:5px;text-align:center;border:1px solid #333;background-color:#f4b400;min-height:20px;color:#fff;font-size:12px;font-weight:bold;">
                               Amount Due
                            </td>
                        </tr>
                        <tr>
                            <td style="width:60%;padding:15px;text-align:left;border:1px solid #333;background-color:#fff;;min-height:140px;color:black;font-size:12px;">
                                <div class="row" style="padding:15px 20px;">
                                <span><b>'.$nombre.'</b></span><br/>
                                <span><b>'.$agent.'</b></span><br/>
                                <span><b>'.$addres.'</b></span><br/>
                                <span><b>Phone Number: </b> '.utf8_decode($client[0]["telefono"]).'</span><br/>
                                <span><b>Email: </b>'.$toaa[0]["email"].'</span>
                            </td>
                            <td style="width:10%;padding:15px;text-align:center;background-color:#fff;min-height:140px;color:black;font-size:12px;">
                                
                            </td>
                            <td style="width:30%;padding:15px;text-align:center;border:1px solid #333;background-color:#fff;;min-height:140px;color:black;font-size:12px;">
                               $'.$order[0]["total"].' '.$order[0]["currency_type"].' 
                            </td>
                        </tr>
                    </table>
                </div><!-- END BILL TO-->
                     <div class="row" id="body_order" style="min-height:300px; padding:15px;"><!--BODY -->
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <table style="width:100%;">
                            <tr>
                                <td style="width:70px;padding:5px;color:white;font-weight:800;text-align:center;font-size:12px;">
                                    
                                </td>
                                <td style="width:350px;padding:5px;color:white;font-weight:800;text-align:center;font-size:12px;">
                                   
                                </td>
                                <td style="width:70px;padding:5px;color:white;font-weight:800;text-align:center;font-size:12px;">
                                   
                                </td>
                                <td style="width:70px;padding:5px;color:white;font-weight:800;text-align:center;font-size:12px;">
                                     
                                </td>
                                <td style="width:100px;padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                    Sales Rep
                                </td>
                            </tr>
                            <tr>
                                <td style="width:70px;padding:5px;color:white;font-weight:800;text-align:center;font-size:12px;">
                                    
                                </td>
                                <td style="width:350px;padding:5px;color:white;font-weight:800;text-align:center;font-size:12px;">
                                   
                                </td>
                                <td style="width:70px;padding:5px;font-weight:800;text-align:center;font-size:12px;">
                                   
                                </td>
                                <td style="width:70px;padding:5px;color:white;font-weight:800;text-align:center;font-size:12px;">
                                     
                                </td>
                                <td style="width:100px;padding:5px;border:1px solid #0f5418;background-color:#fff;color:black;font-weight:800;text-align:center;font-size:12px;">
                                    Matt
                                </td>
                            </tr>
                            <tr>
                                <td style="width:70px;padding:5px;border:1px solid #333;background-color:#f4b400;color:white;font-weight:bold;text-align:center;font-size:12px;">
                                    Licenses valid thru
                                </td>
                                <td style="width:350px;padding:5px;border:1px solid #333;background-color:#f4b400;color:white;font-weight:bold;text-align:center;font-size:12px;">
                                    Description
                                </td>
                                <td style="width:70px;padding:5px;border:1px solid #333;background-color:#f4b400;color:white;font-weight:bold;text-align:center;font-size:12px;">
                                    Quantity
                                </td>
                                <td style="width:70px;padding:5px;border:1px solid #333;background-color:#f4b400;color:white;font-weight:bold;text-align:center;font-size:12px;">
                                     Rate
                                </td>
                                <td style="width:100px;padding:5px;border:1px solid #333;background-color:#f4b400;color:white;font-weight:bold;text-align:center;font-size:12px;">
                                    Amount
                                </td>
                            </tr>';
                        
                        // LICENSES
                        
                            foreach($order_items as $item){
                                
                                $SQL = "SELECT nombre FROM `licenses` WHERE `id_licencia` =  '".$item["id_licencia"]."'";
                                $licenseName = Yii::app()->db->createCommand($SQL)->queryAll();
             
                                $informe .= '
                                <tr style="border:1px solid black;">
                                    <td style="padding:15px 10px;border:1px solid #333;font-weight:800;text-align:center;font-size:14px;border-right:1px solid black;">
                                        '.date("Y-m-d", strtotime($item["date_expedition"]. ' + 1 year')).' 
                                    </td>
                                    <td style="padding:15px 10px;border:1px solid #333;font-weight:800;text-align:left;font-size:14px;border-right:1px solid black;">
                                        '.$licenseName[0]["nombre"].' 
                                    </td>
                                    <td style="padding:15px 10px;border:1px solid #333;font-weight:800;text-align:center;font-size:14px;border-right:1px solid black;">
                                        '.$item["quantity"].'
                                    </td>
                                    <td style="padding:15px 10px;border:1px solid #333;font-weight:800;text-align:center;font-size:14px;border-right:1px solid black;">$
                                        '.$item["price"].'
                                    </td>
                                    <td style="padding:15px 10px;border:1px solid #333;font-weight:800;text-align:right;font-size:14px;border-right:1px solid black;">$
                                        '.$item["total"].'
                                    </td>
                                </tr>';
                            }
                            
                            
                            
                            $informe .= '
                            <tr>
                                <td colspan="3" rowspan="5" style="padding:10px;background-color:#fff">
                                                   '.$bank.'
                                </td>
                                 
                                <td  style="padding:10px;border:1px solid #333; background-color: #77e286;font-size:13px;">
                                    <b>SUBTOTAL</b>
                                </td>
                                <td style="padding:10px;border:1px solid #333; text-align:right;background-color: #77e286;">$
                                   '.$order[0]["subtotal"].' 
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:10px;border:1px solid #333;font-size:13px;">
                                    DISCOUNT
                                </td>
                                <td style="padding:10px;border:1px solid #333; text-align:right;">
                                   '.$order[0]["discount"].' 
                                %</td>
                            </tr>
                            <tr>
                                <td style="padding:10px;border:1px solid #333;font-size:13px;">
                                    TAX RATE
                                </td>
                                <td style="padding:10px;border:1px solid #333; text-align:left;">
                                   %
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:10px;border:1px solid #333;font-size:13px;">
                                    SALES TAX
                                </td>
                                <td style="padding:10px;border:1px solid #333; text-align:left;">
                                   $
                                </td>
                            </tr>
                            
                            <tr>
                                <td style="padding:10px;border:1px solid #333;background-color:#39b54a;color:white;font-size:13px;">
                                    <b>TOTAL</b>
                                </td>
                                <td style="padding:10px;border:1px solid #333; text-align:right;background-color:#39b54a;color:white;font-weight:bold;">$
                                    '.$order[0]["total"].'  '.$order[0]["currency_type"].' 
                                </td>
                            </tr>
                            <tr>
                        <!-- END BODY-->    
                </table>
                <table style="width:100%;margin-top:50px;">
                        <tr>
                            <td style="text-align:center;">
                                <img src="/ilto3/images/ede.png" width="200" style="">    
                            </td>
                            <td style="text-align:center;">
                                <img src="/ilto3/images/tecs.png" width="200" style="">       
                            </td>
                        </tr>
                    </table>';
                                 
            $mPDF1 = Yii::app()->ePdf->mpdf("","Letter");
            $mPDF1->WriteHTML($informe);
            $nombre = "invoice_".$invoice[0]["id_invoice"].".pdf";
            $mPDF1->Output();
            
            $tooaEmail = $toaa[0]["email"].", iltoexams@outlook.com";       
            $to = $tooaEmail;
            
            $modUtil = new Utilidades();
            $content = $mPDF1->Output('','S');
            $content = chunk_split(base64_encode($content));
            $mailto = $to; //Mailto here
            $from_name = 'ILTO'; //Name of sender mail
            $from_mail = 'info@iltoexams.com'; //Mailfrom here
            $subject = 'New purchase '.$agent.', Invoice No.'.$invoice[0]["id_invoice"].'';
            $message = '<p><form action="https://iltoexams.com/ilto3/index.php?r=Clients/default/login" method="post">
    		An invoice has been issued as a result of your purchase confirmation.  Click on the link to view or print the invoce.
    		<input type="hidden" name="flag" value="1">
    		<input type="submit" value="View Info" style="border-radius: 10px;padding:20px;width:150px;background-color:#31b744;color:#FFF;font-size:14px;font-weight:bold;margin:auto;">
    		Sincerely,<br/><br/>
                <a href="http://www.iltoexams.com/"><img style="float:left;top:20px;" alt="ILTO Exams" src="iltoexams.com/logo_ILTO.png" /></a>
    		</form></p>';
            $filename = $nombre;
            //Headers of PDF and e-mail
            $boundary = "XYZ-" . date("dmYis") . "-ZYX"; 
            $header .= "Content-Transfer-Encoding: 8bits\r\n"; 
            $header .= "Content-Type: text/html; charset=ISO-8859-1\r\n\r\n"; // or utf-8
            $header .= "$message\r\n";
            $header .= "--$boundary\r\n";
            $header .= "Content-Type: application/pdf; name=\"".$filename."\"\r\n";
            $header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n";
            $header .= "Content-Transfer-Encoding: base64\r\n\r\n";
            $header .= "$content\r\n"; 
            $header .= "--$boundary--\r\n";
            $header2 = "MIME-Version: 1.0\r\n";
            $header2 .= "From: ".$from_name." <".$from_mail."> \r\n"; 
            $header2 .= "Return-Path: $from_mail\r\n";
            $header2 .= "Content-type: multipart/mixed; boundary=\"$boundary\"\r\n";
            $header2 .= "$boundary\r\n";
            
            
            if(mail($mailto,$subject,$header,$header2, "-r".$from_mail)){
                Yii::app()->user->setFlash('success','An Email was sent to '.$modUsuario->email.' with the certificate');
                return true;
            } else {
                $error = print_r(error_get_last(),true);
                Yii::app()->user->setFlash('error',$error);
                return false;
            }
            
	}
	
	public function InvoiceDetail($id){
	    
	    
		// CONSTRUCCIÓN DEL PDF
            $SQL = "SELECT * FROM `invoices` WHERE id_invoice='".$id."'";
            $invoice = Yii::app()->db->createCommand($SQL)->queryAll();
            
            $SQL = "SELECT * FROM `orders` WHERE id_order='".$invoice[0]["id_order"]."'";
            $order = Yii::app()->db->createCommand($SQL)->queryAll();
            
            $SQL = "SELECT * FROM `order_licenses` WHERE `id_order` =  '".$order[0]["id_order"]."'";
            $order_items = Yii::app()->db->createCommand($SQL)->queryAll();
            
            $SQL = "SELECT * FROM `clients` WHERE id_cliente='".$order[0]["id_client"]."'";
            $client= Yii::app()->db->createCommand($SQL)->queryAll();
            
            $SQL = "SELECT city.name as city, country.name as country FROM `city` INNER JOIN country ON city.CountryCode = country.Code WHERE city.ID ='".$client[0]["id_ciudad"]."'";
            $country= Yii::app()->db->createCommand($SQL)->queryAll();
            
            $SQL = "SELECT nombres, apellidos, email FROM `users_c` WHERE id_cliente='".$client[0]["id_cliente"]."' AND id_perfil='2'";
            $toaa= Yii::app()->db->createCommand($SQL)->queryAll();
            
            $distributor = '<span><b>Agent:</b>ILTOEXAMS'; 
            
            if( $invoice[0]["id_invoice"] < 100){
                $inv = str_pad ($invoice[0]["id_invoice"], 4,"0", STR_PAD_LEFT);
            }
            
            if( ($invoice[0]["id_invoice"] > 100) && ($invoice[0]["id_invoice"] < 1000)){
                $inv = str_pad ($invoice[0]["id_invoice"], 3,"0", STR_PAD_LEFT);
            }
            
            if($order[0]["currency_type"] == "COP"){
                $ilto_header = 'ILTO COLOMBIA<br/>Nit: 900430264_7<br/>
                Contact:312 515 3017<br/>Calle 25 D No 36-07, Bogotá - Colombia<br/>';
                
                $bank = '<p style="font-size:14px;">
                BANCO DAVIVIENDA<br/> 
                SAVING ACCOUNT # 4502 0005 8995
                </p> ';
                
            }else{
                $ilto_header = 'International Language Testing Organization Inc.
                8300 NW 53rd Street Suite 350 Doral FL 33166<br/><br/><br/>';    
                    
                $bank = '<p style="font-size:14px;">
                BANK OF AMERICA<br/> SWIFT ADDRESS: BOFAUS3N<br/> RECEIVING BANK: ABA # is 026009593<br/> BENEFICIARY NAME: international Language Testing Organization Inc.<br/>
                BENEFICIARY ACCOUNT#: 898062667428<br/> ADDITIONAL REFERENCE: Bank of America, FL7-573-01-01<br/> 3025 NW 87 Ave, Doral, FL 33172
                </p> ';
            }
            
            switch ($order[0]['terms']) {
        		case '0':
        		    $terms= "Due on recipt";
        		break;
        		case '1':
        		    $terms= "30 days";
        		break;
        		case '2':
        		    $terms= "60 days";
        		break;
        		case '3':
        		    $terms= "90 days";
        		break;
        		default:
        		    $terms= "Due on recipt";
        	}
            $nombres = $toaa[0]["nombres"]." ".$toaa[0]["apellidos"];
            $addr = $client[0]["direccion"]." ".$country[0]["country"]." ".$country[0]["city"];
            setlocale(LC_ALL, "en_US.utf8"); 
            $nombre = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $nombres);
            $agent = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE',$client[0]["nombre_rsocial"]);
            $addres = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $addr);
            $informe = '
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
         <div class="widget">
            <div class="widget-body">
                <div class="row" style="padding:10px;"><!--ORDER Header-->
                    <table>
                        <tr>
                            <td style="width:33%;">
                                    <img src="/ilto3/images/logo_ilto_original.png" width="200" style="">
                            </td>
                            <td style="width:33%;text-align:left;font-size:12px;">'.$ilto_header.'
                                        www.iltoexams.com
                            </td>
                            <td style="width:33%;">
                            
                                <div class="col-lg-4 col-sm-4 col-xs-4">
                                    <table style="padding:0px;margin:0px;">
                                        <tr>
                                            <td colspan="2" style="text-align:center;">
                                                <h2 style="left:30px;color:#f4b400;">COMMERCIAL INVOICE</h2>
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #333;background-color:#39b54a;color:white;font-weight:bold;text-align:center;font-size:12px;">
                                                Terms
                                            </td>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #333;background-color:#39b54a;color:white;font-weight:bold;text-align:center;font-size:12px;">
                                                Invoice No. 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #333; text-align:center;font-size:12px;">
                                             '.$terms.' 
                                            </td>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #333; text-align:center;font-size:12px;">
                                                0'.$inv.'
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #333;background-color:#f4b400;color:white;font-weight:bold;text-align:center;font-size:12px;">
                                                Date
                                            </td>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #333;background-color:#f4b400;color:white;font-weight:bold;text-align:center;font-size:12px;">
                                                Due Date
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #333; text-align:center;font-size:12px;">
                                               '.date("Y-m-d", strtotime($invoice[0]["date_expedition"])).' 
                                            </td>
                                            <td style="width:100px;height:15px;padding:5px;border:1px solid #333; text-align:center;font-size:12px;">
                                                '.date("Y-m-d", strtotime($order[0]["date_expire"])).'
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <!-- END ORDER Header-->
                    <div class="row" style="padding:15px;"><!--BILL TO-->
                    <table style="width:100%;">
                        <tr>
                            <td style="width:60%;padding:5px;text-align:left;border:1px solid #333;background-color:#39b54a;min-height:20px;color:#fff;font-size:12px;font-weight:bold;">
                                Bill to
                            </td>
                            <td style="width:10%;padding:5px;text-align:center;background-color:#fff;min-height:20px;color:black;font-size:12px;">
                                
                            </td>
                            <td style="width:30%;padding:5px;text-align:center;border:1px solid #333;background-color:#f4b400;min-height:20px;color:#fff;font-size:12px;font-weight:bold;">
                               Amount Due
                            </td>
                        </tr>
                        <tr>
                            <td style="width:60%;padding:15px;text-align:left;border:1px solid #333;background-color:#fff;;min-height:140px;color:black;font-size:12px;">
                                <div class="row" style="padding:15px 20px;">
                               <span><b>'.$agent.'</b></span><br/>  
                                <span><b>'.$client[0]["nit_id_legal"].'</b></span><br/>
                                <span><b>'.$addres.'</b></span><br/>
                                <span><b>Phone Number: </b> '.utf8_decode($client[0]["telefono"]).'</span><br/>
                                <span><b>Email: </b>'.$toaa[0]["email"].'</span></td>
                            <td style="width:10%;padding:15px;text-align:center;background-color:#fff;min-height:140px;color:black;font-size:12px;">
                                
                            </td>
                            <td style="width:30%;padding:15px;text-align:center;border:1px solid #333;background-color:#fff;;min-height:140px;color:black;font-size:12px;">
                               $'.number_format($order[0]["total"],2,",",".").' '.$order[0]["currency_type"].' 
                            </td>
                        </tr>
                    </table>
                </div><!-- END BILL TO-->
                     <div class="row" id="body_order" style="min-height:300px; padding:15px;"><!--BODY -->
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <table style="width:100%;">
                            <tr>
                                <td style="width:70px;padding:5px;color:white;font-weight:800;text-align:center;font-size:12px;">
                                    
                                </td>
                                <td style="width:350px;padding:5px;color:white;font-weight:800;text-align:center;font-size:12px;">
                                   
                                </td>
                                <td style="width:70px;padding:5px;color:white;font-weight:800;text-align:center;font-size:12px;">
                                   
                                </td>
                                <td style="width:70px;padding:5px;color:white;font-weight:800;text-align:center;font-size:12px;">
                                     
                                </td>
                                <td style="width:100px;padding:5px;border:1px solid #0f5418;background-color:#39b54a;color:white;font-weight:800;text-align:center;font-size:12px;">
                                    Sales Rep
                                </td>
                            </tr>
                            <tr>
                                <td style="width:70px;padding:5px;color:white;font-weight:800;text-align:center;font-size:12px;">
                                    
                                </td>
                                <td style="width:350px;padding:5px;color:white;font-weight:800;text-align:center;font-size:12px;">
                                   
                                </td>
                                <td style="width:70px;padding:5px;font-weight:800;text-align:center;font-size:12px;">
                                   
                                </td>
                                <td style="width:70px;padding:5px;color:white;font-weight:800;text-align:center;font-size:12px;">
                                     
                                </td>
                                <td style="width:100px;padding:5px;border:1px solid #0f5418;background-color:#fff;color:black;font-weight:800;text-align:center;font-size:12px;">
                                    Matt
                                </td>
                            </tr>
                            <tr>
                                <td style="width:70px;padding:5px;border:1px solid #333;background-color:#f4b400;color:white;font-weight:bold;text-align:center;font-size:12px;">
                                    Licenses valid thru
                                </td>
                                <td style="width:350px;padding:5px;border:1px solid #333;background-color:#f4b400;color:white;font-weight:bold;text-align:center;font-size:12px;">
                                    Description
                                </td>
                                <td style="width:70px;padding:5px;border:1px solid #333;background-color:#f4b400;color:white;font-weight:bold;text-align:center;font-size:12px;">
                                    Quantity
                                </td>
                                <td style="width:70px;padding:5px;border:1px solid #333;background-color:#f4b400;color:white;font-weight:bold;text-align:center;font-size:12px;">
                                     Rate
                                </td>
                                <td style="width:100px;padding:5px;border:1px solid #333;background-color:#f4b400;color:white;font-weight:bold;text-align:center;font-size:12px;">
                                    Amount
                                </td>
                            </tr>';
                        
                        // LICENSES
                        
                            foreach($order_items as $item){
                                
                                $SQL = "SELECT nombre FROM `licenses` WHERE `id_licencia` =  '".$item["id_licencia"]."'";
                                $licenseName = Yii::app()->db->createCommand($SQL)->queryAll();
             
                                $informe .= '
                                <tr style="border:1px solid black;">
                                    <td style="padding:15px 10px;border:1px solid #333;font-weight:800;text-align:center;font-size:14px;border-right:1px solid black;">
                                        '.date("Y-m-d", strtotime($order[0]["date_expire"])).' 
                                    </td>
                                    <td style="padding:15px 10px;border:1px solid #333;font-weight:800;text-align:left;font-size:14px;border-right:1px solid black;">
                                        '.$licenseName[0]["nombre"].' 
                                    </td>
                                    <td style="padding:5px 10px;border:1px solid #333;font-weight:800;text-align:center;font-size:14px;border-right:1px solid black;">
                                        '.$item["quantity"].'
                                    </td>
                                    <td style="padding:15px 10px;border:1px solid #333;font-weight:800;text-align:center;font-size:14px;border-right:1px solid black;">$
                                        '.$item["price"].'
                                    </td>
                                    <td style="padding:15px 10px;border:1px solid #333;font-weight:800;text-align:right;font-size:14px;border-right:1px solid black;">$
                                        '.number_format($item["total"],2,",",".").'
                                    </td>
                                </tr>';
                            }
                            
                            $informe .= '
                            <tr>
                                <td colspan="3" rowspan="5" style="padding:10px;background-color:#fff">
                                     '.$bank.'              
                                </td>
                                 
                                <td  style="padding:10px;border:1px solid #333; background-color: #77e286;font-size:13px;">
                                    <b>SUBTOTAL</b>
                                </td>
                                <td style="padding:10px;border:1px solid #333; text-align:right;background-color: #77e286;">$
                                   '.number_format($order[0]["subtotal"],2,",",".").' 
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:10px;border:1px solid #333;font-size:13px;">
                                    DISCOUNT
                                </td>
                                <td style="padding:10px;border:1px solid #333; text-align:right;">
                                   '.$order[0]["discount"].' 
                                %</td>
                            </tr>
                            <tr>
                                <td style="padding:10px;border:1px solid #333;font-size:13px;">
                                    TAX RATE
                                </td>
                                <td style="padding:10px;border:1px solid #333; text-align:left;">
                                   %
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:10px;border:1px solid #333;font-size:13px;">
                                    SALES TAX
                                </td>
                                <td style="padding:10px;border:1px solid #333; text-align:left;">
                                   $
                                </td>
                            </tr>
                            
                            <tr>
                                <td style="padding:10px;border:1px solid #333;background-color:#39b54a;color:white;font-size:13px;">
                                    <b>TOTAL</b>
                                </td>
                                <td style="padding:10px;border:1px solid #333; text-align:right;background-color:#39b54a;color:white;font-weight:bold;">$
                                    '.number_format($order[0]["total"],2,",",".").' '.$order[0]["currency_type"].'  
                                </td>
                            </tr>
                            <tr>
                        <!-- END BODY-->    
                </table>
                <table style="width:100%;margin-top:50px;">
                        <tr>
                            <td style="text-align:center;">
                                <img src="/ilto3/images/ede.png" width="200" style="">    
                            </td>
                             <td style="text-align:center;">
                                 <img src="/ilto3/images/tecs.png" width="200" style="">       
                             </td>
                         </tr>
                </table>';
                
                //require '/home/thetec/public_html/vendor/autoload.php';
				require '/home/iltoexam/public_html/vendor/autoload.php';
					
			    $mpdf = new \Mpdf\Mpdf(['tempDir' => __DIR__ . '/../temp']);
				$mpdf->WriteHTML($informe);
				$mpdf->Output();
                                 
           
	}
	
	public function changePass($user, $oldPass, $newPass){
	    
	    $model = UsersC::model()->findByAttributes(array("id_usuario_c"=>$user));
	    $md5_oldPass =md5($oldPass);
	    $md5_newPass =md5($newPass);
	    $flagError = 0;
	    
	    // Validamos si la contraseña anterior y la nueva son iguales
	    if(strcmp($md5_oldPass, $md5_newPass) == 0){
	        return false;
	    }else{
	       
	        //Si la contraseña anterior es igual a la password antigua que viene del POST     
    	    if(strcmp($md5_oldPass, $model["clave"]) == 0){
	        
    	        $SQL = "UPDATE users_c SET clave= '".$md5_newPass."', clave2 = '".$newPass."' WHERE `id_usuario_c` = '".$user."'";
                $update= Yii::app()->db->createCommand($SQL)->query();
                
                $SQL = "INSERT INTO password_change (id_usuario_c, date, email_confirm) VALUES ('".$user."','".date("Y-m-d H:i:s")."', 0)";
                $ins= Yii::app()->db->createCommand($SQL)->query();
                
                $to = $model->email;
                $subject = "Password Update";
                $htmlBody = '<h3>IMPORTANT NOTICE</h3><p><form action="https://www.iltoexams.com/ilto3/index.php?r=Clients/default/login&type=2" method="post">
        		By clicking on the "I Accept" button, I acknowledge that: <br/><br/> 
                1. The username and password is for my use only and I cannot share this information with anyone else. <br/>
                2. If my username is involved in any fraud activity, I will take responsibility fot it and I will cooperate with any fraud investigation.<br/>
                3. If my username is involved in any fraud related activity, I will be banned from using ILTO  administrator dashboard or perform any rater or daministator related activity.<br/>
                4. The International Language Testing Organization is a company based in U.S.A., protected by U.S. the government agencies to prevent any fraud related activity. <br/>
                <br/><br/><br/>
        		<input type="hidden" name="flag_econfirm" value="1">
        		<input type="submit" value="I ACCEPT" style="border-radius: 10px;padding:20px;width:150px;background-color:#31b744;color:#FFF;font-size:14px;font-weight:bold;margin:auto;"><br/><br/>
        		Sincerely,<br/><br/>
                    <a href="http://www.iltoexams.com/"><img style="float:left;top:20px;" alt="ILTO Exams" src="iltoexams.com/logo_ILTO.png" /></a>
        		</form></p>
                ';
                $dat = "{From: 'noreply@iltoexams.com', To: '$to', Cc: 'info@iltoexams.com', Subject: '$subject', HtmlBody: '$htmlBody'}";//array('From' => 'noreply@iltoexams.com', 'To'=>'dokho_02@hotmail.com', 'Subject'=>'Hola Chris Fer', 'HtmlBody'=>'<strong>Hello</strong> dear Postmark user.');
                $this->sendEmail($dat);
                
                return true;
    	    }else {
                return false;
            }
        }
	}
	
	 public function getAvailableTecsId($id_client){
        
        //nos traemos la lista de los examenes TECS
        $SQL = "SELECT licenses.id_licencia FROM `licenses` INNER JOIN licenses_client ON licenses.id_licencia = licenses_client.id_licencia 
        WHERE `nombre` LIKE '%Test of English Communication Skills%' AND licenses.estado = 'A' 
        AND id_cliente = '".$id_client."' AND licenses_client.estado = 'A' ";
        $ids_query= Yii::app()->db->createCommand($SQL)->queryAll();
        
        //instance an array for the ids
        $ids = array();
        foreach ($ids_query as $id) {
            //push id into array
            array_push($ids, $id["id_licencia"]);
        }
       
        //entregamos el string
        return $ids;
        
    }
	
    
    public function getIdTecs(){
        
        $SQL = "SELECT id_licencia FROM `licenses` WHERE `nombre` LIKE '%TECS - Test of English Communication Skills%' ORDER BY id_licencia DESC LIMIT 1";
        $id_last_tecs= Yii::app()->db->createCommand($SQL)->queryAll();
        $id_tecs = $id_last_tecs[0]["id_licencia"];
        
        return $id_tecs;
        
    }
    
    public function getIdsTecs(){
        
        //nos traemos la lista de los examenes TECS
        $SQL = "SELECT id_licencia FROM `licenses` WHERE `nombre` LIKE '%TECS%'";
        $ids= Yii::app()->db->createCommand($SQL)->queryAll();
        
        //armamos la lista de ids en un string
        $ids_string = "";
        foreach ($ids as $id) {
            $ids_string .= $id["id_licencia"].",";
        }
        
        //le quitamos el último caracter para quitar la coma final
        $ids_string = rtrim($ids_string, ",");
        //entregamos el string
        return $ids_string;
        
    }
    
    public function getIdsEde(){
        
        //nos traemos la lista de los examenes TECS
        $SQL = "SELECT id_licencia FROM `licenses` WHERE `nombre` LIKE '%EDE%'";
        $ids= Yii::app()->db->createCommand($SQL)->queryAll();
        
        //armamos la lista de ids en un string
        $ids_string = "";
        foreach ($ids as $id) {
            $ids_string .= $id["id_licencia"].",";
        }
        
        //le quitamos el último caracter para quitar la coma final
        $ids_string = rtrim($ids_string, ",");
        //entregamos el string
        return $ids_string;
        
    }
    
    
    public function getIdEde(){
        
        $SQL = "SELECT id_licencia FROM `licenses` WHERE `nombre` LIKE '%EDE - English diagnosis exam%' ORDER BY id_licencia DESC LIMIT 1";
        $id_last_ede= Yii::app()->db->createCommand($SQL)->queryAll();
        $id_ede = $id_last_ede[0]["id_licencia"];
        
        return $id_ede;
        
    }
    
    public function getIdSpeaking(){
        
        $SQL = "SELECT id_examen FROM `exams` WHERE `nombre` LIKE '%speaking%' ORDER BY id_examen DESC LIMIT 1";
        $id_last_speaking= Yii::app()->db->createCommand($SQL)->queryAll();
        $id_speaking = $id_last_speaking[0]["id_examen"];
        
        return $id_speaking;
        
    }
    
    public function IsSpeaking($id_exam){
    
        $SQL = "SELECT id_examen FROM `exams` WHERE `nombre` LIKE '%speaking%' AND id_examen ='".$id_exam."'";
        $check_speaking= Yii::app()->db->createCommand($SQL)->queryAll();
        
        if($check_speaking[0]["id_examen"] != ""){
            $is_speaking = 1;
        }else{
            $is_speaking = 0;
        }
        
        return $is_speaking;
        
    }
    
    public function isEde($license){
        // get License ID type
        $SQL = "SELECT id_licencia FROM licenses_user INNER JOIN licenses_client ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente WHERE id_licencias_usuario ='".$license."'";
        $license= Yii::app()->db->createCommand($SQL)->queryAll();
        
        //check if this License have EDE on name's license
        $SQL = "SELECT * FROM `licenses` WHERE `id_licencia` = '".$license[0]["id_licencia"]."' AND nombre LIKE '%EDE%'";
        $check_ede= Yii::app()->db->createCommand($SQL)->queryAll();
        
        if(!empty($check_ede)){
            return true;
        }else{
            return false;
        }
       
        
    }
    
    public function isTecs($license){
        
       // get License ID type
        $SQL = "SELECT id_licencia FROM licenses_user INNER JOIN licenses_client ON licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente WHERE id_licencias_usuario ='".$license."'";
        $license= Yii::app()->db->createCommand($SQL)->queryAll();
        
        //check if this License have EDE on name's license
        $SQL = "SELECT * FROM `licenses` WHERE `id_licencia` = '".$license[0]["id_licencia"]."' AND nombre LIKE '%TECS%'";
        $check_tecs= Yii::app()->db->createCommand($SQL)->queryAll();
        
        if(!empty($check_tecs)){
            return true;
        }else{
            return false;
        }
        
    }
    
    public function updateSingleLicence($id, $amount_p, $price_p, $free, $currency){
        $modUtil = new Utilidades();
        $edeId = $modUtil->getIdEde();
        
        $SQL = "SELECT * FROM `order_licenses` WHERE `id_order_license` =  '".$id."'";
	    $order_items = Yii::app()->db->createCommand($SQL)->queryAll();
	         
            // Validamos, si varia el precio
            if($price_p != $order_items[0]["price"]){
                
            	if($amount_p != $order_items[0]["quantity"]){
            		//VARIA PRECIO, VARIA CANTIDAD
            		$total_lic = ($amount_p*$price_p);
            		
            		$price = $price_p;
            		//EDE FREE
            		
            			if($order_items[0]["id_licencia"]==$edeId){
            			    if($free=="on"){
            				$price = 0;
            				//	var_dump("ede free");
            			}
            		}
            		$SQL = "UPDATE `order_licenses` SET `quantity`='".$amount_p."',`price`='".$price."',`total`='".$total_lic."' WHERE `id_order_license`='".$id."'";
            		$update_order = Yii::app()->db->createCommand($SQL)->query();
            		$subtotal_order_lic =  $subtotal_order_lic + $total_lic;
            		
            	}else{ 
            		//VARIA PRECIO, NO VARIA CANTIDAD
            		//EDE FREE
            			if($order_items[0]["id_licencia"]==$edeId){
            				if($free=="on"){
            		
            				$price = 0;
            			//var_dump("ede free");
            			}
            		}
            		$total_lic = ($amount_p*$price_p);
            		$SQL = "UPDATE `order_licenses` SET `quantity`='".$amount_p."',`price`='".$price_p."',`total`='".$total_lic."' WHERE `id_order_license`='".$id."'";
            		$update_order = Yii::app()->db->createCommand($SQL)->query();
            		$subtotal_order_lic =  $subtotal_order_lic + $total_lic;
            		
            	}
            	
            }else{ 
                
            	//NO VARIA PRECIO, VARIA CANTIDAD
            	if($amount_p != $order_items[0]["quantity"]){
            		//VARIA precio, VARIA cantidad 
            		//CONSULTAMOS EL PRECIO POR LA CANTIDAD ESPECIFICADA
            		$SQL = "SELECT price FROM licenses_prices WHERE '".$amount_p."' BETWEEN `min` AND `max` AND id_licencia= '".$order_items[0]["id_licencia"]."'";
                    $price_result= Yii::app()->db->createCommand($SQL)->queryAll();
                    $price = ($price_result[0]["price"]*$currency);
                    //EDE FREE
            		if($free=="on"){
            			if($order_items[0]["id_licencia"]==$edeId){
            				$price = 0;
            				//var_dump("ede free");
            			}
            		}
                    $total_lic = ($amount_p*$price);
            		$SQL = "UPDATE `order_licenses` SET `quantity`='".$amount_p."',`price`='".$price."',`total`='".$total_lic."' WHERE `id_order_license`='".$id."'";
            		$update_order = Yii::app()->db->createCommand($SQL)->query();
            		//var_dump($SQL);
            		$subtotal_order_lic =  $subtotal_order_lic + $total_lic;
            	}else{
            		//EDE FREE, NO VARIA PRECIO; NO VARIA CANTIDAD
            		$price = $price_p;
            		$total_lic = ($amount_p*$price);
            		
            		if($free=="on"){
			        	if($order_items[0]["id_licencia"]==$edeId){
			            	$price = 0;
			            	$total_lic = ($amount_p*$price);
			            	//var_dump("ede free");
			            	$SQL = "UPDATE `order_licenses` SET `quantity`='".$amount_p."',`price`='".$price."',`total`='".$total_lic."' WHERE `id_order_license`='".$id."'";
            				$update_order = Yii::app()->db->createCommand($SQL)->query();
            				//var_dump($SQL);
			            }
			         } 
			         $subtotal_order_lic =  $subtotal_order_lic + $total_lic;
			         
			     }
			     
        	}
        	return $subtotal_order_lic;
    }
    
    function getExchangeRate() {
        
        /*
        $data = file_get_contents("https://dolar.wilkinsonpc.com.co/divisas/dolar.html");
        var_dump($data);die();
        preg_match_all('#<span[^<>]*>([\d,]+).*?</span>#',$data, $converted);
        return $converted[0][0];*/
        $converted = 4080.00;
        return $converted;
    } 
    
    function tofloat($num) {
        $dotPos = strrpos($num, '.');
        $commaPos = strrpos($num, ',');
        $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos : 
            ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);
       
        if (!$sep) {
            return floatval(preg_replace("/[^0-9]/", "", $num));
        } 
    
        return floatval(
            preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
            preg_replace("/[^0-9]/", "", substr($num, $sep+1, strlen($num)))
        );
    }
    
    function deleteClient($client){
		
		//Delete client licenses
		$SQL = "DELETE FROM `licenses_client` WHERE `id_cliente` LIKE '".$client."'";
		$list= Yii::app()->db->createCommand($SQL)->query();
				    
		//Delete client class rooms
		$SQL = "DELETE FROM `class_rooms` WHERE `id_cliente` LIKE '".$client."'";
		$list= Yii::app()->db->createCommand($SQL)->query();
				
		// Borrar grupos
		$SQL = "DELETE FROM `groups_users_clients` WHERE `id_cliente` LIKE '".$client."'";
		$list= Yii::app()->db->createCommand($SQL)->query();
				    	
		//Borrar usuarios
		$SQL = "DELETE FROM `users_c` WHERE `id_cliente` LIKE '".$client."'";
		$list= Yii::app()->db->createCommand($SQL)->query();
				    	
		//Borrar cliente
		$SQL = "DELETE FROM `clients` WHERE `id_cliente` LIKE '".$client."'";
		$list= Yii::app()->db->createCommand($SQL)->query();
	}
	
	public function LicensesAlertDashboard(){
	    
	    $idCliente = Yii::app()->user->getState('cliente');
	    //nos traemos las licencias de cada cliente que están disponibles con su fecha final
        $SQL = "SELECT id_licencias_cliente, licenses_client.id_licencia, nombre, fecha_final, (cantidad - utilizados) as available FROM `licenses_client` INNER JOIN licenses ON licenses.id_licencia = licenses_client.id_licencia WHERE id_cliente = '".$idCliente."' AND licenses_client.estado ='A'";
        $licensesClient= Yii::app()->db->createCommand($SQL)->queryAll();
        
        $notifications = array();
        $idClient = $client['id_cliente'];
        //comparamos esa fecha final, y guardamos los ids de las licencias en los respectivos arrays
        foreach($licensesClient as $license){
            //Convert to date
            $datefL = $license["fecha_final"];//Your date
            $idL = $license["id_licencias_cliente"];
            $quantity = $license["available"];
            $licenseName = explode("V", $license["nombre"]);
            $date=strtotime($datefL);//Converted to a PHP date (a second count)
            
            //Calculate difference
            $diff=$date-time();//time returns current time in seconds
            $days=intval(floor($diff/(60*60*24)));//seconds/minute*minutes/hour*hours/day)
           // var_dump($idL, $datefL, $days);
           
            // guardamos el id de la licencias del cliente en array 30 days left
           if($days == 29){
               $date = substr($datefL,0,10);
               $date = " on ".$date;
               array_push($notifications, array($idL,$licenseName[0],$date,$quantity,$days+1));
            }
            
            // guardamos el id de la licencias del cliente en array 15 days left
            if($days == 14){
                $date = substr($datefL,0,10);
               $date = " on ".$date;
                array_push($notifications, array($idL,$licenseName[0],$date,$quantity,$days+1));
            }
            
            // guardamos el id de la licencias del cliente en array 5 days left
            if($days == 4){
                $date = substr($datefL,0,10);
               $date = " on ".$date;
                array_push($notifications, array($idL,$licenseName[0],$date,$quantity,$days+1));
            }
            
            if($days == -1){
              array_push($notifications, array($idL,$licenseName[0],"TODAY",$quantity,$days+1));
                
            }
        }
        
        return $notifications;
	}
	
	public function LicensesEndAlertDashboard(){
	    
	    $idCliente = Yii::app()->user->getState('cliente');
	    //nos traemos las licencias de cada cliente que están disponibles con su fecha final
        $SQL = "SELECT id_licencias_cliente, licenses_client.id_licencia, nombre, (cantidad - utilizados) as available FROM `licenses_client` INNER JOIN licenses ON licenses.id_licencia = licenses_client.id_licencia WHERE id_cliente = '".$idCliente."' AND licenses_client.estado ='A'";
        $licensesClient= Yii::app()->db->createCommand($SQL)->queryAll();
        
        $notificationsEnd = array();
        $idClient = $client['id_cliente'];
        //comparamos esa fecha final, y guardamos los ids de las licencias en los respectivos arrays
        foreach($licensesClient as $license){
            //Convert to date
            $datefL = $license["fecha_final"];//Your date
            $idL = $license["id_licencias_cliente"];
            $quantity = $license["available"];
            
            
            //Calculate difference
            $diff=$date-time();//time returns current time in seconds
            $days=intval(floor($diff/(60*60*24)));//seconds/minute*minutes/hour*hours/day)
           // var_dump($idL, $datefL, $days);
           
            // guardamos el id de la licencias del cliente en array 30 days left
           if($quantity == 25){
               array_push($notificationsEnd, array($idL,$licenseName[0],$quantity));
            }
            
            // guardamos el id de la licencias del cliente en array 15 days left
            if($quantity == 0){
                array_push($notificationsEnd, array($idL,$licenseName[0],$quantity));
            }
        }
        
        return $notificationsEnd;
	}
	
	function robocop($userId,$license,$idExam,$roboFlag){
	    
	    
	    //roboFlag en 1 indica que es un speaking, en 0 que es otro examen para posterior evaluación 
	    if($roboFlag == 1){
	        
	        //VALIDAMOS SI EL EXAMEN ES ABIERTO POR OTRO TUTOR
            $SQL = "SELECT * FROM `adm_log` WHERE id_exam = '".$idExam."' GROUP BY id_adm ";
            $tutorQuantity= Yii::app()->db->createCommand($SQL)->queryAll();
            
            //Consultamos la información del usuario
            $SQL = "SELECT * FROM `users_c` WHERE id_usuario_c = '".$userId."'";
            $user= Yii::app()->db->createCommand($SQL)->queryAll();
            
            //Consultamos el correo del total
            $SQL = "SELECT email FROM `users_c` WHERE `id_cliente` LIKE '".$user[0]["id_cliente"]."' AND `id_perfil` = 2 LIMIT 1";
            $email= Yii::app()->db->createCommand($SQL)->queryAll();
            
            //$to = $email[0]["email"];
            $to = "ing.christian.velandia@gmail.com";
            
             $fraudFlag = 0;
            
            // dos tutores abriendo el speaking
    	    if(sizeof($tutorQuantity)>=2){
    	        $subject = "Fraud Alert - Speaking Section/ Multiple raters.";
                $htmlBody = '<p>Dear Test Administrator,<br><br> Our system has detected that the speaking section of the TECS for the test taker <b>'.$user[0]["nombres"].' '.$user[0]["apellidos"].'</b> with ID No. <b>'.$user[0]["id_usuario_c"].'</b> has been opened and possibly altered by different raters as follows:</p>';
    	        $i = 1;
                foreach ($tutorQuantity as $tutor) {
                    //VALIDAMOS SI EL EXAMEN ES ABIERTO MULTIPLES VECES POR UN MISMO TUTOR
            	    $SQL = "SELECT * FROM `host_license` WHERE `id_licencia_usuario` = '".$license."' AND `fecha_inicio_examen` = '".$tutor["date_time"]."' ORDER BY `id_host_license`";
            	    $host= Yii::app()->db->createCommand($SQL)->queryAll();
    	            
    	            $date = explode(" ", $tutor["date_time"]);
                    $htmlBody .= '<b>Attempt '.$i.'</b><br><br>
                    Rater username: '.$tutor["id_adm"].'<br> 
                    Date: '.$date[0].'<br>
                    Time: '.$date[1].'<br>  
                    IP:   '.$host[0]["ip_client"].'<br>
                    DNS:  '.$host[0]["dns_client"].'<br><br>';        
                    $i++;
    	            
    	        }
    	        
    	        $htmlBody .= 'Sincerely,<br/><br/>
                        <a href="http://www.iltoexams.com/"><img style="float:left;top:20px;" alt="ILTO Exams" src="iltoexams.com/logo_ILTO.png" /></a>';
    	        $dat = "{From: 'noreply@iltoexams.com', To: '$to', Cc: 'osdeisa@gmail.com', Subject: '$subject', HtmlBody: '$htmlBody'}";//array('From' => 'noreply@iltoexams.com', 'To'=>'dokho_02@hotmail.com', 'Subject'=>'Hola Chris Fer', 'HtmlBody'=>'<strong>Hello</strong> dear Postmark user.');
                //$this->sendEmail($dat);
                
                //removemos el envio de certificado automatico
                $SQL = "UPDATE `users_c` SET `email_certificate`=0  WHERE `id_usuario_c` = '".$userId."'";
                $updateUser = Yii::app()->db->createCommand($SQL)->query();
            
    	        
    	    }else{// un tutor abriendo el speaking
    	        //VALIDAMOS SI EL EXAMEN ES ABIERTO MULTIPLES VECES POR UN MISMO TUTOR
    	        $SQL = "SELECT COUNT(id_adm_log) as total, id_adm FROM `adm_log` WHERE id_exam = '".$idExam."' ";
                $examCount= Yii::app()->db->createCommand($SQL)->queryAll();
    	            //VALIDAMOS SI EL EXAMEN ES ABIERTO MULTIPLES VECES POR UN MISMO TUTOR
        	    $SQL = "SELECT COUNT(id_host_license) AS total FROM `host_license` WHERE `id_licencia_usuario` = '".$license."' ORDER BY `id_host_license` DESC ";
                $hostCount= Yii::app()->db->createCommand($SQL)->queryAll();
    	            
    	        if($hostCount[0]["total"]>=3){
    	            
        	        $subject = "TECS Fraud Alert -Speaking Section / Same rater.";         
        	        $htmlBody = '<p>Dear Test Administrator,<br><br> Our system has detected that the speaking section of the TECS for the test taker <b>'.$user[0]["nombres"].' '.$user[0]["apellidos"].'</b> with ID No. <b>'.$user[0]["id_usuario_c"].'</b> has been opened and possibly altered by <b>'.$examCount[0]["id_adm"].'</b> in the following dates and devices:</p>';
        	        
        	        //VALIDAMOS SI EL EXAMEN ES ABIERTO MULTIPLES VECES POR UN MISMO TUTOR
                	$SQL = "SELECT * FROM `host_license` WHERE `id_licencia_usuario` = '".$license."' ORDER BY `fecha_inicio_examen` ASC ";
                    $host_list= Yii::app()->db->createCommand($SQL)->queryAll();
                    
                    
                    $i = 1;        
                    foreach($host_list as $host){
                        
                        $date = explode(" ", $host["fecha_inicio_examen"]);
                        $htmlBody .= '<b>Attempt '.$i.'</b><br><br> 
                        Date: '.$date[0].'<br>
                        Time: '.$date[1].'<br>  
                        IP:   '.$host["ip_client"].'<br>
                        DNS:  '.$host["dns_client"].'<br><br>';        
                        $i++;
                    }
                    
                    $htmlBody .= 'Sincerely,<br/><br/>
                            <a href="http://www.iltoexams.com/"><img style="float:left;top:20px;" alt="ILTO Exams" src="iltoexams.com/logo_ILTO.png" /></a>';
                    $dat = "{From: 'noreply@iltoexams.com', To: '$to', Subject: '$subject', HtmlBody: '$htmlBody'}";//array('From' => 'noreply@iltoexams.com', 'To'=>'dokho_02@hotmail.com', 'Subject'=>'Hola Chris Fer', 'HtmlBody'=>'<strong>Hello</strong> dear Postmark user.');
                    $this->sendEmail($dat);
                    
                    //removemos el envio de certificado automatico
                    $SQL = "UPDATE `users_c` SET `email_certificate`=0  WHERE `id_usuario_c` = '".$userId."'";
                    $updateUser = Yii::app()->db->createCommand($SQL)->query();
    	        } 
    	    }
	    }else{
	        
	        //revisamos si la licencia tiene regitro en dos equipos diferentes
	        $SQL = "SELECT COUNT(id_host_license) as total FROM `host_license` WHERE `id_licencia_usuario` = '".$license."' ORDER BY `fecha_inicio_examen` ASC ";
            $regLicense= Yii::app()->db->createCommand($SQL)->queryAll();
	        
	        //si hay más de dos equipos registrados activamos la seguridad de robocop
	        if($regLicense[0]["total"]>=3){
	            $SQL = "SELECT * FROM `host_license` WHERE `id_licencia_usuario` = '".$license."' ORDER BY `fecha_inicio_examen` ASC ";
                $host_list= Yii::app()->db->createCommand($SQL)->queryAll();
	            
	            $subject = "TECS Fraud Alert - License opened in two different terminals by takers.";         
        	    $htmlBody = '<p>Dear Test Administrator,<br><br> Our system has detected that the license of the TECS for the test taker <b>'.$user[0]["nombres"].' '.$user[0]["apellidos"].'</b> with ID No. <b>'.$user[0]["id_usuario_c"].'</b> has been opened and possibly altered by in the following dates and devices:</p>';
        	        
	            
	            $i = 1;        
                    foreach($host_list as $host){
                        
                        $date = explode(" ", $host["fecha_inicio_examen"]);
                        $htmlBody .= '<b>Attempt '.$i.'</b><br><br> 
                        Date: '.$date[0].'<br>
                        Time: '.$date[1].'<br>  
                        IP:   '.$host["ip_client"].'<br>
                        DNS:  '.$host["dns_client"].'<br><br>';        
                        $i++;
                    }
                    
                    $htmlBody .= 'Sincerely,<br/><br/>
                            <a href="http://www.iltoexams.com/"><img style="float:left;top:20px;" alt="ILTO Exams" src="iltoexams.com/logo_ILTO.png" /></a>';
                    $dat = "{From: 'noreply@iltoexams.com', To: '$to', Cc: 'osdeis@gmail.com', Subject: '$subject', HtmlBody: '$htmlBody'}";//array('From' => 'noreply@iltoexams.com', 'To'=>'dokho_02@hotmail.com', 'Subject'=>'Hola Chris Fer', 'HtmlBody'=>'<strong>Hello</strong> dear Postmark user.');
                    $this->sendEmail($dat);
                    
                    //removemos el envio de certificado automatico
                    $SQL = "UPDATE `users_c` SET `email_certificate`=0  WHERE `id_usuario_c` = '".$userId."'";
                    $updateUser = Yii::app()->db->createCommand($SQL)->query();
	            
	            //revisamos si las dos IPs son las mismas y la hora en la que fue abierta la licencia desde diferentes secciones
	            
	        }
	        
	        
	    }
	    
	    
    }
    
    function normalizeDemo(){
        
        //how many demo licenses there are pending
        
        $SQL = "SELECT COUNT(id_licencias_usuario) as total FROM `licenses_user` WHERE `id_usuario` = '1001' AND estado = 'A' ORDER BY id_licencias_usuario ASC LIMIT 1";
        $count_demo= Yii::app()->db->createCommand($SQL)->queryAll();
        
        if($count_demo[0]["total"]>1){
            
            //get the older and pending license
            $SQL = "SELECT * FROM `licenses_user` WHERE `id_usuario` = '1001' AND estado = 'A' ORDER BY id_licencias_usuario ASC LIMIT 1";
            $old_license= Yii::app()->db->createCommand($SQL)->queryAll();
            
            //update the older and pending license to Finalized status
            $SQL = "UPDATE `licenses_user` SET `estado`='F'  WHERE `id_licencias_usuario` = '".$old_license[0]["id_licencias_usuario"]."'";
            $updateUser = Yii::app()->db->createCommand($SQL)->query();
        }
        
    }
    
    public function allocateEDELicense($id_taker, $data, $id_request, $id_admin, $reference){
        
        $modLicUsu = new LicensesUser();
        $modLicUsu->id_licencia_cliente = $data['id_licencias_cliente'];
        $modLicUsu->id_usuario=$id_taker;
        $modLicUsu->fecha_asignacion = new CDbExpression('NOW()');
        $modLicUsu->fecha_presentacion = $data['fecha_presentacion'];
        $modLicUsu->hora = $data['hora'];
        $modLicUsu->estado="A";
        
        $SQL = "INSERT INTO `licenses_user` (`id_licencias_usuario`, `id_licencia_cliente`, `id_usuario`, `fecha_asignacion`, `estado`, `fecha_presentacion`, `hora`, `calificacion`, `nivel`, `terms`) 
        VALUES (NULL, '".$data['id_licencias_cliente']."', '".$id_taker."', NOW(), 'A', '".$data['fecha_presentacion']."', '".$data['hora']."', '0.0', '', '0')";
       
        $insert_license= Yii::app()->db->createCommand($SQL)->query();
        
        //$modLicUsu->save();
        $id_license = Yii::app()->db->getLastInsertID();
        //Gastar la licencia
        
        $this->gastarLicencia($data['id_licencias_cliente']);
        //Asignar los exámenes al usuario
        $modLicenciasCliente = LicensesClient::model()->findByPk($modLicUsu->id_licencia_cliente);
        $modExamenes = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>$modLicenciasCliente->id_licencia));
        
        foreach($modExamenes as $examen){
	        $modExamenUsuario = new ExamsUser();
	        $modExamenUsuario->id_examen = $examen->id_examen;
            $modExamenUsuario->id_licencia_usuario = $modLicUsu->id_licencias_usuario;
            $modExamenUsuario->fecha_presentacion = $data['fecha_presentacion'];
            $modExamenUsuario->hora = $data['hora'];
            $modExamenUsuario->salon = $data['salon'];
            if(strlen($datos['tutor'])>0){
            	$modExamenUsuario->tutor = $data['tutor']; 
            }
            	$modExamenUsuario->estado = "A";
                //$modExamenUsuario->save();
                 $SQL = "INSERT INTO `exams_user` 
                 (`id_examen_usuario`, `id_licencia_usuario`, `id_examen`, `fecha_presentacion`, `calificacion`, `nivel`, `estado`, `hora`, `tutor`, `salon`, `a1_level`, `a2_level`, `b1_level`, `b2_level`, `c1_level`) 
                 VALUES (NULL, '".$id_license."', '".$examen->id_examen."', '".$data['fecha_presentacion']."', NULL, NULL, 'A', '".$data['hora']."', '".$id_admin."', '".$data['salon']."', '0', '0', '0', '0', '0')"; 
				$insert_exams= Yii::app()->db->createCommand($SQL)->query();
            }
                                
           
            	// Guardamos el registro en la tabla de period
                $SQL = "INSERT INTO `users_c_period`(`id_users_c_period`, `id_usuario_c`, `id_cliente`, `period`, `id_city`, `id_school`, `id_program`, `id_licencias_usuario`) 
                	VALUES ('', '".$id_taker."', '".$data['id_cliente']."', '".$data["period"]."', '".$data["city"]."', '".$data["school"]."', '".$data["program"]."', '".$id_license."')";
                    $list= Yii::app()->db->createCommand($SQL)->query();   
            
            
			// agregamos al log del usuario
            $SQL = "INSERT INTO `adm_log` (`id_adm_log`, `log_type`, `id_adm`, `date_time`, `id_license`, `id_exam`, `comment`, `id_user`) VALUES (NULL, 1, '".$id_admin."', NOW(), '$id_license', '', 'Assign a license to a test taker', '$id_taker')";
            $log= Yii::app()->db->createCommand($SQL)->query(); 
                                
            
            //update TECS ACADEMY REGISTER TO ENABLED TECS INTRO
    	
    		$SQL = "UPDATE `tecs_academy` SET `id_license`='".$id_license."' WHERE `id_tecs_academy`='".$id_request."'";
    		$licUpdate= Yii::app()->db->createCommand($SQL)->query();
         
            return $id_license;                          
    }



/*   
public function saveImage($file,$target_dir,$tn){
    $File = strtolower($tn.basename($file["name"]));
    $target_file = $target_dir . $File;
    
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    // Check if image file is a actual image or fake image
    if(isset($_POST["submit"])) {
        $check = getimagesize($file["tmp_name"]);
        if($check !== false) {
          //  echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            //echo "File is not an image.<br>";
            $uploadOk = 0;
        }
    }
    // Check if file already exists
    if (file_exists($target_file)) {
       // echo "Sorry, file already exists.";
       // $uploadOk = 0;
    }
    // Check file size
    if ($file["size"] > 500000) {
        //echo "Sorry, your file is too large.<br><br>";
        $uploadOk = 0;
    }
    // Allow certain file formats
    //var_dump($imageFileType);
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
        //echo "Sorry, only JPG, JPEG, PNG files are allowed.<br><br>";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        //echo "Sorry, your file was not uploaded.";
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($file["tmp_name"], $target_file)) {
         //   echo "The file ". basename( $file["name"]). " has been uploaded.";
        } else {
           // echo "Sorry, there was an error uploading your file.<br>";
        }
    }
    
    return basename($file["name"]);
}
    
  */  
}          
 
 
<?php

/**
 * This is the model class for table "answers".
 *
 * The followings are the available columns in table 'answers':
 * @property integer $id_respuesta
 * @property integer $id_pregunta
 * @property string $ruta_imagen
 * @property string $ruta_video
 * @property string $ruta_audio
 * @property string $texto
 * @property integer $es_verdadera
 *
 * The followings are the available model relations:
 * @property Questions $idPregunta
 * @property AnswersExamUser[] $answersExamUsers
 */
class Answers extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'answers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_pregunta, estado', 'required'),
			array('id_pregunta, es_verdadera', 'numerical', 'integerOnly'=>true),
			array('ruta_imagen, ruta_video, ruta_audio', 'length', 'max'=>200),
			array('texto, estado', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_respuesta, id_pregunta, ruta_imagen, ruta_video, ruta_audio, texto, es_verdadera', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idPregunta' => array(self::BELONGS_TO, 'Questions', 'id_pregunta'),
			'answersExamUsers' => array(self::HAS_MANY, 'AnswersExamUser', 'id_respuesta'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_respuesta' => 'Id ',
			'id_pregunta' => 'Question',
			'ruta_imagen' => 'Image',
			'ruta_video' => 'Video',
			'ruta_audio' => 'Audio',
			'estado' => 'Status',
			'texto' => 'Text',
			'es_verdadera' => 'Correct',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_respuesta',$this->id_respuesta);
		$criteria->compare('id_pregunta',$this->id_pregunta);
		$criteria->compare('ruta_imagen',$this->ruta_imagen,true);
		$criteria->compare('ruta_video',$this->ruta_video,true);
		$criteria->compare('ruta_audio',$this->ruta_audio,true);
		$criteria->compare('estado',$this->estado,true);
		$criteria->compare('texto',$this->texto,true);
		$criteria->compare('es_verdadera',$this->es_verdadera);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'pagination'=>array('pageSize'=>50,),

		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Answers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

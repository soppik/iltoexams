<?php
$baseUrl = Yii::app()->theme->baseUrl; 
?>
<div class="summary plugin_usuarios">
    <div class="titulo_plugin">My Info</div>
        <ul>
              <li>
              <span class="summary-icon">
                      <img src="<?php echo $baseUrl ;?>/img/group.png" width="36" height="36">
              </span>
              <span class="summary-title"> User: <?php echo Yii::app()->user->getState('usuario');?> </span>
              <span class="summary-number"><?php echo Yii::app()->user->getState('nombres_apellidos');?></span>
          </li>
          <li>
              <span class="summary-icon">
                      <img src="<?php echo $baseUrl ;?>/img/page_white_edit.png" width="36" height="36" alt="Open Invoices">
              </span>
              <span class="summary-title"> Profile</span>
              <span class="summary-number"><?php echo Yii::app()->user->getState('nombre_perfil');?></span>
          </li>
        </ul>
</div>

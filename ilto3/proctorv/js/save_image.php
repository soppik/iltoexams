<?php

$servername = "localhost";
$username = "iltoexam_newvers";
$password = "Ilto.2015";
try {
    $conn = new PDO("mysql:host=$servername;dbname=iltoexam_newversion", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }
    
$folder = "/ilto3/images/proctorv_users/";
$destinationFolder = $_SERVER['DOCUMENT_ROOT'] . $folder; // you may need to adjust to your server configuration
$maxFileSize = 2 * 1024 * 1024;

// Get the posted data
$postdata = file_get_contents("php://input");

if (!isset($postdata) || empty($postdata))
    exit(json_encode(["success" => false, "reason" => "Not a post data"]));

// Extract the data
$request = json_decode($postdata);

// Validate
if (trim($request->data) === "")
    exit(json_encode(["success" => false, "reason" => "Not a post data"]));


$file = $request->data;
$id_taker = $request->id_request;
$id_request = $request->id_request;
$flag = $request->flag;

// getimagesize is used to get the file extension
// Only png / jpg mime types are allowed
$size = getimagesize($file);

$ext = $size['mime'];
if ($ext == 'image/jpeg')
    $ext = '.jpg';
elseif ($ext == 'image/png')
    $ext = '.png';
else
    exit(json_encode(['success' => false, 'reason' => 'only png and jpg mime types are allowed']));

// Prevent the upload of large files
if (strlen(base64_decode($file)) > $maxFileSize)
    exit(json_encode(['success' => false, 'reason' => "file size exceeds {$maxFileSize} Mb"]));

// Remove inline tags and spaces
$img = str_replace('data:image/png;base64,', '', $file);
$img = str_replace('data:image/jpeg;base64,', '', $img);
$img = str_replace(' ', '+', $img);

// Read base64 encoded string as an image
$img = base64_decode($img);

// Give the image a unique name. Don't forget the extension
$filename = $id_taker."-". time() . $ext;


// The path to the newly created file inside the upload folder
$destinationPath = "$destinationFolder$filename";

// Create the file or return false
$success = file_put_contents($destinationPath, $img);



// flag to set the file type to save in bd
if($flag == 0){
    // personal photo
    $sql = "UPDATE `proctorv_request` SET `photo_route`= '".$filename."', `photo_step`=0 WHERE id_proctorv_request = '".$id_request."' ";
    if ($conn->query($sql) === TRUE) {
    
    } else {
    echo "Error query: " . $sql . "<br>" . $conn->error;
    }
    
}else{
    // card photo
    $sql = "UPDATE `proctorv_request` SET `card_route`= '".$filename."', `card_step`=0 WHERE id_proctorv_request = '".$id_request."' ";
}

if ($conn->query($sql) === TRUE) {
    
} else {
    echo "Error query: " . $sql . "<br>" . $conn->error;
}

$conn->close();

if (!$success)
    exit(json_encode(['success' => false, 'reason' => 'the server failed in creating the image']));

// Inform the browser about the path to the newly created image
exit(json_encode(['success' => true, 'path' => "$folder$filename"])); 
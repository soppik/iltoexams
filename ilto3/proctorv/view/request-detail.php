<?php

    //database connection
	if(str_replace('www.', '', $_SERVER['SERVER_NAME']) == 'iltoexams.com'){
            	    $conn = @mysqli_connect('localhost', 'iltoexam_newvers', 'Ilto.2015', 'iltoexam_newversion');      
         }else{
             	    $conn = @mysqli_connect('localhost', 'thetec_newvers', 'Ilto.2015', 'thetec_iltoexam_newversion');
         }
    
    if (!$conn) {
	    echo "Error: " . mysqli_connect_error();
		exit();
	}
	
	$flag_complete = 1;
	$id_request = base64_decode($_GET["id"]);
	
	$SQL = "SELECT id_proctorv_request, date_request, date_allocate, step_request, id_user, id_license, questions_step, photo_step, photo_route, card_step, card_route, status_request, identity_verified, nombres, apellidos FROM `proctorv_request` INNER JOIN users_c ON proctorv_request.id_user = users_c.id_usuario_c WHERE id_proctorv_request = '".$id_request."'";
    $request_query = mysqli_query($conn, $SQL);
    $proctorv_request = mysqli_fetch_assoc($request_query);
    
	//checking if there is a tecs2go payment
	$SQL = "SELECT * FROM `proctorv_user_payment` WHERE id_request = '".$id_request."' AND sell_reference LIKE '%TECS2GO%' AND status = 4 ";
	$payment = mysqli_query($conn, $SQL);
	$row_payment=mysqli_fetch_assoc($payment);

	$payU_flag = 0;
	if(isset($row_payment["id_proctorv_user_payment"])){
		$payU_flag = 1;
	}

	//checking if there is a tecs2go payment
	$SQL = "SELECT * FROM `proctorv_user_payment` WHERE id_request = '".$id_request."' AND payment_number IS NOT NULL ";
	$university_payment = mysqli_query($conn, $SQL);
	$row_university_payment=mysqli_fetch_assoc($university_payment);
	
	$payUniversity_flag = 0;
	if(isset($row_university_payment["id_proctorv_user_payment"])){
		$payUniversity_flag = 1;
	}


    //checking if the taker has selected SPEAKING DATE
	$SQL = "SELECT * FROM `proctorv_user_dates` WHERE id_request = '".$id_request."' AND speaking_date = '1' ";
	$speaking = mysqli_query($conn, $SQL);
	$row_speaking=mysqli_fetch_assoc($speaking);
	
	//checking if the taker has selected SPEAKING DATE
	$SQL = "SELECT * FROM `proctorv_user_dates` WHERE id_request = '".$id_request."' AND speaking_date = '0' ";
	$online = mysqli_query($conn, $SQL);
	$row_online=mysqli_fetch_assoc($online);
	
	if($proctorv_request["questions_step"]==0){
	    header("Location: /ilto3/proctorv/view/recovery-pass.php?id=".base64_encode($id_request)."");

	}else{
	    if($proctorv_request["photo_step"]==2){
	        header("Location: /ilto3/proctorv/view/take-photo.php?id=".base64_encode($id_request)."");
        }else{
            if($proctorv_request["card_step"]==2){
    	        header("Location: /ilto3/proctorv/view/take-card-photo.php?id=".base64_encode($id_request)."");
            }else{
                //check if user is already register and check if there is not photo_route saved
            	if($payU_flag == 0 && $payUniversity_flag == 0){
            	    
                	header("Location: /ilto3/proctorv/view/tecs2go-payment.php?id=".$id_request."");
            	}else{
            	   
            	   if(!isset($row_speaking["id_request"])){
                	header("Location: /ilto3/proctorv/view/date-tecs2go.php?id=".base64_encode($id_request)."");
            	    }else{
            	        if(!isset($row_online["id_request"])){
                    	 header("Location: /ilto3/proctorv/view/date-online-tecs2go.php?id=".base64_encode($id_request)."");
                	    }    
            	    }
            	    
            	    
            	    
            	}
            	
            }
        }
	}

	
	
	$id_taker = $proctorv_request["id_user"];
	/*
	    SESSION MANAGER
	*/
	session_start();

	//validate session identity with id user
	if($id_taker != $_SESSION["id_user"]){
	   session_unset();
	   session_destroy();
	   $newURL = '/ilto3/proctorv/view/login.php?id=5';
       header('Location: '.$newURL);   
       die(); 
	}
	
	//validate session lifetime 30 mins
    if ($_SESSION['loggedin_time'] + 30 * 60 < time()) {
       include '../controller/sessionController.php';
    } 
    
	
	
    
    $date_placed_request = explode(" ", $proctorv_request["date_request"]);
    
    //SPEAKING DATE
    $SQL = "SELECT * FROM proctorv_user_dates  WHERE id_request = '".$id_request."' AND speaking_date = 1";
    $date_speaking_query = mysqli_query($conn, $SQL);
    $date_speaking_exist = mysqli_fetch_assoc($date_speaking_query);

    
    //ONLINE SECTION DATE
    $SQL = "SELECT * FROM proctorv_user_dates  WHERE id_request = '".$id_request."' AND speaking_date = 0";
    $date_online_query = mysqli_query($conn, $SQL);
    $date_online_exist = mysqli_fetch_assoc($date_online_query);

//STATUS SWITCH
switch ($proctorv_request["status_request"]) {
    case 0:
        $status = '<img src="/ilto3/images/iconos/alert-warning.png" title="Pending for check it!" width="32"> <span class="red_text">PENDIENTE</span>';
        $available_at = '<p>Esperando Activación</p>';
        break;
    case 1:
        $status = '<img src="/ilto3/images/iconos/alert-warning.png" title="Pending for check it!" width="32"> <span class="red_text">PENDING</span>
        <p>Waiting Identity Validation</p>';
        $available_at = '<a href="/ilto3/proctorv/view/identity-validation.php?id='.base64_encode($id_request).'" class="btn btn-warning" id="allocate">Verificar Identidad</a><br/><br/>';
        break;
    case 2:
        $status = '<img src="/ilto3/images/iconos/verified.png" title="Pending for check it!" width="32"> <span class="green_text">Listo para tomar Conversacional/span>';
        ;
    break;
    case 3:
       $status = '<img src="/ilto3/images/iconos/verified.png" title="Pending for check it!" width="32"> <span class="green_text">Listo para tomar ONLINE SECTION</span>';
       $available_at = '<p>Listo para tomar Online Section.</p>';
    break;
    case 4:
       $status = '<img src="/ilto3/images/iconos/verified.png" title="Ok" width="32"> <span class="green_text">OK</span>';
       $available_at = '<p>TECS2GO Finalizado.</p>';
    break;
    case 5:
       $status = '<img src="/ilto3/images/iconos/alert-danger.png" title="Pending for check it!" width="32"> <span class="red_text">ANULADO</span>';
       $available_at = '<p>Por favor contacte al Administrador.</p>';
    break;
    default:
        // code...
        break;
}

// Presentation date for be able the IDENTITY VERIFY


//IDENTITY SWITCH
switch ($proctorv_request["identity_verified"]) {
    case 0:
        $identity_step = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Pendiente!" width="32"> 
        <span class="yellow_text">PENDIENTE</span><br/>
        Validación de Identidad</h4><br>
        <input type="hidden" id="sr" value="'.$proctorv_request["status_request"].'" />
        '.$available_at.'<br>
        <small style="color:red;"><b>Importante:</b> La validación de identidad se realizará el día del examen Conversacional.</small>';
        break;
    case 1:
        $identity_step = '<h4><img src="/ilto3/images/iconos/verified.png" title="OK!" width="32">
        <span class="green_text">OK</h4> 
        Validación de Identidad</h4><br>';
        break;
    case 2:
        $identity_step = '<h4><img src="/ilto3/images/iconos/alert-danger.png" title="No completado!" width="32">
        <span class="red_text">NO APROBADO</h4>
        Validación de Identidad</h4><br>
        Validation not sucessful. Contact tecs2go@iltoexams.com</h4>';
        break;
    default:
        // code...
        break;
}


//profile picture
switch ($proctorv_request["photo_step"]) {
    case 0:
        $photo_step = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Not OK!" width="32"> 
                            <span class="yellow_text">VALIDACIÓN PENDIENTE</span><h4>
                            <h4>3. Foto del Rostro</h4><br/>';
                         if($proctorv_request["photo_route"]!=NULL){
                                $photo_step .= '<img class="requirement-picture" src="/ilto3/images/proctorv_users/'.$proctorv_request['photo_route'].'" title="'.$proctorv_request['photo_route'].'"> ';
                            }else{
                                $photo_step .= '<img class="requirement-picture" src="/ilto3/images/no-profile-photo-ilto.jpg" title="No profile picture found"> ';
                            }
        break;
    case 1:
        $photo_step = '<h4><img src="/ilto3/images/iconos/verified.png" title="OK!" width="32">
                        <span class="green_text">OK</span></h4>
                        <h4>3. Foto del Rostro</h4><br/>';
                            if($proctorv_request["photo_step"]!=NULL){
                               $photo_step .= '<img class="requirement-picture"  src="/ilto3/images/proctorv_users/'.$proctorv_request['photo_route'].'" title="'.$proctorv_request['photo_route'].'"> ';
                            }else{
                                $photo_step .= '<img class="requirement-picture" src="/ilto3/images/no-profile-photo-ilto.jpg" title="No profile picture found"> ';
                            }
        break;
     case 2:
        $photo_step = '<h4><img src="/ilto3/images/iconos/alert-danger.png" title="Alert!" width="32">
                <span class="red_text">FOTO PENDIENTE</span><h4>
                 <h4>3. Foto del Rostro</h4><br/>
                 <a href="/ilto3/proctorv/view/take-photo.php?id='.base64_encode($id_request).'" class="btn btn-warning" id="allocate">Tomar fotografía</a><br/><br/>
                  <img class="requirement-picture" src="/ilto3/images/no-profile-photo-ilto.jpg" title="No profile picture found"> ';
        break;
    case 3:
        $photo_step = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Completed!" width="32">
                        <span class="yellow_text">FOTO ACEPTADA, PENDIENTE VERIFICACIÓN DEL ROSTRO.</span><h4>
                        <h4>3. Foto del Rostro</h4><br/>
                        <img class="requirement-picture" src="/ilto3/images/proctorv_users/'.$proctorv_request['photo_route'].'" title="taker Foto del Rostro">
                        <small style="color:red;"><b>Importante:</b> La verificación del rostro se realizará el día del examen Conversacional.</small>';
        break;
        case 4:
        $photo_step = '<h4><img src="/ilto3/images/iconos/alert-danger.png" title="Completed!" width="32">
                        <span class="red_text">FALLÓ VERIFICACIÓN DEL ROSTRO.</span></h4>
                        <h4>3. Foto del Rostro<br/></h4>
                        <p>La persona en la cámara no coincide con la fotografía del Rostro, por favor contacte a un Administrador de ILTO.</p>
                        <img class="requirement-picture" src="/ilto3/images/proctorv_users/'.$proctorv_request['photo_route'].'" title="taker Foto del Rostro"> ';
        break;
    default:
        // code...
        break;
}

//ID SWITCH
switch ($proctorv_request["card_step"]) {
    case 0:
        $card_step = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Not OK!" width="32"> 
                            <span class="yellow_text">VALIDACIÓN PENDIENTE</span><h4>
                            <h4>4. Foto Documento de Identificación</h4><br/>';
                         if($proctorv_request["card_route"]!=NULL){
                                $card_step .= '<img class="requirement-picture" src="/ilto3/images/proctorv_users/'.$proctorv_request['card_route'].'" title="'.$proctorv_request['card_route'].'"> ';
                            }else{
                                $card_step .= '<img class="requirement-picture" src="/ilto3/images/no-id-photo-ilto.jpg" title="No profile picture found"> ';
                            }
        break;
    case 1:
        $card_step = '<h4><img src="/ilto3/images/iconos/verified.png" title="OK!" width="32">
                        <span class="green_text">OK</span></h4>
                        <h4>4. Foto Documento de Identificación</h4><br/><br/>';
                            if($proctorv_request["card_route"]!=NULL){
                               $card_step .= '<img  class="requirement-picture" src="/ilto3/images/proctorv_users/'.$proctorv_request['card_route'].'" title="'.$proctorv_request['card_route'].'"> ';
                            }else{
                                $card_step .= '<img class="requirement-picture" src="/ilto3/images/no-id-photo-ilto.jpg" title="No profile picture found"> ';
                            }
        break;
     case 2:
        $card_step = '<h4><img src="/ilto3/images/iconos/alert-danger.png" title="Alert!" width="32">
                <span class="red_text">FOTO PENDIENTE</span><h4>
                <h4>4. Foto Documento de Identificación</h4><br/>
                <a href="/ilto3/proctorv/view/take-card-photo.php?id='.base64_encode($id_request).'" class="btn btn-warning" id="allocate">Tomar Fotografía</a><br/><br/>
                <img class="requirement-picture" src="/ilto3/images/no-id-photo-ilto.jpg" title="No profile picture found"> ';
        break;
    
    default:
        // code...
        break;
}

//suggested dates

//DATE SPEAKING
if(count($date_speaking_exist)>0){
    
    $date_speaking = '<h4><img src="/ilto3/images/iconos/verified.png" title="OK!" width="32">
                        <span class="green_text">OK</span></h4>
                        <p><b>Fecha de Speaking</b></p>
                        <h5>'.$date_speaking_exist['date_picked'].'</h5>';

}else{
    $date_speaking = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Pendiente por seleccionar!" width="32"> 
                            <span class="yellow_text">PENDIENTE</span><h4>
                            <h4>5. Fechas de Exámenes</h5><br/><br/><a href="/ilto3/proctorv/view/date-tecs2go.php?id='.base64_encode($id_request).'" class="btn btn-warning" id="allocate">Seleccionar fechas</a><br/><br/>';
}

//DATE SPEAKING
if(count($date_speaking_exist)>0){
    $date_speaking = explode(" ", $date_speaking_exist['date_picked']);
    if($date_speaking[1]=="14:00:00"){
        $time_zone_speaking = "Hora: Entre las 2:00 PM y las 6:00 PM";
    }else{
        $time_zone_speaking = "Hora: Entre las 8:00 AM y las 12:00 M";
    }
    
    
    $date_speaking = '<h4><img src="/ilto3/images/iconos/verified.png" title="OK!" width="32">
                        <span class="green_text">OK</span></h4>
                        <p><b>Selección Fecha y Hora Examen Conversacional</b><br>
                        <b>Duración:</b> 15 minutos apróximadamente.</p>
                        <h5>Fecha: '.$date_speaking[0].'<br> '.$time_zone_speaking.'</h5>';

}




//DATE ONLINE SECTIONS
if(count($date_online_exist)>0){
    $date_split = explode(" ",$date_online_exist['date_picked']);
    $date_online_exist = '<h4><img src="/ilto3/images/iconos/verified.png" title="OK!" width="32">
                        <span class="green_text">OK</span></h4>
                        <p><b>Selección Fecha y Hora secciones ONLINE</b><br>
                        <b>Duración:</b> 2 horas apróximadamente.</p>
                        <h5>Fecha: '.$date_split[0].'<br>HORA EXACTA: '.$date_split[1].'</h5>';

}

//license content
if($proctorv_request["id_license"] == NULL){
    $license_content = '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Pending!" width="32"><span class="yellow_text"> PENDING</span></h4>
    <h4>License Allocation</h4><br><br>';
}else{
    //DATES FOR TECS
    $SQL = "SELECT * FROM licenses_user  WHERE id_licencias_usuario = '".$proctorv_request["id_license"]."'";
    $license_query = mysqli_query($conn, $SQL);
    $license = mysqli_fetch_assoc($license_query);
    switch($license["estado"]){
        case 'A':
            $license_content ='<h4><img src="/ilto3/images/iconos/verified.png" title="License Allocated!" width="32"><span class="green_text"> OK</span></h4>
            <h4>License Allocation</h4><br>
            <p>Status: Pending</p>
            ';
        break;
        case 'F':
            $license_content ='<h4><img src="/ilto3/images/iconos/verified.png" title="License Allocated!" width="32"><span class="green_text"> OK</span></h4>
            <h4>License Allocation</h4><br>
            <p>Status: Completed</p>
            <p>Score: '.$license["calificacion"].'  CEFR: '.$license["nivel"].'</p>
            ';
        break;
         case 'B':
            $license_content ='<h4><img src="/ilto3/images/iconos/verified.png" title="License Allocated!" width="32"><span class="green_text"> OK</span></h4>
            <h4>License Allocation</h4><br>
            <p>Status: Revoked</p>
            ';
        break;
    }
    
}

    
?>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<!DOCTYPE html>
<html>
    <head>
		<title>REGISTRO TECS2GO</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
		<link rel="stylesheet" href="../css/style.css" rel="stylesheet" type="text/css">
	</head>
	<style>
	    .widget-body {
    
    padding: 1px !important;
    text-align: center;
    
}


.widget-header {
    font-weight:800;
    text-align: center;
    
}

input[type=checkbox], input[type=radio] {
    opacity: 1 !important;
    position:relative;
    left:0px;
}

.red_text {
    color: #d73d32 !important;
}
.green_text {
    color: #53a93f !important;
}

.yellow_text {
    color: #efb71a !important;
}

small {
    font-size:12px;
}

.row p {
    font-size:14px;
}

.requirement-picture{
    width:200px;
}
	</style>
	<body>
	
		<div class="loginbox-social" style="text-align:center;">
                    <div class="social-buttons">
                        <img src="/ilto3/images/logo_ILTO.png" alt="ILTO EXAMS">
                    </div>
                </div>
        
		<div class="container" style="text-aling:center;">
			
			<div class="widget">
                <div class="widget-body">
                    <div class="row">
                        <div class="col-lg-6 col-sm-6 col-xs-6" style="text-align:left;">
                           <h3><?php echo utf8_encode($proctorv_request["nombres"])." ".utf8_encode($proctorv_request["apellidos"])?> <a href="../controller/logoutController.php" class="btn btn-danger">Logout</a></h3>  
                           <h3>REGISTRO No.<?php echo $proctorv_request["id_proctorv_request"]; ?> TECS2GO </h3>
                           <p><b>Fecha de Registro: </b><?php echo $date_placed_request[0]; ?>
                           <br>Completa la información requerida: 
                        </div>
                        
                        <div class="col-lg-6 col-sm-6 col-xs-6">
                           
                        </div>
                    </div>
                    <hr> 
                    <div class="row">
                        <div class="col-lg-3 col-sm-3 col-xs-3">
                            <h4><img src="/ilto3/images/iconos/verified.png" title="Pending!" width="32"><span class="green_text"> OK</span></h4>
                                    <b><h4>1. Registro TECS2GO.</h4></b><br><br>
                                 <?php
                        if($proctorv_request["questions_step"] == 0){
                                    echo '<h4><img src="/ilto3/images/iconos/alert-warning.png" title="Pending!" width="32"><span class="red_text"> PENDIENTE POR COMPLETAR/span></h4>
                                    <b><h4>2. Validación de Identidad</h4></b><br><br>
                                    <a href="/ilto3/proctorv/view/recovery-pass.php?id='.base64_encode($id_request).'" class="btn btn-warning" id="allocate">View</a>';
                                }else{
                                   echo '<h4><img src="/ilto3/images/iconos/verified.png" title="License Allocated!" width="32"><span class="green_text"> OK</span></h4>
                                    <b><h4>2. Validación de Identidad</h4></b><br><br>';
                                }
                             ?>
                             <p>Paga en línea o registra tu pago de manera segura y rápida usando PayU.</p>
                             <a class="btn btn-success" href="/ilto3/proctorv/view/tecs2go-payment.php?id=<?php echo $id_request; ?>">Módulo de Pagos</a>
                            <img src="/ilto3/proctorv/img/nuevo.png" title="License Allocated!" width="32">
                        </div>
                        <div class="col-lg-3 col-sm-3 col-xs-3">
                            <?php echo $photo_step; ?>
                        </div>
                        <div class="col-lg-3 col-sm-3 col-xs-3">
                             <?php echo $card_step; ?> 
                        </div>
                        <div class="col-lg-3 col-sm-3 col-xs-3">
                             <?php echo $date_speaking."<br>"; 
                             echo $date_online_exist."<br>";
                             
                             echo $identity_step;
                              ?> 
                        </div>
                        
                    </div>
                    <hr>
                    
                </div>
                
                </div>
    </div>
<script type="text/javascript">
    // Use a named immediately-invoked function expression.
    var id_request = <?php echo $id_request; ?>;
    var sr = $("#sr").val();
    
    
    setInterval(function(){ 
        console.log(sr);
        if(sr == 0)    {
            $.ajax({
    			url : "/ilto3/index.php?r=Api/takerCheckIdentityVal",
    			type: "POST",
    			dataType: "text",
    			data: {
    			    request: id_request
    			},
    			success: function(data) {
    			    document.getElementById("identity_step").innerHTML = data;
    			}
    		});
        }
    
    }, 5000);
    
    /*(function worker() {
        
        $.get('ajax/test.html', function(data) {
            // Now that we've completed the request schedule the next one.
            $('.result').html(data);
            setTimeout(worker, 5000);
        });
    })();
    */
</script>
			
			<!-- Bootstrap core JavaScript
			================================================== -->
			<!-- Placed at the end of the document so the pages load faster -->
		    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
			<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
		</div>
	</body>
<html>



<?php
	//close connection
    $conn->close();

?>
<?php

   		//database connection
	if(str_replace('www.', '', $_SERVER['SERVER_NAME']) == 'iltoexams.com'){
            	    $conn = @mysqli_connect('localhost', 'iltoexam_newvers', 'Ilto.2015', 'iltoexam_newversion');      
         }else{
             	    $conn = @mysqli_connect('localhost', 'thetec_newvers', 'Ilto.2015', 'thetec_iltoexam_newversion');
         }
	
	if (!$conn) {
	    echo "Error: " . mysqli_connect_error();
		exit();
	}

	
	$id_taker = base64_decode($_GET["id"]);
	
	/*
	    SESSION MANAGER
	*/
	session_start();
	
	//validate session identity with id user
	if($id_taker != $_SESSION["id_user"]){
	   session_unset();
	   session_destroy();
	   $newURL = '/ilto3/proctorv/view/login.php?id=5';
       header('Location: '.$newURL);   
       die(); 
	}
	
	//validate session lifetime 30 mins
    if ($_SESSION['loggedin_time'] + 30 * 60 < time()) {
       include '../controller/sessionController.php';
    } 
    
    //END SESION MANAGER
	
	$flag_complete = 1;
	
	$SQL = "SELECT id_proctorv_request, date_request, id_user, status_request FROM `proctorv_request`  WHERE id_user = '".$id_taker."' ORDER BY id_proctorv_request DESC";
	$request_query = mysqli_query($conn, $SQL);

	
	$SQL ="SELECT nombres, apellidos, estado FROM users_c WHERE id_usuario_c = '".$id_taker."' ";
	$user_query = mysqli_query($conn, $SQL);
    $user = mysqli_fetch_assoc($user_query);
    
    
?>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<!DOCTYPE html>
<html>
    <head>
		<title>PROCTORV TECS REGISTER</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
		<link rel="stylesheet" href="../css/style.css" rel="stylesheet" type="text/css">
	</head>
	
	<body>
	
		<div class="loginbox-social" style="text-align:center;">
                    <div class="social-buttons">
                        <img src="/ilto3/images/logo_ILTO.png" alt="ILTO EXAMS">
                    </div>
                </div>
		<div class="container" >
		<div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="widget">
                                <div class="widget-header ">
                                    <div class="col-lg-8 col-sm-8 col-xs-8">
                                        <h3 style=""><span>REGISTRO TECS2GO</span></h3>
                                        <h3><span><?php echo utf8_encode($user["nombres"])." ".utf8_encode($user["apellidos"]); ?></span> <a href="../controller/logoutController.php" class="btn btn-danger">Logout</a></h3>
                                    </div>
                                    <div class="col-lg-4 col-sm-4 col-xs-4" style="text-align:right;">
                                       <?php
                                       if($user["estado"] == 'A'){
                                            //if the user is already register we have to know if there is a PROCTORV request from this user
                                            $SQL = "SELECT * FROM `proctorv_request` INNER JOIN users_c ON proctorv_request.id_user = users_c.id_usuario_c WHERE id_user = $id_taker AND status_request != 4 AND estado = 'A' ";
                                            //query
                                            $request = mysqli_query($conn, $SQL);
                                            $row=mysqli_fetch_assoc($request);

                                            if(!isset($row)){
                                                ?>
                                                  <br>  <button class="btn btn-warning"  id="newTecs2goRequest">Quiero tomar el examen TECS Nuevamente</button>
                                         
                                               <?php
                                            }
                                       }
                                       ?>
                                    </div>
                                </div>
                                <div class="widget-body">
                                    
                                    <table class="table table-striped table-hover table-bordered" id="editabledatatable">
                                        <thead>
                                            <tr role="row">
                                                <th>
                                                    NÚMERO DE REGISTRO
                                                </th>
                                                <th>
                                                    FECHA DE REGISTRO
                                                </th>
                                                <th>
                                                    ESTADO
                                                </th>
                                                <th>
                                                   DETALLES DEL REGISTRO 
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            while($request=mysqli_fetch_assoc($request_query)){
						                    ?>
                                            <tr role="row">
                                            	 <th>
                                                    <?php echo $request["id_proctorv_request"];
                                                    ; ?>
                                                </th>
                                                <th>
                                                    <?php $date = explode(" ",$request["date_request"]);
                                                    echo $date[0];
                                                    ; ?>
                                                </th>
                                                <th>
                                                    <?php
                                                   switch($request["status_request"]){
                                                    	case 0:
                                                            echo "VALIDACIONES PENDIENTES";
                                                            break;
                                                        case 1:
                                                            echo "LISTO PARA TOMAR EXAMEN SPEAKING";
                                                            break;
                                                        case 2:
                                                            echo "LISTO PARA TOMAR EXAMEN SPEAKING";
                                                        break;
                                                        case 3:
                                                            echo "LISTO PARA LAS SECCIONES EN LÍNEA";
                                                        break;
                                                        case 4:
                                                            echo "REALIZADO";
                                                        break;
                                                        case 5:
                                                            echo "ANULADO";
                                                            break;
                                                        default:
                                                            // code...
                                                            break;
                                                    }
                                                    ?>
                                                </th>
                                                <th>
                                                    <a href="/ilto3/proctorv/view/request-detail.php?id=<?php echo base64_encode($request["id_proctorv_request"]);?>" class="btn btn-success" id="allocate">Ingresar al Registro</a>
                                                </th>
                                                
                                            </tr>
    
                                             <?php
                                             }
                                             ?>
                                            
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>						<!-- Bootstrap core JavaScript
				    ================================================== -->
					<!-- Placed at the end of the document so the pages load faster -->
					<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
				<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
		</div>
	</body>
<html>
<script>
    $('#newTecs2goRequest').click(function(){
       var confirmation = confirm("Deseas solicitar una nueva prueba TECS2GO?");
       var id_taker = <?php echo $id_taker; ?>;
        
       if(confirmation == true){
          $.ajax({
              url : "/ilto3/index.php?r=Api/NewTecs2goRequest",
              type: "POST",
              dataType: "text",
              data: {
                  id_taker: id_taker
              },
              success: function(data){
                location.reload();
              }
          })
       }
        
    });

</script>
<?php
	//close connection
    $conn->close();

?>
<?php
	if(str_replace('www.', '', $_SERVER['SERVER_NAME']) == 'iltoexams.com'){
            	    $conn = @mysqli_connect('localhost', 'iltoexam_newvers', 'Ilto.2015', 'iltoexam_newversion');      
         }else{
             	    $conn = @mysqli_connect('localhost', 'thetec_newvers', 'Ilto.2015', 'thetec_iltoexam_newversion');
         }
	
	if (!$conn) {
	    echo "Error: " . mysqli_connect_error();
		exit();
	}
	
	$id_request = base64_decode($_GET["id"]);
	$msg = $_GET["msg"];
	
	$SQL = "SELECT date_request, date_allocate, step_request, id_user, id_license, questions_step, photo_step, photo_route, card_step, card_route, status_request, identity_verified, nombres, apellidos, telefono FROM `proctorv_request` INNER JOIN users_c ON proctorv_request.id_user = users_c.id_usuario_c WHERE id_proctorv_request = '".$id_request."'";
    $request_query = mysqli_query($conn, $SQL);
    $proctorv_request = mysqli_fetch_assoc($request_query);
	
	 //checking if the user has been placed a payment 
	$SQL = "SELECT * FROM `proctorv_user_oayment` WHERE id_request = '".$id_request."' ";
	$payment = mysqli_query($conn, $SQL);
	$row_payment=mysqli_fetch_assoc($payment);
	
	
	//checking if there is a tecs2go payment
	$SQL = "SELECT * FROM `proctorv_user_payment` WHERE id_request = '".$id_request."' AND sell_reference LIKE '%TECS2GO%' AND status = 4 ";
	$payment = mysqli_query($conn, $SQL);
	$row_payment=mysqli_fetch_assoc($payment);

	$payU_flag = 0;
	if(isset($row_payment["id_proctorv_user_payment"])){
		$payU_flag = 1;
	}

	//checking if there is a tecs2go payment
	$SQL = "SELECT * FROM `proctorv_user_payment` WHERE id_request = '".$id_request."' AND payment_number IS NOT NULL ";
	$university_payment = mysqli_query($conn, $SQL);
	$row_university_payment=mysqli_fetch_assoc($university_payment);
	
	$payUniversity_flag = 0;
	if(isset($row_university_payment["id_proctorv_user_payment"])){
		$payUniversity_flag = 1;
	}
	
	//check if user is already register and check if there is not photo_route saved
	if($payU_flag == 0 && $payUniversity_flag == 0){
    	header("Location: /ilto3/proctorv/view/tecs2go-payment.php?id=".$id_request."");
     }
	
	$id_taker = $proctorv_request["id_user"];
	$phone = $proctorv_request["telefono"];
		/*
	    SESSION MANAGER
	*/
	session_start();
	
	//validate session identity with id user
	if($id_taker != $_SESSION["id_user"]){
	   session_unset();
	   session_destroy();
	   $newURL = '/ilto3/proctorv/view/login.php?id=5';
       header('Location: '.$newURL);   
       die(); 
	}
	
	//validate session lifetime 30 mins
    if ($_SESSION['loggedin_time'] + 30 * 60 < time()) {
       include '../controller/sessionController.php';
    } 
	
	//flag to show or not info
	$flag_complete = 0;
	
	//checking if the taker has ONLINE SECTIONS
	$SQL = "SELECT * FROM `proctorv_user_dates` WHERE id_request = '".$id_request."' AND speaking_date ='0' ";
	$taker = mysqli_query($conn, $SQL);
	$row=mysqli_fetch_assoc($taker);
	
	//GET THE SPEAKING DATE TO ENABLE ONLINE DATES
	$SQL = "SELECT date_picked FROM `proctorv_user_dates` WHERE id_request = '".$id_request."' AND speaking_date ='1' ";
	$speaking = mysqli_query($conn, $SQL);
	$speaking_date = mysqli_fetch_assoc($speaking);
	
	//get speaking date for calculate online dates available
	$speaking_date_exploded = explode(" ", $speaking_date["date_picked"]);
	$speaking_d = $speaking_date_exploded[0];
	$today = date('Y-m-d');
	
	if($speaking_d < $today){
		$date_for_online = $today;
	}else {
		$date_for_online = $speaking_d;
	}
	
	$SQL = "SELECT DATE_ADD('".$date_for_online."', INTERVAL 3 DAY) AS online;";
	$online = mysqli_query($conn, $SQL);
	$online_date = mysqli_fetch_assoc($online);
	
	$online_date_exploded = explode(" ", $online_date["online"]);
	$online_d = $online_date_exploded[0];
	
		
	//check if user is already register and check if there is not photo_route saved
	if(isset($row["id_request"])){
		$flag_complete = 1;
	}	
	
	//checking speaking dates available
	$SQL = "SELECT * FROM `proctorv_user_dates` WHERE id_request = '".$id_request."' ";
	$taker = mysqli_query($conn, $SQL);
	$row=mysqli_fetch_assoc($taker);
	
		
	//get client to get programs
	$SQL = "SELECT * from users_c WHERE id_usuario_c = '".$id_taker."'";
    $id_client_query = mysqli_query($conn, $SQL);
    $client = mysqli_fetch_assoc($id_client_query);
    
    $id_client = $client["id_cliente"];

	//GET PROGRAMS BY CLIENT
	$SQL = "SELECT * from programs WHERE id_client = '".$id_client."' ORDER BY name ASC";
    $programs_query = mysqli_query($conn, $SQL);
    $programs = mysqli_fetch_assoc($programs_query);
    
    
	
?>


<!DOCTYPE html>
<html>
    <head>
		<title>TECS2GO REGISTRATION</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
		<link rel="stylesheet" href="../css/style.css" rel="stylesheet" type="text/css">
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
		<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
		<!------ Include the above in your HEAD tag ---------->
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		 
	</script>
	</head>
	<style>
				/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}

select.input-lg {
    height: 46px !important;
}



.input-lg {
	width:60%;
	cursor:pointer !important;
}
	</style>
	<body>
	
		<div class="loginbox-social" style="text-align:center;">
                    <div class="social-buttons">
                        <img src="/ilto3/images/logo_ILTO.png" alt="ILTO EXAMS">
                    </div>
                </div>
		<div class="container">
			<div class="row">
                <div class="col-lg-6 col-sm-6 col-xs-6" style="text-align:left;">
                <h3><?php echo utf8_encode($proctorv_request["nombres"])." ".utf8_encode($proctorv_request["apellidos"])?> <a href="../controller/logoutController.php" class="btn btn-danger">Logout</a></h3>  
                    <h3>Fecha de Examen</h3>
                </div>
                <div class="col-lg-6 col-sm-6 col-xs-6">
                </div>
            </div>
			<div id="exTab1" class="container">	
				<ul  class="nav nav-pills">
					<li>
				    	<a  href="#1a" class="isDisabled">1. Registro</a>
					</li>
					<li>
						<a href="#" class="isDisabled">2. Validación de Identidad</a>
					</li>
					<li>
						<a href="#" class="isDisabled" >3. Foto del Rostro</a>
					</li>
					<li href="#1a" >
						<a href="#" class="isDisabled">4. Foto Documento de Identificación</a>
					</li>
					<li href="#1a" >
						<a href="#" class="isDisabled" >5. Pago TECS2GO</a>
					</li>
					<li href="#1a"  class="active">
						<a href="#"  >6. Fecha de Examen</a>
					</li>
				</ul>
				<div class="tab-content clearfix">
					<div class="tab-pane active" id="1a">
						<div class="row">
							<div class="col-xs-4 col-md-4">
							</div>
							<div class="col-xs-4 col-md-4">
							</div>
							<div class="col-xs-4 col-md-4">
							</div>
						</div>
						<form role="form" method="POST" action="/ilto3/proctorv/controller/dateOnlineRegisterController.php">
							<input type="hidden" name="id_request" value="<?php echo $_GET["id"] ?>"/>
							<input type="hidden" name="id_taker" value="<?php echo $proctorv_request["id_user"] ?>"/>
				    	<div class="row" style="text-align:center;">
				    			<h2 style="text-align:center;">Selecciona Fecha y hora para Examen Secciones ONLINE (Escucha, Lectura, Uso del Idioma) </h2><br>	
							
						</div>
						
						<?php 
						if(isset($msg)){
						?>
							<div class="row" style="text-align:center;">
								<h4 class="btn btn-danger" style="cursor:default;">
									Debes seleccionar una franja horaria para poder continuar, inténtalo de nuevo.
	                        	</h4>
							</div>
						<?php
						}
						//if the user has already complete this step just SHOW A MESSAGE
						if($flag_complete == 1){
						?>
							<div class="row" style="text-align:center;">
								<h4><img src="/ilto3/images/iconos/verified.png" title="Completed!" width="32">
	                        	<span class="green_text">REALIZADO</span></h4><br><br>
	                        	
							</div>
							<a href="/ilto3/proctorv/view/request-detail.php?id=NzU=" class="btn btn-success" id="allocate">Ir al Resumen del Registro</a>
						<?php
						}else{
						?>	
						</div>	
						<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-6">
								<div class="form-group">
									<h3>2. Fecha y hora para Sección ONLINE (Escucha, Lectura, Uso del Idioma)</h3>
									<p>Por favor selecciona la fecha para realizar la Sección ONLINE (Escucha, Lectura, Uso del Idioma):<br>
									<b>Duración:</b> 2 horas apróximadamente.</p>
									<input type="hidden" id="speaking_date_selected" name="speaking_date_selected" value="<?php echo $online_d ?>" />
									<input type="hidden" id="speaking_emailing" name="speaking_emailing" value="<?php echo $speaking_date_exploded[1] ?>" />
									<input type="text" id="date" name="date" tabindex="1"  required  class="form-control input-lg" readonly data-toggle="tooltip" title="Clic para seleccionar fecha"><br><br>
									<p>Despues de escoger la fecha para realizar la sección ONLINE, selecciona la hora para presentar esta sección:</p>
									<div class="form-group" >
										<select id="time_zone_online" name="time_zone_online"  class="form-control input-lg">
											
										</select><br/><br/>
										<p>Te llegará un primer email para hacer una verificación del sistema y un segundo email con acceso al examen. </p>
									<label id="description"></label>
									</div>
									<button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal" id="register" disabled>
								 GUARDAR Y FINALIZAR
								</button></p>
								</div>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6">
								<p><h3 style="color:#d9534f;">IMPORTANTE:</h3>
					          			Recuerda que el sistema sólo te permite seleccionar fechas dentro de los próximos 30 días.<br><br>
					          			El Examen ONLINE (Reading, Listening, Language Use) se habilita 2 días después del examen de Conversación.
					          		</p>
				          	</div>
						</div>
							
							<?php
							}
							?>
							
					
						
						<!-- Modal -->
						<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						  <div class="modal-dialog" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <h5 class="modal-title" id="exampleModalLabel">¡Gracias por Registrarte!</h5>
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						      </div>
						      <div class="modal-body">
						       <p>
						       	<strong>IMPORTANTE:</strong>  Si no encuentras nuestro email, por favor revisa la bandeja de correo spam o no deseado.<br><br>

								La FPO - Fraud Prevention Office (Oficina de Prevencion de Fraude) te recuerda que cualquier intento de fraude antes, durante o despues del examen será detectado y tu examen será anulado. <br><br>
								
								<strong>Antes de hacer fraude, piensa en las consecuencias.  </strong>
						       	
						       </p>
						      </div>
						      <div class="modal-footer">
						        <button type="submit" name="register" id="register-form" class="btn btn-success btn-block btn-lg" tabindex="6" style="margin-top: 100px;">Entendido</button>
						      </div>
						    </div>
						  </div>
						</div>
<hr class="colorgraph">
					</div>
					</form>
						
						
						
					</div><!-- /.tab-pane1 -->
				</div>
			</div>
		</div>

			
			
			
			
			
			
	</body>
<html>
<script type="text/javascript">



$( document ).ready(function() {
	//obttenemos la fecha de hoy
	
	var speakingDate = $('#speaking_date_selected').val();
	
	var today = new Date(speakingDate+" 08:00:00");
    

	today = speakingDate + ' 00:00:00';
	
var array = ["2021-05-09","2021-05-16","2021-05-17","2021-05-23","2021-05-30","2021-06-06","2021-06-07","2021-06-13","2021-06-14","2021-06-20","2021-07-04","2021-07-05","2021-07-11","2021-07-18","2021-07-20","2021-07-25","2021-08-01","2021-08-03","2021-08-04","2021-08-05","2021-08-07","2021-08-08","2021-08-15","2021-08-16","2021-08-22","2021-08-28","2021-08-29","2021-09-05","2021-09-12","2021-09-19","2021-09-26",
"2021-10-03","2021-10-10","2021-10-17","2021-10-18","2021-10-24","2021-10-31","2021-11-01","2021-11-07","2021-11-14","2021-11-15","2021-11-21","2021-11-28","2021-12-05","2021-12-08","2021-12-12","2021-12-19","2021-12-24","2021-12-25","2021-12-26","2021-12-31","2022-01-01","2022-01-02","2022-01-03"];
	

		

	//instanciamos el datepicker
	$("#date").datepicker({
		changeYear: true,
		         changeMonth:true,
		         dateFormat: 'yy-mm-dd',
		         minDate: new Date(today),
		         maxDate: '+1M',
				 beforeShowDay: function(date){
			        var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
			        return [ array.indexOf(string) == -1 ]
			     },
			    
		         onSelect: function() {
		         	var newDate = $('#date').val();
		         	console.log(newDate)
					$('#register').prop("disabled", false);
					$.ajax({
		              url : "/ilto3/index.php?r=Api/VerifyOlineHours",
		              type: "POST",
		              dataType: "text",
		              data: {
		                  today: newDate
		              },
		              success: function(data){
		                  document.getElementById("time_zone_online").innerHTML = data;
		              }
		          })
		        }

	        })
	});
	
	$( "#time_zone_speaking" ).change(function() {
	   var payment_status = $('#time_zone_speaking').val();
		if(payment_status == "am"){
			$('#description').text("Recibirás un correo electrónico con la invitación a la vídeo llamada que se realizará entre las 8:00 AM y las 12:00 M.");
		}else{
			if(payment_status == "pm"){
				$('#description').text("Recibirás un correo electrónico con la invitación a la vídeo llamada que se realizará entre las 14:00 AM y las 18:00 PM.");
			}else{
				$('#description').text("");	
			}
			
		}
		
	});
	
	
/*	
										
*/
</script>
<?php
	//close connection
    $conn->close();

?>
<?php
		//database connection
	if(str_replace('www.', '', $_SERVER['SERVER_NAME']) == 'iltoexams.com'){
            	    $conn = @mysqli_connect('localhost', 'iltoexam_newvers', 'Ilto.2015', 'iltoexam_newversion');      
         }else{
             	    $conn = @mysqli_connect('localhost', 'thetec_newvers', 'Ilto.2015', 'thetec_iltoexam_newversion');
         }
	
	if (!$conn) {
	    echo "Error: " . mysqli_connect_error();
		exit();
	}


	
	$id_request = base64_decode($_GET["id"]);
	
	$SQL = "SELECT date_request, date_allocate, step_request, id_user, id_license, questions_step, photo_step, photo_route, card_step, card_route, status_request, identity_verified, nombres, apellidos FROM `proctorv_request` INNER JOIN users_c ON proctorv_request.id_user = users_c.id_usuario_c WHERE id_proctorv_request = '".$id_request."'";
    $request_query = mysqli_query($conn, $SQL);
    $proctorv_request = mysqli_fetch_assoc($request_query);
	
	
	$id_taker = $proctorv_request["id_user"];
	
	
		/*
	    SESSION MANAGER
	*/
	session_start();
	
	//validate session identity with id user
	if($id_taker != $_SESSION["id_user"]){
	   session_unset();
	   session_destroy();
	   $newURL = '/ilto3/proctorv/view/login.php?id=5';
       header('Location: '.$newURL);   
       die(); 
	}
	
	//validate session lifetime 30 mins
    if ($_SESSION['loggedin_time'] + 30 * 60 < time()) {
       include '../controller/sessionController.php';
    } 
	
	//flag to show or not info
	$flag_complete = 0;
	//check if user is already register and check if there is not photo_route saved
	if($proctorv_request["card_step"]==0){
		$flag_complete = 1;
	}
	
	

?>

<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<!DOCTYPE html>
<html>
    <head>
		<title>PROCTORV TECS REGISTER</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
		<link rel="stylesheet" href="../css/style.css" rel="stylesheet" type="text/css">
	</head>
	
	<body>
		<div class="loginbox-social" style="text-align:center;">
            <div class="social-buttons">
        		<img src="/ilto3/images/logo_ILTO.png" alt="ILTO EXAMS">
            </div>
        </div>
		<div class="container">
			<div class="row">
                        <div class="col-lg-6 col-sm-6 col-xs-6" style="text-align:left;">
                           <h3><?php echo utf8_encode($proctorv_request["nombres"])." ".utf8_encode($proctorv_request["apellidos"])?> <a href="../controller/logoutController.php" class="btn btn-danger">Salir</a></h3>  
                           <h3>REGISTRO No.<?php echo $proctorv_request["id_proctorv_request"]; ?> TECS2GO </h3>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-6">
                            
                        </div>
                    </div>
			<div id="exTab1" class="container">	
				<ul  class="nav nav-pills">
					<li class="active">
				    	<a  href="#1a" class="isDisabled">1. Registro</a>
					</li>
					<li>
						<a href="#" class="isDisabled">2. Validación de Identidad</a>
					</li>
					<li>
						<a href="#" class="isDisabled" >3. Foto del Rostro</a>
					</li>
					<li href="#1a" >
						<a href="#" >4. Foto Documento de Identificación</a>
					</li>
					<li href="#1a" >
						<a href="#" class="isDisabled" >5. Pago TECS2GO</a>
					</li>
					<li href="#1a" >
						<a href="#" class="isDisabled" >6. Fecha de Examen</a>
					</li>
				</ul>
				<div class="tab-content clearfix">
					<div id="exTab1" class="container">	
						<div class="row">
							<div class="col-xs-4 col-md-4">
							
							</div>
							<div class="col-xs-4 col-md-4">
							</div>
							<div class="col-xs-4 col-md-4">
							</div>
						</div>
						<?php 
						//if the user has already complete this step just SHOW A MESSAGE
						if($flag_complete == 1){
						?>
							<div class="row" style="text-align:center;">
								<h4><img src="/ilto3/images/iconos/verified.png" title="Completed!" width="32">
	                        	<span class="green_text">REALIZADO</span></h4><br><br>
	                        		<a href="/ilto3/proctorv/view/tecs2go-payment.php?id=<?php echo $id_request?>" class="btn btn-success" id="allocate">CONTINUAR</a>
							</div>
						<?php
						}else{
						?>
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4" style="border-right:1px solid #d6d6d6;text-align:center;">
								<h2>TOME UNA FOTO DE SU DOCUMENTO</h2>
                					<div id="my_camera"></div><br>
				                	<!-- First, include the Webcam.js JavaScript Library -->
				                	<script type="text/javascript" src="/ilto3/proctorv/js/webcam.min.js"></script>
				                	<!-- A button for taking snaps -->
				                	<form>
				                		<input type="button" value="CLIC PARA TOMAR FOTO" class="btn btn-primary" onClick="take_snapshot()">
				                	</form>
				                  
				                  	<!-- Configure a few settings and attach camera -->
				                	<script language="JavaScript">
				                		Webcam.set({
				                			width: 320,
				                			height: 240,
				                			image_format: 'jpeg',
				                			jpeg_quality: 90
				                		});
				                		Webcam.attach( '#my_camera' );
				                	</script>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4" style="border-right:1px solid #d6d6d6;text-align:center;">
								<h2>FOTO DE CÉDULA/DNI/RFC</h2>
								<h3>ESTA FOTO DE SU DOCUMENTO SERÁ GUARDADA EN EL REGISTRO</h3>
								<h4>Documento de Identidad del país</h4>
								<div id="results">Su captura aparecerá aquí..</div>	
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4" style="border:1px solid #transparent;text-align:center;">
								<h2>EJEMPLO DEL DOCUMENTO</h2><br><br>
								<img src="../img/card.jpeg" title="Example for take a personal photo" width="200"/>
								<p>El número del documento debe verse claramente.</p>
							</div>
						</div>
						<div class="row">
								<div class="col-xs-4 col-md-4">
								</div>
								<div class="col-xs-4 col-md-4">
									<input type="button" id="next" value="GUARDAR Y CONTINUAR" onClick="take_photo()" class="btn btn-success btn-block btn-lg flink" disabled>
								</div>
								<div class="col-xs-4 col-md-4">
								</div>
							</div>
						<?php
						} 
						?>
					</div>
			</div>
		</div>
		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
		</div>
	<script>

	var photo_taked = "";
		function take_snapshot() {
			// take snapshot and get image data
			Webcam.snap( function(data_uri) {
				photo_taked = data_uri;
				// display results in page
				document.getElementById('results').innerHTML = 
					'<img src="'+data_uri+'"/>' + 
					'<p>Here is your image.</p>';
				document.getElementById('next').disabled = false;
			} );
		}
		
		function take_photo(){
			data_uri = photo_taked;
			const urlParams = new URLSearchParams(window.location.search);
			const myParam = urlParams.get('id');
			const id_request = atob(myParam);
			const id_taker = <?php echo $id_taker; ?>;
		    var flag = 1;
				// display results in page
				// Save the file by posting it to the server
          fetch("/ilto3/proctorv/js/save_image.php", {
            method: "post",
            body: JSON.stringify({ data: data_uri, id_request: id_request, id_taker: id_taker, flag: flag })
          })
            .then(res => res.json())
            .then(data => {
              if (data.success) {
                // Create the image and give it the CSS style with a random tilt
                //  and a z-index which gets bigger
                //  each time that an image is added to the div
                let newImage = createImage(
                  data.path,
                  "new image",
                  "new image",
                  width,
                  height,
                  "masked"
                );
                
                let tilt = -(20 + 60 * Math.random());
                newImage.style.transform = "rotate(" + tilt + "deg)";
                zIndex++;
                newImage.style.zIndex = zIndex;
                newImages.appendChild(newImage);
                canvasElement.classList.add("masked");
              }
            })
            .catch(error => console.log(error));
			window.location.href = "/ilto3/proctorv/view/tecs2go-payment.php?id="+id_request;
		}
	</script>
	
</body>
</html>
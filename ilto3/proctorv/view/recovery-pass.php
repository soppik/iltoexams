<?php

	//database connection
	if(str_replace('www.', '', $_SERVER['SERVER_NAME']) == 'iltoexams.com'){
            	    $conn = @mysqli_connect('localhost', 'iltoexam_newvers', 'Ilto.2015', 'iltoexam_newversion');      
         }else{
             	    $conn = @mysqli_connect('localhost', 'thetec_newvers', 'Ilto.2015', 'thetec_iltoexam_newversion');
         }
	if (!$conn) {
	    echo "Error: " . mysqli_connect_error();
		exit();
	}


	$id_request = base64_decode($_GET["id"]);
	
	$SQL = "SELECT date_request, date_allocate, step_request, id_user, id_license, questions_step, photo_step, photo_route, card_step, card_route, status_request, identity_verified, nombres, apellidos FROM `proctorv_request` INNER JOIN users_c ON proctorv_request.id_user = users_c.id_usuario_c WHERE id_proctorv_request = '".$id_request."'";
    $request_query = mysqli_query($conn, $SQL);
    $proctorv_request = mysqli_fetch_assoc($request_query);
	
	
	$id_taker = $proctorv_request["id_user"];
	
	
		/*
	    SESSION MANAGER
	*/
	session_start();
	
	//validate session identity with id user
	if($id_taker != $_SESSION["id_user"]){
	   session_unset();
	   session_destroy();
	   $newURL = '/ilto3/proctorv/view/login.php?id=5';
       header('Location: '.$newURL);   
       die(); 
	}
	
	//validate session lifetime 30 mins
    if ($_SESSION['loggedin_time'] + 30 * 60 < time()) {
       include '../controller/sessionController.php';
    } 
	
	//flag to show or not info
	$flag_complete = 0;
	//check if user is already register and check if there is not photo_route saved
	if($proctorv_request["questions_step"]==1){
		$flag_complete = 1;
	}
	

	//instance the array
	$arrayQuestions = array();
	
	//fill the 10 random questions
	while(count($arrayQuestions)<10){
		//generate random number
		$random = rand(1, 20);
		//check if the random is in array
		if(!in_array($random, $arrayQuestions, true)){
			//if not, insert into array
            array_push($arrayQuestions, $random);    
        }
	}
	
	
	
	

?>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<!DOCTYPE html>
<html>
    <head>
		<title>PROCTORV TECS REGISTER</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
		<link rel="stylesheet" href="../css/style.css" rel="stylesheet" type="text/css">
	</head>
	
	<body>
	
		<div class="loginbox-social" style="text-align:center;">
                    <div class="social-buttons">
                        <img src="/ilto3/images/logo_ILTO.png" alt="ILTO EXAMS">
                    </div>
                </div>
		<div class="container">
			
			<div id="exTab1" class="container">	
				<ul  class="nav nav-pills">
					
					<li class="active">
				    	<a  href="#1a" class="isDisabled">1. Registro</a>
					</li>
					<li>
						<a href="#" >2. Validación de Identidad</a>
					</li>
					<li>
						<a href="#" class="isDisabled" >3. Foto del Rostro</a>
					</li>
					<li href="#1a" >
						<a href="#" class="isDisabled">4. Foto Documento de Identificación</a>
					</li>
					<li href="#1a" >
						<a href="#" class="isDisabled" >5. Pago TECS2GO</a>
					</li>
					<li href="#1a" >
						<a href="#" class="isDisabled" >6. Fecha de Examen</a>
					</li>
				</ul>
				<div class="tab-content clearfix">
					<div id="exTab1" class="container">	
					<div class="row">
								<div class="col-xs-4 col-md-4">
								
								</div>
								<div class="col-xs-4 col-md-4">
								</div>
								<div class="col-xs-4 col-md-4">
								</div>
							</div>
							<?php 
							//if the user has already complete this step just SHOW A MESSAGE
							if($flag_complete == 1){
							?>
							<div class="row" style="text-align:center;">
								<h4><img src="/ilto3/images/iconos/verified.png" title="Completed!" width="32">
								<br><a href="/ilto3/proctorv/view/take-photo.php?id=<?php echo base64_encode($id_request)?>" class="btn btn-success" id="allocate">CONTINUAR</a>
	                        	<span class="green_text">REALIZADO</span></h4>
									
							</div>
							<?php
							}else{
							?>
					</div>
						<form role="form" method="POST" action="/ilto3/proctorv/controller/recoveryPassController.php">
						<input type="hidden" name="id_taker" value="<?php echo base64_encode($id_taker) ?>"/>
						<input type="hidden" name="id_request" value="<?php echo base64_encode($id_request) ?>"/>
							<h3>Por favor responda las siguientes preguntas de información personal: </h3>
							<?php 
							$counter = 0;
				          	foreach($arrayQuestions as $question){
				          			//get countries list
				          			$counter++;
									$SQL='SELECT * FROM proctorv_questions WHERE id_proctorv_question = "'.$question.'"';
									$question_query = mysqli_query($conn, $SQL);
				          			$question_row=mysqli_fetch_assoc($question_query);
				          		//	var_dump($question_row);
				          			
							?>
								<div class="row">
									<div class="col-xs-8 col-sm-8 col-md-8 col-sm-offset-2 col-md-offset-3">
										<label><?php echo "<p class='question_text'>".$counter."- ".utf8_encode($question_row["description"])."</p>"; ?></label>
										<div class="form-group questions">
											<input type="text" name="<?php echo $question ?>" id="<?php echo $question ?>" class="form-control input-lg" placeholder="Respuesta" tabindex="<?php echo $counter; ?>" required>
										</div>
									</div>
								</div>
								
							<?php
				          	}
							?>
							<div class="row">
								<div class="col-xs-4 col-md-4">
								
								</div>
								<div class="col-xs-4 col-md-4">
								
								</div>
								<div class="col-xs-4 col-md-4">
									<button type="submit" name="register" id="register" class="btn btn-success btn-block btn-lg">CONTINUAR</button>
								</div>
							</div>
						</form>
						<?php 
						}
						?>
					</div>
				</div>
			</div>
		</div>
			<script src="https://www.google.com/recaptcha/api.js?render=6Let4uoUAAAAAD_PjMRfVb2-49diSGoxL4l0DexP"></script>
							<script>
							grecaptcha.ready(function() {
							    grecaptcha.execute('6Let4uoUAAAAAD_PjMRfVb2-49diSGoxL4l0DexP', {action: 'homepage'}).then(function(token) {
							       ...
							    });
							});
							</script>	
				
				<!-- Bootstrap core JavaScript
				    ================================================== -->
					<!-- Placed at the end of the document so the pages load faster -->
					<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
			
			
			
			
			
			
			
			
		</div>
	</body>
<html>
<script>



	$(function () {
    $('.button-checkbox').each(function () {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }

        // Initialization
        function init() {

            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i>');
            }
        }
        init();
    });
});
</script>
<?php
	//close connection
    $conn->close();

?>
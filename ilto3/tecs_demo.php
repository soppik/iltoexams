<?php 

$body_background = 'background-image: url(/ilto3/themes/ilto/img/desktop.jpg) !important;';

$message = $_GET["message"];

if(!is_null($message)){
    echo '
    <style> 
        .logobox {
            border-color: #df5138 !important;
            background: #e46f61 !important;
            padding:5px !important;
            color: #fff;
            min-height:50px !important;
            height: auto !important;
            text-align:center !important;
        }
    </style>';
}

if($flag_econfirm==1){
    echo '
    <style> 
        .logobox {
            border-color: #1d6126 !important;
            background: #3ab54b !important;
            padding:5px !important;
            color: #fff;
            min-height:50px !important;
            height: auto !important;
            text-align:center !important;
        }
    </style>';
}

?>


<html lang="en">

<head>
<meta charset="utf-8">
<title>ILTO - TECS DEMO</title>
<meta name="description" content="inbox" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="google" content="notranslate" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" href="https://www.iltoexams.com/wp-content/uploads/2019/03/cropped-favicon-32x32.png" sizes="32x32" />
<link rel="icon" href="https://www.iltoexams.com/wp-content/uploads/2019/03/cropped-favicon-192x192.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="https://www.iltoexams.com/wp-content/uploads/2019/03/cropped-favicon-180x180.png" />
<meta name="msapplication-TileImage" content="https://www.iltoexams.com/wp-content/uploads/2019/03/cropped-favicon-270x270.png" />

<!--Basic Styles-->
<link href="/ilto3/themes/ilto/html/assets/css/bootstrap.min.css" rel="stylesheet" />
<link id="bootstrap-rtl-link" href="" rel="stylesheet" />
<link href="/ilto3/themes/ilto/html/assets/css/font-awesome.min.css" rel="stylesheet" />
<link href="/ilto3/themes/ilto/html/assets/css/weather-icons.min.css" rel="stylesheet" />

<!--Fonts-->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css">

<!--Beyond styles-->
<link id="beyond-link" href="/ilto3/themes/ilto/html/assets/css/beyond.min.css" rel="stylesheet" />
<link href="/ilto3/themes/ilto/html/assets/css/demo.min.css" rel="stylesheet" />
<link href="/ilto3/themes/ilto/html/assets/css/typicons.min.css" rel="stylesheet" />
<link href="/ilto3/themes/ilto/html/assets/css/animate.min.css" rel="stylesheet" />
<link id="skin-link" href="" rel="stylesheet" type="text/css" />


<script>
  window.templateUrl = "/ilto3/themes/ilto/html";
</script>

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

  <script src="/ilto3/assets/js/jquery.min.js"></script>
    <script src="/ilto3/assets/js/bootstrap.min.js"></script>
<!--Skin Script: Place this script in head to load scripts for skins and rtl support-->
<script src="/ilto3/themes/ilto/html/assets/js/skins.js"></script>

<style> 
    body {
    <?php echo $body_background; ?>
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    color:#fff;
    }
    body:before {
    background-color: transparent !important;
    }
    .login-container .loginbox  {
        width: 400px !important;
        padding: 30px 0px !important;
    }
    .login-container .logobox {
        box-shadow: 3px 3px 3px #444;
        width: 400px !important;
    }
    .flash-error {
        border-color: #df5138;
        background: #e46f61;
        margin-bottom: 20px;
        margin-top: 0;
        color: #fff;
        border-width: 0;
        border-left-width: 5px;
        padding: 10px;
        border-radius: 0;
        }
        
    .login-container .logobox {
    height: auto !important;
    position: relative;
        font-size: 14px;
    padding: 12px !important;
    }    
    
    .login-container{margin:2% auto !important;
    }    
    
    .form-control, select {
        font-size: 16px !important;
    }
    .spacer {
        display:block;
        height:120px;
    }
    
    #home_link {
        color: #fff;
        text-decoration:none;
    }
    
    #home_link over{
        color: #fff;
        text-decoration:none;
        font-weight:bold;
    }
    
    .description_exam {
        position:relavite;
    }
    
    .step_warning {
        text-align:left;
    }
    
    #contact-number {
        width:300px;
        height:30px;
        font-size:14px;
    }
    
    input[type=checkbox] {
        position:relative;
        left:0px;
        opacity:1;
    }
    
    .btnExam {
        border-color: #1d7f2b;
        background-color: #31b744;
        color: #ffffff;
        font-family: Montserrat, sans-serif;
        line-height: 1;
        display: inline-block;
        font-size: 18px !important;
        font-weight: 600 !important;
        width: 340px !important;
        text-overflow: ellipsis;
        white-space: nowrap;
        padding: 25px 37.5px;
        cursor: pointer;
        text-align: center;
        overflow: hidden;
        vertical-align: top;
        white-space: nowrap;
        text-overflow: ellipsis;
        border: 1px solid;
        box-shadow: none;
        transition: .3s;
        text-transform: uppercase;
        letter-spacing: 0.06px;
        border-radius: 3px;
    }
    
    #photo_name {
        text-align:center;
    }
    
 </style>

<script>
  function Html2CSV(tableId, filename,alinkButtonId) {
        var array = [];
        var headers = [];
        var arrayItem = [];
        var csvData = new Array();
        $('#' + tableId + ' th').each(function (index, item) {
            headers[index] = '"' + $(item).html() + '"';
        });
        csvData.push(headers);
        $('#' + tableId + ' tr').has('td').each(function () {

            $('td', $(this)).each(function (index, item) {
                arrayItem[index] = '"' + $(item).html() + '"';
            });
            array.push(arrayItem);
            csvData.push(arrayItem);
        });




        var fileName = filename + '.csv';
        var buffer = csvData.join("\n");
        var blob = new Blob([buffer], {
            "type": "text/csv;charset=utf8;"
        });
        var link = document.getElementById(alinkButtonId);

        if (typeof link.download !== undefined) { // feature detection
            // Browsers that support HTML5 download attribute
            link.setAttribute("href", window.URL.createObjectURL(blob));
            link.setAttribute("download", fileName);
        }
        else if (navigator.msSaveBlob) { // IE 10+
            link.setAttribute("href", "#");
            link.addEventListener("click", function (event) {
                navigator.msSaveBlob(blob, fileName);
            }, false);
        }
        else {
            // it needs to implement server side export
            link.setAttribute("href", "http://www.example.com/export");
        }
    }

</script></head>
<!-- Body -->

<body>
    <div class="spacer" style="height:50px;">
    </div>  
    <div class="row" style="padding:20px;max-width:900px;text-align:center;margin:auto;">
        <div class="col-sm-4">
            <img src="/ilto3/images/iltoexams-logo-100-white.png" alt="ILTO EXAMS">
        </div>
        <div class="col-sm-4">
            
        </div>
        <div class="col-sm-4">
            <img src="/ilto3/images/tecs-short-logo.png" height="75"  alt="TECS ILTO">
        </div>
    </div>
    <div class="row" style="max-width:900px;margin:auto;text-align:left;">
        <h4 style='text-shadow: 1px 1px #191919;'><b>TECS DEMO</b></h4>
        <p style="text-align:justify;">The Test of English Communication Skills – <span style='color:#ffc000;font-weight:bold;'>TECS</span> – is a Standardized exam that assesses your English proficiency level through a comprehensive items bank, based on updated topics used by individuals in real communication contexts.
The <span style='color:#ffc000;font-weight:bold;'>TECS</span> is made of four different sections that refer to each of the main communication skills as follows:
        </p>
        <ul>
            <li>Rated interview.</li>
            <li>Listening in context.</li>
            <li>Reading in context. </li>
            <li>Language Use.</li>
        </ul>
        <p style="text-align:justify;">A trained and certified TECS Rater who will ask questions written in a TECS official online form, will rate each answer considering the comprehension and response of such question. </br></br>
        The questions asked to the test taker are presented in progressive difficulty, from A1 to C1 levels according to the CEFR. The <span style='color:#ffc000;font-weight:bold;'>TECS</span> algorithm will stop showing questions according to the level reached by the taker. For instance, if questions pertaining to a B1 level are not being answered correctly or the comprehension and response performance is low, the system will stop showing questions of a higher level and ask the test taker to continue to the remaining sections. </br></br> 
Click on the following button to access a demo test of 10 questions per section. This will give you an idea of the <span style='color:#ffc000;font-weight:bold;'>TECS</span> Structure.
        </p>
        <p style='width:100%;text-align:center;'><strong>Please allow pop up windows in your browser. Go to options and select allow pop up windows. If not, you will not be able to access the test.</strong>
        </p> 
        
    </div>
    <div class="spacer" style="height:25px;">
    </div>  
    <div class="row" style="max-width:900px;text-align:center;margin:auto;">
        <form  action="/ilto3/index.php?r=UsersC/demo_tecs" method="post" id="ede_form">       
            <div class="loginbox-submit">
                <input type="hidden" name="id_usuario_c" value="1001">
                <input class="btnExam" type="submit" value="start DEMO Test" style="border:none;height: 80px !important;font-size:18px !important;">
            </div>        
            <div class="loginbox-submit">
                <?php
                if(isset($_GET['err'])){
                    if($_GET['err']==1){
                        $message='There is already user with this email, please ask your Test Administrator';
                    }
                    if($_GET['err']==2){
                        $message='There are no more licenses for this Exam, please ask your Test Administrator';
                    }
                    $key='error';
                    echo '<div onClick="divclose(this);" id="flash-'.$key.'" class="flash-' . $key . '">' . $message . "&nbsp;&nbsp;&nbsp;Click Here to dismiss</div>\n";
                    }
                    ?>
            </div>
        </form>
    </div>    
    <script>
        function divclose(divObj){
            $(divObj).fadeOut();
        }
    </script>
</body>
<!-- /Body -->
</html>
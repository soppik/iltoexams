<?php 


$userId = Yii::app()->user->getState('cliente');
$userType = Yii::app()->user->getState('id_perfil');
if($userType != 6){ 

//TECS V 4.0
$cantidad_tecs = $utilizados_tecs = $cantidad_tecs3 = $utilizados_tecs3 = $cantidad_ede3 =  $utilizados_ede3 = $cantidad_ede =  $utilizados_ede = $cantidad_takers = 0;
$SQL = "SELECT sum(cantidad) as cantidad, sum(utilizados) as utilizados FROM `licenses_client` WHERE `id_cliente` LIKE '$userId' and id_licencia = 15 and licenses_client.demo ='0';";


$list= Yii::app()->db->createCommand($SQL)->queryAll();

foreach($list as $value){
    $cantidad_tecs = $value['cantidad'];
    $utilizados_tecs = $value['utilizados'];
}

//TECS 3.0
$SQL = "SELECT sum(cantidad) as cantidad, sum(utilizados) as utilizados FROM `licenses_client` WHERE `id_cliente` LIKE '$userId' and id_licencia = 7 and licenses_client.demo ='0';";


$list= Yii::app()->db->createCommand($SQL)->queryAll();

foreach($list as $value){
    $cantidad_tecs3 = $value['cantidad'];
    $utilizados_tecs3 = $value['utilizados'];
}

$cantidad_tecs = $cantidad_tecs + $cantidad_tecs3;
$utilizados_tecs = $utilizados_tecs + $utilizados_tecs3;

//EDE 4.0
$SQL = "SELECT sum(cantidad) as cantidad, sum(utilizados) as utilizados  FROM `licenses_client` WHERE `id_cliente` LIKE '$userId' and id_licencia = 16;";
$list= Yii::app()->db->createCommand($SQL)->queryAll();

foreach($list as $value){
    $cantidad_ede = $value['cantidad'];
    $utilizados_ede = $value['utilizados'];
}

//EDE 3.0
$SQL = "SELECT sum(cantidad) as cantidad, sum(utilizados) as utilizados  FROM `licenses_client` WHERE `id_cliente` LIKE '$userId' and id_licencia = 6;";
$list= Yii::app()->db->createCommand($SQL)->queryAll();

foreach($list as $value){
    $cantidad_ede3 = $value['cantidad'];
    $utilizados_ede3 = $value['utilizados'];
}

$cantidad_ede = $cantidad_ede + $cantidad_ede3;
$utilizados_ede = $utilizados_ede + $utilizados_ede3;


$SQL = "SELECT count(id_usuario_c) as totales  FROM `users_c` WHERE `id_cliente` LIKE '$userId' AND `id_perfil` = 6";
$list= Yii::app()->db->createCommand($SQL)->queryAll();

$SQL = "SELECT COUNT(id_usuario) as totale FROM `licenses_user` INNER JOIN licenses_client ON id_licencias_cliente = id_licencia_cliente and id_licencia IN (6,16) INNER JOIN users_c ON licenses_user.id_usuario = users_c.id_usuario_c AND users_c.id_cliente = '".(int)Yii::app()->user->getState('cliente')."' ";
$edeTakers= Yii::app()->db->createCommand($SQL)->queryAll();

$SQL = "SELECT COUNT(id_usuario) as totale FROM `licenses_user` INNER JOIN licenses_client ON id_licencias_cliente = id_licencia_cliente and id_licencia IN (7,15) INNER JOIN users_c ON licenses_user.id_usuario = users_c.id_usuario_c AND users_c.id_cliente = '".(int)Yii::app()->user->getState('cliente')."' ";
$tecsTakers= Yii::app()->db->createCommand($SQL)->queryAll();

$totalTakers = ($edeTakers[0]['totale']) + $tecsTakers[0]['totale'];


foreach($list as $value){
       $cantidad_takers = $value['totales'];
}

    $SQL ="SELECT licenses_user.fecha_asignacion FROM `licenses_user` INNER JOIN licenses_client ON id_licencias_cliente = id_licencia_cliente and id_licencia IN (7,15) INNER JOIN users_c ON licenses_user.id_usuario = users_c.id_usuario_c AND users_c.id_cliente = '".(int)Yii::app()->user->getState('cliente')."' ORDER BY licenses_user.fecha_asignacion ASC LIMIT 1";
    $date= $connection->createCommand($SQL)->queryAll();
    
    $getDate = substr($date[0]['fecha_asignacion'], 0, 7);


?>
<style>
    
    .bg-themesecondary {
        background-color: #EE5959 !important;
    }
    
    .bg-themethirdcolor {
        background-color: #FFDA74 !important;
    }
    
    .bg-themeprimary {
         background-color: #9055CF !important;
    }
    
    .databox .databox-number {
         margin:1px;   
    }
    
    .databox .databox-text {
        display: block;
        font-size: 10px;
        margin: 0px;
        position: relative;
    }
    .databox a:hover {
        text-decoration:none;
    }
</style>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="databox bg-white radius-bordered">
                                        <div class="databox-left bg-themesecondary">
                                            <div class="databox-piechart">
                                                <div data-toggle="easypiechart" class="easyPieChart" data-barcolor="#fff" data-linecap="butt" data-percent="<?php echo 100 - ( ($cantidad_tecs>0)?ceil(($utilizados_tecs/$cantidad_tecs)*100):0 )?>" data-animate="500" data-linewidth="3" data-size="47" data-trackcolor="rgba(255,255,255,0.1)" style="width: 47px; height: 47px; line-height: 47px;"><span class="white font-90"><?php echo  100 - ( ($cantidad_tecs>0)?ceil(($utilizados_tecs/$cantidad_tecs)*100):0 )?>%</span><canvas width="47" height="47"></canvas></div>
                                                
                                            </div>
                                        </div>
                                        <div class="databox-right">
                                            <a href="/ilto3/index.php?r=Clients/MyAccount"><span class="databox-number themesecondary"><?php echo $cantidad_tecs ?> TOTAL</span>
                                                <div class="databox-text darkgray"><b>TECS LICENSES</b> (<?php echo $utilizados_tecs ?> of <?php echo $cantidad_tecs ?>, <?php echo 100 - (  ($cantidad_tecs>0)?ceil(($utilizados_tecs/$cantidad_tecs)*100):0 ) ?>% available)</div>
                                                <div class="databox-stat themesecondary radius-bordered">
                                                    <i class="stat-icon icon-lg fa fa-tasks"></i>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="databox bg-white radius-bordered">
                                        <div class="databox-left bg-themethirdcolor">
                                            <div class="databox-piechart">
                                                <div data-toggle="easypiechart" class="easyPieChart" data-barcolor="#fff" data-linecap="butt" data-percent="<?php echo 100 - (  ($cantidad_ede>0)?ceil(($utilizados_ede/$cantidad_ede)*100):0 ) ?>" data-animate="500" data-linewidth="3" data-size="47" data-trackcolor="rgba(255,255,255,0.2)" style="width: 47px; height: 47px; line-height: 47px;"><span class="white font-90"><?php echo 100 - (  ($cantidad_ede>0)?ceil(($utilizados_ede/$cantidad_ede)*100):0 ) ?>%</span><canvas width="47" height="47"></canvas></div>
                                            </div>
                                        </div>
                                        <div class="databox-right">
                                            <a href="/ilto3/index.php?r=Clients/MyAccount"><span class="databox-number themethirdcolor"><?php echo $cantidad_ede ?> TOTAL</span>
                                                <div class="databox-text darkgray"><b>EDE LICENSES</b> (<?php echo $utilizados_ede ?> of <?php echo $cantidad_ede ?>, <?php echo  100 - ( ($cantidad_ede>0)?ceil(($utilizados_ede/$cantidad_ede)*100):0 ) ?>% available) </div>
                                                <div class="databox-stat themethirdcolor radius-bordered">
                                                    <i class="stat-icon  icon-lg fa fa-check-circle" style="font-size: 24px;"></i>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="databox bg-white radius-bordered">
                                        <div class="databox-left bg-themeprimary">
                                            <div class="databox-piechart">
                                                <div id="users-pie" data-toggle="easypiechart" class="easyPieChart" data-barcolor="#fff" data-linecap="butt"  data-animate="500" data-linewidth="3" data-size="47" data-trackcolor="rgba(255,255,255,0.1)" style="width: 47px; height: 47px; line-height: 47px;"><span class="white font-90"><?php echo $totalTakers ?></span><canvas width="47" height="47"></canvas></div>
                                            </div>
                                        </div>
                                        <div class="databox-right">
                                            <span class="databox-number themeprimary"><?php echo $totalTakers ?> TOTAL</span>
                                            <div class="databox-text darkgray"><b>TOTAL TECS TAKERS </b><?php echo $tecsTakers[0]['totale']; //$getDate;  ?></div>
                                            <div class="databox-text darkgray"><b>TOTAL EDE TAKERS </b><?php echo $edeTakers[0]['totale']; //$getDate;  ?></div>
                                            <div class="databox-state bg-themeprimary">
                                                <i class="fa fa-check"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                         
                            </div>
                        </div>
<style>
    
    .default.index .databox {
        display: inline-block;
        width: 100%;
        height: 65px;
        padding: 0;
        font-size: 0;
        margin-bottom: 5px;
        vertical-align: top;
        min-width: 130px;
    }

</style>
<?php 
}
?>
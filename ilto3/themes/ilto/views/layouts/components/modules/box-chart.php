<style>
    .navbar-inner .navbar-header .navbar-account .account-area>li {
        float: left;
        position: relative;
        height: 45px;
        width: 215px;
    }
    a.login-area.dropdown-toggle {
        width: 215px;
    }
    .item-time.pull-right {
        margin-top: 6px;
        text-align: right;
    }
    .item-time.pull-right {
        margin-top: 6px;
        text-align: right;
        margin-right: 26px;
    }
  .legend {
    left: 151px;
    position: relative;
}
    .orders-container .orders-list .order-item {
    position: relative;
    padding: 3px 11px;
    vertical-align: top;
    border-bottom: 1px solid #e5e5e5;
    height: 38px;
    font-size:10px;
}

.orders-container .orders-list .order-item .item-left .item-time {
    color: #39B54A;
}

.orders-container .orders-header {
    background-color:#1363DC;
    text-align: center;
    color:#fff;


}

.orders-container .orders-header h6 {
    text-transform:uppercase;
 }

.orders-container .orders-list {
    min-height: 379px;
}

.cerf_level {
    color:#EE5959;
    font-weight:600;
}

.x-line {
    position: absolute;
    bottom: 33px;
    right: 20px;
    font-size: 10;
    font-weight: 700;
}


.y-line {
    position: absolute;
    top: 40px;
    left: 30px;
    font-size: 10;
    font-weight: 700;
}

#1 {
    display:block;
}

#ede_order {
    min-height: 476px;
}

.flot-base {
    top:-100px;
}

#no_statistics {
    margin: auto;
    padding: 25px;
}

#no_statistics2 {
    margin: auto;
    padding: 25px;
}

</style>

<?php 
    $SQL ="SELECT licenses_user.fecha_asignacion FROM `licenses_user` INNER JOIN licenses_client ON id_licencias_cliente = id_licencia_cliente and id_licencia IN (7,15) INNER JOIN users_c ON licenses_user.id_usuario = users_c.id_usuario_c AND users_c.id_cliente = '".(int)Yii::app()->user->getState('cliente')."' ORDER BY licenses_user.fecha_asignacion ASC LIMIT 1";
    $date= $connection->createCommand($SQL)->queryAll();
    
    
    $getDate = substr($date[0]['fecha_asignacion'], 0, 7);
    if($getDate!=false){
      $getDate = "SINCE ".$getDate;  
    }
    

?>

<div class="row">
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" id="5">
        <div class="orders-container" style="height: 520px;">
            <div class="orders-header">
                <h6><b>General English proficiency TECS level of the organization <?php echo $getDate; ?></b></h6>
            </div>
            <?php
            //$SQL = "SELECT COUNT(nivel) FROM `licenses_user` INNER JOIN users_c ON licenses_user.id_usuario = users_c.id_usuario_c  WHERE users_c.id_cliente = '".(int)Yii::app()->user->getState('cliente')."' AND nivel = 'A1'";
            //$list_level= $connection->createCommand($SQL)->queryAll();
            $SQL = "SELECT *, count(nivel) as totales FROM `licenses_user` INNER JOIN licenses_client ON id_licencias_cliente = id_licencia_cliente and id_licencia IN (7,15)  INNER JOIN users_c ON licenses_user.id_usuario = users_c.id_usuario_c  WHERE users_c.id_cliente = '".(int)Yii::app()->user->getState('cliente')."' and licenses_client.demo ='0' group by nivel";
            
            $list_groups= $connection->createCommand($SQL)->queryAll();
           
            
            $colors = array('themeprimary', 'themesecondary', 'themethirdcolor', 'themefourthcolor', 'themefifthcolor', 'themesixthcolor','themeseventhcolor');
            //$colors = array('#9A1D56', '#F9E500', '#1974BA', '#DC2947', '#4EB92C', 'themesecondary', 'themeprimary');  
            foreach($list_groups as $key => $li){
                if(strlen($li['nivel'])==0){
                    $li['nivel'] ='No Level';
                }
                $Data_[] = '{ label: "'.$li['nivel'].' ('.$li['totales'].')", data: [[1, '.$li['totales'].']], color: '.$colors[$key].' }';    
            }
                        
            $Data  = "var data = [";
                if($Data_){
                    $Data .= implode(',',$Data_);
                }
            $Data .= "];"; 
            if(empty($list_groups)){
                $Data = 'var data = [{ label: "No Level (1)", data: [[1, 1]], color: themeprimary }, { label: "-A1 (1)", data: [[1, 1]], color: themesecondary }, { label: "A1 (1)", data: [[1, 1]], color: themethirdcolor }, { label: "A2 (1)", data: [[1, 1]], color: themefourthcolor }, { label: "B1 (1)", data: [[1, 1]], color: themefifthcolor }, { label: "B2 (1)", data: [[1, 1]], color: themesixthcolor }, { label: "C1 (1)", data: [[1, 1]], color: themeseventhcolor }]';
            }
                            
            /*$SQL = "SELECT COUNT(*) FROM `licenses_user` INNER JOIN users_c ON licenses_user.id_usuario = users_c.id_usuario_c  WHERE users_c.id_cliente = '".(int)Yii::app()->user->getState('cliente')."'";
            $list_total= $connection->createCommand($SQL)->queryAll();
            */
                            
            ?>
            <style>
                #pie-chart {
            		width: 450px;
            		top:-40px;
            	}
            	#pie-chart2 {
            		width: 450px;
            		top:-40px;
            	}
            	.chart-1 {
            		height: 450px;
            	}
            	    </style>
            <a class="btn btn-success" style="top: 300px; left: 480px;" href="/ilto3/index.php?r=Api/ReportExport&id_cliente=<?php echo Yii::app()->user->getState('cliente') ?>">Click here to <br/>download the Report</a>
                    <div id="pie-chart" class="chart-1 chart-1"></div>
           </div>
    </div><!--END 5 -->
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="4">
        <?php
        $SQL = "SELECT * FROM licenses_user lu, licenses_client lc WHERE lu.id_licencia_cliente = lc.id_licencias_cliente 
        AND fecha_presentacion >= '".date("Y-m-d")."' 
        AND id_cliente = '$userId' 
        GROUP BY day(fecha_presentacion) limit 1";
        
        
        $list= Yii::app()->db->createCommand($SQL)->queryAll();
        $ProxDate = false;
        foreach($list as $value){
            $proxDate = $value;
        }
        ?>
        <div class="dashboard-box">
             <div class="box-visits">
                <div class="row">
                        <a href="/ilto3/index.php?r=Clients/ExamsUser/schedule" style="display:block;">
                    <div class="col-lg-12 col-sm-12 col-xs-12">
                        <div class="notification">
                            <div class="clearfix">
                                <div class="notification-icon">
                                    <i class="fa fa-check azure bordered-1 bordered-azure"></i>
                                </div>
                                <div class="notification-body">
                                    <span class="title">Next scheduled test</span>
                                    <?php if($proxDate['fecha_presentacion']){
                                    ?>
                                    <span class="description"><?php echo date('d F Y H:i', strtotime($proxDate['fecha_presentacion']))?></span>
                                    <?php
                                    }else{
                                    ?>
                                    <span class="description">No Date</span>
                                    <?php
                                    }?>
                                </div>
                                <div class="notification-extra">
                                    <a href="/ilto3/index.php?r=Clients/ExamsUser/schedule">
                                    <i class="fa fa-calendar azure"></i>
                                    <i class="fa fa-clock-o azure"></i>
                                    </a>
                                    <span class="description"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
        </div>
    </div><!-- END 4 -->
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="3">
        <div class="orders-container">
            <div class="orders-header">
                <h6><b>Best TECS results <?php echo $getDate; ?></b></h6>
            </div>
                <ul class="orders-list">
                    <?php 
                    $config = Yii::app()->getComponents(false);
                    $connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
                    $connection->active=true;
                    
                    $SQL = "SELECT * FROM `licenses_user` INNER JOIN licenses_client ON id_licencias_cliente = id_licencia_cliente and id_licencia IN (7,15) INNER JOIN users_c ON licenses_user.id_usuario = users_c.id_usuario_c AND users_c.id_cliente = '".(int)Yii::app()->user->getState('cliente')."' and licenses_client.demo ='0' and licenses_user.estado = 'F' ORDER BY calificacion DESC LIMIT 10"; 
                    $list_g= $connection->createCommand($SQL)->queryAll();
        
                    if(empty($list_g)){
                        ?>
                        <ul class="orders-list">
                         <div class="row" id="no_statistics">
                            <h3>This area shows the best TECS results, when at least 1 test has been submitted.</h3><br/>
                            <img src="<?php echo Yii::app()->theme->baseUrl ?>/img/TECS.png" width="200" style="margin-left: 25px;margin-top: 30px;"/>
                        </div>
                        <?php
                    }else{
                        foreach($list_g as $item_){
                        ?>
                         
                            <li class="order-item">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 item-left"><a target="_blank" href="/ilto3/index.php?r=Clients/ExamsUser/admin&id=<?php echo $item_['id_licencias_usuario'] ?>">
                                            <div class="item-booker pull-left"><?php echo strtoupper(utf8_encode($item_['nombres'])).' '.strtoupper(utf8_encode($item_['apellidos']));  ?></div>
                                            <div class="item-time pull-right ">
                                            <span><?php echo $item_['calificacion'] ?></span>
                                            <i class="fa fa-check"></i>
                                            <span class="cerf_level"><?php echo $item_['nivel'] ?></span>
                                            </div></a>
                                        </div>
                                </div>
                            </li>
                       
                        <?php
                        }
                    }
                    ?>
                     </ul>
            </div>
    </div><!--END 3 -->
     <div class="col-xs-12 col-md-12 col-lg-12" id="1" >
        <div class="widget">
            <div class="widget-header">
                <span class="widget-caption">MONTHLY TECS EXAMS 2016</span>
                <div class="widget-buttons">
                    <a href="#" data-toggle="maximize"><i class="fa fa-expand"></i></a>
                    <a href="#" data-toggle="collapse"><i class="fa fa-minus"></i></a>
                    <a href="#" data-toggle="dispose"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <div class="widget-body">
                <div class="row">
                    <div class="col-sm-12" >
                        <div id="selectable-chart" class="chart chart-lg"></div>
                    </div>
                    <span class="x-line">Month</span>
                    <span class="y-line">Exams</span>
                </div>
            </div>
        </div>
    </div><!-- end 1 -->
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" id="5">
        <div class="orders-container" style="height: 520px;">
            <div class="orders-header">
                <h6><b>General English proficiency EDE level of the organization <?php echo $getDate; ?></b></h6>
            </div>
            <?php
            //$SQL = "SELECT COUNT(nivel) FROM `licenses_user` INNER JOIN users_c ON licenses_user.id_usuario = users_c.id_usuario_c  WHERE users_c.id_cliente = '".(int)Yii::app()->user->getState('cliente')."' AND nivel = 'A1'";
            //$list_level= $connection->createCommand($SQL)->queryAll();
            $SQL = "SELECT *, count(nivel) as totales FROM `licenses_user` INNER JOIN licenses_client ON id_licencias_cliente = id_licencia_cliente and id_licencia IN (6,16)  INNER JOIN users_c ON licenses_user.id_usuario = users_c.id_usuario_c  WHERE users_c.id_cliente = '".(int)Yii::app()->user->getState('cliente')."' and licenses_client.demo ='0' group by nivel";
           
            $list_groups= $connection->createCommand($SQL)->queryAll();
            $colors = array('themeprimary', 'themesecondary', 'themethirdcolor', 'themefourthcolor', 'themefifthcolor', 'themesixthcolor','themeseventhcolor');
            //$colors = array('#9A1D56', '#F9E500', '#1974BA', '#DC2947', '#4EB92C', 'themesecondary', 'themeprimary');  
            foreach($list_groups as $key => $li){
                if(strlen($li['nivel'])==0){
                    $li['nivel'] ='No Level';
                }
                $DataE_[] = '{ label: "'.$li['nivel'].' ('.$li['totales'].')", data: [[1, '.$li['totales'].']], color: '.$colors[$key].' }';    
            }
                        
            $DataE  = "var data = [";
                if($DataE_){
                    $DataE .= implode(',',$DataE_);
                }
            $DataE .= "];"; 
            
            
            if(empty($list_groups)){
                  $DataE = 'var data = [{ label: "No Level (1)", data: [[1, 1]], color: themeprimary }, { label: "-A1 (1)", data: [[1, 1]], color: themesecondary }, { label: "A1 (1)", data: [[1, 1]], color: themethirdcolor }, { label: "A2 (1)", data: [[1, 1]], color: themefourthcolor }, { label: "B1 (1)", data: [[1, 1]], color: themefifthcolor }, { label: "B2 (1)", data: [[1, 1]], color: themesixthcolor }, { label: "C1 (1)", data: [[1, 1]], color: themeseventhcolor }]';
           }
                 
            /*$SQL = "SELECT COUNT(*) FROM `licenses_user` INNER JOIN users_c ON licenses_user.id_usuario = users_c.id_usuario_c  WHERE users_c.id_cliente = '".(int)Yii::app()->user->getState('cliente')."'";
            $list_total= $connection->createCommand($SQL)->queryAll();
            */
                            
            ?>
            <style>
                #pie-chart2 {
            		width: 450px;
            	}
            	.chart-2 {
            		height: 450px;
            	}
            	    </style>
            <a class="btn btn-success" style="top: 300px; left: 480px;" href="/ilto3/index.php?r=Api/ReportExport&id_cliente=<?php echo Yii::app()->user->getState('cliente') ?>">Click here to <br/>download the Pie Report</a>
                    <div id="pie-chart2" class="chart-2 chart-2"></div>
           </div>
    </div><!--END 5 -->
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="3">
        <div class="orders-container">
            <div class="orders-header">
                <h6><b>Best EDE results <?php echo $getDate; ?></b></h6>
            </div>
                <ul class="orders-list" id="ede_order">
                    <?php 
                    $config = Yii::app()->getComponents(false);
                    $connection=new CDbConnection($config["db"]->connectionString,$config["db"]->username,$config["db"]->password);
                    $connection->active=true;
                    
                    $SQL = "SELECT * FROM `licenses_user` INNER JOIN licenses_client ON id_licencias_cliente = id_licencia_cliente and id_licencia IN (6,16) INNER JOIN users_c ON licenses_user.id_usuario = users_c.id_usuario_c AND users_c.id_cliente = '".(int)Yii::app()->user->getState('cliente')."' and licenses_client.demo ='0' and licenses_user.estado = 'F' ORDER BY calificacion DESC LIMIT 10"; 
                    
                    $list_g= $connection->createCommand($SQL)->queryAll();
                    
                    if(empty($list_g)){
                       
                        ?>
                        <div class="row" id="no_statistics2">
                            <h3>This area shows the best EDE results, when at least 1 test has been submitted.</h3><br/>
                            <img src="<?php echo Yii::app()->theme->baseUrl ?>/img/EDE.png" width="250" style="margin-left: 5px;margin-top: 30px;"/>
                        </div>
                        <?php
                    }else {
                        foreach($list_g as $item_){
                        ?>
                            <li class="order-item">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 item-left"><a target="_blank" href="/ilto3/index.php?r=Clients/ExamsUser/admin&id=<?php echo $item_['id_licencias_usuario'] ?>">
                                            <div class="item-booker pull-left"><?php echo strtoupper(utf8_encode($item_['nombres'])).' '.strtoupper(utf8_encode($item_['apellidos']));  ?></div>
                                            <div class="item-time pull-right ">
                                            <span><?php echo $item_['calificacion'] ?></span>
                                            <i class="fa fa-check"></i>
                                            <span class="cerf_level"><?php echo $item_['nivel'] ?></span>
                                            </div></a>
                                        </div>
                                </div>
                            </li>
                        <?php
                        }
                    }
                    ?>
                </ul>
            </div>
    </div><!--END 3 -->
    
    
   <!-- <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" id="2">
        <div class="row">
            <div class="col-xs-12">
                <div class="dashboard-box">
                    <div class="box-header">
                        <div class="deadline">
                            <?php  
                            $DaysInMonth = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
                            $Percent = (date('d')/$DaysInMonth)*100;
                            ?>
                            
                        </div>
                    </div>
                    <div class="box-progress">
                        <style>
                        .dashboard-box .box-progress .progress-handle {
                            -lh-property: 0;
                            left: -webkit-calc(<?php //echo ceil($Percent); ?>% - 35px);
                            left: -moz-calc(<?php //echo ceil($Percent); ?>% - 35px);
                            left: calc(<?php //echo ceil($Percent); ?>% - 35px);
                        }
                        .dashboard-box .box-header {
                            padding: 31px;
                        }
                        .breadcrumb {
                            margin: 9px 30px 0 12px;
                        }
                        </style>
                        <div class="progress-handle">day <?php //echo date('d') ?></div>
                        <div class="progress progress-xs progress-no-radius bg-whitesmoke">
                            <div class="progress-bar" role="progressbar" aria-valuenow="<?php // echo ceil($Percent); ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ceil($Percent) ; ?>%">
                            </div>
                        </div>
                    </div>
                    <div class="box-tabbs">
                        <div class="tabbable">
                            <ul class="nav nav-tabs tabs-flat  nav-justified" id="myTab11">
                                <?php /*<li class="active">
                                <a data-toggle="tab" href="#realtime">
                                Real-Time
                                </a>
                                </li>*/ ?>
                                <li class="active">
                                    <a data-toggle="tab" href="#visits">Licenses acquired in the current month.</a>
                                </li>
                                <?php /*
                                <li>
                                <a data-toggle="tab" id="contacttab" href="#bandwidth">
                                Bandwidth
                                </a>
                                </li>
                                <li>
                                <a data-toggle="tab" href="#sales">
                                Sales
                                </a>
                                </li>*/ ?>
                            </ul>
                            <div class="tab-content tabs-flat no-padding">
                                <?php /*<div id="realtime" class="tab-pane active padding-5 animated fadeInUp">
                                <div class="row">
                                <div class="col-lg-12">
                                <div id="dashboard-chart-realtime" class="chart chart-lg no-margin"></div>
                                </div>
                                </div>
                                </div> */ ?>
                                <div id="visits" class="tab-pane active animated fadeInUp">
                                    <div class="row">
                                        <div class="col-lg-12 chart-container">
                                            <div id="dashboard-chart-visits" class="chart chart-lg no-margin" style="width:100%"></div>
                                        </div>
                                    </div>
                                </div>
                                <?php /*
                                <div id="bandwidth" class="tab-pane padding-10 animated fadeInUp">
                                    <div class="databox-sparkline bg-themeprimary">
                                        <span id="dashboard-bandwidth-chart" data-sparkline="compositeline" data-height="250px" data-width="100%" data-linecolor="#fff" data-secondlinecolor="#eee"
                                          data-fillcolor="rgba(255,255,255,.1)" data-secondfillcolor="rgba(255,255,255,.25)"
                                          data-spotradius="0"
                                          data-spotcolor="#fafafa" data-minspotcolor="#fafafa" data-maxspotcolor="#ffce55"
                                          data-highlightspotcolor="#fff" data-highlightlinecolor="#fff"
                                          data-linewidth="2" data-secondlinewidth="2"
                                          data-composite="500, 400, 100, 450, 300, 200, 100, 200">
                                            300,300,400,300,200,300,300,200
                                        </span>
                                    </div>
                                </div>
                                <div id="sales" class="tab-pane animated fadeInUp no-padding-bottom" style="padding:20px 20px 0 20px;">
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                            <div class="databox databox-xlg databox-vertical databox-inverted databox-shadowed">
                                                <div class="databox-top">
                                                    <div class="databox-sparkline">
                                                        <span data-sparkline="line" data-height="125px" data-width="100%" data-fillcolor="false" data-linecolor="themesecondary"
                                                         data-spotcolor="#fafafa" data-minspotcolor="#fafafa" data-maxspotcolor="#ffce55"
                                                          data-highlightspotcolor="#ffce55" data-highlightlinecolor="#ffce55"
                                                          data-linewidth="1.5" data-spotradius="2">
                                                            1,2,4,3,5,6,8,7,11,14,11,12
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="databox-bottom no-padding text-align-center">
                                                    <span class="databox-number lightcarbon no-margin">224</span>
                                                    <span class="databox-text lightcarbon no-margin">Sale Unit / Hour</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                            <div class="databox databox-xlg databox-vertical databox-inverted databox-shadowed">
                                                <div class="databox-top">
                                                    <div class="databox-sparkline">
                                                        <span data-sparkline="line" data-height="125px" data-width="100%" data-fillcolor="false" data-linecolor="themefourthcolor"
                                                          data-spotcolor="#fafafa" data-minspotcolor="#fafafa" data-maxspotcolor="#8cc474"
                                                          data-highlightspotcolor="#8cc474" data-highlightlinecolor="#8cc474"
                                                          data-linewidth="1.5" data-spotradius="2">
                                                            100,208,450,298,450,776,234,680,1100,1400,1000,1200
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="databox-bottom no-padding text-align-center">
                                                    <span class="databox-number lightcarbon no-margin">7063$</span>
                                                    <span class="databox-text lightcarbon no-margin">Income / Hour</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                            <div class="databox databox-xlg databox-vertical databox-inverted databox-shadowed">
                                                <div class="databox-top">
                                                    <div class="databox-piechart">
                                                        <div data-toggle="easypiechart" class="easyPieChart block-center"
                                                         data-barcolor="themeprimary" data-linecap="butt" data-percent="80" data-animate="500"
                                                         data-linewidth="8" data-size="125" data-trackcolor="#eee">
                                                            <span class="font-200"><i class="fa fa-gift themeprimary"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="databox-bottom no-padding text-align-center">
                                                    <span class="databox-number lightcarbon no-margin">9</span>
                                                    <span class="databox-text lightcarbon no-margin">NEW ORDERS</span>
                                                </div>
                                            </div>
                                        </div>
                                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                                <div class="databox databox-xlg databox-vertical databox-inverted  databox-shadowed">
                                                    <div class="databox-top">
                                                        <div class="databox-piechart">
                                                            <div data-toggle="easypiechart" class="easyPieChart block-center"
                                                             data-barcolor="themethirdcolor" data-linecap="butt" data-percent="40" data-animate="500"
                                                             data-linewidth="8" data-size="125" data-trackcolor="#eee">
                                                                <span class="white font-200"><i class="fa fa-tags themethirdcolor"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="databox-bottom no-padding text-align-center">
                                                    <span class="databox-number lightcarbon no-margin">11</span>
                                                    <span class="databox-text lightcarbon no-margin">NEW TICKETS</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> */ ?>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>--><!-- END 2 -->
</div>



    <script src="<?php echo $baseUrl; ?>/html/assets/js/charts/flot/jquery.flot.js"></script>

    <script src="<?php echo $baseUrl; ?>/html/assets/js/charts/flot/jquery.flot.pie.js"></script>
    
    <?php 
        //$SQL = "SELECT count(*) as total, month(t.fecha_presentacion) month FROM `exams_user` `t`, exams, licenses_user, licenses_client WHERE id_cliente='".Yii::app()->user->getState('cliente')."' and exams.id_examen = t.id_examen and id_licencias_usuario = id_licencia_usuario and licenses_user.id_licencia_cliente = licenses_client.id_licencias_cliente and year(t.fecha_presentacion)='".date('Y')."' GROUP BY month(t.fecha_presentacion)";
        $SQL ="SELECT count(l.id_licencias_usuario) as total, month(l.fecha_presentacion) as month FROM `licenses_user` `l`, licenses_client WHERE id_cliente='".Yii::app()->user->getState('cliente')."' and l.id_licencia_cliente = licenses_client.id_licencias_cliente and id_licencia IN (7,15) and year(l.fecha_presentacion)='".date('Y')."' and licenses_client.demo ='0' GROUP BY month(l.fecha_presentacion)";
        
        $list_grafica= $connection->createCommand($SQL)->queryAll();
       
           foreach($list_grafica as $key => $li){
                
                $Data_linea[] = '['.$li['month'].', '.$li['total'].']';    
            }
            
            $Datalinea  = "data: [";
            if($Data_linea){
                $Datalinea .= implode(',',$Data_linea);
            }
            $Datalinea .= "]"; 
            
    ?>

     <script>
     function labelFormatter(label, series) {
                    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
                }
     $(window).bind("load", function () {
         
         
          
               /*Sets Themed Colors Based on Themes*/
            themeprimary = '#9055CF'; //PURPLE
            themesecondary = '#a5a5a5';
            themethirdcolor = '#1363DC'; //BLUE
            themefourthcolor = '#EE5959'; //RED
            themefifthcolor = '#31B744';
            themesixthcolor = '#FFDA74';
            themeseventhcolor = '#7AD1EA';
            
            <?php 
                echo $Data;
            ?>
                

                var placeholder = $("#pie-chart");
                
                placeholder.unbind();

                $("#title").text("Custom Label Formatter");

                $.plot(placeholder, data, {
                    series: {
                        pie: {
                            show: true,
                            radius: 1,
                            label: {
                                show: true,
                                radius: 1,
                                formatter: labelFormatter,
                                background: {
                                    opacity: 0.8
                                }
                            }
                        }
                    },
                    legend: {
                        show: true
                    }, 
                    grid: {
                        hoverable: true,
                        clickable: true
                    }
                });
               
         
    
     
     
     
        
     
     
     
            var data = [{
                color: themeprimary,
                label: "Exams",
                <?php echo $Datalinea ?>
            }
            /*, {
                color: themethirdcolor,
                label: "Linux",
                data: [[1990, 10.0], [1991, 11.3], [1992, 9.9], [1993, 9.6], [1994, 9.5], [1995, 9.5], [1996, 9.9], [1997, 9.3], [1998, 9.2], [1999, 9.2], [2000, 9.5], [2001, 9.6], [2002, 9.3], [2003, 9.4], [2004, 9.79]]
            }
            , {
                color: themesecondary,
                label: "Mac OS",
                data: [[1990, 5.8], [1991, 6.0], [1992, 5.9], [1993, 5.5], [1994, 5.7], [1995, 5.3], [1996, 6.1], [1997, 5.4], [1998, 5.4], [1999, 5.1], [2000, 5.2], [2001, 5.4], [2002, 6.2], [2003, 5.9], [2004, 5.89]]
            }, {
                color: themefourthcolor,
                label: "DOS",
                data: [[1990, 8.3], [1991, 8.3], [1992, 7.8], [1993, 8.3], [1994, 8.4], [1995, 5.9], [1996, 6.4], [1997, 6.7], [1998, 6.9], [1999, 7.6], [2000, 7.4], [2001, 8.1], [2002, 12.5], [2003, 9.9], [2004, 19.0]]
            }*/
            ];

            var options = {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                legend: {
                    noColumns: 1
                },
                xaxis: {
                    tickDecimals: 0,
                    color: gridbordercolor
                },
                yaxis: {
                    min: 0,
                    color: gridbordercolor
                },
                selection: {
                    mode: "x"
                },
                grid: {
                    hoverable: true,
                    clickable: false,
                    borderWidth: 0,
                    aboveData: false
                },
                tooltip: true,
                tooltipOpts: {
                    defaultTheme: false,
                    content: "<b>%s</b> : <span>%x</span> : <span>%y</span>",
                },
                crosshair: {
                    mode: "x"
                }
            };

            var placeholder = $("#selectable-chart");

            placeholder.bind("plotselected", function (event, ranges) {

                var zoom = $("#zoom").is(":checked");

                if (zoom) {
                    plot = $.plot(placeholder, data, $.extend(true, {}, options, {
                        xaxis: {
                            min: ranges.xaxis.from,
                            max: ranges.xaxis.to
                        }
                    }));
                }
            });

            placeholder.bind("plotunselected", function (event) {
                // Do Some Work
            });

            var plot = $.plot(placeholder, data, options);

            $("#clearSelection").click(function () {
                plot.clearSelection();
            });

            $("#setSelection").click(function () {
                plot.setSelection({
                    xaxis: {
                        from: 1,
                        to: 12
                    }
                });
            });
            
            
 });
 
 // EDE
 
  $(window).bind("load", function () {
         
         
          
               /*Sets Themed Colors Based on Themes*/
            themeprimary = '#9055CF';
            themesecondary = '#a5a5a5';
            themethirdcolor = '#1363DC';
            themefourthcolor = '#EE5959';
            themefifthcolor = '#31B744';
            themesixthcolor = '#FFDA74';
            themeseventhcolor = '#7AD1EA';
            
            <?php 
                echo $DataE;
            ?>
                

                var placeholder2 = $("#pie-chart2");
                
                placeholder2.unbind();

                $("#title").text("Custom Label Formatter");

                $.plot(placeholder2, data, {
                    series: {
                        pie: {
                            show: true,
                            radius: 1,
                            label: {
                                show: true,
                                radius: 1,
                                formatter: labelFormatter,
                                background: {
                                    opacity: 0.8
                                }
                            }
                        }
                    },
                    legend: {
                        show: true
                    }, 
                    grid: {
                        hoverable: true,
                        clickable: true
                    }
                });
               

     
            var data = [{
                color: themeprimary,
                label: "Exams",
                <?php echo $Datalinea ?>
            }
            /*, {
                color: themethirdcolor,
                label: "Linux",
                data: [[1990, 10.0], [1991, 11.3], [1992, 9.9], [1993, 9.6], [1994, 9.5], [1995, 9.5], [1996, 9.9], [1997, 9.3], [1998, 9.2], [1999, 9.2], [2000, 9.5], [2001, 9.6], [2002, 9.3], [2003, 9.4], [2004, 9.79]]
            }
            , {
                color: themesecondary,
                label: "Mac OS",
                data: [[1990, 5.8], [1991, 6.0], [1992, 5.9], [1993, 5.5], [1994, 5.7], [1995, 5.3], [1996, 6.1], [1997, 5.4], [1998, 5.4], [1999, 5.1], [2000, 5.2], [2001, 5.4], [2002, 6.2], [2003, 5.9], [2004, 5.89]]
            }, {
                color: themefourthcolor,
                label: "DOS",
                data: [[1990, 8.3], [1991, 8.3], [1992, 7.8], [1993, 8.3], [1994, 8.4], [1995, 5.9], [1996, 6.4], [1997, 6.7], [1998, 6.9], [1999, 7.6], [2000, 7.4], [2001, 8.1], [2002, 12.5], [2003, 9.9], [2004, 19.0]]
            }*/
            ];

            var options = {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                legend: {
                    noColumns: 1
                },
                xaxis: {
                    tickDecimals: 0,
                    color: gridbordercolor
                },
                yaxis: {
                    min: 0,
                    color: gridbordercolor
                },
                selection: {
                    mode: "x"
                },
                grid: {
                    hoverable: true,
                    clickable: false,
                    borderWidth: 0,
                    aboveData: false
                },
                tooltip: true,
                tooltipOpts: {
                    defaultTheme: false,
                    content: "<b>%s</b> : <span>%x</span> : <span>%y</span>",
                },
                crosshair: {
                    mode: "x"
                }
            };

            var placeholder2 = $("#selectable-chart");

            placeholder2.bind("plotselected", function (event, ranges) {

                var zoom = $("#zoom").is(":checked");

                if (zoom) {
                    plot = $.plot(placeholder2, data, $.extend(true, {}, options, {
                        xaxis: {
                            min: ranges.xaxis.from,
                            max: ranges.xaxis.to
                        }
                    }));
                }
            });

            placeholder2.bind("plotunselected", function (event) {
                // Do Some Work
            });

            var plot = $.plot(placeholder2, data, options);

            $("#clearSelection").click(function () {
                plot.clearSelection();
            });

            $("#setSelection").click(function () {
                plot.setSelection({
                    xaxis: {
                        from: 1,
                        to: 12
                    }
                });
            });
            
            
 });
 
     </script>

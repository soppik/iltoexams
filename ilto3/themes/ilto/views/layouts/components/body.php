    <style>
        a.login-area.dropdown-toggle {
            width: 215px;
        }
        .navbar .navbar-inner .navbar-header .navbar-account .account-area .login-area+.dropdown-menu {
            width: 100%;
            min-width: 100px;
        }
        li.clase0.open.opciones.first.col0 {
            color: black;
            font-weight: bold;
        }
        li.clase0.open.opciones.first.col0 a{
            color: white!important;
                box-shadow: 3px 4px 4px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
            border-radius: 15px;
        }
        
        .w3-btn, .w3-btn:link, .w3-btn:visited {
   
    box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
}
        
        .flash-error {
            border-color: #df5138;
            background: #e46f61;
            margin-bottom: 20px;
            margin-top: 0;
            color: #fff;
            border-width: 0;
            border-left-width: 5px;
            padding: 10px;
            border-radius: 0;
        }
        
        .navbar .navbar-inner {
            background: #1363DC;
        }
        .navbar .navbar-inner .login-area {
            background-color: #1e8ce0;
        }
        
        .claseXX.last.col1 {
            display:block;    
        }
        
        .go_back {
            cursor: pointer;
            vertical-align: middle;
            margin: 0;
            position: relative;
            display: inline-block;
            color: #fff;
            border:none;
            border-radius: 2px;
            background-clip: padding-box;
            font-size: 13px;
            background-color:#8E54CC;
        }
        
    </style>
    <!-- Loading Container -->
    <div class="loading-container">
        <div class="loader"></div>
    </div>
    <!--  /Loading Container -->
    <!-- Navbar -->
    <?php 
    if($_GET["r"]!="Clients/default/changePass"){
        include 'navbar.php';
    }
    ?>
    <!-- /Navbar -->
    <!-- Main Container -->
    <div class="main-container container-fluid">
        <!-- Page Container -->
        <div class="page-container">
            <!-- Page Sidebar -->
            <?php 
            if($_GET["r"]!="Clients/default/changePass"){
                include 'sidebar.php';
            }
            ?>
            <!-- /Page Sidebar -->
            <!-- Chat Bar -->
            <?php 
                include 'chatbar.php';
            ?>
            <!-- /Chat Bar -->
            <!-- Page Content -->
            
            <div class="page-content">

              <?php //require_once('tpl_navigation.php')
              if($_GET["r"]!="Clients/default/changePass"){
               
              ?>
            <!-- Page Breadcrumb -->
            
                <div class="page-breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="/ilto3/index.php?r=Clients/">Home</a>
                        </li>
                        <li class="active">Dashboard</li>
                    </ul>
                </div>
                <!-- /Page Breadcrumb -->
                <!-- Page Header -->
                <div class="page-header position-relative">
                    <div class="header-title">
                        <h1>
                            Dashboard
                        </h1>
                    </div>
                    <?php 
                        if($_GET["r"]!="Clients/ExamsUser/admin"){
                            echo '<button class="go_back" onclick="goBack()">Back</button>';
                        }
                    ?>
                    
                    <!--Header Buttons-->
                    <div class="header-buttons">
                        <a class="sidebar-toggler" href="#">
                            <i class="fa fa-arrows-h"></i>
                        </a>
                        <a class="refresh" id="refresh-toggler" href="">
                            <i class="glyphicon glyphicon-refresh"></i>
                        </a>
                        <a class="fullscreen" id="fullscreen-toggler" href="#">
                            <i class="glyphicon glyphicon-fullscreen"></i>
                        </a>
                    </div>
                    <!--Header Buttons End-->
                </div>
                <?php
                }
                ?>
                <!-- /Page Header -->
                <!-- Page Body -->
                <div class="page-body">
                       <?php 
             
                            if(Yii::app()->controller->id == 'default' && Yii::app()->controller->action->id == 'index'){
                                ?>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="row">
                                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                                  <?php echo $content; ?>
                                            </div>
                                             <?php include 'modules/header-boxes.php'?>
                                        </div>
                                    </div>
                                </div>
                                
                               
                                <?php include 'modules/box-chart.php'?>
                                <?php
                            }else{
                                ?>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                  <?php echo $content; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                       ?>
                       
                    
                </div>
                <!-- /Page Body -->
                <?php //require_once('tpl_footer.php')?>
            </div>
            
            <!-- /Page Content -->
        </div>
        <!-- /Page Container -->
        <!-- Main Container -->
    </div>
 
  
    <script src="<?php echo $siteUrl; ?>/assets/js/slimscroll/jquery.slimscroll.min.js"></script>

    <!--Beyond Scripts-->
    <script src="<?php echo $siteUrl; ?>/assets/js/beyond.min.js"></script>
    
    <link href="<?php echo $baseUrl; ?>/source/jquery.fancybox.css?v=2.1.5" rel="stylesheet" />
    <script src="<?php echo $baseUrl; ?>/source/jquery.fancybox.pack.js"></script>
    
    
    <!--Page Related Scripts-->
    <!--Sparkline Charts Needed Scripts-->
    <script src="<?php echo $baseUrl; ?>/html/assets/js/charts/sparkline/jquery.sparkline.js"></script>
    <script src="<?php echo $baseUrl; ?>/html/assets/js/charts/sparkline/sparkline-init.js"></script>

    <!--Easy Pie Charts Needed Scripts-->
    <script src="<?php echo $baseUrl; ?>/html/assets/js/charts/easypiechart/jquery.easypiechart.js"></script>
    <script src="<?php echo $baseUrl; ?>/html/assets/js/charts/easypiechart/easypiechart-init.js"></script>

    <!--Flot Charts Needed Scripts-->
    <script src="<?php echo $baseUrl; ?>/html/assets/js/charts/flot/jquery.flot.js"></script>
    <script src="<?php echo $baseUrl; ?>/html/assets/js/charts/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo $baseUrl; ?>/html/assets/js/charts/flot/jquery.flot.pie.js"></script>
    <script src="<?php echo $baseUrl; ?>/html/assets/js/charts/flot/jquery.flot.tooltip.js"></script>
    <script src="<?php echo $baseUrl; ?>/html/assets/js/charts/flot/jquery.flot.orderBars.js"></script>
    
    <!--Page Related Scripts-->
    <script>
        jQuery(document).ready(function(){
            jQuery(document).on('keyup change', '.search',function(){
               jQuery.post(jQuery(this).parents('.search-form').attr('action'), jQuery('.search-form').serialize(), function(data){
                    jQuery('.search-rows').html(data.html);
               });
            
            })            
        })

        
    </script>
    <?php
        if(Yii::app()->controller->id == 'default' && Yii::app()->controller->action->id == 'index'){
            $SQL = "SELECT sum(cantidad) as cantidad, fecha_asignacion FROM `licenses_client` where id_cliente = '".$userId."'  and (fecha_asignacion BETWEEN '".date('Y')."-".date('m')."-01' AND '".date('Y')."-".date('m')."-31') group by DATE_FORMAT(fecha_asignacion, '%Y-%m-%d')";
     
            $list= Yii::app()->db->createCommand($SQL)->queryAll();
    
            $Items = array();
            foreach($list as $value){
                $Date = explode('-',$value['fecha_asignacion']);
                $day = end($Date);
                $day_ = explode(' ',$day);
                $day_ = reset($day_);
                $Items[] = '['.$day_.', '.$value['cantidad'].']';
            }
        }

    ?>
    
    <script type="text/javascript">
        jQuery(document).ready(function(){
              var aitem = jQuery('.clase0.col1').find('a.menu-dropdown')
              var htmla = aitem.html();
              aitem.html('<i class="menu-icon fa fa-table" ></i><span class="menu-text">'+htmla+'</span><i class="menu-expand"></i>');
              
              var aitem = jQuery('.clase0.col2').find('a.menu-dropdown')
              var htmla = aitem.html();
              aitem.html('<i class="menu-icon fa fa-users" ></i><span class="menu-text">'+htmla+'</span><i class="menu-expand"></i>');
              
              var aitem = jQuery('.clase0.col3').find('a.menu-dropdown')
              var htmla = aitem.html();
              aitem.html('<i class="menu-icon fa fa-user-secret " ></i><span class="menu-text">'+htmla+'</span><i class="menu-expand"></i>');
              
              var aitem = jQuery('.clase0.col4').find('a.menu-dropdown')
              var htmla = aitem.html();
              aitem.html('<i class="menu-icon fa fa-pencil-square-o" ></i><span class="menu-text">'+htmla+'</span><i class="menu-expand"></i>');
        });
        
        <?php
        if(Yii::app()->controller->id == 'default' && Yii::app()->controller->action->id == 'index'){
            ?>
        $(window).bind("load", function () {

           
            themeprimary = getThemeColorFromCss('themeprimary');
            themesecondary = getThemeColorFromCss('themesecondary');
            themethirdcolor = getThemeColorFromCss('themethirdcolor');
            themefourthcolor = getThemeColorFromCss('themefourthcolor');
            themefifthcolor = getThemeColorFromCss('themefifthcolor');

            //Sets The Hidden Chart Width
            $('#dashboard-bandwidth-chart')
                .data('width', $('.box-tabbs')
                    .width() - 20);

            //-------------------------Visitor Sources Pie Chart----------------------------------------//
            var data = [
                {
                    data: [[1, 21]],
                    color: '#fb6e52'
                },
                {
                    data: [[1, 12]],
                    color: '#e75b8d'
                },
                {
                    data: [[1, 11]],
                    color: '#a0d468'
                },
                {
                    data: [[1, 10]],
                    color: '#ffce55'
                },
                {
                    data: [[1, 46]],
                    color: '#5db2ff'
                }
            ];
            var placeholder = $("#dashboard-pie-chart-sources");
            placeholder.unbind();

        

            //------------------------------Visit Chart------------------------------------------------//
            var data2 = [{
                    color: themeprimary,
                    label: "Licenses Purchased",
                    data: [ <?php echo implode(', ',$Items); ?> ],
                    bars: {
                        order: 1,
                        show: true,
                        borderWidth: 0,
                        barWidth: 0.4,
                        lineWidth: .5,
                        fillColor: {
                            colors: [{
                                opacity: 0.4
                            }, {
                                opacity: 1
                            }]
                        }
                    }
                }
            ];
            var options = {
                legend: {
                    show: false
                },
                xaxis: {
                    tickDecimals: 0,
                    color: '#f3f3f3'
                },
                yaxis: {
                    min: 0,
                    color: '#f3f3f3',
                    tickFormatter: function (val, axis) {
                        return "";
                    },
                },
                grid: {
                    hoverable: true,
                    clickable: false,
                    borderWidth: 0,
                    aboveData: false,
                    color: '#fbfbfb'

                },
                tooltip: true,
                tooltipOpts: {
                    defaultTheme: false,
                    content: " <b>%x May</b> , <b>%s</b> : <span>%y</span>",
                }
            };
            var placeholder = $("#dashboard-chart-visits");
            var plot = $.plot(placeholder, data2, options);

            //------------------------------Real-Time Chart-------------------------------------------//
            var realTimedata = [],
                realTimedata2 = [],
                totalPoints = 300;

            var getSeriesObj = function () {
                return [
                {
                    data: getRandomData(),
                    lines: {
                        show: true,
                        lineWidth: 1,
                        fill: true,
                        fillColor: {
                            colors: [
                                {
                                    opacity: 0
                                }, {
                                    opacity: 1
                                }
                            ]
                        },
                        steps: false
                    },
                    shadowSize: 0
                }, {
                    data: getRandomData2(),
                    lines: {
                        lineWidth: 0,
                        fill: true,
                        fillColor: {
                            colors: [
                                {
                                    opacity: .5
                                }, {
                                    opacity: 1
                                }
                            ]
                        },
                        steps: false
                    },
                    shadowSize: 0
                }
                ];
            };
            //-------------------------Initiates Easy Pie Chart instances in page--------------------//
            InitiateEasyPieChart.init();

            //-------------------------Initiates Sparkline Chart instances in page------------------//
            InitiateSparklineCharts.init();
        });
        <?php } ?>
    </script>
    <script>
function goBack() {
    window.history.back();
}
</script>
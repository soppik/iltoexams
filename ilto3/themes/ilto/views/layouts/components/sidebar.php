
<div class="page-sidebar" id="sidebar">
    <!-- Page Sidebar Header-->

    <style>
        .sp {
            font-weight:800;
        }
        
        .btn-main {
            font-size:14px;
            font-weight:bolder;
            border-radius: 5px !important;
        }
        
        .col2 a{
            color: #ee5959 !important;font-weight:bold;
        }
    </style>

    <div class="sidebar-header-wrapper">
       <b>MAIN MENU</b>
    </div>
    <!-- /Page Sidebar Header -->
    
    
    <?php
                $permiso = array(0,0,0,0,0);
                $modelperfil=  Profiles::model()->findByPk(Yii::app()->user->getState('id_perfil'));
                $cliente = Profiles::model()->findByPk(Yii::app()->user->getState('id_cliente'));
                if(is_null ($modelperfil)){
                        $perfilestandar=''; 
                }else{
                        $perfilestandar=$modelperfil->configuracion;
                }
                
            ?>



			<?php 

					$configact=explode(',',$perfilestandar);

					$arreglo = array();


					$criteria = new CDbCriteria();
	
					$criteria->addCondition("nodo=0","estado='A'");
	
					$model= Menu::model()->findAll($criteria);
	                foreach ($this->menu as $value) {
			            $items=array('label'=>$value["label"],'url'=>array($value['url'][0]), 'itemOptions'=>array('class'=>'clase0 open opciones'));
				        array_push($arreglo, $items);
				    }
					foreach($model as $cero){
                                            if($cero->url=='#'){
                                               $criteria = new CDbCriteria();
                                               $criteria->addCondition("nodo=".$cero->opcion);
                                               $modelodentro = Menu::model()->findAll($criteria);
                                               $opcionuno = array();
                                               foreach($modelodentro as $interno){
                                                    $interno->nombre = mb_convert_encoding($interno->nombre, 'HTML-ENTITIES', "UTF-8"); 
                                                    $cero->nombre = mb_convert_encoding($cero->nombre, 'HTML-ENTITIES', "UTF-8"); 
                                                    
                                                   if($interno->url=='#'){
                                                        $criteria2 = new CDbCriteria();
                                                        $criteria2->addCondition("nodo=".$interno->opcion);
                                                        $modelodentro2 = Menu::model()->findAll($criteria2);
                                                        $opciontres = array();
                                                        foreach($modelodentro2 as $interno2){
                                                            
                                                            $interno2->nombre = mb_convert_encoding($interno2->nombre, 'HTML-ENTITIES', "UTF-8"); 
                                                            $config2=$interno2->nodo.'-'.$interno2->opcion;
                                                            $estadointerno2=0;
                                                            foreach($configact as $pareja){
                                                                if($pareja==$config2){
                                                                    $estadointerno2=1;					
                                                                }
                                                           }
                                                            $arreglointerno2 = array('label'=>$interno2->nombre, 'url'=>Yii::app()->createUrl($interno2->url),'visible'=>$estadointerno2, 'itemOptions'=>array('class'=>'clase2'), 'submenuOptions'=>array('class'=>'submenu', 'style'=>"display: block;"), 'linkOptions'=>array('class'=>'menu-dropdown') );
                                                            array_push($opciontres,$arreglointerno2);
                                                        }
                                                       $config=$interno->nodo.'-'.$interno->opcion;
                                                       $estadogeneral=0;
                                                       foreach($configact as $pareja){
                                                            if($pareja==$config){
                                                                $estadogeneral=1;					
                                                            }
                                                       }
                                                       $arreglointerno = array('label'=>$interno->nombre, 'url'=>$interno->url, 'visible'=>$estadogeneral,'items'=>$opciontres, 'itemOptions'=>array('class'=>'clase1'), 'submenuOptions'=>array('class'=>'submenu', 'style'=>"display: block;"), 'linkOptions'=>array('class'=>'menu-dropdown') );
                                                   } else {
                                                        $config=$interno->nodo.'-'.$interno->opcion;
                                                        $estadointerno=0;
                                                        foreach($configact as $pareja){
                                                            if($pareja==$config){
                                                                $estadointerno=1;					
                                                            }
                                                        }
                                                      
                                                        $arreglointerno = array('label'=>strip_tags($interno->nombre), 'url'=>Yii::app()->createUrl($interno->url),'visible'=>$estadointerno, 'itemOptions'=>array('class'=>'claseXX'), 'submenuOptions'=>array('class'=>'submenu', 'style'=>"display: block;")  );
                                                   } 
                                                   array_push($opcionuno,$arreglointerno);
                                               }
                                              $config=$cero->nodo.'-'.$cero->opcion;
                                              $estadogeneral=0;
                                              foreach($configact as $pareja){
                                                   if($pareja==$config){
                                                       $estadogeneral=1;					
                                                   }
                                              }
                                              $opcioncero = array('label'=>$cero->nombre, 'url'=>$cero->url, 'visible'=>$estadogeneral,'items'=>$opcionuno, 'itemOptions'=>array('class'=>'clase0 open '), 'submenuOptions'=>array('class'=>'submenu', 'style'=>"display: block;"), 'linkOptions'=>array('class'=>'menu-dropdown') );
                                            } else {
                                               $config=$cero->nodo.'-'.$cero->opcion;
                                               $estadogeneral=0;
                                               foreach($configact as $pareja){
                                                       if($pareja==$config){
                                                           $estadogeneral=1;					
                                                        }
                                               }
                                               $opcioncero = array('label'=>$cero->nombre, 'url'=>array($cero->url), 'visible'=>$estadogeneral, 'itemOptions'=>array('class'=>'clase6'), 'submenuOptions'=>array('class'=>'submenu', 'style'=>"display: none;"), 'linkOptions'=>array('class'=>'menu-dropdown') );
                                            }        
                                            array_push($arreglo,$opcioncero);
					}
					
                    //array_push($arreglo, $items);
                    
					$ingresar = array('label'=>'LogIn', 'url'=>array('default/login'), 'visible'=>Yii::app()->user->isGuest);
					$salir = array('label'=>'LogOut ('.Yii::app()->user->name.')', 'url'=>array('/site/logout&id='.Yii::app()->user->name), 'visible'=>!Yii::app()->user->isGuest, 'class'=>'more');
					array_push($arreglo,$ingresar);
					array_push($arreglo,$salir);
//					$this->widget('zii.widgets.CMenu',array(
					$this->widget('ext.emenu.EMenu',array(
                    'items'=>$arreglo,
                                            'theme'=>'new.ilto',
                )); ?>

<script>
    jQuery(document).ready(function(){
         jQuery('.clase0.opciones.first.col0').css({'padding':10})
        jQuery('.clase0.opciones.first.col0 a').addClass('btn btn-success btn-main')
    
        
        var x = document.getElementsByClassName("claseXX last col1");
        var i;
        for (i = 0; i < x.length; i++) {
            x[i].style.backgroundColor = "#ffda74";
            x[i].style.color = "#fff!important";
            
        }
    })
    
    
</script>
            
    <div class="col-lg-12 col-md-12 col-sm-12 text-center" style="margin-top: 50px;position: relative;z-index: 999999;">
                            <?php 
                                $modCliente = Clients::model()->findByPk(Yii::app()->user->getState('cliente'));
                                if(!empty($modCliente->ruta_logo)){
                                    echo '<img src="/ilto3/images/clients/tn2_'.$modCliente->ruta_logo.'" alt="" class="header-avatar" style="width:100%">';
                                }
                            ?>
                            
                            

                        </div>

<?php /*
    <!-- Sidebar Menu -->
    <ul class="nav sidebar-menu">
        <!--Dashboard-->
        <li>
            <a href="index.html">
                <i class="menu-icon glyphicon glyphicon-home"></i>
                <span class="menu-text"> Dashboard </span>
            </a>
        </li>
        <!--Databoxes-->
        <li>
            <a href="databoxes.html">
                <i class="menu-icon glyphicon glyphicon-tasks"></i>
                <span class="menu-text"> Data Boxes </span>
            </a>
        </li>
        <!--Widgets-->
        <li>
            <a href="widgets.html">
                <i class="menu-icon fa fa-th"></i>
                <span class="menu-text"> Widgets </span>
            </a>
        </li>
        <!--UI Elements-->
        <li>
            <a href="#" class="menu-dropdown">
                <i class="menu-icon fa fa-desktop"></i>
                <span class="menu-text"> Elements </span>

                <i class="menu-expand"></i>
            </a>

            <ul class="submenu">
                <li>
                    <a href="elements.html">
                        <span class="menu-text">Basic Elements</span>
                    </a>
                </li>
                <li>
                    <a href="#" class="menu-dropdown">
                        <span class="menu-text">
                            Icons
                        </span>
                        <i class="menu-expand"></i>
                    </a>

                    <ul class="submenu">
                        <li>
                            <a href="font-awesome.html">
                                <i class="menu-icon fa fa-rocket"></i>
                                <span class="menu-text">Font Awesome</span>
                            </a>
                        </li>
                        <li>
                            <a href="glyph-icons.html">
                                <i class="menu-icon glyphicon glyphicon-stats"></i>
                                <span class="menu-text">Glyph Icons</span>
                            </a>
                        </li>
                        <li>
                            <a href="typicon.html">
                                <i class="menu-icon typcn typcn-location-outline"></i>
                                <span class="menu-text"> Typicons</span>
                            </a>
                        </li>
                        <li>
                            <a href="weather-icons.html">
                                <i class="menu-icon wi wi-hot"></i>
                                <span class="menu-text">Weather Icons</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="tabs.html">
                        <span class="menu-text">Tabs & Accordions</span>
                    </a>
                </li>
                <li>
                    <a href="alerts.html">
                        <span class="menu-text">Alerts & Tooltips</span>
                    </a>
                </li>
                <li>
                    <a href="modals.html">
                        <span class="menu-text">Modals & Wells</span>
                    </a>
                </li>
                <li>
                    <a href="buttons.html">
                        <span class="menu-text">Buttons</span>
                    </a>
                </li>
                <li>
                    <a href="nestable-list.html">
                        <span class="menu-text"> Nestable List</span>
                    </a>
                </li>
                <li>
                    <a href="treeview.html">
                        <span class="menu-text">Treeview</span>
                    </a>
                </li>
            </ul>
        </li>
        <!--Tables-->
        <li>
            <a href="#" class="menu-dropdown">
                <i class="menu-icon fa fa-table"></i>
                <span class="menu-text"> Tables </span>

                <i class="menu-expand"></i>
            </a>

            <ul class="submenu">
                <li>
                    <a href="tables-simple.html">
                        <span class="menu-text">Simple & Responsive</span>
                    </a>
                </li>
                <li>
                    <a href="tables-data.html">
                        <span class="menu-text">Data Tables</span>
                    </a>
                </li>
            </ul>
        </li>
        <!--Forms-->
        <li class="active open">
            <a href="#" class="menu-dropdown">
                <i class="menu-icon fa fa-pencil-square-o"></i>
                <span class="menu-text"> Forms </span>

                <i class="menu-expand"></i>
            </a>

            <ul class="submenu">
                <li class="active">
                    <a href="form-layouts.html">
                        <span class="menu-text">Form Layouts</span>
                    </a>
                </li>

                <li>
                    <a href="form-inputs.html">
                        <span class="menu-text">Form Inputs</span>
                    </a>
                </li>

                <li>
                    <a href="form-pickers.html">
                        <span class="menu-text">Data Pickers</span>
                    </a>
                </li>
                <li>
                    <a href="form-wizard.html">
                        <span class="menu-text">Wizard</span>
                    </a>
                </li>
                <li>
                    <a href="form-validation.html">
                        <span class="menu-text">Validation</span>
                    </a>
                </li>
                <li>
                    <a href="form-editors.html">
                        <span class="menu-text">Editors</span>
                    </a>
                </li>
                <li>
                    <a href="form-inputmask.html">
                        <span class="menu-text">Input Mask</span>
                    </a>
                </li>
            </ul>
        </li>
        <!--Charts-->
        <li>
            <a href="#" class="menu-dropdown">
                <i class="menu-icon fa fa-bar-chart-o"></i>
                <span class="menu-text"> Charts </span>

                <i class="menu-expand"></i>
            </a>

            <ul class="submenu">
                <li>
                    <a href="flot.html">
                        <span class="menu-text">Flot Charts</span>
                    </a>
                </li>

                <li>
                    <a href="morris.html">
                        <span class="menu-text"> Morris Charts</span>
                    </a>
                </li>
                <li>
                    <a href="sparkline.html">
                        <span class="menu-text">Sparkline Charts</span>
                    </a>
                </li>
                <li>
                    <a href="easypiecharts.html">
                        <span class="menu-text">Easy Pie Charts</span>
                    </a>
                </li>
                <li>
                    <a href="chartjs.html">
                        <span class="menu-text"> ChartJS</span>
                    </a>
                </li>
            </ul>
        </li>
        <!--Profile-->
        <li>
            <a href="profile.html">
                <i class="menu-icon fa fa-picture-o"></i>
                <span class="menu-text">Profile</span>
            </a>
        </li>
        <!--Mail-->
        <li>
            <a href="#" class="menu-dropdown">
                <i class="menu-icon fa fa-envelope-o"></i>
                <span class="menu-text"> Mail </span>

                <i class="menu-expand"></i>
            </a>

            <ul class="submenu">
                <li>
                    <a href="inbox.html">
                        <span class="menu-text">Inbox</span>
                    </a>
                </li>

                <li>
                    <a href="message-view.html">
                        <span class="menu-text">View Message</span>
                    </a>
                </li>
                <li>
                    <a href="message-compose.html">
                        <span class="menu-text">Compose Message</span>
                    </a>
                </li>
            </ul>
        </li>
        <!--Calendar-->
        <li>
            <a href="calendar.html">
                <i class="menu-icon fa fa-calendar"></i>
                <span class="menu-text">
                    Calendar
                </span>
            </a>
        </li>
        <!--Pages-->
        <li>
            <a href="#" class="menu-dropdown">
                <i class="menu-icon glyphicon glyphicon-paperclip"></i>
                <span class="menu-text"> Pages </span>

                <i class="menu-expand"></i>
            </a>
            <ul class="submenu">
                <li>
                    <a href="timeline.html">
                        <span class="menu-text">Timeline</span>
                    </a>
                </li>
                <li>
                    <a href="pricing.html">
                        <span class="menu-text">Pricing Tables</span>
                    </a>
                </li>

                <li>
                    <a href="invoice.html">
                        <span class="menu-text">Invoice</span>
                    </a>
                </li>

                <li>
                    <a href="login.html">
                        <span class="menu-text">Login</span>
                    </a>
                </li>
                <li>
                    <a href="register.html">
                        <span class="menu-text">Register</span>
                    </a>
                </li>
                <li>
                    <a href="lock.html">
                        <span class="menu-text">Lock Screen</span>
                    </a>
                </li>
                <li>
                    <a href="typography.html">
                        <span class="menu-text"> Typography </span>
                    </a>
                </li>
            </ul>
        </li>
        <!--More Pages-->
        <li>
            <a href="#" class="menu-dropdown">
                <i class="menu-icon glyphicon glyphicon-link"></i>

                <span class="menu-text">
                    More Pages
                </span>

                <i class="menu-expand"></i>
            </a>

            <ul class="submenu">
                <li>
                    <a href="error-404.html">
                        <span class="menu-text">Error 404</span>
                    </a>
                </li>

                <li>
                    <a href="error-500.html">
                        <span class="menu-text"> Error 500</span>
                    </a>
                </li>
                <li>
                    <a href="blank.html">
                        <span class="menu-text">Blank Page</span>
                    </a>
                </li>
                <li>
                    <a href="grid.html">
                        <span class="menu-text"> Grid</span>
                    </a>
                </li>
                <li>
                    <a href="#" class="menu-dropdown">
                        <span class="menu-text">
                            Multi Level Menu
                        </span>
                        <i class="menu-expand"></i>
                    </a>

                    <ul class="submenu">
                        <li>
                            <a href="#">
                                <i class="menu-icon fa fa-camera"></i>
                                <span class="menu-text">Level 3</span>
                            </a>
                        </li>

                        <li>
                            <a href="#" class="menu-dropdown">
                                <i class="menu-icon fa fa-asterisk"></i>

                                <span class="menu-text">
                                    Level 4
                                </span>
                                <i class="menu-expand"></i>
                            </a>

                            <ul class="submenu">
                                <li>
                                    <a href="#">
                                        <i class="menu-icon fa fa-bolt"></i>
                                        <span class="menu-text">Some Item</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <i class="menu-icon fa fa-bug"></i>
                                        <span class="menu-text">Another Item</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <!--Right to Left-->
        <li>
            <a href="#" class="menu-dropdown">
                <i class="menu-icon fa fa-align-right"></i>
                <span class="menu-text"> Right to Left </span>

                <i class="menu-expand"></i>
            </a>
            <ul class="submenu">
                <li>
                    <a>
                        <span class="menu-text">RTL</span>
                        <label class="pull-right margin-top-10">
                            <input id="rtl-changer" class="checkbox-slider slider-icon colored-primary" type="checkbox">
                            <span class="text"></span>
                        </label>
                    </a>
                </li>
                <li>
                    <a href="index-rtl-ar.html">
                        <span class="menu-text">Arabic Layout</span>
                    </a>
                </li>

                <li>
                    <a href="index-rtl-fa.html">
                        <span class="menu-text">Persian Layout</span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="versions.html">
                <i class="menu-icon glyphicon glyphicon-fire themesecondary"></i>
                <span class="menu-text">
                    BeyondAdmin Versions
                </span>
            </a>
        </li>
    </ul> */ ?>
    <!-- /Sidebar Menu -->
</div>

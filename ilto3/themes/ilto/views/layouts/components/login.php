<?php

// Id de la ruta para el login
if(isset($_POST["flag"])){
    $flag = $_POST["flag"];
}else{
    $flag = 0;
}

if(isset($_POST["flag_econfirm"])){
    $flag_econfirm = $_POST["flag_econfirm"];
}else{
    $flag_econfirm = 0;
}


$message = $_GET["message"];

if(!is_null($message)){
    echo '
    <style> 
        .logobox {
            border-color: #df5138 !important;
            background: #e46f61 !important;
            padding:5px !important;
            color: #fff;
            min-height:50px !important;
            height: auto !important;
            text-align:center !important;
        }
        
        #alertR_lbl {
            color: red;
        }
        #alertG_lbl {
            color: ##53a83f;
        }
    </style>';
}

if($flag_econfirm==1){
    echo '
    <style> 
        .logobox {
            border-color: #1d6126 !important;
            background: #3ab54b !important;
            padding:5px !important;
            color: #fff;
            min-height:50px !important;
            height: auto !important;
            text-align:center !important;
        }
    </style>';
} 

switch ($message) {
    case 1:
        $text = "The ID number does not exist.";
    break;
    case 2:
         $text = "Incorrect username or password.";
    break;
    case 3:
        $text = "You have one pending test. Enter your username and password to complete the test.";
    break;
    case 4:
        $text = "You have not done the speaking section of the test. Contact the test administrator.";
    break;
    case 5:
        $text = "You have already finished the EDE. Contact your test administrator.";
    break;
    case 6:
        $text = "The taker has been blocked, due to suspicious fraud actions. Contact info@iltoexams.com for more information.";
    break;
    case 7:
        $text = "Se finalizó la licencia EDE pendiente por terminar o presentar para poder ejecutar la licencia TECS, por favor, ingrese de nuevo los datos del taker para comenzar la licencia TECS.";
    break;
    case 8:
        $text = "Your password has been changed successfully.  Please check your email to confirm the update.";
    break;
    case 9:
        $text = "Please check your email to confirm the update.";
    break;
    case 10:
        $text = "You are not a rater, a fraund report will be send to Iltoexams.";
    break;
    case 11:
        $text = "This user is already online.";
    break;
    case 12:
        $text = "Access Denied.";
    break;
    case 13:
        $text = "TECS license is not allowed.";
    break;
    case 14:
        $text = "EDE license is not allowed.";
    break;
    default:
         $text = "Username or ID does not exist.";
}

$type_login = $_GET["type"];
if(!isset($_GET["type"])){
    $type_login = 1;
}

switch ($type_login) {
    case '1':
        $title = 'Test Taker - TECS'; 
        $links = '<a href="#" style="color: #069;text-decoration: underline;font-size: 12px;" target="_blank">TEST ADMINISTRATOR</a><br>
                    <a href="#" style="color: #069;text-decoration: underline;font-size: 12px;" target="_blank">TECS</a>';
        $description = 'Enter the Taker ID, the Proctor Security Code (PSC) and password.';
        $body_background = 'background-image: url(/ilto3/themes/ilto/img/students.jpg) !important;';
        $html = '<div class="login-container animated fadeInDown">
                <form id="login-form" action="/ilto3/index.php?r=Clients/default/login" method="post">        
                    <div class="loginbox bg-white">
                        <div class="loginbox-social">
                            <div class="social-buttons">
                                <a id="home_link" href="https://www.iltoexams.com">
                                    <img src="/ilto3/images/iltoexams-logo-100.png" alt="ILTO EXAMS">
                                </a>
                            </div>
                        </div>
                        <div class="loginbox-title">&nbsp;</div>
                        <div class="loginbox-title">SIGN IN</div>
                        <div class="loginbox-textbox">
                            <input type="text" class="form-control" placeholder="TAKER ID" name="LoginForm[username]" id="LoginForm_username" required/>
                        </div>
                        <div class="loginbox-textbox">
                            <input type="password" class="form-control" placeholder="PROCTOR SECURITY CODE" name="LoginForm[authorizeCode]" id="authorization-code" required/>
                        </div>
                        <div class="loginbox-textbox">
                            <span class="btn btn-warning btn-block" id="verifyButton">Verify Code</span><br/><br/>
                        </div>
                        <div class="loginbox-textbox autho-code" id="code-validate" style="text-align: center;">
                           
                        </div>
                        <div class="loginbox-textbox">
                            <input type="password" class="form-control" placeholder="Password" name="LoginForm[password]" id="LoginForm_password" required/>
                            <input type="hidden" class="form-control" placeholder="Password" name="LoginForm[type]" id="type" value="1"/>
                        </div>
                        <div class="loginbox-submit">
                            
                            <input class="btn btn-success btn-block" value="Login" type="submit" name="yt0"><br/><br/>
                            <a href="/terms.php" style="color: #069;text-decoration: underline;font-size: 12px;" target="_blank">Terms and Conditions</a> 
                        </div>
                    </div>
                </form>
            </div>
            ';
        break;
    case '2':
        $title = 'Test Administrator'; 
        $links = '<a href="#" style="color: #069;text-decoration: underline;font-size: 12px;" target="_blank">EDE</a><br>
                    <a href="#" style="color: #069;text-decoration: underline;font-size: 12px;" target="_blank">TECS</a>';
        $description = 'Enter your username and password to access the administrator dashboard.';
        $body_background = 'background-image: url(/ilto3/themes/ilto/img/ede-background.jpg) !important;';
        $html = '<div class="login-container animated fadeInDown">
                <form id="login-form" action="/ilto3/index.php?r=Clients/default/login" method="post">        
                    <div class="loginbox bg-white">
                        <div class="loginbox-social">
                            <div class="social-buttons">
                                <a id="home_link" href="https://www.iltoexams.com">
                                    <img src="/ilto3/images/iltoexams-logo-100.png" alt="ILTO EXAMS">
                                </a>
                            </div>
                        </div>
                        <div class="loginbox-title">&nbsp;</div>
                        <div class="loginbox-title">SIGN IN</div>
                        <div class="loginbox-textbox">
                            <input type="text" class="form-control" placeholder="Username" name="LoginForm[username]" id="LoginForm_username" />
                        </div>
                        <div class="loginbox-textbox">
                            <input type="password" class="form-control" placeholder="Password" name="LoginForm[password]" id="LoginForm_password"/>
                            <input type="hidden" class="form-control" placeholder="Password" name="LoginForm[type]" id="type" value="2"/>
                        </div>
                        <div class="loginbox-forgot">
                            <a href="index.php?r=Clients/default/rememberPass">Forgot Password?</a>
                        </div>
                        <div class="loginbox-submit">
                            <input class="btn btn-primary btn-block" value="'.$flag.'" type="hidden" name="flag">
                            <input class="btn btn-primary btn-block" value="'.$flag_econfirm.'" type="hidden" name="flag_econfirm">
                            <input class="btn btn-success btn-block" value="Login" type="submit" name="yt0"><br/><br/>
                            <a href="/terms.php" style="color: #069;text-decoration: underline;font-size: 12px;" target="_blank">Terms and Conditions</a> 
                        </div>
                    </div>
                </form>
            </div>';
        break;
        case '3':
        $title = 'Test Taker - EDE'; 
        $links = '<a href="#" style="color: #069;text-decoration: underline;font-size: 12px;" target="_blank">TEST ADMINISTRATOR</a><br>
                    <a href="#" style="color: #069;text-decoration: underline;font-size: 12px;" target="_blank">TECS</a>';
        $description = 'Enter the TAKER ID and password provided by your test administrator.';
        $body_background = 'background-image: url(/ilto3/themes/ilto/img/ede-background.jpg) !important;';
        $html = '<div class="login-container animated fadeInDown">
                <form id="login-form" action="/ilto3/index.php?r=Clients/default/login" method="post">        
                    <div class="loginbox bg-white">
                        <div class="loginbox-social">
                            <div class="social-buttons">
                                <a id="home_link" href="https://www.iltoexams.com">
                                    <img src="/ilto3/images/iltoexams-logo-100.png" alt="ILTO EXAMS">
                                </a>
                            </div>
                        </div>
                        <div class="loginbox-title">&nbsp;</div>
                        <div class="loginbox-title">SIGN IN</div>
                        <div class="loginbox-textbox">
                            <input type="text" class="form-control" placeholder="TAKER ID" name="LoginForm[username]" id="LoginForm_username" />
                        </div>';
                        if($_GET["type"]!=3){
                            
                        $html .='<div class="loginbox-textbox">
                            <input type="password" class="form-control" placeholder="Password" name="LoginForm[password]" id="LoginForm_password"/>
                            
                        </div>';
                        }
                        $html .= '<input type="hidden" class="form-control" placeholder="Password" name="LoginForm[type]" id="type" value="3"/>
                        <div class="loginbox-submit">
                         
                            <input class="btn btn-success btn-block" value="Login" type="submit" name="yt0" ><br/><br/>
                            <a href="/terms.php" style="color: #069;text-decoration: underline;font-size: 12px;" target="_blank">Terms and Conditions</a> 
                        </div>
                    </div>
                </form>
            </div>';
        break;
    default:
        $title = 'Test Administrator';
        $description = 'Enter the username name and password provided by your test administrator.';
        $body_background = 'background-image: url(/ilto3/themes/ilto/img/ede-background.jpg) !important;';
        break;
}

?>


 <style> 
    body {
    <?php echo $body_background; ?>
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    }
    body:before {
    background-color: transparent !important;
    }
    .login-container .loginbox  {
        width: 400px !important;
        padding: 30px 0px !important;
    }
    .login-container .logobox {
        box-shadow: 3px 3px 3px #444;
        width: 400px !important;
    }
    .flash-error {
        border-color: #df5138;
        background: #e46f61;
        margin-bottom: 20px;
        margin-top: 0;
        color: #fff;
        border-width: 0;
        border-left-width: 5px;
        padding: 10px;
        border-radius: 0;
        }
        
    .login-container .logobox {
    height: auto !important;
    position: relative;
        font-size: 14px;
    padding: 12px !important;
    }    
    
    .login-container{margin:2% auto !important;
    }    
    
    .form-control, select {
        font-size: 16px !important;
    }
    .spacer {
        display:block;
        height:120px;
    }
    
    #home_link {
        color: #fff;
        text-decoration:none;
    }
    
    #home_link over{
        color: #fff;
        text-decoration:none;
        font-weight:bold;
    }
 </style>
       
    
    <div class="row-fluid">
        
            <div style="margin:auto;padding:20px;">
                <?php
                
                    foreach(Yii::app()->user->getFlashes() as $key => $message) {
                        echo '<div onClick="divclose(this);" id="flash-'.$key.'" class="flash-' . $key . '">' . $message . "&nbsp;&nbsp;&nbsp;Click Here to dismiss</div>\n";
                    }
                ?>
            </div>
    </div>
    <div class="row">
        <div class="spacer">
        </div>  
        <div class="col-sm-5">
            <?php 
                echo $html;
            ?> 
        </div>
        <div class="col-sm-7" style="padding:50px;">
            <div class="spacer" style="height:150px;">
             </div>    
            <?php
                if($message){
                    echo '<div class="logobox">
                
        <span>'.$text.'</span></div>';
                    
                }
                if($flag_econfirm==1){
                    echo '<span>Your email is confirmed, you can log in now.</span>';
                }
                ?>
                <h1 style="color: #fff;font-weight: 400 !important;font-size:50px;text-shadow: 2px 1px #000;">
                    <?php 
                    echo $title;
                    ?> 
                </h1>
                <h2 style="color: #fff;;"><?php echo $description; ?></h2>
                <h2 style="color: #fff;"><a id="home_link" href="https://www.iltoexams.com">www.iltoexams.com</a></h2>
        </div>
    </div>
    
<script>
    function divclose(divObj){
        $(divObj).fadeOut();
    }
    
    
    $("#verifyButton").click(function() {
		var code = $("#authorization-code").val();
		var id = $("#LoginForm_username").val();
		$.ajax({
			url : "/ilto3/index.php?r=Api/getPassByAuthorizationCode",
			type: "POST",
			dataType: "text",
			data: {
			    code: code,
			    id: id
			},
			success: function(data) {
			    var q = data;
			    if(q.length == 1){
			        switch(data) {
                      case 'A':
                        msg = "INVALID PROCTOR SECURITY CODE";
    			        $("#code-validate").html(msg);
    			        $("#authorization-code").val("");
                        break;
                      case 'B':
                        msg = 'INVALID ID TAKER';
                        $("#code-validate").html(msg);
                        $("#authorization-code").val("");
                        break;
                      case 'C':
                        msg= "NO LICENSE AVAILABLE";
        			    $("#code-validate").html(msg);
        			    $("#authorization-code").val("");
        			    break;    
                      default:
                        // code block
                    }
			    }else{
			       //else
				    msg= "CLICK IN THE LOGIN BUTTON TO START THE TEST";
    			    $("#code-validate").html(msg);
    			    $("#LoginForm_password").val(data); 
    			    $('#LoginForm_password').prop('readonly', true); 
			    }
			    
				
				
				
			}
		});
	});
	
	
</script>
<?php 
$url = Yii::app()->baseUrl;
?>
<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner nav-ilto">
            <div class="container"> 
            <!-- Be sure to leave the brand out there if you want it shown -->
            <div class="left-logo"><a class="brand" href="<?php echo $url?>/index.php?r=Clients/default/index"><img src="<?php echo $url?>/themes/ilto/img/logo_ILTO.png"></a></div>
            <?php 
                $logo= $url."/images/clients/tn2_".Yii::app()->user->getState('logo_cliente');
            ?>
                <div class="center-logo"><img src="<?php echo $logo;?>"></div>
                <div class="right-logo"><img src="<?php echo Yii::app()->user->getState('rutalogoexam');?>"></div>
            </div>
        </div>
    <div style="width:100%; height:7px;">
        <div style="width:30%;height:7px; float:left; background-color: #31B744"></div>
        <div style="width:15%;height:7px; float:left; background-color: #FFDA74"></div>
        <div style="width:15%;height:7px; float:left; background-color: #1363DC"></div>
        <div style="width:15%;height:7px; float:left; background-color: #9055CF"></div>
        <div style="width:15%;height:7px; float:left; background-color: #EE5959"></div>
        <div style="width:10%;height:7px; float:left; background-color: #31B744"></div>
    </div>
</div>

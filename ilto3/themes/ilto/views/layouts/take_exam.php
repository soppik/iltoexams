<?php 
  $body_background = 'background-image: url(/ilto3/themes/ilto/img/desktop.jpg) !important;';
  ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
  <title>ILTO - ADMINISTRATOR</title>
  <meta name="description" content="inbox" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="google" content="notranslate" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="icon" href="https://www.iltoexams.com/wp-content/uploads/2019/03/cropped-favicon-32x32.png" sizes="32x32" />
<link rel="icon" href="https://www.iltoexams.com/wp-content/uploads/2019/03/cropped-favicon-192x192.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="https://www.iltoexams.com/wp-content/uploads/2019/03/cropped-favicon-180x180.png" />
<meta name="msapplication-TileImage" content="https://www.iltoexams.com/wp-content/uploads/2019/03/cropped-favicon-270x270.png" />
  
  <!--Basic Styles-->
  <link href="/ilto3/themes/ilto/html/assets/css/bootstrap.min.css" rel="stylesheet" />
  <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
  <link href="/ilto3/themes/ilto/html/assets/css/font-awesome.min.css" rel="stylesheet" />
  <link href="/ilto3/themes/ilto/html/assets/css/weather-icons.min.css" rel="stylesheet" />
  
  
  <!--Fonts-->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css">
  
  <!--Beyond styles-->
  <link id="beyond-link" href="/ilto3/themes/ilto/html/assets/css/beyond.min.css" rel="stylesheet" />
  <link href="/ilto3/themes/ilto/html/assets/css/demo.min.css" rel="stylesheet" />
  <link href="/ilto3/themes/ilto/html/assets/css/typicons.min.css" rel="stylesheet" />
  <link href="/ilto3/themes/ilto/html/assets/css/animate.min.css" rel="stylesheet" />
  <link id="skin-link" href="" rel="stylesheet" type="text/css" />
  <script src="/ilto3/assets/js/jquery.min.js"></script>
  <script src="/ilto3/assets/js/bootstrap.min.js"></script>

    <title><?php echo Yii::app()->name;?></title>
  </head>
  <style> 
    body {
    <?php echo $body_background; ?>
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    color:#fff;
    }
    body:before {
    background-color: transparent !important;
    }
    .login-container .loginbox  {
        width: 400px !important;
        padding: 30px 0px !important;
    }
    .login-container .logobox {
        box-shadow: 3px 3px 3px #444;
        width: 400px !important;
    }
    .flash-error {
        border-color: #df5138;
        background: #e46f61;
        margin-bottom: 20px;
        margin-top: 0;
        color: #fff;
        border-width: 0;
        border-left-width: 5px;
        padding: 10px;
        border-radius: 0;
        }
        
    .login-container .logobox {
    height: auto !important;
    position: relative;
        font-size: 14px;
    padding: 12px !important;
    }    
    
    .login-container{margin:2% auto !important;
    }    
    
    .form-control, select {
        font-size: 16px !important;
    }
    .spacer {
        display:block;
        height:120px;
    }
    
    #home_link {
        color: #fff;
        text-decoration:none;
    }
    
    #home_link over{
        color: #fff;
        text-decoration:none;
        font-weight:bold;
    }
    
    .description_exam {
        position:relavite;
    }
    
    .step_warning {
        text-align:left;
    }
    
    #contact-number {
        width:300px;
        height:30px;
        font-size:14px;
    }
    
    input[type=checkbox] {
        position:relative;
        left:0px;
        opacity:1;
    }
    
    .btnExam {
        border-color: #1d7f2b;
        background-color: #31b744;
        color: #ffffff;
        font-family: Montserrat, sans-serif;
        line-height: 1;
        display: inline-block;
        font-size: 18px !important;
        font-weight: 600 !important;
        width: 340px !important;
        text-overflow: ellipsis;
        white-space: nowrap;
        padding: 25px 37.5px;
        cursor: pointer;
        text-align: center;
        overflow: hidden;
        vertical-align: top;
        white-space: nowrap;
        text-overflow: ellipsis;
        border: 1px solid;
        box-shadow: none;
        transition: .3s;
        text-transform: uppercase;
        letter-spacing: 0.06px;
        border-radius: 3px;
    }
    
    #photo_name {
        text-align:center;
    }
    
 </style>
<body>
    
<section class="main-body">
    <div class="container-fluid">
            <!-- Include content pages -->
 
            <?php echo $content; ?>
           
           
    </div>
</section>

  </body>
</html>
<div class="navbar navbar-inverse navbar-fixed-top here">
	<div class="navbar-inner nav-ilto">
            <div class="container"> 
            <!-- Be sure to leave the brand out there if you want it shown -->
            <div class="left-logo"><a class="brand" href="/index.php"><img src="themes/ilto/img/logo_ILTO.png"></a></div>
            <div class="title_customer" style="float:left;"><h1><center><?php echo  ((strtolower(Yii::app()->user->getState('isDemo')) == 1)?'DEMO: ':''). strtolower(Yii::app()->user->getState('nombre_cliente')); ?></center></h1></div>
            <?php 
            if(!Yii::app()->user->isGuest){
                $logo= "images/clients/tn2_".Yii::app()->user->getState('logo_cliente');
                ?>
                <div class="right-logo"><a class="brand" href="/index.php"><img src="<?php echo $logo;?>"></a></div>
            <?php } else {?>
                <div class="right-logo"></div>
            <?php } ?>
            </div>
        </div>
    <div style="width:100%; height:7px;">
        <div style="width:30%;height:7px; float:left; background-color: #31B744"></div>
        <div style="width:15%;height:7px; float:left; background-color: #FFDA74"></div>
        <div style="width:15%;height:7px; float:left; background-color: #1363DC"></div>
        <div style="width:15%;height:7px; float:left; background-color: #9055CF"></div>
        <div style="width:15%;height:7px; float:left; background-color: #EE5959"></div>
        <div style="width:10%;height:7px; float:left; background-color: #31B744"></div>
    </div>
</div>

<!DOCTYPE html>
<html lang="en">

<head>
  <?php 
    $siteUrl=Yii::app()->baseUrl; 
    $baseUrl = Yii::app()->theme->baseUrl; 
    include 'components/head.php'; 
  ?>
</head>
<!-- Body -->

<body  class="<?php echo Yii::app()->controller->id ?> <?php echo Yii::app()->controller->action->id ?>">

  <?php 
  if($_REQUEST[ 'r']=='Clients/default/login' ){ 
      include 'components/login.php'; 
  }else{ 
      include 'components/header.php'; 
      include 'components/body.php';
      include 'components/footer.php'; 
  } ?>

</body>
<!-- /Body -->

</html>
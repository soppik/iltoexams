
<!DOCTYPE html>
<html lang="en">

<head>
  <?php 
    $siteUrl=Yii::app()->baseUrl; 
    $baseUrl = Yii::app()->theme->baseUrl; 
    include 'components/head.php'; 
  ?>
  
<script type="text/javascript" src="/ilto3/js/jquery.pietimer.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Oxygen|Poppins|Roboto" rel="stylesheet">

</head>
<!-- Body -->
<body>

<section id="navigation-main">   
<!-- Require the navigation -->
<?php //require_once('tpl_navigation_q.php')?>
</section><!-- /#navigation-main -->
    
<section class="main-body">
    <div class="container-fluid">
            <!-- Include content pages -->
 
            <?php echo $content; ?>
           
           
    </div>
</section>

<!-- Require the footer -->
<?php //require_once('tpl_footer.php')?>

  </body>
</html>
jQuery(document).ready(function($){
	//open popup
	$('.cd-popup-trigger').on('click', function(event){
		event.preventDefault();
		$('.cd-popup').addClass('is-visible');
		$(this).removeClass('is-visible');
		var id = $(this).attr('id');
        var path = "/ilto3/index.php?r=Clients/LicensesUser/delete&id=";
        var result = path.concat(id);
        $("#confirm-btn").attr("href", result);
	});
	
	//close popup
	$('.cd-popup').on('click', function(event){
		if( $(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup') ) {
			$('.cd-popup').removeClass('is-visible');
			event.preventDefault();
        }
	});
	//close popup when clicking the esc keyboard button
	$(document).keyup(function(event){
    	if(event.which=='27'){
    		$('.cd-popup').removeClass('is-visible');
	    }
    });

});


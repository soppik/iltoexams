<meta charset="utf-8">
<title><?php echo Yii::app()->name;?></title>
<meta name="description" content="inbox" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="<?php echo $baseUrl; ?>/html/assets/img/favicon.png" type="image/x-icon">

<!--Basic Styles-->
<link href="<?php echo $baseUrl; ?>/html/assets/css/bootstrap.min.css" rel="stylesheet" />
<link id="bootstrap-rtl-link" href="" rel="stylesheet" />
<link href="<?php echo $baseUrl; ?>/html/assets/css/font-awesome.min.css" rel="stylesheet" />
<link href="<?php echo $baseUrl; ?>/html/assets/css/weather-icons.min.css" rel="stylesheet" />

<!--Fonts-->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css">

<!--Beyond styles-->
<link id="beyond-link" href="<?php echo $baseUrl; ?>/html/assets/css/beyond.min.css" rel="stylesheet" />
<link href="<?php echo $baseUrl; ?>/html/assets/css/demo.min.css" rel="stylesheet" />
<link href="<?php echo $baseUrl; ?>/html/assets/css/typicons.min.css" rel="stylesheet" />
<link href="<?php echo $baseUrl; ?>/html/assets/css/animate.min.css" rel="stylesheet" />

<link id="skin-link" href="" rel="stylesheet" type="text/css" />

<script>
  window.templateUrl = "<?php echo $baseUrl; ?>/html";
</script>

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


<!--Skin Script: Place this script in head to load scripts for skins and rtl support-->
<script src="<?php echo $baseUrl; ?>/html/assets/js/skins.js"></script>

<style type="text/css">
  /**
    NEW STYLES PLEASE MOVE TO CSS FILE AFTER END IMPLEMENTATION
    by cArLiiToX
  */
  .navbar .navbar-inner .navbar-header .navbar-account .account-area>li .dropdown-menu.dropdown-login-area>li.email, .navbar .navbar-inner .navbar-header .navbar-account .account-area>li .dropdown-menu.dropdown-login-area>li.email a {
      color: #505050;
  }
  
  li.profile {
      text-align: center;
      text-transform: uppercase;
      margin-top: 5px;
  }
  
  li.profile label a {
      font-weight: bold;
  }
  
  .profile span.label.label-primary {
      position: relative;
      top: -1px;
  }
  
  .navbar .navbar-brand small img {
      height: 40px;
      width: auto;
      margin-left: 12px;
  }
  
  .sidebar-header-wrapper b {
      font-size: 17px;
      color: #6B6A6A;
      opacity: 1;
      z-index: 9999999;
      position: relative;
      display: block;
      top: 13px;
      left: 15px;
  }
  
  .navbar .sidebar-collapse {
    left: 138px;
  }
  .menu-compact .sidebar-header-wrapper b{
    display:none;
  }
  
  .page-sidebar.menu-compact~.page-content {
      margin-left: 47px;
  }
</style>


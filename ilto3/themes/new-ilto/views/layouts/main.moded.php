<!DOCTYPE html>
<html lang="en">
  <head>
    <?php
	  $siteUrl = Yii::app()->baseUrl;
	  $baseUrl = Yii::app()->theme->baseUrl; 
    include 'components/head.php';
	  ?>
  </head>
  <!-- Body -->
      <body>
        <?php
          include 'components/header.php';
          include 'components/body.php';
          include 'components/footer.php';
        ?>
      </body>
  <!-- /Body -->
</html>
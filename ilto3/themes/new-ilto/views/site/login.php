<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */
Yii::app()->session->destroy();

?>
<style>
    body{
    background-image: url('themes/abound/img/login_bg.jpg') !important;
    background-attachment: fixed;
    background-repeat: no-repeat;
    background-size: 100%;
}
.row label{
    float: none !important;
}
</style>

<div class="row-fluid">
	
    <div class="span3 offset5 transparente">
<?php
	$this->beginWidget('zii.widgets.CPortlet', array(
		'title'=>"Restricted Access",
	));
	
?>



    <p>Complete this form to access the system:</p>    
    
    <div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'login-form',
        'enableClientValidation'=>false,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ), 
    )); ?>
        
        <div class="row">
            <?php echo $form->labelEx($model,'User'); ?>
            <?php echo $form->textField($model,'username'); ?>
            <?php echo $form->error($model,'username'); ?>
        </div>
    
        <div class="row">
            <?php echo $form->labelEx($model,'Password'); ?>
            <?php echo $form->passwordField($model,'password'); ?>
            <?php echo $form->error($model,'password'); ?>
        </div>
        <div class="row rememberMe">
            <?php echo $form->checkBox($model,'rememberMe',array('style'=>'float:left !important;')); ?>
            <?php echo $form->label($model,'rememberMe'); ?>
            <?php echo $form->error($model,'rememberMe'); ?>
        </div>
    
        <div class="row buttons">
            <?php echo CHtml::submitButton('Login',array('class'=>'btn btn btn-primary')); ?>
        </div>
    
    <?php $this->endWidget(); ?>
    </div><!-- form -->

<?php $this->endWidget();?>

    </div>

</div>
<?php
/*    if(isset($_GET['auto'])){
        echo "<script>window.location = '/ilto3/index.php?r=site/login&usr=".$_GET['username']."&pwd=".$_GET['password']."';</script>";
    } */
?>
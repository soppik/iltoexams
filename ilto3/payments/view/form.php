<?php
	//database connection
	//$conn = @mysqli_connect('localhost', 'thetec_newvers', 'Ilto.2015', 'thetec_iltoexam_newversion');
	$conn = @mysqli_connect('localhost', 'iltoexam_newvers', 'Ilto.2015', 'iltoexam_newversion');
	  
	
	if (!$conn) {
	    echo "Error: " . mysqli_connect_error();
		exit();
	}

	include(dirname(__FILE__).'/config.php');
	//get countries list
	$SQL='SELECT * FROM country';
	$countries_query = mysqli_query($conn, $SQL);
	
	//message control module
	$message = $_GET["message"];
	if(isset($message)){
		
		switch ($message) {
			case '1':
				$description_message = "This is NOT a available ID CLIENT";
				break;
			case '3':
				$description_message = "There is not a request for this ID, please fill the form and complete the apply.";
				break;
			
			default:
				// code...
				break;
		}
	}
?>
<!DOCTYPE html>
<html>
    <head>
		<title>TECS PRESENCIAL</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
		<link rel="stylesheet" href="../css/style.css" rel="stylesheet" type="text/css">
		
<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
	</head>
	<style>
		/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}

select.input-lg {
    height: 46px !important;
}


/* The Close Button */
.close {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

.row .col-xs-4 {
	padding: 20px;
}

.social-buttons {
	padding:5px;
}
	</style>
	<body>
	
		<div class="loginbox-social" style="text-align:center;">
                    <div class="social-buttons">
                        <img src="/ilto3/images/logo_ILTO.png" alt="ILTO EXAMS">&nbsp;&nbsp;&nbsp;&nbsp;
                        <img src="/ilto3/images/tecs.png" alt="TECS" height="100">
                    </div>
                </div>
		<div class="container">
			
			<div id="exTab1" class="container">	
				
				<div class="tab-content clearfix">
					<div class="tab-pane active" id="1a">
							<form method="post" action="/ilto3/payments/controller/registerController.php">
							
							<?php
								$tecs2go_amount = 120000;
								
							?>
				    	<div class="row">
							<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
								
									<h2>PAGA TU TECS PRESENCIAL</h2>
									<h2><small>Este formulario es EXCLUSIVAMENTE para pagar el examen <span style="color:#ffc000;"><b>TECS</b></span> presencial en la <b>Fundación Universitaria del Area Andina</b>.</small></h2>
									<p style="color:#d9534f;">Por favor realiza el registro desde un computador y te recomendamos usar el navegador Chrome. <b>NO ESTÁ HABILITADO PARA TELÉFONO CELULAR.</b></p>
									<hr class="colorgraph">
									<div class="row">
										<?php 
									if(isset($message)){
											echo '<span class="badge badge-danger" style="margin:auto;margin-bottom:10px;"><h2>'.$description_message."</h2></span>";
										}
									?>
										<div class="col-xs-12 col-sm-6 col-md-6">
											<div class="form-group">
									               <input type="text" name="name" id="name" class="form-control input-lg" placeholder="Nombres" tabindex="1" required>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-6">
											<div class="form-group">
									           	 <input type="text" name="surname" id="name" class="form-control input-lg" placeholder="Apellidos" tabindex="2" required>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-6">
											<div class="form-group">
									               	<input type="number" name="payerDocument" id="id_number" class="form-control input-lg" placeholder="Número de Cédula/DNI/RFC" tabindex="3" required>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-6">
											<div class="form-group">
									          
											</div>
										</div>
									</div>
									<div class="row">
										
										<div class="col-xs-12 col-sm-6 col-md-6">
											<div class="form-group">
												<label>Ciudad </label>
												<select name="extra2" id="city-select" class="form-control input-lg" placeholder="Ciudad" tabindex="4" required disabled>
													<option value=""></option>
		                                            <option value="2257">Bogotá</option>
		                                            <option value="2259">Medellin</option>
		                                            <option value="2265">Pereira</option>
		                                            <option value="2275">Valledupar</option>
		                                            <option value="4088">Virtual</option>
												</select>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-6">
											<div class="form-group">
												<label>Programa</label>
												<div id="program_container">
													
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										
										<div class="col-xs-12 col-sm-6 col-md-6">
											<div class="form-group">
												<label>Semestre </label>
													<select id="period" name="extra1" class="form-control input-lg" tabindex="6" required>
		                                                <option value=""></option>
		                                                <option value="1">1</option>
		                                                <option value="2">2</option>
		                                                <option value="3">3</option>
		                                                <option value="4">4</option>
		                                                <option value="5">5</option>
		                                                <option value="6">6</option>
		                                                <option value="7">7</option>
		                                                <option value="8">8</option>
		                                                <option value="9">9</option>
		                                                <option value="10">10</option>
		                                                <option value="11">Egresado</option>
		                                            </select>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-6">
											
										</div>
									</div>
									
									
								
									
									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-6">
											<div class="form-group">
												 <strong class="label label-danger">NO SE PERMITEN CUENTAS DE HOTMAIL</strong>
												<input type="email" name="buyerEmail" id="email" class="form-control input-lg" placeholder="Correo Electrónico" tabindex="5" required>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-6">
											<div class="form-group">
												<label>&nbsp;</label>
												<input type="email" name="re-email" id="re-email" onChange="reEmail(this.value)" class="form-control input-lg" placeholder="Repite tu Correo Electrónico" tabindex="6" required>
												<label id="email-alert" style="color:red;display:none;"><strong>Tu email y confirmación de Email no coinciden</strong></label>
											</div>
										</div>
									</div>
									<div class="row">
										
										<div class="col-xs-12 col-sm-6 col-md-6">
											<div class="form-group">
												<input type="number" name="phone" id="mobilePhone" class="form-control input-lg" placeholder="Número de teléfono Celular/Móvil" tabindex="7" required>
											</div>
										</div>
										
										<div class="col-xs-12 col-sm-6 col-md-6">
											
										</div>
									</div>
									<!--
									<div class="row">
										<div class="col-xs-4 col-sm-3 col-md-3" style="padding:0px !important">
											<span class="button-checkbox">
												<button type="button" class="btn trigger_popup_fricc" id="tac" data-color="info" tabindex="10" onClic="toogleNext()" disabled>ACEPTO</button>
									            <input type="checkbox" name="t_and_c" id="t_and_c" class="hidden" value="1">
											</span>
										</div>
										<div class="col-xs-8 col-sm-9 col-md-9">
											Al hacer clic en  <strong class="label label-primary"> ACEPTO</strong>, estás aceptando <a href="#" id="myBtn">nuestros Términos y Condiciones</a> inclyuendo nuestro uso de Cookies.
										</div>
									</div> -->
									<hr class="colorgraph">
									<div class="row">
										<div class="col-xs-12 col-md-6">
											<button type="submit" name="confirm" id="confirm" class="btn btn-success btn-block btn-lg" tabindex="8" >CONFIRMAR Y PAGAR</button>
										</div>
									</div>
							</div>
							<script src="https://www.google.com/recaptcha/api.js?render=6Let4uoUAAAAAD_PjMRfVb2-49diSGoxL4l0DexP"></script>
							<script>
							grecaptcha.ready(function() {
							    grecaptcha.execute('6Let4uoUAAAAAD_PjMRfVb2-49diSGoxL4l0DexP', {action: 'homepage'}).then(function(token) {

							    });
							});
							</script>
							
						</div>
						
					<div class="modal" id="myModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <h5 class="modal-title">IMPORTANTE</h5>
					        <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					          <span aria-hidden="true">&times;</span>
					        </button> -->
					      </div>
					      <div class="modal-body">
					      	<p>Recuerda que cuentas con máximo 60 dias para realizar tú examen <span style="color:#ffc000;"><b>TECS</b></span>. Despues de este tiempo, deberas pagarlo nuevamente.<br><br>
					      		<span style="color:#d9534f;"><small>Cuando realizas algún pago se genera un servicio automáticamente y por tanto, se generan unos costos. <b>ILTO</b> (International Language Testing Organization) no realiza devolucion de dineros por ningún concepto.</small></span>
					      	</p>
					      </div>
					      <div class="modal-footer">
					        <button type="button"  class="btn btn-success" data-dismiss="modal">ACEPTO</button>
					      </div>
					    </div>
					  </div>
					</div>
		
						</form>
					</div><!-- /.tab-pane1 -->
				</div>
			</div>
		</div>
				<!-- Bootstrap core JavaScript
				    ================================================== -->
					<!-- Placed at the end of the document so the pages load faster -->
					<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
			
			
	
	</body>
<html>
<script>

	$(document).ready(function(){
		
	$("#city-select").change(function() {
		var id = $("#city-select").val();
		var id_number = $("#id_number").val();
		$.ajax({
			url : "/ilto3/index.php?r=Api/getProgramsPse",
			type: "POST",
			dataType: "text",
			data: {
			id: id, id_number: id_number
			},
			success: function(data) {
			    $("#program_container").html(data);
			    $("#program_container").show();
			}
	    });
	});
	
	
	$("#id_number").keypress(function(){
	document.getElementById("city-select").disabled = false;
	});
	
	
	$("#period").change(function() {
		var apiKey = $("#apikey").val();
		var referenceCode = $("#referenceCode").val();
		var currency = $("#currency").val();
		var amount = $("#amount").val();
		var merchantId = $("#merchantId").val();
		console.log(apiKey,referenceCode,currency,amount,merchantId);
		
		$.ajax({
			url : "/ilto3/index.php?r=Api/getMd5TecsPayment",
			type: "POST",
			dataType: "text",
			data: {
			apiKey: apiKey, referenceCode: referenceCode, currency: currency, amount: amount, merchantId: merchantId
			},
			success: function(data) {
			    $("#signature").val(data);
			    console.log(data);
			}
	    });
		
		
	});

	


	$(window).load(function(){        
		
		$('#myModal').modal('show');
		
    });
    
    $("#cancel_popup").click(function(){
        window.history.back();
    });

});


function reEmail(val) {
console.log(val);
  var x = document.getElementById("email").value;
  
  if(x == val){
  	document.getElementById("confirm").disabled = false;
	document.getElementById('email-alert').style.display = 'none';
	document.getElementById("email").style.borderColor = "#ccc";
  	document.getElementById("re-email").style.borderColor = "#ccc";
  }else{
  	document.getElementById("confirm").disabled = true;
  	document.getElementById('email-alert').style.display = 'block';
  	document.getElementById("email").style.borderColor = "red";
  	document.getElementById("re-email").style.borderColor = "red";
  }
}

</script>
<?php
	//close connection
    $conn->close();

?>
<?php  
	
	//message description
	$message = $_GET["id"];
	//if message existe
	if(isset($message)){
		switch ($message) {
		case '1':
			$description = "Tienes una solicitud TECS2GO pendiente.";
		break;
		case '2':
			$description = "Tu registro a TECS ACADEMY se ha realizado.<br>  Encuentra tu usuario y contraseña para el<br> ingreso en tu email.";
		break;
		case '3':
			$description = "No hay un registro TECS2GO para ésta CÉDULA/DNI/RFC.";
		break;
		case '4':
			$description = "Usuario en línea.";
		break;
		case '5':
			$description = "Por favor inicia sesión para acceder al dashboard.";
		break;
		case '6':
			$description = "Tu sesión ha expirado.";
		break;
		case '7':
			$description = "Usuario o contraseña incorrecta.";
		break;
		case '8':
			$description = "Usuario bloqueado por fraude, contácte al administrador.";
		break;
		default:
			// code...
			break;
		}
	}

?>


<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html>
    
<head>
	<title>TECS2GO Login</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
</head>
<style>
    /* Coded with love by Mutiullah Samim */
		body,
		html {
			margin: 0;
			padding: 0;
			height: 100%;
			background: #39b54a !important;
		}
		.user_card {
			height: 550px;
			width: 400px;
			margin-top: auto;
			margin-bottom: auto;
			background: #e8e8e7;
			position: relative;
			display: flex;
			justify-content: center;
			flex-direction: column;
			padding: 10px;
			box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
			-webkit-box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
			-moz-box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
			border-radius: 5px;

		}
		.brand_logo_container {
			position: absolute;
			height: 170px;
			width: 170px;
			top: -75px;
			border-radius: 50%;
			background: #60a3bc;
			padding: 10px;
			text-align: center;
		}
		.brand_logo {
			height: 150px;
			width: 150px;
			border-radius: 50%;
			border: 2px solid white;
		}
		.form_container {
			margin-top: 30px;
		}
		.login_btn {
			width: 100%;
			background: #c0392b !important;
			color: white !important;
		}
		.login_btn:focus {
			box-shadow: none !important;
			outline: 0px !important;
		}
		.login_container {
			padding: 0 2rem;
		}
		.input-group-text {
			background: #c0392b !important;
			color: white !important;
			border: 0 !important;
			border-radius: 0.25rem 0 0 0.25rem !important;
		}
		.input_user,
		.input_pass:focus {
			box-shadow: none !important;
			outline: 0px !important;
		}
		.custom-checkbox .custom-control-input:checked~.custom-control-label::before {
			background-color: #c0392b !important;
		}
</style>
<!--Coded with love by Mutiullah Samim-->
<body>
	<div class="container h-100">
		
		<div class="d-flex justify-content-center h-100">
			<div class="row">
			</div>
			<div class="row">
				<div class="user_card">
					<span style="font-size:11px;" class="btn btn-danger">Use un computador con cámara UNICAMENTE. NO use celular.</span><br>
					<div class="d-flex justify-content-center">
							 
							<img src="/ilto3/images/tecs-academy.png" alt="TECS ACADEMY" />
						
					</div>
					<br>
					<span style="font-size:14px;text-align:center;"><b>Registro para Cursos de Inglés</b></span>
					<span style="font-size:14px;text-align:center;">Ingrese datos si ya está regsitrado.</span>
					<div class="d-flex justify-content-center form_container">
						<form action="/ilto3/tecs-academy/controller/loginController.php" method="POST">
							<div class="input-group mb-3">
								<div class="input-group-append">
									<span class="input-group-text"><i class="fas fa-user"></i></span>
								</div>
								<input type="text" name="username" class="form-control input_user" value="" placeholder="CÉDULA/DNI/RFC">
							</div>
							<div class="input-group mb-2">
								<div class="input-group-append">
									<span class="input-group-text"><i class="fas fa-key"></i></span>
								</div>
								<input type="password" name="password" class="form-control input_pass" value="" placeholder="Contraseña">
							</div>
							<div class="input-group mb-1">
									
							</div>
								<div class="d-flex justify-content-center mt-3 login_container">
					 		<button type="submit" name="button" class="btn login_btn">Ingresar</button>
					   </div>
						</form>
					</div>
				
					<div class="mt-4">
						<div class="d-flex justify-content-center links">
							<b>¿Registro por primera vez?</b> <a class="btn btn-primary" href="/ilto3/tecs-academy/view/sign-up.php" class="ml-2" style="margin-left:10px;margin-bottom:10px;bottom:10px;">Registrarse</a>
						</div>
						<div class="d-flex justify-content-center links">
							<!--<a href="#">Forgot your password?</a>-->
						</div>
					</div>
						<?php if(isset($message)){
								?>	
								
									<span class="badge badge-warning" style="margin:auto;margin-top: 10px;font-size:14px;"><?php echo $description; ?></span>
								
								<?php 	} ?>
				</div>	
			</div>
		</div>
	</div>
	
</body>
</html>
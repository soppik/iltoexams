<?php
	//database connection
	//$conn = @mysqli_connect('localhost', 'thetec_newvers', 'Ilto.2015', 'thetec_iltoexam_newversion');
	$conn = @mysqli_connect('localhost', 'iltoexam_newvers', 'Ilto.2015', 'iltoexam_newversion');
	  
	
	if (!$conn) {
	    echo "Error: " . mysqli_connect_error();
		exit();
	}


	//get countries list
	$SQL='SELECT * FROM country';
	$countries_query = mysqli_query($conn, $SQL);
	
	//message control module
	$message = $_GET["message"];
	if(isset($message)){
		
		switch ($message) {
			case '1':
				$description_message = "This is NOT a available ID CLIENT";
				break;
			case '3':
				$description_message = "There is not a request for this ID, please fill the form and complete the apply.";
				break;
			
			default:
				// code...
				break;
		}
	}
?>
<!DOCTYPE html>
<html>
    <head>
		<title>TECS INTRO REGISTRATION FORM</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
		<link rel="stylesheet" href="../css/style.css" rel="stylesheet" type="text/css">
		
<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
	</head>
	<style>
		/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}

select.input-lg {
    height: 46px !important;
}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 50px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
  text-align:center;
}

/* Modal Content */
.modal-content {
  background-color: #fefefe;
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
  width: 80%;
}

/* The Close Button */
.close {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

.row .col-xs-4 {
	padding: 20px;
}
	</style>
	<body>
	
		<div class="loginbox-social" style="text-align:center;">
                    <div class="social-buttons">
                        <img src="/ilto3/images/logo_ILTO.png" alt="ILTO EXAMS">
                    </div>
                </div>
		<div class="container">
			
			<div id="exTab1" class="container">	
			<!--	<ul  class="nav nav-pills">
					<li class="active">
				    	<a  href="#1a">1. Registro</a>
					</li>
					<li>
						<a href="#" class="isDisabled">2. Validación de Identidad</a>
					</li>
					<li>
						<a href="#" class="isDisabled" >3. Foto del Rostro</a>
					</li>
					<li href="#1a" >
						<a href="#" class="isDisabled">4. Foto Documento de Identificación</a>
					</li>
					<li href="#1a" >
						<a href="#" class="isDisabled" >5. Pago TECS2GO</a>
					</li>
					<li href="#1a" >
						<a href="#" class="isDisabled" >6. Fecha de Examen</a>
					</li>
				</ul> -->
				<div class="tab-content clearfix">
					<div class="tab-pane active" id="1a">
						<form role="form" method="POST" action="/ilto3/tecs-intro/controller/registerController.php">
				    	<div class="row">
							<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
								
									<h2>TECS INTRO REGISTRATION FORM</h2>
									<h2><small>Aprende acerca de las 4 secciones del examen TECS y su logística.</small></h2>
									<hr class="colorgraph">
									<div class="row">
										<?php 
									if(isset($message)){
											echo '<span class="badge badge-danger" style="margin:auto;margin-bottom:10px;"><h2>'.$description_message."</h2></span>";
										}
									?>
										<div class="col-xs-12 col-sm-6 col-md-6">
											<div class="form-group">
									               <input type="text" name="name" id="name" class="form-control input-lg" placeholder="Nombres" tabindex="1" required>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-6">
											<div class="form-group">
												<input type="text" name="surename" id="surename" class="form-control input-lg" placeholder="Apellidos" tabindex="2" required>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-6">
											<div class="form-group">
												<label>País</label>
									   	       <select name="country" id="country-select" class="form-control input-lg" placeholder="Country" tabindex="3" required>
													<option value=""></option>
														<?php
														while($country_row=mysqli_fetch_assoc($countries_query)){
						                                   	echo '<option value="'.$country_row['Code'].'">'.utf8_encode($country_row['Name']).'</option>';
						                                   }
														?>
												</select>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-6">
											<div class="form-group">
												<label>Ciudad </label>
												<select name="city" id="city-select" class="form-control input-lg" placeholder="Country" tabindex="4" required>
												</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-6">
											<div class="form-group">
									           	<input type="number" name="id_number" id="id_number" class="form-control input-lg" placeholder="Número de Cédula/DNI/RFC" tabindex="5" required>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-6">
											<div class="form-group">
												<input type="number" name="phone" id="phone" class="form-control input-lg" placeholder="Número de teléfono Celular/Móvil" tabindex="6" required>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-6">
											<div class="form-group">
												 <strong class="label label-danger">NO SE PERMITE CUENTAS DE HOTMAIL</strong>
												<input type="email" name="email" id="email" class="form-control input-lg" placeholder="Correo Electrónico" tabindex="7" required>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-6">
											<div class="form-group">
											<label>&nbsp;</label>
												<input type="email" name=re-"email" id="re-email" onChange="reEmail(this.value)" class="form-control input-lg" placeholder="Repite tu Correo Electrónico" tabindex="8" required>
												<label id="email-alert" style="color:red;display:none;"><strong>Tu email y confirmación de Email no coinciden</strong></label>
											</div>
										</div>
									</div>
								
									
									<div class="row">
										<div class="col-xs-4 col-sm-3 col-md-3" style="padding:0px !important">
											<span class="button-checkbox">
												<button type="button" class="btn trigger_popup_fricc" id="tac" data-color="info" tabindex="10" onClic="toogleNext()" disabled>ACEPTO</button>
									            <input type="checkbox" name="t_and_c" id="t_and_c" class="hidden" value="1">
											</span>
										</div>
										<div class="col-xs-8 col-sm-9 col-md-9">
											Al hacer clic en  <strong class="label label-primary"> ACEPTO</strong>, estás aceptando <a href="#" id="myBtn">nuestros Términos y Condiciones</a> inclyuendo nuestro uso de Cookies.
										</div>
									</div>
									<hr class="colorgraph">
									<div class="row">
										<div class="col-xs-12 col-md-6">
											<button type="submit" name="register" id="register" class="btn btn-success btn-block btn-lg" tabindex="11" disabled>Registrarme</button>
										</div>
									</div>
							</div>
							<script src="https://www.google.com/recaptcha/api.js?render=6Let4uoUAAAAAD_PjMRfVb2-49diSGoxL4l0DexP"></script>
							<script>
							grecaptcha.ready(function() {
							    grecaptcha.execute('6Let4uoUAAAAAD_PjMRfVb2-49diSGoxL4l0DexP', {action: 'homepage'}).then(function(token) {

							    });
							});
							</script>
							
						</div>
						<!-- The Modal -->
						<div id="myModal" class="modal">
						
						  <!-- Modal content -->
						  <div class="modal-content">
						    <span class="close">&times;</span>
						    <div class="row">
						    	<h2 style="text-align:center;width: 100%;color: #39b54a;">TÉRMINOS Y CONDICIONES</h2>
						    	<h4 style="text-align:center;width: 100%;">Lee cuidadosamente los terminos y condiciones que aceptarás al registrarte en <strong>TECS2GO</strong></h4>
						    </div>
						    <div class="row">
						    	<div class="col-xs-3 col-sm-3 col-md-3">
									<img src="../img/business-and-finance.png" width="64"></img>
									<h5>1. Para presentar el examen debes estar solo en la oficina o habitación.</h5>
								</div>
								<div class="col-xs-3 col-sm-3 col-md-3">
									<img src="../img/signals.png" width="64"></img>
									<h5>2. Debes tener una conexión a Internet estable.</h5>
								</div>
								<div class="col-xs-3 col-sm-3 col-md-3">
									<img src="../img/seo-and-web.png" width="64"></img>
									<h5>3. El espacio donde desarrolles el examen debe estar bien iluminado.</h5>
								</div>
								<div class="col-xs-3 col-sm-3 col-md-3">
									<img src="../img/silhouette.png" width="64"></img>
									<h5>4. Si no podemos verte o ver tu pantalla durante el examen, este será <strong>anulado</strong>.</h5>
								</div>
						    </div>
						    <div class="row">
						    	<div class="col-xs-3 col-sm-3 col-md-3">
									<img src="../img/interface.png" width="64"></img>
									<h5>5. Si durante el examen observamos o escuchamos a alguien externo, el examen será <strong>anulado</strong>.</h5>
								</div>
								<div class="col-xs-3 col-sm-3 col-md-3">
									<img src="../img/tools-and-utensils.png" width="64"></img>
									<h5>6. Durante el examen monitoreamos la coherencia de los resultados; si estos <strong>NO</strong> son coherentes, <strong><span style="color:#39b54a;">ILTO</span></strong> se reservará el derecho de anular el examen.</h5>
								</div>
								<div class="col-xs-3 col-sm-3 col-md-3">
									<img src="../img/computer.png" width="64"></img>
									<h5>7.<strong><span style="color:#39b54a;">ILTO</span></strong> revisará las grabaciones y resultados del examen y se reservará el derecho de anular el examen si encuentra indicios de fraude o anomalías.</h5>
								</div>
								<div class="col-xs-3 col-sm-3 col-md-3">
									<img src="../img/country.png" width="64"></img>
									<h5>8. <strong><span style="color:#39b54a;">ILTO</span></strong> es una compañía basada en Los Estados Unidos de América. Cualquier acción legal debe ser llevada a cabo ante las autoridades de Dade County en el estado de La Florida.</h5>
								</div>
						    </div>
						    <div class="row">
								<div class="col-xs-3 col-sm-3 col-md-3" >
								<img src="../img/hdmi.png" width="64"></img>
									<h5>9. No se permiten conexiones con cables HDMI en el computador.</h5>
								
								</div>
									<div class="col-xs-3 col-sm-3 col-md-3">
									</div>
								<div class="col-xs-3 col-sm-3 col-md-3">
								</div>
								<div class="col-xs-3 col-sm-3 col-md-3" >
									
								
								</div>
						    </div>
						    <div class="row">
								<div class="col-xs-3 col-sm-3 col-md-3" >
								
								</div>
									<div class="col-xs-3 col-sm-3 col-md-3">
									</div>
								<div class="col-xs-3 col-sm-3 col-md-3">
									<h5 style="line-height:1.5">Al hacer clic en el botón <span  class="label label-primary" style="color: #fff;background-color: #117a8b;border-color: #10707f;"><strong>ACEPTO</strong></span> tomaremos tu número de Cédula/DNI/RFC digitado en tu registro como una firma digital según las leyes vigentes de tu país.</h5>
								</div>
								<div class="col-xs-3 col-sm-3 col-md-3" >
									
									<input type="button" class="btn btn-success btn-block btn-lg" id="agree_btn" value="ENTENDIDO">
									<b><small style="color:red;">Recuerda cumplir con la asistencia programada a los dos exámenes, evita sanciones o pagos adicionales.</samll></b>
								</div>
						    </div>
						    <div class="row">
						    
						    </div>
						  </div>

							</div>
						</form>
					</div><!-- /.tab-pane1 -->
				</div>
			</div>
		</div>
				
				
				<!-- Bootstrap core JavaScript
				    ================================================== -->
					<!-- Placed at the end of the document so the pages load faster -->
					<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
			
			
			
			
			
			
			
			
	</body>
<html>
<script>

	$(document).ready(function(){
		modal.style.display = "none";
		
								    $('#country-select').change(function() {
								        
								        loadCity($(this).find(':selected').val())
								    })
								
								const pasteBox = document.getElementById("re-email");
								  pasteBox.onpaste = e => {
								    e.preventDefault();
								    return false;
								  };
								
								})

								function loadCity(countryId){
								        $("#city-select").children().remove()
								        $.ajax({
								            type: "POST",
								            url: "/ajax.php",
								            data: "get=city&countryId=" + countryId
								            }).done(function( result ) {
								                $(result).each(function(i,j){
								                    $("#city-select").append($('<option>', {
								                        value: j.ID,
								                        text: j.Name,
								                    }));
								                })
								            });
								}


	$(function () {
    $('.button-checkbox').each(function () {
		
        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });
        
        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');
			
		
            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");
            

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
                document.getElementById('register').disabled = false;
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
                document.getElementById('register').disabled = true;
            }
        }
        
        // Actions
        function toogleIlto() {
           console.log("toogle");
			var iltoChecked = $('#iltocheck').is(':checked');
			
		
           
             // Set the button's state
            $button.data('state', (iltoChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (iltoChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
                document.getElementById('client').disabled = true;
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
                document.getElementById('client').disabled = false;
            }
            
        }

        // Initialization
        function init() {

            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i>');
            }
        }
        init();
    });
});



// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

var ok = document.getElementById("agree_btn");

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
  
  console.log()
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

ok.onclick = function(){
	modal.style.display = "none";
	
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}



function reEmail(val) {
  var x = document.getElementById("email").value;
  console.log(x, val);
  if(x == val){
  	document.getElementById("tac").disabled = false;
	document.getElementById('email-alert').style.display = 'none';
	document.getElementById("email").style.borderColor = "#ccc";
  	document.getElementById("re-email").style.borderColor = "#ccc";
  }else{
  	document.getElementById("tac").disabled = true;
  	document.getElementById('email-alert').style.display = 'block';
  	document.getElementById("email").style.borderColor = "red";
  	document.getElementById("re-email").style.borderColor = "red";
  }
}



</script>
<?php
	//close connection
    $conn->close();

?>
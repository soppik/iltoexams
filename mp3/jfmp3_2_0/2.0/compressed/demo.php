<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>JF MP3 Player Demo Page</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8"/>

	<link rel="stylesheet" href="jfmp3/jfmp3.css" type="text/css" media="screen"/>
	<!--[if lt IE 7]>
		<link rel="stylesheet" href="jfmp3/jfmp3_ie6.css" type="text/css" media="screen"/>
        <script type="text/javascript" src="jfmp3/jfmp3_iefix.js"></script>
	<![endif]-->
	<script type="text/javascript" src="jfmp3/mootools.js"></script>
	<script type="text/javascript" src="jfmp3/soundmanager2.js"></script>
	<script type="text/javascript" src="jfmp3/jfmp3.js"></script>

</head>
<body>
	
	<div id="jfmp3_player">Loading MP3 player...</div>
	<ul id="jfmp3_prePlaylist">
    	<li>
        	<a href="/mp3/RockTheCasbah.mp3">
            	<span class="title">Rock The Casbah</span> -
                <span class="artist">JF MP3</span> -
                <span class="album">Demo Album</span> -
                <span class="coverart">http://www.pezus.com/assets/pezus.png</span>
            </a>
		</li>
        
    </ul>
	
</body>
</html>
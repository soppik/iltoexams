<!DOCTYPE html>
<html>
<head>
    <title>About ILTO</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="Content-Language" content="ES" />
    
    <link rel="canonical" href="/" />

<link rel="shortcut icon" href="media/favicon.ico">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!--jQuery-->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" ></script>
<script src="media/all_min.js"></script>


<script type="text/javascript" src="http:/maps.google.com/maps/api/js?sensor=false"></script>
<link href='includes/libs/js/fancybox/jquery.fancybox-1.3.4.css'  type="text/css" rel="stylesheet">
<script src="includes/libs/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>

<script src="includes/libs/js/infobox/infobox.js"></script>

<script src="http:/code.jquery.com/ui/1.10.3/jquery-ui.js"></script>




    <link rel="stylesheet" type="text/css" href="includes/style.css">
<script type="text/javascript" src="includes/include.js"></script>


    
</head>

<!--[if lt IE 7 ]> <body class="ie6 lang-ES" > <![endif]-->
<!--[if IE 7 ]> <body class="ie7 lang-ES" > <![endif]-->
<!--[if IE 8 ]> <body class="ie8 lang-ES" > <![endif]-->
<!--[if IE 9 ]> <body class="ie9 lang-ES" > <![endif]-->
<!--[if !IE]><!--> <body class="lang-ES" > <!--<![endif]-->

<script type="text/javascript">
/* <![CDATA[ */* ]]> */
</script>





<div class="transit-internacional sub-page sub-landing" id="page">
<?php include("includes/header.php"); ?>

  <!-- GALLERY --> <section id="main-gallery" class="drop-shadow"> <div id="mg-container">
                      <div class="side-grid-container">
              <div class="side-grid" id="left"></div>
              <div class="side-grid" id="right"></div>
            </div>
        <div class="flexslider">
           
                      <ul class="slides"><li id="call-id-267">

                

                <div class="caption-container-wrapper">
                  <div class="caption-container">
                    <div class="caption">
                      <h5> </h5>
                      <p></p>
                      <a href="" class="read-more"></a>
                    </div>
                  </div>
                </div>
                <img src="img/im_about2.jpg" alt=" "/>
              </li><li id="call-id-456">

                

                <div class="caption-container-wrapper">
                  <div class="caption-container">
                    <div class="caption">
                      <h5></h5>
                      <p></p>
                      <a href="" class="read-more"></a>
                    </div>
                  </div>
                </div>
                <img src="img/im_about1.jpg" alt="E"/>
              </li></ul>
            </div>
        </div> </section> <article>  </article><!-- CONTENT --> <section id="content">
  <div class="wrapper"><ul id="breadcrumb">
    <li class="home"><a href="../"></a></li><li class="class">ABOUT ILTO?</li></ul> <article class="narrow large-text">
            <div class="wrapper">

                      <h1>ABOUT ILTO?</h1>
    
              <p>
              International language testing organization</p>
              <p>&nbsp;</p>
              <p>At <strong>ILTO</strong>  we believe in the importance of assessing the skills and knowledge of the person in the English language in order to give organizations and individuals a reliable tool to know the level of communication skills for education, work, travel or leisure.</p>
              <p>&nbsp; </p>
              <p>                We have managed to meet the highest quality standards by testing our language assessment in more than 10.000 people worldwide.</p>
              <p>&nbsp;</p>
              <p>                We have developed tests suitable to language students, education institutions, companies and government.</p>
              <p>&nbsp;</p>
              <p>                We build our tests by interacting with people from different educational institutions and companies around the world, who have taken part in important quality surveys and studies in order to achieve reliable results. </p>
              <p>&nbsp;</p>
              <p>                Our tests are based on updated topics used by people in real communication. These topics have to do with social networks, today´s culture, music, sports, news and in general terms contextualized questions that assess real communication between individuals in a day to day basis. </p>
              <p>&nbsp;</p>
              <p>                With over 10 year experience in assessing the proficiency of individuals in the English language, <strong>ILTO</strong> has helped teachers, managers and public and private institutions in the United States and Latin America have a real appreciation of how people in their organizations would perform in communication tasks by using a totally measurable and reliable system.</p>
              <p>&nbsp;</p>
              <p>                We also develop various products for language learning in different educational and professional levels by working together with our customers´ needs.</p>
              <p>&nbsp;</p>
              <p>                <strong>ILTO</strong> is the most reliable option for your organization or yourself when thinking about assessing communication skills in English.</p>
              <p>&nbsp;</p>
              
               <div style="position:relative; width:550px; height:200px;margin:auto;padding-top:10px;padding-bottom:10px;">
            <p>ILTO is permanently striving to enhance the quality of our tests and be updated with the recent test developments. We also value to network with testing professionals. This is why we are proud members of:</p>     
            <div style="float:left;margin-left:50px;">
              <a href="http://www.iltaonline.com/" target="_blank"><img src="/img/ilta.png" alt="ilta" style="margin-top: 10px;"/></a>
            <h4 style="text-align:center;color: #31B744;"><a href="http://www.iltaonline.com/" target="_blank">International Language Testing <br/>Association</a></h4>
            </div>
            <div style="float:left;margin-left:30px;">
              <a href="http://www.ncta-testing.org/" target="_blank"><img src="/img/ncta.png" alt="ncta" style="margin-top: 10px;"/></a>
               <h4 style="text-align:center;color: #31B744;"> <a href="http://www.ncta-testing.org/" target="_blank">National College Testing <br/>Association</a></h4>
            </div>
        </div>
              
              <p><CENTER><img src="img/ABOUT-ILTO.png" width="500" height="354"></CENTER></p>
<p>&nbsp;</p>
            </div>

          </article> 
  <a href="/#" style="display:none;"></a></div>
  </section> 
<?php include("includes/pie.php"); ?>

    	</body>
        </html>
        
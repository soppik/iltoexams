<?php /* @var $this Controller */ 
?>
<script>
    function divclose(divObj){
        $(divObj).fadeOut();
    }
</script>    
    
<?php $this->beginContent('//layouts/main'); ?>
<?php require_once('menu.php')?>
  <div class="row-fluid">
  <div class="span9">
    <!-- Include content pages -->
    <div class="ilto-main-table">
          <?php
            foreach(Yii::app()->user->getFlashes() as $key => $message) {
                echo '<div onClick="divclose(this);" id="flash-'.$key.'" class="flash-' . $key . '">' . $message . "&nbsp;&nbsp;&nbsp;Click Here to dismiss</div>\n";
            }
        ?>
        <?php if(!Yii::app()->user->isGuest){
            if(!is_null(Yii::app()->user->getState('ruta_foto'))){
                $logo = "images/users/tn2_".Yii::app()->user->getState('ruta_foto');
            } else {
                $logo= "images/clients/tn2_".Yii::app()->user->getState('logo_cliente');
            }
    echo "<img class='imagen_logo_agua' src='$logo'>";
}?>

    <?php echo $content; ?>
    </div>    
	</div><!--/span-->
        <div class="span3"><h3><center>MAIN MENU</center></h3><h6 style="margin-top:-20px;"><center><?php echo Yii::app()->user->getState('nombre_perfil');?></center></h6>
		<div class="sidebar-nav">
		  <?php 
                  if(count($this->menu)>0){
                      $items=array(
				array('itemOptions'=>array('class'=>'opciones'),'items'=>$this->menu),
                                array('label'=>''),
                                $arreglo_mnu[0]);
                  } else {
                      $items=$arreglo_mnu;
                  }
                  if(count($this->menu)>0){
                      array_push($arreglo_mnu, $this->menu);
                  }
                  $this->widget('zii.widgets.CMenu', array(
			/*'type'=>'list',*/
			'encodeLabel'=>false,
                        'items'=>$items,
			));?>
		</div>
        <br>
    </div><!--/span-->

  </div><!--/row-->


<?php $this->endContent(); ?>
<?php 
// Procedi para el menu
    $permiso = array(0,0,0,0,0);
    $modelperfil=  Profiles::model()->findByPk(Yii::app()->user->getState('id_perfil'));
    if(is_null ($modelperfil)){
            $perfilestandar='';
    }else{
            $perfilestandar=$modelperfil->configuracion;
    }
    $configact=explode(',',$perfilestandar);
    $arreglo_mnu = array();
    $criteria = new CDbCriteria();
    $criteria->addCondition("nodo=0","estado='A'");
    $model= Menu::model()->findAll($criteria);
    foreach($model as $cero){
        if($cero->url=='#'){
           $criteria = new CDbCriteria();
           $criteria->addCondition("nodo=".$cero->opcion);
           $modelodentro = Menu::model()->findAll($criteria);
           $opcionuno = array();
           foreach($modelodentro as $interno){
               if($interno->url=='#'){
                    $criteria2 = new CDbCriteria();
                    $criteria2->addCondition("nodo=".$interno->opcion);
                    $modelodentro2 = Menu::model()->findAll($criteria2);
                    $opciontres = array();
                    foreach($modelodentro2 as $interno2){
                        $config2=$interno2->nodo.'-'.$interno2->opcion;
                        $estadointerno2=0;
                        foreach($configact as $pareja){
                            if($pareja==$config2){
                                $estadointerno2=1;					
                            }
                       }
                        $arreglo_mnuinterno2 = array('label'=>$interno2->nombre, 'url'=>Yii::app()->createUrl($interno2->url),'visible'=>$estadointerno2);
                        if($estadointerno2==1){
                            array_push($opciontres,$arreglo_mnuinterno2);
                        }
                    }
                   $config=$interno->nodo.'-'.$interno->opcion;
                   $estadogeneral=0;
                   foreach($configact as $pareja){
                        if($pareja==$config){
                            $estadogeneral=1;					
                        }
                   }
                   $arreglo_mnuinterno = array('label'=>$interno->nombre, 'url'=>$interno->url, 'visible'=>$estadogeneral,'items'=>$opciontres);
               } else {
                    $config=$interno->nodo.'-'.$interno->opcion;
                    $estadointerno=0;
                    foreach($configact as $pareja){
                        if($pareja==$config){
                            $estadointerno=1;					
                        }
                    }
                    $arreglo_mnuinterno = array('label'=>$interno->nombre, 'url'=>Yii::app()->createUrl($interno->url),'visible'=>$estadointerno);
               } 
               if($estadointerno==1){
                   array_push($opcionuno,$arreglo_mnuinterno);
               }
           }
          $config=$cero->nodo.'-'.$cero->opcion;
          $estadogeneral=0;
          foreach($configact as $pareja){
               if($pareja==$config){
                   $estadogeneral=1;					
               }
          }
          $opcioncero = array('itemOptions'=>array('class'=>'general'),'items'=>$opcionuno);
        } else {
           $config=$cero->nodo.'-'.$cero->opcion;
           $estadogeneral=0;
           foreach($configact as $pareja){
                   if($pareja==$config){
                       $estadogeneral=1;					
                    }
           }
           $opcioncero = array('label'=>$cero->nombre, 'url'=>array($cero->url), 'visible'=>$estadogeneral);
        }       
        if($estadogeneral==1){
            array_push($arreglo_mnu,$opcioncero);
        }
    }
    $ingresar = array('label'=>'LogIn', 'url'=>array('/Clients/default/login'), 'visible'=>Yii::app()->user->isGuest);
    $salir = array('label'=>'LogOut ('.Yii::app()->user->name.')', 'url'=>array('/Clients/default/logout'), 'visible'=>!Yii::app()->user->isGuest);
    array_push($arreglo_mnu,$ingresar);
    array_push($arreglo_mnu,$salir);
    ?>

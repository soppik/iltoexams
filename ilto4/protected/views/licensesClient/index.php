<?php
/* @var $this LicensesClientController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Licenses Clients',
);

$this->menu=array(
	array('label'=>'Create LicensesClient', 'url'=>array('create')),
	array('label'=>'Manage LicensesClient', 'url'=>array('admin')),
);
?>

<h1>Licenses Clients</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

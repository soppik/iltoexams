<?php
/* @var $this LicensesClientController */
/* @var $model LicensesClient */

$this->breadcrumbs=array(
	'Licenses Clients'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Assign new License', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#licenses-client-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Licenses per Client</h1>



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'licenses-client-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_licencias_cliente',
            array(
                'name'=>'id_cliente',
                'value'=>'$data->idCliente->nombre_rsocial',
               'filter'=>CHtml::listData(Clients::model()->findAll(),"id_cliente","nombre_rsocial"),
                ),
            array(
                'name'=>'id_licencia',
                'value'=>'$data->idLicencia->nombre',
               'filter'=>CHtml::listData(Licenses::model()->findAll(),"id_licencia","nombre"),
                ),
		'cantidad',
		'utilizados',
            array(
                'header'=>'Remain',
                'value'=>'$data->cantidad-$data->utilizados',
               'filter'=>'',
                ),
                array(
                    'name'=>'demo',
                    'value'=>'(($data->demo))?CHtml::image(Yii::app()->baseUrl."/images/iconos/ok.png","",array("style"=>"max-width:20px;")):CHtml::image(Yii::app()->baseUrl."/images/iconos/delete.png","",array("style"=>"max-width:20px;"))',
                    'filter'=>'',
                    'type'=>'raw',
                    ),
		/*
		'fecha_inicial',
		'fecha_final',
		'estado',
		*/
		array(
			'class'=>'CButtonColumn',
                    'template'=>'{update}{delete}',
                    'buttons'=>array(
                        'delete'=>array(
                            'label'=>'Delete&nbsp;',
                            'imageUrl'=>NULL
                        ),
                        'update'=>array(
                            'label'=>'Edit&nbsp;',
                            'imageUrl'=>NULL
                        )
                    )
		),
	),
)); ?>

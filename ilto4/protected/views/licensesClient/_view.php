<?php
/* @var $this LicensesClientController */
/* @var $data LicensesClient */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_licencias_cliente')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_licencias_cliente), array('view', 'id'=>$data->id_licencias_cliente)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_cliente')); ?>:</b>
	<?php echo CHtml::encode($data->id_cliente); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_licencia')); ?>:</b>
	<?php echo CHtml::encode($data->id_licencia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cantidad')); ?>:</b>
	<?php echo CHtml::encode($data->cantidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('utilizados')); ?>:</b>
	<?php echo CHtml::encode($data->utilizados); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('demo')); ?>:</b>
	<?php echo CHtml::encode($data->demo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_inicial')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_inicial); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_final')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_final); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado')); ?>:</b>
	<?php echo CHtml::encode($data->estado); ?>
	<br />

	*/ ?>

</div>
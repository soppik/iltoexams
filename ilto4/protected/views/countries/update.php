<?php
/* @var $this CountriesController */
/* @var $model Countries */

$this->breadcrumbs=array(
	'Countries'=>array('index'),
	$model->id_pais=>array('view','id'=>$model->id_pais),
	'Update',
);

$this->menu=array(
	array('label'=>'Create Countries', 'url'=>array('create')),
	array('label'=>'Manage Countries', 'url'=>array('admin')),
);
?>

<h1>Update Countries <?php echo $model->id_pais; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
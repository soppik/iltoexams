<?php
/* @var $this AnswersController */
/* @var $model Answers */

$this->breadcrumbs=array(
	'Answers'=>array('index'),
	$model->id_respuesta=>array('view','id'=>$model->id_respuesta),
	'Update',
);

$this->menu=array(
	array('label'=>'Create Answers', 'url'=>array('create','question'=>$question)),
	array('label'=>'Manage Answers', 'url'=>array('admin','question'=>$question)),
);
?>

<h1>Update Answers <?php echo $model->id_respuesta; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
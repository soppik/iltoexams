<?php
/* @var $this QuestionsExamController */
/* @var $model QuestionsExam */

$this->breadcrumbs=array(
	'Questions Exams'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage QuestionsExam', 'url'=>array('admin')),
);
?>

<h1>Create QuestionsExam</h1>

<?php $this->renderPartial('_form', array('model'=>$model,'exam'=>$exam)); ?>
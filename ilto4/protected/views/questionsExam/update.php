<?php
/* @var $this QuestionsExamController */
/* @var $model QuestionsExam */

$this->breadcrumbs=array(
	'Questions Exams'=>array('index'),
	$model->id_preguntas_examen=>array('view','id'=>$model->id_preguntas_examen),
	'Update',
);

$this->menu=array(
	array('label'=>'Create QuestionsExam', 'url'=>array('create')),
	array('label'=>'Manage QuestionsExam', 'url'=>array('admin')),
);
?>

<h1>Update QuestionsExam <?php echo $model->id_preguntas_examen; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'exam'=>$exam)); ?>
<?php 
$logo="NOLOGO";  
$nombreExamen = $examsUser[0]->idLicenciaUsuario->idLicenciaCliente->idLicencia->nombre;
?>
<p style="text-align:center; clear:both;">
    ACCOUNT INFORMATION <br></p>
<table><tr style="width:100%">
    <?php if($logo != "NOLOGO"){?>
        <td style="width:50%">
    <img style="float:left;" src="<?php echo Yii::app()->getBaseUrl(true)."/images/".$logo; ?>"/>
        </td>
    <?php } ?>
        <td style="width:50%">
    <a href="http://www.iltoexams.com/"><img style="float:right;" alt="ILTO Exams" src="logo_ILTO.png" /></a>
        </td>
    </tr>
</table>
<p>
    <?php echo $model->nombres." ".$model->apellidos;?><br>
</p>
<h2> IMPORTANT:</h2>
<p>
    You have a new account in www.iltoexams.com<br> 
</p>
<p>
<h2>
TO USE YOUR NEW ACCOUNT PLEASE FOLLOW THE STEPS BELLOW:
</h2>
</p>
<ul><li>
        Click the following link:    <br>
        <a target="_blank" href="http://www.iltoexams.com">http://www.iltoexams.com</a>
    </li>
    <li>Once on the page, click on "TEST ADMINISTRATORS" link and use the following information:<br>
        <strong>Company:&nbsp;</strong> <?php echo $model->id_cliente;?><br>
        <strong>Username:&nbsp;</strong> <?php echo $model->id_usuario_c;?><br>
        <strong>Password:&nbsp;</strong> <?php echo $clave;?><br>
    </li></ul>
<p>ILTO EXAMS LLC.  All rights reserved.</p>

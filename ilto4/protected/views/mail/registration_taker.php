<?php 
if($examsUser[0]->idExamen->tipologo==1){
    $logo = "TECS.png";
} else {
    $logo = "RPT.png";
} 
$nombreExamen = $examsUser[0]->idLicenciaUsuario->idLicenciaCliente->idLicencia->nombre;
?>
<p style="text-align:center;">
    INSTRUCCIONES PARA PRESENTAR EL EXÁMEN<br>
<table style="width:100%"><tr style="width:100%">
    <?php if($logo != "NOLOGO"){?>
        <td style="width:50%">
    <img style="float:left;" src="<?php echo Yii::app()->getBaseUrl(true)."/images/".$logo; ?>"/>
        </td>
    <?php } ?>
        <td style="width:50%">
    <a href="http://www.iltoexams.com/"><img style="float:right;" alt="ILTO Exams" src="logo_ILTO.png" /></a>
        </td>
    </tr>
</table>
</p>
<p>
    <?php echo $model->nombres." ".$model->apellidos;?><br>
</p>
<p>
A continuacion la información para presentar su exámen
</p>
<p>
<h2>
REQUERIMIENTOS TÉCNICOS
</h2>
<ul>
    <li>Conexión a internet por cable (No usar modems de servicios de telefonía celular).</li>
    <li>Usar solo Internet Explorer 7.0 o mayor.</li>
    <li>Tener parlantes o audifonos para la sección de escucha. </li>
</ul>
</p>
<p>
<h2> IMPORTANTE:</h2>

La duración aproximada del examen es de 1.5 horas.
El usuario y contraseña solo se pueden usar una sola vez. Si usted ingresa esta información y no presenta el examen o lo presenta incompleto, deberá pagar nuevamente para poder presentar el examen.
El usuario y contraseña tiene una validez de 15 días, despues de este tiempo el examen no estará disponible y deberá pagar nuevamente para poder presentar el examen. 
</p>
<p>
<?php if($login!=0){?>
<h2>
REALICE LOS SIGUIENTES PASOS PARA PRESENTAR EL EXÁMEN:
</h2>
</p>
<ul><li>
        Haga click en el enlace a continuación:    <br>
        <a target="_blank" href="<?php echo Yii::app()->getBaseUrl(true);?>/index.php?r=Clients"><?php echo Yii::app()->getBaseUrl(true);?>/index.php?r=Clients</a>
    </li>
    <li>Una vez se encuentre en la ventana del exámen, ingrese la siguiente información:<br>
        <strong>Company:&nbsp;</strong> <?php echo Yii::app()->user->getState('cliente');?><br>
        <strong>Username:&nbsp;</strong> <?php echo $model->id_usuario_c;?><br>
        <strong>Password:&nbsp;</strong> <?php echo $clave;?><br>
    </li></ul>
<?php } ?>
<p>ILTO EXAMS LLC.  All rights reserved.</p>

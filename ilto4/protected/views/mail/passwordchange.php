<?php 
if(isset($examsUser[0]->idExamen->tipologo)){
    if($examsUser[0]->idExamen->tipologo==1){
        $logo = "TECS.png";
    } else {
        $logo = "RPT.png";
    } 
} else {
  $logo="NOLOGO";  
}
$nombreExamen = $examsUser[0]->idLicenciaUsuario->idLicenciaCliente->idLicencia->nombre;
?>
<p style="text-align:center; clear:both;">
    THE ADMINISTRATOR HAS CHANGED YOUR PASSWORD<br></p>
<table><tr style="width:100%">
    <?php if($logo != "NOLOGO"){?>
        <td style="width:50%">
    <img style="float:left;" src="<?php echo Yii::app()->getBaseUrl(true)."/images/".$logo; ?>"/>
        </td>
    <?php } ?>
        <td style="width:50%">
    <a href="http://www.iltoexams.com/"><img style="float:right;" alt="ILTO Exams" src="logo_ILTO.png" /></a>
        </td>
    </tr>
</table>
<p>
    <?php echo $model->nombres." ".$model->apellidos;?><br>
</p>
<h2> IMPORTANTE:</h2>
<p>
Como respuesta a su requerimiento o por motivo de seguridad, el Administrador ha cambiado su informacion de Inicio de Sesion.
Tenga en cuenta que de ahora en adelante debera iniciar sesion utilizando la siguiente informacion:
</p>
<p>
<h2>
REALICE LOS SIGUIENTES PASOS PARA INGRESAR A LA PLATAFORMA:
</h2>
</p>
<ul><li>
        Haga click en el enlace a continuación:    <br>
        <a target="_blank" href="<?php echo Yii::app()->getBaseUrl(true);?>/index.php?r=Clients"><?php echo Yii::app()->getBaseUrl(true);?>/index.php?r=Clients</a>
    </li>
    <li>Una vez se encuentre en la ventana de inicio de sesion ingrese la siguiente información:<br>
        <strong>Company:&nbsp;</strong> <?php echo Yii::app()->user->getState('cliente');?><br>
        <strong>Username:&nbsp;</strong> <?php echo $model->id_usuario_c;?><br>
        <strong>Password:&nbsp;</strong> <?php echo $clave;?><br>
    </li></ul>
<p>ILTO EXAMS LLC. All rights reserved.</p>

<p>
    The user <?php echo Yii::app()->user->getState('nombres')." ".Yii::app()->user->getState('apellidos')." from ";?><br>
    <?php echo Yii::app()->user->getState('nombre_cliente');?> have completed the Purchase Process.
</p>
<table>
    <tr>
        <td><?php echo $model->getAttributeLabel('amount');?></td>
        <td><?php echo $model->amount;?></td>
    </tr>
    <tr>
        <td>License</td>
        <td><?php echo $licenseName;?></td>
    </tr>
    <tr>
        <td>Purchase Date (D-M-Y): </td>
        <td><?php echo date("d-m-Y");?></td>
    </tr>
    
</table>

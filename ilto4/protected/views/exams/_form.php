<?php
/* @var $this ExamsController */
/* @var $model Exams */
/* @var $form CActiveForm */
$modUtil= new Utilidades();
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'exams-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nivel'); ?>
		<?php
            $dataCombo=$modUtil->arregloNiveles();
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'Exams[nivel]',
            'id'=> 'combo_nivel',
            'data'=>$dataCombo,
                'value'=>$model->nivel,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'120px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'nivel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'preguntas_aleatorias'); ?>
		<?php echo $form->checkBox($model,'preguntas_aleatorias'); ?>
		<?php echo $form->error($model,'preguntas_aleatorias'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'tutor'); ?>
		<?php echo $form->checkBox($model,'tutor'); ?>
		<?php echo $form->error($model,'tutor'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'numero_preguntas'); ?>
		<?php echo $form->textField($model,'numero_preguntas',array('style'=>'width:20px;', 'size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'numero_preguntas'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_seccion'); ?>
		<?php
            $dataCombo=CHtml::listData(Sections::model()->findAllByAttributes(array('estado'=>'A')),'id_seccion','nombre');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'Exams[id_seccion]',
            'id'=> 'combo_seccion',
            'data'=>$dataCombo,
                'value'=>$model->id_seccion,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'200px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'id_seccion'); ?>
	</div>

        <div class="row">
		<?php echo $form->labelEx($model,'vistaunoauno'); ?>
		<?php echo $form->checkBox($model,'vistaunoauno'); ?>
		<?php echo $form->error($model,'vistaunoauno'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estado'); ?>
		<?php
            $dataCombo=array('A'=>'Enabled','I'=>'Disabled');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'Exams[estado]',
            'id'=> 'combo_estado',
            'data'=>$dataCombo,
                'value'=>$model->estado,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'120px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'estado'); ?>
	</div>

	<div class="row buttons" style="clear:left;">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php
/* @var $this ExamsController */
/* @var $model Exams */
$modUtil=new Utilidades();

$this->breadcrumbs=array(
	'Exams'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create Exams', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#exams-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Exams</h1>



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'exams-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'nombre',
            array(
		'name'=>'nivel',
                'filter'=>$modUtil->arregloNiveles(),
                ),
                array(
                    'name'=>'preguntas_aleatorias',
                    'value'=>'(($data->preguntas_aleatorias))?CHtml::image(Yii::app()->baseUrl."/images/iconos/ok.png","",array("style"=>"max-width:20px;")):CHtml::image(Yii::app()->baseUrl."/images/iconos/delete.png","",array("style"=>"max-width:20px;"))',                    
                    'filter'=>'',
                    'type'=>'raw',
                    ),
                array(
                    'name'=>'id_seccion',
                    'value'=>'$data->idSeccion->nombre',
                    'filter'=>CHtml::listData(Sections::model()->findAll(),"id_seccion","nombre"),
                ),
                array(
                    'name'=>'estado',
                    'value'=>'($data->estado=="A" ? "Enabled" : "Disabled")',
                    'filter'=>array("A"=>"Enable","I"=>"Disable"),
                    ),
                array(
                    'name'=>'tutor',
                    'value'=>'(($data->tutor))?CHtml::image(Yii::app()->baseUrl."/images/iconos/ok.png","",array("style"=>"max-width:20px;")):CHtml::image(Yii::app()->baseUrl."/images/iconos/delete.png","",array("style"=>"max-width:20px;"))',                    
                    'filter'=>'',
                    'type'=>'raw',
                    ),
		'numero_preguntas',
		array(
			'class'=>'CButtonColumn',
                    'template'=>'{update}{delete}{questions}',
                    'buttons'=>array(
                        'delete'=>array(
                            'label'=>'Delete&nbsp;',
                            'imageUrl'=>NULL
                        ),
                        'update'=>array(
                            'label'=>'Edit&nbsp;',
                            'imageUrl'=>NULL
                        ),
                        'questions'=>array(
                            'url'=> 'Yii::app()->createUrl("questionsExam/admin",array("exam"=>$data->id_examen))',
                            'imageUrl' => NULL,
                            'label'=>'Questions',
                            'visible'=>'($data->preguntas_aleatorias=="1")?false:true',
                            
                        )
                    )
		),
	),
)); ?>

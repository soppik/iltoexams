<?php
/* @var $this ExamsController */
/* @var $model Exams */
$this->layout="column0";
$modUtil=new Utilidades();

Yii::app()->clientScript->registerScript('llevar', "

function enviar(){        
        var atLeastOneIsChecked = $('input[name=\"exams-grid_c0[]\"]:checked').length > 0;

        if (atLeastOneIsChecked)
        {
                document.getElementById('exams-search-form').action='index.php?r=examsLicense/addAll/license/".$license."';
                document.getElementById('exams-search-form').submit();
        }else{
            alert('At least One have to be checked');
        }
}");
?>

<h1>Add Exams</h1>

<?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'exams-search-form',
        'enableAjaxValidation'=>false,
        'htmlOptions'=>array('enctype' => 'multipart/form-data')
));
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'exams-grid',
	'dataProvider'=>$model->search_license($license),
        'selectableRows'=>2,
	'filter'=>$model,
	'columns'=>array(
                array(
                'value'=>'$data->id_examen',
                'class'=>'CCheckBoxColumn',
                ),
		'nombre',
            array(
		'name'=>'nivel',
                'filter'=>$modUtil->arregloNiveles(),
                ),
                array(
                    'name'=>'preguntas_aleatorias',
                    'value'=>'(($data->preguntas_aleatorias))?CHtml::image(Yii::app()->baseUrl."/images/iconos/ok.png","",array("style"=>"max-width:20px;")):CHtml::image(Yii::app()->baseUrl."/images/iconos/delete.png","",array("style"=>"max-width:20px;"))',                    
                    'filter'=>'',
                    'type'=>'raw',
                    ),
                array(
                    'name'=>'id_seccion',
                    'value'=>'$data->idSeccion->nombre',
                    'filter'=>CHtml::listData(Sections::model()->findAll(),"id_seccion","nombre"),
                ),
                array(
                    'name'=>'estado',
                    'value'=>'($data->estado=="A" ? "Enabled" : "Disabled")',
                    'filter'=>array("A"=>"Enable","I"=>"Disable"),
                    ),
		'numero_preguntas',
	),
)); ?>
<?php $this->endWidget(); ?>
<a href="javascript:enviar();">Select & Close</a>
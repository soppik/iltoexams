<?php
/* @var $this ClientsController */
/* @var $model Clients */

$this->breadcrumbs=array(
	'Clients'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create Clients', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#clients-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Clients</h1>



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'clients-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_cliente',
		'nombre_rsocial',
		'direccion',
                array(
                    'name'=>'id_pais',
                    'value'=>'$data->idPais->nombre',
                    'filter'=>CHtml::listData(Countries::model()->findAll(),"id_pais","nombre"),
                ),
                array(
                    'name'=>'id_ciudad',
                    'value'=>'$data->idCiudad->nombre',
                    'filter'=>CHtml::listData(Cities::model()->findAll(),"id_ciudad","nombre"),
                ),
		'nit_id_legal',
            array(
                'name'=>'ruta_logo',
                'filter'=>'',
                'value'=>'(!empty($data->ruta_logo))?CHtml::image(Yii::app()->baseUrl."/images/clients/tn1_".$data->ruta_logo,"",array("class"=>"imagen", "href"=>Yii::app()->baseUrl."/images/clients/tn3_$data->ruta_logo","style"=>"width:150px !important; max-width:100% !important;")):"no image"',
                'type'=>'raw'
                ),
		/*
		'telefono',
		'contacto',
		'email',
		'estado',
		'logo_en_certificado',
		*/
		array(
			'class'=>'CButtonColumn',
                    'template'=>'{view}{update}{delete}',
                    'buttons'=>array(
                        'view'=>array(
                            'label'=>'View&nbsp;',
                            'imageUrl'=>NULL
                        ),
                        'delete'=>array(
                            'label'=>'Delete&nbsp;',
                            'imageUrl'=>NULL
                        ),
                        'update'=>array(
                            'label'=>'Edit&nbsp;',
                            'imageUrl'=>NULL
                        )
                    )
		),
	),
)); ?>

<?php
/* @var $this ClientsController */
/* @var $model Clients */
/* @var $form CActiveForm */
?>

<div class="form">

<?php 
if(!$model->isNewRecord){
    if(is_null($model->id_distributor)){
        $model->id_distributor = '-1';
    }
} else {
        $model->id_distributor = '-1';
}

$form=$this->beginWidget('CActiveForm', array(
	'id'=>'clients-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>


	<div class="row">
		<?php echo $form->labelEx($model,'nombre_rsocial'); ?>
		<?php echo $form->textField($model,'nombre_rsocial',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'nombre_rsocial'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'direccion'); ?>
		<?php echo $form->textField($model,'direccion',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'direccion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_pais'); ?>
		<?php
            $dataCombo=CHtml::listData(Countries::model()->findAllByAttributes(array('estado'=>'A')),'id_pais','nombre');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'Clients[id_pais]',
            'id'=> 'combo_paises',
            'data'=>$dataCombo,
                'value'=>$model->id_pais,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'200px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'id_pais'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_ciudad'); ?>
            <select name="Clients[id_ciudad]" id="Clients_id_ciudad"></select>
		<?php echo $form->error($model,'id_ciudad'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nit_id_legal'); ?>
		<?php echo $form->textField($model,'nit_id_legal',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'nit_id_legal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telefono'); ?>
		<?php echo $form->textField($model,'telefono',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'telefono'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contacto'); ?>
		<?php echo $form->textField($model,'contacto',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'contacto'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'ruta_logo'); ?>
		<?php echo $form->fileField($model,'ruta_logo',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'ruta_logo'); ?>
            <?php if(!$model->isNewRecord){
                if(strlen($model->ruta_logo)>0){
                    echo "<img src='images/clients/tn2_$model->ruta_logo' style='width:200px;'>";
                    echo "<input type='hidden' name='ruta_imagen_actual' value='$model->ruta_logo'>";
                    echo "&nbsp;&nbsp;<input type='checkbox' name='delete_imagen' value='1'>&nbsp;Delete Image on Save";
                }
            } ?>
            
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'logo_en_certificado'); ?>
		<?php echo $form->checkBox($model,'logo_en_certificado'); ?>
		<?php echo $form->error($model,'logo_en_certificado'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'is_distributor'); ?>
		<?php
            $dataCombo=array('1'=>'Yes','2'=>'No');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'Clients[is_distributor]',
            'id'=> 'combo_isdistri',
            'data'=>$dataCombo,
                'value'=>$model->is_distributor,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'120px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'is_distributor'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'id_distributor'); ?>
		<?php
            $dataCombo=CHtml::listData(Clients::model()->distributors()->findAllByAttributes(array('estado'=>'A')),'id_cliente','nombre_rsocial');
            $dataCombo['-1']='ILTO';
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'Clients[id_distributor]',
            'id'=> 'combo_distribuitors',
            'data'=> $dataCombo,
                'value'=>$model->id_distributor,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'200px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'id_distributor'); ?>
	</div>

<?php if(!$model->isNewRecord){?>
	<div class="row">
		<?php echo $form->labelEx($model,'examen_asistido'); ?>
		<?php
            $dataCombo=array('1'=>'Enabled','2'=>'Disabled');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'Clients[examen_asistido]',
            'id'=> 'combo_asistido',
            'data'=>$dataCombo,
                'value'=>$model->examen_asistido,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'200px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'estado'); ?>
	</div>
<?php } ?>
	<div class="row">
		<?php echo $form->labelEx($model,'estado'); ?>
		<?php
            $dataCombo=array('A'=>'Enabled','I'=>'Disabled');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'Clients[estado]',
            'id'=> 'combo_estado',
            'data'=>$dataCombo,
                'value'=>$model->estado,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'200px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'estado'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $('#combo_paises').change( function() {
        debugger;
        var valor = $('#combo_paises').val();
       var url = "index.php?r=cities/citiesCountry/country/"+valor;
    $.getJSON( url, {
        format: "json"
      }).done(function( data ) {
          debugger;
          $('#Clients_id_ciudad').length=0;
          var salida= [];
          $.each( data, function( i, item ) {
              salida.push('<option value="'+i+'">'+ item+"</option>");
          });
          $('#Clients_id_ciudad').html(salida.join(''));
        }); 
    });
        <?php 
    if(!$model->isNewRecord){
        echo "$('#combo_paises').trigger('change');";
    }
    ?>

    </script>
<?php
/* @var $this CitiesController */
/* @var $model Cities */

$this->breadcrumbs=array(
	'Cities'=>array('index'),
	$model->id_ciudad=>array('view','id'=>$model->id_ciudad),
	'Update',
);

$this->menu=array(
	array('label'=>'Create Cities', 'url'=>array('create')),
	array('label'=>'Manage Cities', 'url'=>array('admin')),
);
?>

<h1>Update Cities <?php echo $model->id_ciudad; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
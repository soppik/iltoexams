<?php
/* @var $this CitiesController */
/* @var $model Cities */

$this->breadcrumbs=array(
	'Cities'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create Cities', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#cities-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Cities</h1>



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'cities-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
                array(
                    'name'=>'id_pais',
                    'value'=>'$data->idPais->nombre',
                    'filter'=>CHtml::listData(Countries::model()->findAll(),"id_pais","nombre"),
                ),
		'nombre',
                array(
                    'name'=>'estado',
                    'value'=>'($data->estado=="A" ? "Enabled" : "Disabled")',
                    'filter'=>array("A"=>"Enable","I"=>"Disable"),
                    ),
		array(
			'class'=>'CButtonColumn',
                    'template'=>'{update}{delete}',
                    'buttons'=>array(
                        'delete'=>array(
                            'label'=>'Delete&nbsp;',
                            'imageUrl'=>NULL
                        ),
                        'update'=>array(
                            'label'=>'Edit&nbsp;',
                            'imageUrl'=>NULL
                        )
                    )
                    
		),
	),
)); ?>

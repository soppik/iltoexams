<?php
/* @var $this LicensesController */
/* @var $model Licenses */

$this->breadcrumbs=array(
	'Licenses'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create Licenses', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#licenses-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Licenses</h1>



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'licenses-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'nombre',
                array(
                    'name'=>'estado',
                    'value'=>'($data->estado=="A" ? "Enabled" : "Disabled")',
                    'filter'=>array("A"=>"Enable","I"=>"Disable"),
                    ),
                array(
                    'name'=>'venta',
                    'value'=>'(($data->venta))?CHtml::image(Yii::app()->baseUrl."/images/iconos/ok.png","",array("style"=>"max-width:20px;")):CHtml::image(Yii::app()->baseUrl."/images/iconos/delete.png","",array("style"=>"max-width:20px;"))',                    
                    'filter'=>'',
                    'type'=>'raw',
                    ),
                array(
                    'name'=>'genera_certificado',
                    'value'=>'(($data->genera_certificado))?CHtml::image(Yii::app()->baseUrl."/images/iconos/ok.png","",array("style"=>"max-width:20px;")):CHtml::image(Yii::app()->baseUrl."/images/iconos/delete.png","",array("style"=>"max-width:20px;"))',                    
                    'filter'=>'',
                    'type'=>'raw',
                    ),
		array(
			'class'=>'CButtonColumn',
                    'template'=>'{update}{delete}{examenes}',
                    'buttons'=>array(
                        'delete'=>array(
                            'label'=>'Delete&nbsp;',
                            'imageUrl'=>NULL
                        ),
                        'update'=>array(
                            'label'=>'Edit&nbsp;',
                            'imageUrl'=>NULL
                        ),
                        'examenes'=>array(
                            'url'=> 'Yii::app()->createUrl("examsLicense/admin",array("license"=>$data->id_licencia))',
                            'imageUrl' => NULL,
                            'label'=>'Exams&nbsp;',
                        )
                    ),
		),
	),
)); ?>

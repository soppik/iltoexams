<?php
/* @var $this LicensesController */
/* @var $model Licenses */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'licenses-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estado'); ?>
		<?php
            $dataCombo=array('A'=>'Enabled','I'=>'Disabled');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'Licenses[estado]',
            'id'=> 'combo2',
            'data'=>$dataCombo,
                'value'=>$model->estado,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'120px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'estado'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'venta'); ?>
		<?php echo $form->checkBox($model,'venta'); ?>
		<?php echo $form->error($model,'venta'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'genera_certificado'); ?>
		<?php echo $form->checkBox($model,'genera_certificado'); ?>
		<?php echo $form->error($model,'genera_certificado'); ?>
	</div>

        <div class="row">
		<?php echo $form->labelEx($model,'intro_general'); ?>
		<?php echo $form->checkBox($model,'intro_general'); ?>
		<?php echo $form->error($model,'intro_general'); ?>
	</div>
        <div class="row">
		<?php echo $form->labelEx($model,'intro_secciones'); ?>
		<?php echo $form->checkBox($model,'intro_secciones'); ?>
		<?php echo $form->error($model,'intro_secciones'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'logo'); ?>
		<?php
                $modUtil = new Utilidades();
            $dataCombo= $modUtil->arregloLogoExamen();
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'Licenses[logo]',
            'id'=> 'combo_tipologo',
            'data'=>$dataCombo,
                'value'=>$model->logo,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'120px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'logo'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
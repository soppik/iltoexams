<?php
/* @var $this LicensesController */
/* @var $model Licenses */

$this->breadcrumbs=array(
	'Licenses'=>array('index'),
	$model->id_licencia,
);

$this->menu=array(
	array('label'=>'List Licenses', 'url'=>array('index')),
	array('label'=>'Create Licenses', 'url'=>array('create')),
	array('label'=>'Update Licenses', 'url'=>array('update', 'id'=>$model->id_licencia)),
	array('label'=>'Delete Licenses', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_licencia),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Licenses', 'url'=>array('admin')),
);
?>

<h1>View Licenses #<?php echo $model->id_licencia; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_licencia',
		'nombre',
		'estado',
		'genera_certificado',
	),
)); ?>

<?php
/* @var $this PerfilesController */
/* @var $model Perfiles */

$this->menu=array(
	array('label'=>'Back to Profile Administrator', 'url'=>array('admin')),
);
?>

<h1>Update user's Profile <?php echo $model->nombre; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
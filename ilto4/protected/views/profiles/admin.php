<?php
/* @var $this PerfilesController */
/* @var $model Perfiles */


$this->menu=array(
	array('label'=>'Create Profile', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('profiles-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Profiles</h1>

<div align="right">
 
<?php
$dataProvider = $model->search();
 ?>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'perfiles-grid',
	'dataProvider'=>$dataProvider,
	'filter'=>$model,
	'columns'=>array(
		'id_perfil',
		'nombre',
                array(
                    'name'=>'estado',
                    'value'=>'($data->estado=="A" ? "Enabled" : "Disabled")',
                    'filter'=>array("A"=>"Enable","I"=>"Disable"),
                    ),
		array(
			'class'=>'CButtonColumn',
                    'template'=>'{update}{delete}',
                    'buttons'=>array(
                        'update'=>array(
                            'imageUrl'=>NULL,
                            'label'=>'Edit&nbsp;'
                            ),
                        'delete'=>array(
                            'label'=>'Delete&nbsp;',
                            'imageUrl'=>NULL
                            ),
                        )
		),
	),
)); ?>

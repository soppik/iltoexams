<?php
/* @var $this AgentsController */
/* @var $data Agents */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('prefix')); ?>:</b>
	<?php echo CHtml::encode($data->prefix); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_name')); ?>:</b>
	<?php echo CHtml::encode($data->first_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_name')); ?>:</b>
	<?php echo CHtml::encode($data->last_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('job_title')); ?>:</b>
	<?php echo CHtml::encode($data->job_title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('department')); ?>:</b>
	<?php echo CHtml::encode($data->department); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
	<?php echo CHtml::encode($data->phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('prefix_1')); ?>:</b>
	<?php echo CHtml::encode($data->prefix_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_name_1')); ?>:</b>
	<?php echo CHtml::encode($data->first_name_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_name_1')); ?>:</b>
	<?php echo CHtml::encode($data->last_name_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('job_title_1')); ?>:</b>
	<?php echo CHtml::encode($data->job_title_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('department_1')); ?>:</b>
	<?php echo CHtml::encode($data->department_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email_1')); ?>:</b>
	<?php echo CHtml::encode($data->email_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone_1')); ?>:</b>
	<?php echo CHtml::encode($data->phone_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('prefix_2')); ?>:</b>
	<?php echo CHtml::encode($data->prefix_2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_name_2')); ?>:</b>
	<?php echo CHtml::encode($data->first_name_2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_name_2')); ?>:</b>
	<?php echo CHtml::encode($data->last_name_2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('job_title_2')); ?>:</b>
	<?php echo CHtml::encode($data->job_title_2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('department_2')); ?>:</b>
	<?php echo CHtml::encode($data->department_2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email_2')); ?>:</b>
	<?php echo CHtml::encode($data->email_2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone_2')); ?>:</b>
	<?php echo CHtml::encode($data->phone_2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('country')); ?>:</b>
	<?php echo CHtml::encode($data->country); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('city')); ?>:</b>
	<?php echo CHtml::encode($data->city); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('institution_name')); ?>:</b>
	<?php echo CHtml::encode($data->institution_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address_1')); ?>:</b>
	<?php echo CHtml::encode($data->address_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address_2')); ?>:</b>
	<?php echo CHtml::encode($data->address_2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address_3')); ?>:</b>
	<?php echo CHtml::encode($data->address_3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('state')); ?>:</b>
	<?php echo CHtml::encode($data->state); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('zip')); ?>:</b>
	<?php echo CHtml::encode($data->zip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('organization_type')); ?>:</b>
	<?php echo CHtml::encode($data->organization_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alter_country')); ?>:</b>
	<?php echo CHtml::encode($data->alter_country); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alter_city')); ?>:</b>
	<?php echo CHtml::encode($data->alter_city); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alter_iinstitution_name')); ?>:</b>
	<?php echo CHtml::encode($data->alter_iinstitution_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alter_address_1')); ?>:</b>
	<?php echo CHtml::encode($data->alter_address_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alter_address_2')); ?>:</b>
	<?php echo CHtml::encode($data->alter_address_2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alter_address_3')); ?>:</b>
	<?php echo CHtml::encode($data->alter_address_3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alter_state')); ?>:</b>
	<?php echo CHtml::encode($data->alter_state); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alter_zip')); ?>:</b>
	<?php echo CHtml::encode($data->alter_zip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_other_agent')); ?>:</b>
	<?php echo CHtml::encode($data->is_other_agent); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('organization_tests')); ?>:</b>
	<?php echo CHtml::encode($data->organization_tests); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('organization_references')); ?>:</b>
	<?php echo CHtml::encode($data->organization_references); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('years_operating')); ?>:</b>
	<?php echo CHtml::encode($data->years_operating); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('years_location')); ?>:</b>
	<?php echo CHtml::encode($data->years_location); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('secure_rooms')); ?>:</b>
	<?php echo CHtml::encode($data->secure_rooms); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('computers_room1')); ?>:</b>
	<?php echo CHtml::encode($data->computers_room1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('computers_room2')); ?>:</b>
	<?php echo CHtml::encode($data->computers_room2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('computers_room3')); ?>:</b>
	<?php echo CHtml::encode($data->computers_room3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('net_type_room1')); ?>:</b>
	<?php echo CHtml::encode($data->net_type_room1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('net_type_room2')); ?>:</b>
	<?php echo CHtml::encode($data->net_type_room2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('net_type_room3')); ?>:</b>
	<?php echo CHtml::encode($data->net_type_room3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('partitions')); ?>:</b>
	<?php echo CHtml::encode($data->partitions); ?>
	<br />

	*/ ?>

</div>
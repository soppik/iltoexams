<?php
/* @var $this AgentsController */
/* @var $model Agents */
/* @var $form CActiveForm */
?>
<style>
    label {
  float: left;
  margin-right: 10px;
  font-size: 105%;
  width: 180px;
}
.row{
    width:100%;
    clear:both;
}
</style>
<h3>Thank you for your interest in becoming a Certified Test Agent</h3>
<p><strong>Please complete this form for ILTO to consider your organization to become a Certified Test Agent.</strong></p>
<div class="form">

<?php 
$modUtil= new Utilidades();
$prefixes = $modUtil->arregloPrefijos();

if($model->isnewrecord){
$model->main_decision = 1;
$model->same_address = 1;
$model->is_other_agent = 2;
}

$form=$this->beginWidget('CActiveForm', array(
	'id'=>'agents-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

        <p><br><br><strong>YOUR INFORMATION</strong></p>
    <div style="width:1024px;">
        <div style="width:500px; float:left;">
	<div class="row">
		<?php echo $form->labelEx($model,'prefix'); ?>
                <?php echo $form->dropDownList($model,'prefix', $prefixes, array('prompt'=>'Select prefix')); ?>
		<?php echo $form->error($model,'prefix'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'first_name'); ?>
		<?php echo $form->textField($model,'first_name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'first_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'last_name'); ?>
		<?php echo $form->textField($model,'last_name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'last_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'job_title'); ?>
		<?php echo $form->textField($model,'job_title',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'job_title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'department'); ?>
		<?php echo $form->textField($model,'department',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'department'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model,'phone',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>
        <div class="row">
            Are you the main decision maker in determining if your organization wants to become a TECS Authorized Agent?
        </div>
	<div class="row">
		<?php echo $form->labelEx($model,'main_decision'); ?>
		<?php echo $form->dropDownList($model,'main_decision', array('1'=> 'Yes', '2'=> 'No')); ?>
		<?php echo $form->error($model,'main_decision'); ?>
	</div>
            

<div class="row" id="main_person" style="display:none;">        
    <p><strong>Please complete the information below:</strong></p>
	<div class="row">
		<?php echo $form->labelEx($model,'prefix_1'); ?>
                <?php echo $form->dropDownList($model,'prefix_1', $prefixes, array('prompt'=>'Select prefix')); ?>
		<?php echo $form->error($model,'prefix_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'first_name_1'); ?>
		<?php echo $form->textField($model,'first_name_1',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'first_name_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'last_name_1'); ?>
		<?php echo $form->textField($model,'last_name_1',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'last_name_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'job_title_1'); ?>
		<?php echo $form->textField($model,'job_title_1',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'job_title_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'department_1'); ?>
		<?php echo $form->textField($model,'department_1',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'department_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email_1'); ?>
		<?php echo $form->textField($model,'email_1',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'email_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phone_1'); ?>
		<?php echo $form->textField($model,'phone_1',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'phone_1'); ?>
	</div>
</div>
        
<div id="additional_user" style="clear:both;">        
    <p><strong>Please list an alternate contact for your institution:</strong></p>
	<div class="row">
		<?php echo $form->labelEx($model,'prefix_2'); ?>
                <?php echo $form->dropDownList($model,'prefix_2', $prefixes, array('prompt'=>'Select prefix')); ?>
		<?php echo $form->error($model,'prefix_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'first_name_2'); ?>
		<?php echo $form->textField($model,'first_name_2',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'first_name_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'last_name_2'); ?>
		<?php echo $form->textField($model,'last_name_2',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'last_name_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'job_title_2'); ?>
		<?php echo $form->textField($model,'job_title_2',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'job_title_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'department_2'); ?>
		<?php echo $form->textField($model,'department_2',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'department_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email_2'); ?>
		<?php echo $form->textField($model,'email_2',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'email_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phone_2'); ?>
		<?php echo $form->textField($model,'phone_2',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'phone_2'); ?>
	</div>

    <div class="row">
		<?php echo $form->labelEx($model,'logo'); ?>
		<?php echo $form->fileField($model,'logo'); ?>
		<?php echo $form->error($model,'logo'); ?>
	</div>
    
</div>
        </div> <div style="width:500px; float:left;">
        <div class="row"><p><strong>Test Site Location</strong><br><br>Please fill out the following boxes regarding the organization location </p></div>
        
	<div class="row">
		<?php echo $form->labelEx($model,'country'); ?>
		<?php
            $dataCombo=CHtml::listData(Countries::model()->findAllByAttributes(array('estado'=>'A')),'id_pais','nombre');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'Agents[country]',
            'id'=> 'combo_paises',
            'data'=>$dataCombo,
                'value'=>$model->country,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'200px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'country'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'city'); ?>
            <select name="Agents[city]" id="Agents_city"></select>
		<?php echo $form->error($model,'city'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'institution_name'); ?>
		<?php echo $form->textField($model,'institution_name',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'institution_name'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'id_legal'); ?>
		<?php echo $form->textField($model,'id_legal',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'id_legal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address_1'); ?>
		<?php echo $form->textField($model,'address_1',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'address_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address_2'); ?>
		<?php echo $form->textField($model,'address_2',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'address_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address_3'); ?>
		<?php echo $form->textField($model,'address_3',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'address_3'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'state'); ?>
		<?php echo $form->textField($model,'state',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'state'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'zip'); ?>
		<?php echo $form->textField($model,'zip',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'zip'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'organization_type'); ?>
		<?php echo $form->textField($model,'organization_type',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'organization_type'); ?>
	</div>
        <div class="row">
            Is the shipping address similar to the Test Site Location?
        </div>
	<div class="row">
		<?php echo $form->labelEx($model,'same_address'); ?>
		<?php echo $form->dropDownList($model,'same_address', array('1'=> 'Yes', '2'=> 'No')); ?>
		<?php echo $form->error($model,'same_address'); ?>
	</div>
        <div id="alter_location" style="display:none;">
	<div class="row">
		<?php echo $form->labelEx($model,'alter_country'); ?>
		<?php echo $form->textField($model,'alter_country'); ?>
		<?php echo $form->error($model,'alter_country'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alter_city'); ?>
		<?php echo $form->textField($model,'alter_city'); ?>
		<?php echo $form->error($model,'alter_city'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alter_iinstitution_name'); ?>
		<?php echo $form->textField($model,'alter_iinstitution_name',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'alter_iinstitution_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alter_address_1'); ?>
		<?php echo $form->textField($model,'alter_address_1',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'alter_address_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alter_address_2'); ?>
		<?php echo $form->textField($model,'alter_address_2',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'alter_address_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alter_address_3'); ?>
		<?php echo $form->textField($model,'alter_address_3',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'alter_address_3'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alter_state'); ?>
		<?php echo $form->textField($model,'alter_state',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'alter_state'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'alter_zip'); ?>
		<?php echo $form->textField($model,'alter_zip',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'alter_zip'); ?>
	</div>
</div>
	<div class="row">
		<?php echo $form->labelEx($model,'is_other_agent'); ?>
		<?php echo $form->dropDownList($model,'is_other_agent', array('1'=> 'Yes', '2'=> 'No')); ?>
		<?php echo $form->error($model,'is_other_agent'); ?>
	</div>
<div id="other_tests" class="row" style="display:none;">
	<div class="row">
		<?php echo $form->labelEx($model,'organization_tests'); ?>
		<?php echo $form->textArea($model,'organization_tests',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'organization_tests'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'organization_references'); ?>
		<?php echo $form->textArea($model,'organization_references',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'organization_references'); ?>
	</div>
</div>
	<div class="row">
		<?php echo $form->labelEx($model,'years_operating'); ?>
		<?php echo $form->textField($model,'years_operating'); ?>
		<?php echo $form->error($model,'years_operating'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'years_location'); ?>
		<?php echo $form->textField($model,'years_location'); ?>
		<?php echo $form->error($model,'years_location'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'secure_rooms'); ?>
		<?php echo $form->textField($model,'secure_rooms'); ?>
		<?php echo $form->error($model,'secure_rooms'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'computers_room1'); ?>
		<?php echo $form->textField($model,'computers_room1'); ?>
		<?php echo $form->error($model,'computers_room1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'computers_room2'); ?>
		<?php echo $form->textField($model,'computers_room2'); ?>
		<?php echo $form->error($model,'computers_room2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'computers_room3'); ?>
		<?php echo $form->textField($model,'computers_room3'); ?>
		<?php echo $form->error($model,'computers_room3'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'net_type_room1'); ?>
		<?php echo $form->textField($model,'net_type_room1'); ?>
		<?php echo $form->error($model,'net_type_room1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'net_type_room2'); ?>
		<?php echo $form->textField($model,'net_type_room2'); ?>
		<?php echo $form->error($model,'net_type_room2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'net_type_room3'); ?>
		<?php echo $form->textField($model,'net_type_room3'); ?>
		<?php echo $form->error($model,'net_type_room3'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'partitions'); ?>
		<?php echo $form->textField($model,'partitions'); ?>
		<?php echo $form->error($model,'partitions'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>
        </div>
    </div>
        
<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
    $('#combo_paises').change( function() {
        debugger;
        var valor = $('#combo_paises').val();
       var url = "/ilto3/index.php?r=cities/citiesCountry/country/"+valor;
    $.getJSON( url, {
        format: "json"
      }).done(function( data ) {
          debugger;
          $('#Agents_city').length=0;
          var salida= [];
          $.each( data, function( i, item ) {
              salida.push('<option value="'+i+'">'+ item+"</option>");
          });
          $('#Agents_city').html(salida.join(''));
        }); 
    });
    $('#Agents_main_decision').on('click',function(){
        if($('#Agents_main_decision').val()==2){
            $('#main_person').show();
        } else {
            $('#main_person').hide();
        }
    });
    $('#Agents_same_address').on('click',function(){
        if($('#Agents_same_address').val()==2){
            $('#alter_location').show();
        } else {
            $('#alter_location').hide();
        }
    });
    $('#Agents_is_other_agent').on('click',function(){
        if($('#Agents_is_other_agent').val()==1){
            $('#other_tests').show();
        } else {
            $('#other_tests').hide();
        }
    });
    
</script>

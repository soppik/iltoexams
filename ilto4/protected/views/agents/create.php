<?php
/* @var $this AgentsController */
/* @var $model Agents */

$this->breadcrumbs=array(
	'Agents'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Agents', 'url'=>array('admin')),
);
?>

<h1>Agents Registration Form</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
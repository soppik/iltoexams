<?php
/* @var $this ExamsLicenseController */
/* @var $model ExamsLicense */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'exams-license-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_licencia'); ?>
		<?php echo $form->textField($model,'id_licencia'); ?>
		<?php echo $form->error($model,'id_licencia'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_examen'); ?>
		<?php echo $form->textField($model,'id_examen'); ?>
		<?php echo $form->error($model,'id_examen'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'es_placement'); ?>
		<?php echo $form->textField($model,'es_placement'); ?>
		<?php echo $form->error($model,'es_placement'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php
/* @var $this ExamsLicenseController */
/* @var $model ExamsLicense */

$this->breadcrumbs=array(
	'Exams Licenses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage ExamsLicense', 'url'=>array('admin')),
);
?>

<h1>Create ExamsLicense</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
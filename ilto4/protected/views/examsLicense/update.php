<?php
/* @var $this ExamsLicenseController */
/* @var $model ExamsLicense */

$this->breadcrumbs=array(
	'Exams Licenses'=>array('index'),
	$model->id_examenes_licencia=>array('view','id'=>$model->id_examenes_licencia),
	'Update',
);

$this->menu=array(
	array('label'=>'Create ExamsLicense', 'url'=>array('create')),
	array('label'=>'Manage ExamsLicense', 'url'=>array('admin')),
);
?>

<h1>Update ExamsLicense <?php echo $model->id_examenes_licencia; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
/* @var $this ExamsLicenseController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Exams Licenses',
);

$this->menu=array(
	array('label'=>'Create ExamsLicense', 'url'=>array('create')),
	array('label'=>'Manage ExamsLicense', 'url'=>array('admin')),
);
?>

<h1>Exams Licenses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

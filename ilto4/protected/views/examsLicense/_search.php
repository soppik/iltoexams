<?php
/* @var $this ExamsLicenseController */
/* @var $model ExamsLicense */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_examenes_licencia'); ?>
		<?php echo $form->textField($model,'id_examenes_licencia'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_licencia'); ?>
		<?php echo $form->textField($model,'id_licencia'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_examen'); ?>
		<?php echo $form->textField($model,'id_examen'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'es_placement'); ?>
		<?php echo $form->textField($model,'es_placement'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
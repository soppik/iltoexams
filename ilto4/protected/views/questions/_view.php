<?php
/* @var $this QuestionsController */
/* @var $data Questions */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_pregunta')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_pregunta), array('view', 'id'=>$data->id_pregunta)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nivel')); ?>:</b>
	<?php echo CHtml::encode($data->nivel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_presentacion')); ?>:</b>
	<?php echo CHtml::encode($data->tipo_presentacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('texto')); ?>:</b>
	<?php echo CHtml::encode($data->texto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_seccion')); ?>:</b>
	<?php echo CHtml::encode($data->id_seccion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ruta_imagen')); ?>:</b>
	<?php echo CHtml::encode($data->ruta_imagen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ruta_video')); ?>:</b>
	<?php echo CHtml::encode($data->ruta_video); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('ruta_audio')); ?>:</b>
	<?php echo CHtml::encode($data->ruta_audio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado')); ?>:</b>
	<?php echo CHtml::encode($data->estado); ?>
	<br />

	*/ ?>

</div>
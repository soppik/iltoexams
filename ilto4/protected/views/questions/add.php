<?php
/* @var $this QuestionsController */
/* @var $model Questions */
$modUtil=new Utilidades();
$this->layout="column0";
?>
<style>
    audio{
        width:150px; 
    }
</style>
<?php Yii::app()->clientScript->registerScript('llevar', "

function enviar(){        
        var atLeastOneIsChecked = $('input[name=\"questions-grid_c0[]\"]:checked').length > 0;

        if (atLeastOneIsChecked)
        {
                document.getElementById('questions-search-form').action='index.php?r=questionsExam/addAll/exam/".$exam."';
                document.getElementById('questions-search-form').submit();
        }else{
            alert('At least One have to be checked');
        }
}");
?>
<h1>Select Questions</h1>

<?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'questions-search-form',
        'enableAjaxValidation'=>false,
        'htmlOptions'=>array('enctype' => 'multipart/form-data')
));
?>

<?php 

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'questions-grid',
    'afterAjaxUpdate'=>"function(){ $('.imagen').fancybox(); }",
	'dataProvider'=>$model->search_exam($exam),
        'selectableRows'=>2,
	'filter'=>$model,
	'columns'=>array(
                array(
                'value'=>'$data->id_pregunta',
                'class'=>'CCheckBoxColumn',
                ),
            array(
		'name'=>'nivel',
                'filter'=>$modUtil->arregloNiveles(),
                ),
            array(
		'name'=>'tipo_presentacion',
                'filter'=>$modUtil->arregloPresentaciones(),
                'value'=>'$data->nombrePresenta',
                ),
	   
            array(
                'name'=>'texto',
                'type'=>'raw'
            ),
                array(
                    'name'=>'id_seccion',
                    'value'=>'$data->idSeccion->nombre',
                    'filter'=>'',
                ),
            array(
                'name'=>'ruta_imagen',
                'filter'=>'',
                'value'=>'(!empty($data->ruta_imagen))?CHtml::image(Yii::app()->baseUrl."/images/questions/tn1_".$data->ruta_imagen,"",array("class"=>"imagen", "href"=>Yii::app()->baseUrl."/images/questions/tn3_$data->ruta_imagen","style"=>"width:150px !important; max-width:100% !important;")):"no image"',
                'type'=>'raw'
                ),
            array(
                'name'=>'ruta_audio',
                'filter'=>'',
                'type'=>'raw',
                'value'=>'(!empty($data->ruta_audio))? "<audio controls><source src=\'$data->ruta_audio\' type=\'audio/wav\'></audio>" : "No Audio"',
            ),
                array(
                    'name'=>'estado',
                    'value'=>'($data->estado=="A" ? "Enabled" : "Disabled")',
                    'filter'=>array("A"=>"Enable","I"=>"Disable"),
                    ),
	),
)); 

$config = array( 
    'scrolling' => 'yes', 
    'titleShow' => false,
    'overlayColor' => '#000',
    'padding' => '10',
    'showCloseButton' => true,
       'width'=>'500px',
        // change this as you need
    );
    $this->widget('application.extensions.fancybox.EFancyBox', array('target'=>'.imagen', 'config'=>$config));

?>
<?php $this->endWidget(); ?>
<a href="javascript:enviar();">Select & Close</a>
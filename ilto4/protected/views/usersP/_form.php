<?php
/* @var $this UsersPController */
/* @var $model UsersP */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-p-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_usuario_p'); ?>
		<?php echo $form->textField($model,'id_usuario_p',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'id_usuario_p'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_perfil'); ?>
		<?php
            $dataCombo=CHtml::listData(Profiles::model()->findAllByAttributes(array('estado'=>'A')),'id_perfil','nombre');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'UsersP[id_perfil]',
            'id'=> 'combo_perfil',
            'data'=>$dataCombo,
                'value'=>$model->id_perfil,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'200px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'id_perfil'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'clave'); ?>
		<?php echo $form->passwordField($model,'clave',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'clave'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'clave2'); ?>
		<?php echo $form->passwordField($model,'clave2',array('size'=>20,'maxlength'=>20,'value'=>$model->clave)); ?>
		<?php echo $form->error($model,'clave2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estado'); ?>
		<?php
            $dataCombo=array('A'=>'Enabled','I'=>'Disabled');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'UsersP[estado]',
            'id'=> 'combo_estado',
            'data'=>$dataCombo,
                'value'=>$model->estado,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'120px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'estado'); ?>
	</div>

	<div class="row buttons">
		<?php 
                if($model->isNewRecord){
                    $model->ultimo_acceso="0000-00-00 00:00:00";
                }
                echo $form->hiddenField($model,'ultimo_acceso');
                echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
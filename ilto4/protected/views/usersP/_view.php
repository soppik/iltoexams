<?php
/* @var $this UsersPController */
/* @var $data UsersP */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_usuario_p')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_usuario_p), array('view', 'id'=>$data->id_usuario_p)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_perfil')); ?>:</b>
	<?php echo CHtml::encode($data->id_perfil); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ultimo_acceso')); ?>:</b>
	<?php echo CHtml::encode($data->ultimo_acceso); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('clave')); ?>:</b>
	<?php echo CHtml::encode($data->clave); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado')); ?>:</b>
	<?php echo CHtml::encode($data->estado); ?>
	<br />


</div>
<?php
/* @var $this UsersCController */
/* @var $model UsersC */
/* @var $form CActiveForm */
$modUtil = new Utilidades();
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-c-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
	<div class="row">
		<?php echo $form->labelEx($model,'id_cliente'); ?>
		<?php
            $dataCombo=CHtml::listData(Clients::model()->findAllByAttributes(array('estado'=>'A')),'id_cliente','nombre_rsocial');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'UsersC[id_cliente]',
            'id'=> 'combo_clientes',
            'data'=>$dataCombo,
                'value'=>$model->id_cliente,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'200px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'id_cliente'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'nombres'); ?>
		<?php echo $form->textField($model,'nombres',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'nombres'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'apellidos'); ?>
		<?php echo $form->textField($model,'apellidos',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'apellidos'); ?>
	</div>

        <div class="row">
		<?php echo $form->labelEx($model,'id_pais'); ?>
		<?php
            $dataCombo=CHtml::listData(Countries::model()->findAllByAttributes(array('estado'=>'A')),'id_pais','nombre');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'UsersC[id_pais]',
            'id'=> 'combo_paises',
            'data'=>$dataCombo,
                'value'=>$model->id_pais,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'200px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'id_pais'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_ciudad'); ?>
            <select name="UsersC[id_ciudad]" id="UsersC_id_ciudad"></select>
		<?php echo $form->error($model,'id_ciudad'); ?>
	</div>

        <div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_perfil'); ?>
		<?php
            $dataCombo=CHtml::listData(Profiles::model()->findAllByAttributes(array('estado'=>'A')),'id_perfil','nombre');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'UsersC[id_perfil]',
            'id'=> 'combo_perfil',
            'data'=>$dataCombo,
                'value'=>$model->id_perfil,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'200px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'id_perfil'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'numero_id'); ?>
		<?php echo $form->textField($model,'numero_id',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'numero_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ruta_foto'); ?>
		<?php echo $form->fileField($model,'ruta_foto',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'ruta_foto'); ?>
            <?php if(!$model->isNewRecord){
                if(strlen($model->ruta_foto)>0){
                    echo "<img src='images/users/tn2_$model->ruta_foto' style='width:200px;'>";
                    echo "<input type='hidden' name='ruta_imagen_actual' value='$model->ruta_foto'>";
                    echo "&nbsp;&nbsp;<input type='checkbox' name='delete_imagen' value='1'>&nbsp;Delete Image on Save";
                }
            } ?>
	</div>
<?php if(!$model->isNewRecord){?>
	<div class="row">
		<?php echo $form->labelEx($model,'estado'); ?>
		<?php
            $dataCombo=array('A'=>'Enabled','I'=>'Disabled');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'UsersC[estado]',
            'id'=> 'combo_estado',
            'data'=>$dataCombo,
                'value'=>$model->estado,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'120px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'estado'); ?>
	</div>
<?php } ?>
	<div class="row buttons">
            
		<?php 
                if($model->isNewRecord){
                    $model->ultimo_acceso="0000-00-00 00:00:00";
                }
                echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
    $('#combo_paises').change( function() {
        debugger;
        var valor = $('#combo_paises').val();
        var url = "index.php?r=UsersC/citiesCountry/country/"+valor;
    $.getJSON( url, {
        format: "json"
      }).done(function( data ) {
          debugger;
          $('#UsersC_id_ciudad').length=0;
          var salida= [];
          $.each( data, function( i, item ) {
              salida.push('<option value="'+i+'">'+ item+"</option>");
          });
          $('#UsersC_id_ciudad').html(salida.join(''));
        }); 
    });
    $('#UsersC_ruta_foto').bind('change', function() {
        var tamanio = this.files[0].size/1024/1024;
        if(tamanio > 2){
            alert('The image excedes the size limit. Please select an image up to 2 Mb');
            $('#UsersC_ruta_foto').val("");
            document.getElementById('UsersC_ruta_foto').value = '';
        }
    });
    
    
    <?php 
    if(!$model->isNewrecord){
        echo "$('#combo_paises').trigger('change');";
    }
    ?>
    </script>
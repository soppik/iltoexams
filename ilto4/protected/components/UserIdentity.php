<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
                $users = UsersP::model()->findByAttributes(array('id_usuario_p'=>$this->username));
                if($users !== NULL){
                    if(!isset($this->username)){
                            $this->errorCode=self::ERROR_USERNAME_INVALID;
                    }
                    elseif($users->clave!==md5($this->password)){
                            $this->errorCode=self::ERROR_PASSWORD_INVALID;
                    }
                    else{
                            Yii::app()->user->setState('usuario', $this->username);
                            Yii::app()->user->setState('nombres_apellidos', $users->nombre);
                            Yii::app()->user->setState('id_perfil', $users->id_perfil);
                            Yii::app()->user->setState('nombre_perfil', Profiles::model()->findByPk($users->id_perfil)->nombre);
                            $this->errorCode=self::ERROR_NONE;
                    }
                } else {
                    $this->errorCode=self::ERROR_USERNAME_INVALID;
                }
                return !$this->errorCode;
	}
}
<?php
class Utilidades extends CFormModel
{

    public function crearThumbNail($name,$filename,$new_w,$new_h){
	$system=explode(".",$name);
	if (preg_match("/jpg|jpeg/",$system[1])){$src_img=imagecreatefromjpeg($name);}
	if (preg_match("/png/",$system[1])){$src_img=imagecreatefrompng($name);}
	$old_x=imagesx($src_img);
	$old_y=imagesy($src_img);
	if ($old_x > $old_y) 
	{
		$thumb_w=$new_w;
		$thumb_h=$old_y*($new_h/$old_x);
	}
	if ($old_x < $old_y) 
	{
		$thumb_w=$old_x*($new_w/$old_y);
		$thumb_h=$new_h;
	}
	if ($old_x == $old_y) 
	{
		$thumb_w=$new_w;
		$thumb_h=$new_h;
	}
	$dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);
	imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y); 
	if (preg_match("/png/",$system[1]))
	{
		imagepng($dst_img,$filename); 
	} else {
		imagejpeg($dst_img,$filename); 
	}
	imagedestroy($dst_img); 
	imagedestroy($src_img); 
    }
    
    public function generaThumb($ruta,$name){
        //A partir de la imagen generar las 4 imágenes 
        $nombre = $ruta."/".$name;
        $tn1 = $ruta."/tn1_".$name;
        $tn2 = $ruta."/tn2_".$name;
        $tn3 = $ruta."/tn3_".$name;
        $this->crearThumbNail($nombre, $tn1, 100, 100);
        $this->crearThumbNail($nombre, $tn2, 250, 250);
        $this->crearThumbNail($nombre, $tn3, 600, 600);
        //Eliminar el archivo original
        @unlink($nombre);
    }
    public function eliminarThumbs($ruta,$name){
        //A partir de la imagen generar las 4 imágenes 
        $tn1 = $ruta."/tn1_".$name;
        $tn2 = $ruta."/tn2_".$name;
        $tn3 = $ruta."/tn3_".$name;
        @unlink($tn1);
        @unlink($tn2);
        @unlink($tn3);
    }
     public function arregloPrefijos(){
        return array('Ms'=>'Ms','Miss'=>'Miss','Mrs'=>'Mrs','Mr'=>'Mr','Dr'=>'Dr');
    }
    public function arregloNiveles(){
        return array('-A1'=>'-A1','A1'=>'A1','A2'=>'A2','B1'=>'B1','B2'=>'B2','C1'=>'C1','C2'=>'C2');
    }
    public function arregloPresentaciones(){
        return array('M'=>'Multiple Selection','D'=>'DropDown','S'=>'Scale');
    } 
    public function arregloMeses(){
        return array('1'=>'Jan','2'=>'Feb','3'=>'Mar','4'=>'Apr','5'=>'May','6'=>'Jun','7'=>'Jul','8'=>'Aug','9'=>'Sep','10'=>'Oct','11'=>'Nov','12'=>'Dec');
    }
    public function arregloLogoExamen(){
        return array('1'=>'TECS','2'=>'RPT','3'=>'EDE');
    }
    public function arregloTiposDocumento(){
        return array('C'=>'Cedula','T'=>'Tarjeta de Identidad','N'=>'Nit');
    }
    public function arregloColores(){
        return array('#31B744'=>'Green','#FFDA74'=>'Yellow','#1363DC'=>'Blue','#9055CF'=>'Purple','#EE5959'=>'Red','#FFFFFF'=>'White');
    }
    public function arregloTemplates(){
        return array('1'=>'Question Left Fixed // Answers Right','2'=>'Question Top // Answers Below','3'=>'In Line (Scale Questions)','4'=>'Media on Top // Answers Below (Dropdown Questions)','5'=>'Question Left Fluid // Answers Right','6'=>'Question Left // Answers Right 1 column');
    }
    public function tieneTutor($id){ //Saber si la licencia asignada al usuario tiene examenes que exijan tutor
        $modLicenciasCliente = LicensesClient::model()->findByPk($id);
        $retorno = array('estutor'=>'0','nombre'=>'');
        if(!is_null($modLicenciasCliente)){
            $modExamenes = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>$modLicenciasCliente->id_licencia));
            if(!is_null($modExamenes)){
                foreach($modExamenes as $examen){
                    if($examen->idExamen->tutor == 1){
                        $retorno['estutor']="1";
                        $retorno['nombre']=$examen->idExamen->nombre;
                    }
                }
            }
        }
        return json_encode($retorno);
    }
    public function generaPassword($modelo){
        //Se recibe el modelo del Usuario al que se le quiere generar la cave
        //Tomar las 2 primeras letras del Nombre
        $clave = substr($modelo->nombres,1,2);
        $clave .= $this->randomString(6);
        $clave .= substr($modelo->apellidos,1,2);
        return $clave;
    }
    function randomString($numero) {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789.!-*";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $numero; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
    return implode($pass); //turn the array into a string
    }
    public function actionEnviarRegistro($userId,$login=0,$tipo=0){
        $model= UsersC::model()->findByAttributes(array('id_usuario_c'=>$userId)); 
        //La clave es el ID del usuario, generalmente la cédula
        $clave=$userId;
        $model->clave=md5($clave);
        $model->clave2=md5($clave);
        $model->save(true,array('clave'));
        if($tipo==0){ //Taker con info del Examen
            //Enviar correo al usuario 
            $licenciaUsuario = LicensesUser::model()->findByAttributes(array('id_usuario'=>$userId,'estado'=>'A'));
            $examsUser = ExamsUser::model()->findAllByAttributes(array('id_licencia_usuario'=>$licenciaUsuario->id_licencias_usuario));
            $mail = new YiiMailer('registration', array('login'=>$login, 'model' => $model,'description'=>'Registration process','header_title'=>'ILTO - ADMINISTRATOR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ACCOUNT INFORMATION','clave'=>$clave,'examsUser'=>$examsUser));
            //set properties
            $mail->setFrom("registration@iltoexams.com", "ILTO Exams");
            $mail->setSubject("Registration complete");
            $mail->setTo($model->email);
        }
        if($tipo==1){  //Registro a un usuario normal con login
            $mail = new YiiMailer('registration', array('login'=>$login, 'model' => $model,'description'=>'Registration process','header_title'=>'ILTO - ADMINISTRATOR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ACCOUNT INFORMATION','clave'=>$clave,));
            //set properties
            $mail->setFrom("registration@iltoexams.com", "ILTO Exams");
            $mail->setSubject("Registration process");
            $mail->setTo($model->email);
        }
        if($tipo>1){
            $mail = new YiiMailer('passwordchange', array('login'=>1, 'model' => $model,'description'=>'Reset Password','clave'=>$clave));
            //set properties
            $mail->setFrom("support@iltoexams.com", "ILTO Exams");
            $mail->setSubject("You have requested a new password");
            $mail->setTo($model->email);
        }
        //send
        if ($mail->send()) {
                Yii::app()->user->setFlash('success','An Email was sent to '.$model->email.' with the Login Information.');
        } else {
                Yii::app()->user->setFlash('error','Error while sending email: '.$mail->getError());
        }
    }

    public function enviarCuenta($userId,$login=0){
        $model= UsersC::model()->findByAttributes(array('id_usuario_c'=>$userId)); 
        $clave=$model->id_usuario_c;
        $model->clave=md5($clave);
        $model->clave2=md5($clave);
        $model->save(true,array('clave'));
        $mail = new YiiMailer('registration', array('login'=>1, 'model' => $model,'header_title'=>'ILTO - ADMINISTRATOR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ACCOUNT INFORMATION','clave'=>$clave));
        //set properties
        $mail->setFrom("support@iltoexams.com", "ILTO Exams");
        $mail->setSubject("New Account Information");
        $mail->setTo($model->email);
        //send
        if ($mail->send()) {
                Yii::app()->user->setFlash('success','An Email was sent to '.$model->email.' with the Login Information.');
        } else {
                Yii::app()->user->setFlash('error','Error while sending email: '.$mail->getError());
        }
    }

    public function fotoUsuario(){
        $foto=Yii::app()->basePath."/../images/users/tn1_".Yii::app()->user->getState('ruta_foto');
        if(file_exists("$foto")){
              echo '<img class="imgUser" src="images/users/tn1_'.Yii::app()->user->getState("ruta_foto").'"/>';
         } else {
              echo '<img class="imgUser" src="images/no_photo.png"/>';
        } 
    }
  
    function draw_calendar($month,$year){

	/* draw table */
	$calendar = '<table cellpadding="0" cellspacing="0" class="calendar">';

	/* table headings */
	$headings = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
	$calendar.= '<tr class="calendar-row"><td class="calendar-day-head">'.implode('</td><td class="calendar-day-head">',$headings).'</td></tr>';

	/* days and weeks vars now ... */
	$running_day = date('w',mktime(0,0,0,$month,1,$year));
	$days_in_month = date('t',mktime(0,0,0,$month,1,$year));
	$days_in_this_week = 1;
	$day_counter = 0;
	$dates_array = array();

	/* row for week one */
	$calendar.= '<tr class="calendar-row">';

	/* print "blank" days until the first of the current week */
	for($x = 0; $x < $running_day; $x++):
		$calendar.= '<td class="calendar-day-np"> </td>';
		$days_in_this_week++;
	endfor;

	/* keep going with days.... */
	for($list_day = 1; $list_day <= $days_in_month; $list_day++):
		$calendar.= '<td class="calendar-day">';
			/* add in the day number */
			$calendar.= '<div class="day-number">'.$list_day.'</div>';
			$calendar.= '<div id="divday_'.(($list_day<10)?"0".$list_day : $list_day).'" class="day-content"></div>';
			
		$calendar.= '</td>';
		if($running_day == 6):
			$calendar.= '</tr>';
			if(($day_counter+1) != $days_in_month):
				$calendar.= '<tr class="calendar-row">';
			endif;
			$running_day = -1;
			$days_in_this_week = 0;
		endif;
		$days_in_this_week++; $running_day++; $day_counter++;
	endfor;

	/* finish the rest of the days in the week */
	if($days_in_this_week < 8):
		for($x = 1; $x <= (8 - $days_in_this_week); $x++):
			$calendar.= '<td class="calendar-day-np"> </td>';
		endfor;
	endif;

	/* final row */
	$calendar.= '</tr>';

	/* end the table */
	$calendar.= '</table>';
	
	/* all done, return result */
	return $calendar;
    }
    
    public function nivelExamen($totALPROMEDIO){
        if(((($totALPROMEDIO)>=0)&&(($totALPROMEDIO)<=12)))
        {
                       $TECSPROFICIENCY ="0";
                       $CEFBYTECS ="- A1";
        }
        if(((($totALPROMEDIO)>12)&&(($totALPROMEDIO)<=31)))
        {
                       $TECSPROFICIENCY ="1";
                       $CEFBYTECS ="A1";

        }
        if(((($totALPROMEDIO)>31)&&(($totALPROMEDIO)<=55)))
        {
                       $TECSPROFICIENCY ="2";
                       $CEFBYTECS ="A2";
        }
        if(((($totALPROMEDIO)>55)&&(($totALPROMEDIO)<=74)))
        {
                       $TECSPROFICIENCY ="3";
                       $CEFBYTECS ="B1";
        }
        if(((($totALPROMEDIO)>74)&&(($totALPROMEDIO)<=89)))
        {
                       $TECSPROFICIENCY ="4";
                       $CEFBYTECS ="B2";
        }
        if(((($totALPROMEDIO)>89)&&(($totALPROMEDIO)<=100)))
        {
                       $TECSPROFICIENCY ="5";
                       $CEFBYTECS ="C1";
        }
        return $CEFBYTECS;
    }

        function levelExplain($id){
                $texto="";
                switch($id){
                    case 'A1':
                        $texto="Can understand and use familiar everyday expressions and very basic phrases aimed at the satisfaction of needs of a concrete type. Can introduce him/herself and others and can ask and answer questions about personal details such as where he/she lives, people he/she knows and things he/she has. Can interact in a simple way provided the other person talks slowly and clearly and is prepared to help.";
                        break;
                    case 'A2':
                        $texto="Can understand sentences and frequently used expressions related to areas of most immediate relevance (e.g. very basic personal and family information, shopping, local geography, employment). Can communicate in simple and routine tasks requiring a simple and direct exchange of information on familiar and routine matters. Can describe in simple terms aspects of his/her background, immediate environment and matters in areas of immediate need.";
                        break;
                    case 'B1':
                        $texto="Can understand the main points of clear standard input on familiar matters
        regularly encountered in work, school, leisure, etc. Can deal with most
        situations likely to arise whilst travelling in an area where the language is
        spoken. Can produce simple connected text on topics which are familiar or of
        personal interest. Can describe experiences and events, dreams, hopes and
        ambitions and briefly give reasons and explanations for opinions and plans";
                        break;
                    case 'B2':
                        $texto="Can understand the main ideas of complex text on both concrete and
        abstract topics, including technical discussions in his/her field of
        specialisation. Can interact with a degree of fluency and spontaneity that
        makes regular interaction with native speakers quite possible without strain
        for either party. Can produce clear, detailed text on a wide range of subjects
        and explain a viewpoint on a topical issue giving the advantages and
        disadvantages of various options.";
                        break;
                    case 'C1':
                        $texto="Can understand a wide range of demanding, longer texts, and recognise
        implicit meaning. Can express him/herself fluently and spontaneously
        without much obvious searching for expressions. Can use language flexibly
        and effectively for social, academic and professional purposes. Can produce
        clear, well-structured, detailed text on complex subjects, showing controlled
        use of organisational patterns, connectors and cohesive devices.";
                        break;
                    case 'C2':
                        $texto=" Can understand with ease virtually everything heard or read. Can summarise
        information from different spoken and written sources, reconstructing
        arguments and accounts in a coherent presentation. Can express him/herself
        spontaneously, very fluently and precisely, differentiating finer shades of
        meaning even in more complex situations.";
                        break;

                }
                return $texto;
    }    
    function generaCertificado($id){
        /***************************************************/
        $modExamsUser = ExamsUser::model()->findAllByAttributes(array('id_licencia_usuario'=>$id));
        $logo= Yii::app()->getBaseUrl(true)."/images/clients/tn1_".Yii::app()->user->getState('logo_cliente');
        $KEYUSERexam=(1000000 + $modExamsUser[0]->id_licencia_usuario);
        $KEYUSERexam = "L".substr($KEYUSERexam,1);
        $idUsuario = $modExamsUser[0]->idLicenciaUsuario->id_usuario;
        $nombreUsuario = $modExamsUser[0]->idLicenciaUsuario->idUsuario->nombres."&nbsp;".$modExamsUser[0]->idLicenciaUsuario->idUsuario->apellidos;
        $imagen = Yii::app()->getBaseUrl(true)."/images/users/tn2_".$modExamsUser[0]->idLicenciaUsuario->idUsuario->ruta_foto;
        $fecha = Yii::app()->dateFormatter->formatDateTime($modExamsUser[0]->fecha_presentacion,"long",NULL)."&nbsp;&nbsp;".$modExamsUser[0]->hora;
        $TRRESULTADOS='';
        $TABLARESULTADOS='<style></style><table width="100%" style="border:#FC0 2px solid"><tr><td width="50%" style="border-right:#FC0 2px solid"  bgcolor="#F2F2F2"><table align="center"  border="0" cellpadding="0" cellspacing="0"  >
            <tr>
              <td colspan="3"  align="center"><strong>MODULE</strong></td>
              <td  >&nbsp;</td>
              <td ><strong>SCORE</strong></td>
              </tr>';
                  $CONT=0;
                  $tot=0;
                  foreach($modExamsUser as $examen){
                          $CONT=$CONT+1;
                          $TRRESULTADOS .=' <tr>
                          <td width="6%"  bgcolor="#F2F2F2" align="center"><strong>'.$CONT.'</strong></td>
                          <td width="6%"  bgcolor="#F2F2F2">&nbsp;</td>
                          <td width="60%"  bgcolor="#F2F2F2" style="text-align:left;">'.$examen->idExamen->nombre.'</td>
                          <td width="5%"  bgcolor="#F2F2F2">&nbsp;</td>
                          <td width="18%" align="center"  bgcolor="#F2F2F2"><strong>'.$examen->calificacion.'</strong></td>
                          </tr>';					
                          $tot+=$examen->calificacion;
                  }
                  $totALPROMEDIO=$tot/$CONT;
                  if(((($totALPROMEDIO)>=0)&&(($totALPROMEDIO)<=12)))
                     {
                          $TECSPROFICIENCY ="0";
                          $CEFBYTECS ="- A1";
                     }
                  if(((($totALPROMEDIO)>12)&&(($totALPROMEDIO)<=31)))
                     {
                          $TECSPROFICIENCY ="1";
                          $CEFBYTECS ="A1";

                     }
                  if(((($totALPROMEDIO)>31)&&(($totALPROMEDIO)<=55)))
                     {
                          $TECSPROFICIENCY ="2";
                          $CEFBYTECS ="A2";
                     }
                  if(((($totALPROMEDIO)>55)&&(($totALPROMEDIO)<=74)))
                     {
                          $TECSPROFICIENCY ="3";
                          $CEFBYTECS ="B1";
                     }
                  if(((($totALPROMEDIO)>74)&&(($totALPROMEDIO)<=89)))
                     {
                          $TECSPROFICIENCY ="4";
                          $CEFBYTECS ="B2";
                     }
                  if(((($totALPROMEDIO)>89)&&(($totALPROMEDIO)<=100)))
                     {
                          $TECSPROFICIENCY ="5";
                          $CEFBYTECS ="C1";
                     }
              $total_scoremail=round($tot/$CONT,2);
                  $TABLARESULTADOS.=$TRRESULTADOS.'</TABLE></td><td  style="background-color:#E6B1BD;"><table  height="96" border="0" align="center" cellpadding="0" cellspacing="0" >
            <tr>
              <td align="center"><strong>Total Score %</strong></td>
              <td align="center"><strong>'.$total_scoremail.'</strong></td>
              </tr>
              <tr><td>&nbsp;</td></tr>
            <tr>
              <td width="67%" align="center"><strong>TECS Proficiency Stage </strong></td>
              <td width="33%" align="center"><strong>'.$TECSPROFICIENCY .'</strong></td>
              </tr>
              <tr><td>&nbsp;</td></tr>
            <tr>
              <td align="center"><strong>CEF</strong></td>
              <td align="center">'.$CEFBYTECS.'</td>
            </tr>
              </table></td></table>';
              $TABLARESULTADOSTOTALES='<table><tr><td width="50%">
<table  height="175" border="0" align="center" cellpadding="0" cellspacing="0" style="border:#FC0 2px solid;font-size:14px">
  <tr>
    <td height="59" align="center"  bgcolor="#EAEAEA"><strong>TECS PROFICIENCY STAGE</strong></td>
    <td  bgcolor="#FFFFFF" align="center"><strong>TECS<br>
      SCORE<br>
    </strong></td>
    <td  bgcolor="#EAEAEA" align="center"><strong>	CEFR<br>
      SCORE
    </strong></td>
    </tr>
  <tr>
    <td width="45%"  bgcolor="#EAEAEA" align="center">0<br>
      1<br>
      2<br>
      3<br>
      4<br>
      5</td>
    <td width="29%"  bgcolor="#FFFFFF" align="center">0-12<br>
      13-31<br>
      31-55<br>
      56-74<br>
      75-89<br>
      90-100</td>
    <td width="26%" height="47"  bgcolor="#EAEAEA" align="center">- A1<br>
      A1<br>
      A2<br>
      B1<br>
      B2<br>
      C1</td>
    </tr>
    </table></td><td style="vertical-align:top"><table border="0" style="font-size:12px; text-align: justify;" ><tr style="background-color:#E6B1BD;"><td width="100%"><h2><center>IMPORTANT INFORMATION</center></h2></td></tr><tr><td  bgcolor="#F2F2F2">
              According to The Common European Framework of Reference for Languages your level corresponds to:<br>
              <strong>'.$CEFBYTECS.':&nbsp;</strong>&quot;<i>'.$this->levelExplain($CEFBYTECS).'</i>&quot;
                  </td></tr></table></td></tr></table>              
<p style="font-family:Arial, Helvetica, sans-serif; font-size:10px;"></p>
          ';
                          $TABLARESULTADOSTOTALCOMPLETA=
                          '
                          <table width="100%" border="0" style="border:#FC0 1px solid; background-position:center; background-repeat:no-repeat; background-position:" background="backgroundtecs.jpg">
            <tr>
              <td height="900" valign="top">

              <table width="98%" border="0" cellpadding="0" cellspacing="0" style="border:#FC0 2px solid" align="center">
            <tr border="1" style="border:#FC0 1px solid">
              <td  width="50%" bgcolor="#F2F2F2" style="text-align:left"><img src="'.Yii::app()->getBaseUrl(true).'/images/TECS.png" width="120" style="margin:5px"></td>
              <td  height="95" bgcolor="#F2F2F2" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size:24px; font-weight:bold" >SCORE REPORT FORM </td>
            </tr>
          </table>
              <br>';
               if(Yii::app()->user->getState('logo_en_certificado')==1){
                  $TABLARESULTADOSTOTALCOMPLETA.= '
                      <table width="98%" border="0" cellpadding="0" cellspacing="0" style="border:#FC0 2px solid;font-size:13px" align="center">
            <tr>
              <td width="15%"  bgcolor="#FFFFFF"><strong>Test Administrator</strong></td>
              <td width="1%"  bgcolor="#F2F2F2">&nbsp;</td>
              <td width="30%"  bgcolor="#F2F2F2">'.Yii::app()->user->getState('nombre_cliente').'</td>
              <td width="2%"  bgcolor="#FFFFFF">&nbsp;</td>
              <td width="15%"  bgcolor="#FFFFFF"><strong>Adm. ID</strong></td>
              <td width="2%"  bgcolor="#F2F2F2">&nbsp;</td>
              <td width="15%"  bgcolor="#F2F2F2">'.Yii::app()->user->getState('cliente').'</td>
              <td width="20%" rowspan="2"><center><img  src="'.$logo.'"></center></td>     
            </tr>
            <tr>
              <td  bgcolor="#FFFFFF"><strong>Test date</strong></td>
              <td  bgcolor="#F2F2F2">&nbsp;</td>
              <td  bgcolor="#F2F2F2">'.$fecha.'</td>
              <td  bgcolor="#FFFFFF">&nbsp;</td>
              <td  bgcolor="#FFFFFF"><strong>Country</strong></td>
              <td  bgcolor="#F2F2F2">&nbsp;</td>
              <td  bgcolor="#F2F2F2">Colombia</td>
            </tr>
              </table>';
               } else {
                  $TABLARESULTADOSTOTALCOMPLETA.= '<table width="98%" border="0" cellpadding="0" cellspacing="0" style="border:#FC0 2px solid;font-size:13px" align="center">
            <tr>
              <td width="23%"  bgcolor="#FFFFFF"><strong>Test Administrator</strong></td>
              <td width="1%"  bgcolor="#F2F2F2">&nbsp;</td>
              <td width="43%"  bgcolor="#F2F2F2">'.Yii::app()->user->getState('nombre_cliente').'</td>
              <td width="2%"  bgcolor="#FFFFFF">&nbsp;</td>
              <td width="13%"  bgcolor="#FFFFFF"><strong>Adm. ID</strong></td>
              <td width="1%"  bgcolor="#F2F2F2">&nbsp;</td>
              <td width="17%"  bgcolor="#F2F2F2">'.Yii::app()->user->getState('cliente').'</td>
            </tr>
            <tr>
              <td  bgcolor="#FFFFFF"><strong>Test date</strong></td>
              <td  bgcolor="#F2F2F2">&nbsp;</td>
              <td  bgcolor="#F2F2F2">'.$fecha.'</td>
              <td  bgcolor="#FFFFFF">&nbsp;</td>
              <td  bgcolor="#FFFFFF"><strong>Country</strong></td>
              <td  bgcolor="#F2F2F2">&nbsp;</td>
              <td  bgcolor="#F2F2F2">Colombia</td>
            </tr>
              </table>';
                   
               }   
               $TABLARESULTADOSTOTALCOMPLETA .= ' 
              <br>
              <br>
              <div style="width:200px; margin:auto;">
              <center>
                <strong>TEST TAKER INFORMATION </strong>
              </center></div><br>
          <table width="98%" border="0" cellpadding="0"  cellspacing="0" style="border:#FC0 2px solid;font-size:15px" align="center">
            <tr>
              <td width="30%" style="text-align:center;"><img style="max-height:200px;" src="'.$imagen.'" ></td> 
              <td width="33%" bgcolor="#F2F2F2">
              <table ><tr>    
              <td>&nbsp;</td>
              <td rowspan="1" ><strong>Full Name</strong></td>
              <td >&nbsp;</td>
              <td rowspan="1" >'.$nombreUsuario.'</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><strong>ID</strong></td>
              <td>&nbsp;</td>
              <td>'.$idUsuario.'</td>
            </tr>
            <tr>
              <td >&nbsp;</td>
              <td ><strong>First Language</strong></td>
              <td >&nbsp;</td>
              <td >Spanish</td>
            </tr></table></td>
            <td width="33%"><table   border="0" align="center" cellpadding="2" cellspacing="0" style="font-size:14px">
            <tr>
              <td height="60" align="center"  bgcolor="#FFFFFF"><strong style="font-size:20px; font-family:Arial, Helvetica, sans-serif; font-weight:bold">Fraud Prevention Code</strong></td>
              </tr>
            <tr>
              <td  height="40" align="center"  bgcolor="#FFFFFF" style="font-size:20px; font-family:Arial, Helvetica, sans-serif; font-weight:bold">'.$KEYUSERexam.'</td>
              </tr>
              <tr><td style="font-size:10px; font-weight:bold;"  bgcolor="#FFFFFF">To verify this is a valid test, please enter the code in the fraud prevention code section in the website www.iltoexams.com</td></tr>
              </table></td>
            </tr>
              </table>          <br>              
<center>
            <strong>TEST RESULTS
            </strong>
          </center>
          <br>
          '.$TABLARESULTADOS.'    <br>
              '.$TABLARESULTADOSTOTALES.'
           </td>
              </tr>
          </table><p style=" font-size:10px; ">The TECS &ndash; Test of English Communication Skills is a product of ILTO &ndash; International Language Testing Organization. All rights Reserved&copy; </p>
<span style="font-size:11px"></span>';
        /***************************************************/
        return $TABLARESULTADOSTOTALCOMPLETA;        
    }
    
    
    public function enviarCertificado($idUsuario,$id){
        $modUsuario = UsersC::model()->findByAttributes(array('id_cliente'=>Yii::app()->user->getState('cliente'), 'id_usuario_c'=>$idUsuario));
        $informe = $this->generaCertificado($id);
        $mPDF1 = Yii::app()->ePdf->mpdf();
        $mPDF1->WriteHTML($informe);
        $nombre = "cert_".$idUsuario.".pdf";
        $content = $mPDF1->Output('','S');
        $content = chunk_split(base64_encode($content));
        $mailto = $modUsuario->email; //Mailto here
        $from_name = 'ILTO'; //Name of sender mail
        $from_mail = 'info@iltoexams.com'; //Mailfrom here
        $subject = 'TECS Results for - '.$modUsuario->id_usuario_c.' - '.$modUsuario->nombres.' '.$modUsuario->apellidos; 
        $message = '<p><h1 style="color:black;">Dear '.$modUsuario->nombres.'</h1></p><p style="font-size:20px;">Thank you for taking the <span style="color:#F69A40; font-weight:bold;">TECS</span> (Test of English Communication Skills).
We are glad you are one of the thousands of candidates who take this test in order to know their English communication lever from a reliable source.<br>
Please contact your test administrator for a copy of the test certificate with your results</p>
<p style="font-size:18px;"><br><br><br>Best regards,<br><br>Matt Kettering<br>Head of Academic Support<br><img src="http://www.iltoexams.com/ilto3/images/logo_ILTO.png"/></p>';
        $filename = "cert_".$modUsuario->id_usuario_c.".pdf";
        //Headers of PDF and e-mail
        $boundary = "XYZ-" . date("dmYis") . "-ZYX"; 
        $header = "--$boundary\r\n"; 
        $header .= "Content-Transfer-Encoding: 8bits\r\n"; 
        $header .= "Content-Type: text/html; charset=ISO-8859-1\r\n\r\n"; // or utf-8
        $header .= "$message\r\n";
        $header .= "--$boundary\r\n";
        $header .= "Content-Type: application/pdf; name=\"".$filename."\"\r\n";
        $header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n";
        $header .= "Content-Transfer-Encoding: base64\r\n\r\n";
        $header .= "$content\r\n"; 
        $header .= "--$boundary--\r\n";
        $header2 = "MIME-Version: 1.0\r\n";
        $header2 .= "From: ".$from_name." <".$from_mail."> \r\n"; 
        $header2 .= "Return-Path: $from_mail\r\n";
        $header2 .= "Content-type: multipart/mixed; boundary=\"$boundary\"\r\n";
        $header2 .= "$boundary\r\n";
        if(mail($mailto,$subject,$header,$header2, "-r".$from_mail)){
            Yii::app()->user->setFlash('success','An Email was sent to '.$modUsuario->email.' with the certificate');
            return true;
        } else {
            $error = print_r(error_get_last(),true);
            Yii::app()->user->setFlash('error',$error);
            return false;
        }
        
    } 
    
    public function calificaLicencia($license){
        $CONT=0;
        $tot=0;
        $totalPROMEDIO=0;
        $CEFBYTECS="";
        $modExamsUser = ExamsUser::model()->findAllByAttributes(array('id_licencia_usuario'=>$license));
        foreach($modExamsUser as $examen){
                $CONT=$CONT+1;
                $tot+=$examen->calificacion;
        }
        if($CONT>0){
            $totALPROMEDIO=$tot/$CONT;
            if(((($totALPROMEDIO)>=0)&&(($totALPROMEDIO)<=12)))
               {
                    $CEFBYTECS ="- A1";
               }
            if(((($totALPROMEDIO)>12)&&(($totALPROMEDIO)<=31)))
               {
                    $CEFBYTECS ="A1";

               }
            if(((($totALPROMEDIO)>31)&&(($totALPROMEDIO)<=55)))
               {
                    $CEFBYTECS ="A2";
               }
            if(((($totALPROMEDIO)>55)&&(($totALPROMEDIO)<=74)))
               {
                    $CEFBYTECS ="B1";
               }
            if(((($totALPROMEDIO)>74)&&(($totALPROMEDIO)<=89)))
               {
                    $CEFBYTECS ="B2";
               }
            if(((($totALPROMEDIO)>89)&&(($totALPROMEDIO)<=100)))
               {
                    $CEFBYTECS ="C1";
               }
            $total_scoremail=round($tot/$CONT,2);
        }
        return array("calificacion"=>$totALPROMEDIO,"nivel"=>$CEFBYTECS);
    }
    
    public function recoverPass($agent, $user, $email){
        $model = UsersC::model()->findByAttributes(array("id_usuario_c"=>$user,"id_cliente"=>$agent,"email"=>$email));
        if($model->email == $email){
            $this->enviarCuenta($user, 1);
            return true;
        } else {
            return false;
        }
    }
   
    public function gastarLicencia($idLicenciaCliente){
        $modLicenciasCliente = LicensesClient::model()->findByPk($idLicenciaCliente);
        $modLicenciasCliente->utilizados += 1;
        $modLicenciasCliente->save(true,array('utilizados'));
        //Si se acaban las licencias cambiar el estado
        if($modLicenciasCliente->utilizados == $modLicenciasCliente->cantidad){
            $modLicenciasCliente->estado = 'F';
            $modLicenciasCliente->save(true,array('estado'));
        }
        //Aplicar control de Alertas
        $modLicCteTodas = LicensesClient::model()->findAllByAttributes(array('id_cliente'=>$modLicenciasCliente->id_cliente, 'id_licencia'=>$modLicenciasCliente->id_licencia));
        $totalRestante = 0;
        $revisando = 0;
        foreach($modLicCteTodas as $modelo){
            $revisando = 1;
            $totalRestante += ($modelo->cantidad - $modelo->utilizados);
        }
        if(($totalRestante == 20 || $totalRestante == 10 || $totalRestante == 0 ) && $revisando==1){
           $this->enviarAlertaLicencias($totalRestante,$modLicenciasCliente->id_licencia,$modLicenciasCliente->id_cliente);
        }
    }
    
    public function enviarAlertaLicencias($cantidad,$licencia,$cliente){
        $nomLicencia = Licenses::model()->findByPk($licencia)->nombre;
        $modToaa = UsersC::model()->findByAttributes(array('id_cliente'=>$cliente, 'id_perfil'=>2));
        if($modToaa){
            $mail = new YiiMailer('licensesAlert', array('model'=>$modToaa,'licencia'=>$nomLicencia, 'cantidadLic'=> $cantidad, 'header_title'=>'ILTO - ADMINISTRATOR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; LICENSES ALERT'));
            //set properties
            $mail->setFrom("support@iltoexams.com", "ILTO Exams");
            $mail->setSubject("LICENSES ALERT");
            $mail->setTo($modToaa->email);
            //send
            $mail->send();
        }
    }
}          
 
 
 
        /*7437976  2862400 ext 1404  yeimy avila 8am 5pm lv sab 8-12 */
        
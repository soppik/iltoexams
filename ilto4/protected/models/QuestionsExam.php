<?php

/**
 * This is the model class for table "questions_exam".
 *
 * The followings are the available columns in table 'questions_exam':
 * @property integer $id_preguntas_examen
 * @property integer $id_examen
 * @property integer $id_pregunta
 *
 * The followings are the available model relations:
 * @property Exams $idExamen
 */
class QuestionsExam extends CActiveRecord
{
    public $level;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'questions_exam';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_examen, id_pregunta', 'required'),
			array('id_examen, id_pregunta', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('level,id_preguntas_examen, id_examen, id_pregunta', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idExamen' => array(self::BELONGS_TO, 'Exams', 'id_examen'),
			'idPregunta' => array(self::BELONGS_TO, 'Questions', 'id_pregunta'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_preguntas_examen' => 'Id',
			'id_examen' => 'Exam',
			'id_pregunta' => 'Question',
                    'level'=>'Level',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                $criteria->with="idPregunta";
		$criteria->compare('id_preguntas_examen',$this->id_preguntas_examen);
		$criteria->compare('id_examen',$this->id_examen);
		$criteria->compare('idPregunta.nivel',$this->level);
		$criteria->compare('id_pregunta',$this->id_pregunta);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'pagination'=>array('pageSize'=>50,),

		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QuestionsExam the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

<?php

/**
 * This is the model class for table "licenses_client".
 *
 * The followings are the available columns in table 'licenses_client':
 * @property integer $id_licencias_cliente
 * @property string $id_cliente
 * @property integer $id_licencia
 * @property integer $cantidad
 * @property integer $utilizados
 * @property integer $demo
 * @property string $fecha_asignacion
 * @property string $fecha_final
 * @property string $estado
 *
 * The followings are the available model relations:
 * @property Licenses $idLicencia
 * @property Clients $idCliente
 * @property LicensesUser[] $licensesUsers
 */
class LicensesClient extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'licenses_client';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_cliente, id_licencia, cantidad, estado', 'required'),
			array('id_licencia, cantidad, utilizados, demo', 'numerical', 'integerOnly'=>true),
			array('id_cliente', 'length', 'max'=>20),
			array('fecha_final, fecha_asignacion', 'length', 'max'=>20),
			array('estado', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_licencias_cliente, id_cliente, id_licencia, cantidad, utilizados, demo,  fecha_final, estado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idLicencia' => array(self::BELONGS_TO, 'Licenses', 'id_licencia'),
			'idCliente' => array(self::BELONGS_TO, 'Clients', 'id_cliente'),
			'licensesUsers' => array(self::HAS_MANY, 'LicensesUser', 'id_licencia_cliente'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_licencias_cliente' => 'Id',
			'id_cliente' => 'Client',
			'id_licencia' => 'License',
			'cantidad' => 'Amount',
			'utilizados' => 'Used',
			'demo' => 'Is a DEMO',
			'fecha_asignacion' => 'Assign Date',
			'fecha_final' => 'End Date',
			'estado' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_licencias_cliente',$this->id_licencias_cliente);
		$criteria->compare('id_cliente',$this->id_cliente,true);
		$criteria->compare('id_licencia',$this->id_licencia);
		$criteria->compare('cantidad',$this->cantidad);
		$criteria->compare('utilizados',$this->utilizados);
		$criteria->compare('demo',$this->demo);
		$criteria->compare('fecha_final',$this->fecha_final,true);
		$criteria->compare('estado',$this->estado,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'pagination'=>array('pageSize'=>50,),

		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LicensesClient the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function scopes(){
            return array(
                'clienteActual'=>array(
                    'condition'=>"id_cliente = '".Yii::app()->user->getState('cliente')."'",
                ),
                'disponibles'=>array(
                    'condition'=>'utilizados < cantidad',
                )
            );
        }
        
}

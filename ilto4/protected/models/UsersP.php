<?php

/**
 * This is the model class for table "users_p".
 *
 * The followings are the available columns in table 'users_p':
 * @property string $id_usuario_p
 * @property string $nombre
 * @property string $email
 * @property integer $id_perfil
 * @property string $ultimo_acceso
 * @property string $clave
 * @property string $estado
 *
 * The followings are the available model relations:
 * @property Profiles $idPerfil
 */
class UsersP extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
    public $clave2;
	public function tableName()
	{
		return 'users_p';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_usuario_p, clave2, nombre, email, id_perfil, ultimo_acceso, clave, estado', 'required'),
			array('id_perfil', 'numerical', 'integerOnly'=>true),
			array('id_usuario_p', 'length', 'max'=>20),
			array('id_usuario_p', 'unique', 'message'=>'This user name already exists...'),
			array('email', 'email', 'message'=>'This email address is not valid...'),
			array('email', 'unique', 'message'=>'This email address already exists...'),
                        array('clave', 'compare', 'compareAttribute'=>'clave2'),
                        array('nombre, email', 'length', 'max'=>200),
			array('clave,clave2', 'length', 'max'=>50),
			array('estado', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_usuario_p, nombre, email, id_perfil, ultimo_acceso, clave, estado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idPerfil' => array(self::BELONGS_TO, 'Profiles', 'id_perfil'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_usuario_p' => 'Id',
			'nombre' => 'Name',
			'email' => 'Email',
			'id_perfil' => 'Profile',
			'ultimo_acceso' => 'Last Access',
			'clave' => 'Password',
			'clave2' => 'Verify Password',
			'estado' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_usuario_p',$this->id_usuario_p,true);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('id_perfil',$this->id_perfil);
		$criteria->compare('ultimo_acceso',$this->ultimo_acceso,true);
		$criteria->compare('estado',$this->estado,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'pagination'=>array('pageSize'=>50,),

		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UsersP the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

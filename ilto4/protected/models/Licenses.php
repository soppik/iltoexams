<?php

/**
 * This is the model class for table "licenses".
 *
 * The followings are the available columns in table 'licenses':
 * @property integer $id_licencia
 * @property string $nombre
 * @property string $estado
 * @property integer $genera_certificado
 *
 * The followings are the available model relations:
 * @property ExamsLicense[] $examsLicenses
 * @property LicensesClient[] $licensesClients
 */
class Licenses extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'licenses';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, logo, estado, intro_general, intro_secciones, genera_certificado, venta', 'required'),
			array('genera_certificado', 'numerical', 'integerOnly'=>true),
			array('texto_intro', 'length', 'allowEmpty'=>true),
			array('nombre', 'length', 'max'=>200),
			array('estado', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_licencia, nombre, venta, estado, genera_certificado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'examsLicenses' => array(self::HAS_MANY, 'ExamsLicense', 'id_licencia'),
			'licensesClients' => array(self::HAS_MANY, 'LicensesClient', 'id_licencia'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_licencia' => 'Id',
			'nombre' => 'Name',
			'estado' => 'Status',
                    'venta'=>'For Sale',
                    'texto_intro'=>'Text for General Intro',
                    'intro_general' => 'Show General Intro',
                    'intro_secciones' => 'Show Sections Intro',
                    'logo' => 'Logo',
			'genera_certificado' => 'Generate certificate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_licencia',$this->id_licencia);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('estado',$this->estado,true);
		$criteria->compare('venta',$this->venta,true);
		$criteria->compare('genera_certificado',$this->genera_certificado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'pagination'=>array('pageSize'=>50,),

		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Licenses the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        public function scopes(){
            return array(
                'forsale'=>array(
                    'condition'=>'venta = 1',
                    ),
            );
        }
}

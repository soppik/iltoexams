<?php

/**
 * This is the model class for table "agents".
 *
 * The followings are the available columns in table 'agents':
 * @property integer $id
 * @property string $prefix
 * @property string $first_name
 * @property string $last_name
 * @property string $job_title
 * @property string $department
 * @property string $email
 * @property string $phone
 * @property string $prefix_1
 * @property string $first_name_1
 * @property string $last_name_1
 * @property string $job_title_1
 * @property string $department_1
 * @property string $email_1
 * @property string $phone_1
 * @property string $prefix_2
 * @property string $first_name_2
 * @property string $last_name_2
 * @property string $job_title_2
 * @property string $department_2
 * @property string $email_2
 * @property string $phone_2
 * @property integer $country
 * @property integer $city
 * @property string $institution_name
 * @property string $address_1
 * @property string $address_2
 * @property string $address_3
 * @property string $state
 * @property string $zip
 * @property string $organization_type
 * @property integer $alter_country
 * @property integer $alter_city
 * @property string $alter_iinstitution_name
 * @property string $alter_address_1
 * @property string $alter_address_2
 * @property string $alter_address_3
 * @property string $alter_state
 * @property string $alter_zip
 * @property string $main_decision
 * @property integer $is_other_agent
 * @property string $organization_tests
 * @property string $organization_references
 * @property integer $years_operating
 * @property integer $years_location
 * @property integer $secure_rooms
 * @property integer $computers_room1
 * @property integer $computers_room2
 * @property integer $computers_room3
 * @property integer $net_type_room1
 * @property integer $net_type_room2
 * @property integer $net_type_room3
 * @property integer $partitions
 *
 * The followings are the available model relations:
 * @property Countries $country0
 * @property Cities $city0
 * @property Cities $alterCountry
 * @property Cities $alterCity
 */
class Agents extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'agents';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('prefix, id_legal, user_id_number, first_name, same_address, last_name, job_title, department, email, phone, main_decision, country, city, institution_name, address_1, state, zip, organization_type, is_other_agent, years_operating, years_location, secure_rooms, computers_room1, net_type_room1, partitions', 'required'),
			array('country,  city, alter_country, alter_city, is_other_agent, years_operating, years_location, secure_rooms, computers_room1, computers_room2, computers_room3, net_type_room1, net_type_room2, net_type_room3, partitions', 'numerical', 'integerOnly'=>true),
			array('prefix, prefix_1, prefix_2, user_id_number2,user_id_number3, zip, alter_zip', 'length', 'max'=>10),
			array('first_name, last_name, job_title, department, email, phone, first_name_1, last_name_1, job_title_1, department_1, email_1, phone_1, first_name_2, last_name_2, job_title_2, department_2, email_2, phone_2, address_1, address_2, address_3, state, alter_address_1, alter_address_2, alter_address_3, alter_state', 'length', 'max'=>50),
			array('institution_name, alter_iinstitution_name', 'length', 'max'=>100),
			array('logo', 'length', 'max'=>250),
			array('organization_type', 'length', 'max'=>1),
			array('organization_tests, organization_references', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, prefix, first_name, last_name, job_title, department, email, phone, prefix_1, first_name_1, last_name_1, job_title_1, department_1, email_1, phone_1, prefix_2, first_name_2, last_name_2, job_title_2, department_2, email_2, phone_2, country, city, institution_name, address_1, address_2, address_3, state, zip, organization_type, alter_country, alter_city, alter_iinstitution_name, alter_address_1, alter_address_2, alter_address_3, alter_state, alter_zip, is_other_agent, organization_tests, organization_references, years_operating, years_location, secure_rooms, computers_room1, computers_room2, computers_room3, net_type_room1, net_type_room2, net_type_room3, partitions', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'country0' => array(self::BELONGS_TO, 'Countries', 'country'),
			'city0' => array(self::BELONGS_TO, 'Cities', 'city'),
			'alterCountry' => array(self::BELONGS_TO, 'Cities', 'alter_country'),
			'alterCity' => array(self::BELONGS_TO, 'Cities', 'alter_city'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'prefix' => 'Prefix',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'job_title' => 'Job Title',
                        'user_id_number'=> 'Id Number',
                        'main_decision' => 'Please select Yes or No',
                        'same_address' => 'Please select Yes or No',
			'department' => 'Department',
			'email' => 'Email',
			'phone' => 'Phone',
			'prefix_1' => 'Prefix',
                        'user_id_number1'=> 'Id Number',
			'first_name_1' => 'First Name',
			'last_name_1' => 'Last Name',
			'job_title_1' => 'Job Title',
			'department_1' => 'Department',
			'email_1' => 'Email',
			'phone_1' => 'Phone',
			'prefix_2' => 'Prefix',
                        'user_id_number2'=> 'Id Number',
			'first_name_2' => 'First Name',
			'last_name_2' => 'Last Name',
			'job_title_2' => 'Job Title',
			'department_2' => 'Department',
			'email_2' => 'Email',
			'phone_2' => 'Phone',
			'country' => 'Country',
			'city' => 'City',
                    'id_legal'=>'Tax Id Number',
			'institution_name' => 'Institution Name',
			'address_1' => 'Address 1',
			'address_2' => 'Address 2',
			'address_3' => 'Address 3',
			'state' => 'State',
			'zip' => 'Zip',
			'organization_type' => 'Organization Type',
			'alter_country' => 'Alter Country',
			'alter_city' => 'Alter City',
			'alter_iinstitution_name' => 'Iinstitution Name',
			'alter_address_1' => 'Address 1',
			'alter_address_2' => 'Address 2',
			'alter_address_3' => 'Address 3',
			'alter_state' => 'State',
			'alter_zip' => 'Zip',
			'is_other_agent' => 'Is Other Agent',
			'organization_tests' => 'Organization Tests',
			'organization_references' => 'Organization References',
			'years_operating' => 'Years Operating',
			'years_location' => 'Years Location',
			'secure_rooms' => 'Secure Rooms',
			'computers_room1' => 'Computers Room1',
			'computers_room2' => 'Computers Room2',
			'computers_room3' => 'Computers Room3',
			'net_type_room1' => 'Net Type Room1',
			'net_type_room2' => 'Net Type Room2',
			'net_type_room3' => 'Net Type Room3',
			'logo' => 'Logo File',
			'partitions' => 'Partitions',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('prefix',$this->prefix,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('job_title',$this->job_title,true);
		$criteria->compare('department',$this->department,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('prefix_1',$this->prefix_1,true);
		$criteria->compare('first_name_1',$this->first_name_1,true);
		$criteria->compare('last_name_1',$this->last_name_1,true);
		$criteria->compare('job_title_1',$this->job_title_1,true);
		$criteria->compare('department_1',$this->department_1,true);
		$criteria->compare('email_1',$this->email_1,true);
		$criteria->compare('phone_1',$this->phone_1,true);
		$criteria->compare('prefix_2',$this->prefix_2,true);
		$criteria->compare('first_name_2',$this->first_name_2,true);
		$criteria->compare('last_name_2',$this->last_name_2,true);
		$criteria->compare('job_title_2',$this->job_title_2,true);
		$criteria->compare('department_2',$this->department_2,true);
		$criteria->compare('email_2',$this->email_2,true);
		$criteria->compare('phone_2',$this->phone_2,true);
		$criteria->compare('country',$this->country);
		$criteria->compare('city',$this->city);
		$criteria->compare('institution_name',$this->institution_name,true);
		$criteria->compare('address_1',$this->address_1,true);
		$criteria->compare('address_2',$this->address_2,true);
		$criteria->compare('address_3',$this->address_3,true);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('zip',$this->zip,true);
		$criteria->compare('organization_type',$this->organization_type,true);
		$criteria->compare('alter_country',$this->alter_country);
		$criteria->compare('alter_city',$this->alter_city);
		$criteria->compare('alter_iinstitution_name',$this->alter_iinstitution_name,true);
		$criteria->compare('alter_address_1',$this->alter_address_1,true);
		$criteria->compare('alter_address_2',$this->alter_address_2,true);
		$criteria->compare('alter_address_3',$this->alter_address_3,true);
		$criteria->compare('alter_state',$this->alter_state,true);
		$criteria->compare('alter_zip',$this->alter_zip,true); 
		$criteria->compare('is_other_agent',$this->is_other_agent);
		$criteria->compare('organization_tests',$this->organization_tests,true);
		$criteria->compare('organization_references',$this->organization_references,true);
		$criteria->compare('years_operating',$this->years_operating);
		$criteria->compare('years_location',$this->years_location);
		$criteria->compare('secure_rooms',$this->secure_rooms);
		$criteria->compare('computers_room1',$this->computers_room1);
		$criteria->compare('computers_room2',$this->computers_room2);
		$criteria->compare('computers_room3',$this->computers_room3);
		$criteria->compare('net_type_room1',$this->net_type_room1);
		$criteria->compare('net_type_room2',$this->net_type_room2);
		$criteria->compare('net_type_room3',$this->net_type_room3);
		$criteria->compare('partitions',$this->partitions);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'pagination'=>array('pageSize'=>50,),

		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Agents the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

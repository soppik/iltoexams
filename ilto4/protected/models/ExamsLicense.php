<?php

/**
 * This is the model class for table "exams_license".
 *
 * The followings are the available columns in table 'exams_license':
 * @property integer $id_examenes_licencia
 * @property integer $id_licencia
 * @property integer $id_examen
 * @property integer $es_placement
 *
 * The followings are the available model relations:
 * @property Licenses $idLicencia
 * @property Exams $idExamen
 */
class ExamsLicense extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'exams_license';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_licencia, id_examen', 'required'),
			array('id_licencia, id_examen, orden', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_examenes_licencia, id_licencia, id_examen, es_placement', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idLicencia' => array(self::BELONGS_TO, 'Licenses', 'id_licencia'),
			'idExamen' => array(self::BELONGS_TO, 'Exams', 'id_examen'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_examenes_licencia' => 'Id',
			'id_licencia' => 'License',
			'id_examen' => 'Exam',
			'orden' => 'Order',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_examenes_licencia',$this->id_examenes_licencia);
		$criteria->compare('id_licencia',$this->id_licencia);
		$criteria->compare('id_examen',$this->id_examen);
		$criteria->compare('orden',$this->orden);
                $criteria->order = "orden ASC";

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'pagination'=>array('pageSize'=>50,),

		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ExamsLicense the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function orden($licencia){
            $modelo = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>$licencia),array('order'=>'orden ASC'));
            $llaves= array();
            foreach($modelo as $registro){
                array_push($llaves, $registro->id_examenes_licencia);
                $registro->orden=NULL;
                $registro->save();
            }
            $orden=1;
            foreach($llaves as $llave){
                $modTmp = ExamsLicense::model()->findByPk($llave);
                $modTmp->orden = $orden;
                $modTmp->save();
                $orden++;
            }
        }
}

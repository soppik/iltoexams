<?php

/**
 * This is the model class for table "exams".
 *
 * The followings are the available columns in table 'exams':
 * @property integer $id_examen
 * @property string $nombre
 * @property integer $nivel
 * @property integer $preguntas_aleatorias
 * @property integer $id_seccion
 * @property string $estado
 * @property double $porcentaje_aprobado
 *
 * The followings are the available model relations:
 * @property Sections $idSeccion
 * @property ExamsLicense[] $examsLicenses
 * @property ExamsUser[] $examsUsers
 * @property QuestionsExam[] $questionsExams
 */
class Exams extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'exams';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, nivel, numero_preguntas, id_seccion, estado,  tutor', 'required'),
			array('numero_preguntas,preguntas_aleatorias, id_seccion, tutor, vistaunoauno, tipologo', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>200),
			array('estado', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_examen, nombre, nivel, preguntas_aleatorias, id_seccion, estado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idSeccion' => array(self::BELONGS_TO, 'Sections', 'id_seccion'),
			'examsLicenses' => array(self::HAS_MANY, 'ExamsLicense', 'id_examen'),
			'examsUsers' => array(self::HAS_MANY, 'ExamsUser', 'id_examen'),
			'questionsExams' => array(self::HAS_MANY, 'QuestionsExam', 'id_examen'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_examen' => 'Id',
			'nombre' => 'Name',
			'nivel' => 'Level',
			'preguntas_aleatorias' => 'Random Questions',
			'id_seccion' => 'Section',
			'estado' => 'Status',
                    'numero_preguntas'=>'Number of Questions',
			'tutor' => 'This test needs a Tutor',
                    'vistaunoauno'=>'Ask Question by Question',
                    'tipologo'=>'Logo for this exam'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_examen',$this->id_examen);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('nivel',$this->nivel);
		$criteria->compare('preguntas_aleatorias',$this->preguntas_aleatorias);
		$criteria->compare('id_seccion',$this->id_seccion);
		$criteria->compare('estado',$this->estado,true);
		$criteria->compare('numero_peguntas',$this->numero_preguntas,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'pagination'=>array('pageSize'=>50,),

		));
	}

        
	public function search_license($license)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_examen',$this->id_examen);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('nivel',$this->nivel);
		$criteria->compare('preguntas_aleatorias',$this->preguntas_aleatorias);
		$criteria->compare('id_seccion',$this->id_seccion);
		$criteria->compare('estado',$this->estado,true);
		$criteria->compare('numero_peguntas',$this->numero_preguntas,true);
                //Excluir los exámenes que ya fueron seleccionados en esta licencia
                $examenesSeleccionadas = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>$license));
                $arregloExamenes = array();
                foreach($examenesSeleccionadas as $examen){
                    array_push($arregloExamenes, "'".$examen->id_examen."'");
                }
                if(count($arregloExamenes)>0){
        		$criteria->addCondition("id_examen not in (".implode(",",$arregloExamenes).")");
                }

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'pagination'=>array('pageSize'=>50,),

		));
	}
        
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Exams the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

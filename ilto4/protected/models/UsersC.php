<?php

/**
 * This is the model class for table "users_c".
 *
 * The followings are the available columns in table 'users_c':
 * @property string $id_usuario_c
 * @property string $nombres
 * @property string $apellidos
 * @property string $email
 * @property string $id_cliente
 * @property string $ultimo_acceso
 * @property integer $id_perfil
 * @property string $numero_id
 * @property string $ruta_foto
 * @property string $estado
 *
 * The followings are the available model relations:
 * @property LicensesUser[] $licensesUsers
 * @property Clients $idCliente
 * @property Profiles $idPerfil
 */
class UsersC extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
    
    public $clave2;
	public function tableName()
	{
		return 'users_c';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombres,clave, clave2, apellidos, email, id_cliente, id_perfil, numero_id, estado,id_pais, id_ciudad', 'required'),
			array('id_perfil,id_pais,id_ciudad', 'numerical', 'integerOnly'=>true),
			array('id_usuario_c, id_cliente, numero_id', 'length', 'max'=>20),
			array('id_usuario_c', 'unique', 'message'=>'This user name already exists...'),
			array('nombres, apellidos', 'length', 'max'=>100),
			array('email', 'email', 'message'=>'This email address is not valid...'),
			array('email', 'unique', 'message'=>'This email address already exists...'),
                        array('clave', 'compare', 'compareAttribute'=>'clave2'),
			array('ruta_foto', 'length', 'max'=>250),
			array('estado', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_usuario_c, nombres, apellidos, email, id_cliente, ultimo_acceso, id_perfil, numero_id, ruta_foto, estado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
                Yii::import('application.modules.Clients.models.GroupsUsersClients');
		return array(
			'licensesUsers' => array(self::HAS_MANY, 'LicensesUser', 'id_usuario'),
			'idCliente' => array(self::BELONGS_TO, 'Clients', 'id_cliente'),
			'idPerfil' => array(self::BELONGS_TO, 'Profiles', 'id_perfil'),
                        'idGrupoCliente' => array(self::BELONGS_TO, 'GroupsUsersClients', 'id_grupo_cliente'),
                        'idPais' => array(self::BELONGS_TO, 'Countries', 'id_pais'),
                        'idCiudad' => array(self::BELONGS_TO, 'Cities', 'id_ciudad'),                    );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_usuario_c' => 'User Code (login)',
			'nombres' => 'First Name',
			'apellidos' => 'Last Name',
			'email' => 'Email',
			'id_cliente' => 'Client',
			'ultimo_acceso' => 'Last Access',
			'id_perfil' => 'Profile',
                    'clave'=>'Password',
			'clave2' => 'Verify Password',
			'numero_id' => 'Id Number',
			'id_grupo_cliente' => 'Category',
			'ruta_foto' => 'Picture',
			'estado' => 'Status',
                    'id_pais'=>'Country',
                    'id_ciudad'=>'City'
		);

                }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_usuario_c',$this->id_usuario_c,true);
		$criteria->compare('nombres',$this->nombres,true);
		$criteria->compare('apellidos',$this->apellidos,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('id_cliente',$this->id_cliente,true);
		$criteria->compare('ultimo_acceso',$this->ultimo_acceso,true);
		$criteria->compare('id_perfil',$this->id_perfil);
		$criteria->compare('numero_id',$this->numero_id,true);
		$criteria->compare('ruta_foto',$this->ruta_foto,true);
		$criteria->compare('estado',$this->estado,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'pagination'=>array('pageSize'=>50,),

		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UsersC the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    public function getNombresapellidos(){
        return $this->nombres." ".$this->apellidos;
    }
}

<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

?>
<style>
    body{
    background: url('images/bg_login_client.jpg')  no-repeat fixed !important;
-webkit-background-size: cover !important;
  -moz-background-size: cover !important;
  -o-background-size: cover !important;
  background-size: cover !important;
    } 
</style>
<script>
    function divclose(divObj){
        $(divObj).fadeOut();
    }
</script>    

<div class="row-fluid">
    <div style="width:200px; margin:auto;padding:20px;">
        <br>
    </div>
	
    <div class="transparente"> 
<?php 
if(isset($_GET['nolicense'])){
    $model->addError('agent', "Pending Activation");
}
	$this->beginWidget('zii.widgets.CPortlet', array(
		'title'=>"Restricted Access", 
	));
	
?>
          <?php
            foreach(Yii::app()->user->getFlashes() as $key => $message) {
                echo '<div onClick="divclose(this);" id="flash-'.$key.'" class="flash-' . $key . '">' . $message . "&nbsp;&nbsp;&nbsp;Click Here to dismiss</div>\n";
            }
        ?>

    <p>Welcome to our new Platform. Please fill the following form to access. </p>    
    
    <div class="form" style="margin: auto;max-width: 216px;">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'login-form',
        'enableClientValidation'=>false,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ), 
    )); ?>
        
        <div class="row">
            <?php echo $form->textField($model,'agent',array('placeholder'=>'Agent Code')); ?>
            <?php echo $form->error($model,'agent'); ?>
        </div>

        <div class="row">
            <?php echo $form->textField($model,'username',array('placeholder'=>'Username')); ?>
            <?php echo $form->error($model,'username'); ?>
        </div>
    
        <div class="row">
            <?php echo $form->passwordField($model,'password',array('placeholder'=>'Password')); ?>
            <?php echo $form->error($model,'password'); ?>
        </div>
        <div class="row rememberMe" style="width:250px;">
            <?php echo $form->checkBox($model,'rememberMe',array("style"=>"float:left;vertical-align:top")); ?>
            <?php echo $form->label($model,'rememberMe',array("style"=>"float:none;width:250px;vertical-align:middle;line-height:30px;")); ?>
            <?php echo $form->error($model,'rememberMe'); ?>
        </div>
    
        <div class="row buttons">
            <?php echo CHtml::submitButton('Login',array('class'=>'btn btn btn-primary iltoLoginbtn')); ?>
        </div>
        <a href="index.php?r=Clients/default/rememberPass">Forgot my Password</a>
        
    <?php $this->endWidget(); ?>
    </div><!-- form -->

<?php $this->endWidget();?>

    </div>

</div>
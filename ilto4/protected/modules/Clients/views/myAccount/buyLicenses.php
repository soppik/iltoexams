<?php
$this->layout="//layouts/column2";
?>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'buyLicenses-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
    <h3>Complete the following form and click on the submit button.</h3>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
		<?php echo $form->labelEx($model,'amount'); ?>
		<?php echo $form->textField($model,'amount',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'amount'); ?>
    </div>
    <div class="row">
		<?php echo $form->labelEx($model,'licensetype'); ?>
		<?php
            $dataCombo=CHtml::listData(Licenses::model()->findAllByAttributes(array('estado'=>'A','venta'=>1)),'id_licencia','nombre');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'BuyLicensesForm[licensetype]',
            'id'=> 'combo_licencias',
            'data'=>$dataCombo,
                'value'=>$model->licensetype,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'250px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'licensetype'); ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?> 
	</div>

<?php $this->endWidget(); ?>
</div>

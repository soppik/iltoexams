<?php
/* @var $this MyAccountController */
$this->layout="//layouts/column2";
$modLicencias = LicensesClient::model()->clienteActual()->disponibles()->findAll();

?>
<h3>Licenses Information </h3>
<div class="span3">
    <h4>My Licenses:</h4>
    <table class="calendar">
        <tr class="calendar-row"><td class="calendar-day-head" style="width:200px;">Type</td><td class="calendar-day-head" >&nbsp;&nbsp;&nbsp;&nbsp;Last&nbsp;Purchase&nbsp;&nbsp;&nbsp;&nbsp;</td><td class="calendar-day-head">Amount</td><td class="calendar-day-head">Used</td><td class="calendar-day-head">Remaining Licenses</td></tr>
        <?php 
        foreach($modLicencias as $rowLicencia){
            echo "<tr><td class='calendar-day mediana'>".str_replace(" ","&nbsp;",$rowLicencia->idLicencia->nombre)."</td>";
            echo "<td class='calendar-day mediana centrado'>".  $rowLicencia->fecha_asignacion."</td>";
            echo "<td class='calendar-day mediana centrado'>".  number_format(($rowLicencia->cantidad),0)."</td>";
            echo "<td class='calendar-day  mediana centrado'>".  number_format(($rowLicencia->utilizados),0)."</td>";
            echo "<td class='calendar-day  mediana centrado'>".  number_format(($rowLicencia->cantidad - $rowLicencia->utilizados),0)."</td></tr>";
        }
        if(Yii::app()->user->getState('id_perfil')==2){
        ?>
        <tr class="calendar-row"><td class="calendar-day  grande" colspan="4"><a href='index.php?r=Clients/MyAccount/buyLicenses'>Buy More Licenses</a></td><td class="calendar-day  grande"><a href="#">History</a></td></tr>
        <?php } ?>
    </table>       
</div>
<?php
/* @var $this GroupsUsersClientsController */
/* @var $model GroupsUsersClients */

$this->breadcrumbs=array(
	'Groups Users Clients'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Groups', 'url'=>array('admin')),
);
$this->layout = "//layouts/column2a";
?>

<h1>Create Groups</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
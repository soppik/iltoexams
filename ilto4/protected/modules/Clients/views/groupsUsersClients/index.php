<?php
/* @var $this GroupsUsersClientsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Groups Users Clients',
);

$this->menu=array(
	array('label'=>'Create GroupsUsersClients', 'url'=>array('create')),
	array('label'=>'Manage GroupsUsersClients', 'url'=>array('admin')),
);
?>

<h1>Groups Users Clients</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

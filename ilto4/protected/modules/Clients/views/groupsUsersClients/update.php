<?php
/* @var $this GroupsUsersClientsController */
/* @var $model GroupsUsersClients */

$this->breadcrumbs=array(
	'Groups Users Clients'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Create Groups', 'url'=>array('create')),
	array('label'=>'Manage Groups', 'url'=>array('admin')),
);
$this->layout = "//layouts/column2a";
?>

<h1>Update Group <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
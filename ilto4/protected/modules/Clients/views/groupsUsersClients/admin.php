<?php
/* @var $this GroupsUsersClientsController */
/* @var $model GroupsUsersClients */

$this->breadcrumbs=array(
	'Groups Users Clients'=>array('index'),
	'Manage',
);
$visible = (Yii::app()->user->getState('id_perfil') < 4) ? 'true':'false';
if($visible=='true'){
$this->menu=array(
	array('label'=>'Create Groups', 'url'=>array('create')),
);
}

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#groups-users-clients-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
$this->layout = "//layouts/column2a";
?>

<h1>Manage Groups</h1>



<?php 
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'groups-users-clients-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'nombre',
                array(
                    'name'=>'estado',
                    'value'=>'($data->estado=="A" ? "Enabled" : "Disabled")',
                    'filter'=>array("A"=>"Enable","I"=>"Disable"),
                    ),
		array(
		'class'=>'CButtonColumn',
                    'template'=>'{update}{delete}',
                    'buttons'=>array(
                        'delete'=>array(
                            'label'=>'Delete&nbsp;',
                            'imageUrl'=>NULL,
                            'visible'=>$visible,
                        ),
                        'update'=>array(
                            'label'=>'Edit&nbsp;',
                            'imageUrl'=>NULL,
                            'visible'=>$visible,
                        )
                    )
		),
	),
)); ?>

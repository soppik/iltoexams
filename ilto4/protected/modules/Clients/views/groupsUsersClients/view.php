<?php
/* @var $this GroupsUsersClientsController */
/* @var $model GroupsUsersClients */

$this->breadcrumbs=array(
	'Groups Users Clients'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List GroupsUsersClients', 'url'=>array('index')),
	array('label'=>'Create GroupsUsersClients', 'url'=>array('create')),
	array('label'=>'Update GroupsUsersClients', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete GroupsUsersClients', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage GroupsUsersClients', 'url'=>array('admin')),
);
?>

<h1>View GroupsUsersClients #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_cliente',
		'nombre',
	),
)); ?>

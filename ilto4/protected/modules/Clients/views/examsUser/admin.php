<?php
/* @var $this ExamsUserController */
/* @var $model ExamsUser */

$this->breadcrumbs=array(
	'Exams Users'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Back to Licenses', 'url'=>array('LicensesUser/admin/id/'.$model->idLicenciaUsuario->id_usuario)),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#exams-user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Exams for <?php echo $model->idLicenciaUsuario->idUsuario->nombres." ".$model->idLicenciaUsuario->idUsuario->apellidos." // ".$model->idLicenciaUsuario->idLicenciaCliente->idLicencia->nombre;?></h1>



<?php 
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'exams-user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
                array(
                    'name'=>'id_examen',
                    'value'=>'$data->idExamen->nombre',
                    'filter'=>''
                ),
		'fecha_presentacion',
		'calificacion',
		'nivel',
                array(
                    'name'=>'estado',
                    'value'=>'($data->estado=="A")? PENDING : TAKEN',
                    'filter'=>''
                ),

            array(
			'class'=>'CButtonColumn',
                        'template'=>'{Take Exam}',
                'buttons'=>array(
                        'Take Exam'=>array(
                            'url'=>'Yii::app()->createUrl("Clients/AnswersExamUser/takeExam",array("userId"=>$data->idLicenciaUsuario->id_usuario,"license"=>$data->id_licencia_usuario,"examUserId"=>$data->id_examen_usuario))',
                            'imageUrl' => NULL,
                            'label'=>'Take Exam',
                            'options'=>array('class'=>"loadTest"),
                            'visible'=>'($data->estado=="A")? (($data->idExamen->tutor==1)? TRUE: FALSE) : FALSE'
                            )
                )
		), 
	),
));
?>
<script>
    $( "a.loadTest" ).click(function( event ) {
        event.preventDefault();
        window.open(this.href,'takeExam','fullscreen=yes,location=no,menubar=no,titlebar=no,toolbar=no')
    });
</script>
<?php
/* @var $this ExamsUserController */
/* @var $model ExamsUser */

$this->breadcrumbs=array(
	'Exams Users'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Go Back', 'url'=>array('LicensesUser/admin/id/'.$id)),
);
?>

<h1>Allocate License to <?php echo UsersC::model()->findByAttributes(array('id_usuario_c'=>$id))->nombres." ".UsersC::model()->findByAttributes(array('id_usuario_c'=>$id))->apellidos;?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
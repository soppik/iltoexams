<?php
/* @var $this ExamsUserController */
/* @var $model ExamsUser */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_examen_usuario'); ?>
		<?php echo $form->textField($model,'id_examen_usuario'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_licencia_usuario'); ?>
		<?php echo $form->textField($model,'id_licencia_usuario'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_examen'); ?>
		<?php echo $form->textField($model,'id_examen'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_presentacion'); ?>
		<?php echo $form->textField($model,'fecha_presentacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'calificacion'); ?>
		<?php echo $form->textField($model,'calificacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nivel'); ?>
		<?php echo $form->textField($model,'nivel'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'estado'); ?>
		<?php echo $form->textField($model,'estado',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
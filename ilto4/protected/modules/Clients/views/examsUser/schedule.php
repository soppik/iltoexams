<?php
$modUtil = new Utilidades();
$arregloMeses = $modUtil->arregloMeses();
$linkAnte = '<a href="index.php?r=Clients/ExamsUser/schedule&mes='.$mesAnte.'&anio='.$anioAnte.'">'.$arregloMeses[$mesAnte].' '.$anioAnte.'</a>';
$linkPost = '<a href="index.php?r=Clients/ExamsUser/schedule&mes='.$mesPost.'&anio='.$anioPost.'">'.$arregloMeses[$mesPost].' '.$anioPost.'</a>';
?>
<table style="width:100%; font-size: 20px;"><tr><td style="text-align: left;"><?php echo $linkAnte;?></td><td style="text-align: center;"><?php echo $mes." ".$anio;?></td><td style="text-align: right;"><?php echo $linkPost;?></td></tr></table>
<?php
echo $modUtil->draw_calendar(date("m"),date("Y"));
$script="<script type='text/javascript'>";
$arreglo = [];
foreach($model as $rowExam){
    $fecha = new DateTime($rowExam->fecha_presentacion);
    $link = "<a class='fancy' href='index.php?r=Clients/ExamsUser/viewDay&day=".$fecha->format("d")."&month=".$fecha->format("m")."&year=".$fecha->format("Y")."&room=".$rowExam->salon."'>".$rowExam->salon0->nombre."</a>";
    if(isset($arreglo[$fecha->format("d")])){
        $arreglo["'".$fecha->format("d")."'"] .= $link;
    } else {
        $arreglo["'".$fecha->format("d")."'"] = $link;
    }
}
foreach($arreglo as $dia=>$enlace){
    $script .= "$('#divday_".str_replace("'","",$dia)."').html(\"".$enlace."\");";
}
$script.="</script>"; 
echo $script; 
$config = array( 
    'scrolling' => 'no', 
    'titleShow' => false,
    'autoscale'=> true,
    'overlayColor' => '#000',
    'showCloseButton' => true,
    'transitionIn'=>'elastic',
    'transitionOut'=>'elastic',
        // change this as you need
    );
    $this->widget('application.extensions.fancybox.EFancyBox', array('target'=>'.fancy', 'config'=>$config));




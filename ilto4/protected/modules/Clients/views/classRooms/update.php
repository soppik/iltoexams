<?php
/* @var $this ClassRoomsController */
/* @var $model ClassRooms */

$this->breadcrumbs=array(
	'Class Rooms'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Create ClassRooms', 'url'=>array('create')),
	array('label'=>'Manage ClassRooms', 'url'=>array('admin')),
);
$this->layout = "//layouts/column2a";
?>

<h1>Update Classrooms <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
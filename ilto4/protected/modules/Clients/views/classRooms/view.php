<?php
/* @var $this ClassRoomsController */
/* @var $model ClassRooms */

$this->breadcrumbs=array(
	'Class Rooms'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ClassRooms', 'url'=>array('index')),
	array('label'=>'Create ClassRooms', 'url'=>array('create')),
	array('label'=>'Update ClassRooms', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ClassRooms', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ClassRooms', 'url'=>array('admin')),
);
?>

<h1>View ClassRooms #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_cliente',
		'nombre',
		'ubicacion',
	),
)); ?>

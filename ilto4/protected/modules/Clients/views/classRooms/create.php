<?php
/* @var $this ClassRoomsController */
/* @var $model ClassRooms */

$this->breadcrumbs=array(
	'Class Rooms'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage ClassRooms', 'url'=>array('admin')),
);
$this->layout = "//layouts/column2a";
?>

<h1>Create Classrooms</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
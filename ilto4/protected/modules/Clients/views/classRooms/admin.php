<?php
/* @var $this ClassRoomsController */
/* @var $model ClassRooms */
$this->layout = "//layouts/column2a";
$this->breadcrumbs=array(
	'Class Rooms'=>array('index'),
	'Manage',
);
$visible = (Yii::app()->user->getState('id_perfil') < 4) ? 'true':'false';
if($visible=='true'){
$this->menu=array(
	array('label'=>'Create Classrooms', 'url'=>array('create')),
);
}

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#class-rooms-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Classrooms</h1>



<?php 
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'class-rooms-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'nombre',
		'ubicacion',
		'capacidad',
                array(
                    'name'=>'estado',
                    'value'=>'($data->estado=="A" ? "Enabled" : "Disabled")',
                    'filter'=>array("A"=>"Enable","I"=>"Disable"),
                    ),
		array(
			'class'=>'CButtonColumn',
                    'template'=>'{update}{delete}',
                    'buttons'=>array(
                        'delete'=>array(
                            'label'=>'Delete',
                            'imageUrl'=>NULL,
                            'visible'=>'$data->seElimina() && '.$visible
                        ),
                        'update'=>array(
                            'label'=>'Edit',
                            'imageUrl'=>NULL,
                            'visible'=>$visible
                        )
                    )
                    
		),
	),
)); ?>

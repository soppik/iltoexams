<?php
/* @var $this ClassRoomsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Class Rooms',
);

$this->menu=array(
	array('label'=>'Create ClassRooms', 'url'=>array('create')),
	array('label'=>'Manage ClassRooms', 'url'=>array('admin')),
);
?>

<h1>Class Rooms</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

<?php
/* @var $this OldPlatformController */
/* @var $model OldPlatform */

$this->breadcrumbs=array(
	'Old Platforms'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Create OldPlatform', 'url'=>array('create')),
	array('label'=>'Manage OldPlatform', 'url'=>array('admin')),
);
?>

<h1>Update OldPlatform <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
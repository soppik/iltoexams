<?php
/* @var $this OldPlatformController */
/* @var $model OldPlatform */

$this->breadcrumbs=array(
	'Old Platforms'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage OldPlatform', 'url'=>array('admin')),
);
?>

<h1>Create OldPlatform</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
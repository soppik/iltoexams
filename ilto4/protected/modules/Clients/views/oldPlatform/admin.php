<?php
/* @var $this OldPlatformController */
/* @var $model OldPlatform */

?>
<h1>Old Platform</h1>


<?php 
if(Yii::app()->user->getState('cliente')=='9002120741'){ $mpath='eww';} //English
if(Yii::app()->user->getState('cliente')=='8605173021'){ $mpath='newtest2';}  // AreaAndina

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'old-platform-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'code',
		'name',
		'exam_date',
		'score',
		'cef',
		array(
			'class'=>'CButtonColumn',
                    'template'=>'{certificate}',
                    'buttons'=> array(
                        'certificate'=>array(
                            'class'=>'CLinkColumn',
                            'header'=>'Certificate',
                            'url'=> 'Yii::app()->baseUrl."/../'.$mpath.'/examspdf/results".$data->code.".pdf"',
                            'options'=>array("target"=>"_blank"),
                        )
                    )
		),
	),
)); ?>

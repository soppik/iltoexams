<?php
/* @var $this LicensesUserController */
/* @var $model LicensesUser */

$this->breadcrumbs=array(
	'Licenses Users'=>array('index'),
	'Manage',
);
$oculta=0;
foreach($model as $modelito){
    if(count($modelito)>0){
        if($modelito->estado == 'A'){
            $oculta=1;
        }
    } else { $oculta=1;}
}
if($oculta==1){
$this->menu=array(
	array('label'=>'Allocate License', 'url'=>array('ExamsUser/create/id/'.$id_usuario)),
);
}
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#licenses-user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Licenses allocated to <?php echo $model->idUsuario->nombres." ".$model->idUsuario->apellidos;?></h1>



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'licenses-user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
                array(
                    'name'=>'id_licencia_cliente',
                    'value'=>'$data->idLicenciaCliente->idLicencia->nombre'
                ),
		'fecha_asignacion',
                array(
                    'name'=>'estado',
                    'value'=>'$data->estado=="A" ? "Enabled" : ($data->estado=="F" ? "Final" : "Disabled")',
                    'filter'=>array("A"=>"Enable","I"=>"Disable","F"=>"Final"),
                    ),
		array(
			'class'=>'CButtonColumn',
                    'template'=>'{Tests}{Certificate}{Email Certificate}',
                    'buttons'=>array(
                        'delete'=>array(
                            'imageUrl'=>NULL
                        ),
                        'Tests'=>array(
                            'url'=> 'Yii::app()->createUrl("Clients/ExamsUser/admin",array("id"=>$data->id_licencias_usuario))',
                            'imageUrl' => NULL,
                        ),
                        'Certificate'=>array(
                            'url'=> 'Yii::app()->createUrl("Clients/licensesUser/certificate",array("id"=>$data->id_licencias_usuario))',
                            'imageUrl' => NULL,
                            'label'=>'View_Certificate',
                            'visible'=>'$data->estado=="F"',
                        ),
                        'Email Certificate'=>array(
                            'url'=> 'Yii::app()->createUrl("Clients/licensesUser/emailcertificate",array("id"=>$data->id_licencias_usuario))',
                            'imageUrl' => NULL,
                            'label'=>'Email_Certificate',
                            'visible'=>'$data->estado=="F"',
                        )
                    )
		),
	),
)); ?>

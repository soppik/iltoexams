<?php
/* @var $this LicensesUserController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Licenses Users',
);

$this->menu=array(
	array('label'=>'Create LicensesUser', 'url'=>array('create')),
	array('label'=>'Manage LicensesUser', 'url'=>array('admin')),
);
?>

<h1>Licenses Users</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

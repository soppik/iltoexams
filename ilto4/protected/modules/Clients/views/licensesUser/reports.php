<?php
/* @var $this LicensesUserController */
/* @var $model LicensesUser */

$this->breadcrumbs=array(
	'Licenses Users'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#licenses-user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Licenses allocated to <?php echo $model->idUsuario->nombres." ".$model->idUsuario->apellidos;?></h1>



<?php 
$modUtil = new Utilidades;
$arregloCef = $modUtil->arregloNiveles();
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'licenses-user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
                array(
                    'name'=>'id_licencia_cliente',
                    'value'=>'$data->idLicenciaCliente->idLicencia->nombre',
                    'filter'=>CHtml::dropDownList('LicensesUsed[usuario_id]','',CHtml::listData(LicensesClient::model()->clienteActual()->with('idLicencia')->findAll(),'id_licencia_cliente','idLicencia.nombre'),array('empty'=>'(select)')),
                ),
                array(
                    'name'=>'usuario_grupo',
                    'value'=>'$data->idUsuario->idGrupoCliente->nombre',
                    'filter'=>CHtml::listData(GroupsUsersClients::model()->findAll(),'id','nombre'),
                ),
                array(
                    'name'=>'usuario_id',
                    'header'=>'Id',
                    'value'=> '$data->id_usuario'
                ),
                array(
                    'header'=>'Email',
                    'value'=> '$data->idUsuario->email'
                ),
                array(
                    'name'=>'usuario_nombre',
                    'value'=> '$data->idUsuario->nombres'
                ),
                array(
                    'name'=>'usuario_apellido',
                    'value'=> '$data->idUsuario->apellidos'
                ),
		'fecha_asignacion',
		'calificacion',
            array(
                'name'=>'nivel',
                'filter'=> $arregloCef,
            ),
                array(
                    'name'=>'estado',
                    'value'=>'$data->estado=="A" ? "Not Finished" : ($data->estado=="F" ? "Final" : "")',
                    'filter'=>array("A"=>"Not Finished","F"=>"Final"),
                    ),
	),
)); ?>

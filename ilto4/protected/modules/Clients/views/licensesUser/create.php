<?php
/* @var $this LicensesUserController */
/* @var $model LicensesUser */

$this->breadcrumbs=array(
	'Licenses Users'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage LicensesUser', 'url'=>array('admin')),
);
?>

<h1>Create LicensesUser</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
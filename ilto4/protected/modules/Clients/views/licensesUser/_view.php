<?php
/* @var $this LicensesUserController */
/* @var $data LicensesUser */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_licencias_usuario')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_licencias_usuario), array('view', 'id'=>$data->id_licencias_usuario)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_licencia_cliente')); ?>:</b>
	<?php echo CHtml::encode($data->id_licencia_cliente); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_usuario')); ?>:</b>
	<?php echo CHtml::encode($data->id_usuario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_asignacion')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_asignacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado')); ?>:</b>
	<?php echo CHtml::encode($data->estado); ?>
	<br />


</div>
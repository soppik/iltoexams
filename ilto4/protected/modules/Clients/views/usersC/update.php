<?php
/* @var $this UsersCController */
/* @var $model UsersC */

$this->breadcrumbs=array(
	'Client Users'=>array('index'),
	$model->id_usuario_c=>array('view','id'=>$model->id_usuario_c),
	'Update',
);

$this->menu=array(
	array('label'=>'Create Users', 'url'=>array('create')),
	array('label'=>'Back to Manager', 'url'=>array('admin')),
);
$this->layout = "//layouts/column2a";
?>

<h1>Update User: <?php echo $model->id_usuario_c; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
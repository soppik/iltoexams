<?php
/* @var $this UsersCController */
/* @var $model UsersC */


$this->menu=array(
	array('label'=>'Back to Users Manager', 'url'=>array('admin')),
);
?>

<h1>View User #<?php echo $model->id_usuario_c; ?></h1>

<?php 
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'nombres',
		'apellidos',
		'email',
            array(
		'name'=>'id_cliente',
                'label'=>'Agent',
                'value'=> $model->idCliente->nombre_rsocial,
                ),
            array('name'=>'id_perfil',
                'value'=> $model->idPerfil->nombre
        ),
		'numero_id',
		array(
                    'name'=> 'estado',
                    'value'=> $model->traeEstado()
                    ), 
		array(
                    'name'=> 'ruta_foto',
                    'type'=> 'raw',
                    'value'=> CHtml::image('images/users/tn2_'.$model->ruta_foto),
                    'visible'=> (!is_null($model->ruta_foto)) ? true:false,
                    )
            
	),
)); ?> 

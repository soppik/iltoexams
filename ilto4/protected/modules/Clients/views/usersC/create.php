<?php
/* @var $this UsersCController */
/* @var $model UsersC */

$this->breadcrumbs=array(
	'Client Users'=>array('index'),
	'Create',
);
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/swfobject.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/scriptCam/scriptcam.js');

$this->menu=array(
	array('label'=>'Back to Manager', 'url'=>array('admin')),
);
$this->layout = "//layouts/column2a";
?>

<h1>Create Users</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
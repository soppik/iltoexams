<?php
/* @var $this UsersCController */
/* @var $model UsersC */

$this->breadcrumbs=array(
	'Users Cs'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create Users', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#users-c-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
$this->layout = "//layouts/column2a";
?>

<h1>Manage Customer Users</h1>


<?php 
$cliente = Yii::app()->user->getState('cliente');
if($cliente=='8605173021'){ $visible=false; $txtVisible ='false';} else {$visible=true; $txtVisible='true';}
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-c-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
                array(
                    'name'=>'id_perfil',
                    'value'=>'$data->idPerfil->nombre',
                    'filter'=>CHtml::listData(Profiles::model()->findAllByAttributes(array('es_cliente'=>1)),"id_perfil","nombre"),
                    'htmlOptions'=>array('style'=>'width:100px;')
                ),
                array(
                    'name'=>'id_grupo_cliente',
                    'value'=>'$data->idGrupoCliente->nombre',
                    'filter'=>CHtml::listData(GroupsUsersClients::model()->findAll(),"id","nombre"),
                ),
		'id_usuario_c',
		'nombres',
		'apellidos',
                array(
                    'name'=>'estado',
                    'value'=>'($data->estado=="A" ? "Enabled" : "Disabled")',
                    'filter'=>array("A"=>"Enable","I"=>"Disable"),
                    ),
		/*
		'tipo_id',
		'numero_id',
		'ruta_foto',
		'estado',
		*/
		array(
			'class'=>'CButtonColumn',
                    'template'=>'{update}{view}{Licenses}{delete}',
                    'buttons'=>array(
                        'update'=>array(
                            'label'=>'Edit&nbsp;',
                            'url'=> 'Yii::app()->createUrl("Clients/UsersC/update",array("id"=>$data->id_usuario_c))',
                            'visible' => '(Yii::app()->user->getState("id_perfil") <= 2  && ($data->id_perfil > 2 && $data->id_perfil < 6) && '.$txtVisible.') ? true:false',
                            'imageUrl'=>NULL
                        ),
                        'view'=>array(
                            'label'=>'View&nbsp;',
                            'url'=> 'Yii::app()->createUrl("Clients/UsersC/view",array("id"=>$data->id_usuario_c))',
                            'imageUrl'=>NULL
                        ),
                        'delete'=>array(
                            'url'=> 'Yii::app()->createUrl("Clients/UsersC/delete",array("id"=>$data->id_usuario_c))',
                            'imageUrl'=>NULL,
                            'label'=>'Delete', 
                            'visible' => '(Yii::app()->user->getState("id_perfil") <= 2  && ($data->id_perfil > 2 && $data->id_perfil < 6) && '.$txtVisible.') ? true:false',
                        ),
                        'Email'=>array(
                            'url'=> 'Yii::app()->createUrl("Clients/UsersC/email",array("id"=>$data->id_usuario_c,"login"=>1))',
                            'imageUrl'=>NULL,
                            'label'=>'Email&nbsp;',
                            'visible' => '$visible',
                            
                        ),
                        'Licenses'=>array(
                            'url'=> 'Yii::app()->createUrl("Clients/LicensesUser/admin",array("id"=>$data->id_usuario_c))',
                            'visible'=>'(Yii::app()->user->getState("id_perfil") <= 4  && $data->id_perfil == 6 && '.$txtVisible.') ? true:false',
                            'imageUrl'=>NULL,
                            'label'=>'Licenses'
                        ),
                    )
		),
	),
)); ?>

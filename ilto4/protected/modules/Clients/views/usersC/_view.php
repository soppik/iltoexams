<?php
/* @var $this UsersCController */
/* @var $data UsersC */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_usuario_c')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_usuario_c), array('view', 'id'=>$data->id_usuario_c)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombres')); ?>:</b>
	<?php echo CHtml::encode($data->nombres); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('apellidos')); ?>:</b>
	<?php echo CHtml::encode($data->apellidos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_cliente')); ?>:</b>
	<?php echo CHtml::encode($data->id_cliente); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ultimo_acceso')); ?>:</b>
	<?php echo CHtml::encode($data->ultimo_acceso); ?>
	<br />

	<b><?php echo CHtml::encode($data->idPerfil->nombre); ?>:</b>
	<?php echo CHtml::encode($data->id_perfil); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_id')); ?>:</b>
	<?php echo CHtml::encode($data->tipo_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numero_id')); ?>:</b>
	<?php echo CHtml::encode($data->numero_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ruta_foto')); ?>:</b>
	<?php echo CHtml::encode($data->ruta_foto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado')); ?>:</b>
	<?php echo CHtml::encode($data->estado); ?>
	<br />

	*/ ?>

</div>
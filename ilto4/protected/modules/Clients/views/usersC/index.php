<?php
/* @var $this UsersCController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Users Cs',
);

$this->menu=array(
	array('label'=>'Create UsersC', 'url'=>array('create')),
	array('label'=>'Manage UsersC', 'url'=>array('admin')),
);
?>

<h1>Users Cs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

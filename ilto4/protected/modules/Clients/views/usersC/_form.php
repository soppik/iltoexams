<?php
/* @var $this UsersCController */
/* @var $model UsersC */
/* @var $form CActiveForm */
$modUtil = new Utilidades();
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-c-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nombres'); ?>
		<?php echo $form->textField($model,'nombres',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'nombres'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'apellidos'); ?>
		<?php echo $form->textField($model,'apellidos',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'apellidos'); ?>
	</div>

        <div class="row">
		<?php echo $form->labelEx($model,'id_pais'); ?>
		<?php
            $dataCombo=CHtml::listData(Countries::model()->findAllByAttributes(array('estado'=>'A')),'id_pais','nombre');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'UsersC[id_pais]',
            'id'=> 'combo_paises',
            'data'=>$dataCombo,
                'value'=>$model->id_pais,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'250px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'id_pais'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_ciudad'); ?>
            <select name="UsersC[id_ciudad]" id="Clients_id_ciudad" style="width:250px;"></select>
		<?php echo $form->error($model,'id_ciudad'); ?>
	</div>
        <div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_perfil'); ?>
            <div class="tool_tip"><img style="width:24px;" src='images/iconos/help.png'/></div>
            <div class="tooltip"><b>TOAA (Total Access Administrator)</b>, This user is able to:<br><ul><li>Manage user accounts (create, delete, modify),</li><li>Allocate test licenses.</li><li>Manage groups or test rooms (Create, delete, modify)</li><li>Purchase test licenses</li><li>Tutor access (Perform the speaking test and assign test licenses)</li><li>Manage the test agenda (set and modify exam dates, approve or reject test takers)</li></ul>
                <b>PAA (Partial Access Administrator)</b>, This user is able to:<br><ul><li>Allocate test licenses,</li><li>Manage groups or test rooms (Create, delete, modify)</li><li>Tutor access (Perform the speaking test and assign test licenses)</li><li>Manage the test agenda (set and modify exam dates, approve or reject test takers)</li></ul>
                <b>TUAA (Tutor Access Administrator)</b>, This user is able to:<br><ul><li>Allocate test licenses,</li><li>See groups or test rooms</li><li>Tutor access (Perform the speaking test and assign test licenses)</li><li>See the test agenda</li></ul>
                <b>TAKER</b>: This user is able just to take the Exam
            </div>
		<?php
            $dataCombo=CHtml::listData(Profiles::model()->superiores()->findAllByAttributes(array('estado'=>'A','es_cliente'=>'1')),'id_perfil','nombre');
            //$dataCombo=CHtml::listData(Profiles::model()->findAllByAttributes(array('estado'=>'A','es_cliente'=>'1')),'id_perfil','nombre');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'UsersC[id_perfil]',
            'id'=> 'combo_perfil',
            'data'=>$dataCombo,
                'value'=>$model->id_perfil,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'226px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'id_perfil'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'numero_id'); ?>
		<?php echo $form->textField($model,'numero_id',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'numero_id'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'ruta_foto'); ?>
		<?php echo $form->fileField($model,'ruta_foto',array('size'=>60,'maxlength'=>250)); ?>
		<?php echo $form->error($model,'ruta_foto'); ?>
            <?php if(!$model->isNewRecord){
                if(strlen($model->ruta_foto)>0){
                    echo "<img src='images/users/tn2_$model->ruta_foto' style='width:200px;'>";
                    echo "<input type='hidden' name='ruta_imagen_actual' value='$model->ruta_foto'>";
                    echo "&nbsp;&nbsp;<input type='checkbox' name='delete_imagen' value='1'>&nbsp;Delete Image on Save";
                }
            } ?>
	</div>
<div id="datosTaker" style="display:none;">
	<div class="row">
		<?php echo $form->labelEx($model,'id_grupo_cliente'); ?>
            <div class="tool_tip"><img style="width:24px;" src='images/iconos/help.png'/></div><div class="tooltip">Please select the group where the user belongs to</div>
		<?php
            $dataCombo=CHtml::listData(GroupsUsersClients::model()->findAllByAttributes(array('estado'=>'A')),'id','nombre');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'UsersC[id_grupo_cliente]',
            'id'=> 'combo_grupo',
            'data'=>$dataCombo,
                'value'=>$model->id_grupo_cliente,
            'options'=>array(
              'placeholder'=>'Choose one group...',
              'allowClear'=>true,
                'width'=>'226px',
                'class'=>'tool_tip',
            ),
          ));
            ?>
		<?php echo $form->error($model,'id_perfil'); ?>
	</div>
</div>
    <?php if(!$model->isNewRecord) {?>
	<div class="row">
		<?php echo $form->labelEx($model,'estado'); ?>
		<?php
            $dataCombo=array('A'=>'Enabled','I'=>'Disabled');
            $this->widget('ext.select2.ESelect2',array(
            'name'=>'UsersC[estado]',
            'id'=> 'combo_estado',
            'data'=>$dataCombo,
                'value'=>$model->estado,
            'options'=>array(
              'placeholder'=>'Choose one...',
              'allowClear'=>true,
                'width'=>'250px',
            ),
          ));
            ?>
		<?php echo $form->error($model,'estado'); ?>
	</div>
    <?php } ?>
	<div class="row buttons">
            
		<?php 
                if($model->isNewRecord){
                    $model->ultimo_acceso="0000-00-00 00:00:00";
                }
                echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save');
                ?>
            <div id="btnTaker" style="display:none;">
                <?php 
                if($model->isNewRecord){
                    echo CHtml::Button('Save & Assign Exam',array('submit'=>'index.php?r=Clients/UsersC/create_exam')); 
                }
                ?> 
            </div>   
            
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
    $('#combo_paises').change( function() {
        debugger;
        var valor = $('#combo_paises').val();
        var url = "index.php?r=Clients/usersC/citiesCountry/country/"+valor;
    $.getJSON( url, {
        format: "json"
      }).done(function( data ) {
          debugger;
          $('#Clients_id_ciudad').length=0;
          var salida= [];
          $.each( data, function( i, item ) {
              salida.push('<option value="'+i+'">'+ item+"</option>");
          });
          $('#Clients_id_ciudad').html(salida.join(''));
        }); 
    });
    <?php 
    if(!$model->isNewrecord){
        echo "$('#combo_paises').trigger('change');";
    }
    ?>
    $("#combo_perfil").change(function(){
        if($("#combo_perfil").val()=="6"){
           $('#datosTaker').show();   
           $('#btnTaker').show();   
        } else {
           $('#datosTaker').hide();   
           $('#btnTaker').hide();   
        }
    })    
    $(".tool_tip").mouseover(function(){
           eleOffset = $(this).offset();
          $(this).next().fadeIn("fast").css({
                                  left: eleOffset.left + $(this).outerWidth(),
                                  top: eleOffset.top
                          });
          }).mouseout(function(){
                  $(this).next().hide();
          });
$('#UsersC_ruta_foto').bind('change', function() {
    if(this.files[0].size > '2048000'){
            alert('The image excedes the size limit. Please select an image up to 2 Mb');
            $('#UsersC_ruta_foto').val("");
            document.getElementById('UsersC_ruta_foto').value = '';

    }
});
</script>
    
    
    
<?php /*
        <?php $path = Yii::app()->getBaseUrl(true)."/js/scriptCam/"; ?>
		<script language="JavaScript"> 
			$(document).ready(function() {
				$("#webcam").scriptcam({
                                    path:'<?php echo $path;?>',
					showMicrophoneErrors:false,
					onError:onError,
					cornerRadius:20,
					disableHardwareAcceleration:1,
					cornerColor:'e3e5e2',
					onWebcamReady:onWebcamReady,
					uploadImage:'upload.gif',
					onPictureAsBase64:base64_tofield_and_image
				});
			});
			function base64_tofield() {
				$('#formfield').val($.scriptcam.getFrameAsBase64());
			};
			function base64_toimage() {
				$('#image').attr("src","data:image/png;base64,"+$.scriptcam.getFrameAsBase64());
			};
			function base64_tofield_and_image(b64) {
				$('#formfield').val(b64);
				$('#image').attr("src","data:image/png;base64,"+b64);
			};
			function changeCamera() {
				$.scriptcam.changeCamera($('#cameraNames').val());
			}
			function onError(errorId,errorMsg) {
				$( "#btn1" ).attr( "disabled", true );
				$( "#btn2" ).attr( "disabled", true );
				alert(errorMsg);
			}			
			function onWebcamReady(cameraNames,camera,microphoneNames,microphone,volume) {
				$.each(cameraNames, function(index, text) {
					$('#cameraNames').append( $('<option></option>').val(index).html(text) )
				}); 
				$('#cameraNames').val(camera);
			}
		</script> 
	</head>
	<body>
		<div style="width:330px;float:left;">
			<div id="webcam">
			</div>
			<div style="margin:5px;">
				<img src="webcamlogo.png" style="vertical-align:text-top"/>
				<select id="cameraNames" size="1" onChange="changeCamera()" style="width:245px;font-size:10px;height:25px;">
				</select>
			</div>
		</div>
		<div style="width:135px;float:left;">
			<p><button class="btn btn-small" id="btn1" onclick="base64_tofield()">Snapshot to form</button></p>
			<p><button class="btn btn-small" id="btn2" onclick="base64_toimage()">Snapshot to image</button></p>
		</div>
		<div style="width:200px;float:left;">
			<p><textarea id="formfield" style="width:190px;height:70px;"></textarea></p>
			<p><img id="image" style="width:200px;height:153px;"/></p>
		</div>
	</body>
</html>
    */?>

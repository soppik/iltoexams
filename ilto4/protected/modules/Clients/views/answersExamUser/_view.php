<?php
/* @var $this AnswersExamUserController */
/* @var $data AnswersExamUser */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_respuesta_examen_usuario')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_respuesta_examen_usuario), array('view', 'id'=>$data->id_respuesta_examen_usuario)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_examen_usuario')); ?>:</b>
	<?php echo CHtml::encode($data->id_examen_usuario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_pregunta')); ?>:</b>
	<?php echo CHtml::encode($data->id_pregunta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_respuesta')); ?>:</b>
	<?php echo CHtml::encode($data->id_respuesta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('escala')); ?>:</b>
	<?php echo CHtml::encode($data->escala); ?>
	<br />


</div>
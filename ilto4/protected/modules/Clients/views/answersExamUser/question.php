<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$pregunta = Questions::model()->findByPk($parametros['id_pregunta']);
$tplQuestion = "tpl_".$pregunta->template."_txtQuestion";
$tplAnswer = "tpl_".$pregunta->template."_txtAnswer";
echo "<div>";
if($pregunta->tipo_presentacion=='S'){
   
    echo  "<div class='txtQuestion $tplQuestion'>";
    echo  $pregunta->texto;
    echo  "</div><div class='respuesta $tplAnswer'>";
    $this->widget('zii.widgets.jui.CJuiSliderInput',array(
            'name'=>$parametros['nombreCampo'],
            'value'=>0,
            'model'=>$parametros['model'],
            'event'=>'change',
            // additional javascript options for the slider plugin
            'options'=>array(
                'min'=>0,
                'max'=>100,
                'slide'=>'js:function(event,ui){$("#amt_'.$parametros['id_pregunta'].'").html(ui.value+"%");$("#response").val(1);}',
                ),
            'htmlOptions'=>array(
                'style'=>'height:20px; width:120px;float:left; margin-left:20px;',
                'class'=>'respuesta',
            ),
        ));
    echo  "<span type='text' id='amt_".$parametros['id_pregunta']."'  style='float:left;margin-left:20px;'>0 %</span>";
    echo  "</div>";
    echo "<script>hacetotal=1;</script>";
}
if($pregunta->tipo_presentacion=='D'){
    echo "<div style='font-size:140%; color:blue; margin-top:20px;  margin-bottom:10px;'>Select the correct answer according to the sentence, question or answer in the drop down menu.</div>";
    echo  "<div class='txtQuestion $tplQuestion'><p class='qtitle'>";
    if(strlen($pregunta->ruta_imagen)>0){
        echo  "<img src='".Yii::app()->baseUrl."/images/questions/tn2_".$pregunta->ruta_imagen."'><br>";
    }
    if(strlen($pregunta->ruta_audio)>0){
        Yii::app()->controller->widget("ext.jouele.jouele.Jouele", array(
                            "file" => Yii::app()->baseUrl."/".$pregunta->ruta_audio,
                            "name" => "Question",
                            "htmlOptions" => array(
                                "class" => "jouele-skin-silver",
                                "style"=>"width:180px; background-color:#f2f2f2",
                             )
                ));
    }
    echo "<br>";
    echo  "</div><div class='respuesta $tplAnswer'>";
    $posIni = strpos($pregunta->texto,'DROP_HERE')-1;
    $modResPreg = Answers::model()->findAllByAttributes(array('id_pregunta'=>$parametros['id_pregunta']));
    shuffle($modResPreg);
    $arrResp = array();
    foreach($modResPreg as $respuesta){
        $arrResp[$respuesta->id_respuesta]=$respuesta->texto;
    }
    echo substr($pregunta->texto,0,$posIni)."&nbsp;<span>";
    echo CHtml::dropDownList($parametros['nombreCampo'],'',$arrResp,array('empty' => 'Choose one...','onclick'=>'do_response();','class'=>'drop-answer'));
    echo "</span>".substr($pregunta->texto,$posIni+10);
    echo  "</div>";
}
if($pregunta->tipo_presentacion=='M'){
    echo "<div style='font-size:140%; color:blue; margin-top:20px; margin-bottom:10px;'>Choose the correct answer.</div>";
    echo  "<div class='txtQuestion $tplQuestion'>";
    echo "<script>marcaradio=1;</script>";
    if(strlen($pregunta->texto)>0 && $pregunta->texto !="nbsp;" ){
        echo  $pregunta->texto; 
        echo "<br>";
    }
    if(strlen($pregunta->ruta_imagen)>0){
        echo  "<img src='".Yii::app()->baseUrl."/images/questions/tn3_".$pregunta->ruta_imagen."'><br><br>";
    }
    if(strlen($pregunta->ruta_audio)>0){
        Yii::app()->controller->widget("ext.jouele.jouele.Jouele", array(
                            "file" => Yii::app()->baseUrl."/".$pregunta->ruta_audio,
                            "name" => "Question",
                            "htmlOptions" => array(
                                "class" => "jouele-skin-silver",
                                "style"=>"width:180px; background-color:#f2f2f2",
                             )
                ));
    }
    $modResPreg = Answers::model()->findAllByAttributes(array('id_pregunta'=>$parametros['id_pregunta']));
    shuffle($modResPreg);
    $col=1;
    echo "</div><div style='vertical-align:middle; padding-left:10px; margin-top:40px;' class='respuesta $tplAnswer'>";
    if(!is_null($pregunta->titulo_respuesta)){
        echo "<span style='color:red'>".$pregunta->titulo_respuesta."</span><br>";
    }
    echo "<table style='width:100%; margin-top:10px;'>";
    echo '<input type="radio" id="btnFake" checked="" style="display:none"  value="-1" name="'.$parametros['nombreCampo'].'">';
    foreach($modResPreg as $respuesta){
        if($col==1){
            echo "<tr>";
        }
        echo "<td class='td_$tplAnswer' style='width:50%'>";
        echo '<div class="radio"> <label class="i-checks">';
        echo '<input type="radio" onchange="do_response();"  value='.$respuesta->id_respuesta.' name="'.$parametros['nombreCampo'].'"> <i></i>'.$respuesta->texto;
        if(strlen($respuesta->ruta_imagen)>0){  
            echo  "<img class='img_answer' src='".Yii::app()->baseUrl."/images/answers/tn2_".$respuesta->ruta_imagen."'>";
        }
        if(strlen($respuesta->ruta_audio)>0){
                    Yii::app()->controller->widget("ext.jouele.jouele.Jouele", array(
                            "file" => Yii::app()->baseUrl."/".$respuesta->ruta_audio,
                            "name" => "Question",
                            "ready"=> 'function(){ $(this).jPlayer("play");}',
                            "htmlOptions" => array(
                                "class" => "jouele-skin-silver",
                                "style"=>"width:180px; background-color:#f2f2f2",
                             )
                ));
        }
	echo '</label></div>';
        echo  "</td>";
        $col++;
        if($pregunta->template==6){ //A una sola columna
            $col=3;
        }
        if($col==3){
            echo "</tr>";
            $col=1;
        }
    }
    echo  "</table></div>";
}
echo  "</div>";
echo '<script>function do_response(){debugger;$("#response").val(1);}</script>';
?>

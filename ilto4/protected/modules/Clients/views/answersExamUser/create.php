<?php
/* @var $this AnswersExamUserController */
/* @var $model AnswersExamUser */

$this->breadcrumbs=array(
	'Answers Exam Users'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage AnswersExamUser', 'url'=>array('admin')),
);
?>

<h1>Create AnswersExamUser</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
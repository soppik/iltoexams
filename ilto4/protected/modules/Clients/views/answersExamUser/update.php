<?php
/* @var $this AnswersExamUserController */
/* @var $model AnswersExamUser */

$this->breadcrumbs=array(
	'Answers Exam Users'=>array('index'),
	$model->id_respuesta_examen_usuario=>array('view','id'=>$model->id_respuesta_examen_usuario),
	'Update',
);

$this->menu=array(
	array('label'=>'Create AnswersExamUser', 'url'=>array('create')),
	array('label'=>'Manage AnswersExamUser', 'url'=>array('admin')),
);
?>

<h1>Update AnswersExamUser <?php echo $model->id_respuesta_examen_usuario; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
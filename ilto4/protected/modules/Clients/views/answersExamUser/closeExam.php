<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<script>
    function cerrar(){
        window.opener.location.reload();
        window.close();
    }
    </script>
<style>
.myButton {
	background-color:#44c767;
	-moz-border-radius:39px;
	-webkit-border-radius:39px;
	border-radius:39px;
	border:7px solid #18ab29;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Arial;
	font-size:18px;
	font-weight:bold;
	padding:20px 56px;
	text-decoration:none;
	text-shadow:0px 1px 0px #2f6627;
}
.myButton:hover {
	background-color:#5cbf2a;
}
.myButton:active {
	position:relative;
	top:1px;
}
</style>
<?php
if(!is_null($licencia)){ //Porque lo manda un tutor?>
<div style="text-align: center">
    <p style="padding: 20px 5px 5px 40px; font-size: 18px; text-align:center ">
        <img src="images/thanksteacher2.jpg"/>
</p>
    <p>                                
        <a href="index.php?r=Clients/ExamsUser/admin&id=<?php echo $licencia;?>" class="myButton">Back to User Admin</a>
    </p>
</div>
<?php
} else {
?>
<div>
<div style="font-family: Arial !important; text-align: center">
    <p style="padding: 20px 5px 5px 40px; font-size: 24px; text-align:center ">
    <strong>Thank you for answering the Test.</strong></p>
    <p style="padding: 20px 5px 5px 40px; font-size: 24px; text-align:center ">
    <strong>The results will be given by your test administrator.</strong></p>
    <p>                                
        <a href="javascript:cerrar();" class="myButton">Close this Window</a>
    </p>
</div>
<div style="logos_close">
    <br><br><br><br>
    <table style="width:100%">
        <tr>
            <td style="width:20%;">
            </td>
            <td colspan="3" style="width:60%;"><center>
                <img style="margin-left:auto; width:200px;" alt="ILTO Exams" src="<?php echo Yii::app()->getBaseUrl(true)."/images/logo_ILTO.png";?>" />
        </center>
            </td>
            <td style="width:20%;">
            </td>
        </tr>
        <tr>
            <td style="width:20%;">
            </td>
            <td style="width:20%;">
                <img style="float:right;  width:142px;" src="<?php echo Yii::app()->getBaseUrl(true)."/images/TECS.png";?>"/>
            </td>
            <td style="width:20%;"><center>
                <img style="margin-left:auto;"   alt="ILTO Exams" src="<?php echo Yii::app()->getBaseUrl(true)."/images/EDE.png";?>" />
        </center>
            </td>
            <td style="width:20%;">
                <img style="float:left;" src="<?php echo Yii::app()->getBaseUrl(true)."/images/RPT.png";?>"/>
            </td>
            <td style="width:20%;">
            </td>
        </tr>
    </table>
</div>
</div>
<?php } ?>
<?php

/**
 * This is the model class for table "groups_users_clients".
 *
 * The followings are the available columns in table 'groups_users_clients':
 * @property integer $id
 * @property string $id_cliente
 * @property string $nombre
 *
 * The followings are the available model relations:
 * @property Clients $idCliente
 * @property UsersC[] $usersCs
 */
class GroupsUsersClients extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'groups_users_clients';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_cliente, nombre, estado', 'required'),
			array('id_cliente', 'length', 'max'=>20),
			array('nombre', 'length', 'max'=>200),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_cliente, nombre, estado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idCliente' => array(self::BELONGS_TO, 'Clients', 'id_cliente'),
			'usersCs' => array(self::HAS_MANY, 'UsersC', 'id_grupo_cliente'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_cliente' => 'Client',
			'nombre' => 'Name',
			'estado' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_cliente',$this->id_cliente,true);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('estado',$this->estado,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'pagination'=>array('pageSize'=>50,),

		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return GroupsUsersClients the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        public function defaultScope()
    {
            if(Yii::app()->user->isGuest){
                return array();
            } else {
                return array(
                    'condition'=>"id_cliente='".Yii::app()->user->getState('cliente')."'",
                );
            }
    }
}

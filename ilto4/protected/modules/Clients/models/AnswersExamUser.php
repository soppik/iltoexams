<?php

/**
 * This is the model class for table "answers_exam_user".
 *
 * The followings are the available columns in table 'answers_exam_user':
 * @property string $id_respuesta_examen_usuario
 * @property integer $id_examen_usuario
 * @property integer $id_pregunta
 * @property integer $id_respuesta
 * @property integer $escala
 *
 * The followings are the available model relations:
 * @property ExamsUser $idExamenUsuario
 * @property Questions $idPregunta
 * @property Answers $idRespuesta
 */
class AnswersExamUser extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'answers_exam_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_examen_usuario, id_pregunta', 'required'),
			array('id_examen_usuario, id_pregunta, id_respuesta, escala', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_respuesta_examen_usuario, id_examen_usuario, id_pregunta, id_respuesta, escala', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idExamenUsuario' => array(self::BELONGS_TO, 'ExamsUser', 'id_examen_usuario'),
			'idPregunta' => array(self::BELONGS_TO, 'Questions', 'id_pregunta'),
			'idRespuesta' => array(self::BELONGS_TO, 'Answers', 'id_respuesta'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_respuesta_examen_usuario' => 'Id Respuesta Examen Usuario',
			'id_examen_usuario' => 'Id Examen Usuario',
			'id_pregunta' => 'Id Pregunta',
			'id_respuesta' => 'Id Respuesta',
			'escala' => 'Escala',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_respuesta_examen_usuario',$this->id_respuesta_examen_usuario,true);
		$criteria->compare('id_examen_usuario',$this->id_examen_usuario);
		$criteria->compare('id_pregunta',$this->id_pregunta);
		$criteria->compare('id_respuesta',$this->id_respuesta);
		$criteria->compare('escala',$this->escala);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'pagination'=>array('pageSize'=>50,),

		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AnswersExamUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

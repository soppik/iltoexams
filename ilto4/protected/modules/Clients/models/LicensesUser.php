<?php

/**
 * This is the model class for table "licenses_user".
 *
 * The followings are the available columns in table 'licenses_user':
 * @property integer $id_licencias_usuario
 * @property integer $id_licencia_cliente
 * @property string $id_usuario
 * @property string $fecha_asignacion
 * @property string $estado
 *
 * The followings are the available model relations:
 * @property ExamsUser[] $examsUsers
 * @property LicensesClient $idLicenciaCliente
 * @property UsersC $idUsuario
 */
class LicensesUser extends CActiveRecord
{
    public $usuario_id;
    public $usuario_nombre;
    public $usuario_apellido;
    public $elCliente;
    public $usuario_grupo;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'licenses_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_licencia_cliente, id_usuario, fecha_asignacion, estado', 'required'),
			array('id_licencia_cliente', 'numerical', 'integerOnly'=>true),
			array('calificacion', 'numerical'),
			array('nivel', 'length', 'max'=>4),
			array('id_usuario', 'length', 'max'=>20),
			array('estado', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_licencias_usuario, usuario_grupo, elCliente, usuario_nombre, usuario_apellido, usuario_id, id_licencia_cliente, id_usuario, fecha_asignacion, estado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'examsUsers' => array(self::HAS_MANY, 'ExamsUser', 'id_licencia_usuario'),
			'idLicenciaCliente' => array(self::BELONGS_TO, 'LicensesClient', 'id_licencia_cliente'),
			'idUsuario' => array(self::BELONGS_TO, 'UsersC', 'id_usuario'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_licencias_usuario' => 'Id',
			'id_licencia_cliente' => 'Client License',
			'id_usuario' => 'User',
			'fecha_asignacion' => 'Allocation Date',
			'fecha_presentacion' => 'Test Date',
                    'nivel'=>'CEF Level',
                    'calificacion'=>'Score',
			'hora' => 'Time',
			'estado' => 'Status',
                    'usuario_id'=>'User Id',
                    'usuario_grupo'=>'Group',
                    'usuario_nombre'=>'Name',
                    'usuario_apellido'=>'Last Name',
                    'elCliente'=>'Client',
                    
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                $criteria->with = array("idUsuario", "idLicenciaCliente");
    		$criteria->compare('t.id_licencias_usuario',$this->id_licencias_usuario);
		$criteria->compare('t.id_licencia_cliente',$this->id_licencia_cliente);
		$criteria->compare('t.id_usuario',$this->id_usuario,true);
		$criteria->compare('t.fecha_asignacion',$this->fecha_asignacion,true);
		$criteria->compare('t.estado',$this->estado,true);
		$criteria->compare('t.id_usuario',$this->usuario_id,true);
		$criteria->compare('t.nivel',$this->nivel,false);
		$criteria->compare('idUsuario.nombres',$this->usuario_nombre,true);
		$criteria->compare('idUsuario.id_grupo_cliente',$this->usuario_grupo,true);
		$criteria->compare('idUsuario.apellidos',$this->usuario_apellido,true);
		$criteria->compare('idLicenciaCliente.id_cliente',$this->elCliente,true);
                

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                    'pagination'=>array('pageSize'=>50,),
                    'sort'=>array(
                            'attributes'=>array(
                                'usuario_id'=>array(
                                    'asc'=>'id_usuario',
                                    'desc'=>'id_usuario DESC',
                                ),
                                'usuario_nombre'=>array(
                                    'asc'=>'idUsuario.nombres',
                                    'desc'=>'idUsuario.nombres DESC',
                                ),
                                'usuario_apellido'=>array(
                                    'asc'=>'idUsuario.apellidos',
                                    'desc'=>'idUsuario.apellidos DESC',
                                ),
                                '*',
                            ),
                        ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LicensesUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

        public function defaultScope()
    {
            if(Yii::app()->user->isGuest){
                return array();
            } else {
                return array(
//                    'with'=>'idLicenciaCliente',
//                    'condition'=>"idLicenciaCliente.id_cliente='".Yii::app()->user->getState('cliente')."'",
                );
            }
    }
        
}

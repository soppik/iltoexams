<?php

class AnswersExamUserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('exam','takeExam', 'answer','admin','delete','close','finishExam','myExams'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

        public function actionMyExams(){
                $userId = Yii::app()->user->getState('usuario');
                $modLicUs = LicensesUser::model()->findByAttributes(array('id_usuario'=>$userId));
                $license = $modLicUs->id_licencias_usuario;
		$criteria=new CDbCriteria;
		$criteria->addcondition('id_licencia_usuario = '.$license.' and estado != "F"');
                $examenes = ExamsUser::model()->findAll($criteria);
                $examUserId=NULL;
                if(count($examenes)>0){
                    $examUserId = $examenes[0]->id_examen_usuario;
                    $this->render('myExams',array('userId'=>$userId,'license'=>$license,'examUserId'=>$examUserId));
                } else {
                    $this->render('noExams');
                }
        }
        
        public function actionAnswer()
	{
		$model=new AnswersExamUser;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AnswersExamUser']))
		{
			$model->attributes=$_POST['AnswersExamUser'];
                        //$mod2 = AnswersExamUser::model()->findByAttributes(array(''))
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

        /**
	 * Exam ==> Generar el exámen del usuario de acuerdo a lo que tenga asignado o de acuerdo al proceso que lleve.
	 * Si no ha terminado el exámen debe ponerlo en el punto en que vaya
	 */
	public function actionExam($userId,$license=NULL,$now=NULL)
	{
            //Identificar si el usuario ya ha presentado algun exámen y enviarlo al ultimo punto
            //Identificar si es el examen oral que debe tomar el tutor para enviarlo de una vez a presentar
            if(!is_null($license)){
                //Aquí viene cuando acaba de ser creado y le van a tomar el examen de una vez
                $modExamUser = ExamsUser::model()->findAllByAttributes(array('id_licencia_usuario'=>$license));
                foreach($modExamUser as $examen){
                    if($examen->idExamen->tutor=="1"){
                        //Presentar el Examen
                        $this->actionTakeExam($userId,$license,$examen->id_examen_usuario);
                        break;
                    }
                }
            } else {
                //Verificar el estado en el que el usuario se encuentra 
            }
	}
        
        /**
	 * TakeExam ==> Presentar el Exámen
	 * Si no ha terminado el exámen debe ponerlo en el punto en que vaya
	 */
        public function actionTakeExam($userId,$license,$examUserId){
            //Verificar cuantas preguntas tiene el examen
            //Mirar si las preguntas son fijas o aleatorias
            //Ver cuantas preguntas hay pendientes
            $modExamsUser = ExamsUser::model()->findByPk($examUserId);
            $muestraIntro = Licenses::model()->findByPk($modExamsUser->idLicenciaUsuario->idLicenciaCliente->id_licencia)->intro_secciones;
            $tipologo = Licenses::model()->findByPk($modExamsUser->idLicenciaUsuario->idLicenciaCliente->id_licencia)->logo;
            $examId = $modExamsUser->id_examen;
            $modExamen = Exams::model()->findByPk($modExamsUser->id_examen);
            $totalPreguntas = $modExamen->numero_preguntas;
            $totalContestadas = 0;
            if(!isset($_POST['preguntas'])){ //Empezar o Continuar con el Examen
                //Si las preguntas NO son aleatorias
                    //Seleccionar todas las preguntas del examen
                    if($modExamen->preguntas_aleatorias==1){
                        $modPeguntas = Questions::model()->findAllByAttributes(array('id_seccion'=>$modExamen->id_seccion));
                    } else {
                        $modPeguntas = QuestionsExam::model()->findAllByAttributes(array('id_examen'=>$examId));
                    }
                    $modContestadas = AnswersExamUser::model()->findAllByAttributes(array('id_examen_usuario'=>$examUserId));
                    $totalContestadas = count($modContestadas);
                    if($totalContestadas>0){
                        if($totalPreguntas > $totalContestadas){
                            //Continuar con el examen porque algo paso
                            $arregloContestadas =  array_keys(CHtml::listData(AnswersExamUser::model()->findAllByAttributes(array('id_examen_usuario'=>$examUserId)),'id_pregunta','id_respuesta'));
                            //Seleccionar todas las preguntas del examen que no se hayan contestado
                            $contestadas = "('".implode("','",$arregloContestadas)."')";
                            $criterio = new CDbCriteria;
                            $criterio->select = '*';
                            $criterio->with ="idPregunta";
                            $criterio->condition = 'id_examen = '.$examId.' and t.id_pregunta not in '.$contestadas;
                            if(!is_null(Yii::app()->user->getState('relacionada'))){
                                $criterio->condition.= ' and relacionada='.Yii::app()->user->getState('relacionada');
                                $modPeguntas = QuestionsExam::model()->findAll($criterio);
                                $texto=" es este: ".print_r($criterio,true);
                                Yii::app()->user->setState('txtquery',$texto);
                                if(count($modPeguntas)<=0){ 
                                    Yii::app()->user->setState('relacionada',NULL);
                                    $criterio->condition = 'id_examen = '.$examId.' and t.id_pregunta not in '.$contestadas;
                                    $modPeguntas = QuestionsExam::model()->findAll($criterio);
                                }
                            }
                            if(count($modPeguntas)>0){
                                if($modExamen->vistaunoauno==1){
                                    shuffle($modPeguntas);
                                }
                                $model = new AnswersExamUser;
                                if($tipologo==2){
                                    $rutalogo = "images/RPT.png";
                                } elseif($tipologo==1) {
                                    $rutalogo = "images/TECS.png";
                                } else {
                                    $rutalogo = "images/EDE.png";
                                }
                                Yii::app()->user->setState('rutalogoexam',$rutalogo);
                                if($muestraIntro == 1){
                                    $muestraIntro = true;
                                } else {
                                    $muestraIntro=false;
                                }
                                $this->render('askQuestion',array(
                                'model'=>$model,'unoauno'=>$modExamen->vistaunoauno,'totalP'=>$totalPreguntas,'totalC'=>$totalContestadas,
                                    'modpreguntas'=>$modPeguntas,'titulo'=>$modExamen->nombre,'muestraIntro'=>$muestraIntro,
                                    'bgColor'=>$modExamen->idSeccion->bg_color,'txtColor'=>$modExamen->idSeccion->txt_color
                                ));
                            } else {
                                //No hay mas preguntas
                                $this->actionFinishExam($userId,$license,$examUserId);
                            }                       
                        } else {
                            //No hay mas preguntas
                            $this->actionFinishExam($userId,$license,$examUserId);
                        }                       
                    } else {
                        //Seleccionar la pregunta a realizar
                        //Empezar con el examen por primera vez.
                        if($modExamen->vistaunoauno=1){ //Solo revolver cuando son uno a uno
                            shuffle($modPeguntas);
                        }
                        $model = new AnswersExamUser;
                            if($tipologo==2){
                                $rutalogo = "images/RPT.png";
                            } elseif($tipologo==1) {
                                $rutalogo = "images/TECS.png";
                            } else {
                                $rutalogo = "images/EDE.png";
                            }
                        Yii::app()->user->setState('rutalogoexam',$rutalogo);
                            if($muestraIntro == 1){
                                $muestraIntro = true;
                            } else {
                                $muestraIntro=false;
                            }
                        $this->render('askQuestion',array(
                        'model'=>$model,'unoauno'=>$modExamen->vistaunoauno,'totalP'=>$totalPreguntas,'totalC'=>$totalContestadas,
                            'modpreguntas'=>$modPeguntas,'titulo'=>$modExamen->nombre,
                            'bgColor'=>$modExamen->idSeccion->bg_color,'txtColor'=>$modExamen->idSeccion->txt_color,'muestraIntro'=>$muestraIntro
                        ));
                    }
               
            } else { 
                //Guardar las respuestas
                foreach($_POST['preguntas'] as $pregunta=>$respuesta){
                    $modRespuestas = new AnswersExamUser;
                    $modRespuestas->id_pregunta = $pregunta;
                    if($modRespuestas->idPregunta->tipo_presentacion=="S"){
                        $modRespuestas->escala = $respuesta;
                    } else {
                        if($respuesta == -1){
                            $modRespuestas->id_respuesta = NULL;
                        } else {
                            $modRespuestas->id_respuesta = $respuesta;
                        }
                    }
                    $modRespuestas->id_examen_usuario = $examUserId;
                    try{
                     $modRespuestas->save();
                    }
                    catch(Exception $e){}
                }
                if($modExamen->vistaunoauno==1){ //Viene una sola pregunta
                    //Continuar con la siguiente pregunta
                    //if($modExamen->preguntas_aleatorias==0){
                        //Preguntas contestadas
                        $arregloContestadas =  array_keys(CHtml::listData(AnswersExamUser::model()->findAllByAttributes(array('id_examen_usuario'=>$examUserId)),'id_pregunta','id_respuesta'));
                        //Seleccionar todas las preguntas del examen que no se hayan contestado
                        $contestadas = "('".implode("','",$arregloContestadas)."')";
                        $totalContestadas = count($arregloContestadas);
                        if($modExamen->numero_preguntas>$totalContestadas){
                            $criterio = new CDbCriteria;
                            $criterio->select = '*';
                            $criterio->condition = 'id_examen = '.$examId.' and id_pregunta not in '.$contestadas;
                            $criterio->with ="idPregunta";
                            $criterio->condition = 'id_examen = '.$examId.' and t.id_pregunta not in '.$contestadas;
                            if(!is_null(Yii::app()->user->getState('relacionada'))){
                                $criterio->condition.= ' and relacionada='.Yii::app()->user->getState('relacionada');
                                $modPeguntas = QuestionsExam::model()->findAll($criterio);
                                $texto=" es este: ".print_r($criterio,true);
                                Yii::app()->user->setState('txtquery',$texto);
                                if(count($modPeguntas)<=0){ 
                                    Yii::app()->user->setState('relacionada',NULL);
                                    $criterio->condition = 'id_examen = '.$examId.' and t.id_pregunta not in '.$contestadas;
                                    $modPeguntas = QuestionsExam::model()->findAll($criterio);
                                }
                            } else {
                                $modPeguntas = QuestionsExam::model()->findAll($criterio);
                            }
                            if(count($modPeguntas)>0){
                                shuffle($modPeguntas);
                                $model = new AnswersExamUser;
                                if($tipologo==2){
                                    $rutalogo = "images/RPT.png";
                                } elseif($tipologo==1) {
                                    $rutalogo = "images/TECS.png";
                                } else {
                                    $rutalogo = "images/EDE.png";
                                }
                                Yii::app()->user->setState('rutalogoexam',$rutalogo);
                                $this->render('askQuestion',array(
                                'model'=>$model,'unoauno'=>$modExamen->vistaunoauno,'totalP'=>$totalPreguntas,'totalC'=>$totalContestadas,
                                    'modpreguntas'=>$modPeguntas,'titulo'=>$modExamen->nombre,
                                    'bgColor'=>$modExamen->idSeccion->bg_color,'txtColor'=>$modExamen->idSeccion->txt_color,'muestraIntro'=>false
                                ));
                            } else {
                                //No hay mas preguntas
                                $this->actionFinishExam($userId,$license,$examUserId);
                            }
                        } else {
                            //No hay mas preguntas
                            $this->actionFinishExam($userId,$license,$examUserId);
                        }
                    //}
                } else {
                    $this->actionFinishExam($userId,$license,$examUserId);
                }
            }
        }

        /**
         * Finish Exam ==> Cerrar el primer exámen. Para enviar el correo al usuario con la agenda
         * @param type $userId
         * @param type $license
         * @param type $examUserId
         */
        public function actionFinishExam($userId,$license,$examUserId){
            //Calificar el exámen y darlo por terminado para el usuario en esa licencia
            $this->calificarExamen($examUserId);
            //Si el que llega calificando es un TAKER hay que enviarle el siguiente exámen
            if(Yii::app()->user->getState('id_perfil')==6){
		$criteria=new CDbCriteria;
		$criteria->addcondition('id_licencia_usuario = '.$license.' and estado != "F"');
                $examenes = ExamsUser::model()->findAll($criteria);
                if(count($examenes)>0){
                    $examUserId = $examenes[0]->id_examen_usuario;
                    unset($_POST['preguntas']);
                    $this->redirect('index.php?r=Clients/AnswersExamUser/takeExam&userId='.$userId.'&license='.$license.'&examUserId='.$examUserId);
                } else {
                    $modUtil = new Utilidades();
                    $modLicenciaUsuario = LicensesUser::model()->findByAttributes(array('id_licencias_usuario'=>$license));
                    $resultados= $modUtil->calificaLicencia($license);
                    $modLicenciaUsuario->calificacion= $resultados['calificacion'];
                    $modLicenciaUsuario->nivel= $resultados['nivel'];
                    $modLicenciaUsuario->estado="F";
                    $modLicenciaUsuario->save(true,array('estado','nivel','calificacion'));
                    $modUtil = new Utilidades;
                    if($modLicenciaUsuario->idLicenciaCliente->idLicencia->genera_certificado==1){
                        $modUtil->enviarCertificado($userId,$license);
                    } else {
                        Yii::app()->user->setFlash('success','Ask your Test Administrator for the test results');
                    }
                   $this->renderPartial('closeExam');
                }
             }else {
                //Si es un Tutor el que manda
               $modUsuario = UsersC::model()->findByAttributes(array('id_usuario_c'=>$userId));
               if($modUsuario->estado=="P"){ //Pendiente de registro
                   $modUtil = new Utilidades();
                   //Enviar registro al usuario
                   //$modUtil->actionEnviarRegistro($userId);
                   //Actualizar el estado
                   $modUsuario->estado="R";
                   $modUsuario->save(true,array('estado'));
               }
               $this->renderPartial('closeExam',array('licencia'=>$license));
            }
        }
        
        public function actionClose(){
              $this->renderPartial('closeExam');

        }
        /**
         * 
         * @param type $examUserId
         */
        public function calificarExamen($examUserId){
            //Seleccionar todas las preguntas del examen
            $modUtil = new Utilidades();
            $modPreguntasEx = AnswersExamUser::model()->findAllByAttributes(array('id_examen_usuario'=>$examUserId));
            $numEsc=0;
            $acumEsc=0;
            $numAciertos=0;
            $acumAciertos=0;
            foreach($modPreguntasEx as $rowPregunta){
                if($rowPregunta->idPregunta->tipo_presentacion=="S"){
                    $acumEsc+=$rowPregunta->escala;
                    $numEsc++;
//                    echo "<br>sumando ".$rowPregunta->escala;
                } else {
                    $modResp = Answers::model()->findByPk($rowPregunta->id_respuesta);
                    if($modResp->es_verdadera==1){
                        $acumAciertos++;
                    }
                    $numAciertos++;
                }
            }
            //Guardar Calificacion
            $modExamUser = ExamsUser::model()->findByPk($examUserId);
//             echo "<br>total escalas ".$numEsc;
//             echo "<br>acum escalas ".$acumEsc;
//             die();
            if($numEsc>0){
                $modExamUser->calificacion = $acumEsc/$numEsc;
            }else{
                $modExamUser->calificacion = ($acumAciertos/$numAciertos)*100;
            }
            $modExamUser->nivel = $modUtil->nivelExamen($modExamUser->calificacion);
            $modExamUser->estado="F";
            $modExamUser->save();
        }
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AnswersExamUser']))
		{
			$model->attributes=$_POST['AnswersExamUser'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('AnswersExamUser');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new AnswersExamUser('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['AnswersExamUser']))
			$model->attributes=$_GET['AnswersExamUser'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return AnswersExamUser the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=AnswersExamUser::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param AnswersExamUser $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='answers-exam-user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

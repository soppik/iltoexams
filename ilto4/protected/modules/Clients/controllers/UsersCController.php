<?php

class UsersCController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','email', 'create_exam','citiesCountry','update','admin','delete'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        


        public function actionCitiesCountry($country){
            $retorno = CHtml::listData(Cities::model()->findAllByAttributes(array('id_pais'=>$country)), 'id_ciudad', 'nombre');
            echo json_encode($retorno);
        }

        /**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($exam=NULL)
	{
		$model=new UsersC;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['UsersC']))
		{
                        $modUtil = new Utilidades();
			$model->attributes=$_POST['UsersC'];
                        $model->id_usuario_c=$model->numero_id;
                        $model->id_cliente = Yii::app()->user->getState('cliente');
                        $ruta_foto=CUploadedFile::getInstance($model,'ruta_foto');
                        $ahora = time();
                        $clave=$model->id_cliente;
                        $model->clave=md5($model->id_usuario_c);
                        $model->clave2=md5($model->id_usuario_c);
                        $model->estado="P";
			if($model->save(true,array('numero_id','id_cliente','id_usuario_c','nombres','apellidos','clave','email','id_perfil','tipo_id','numero_id','estado','id_pais','id_ciudad'))){
                            $modUtil = new Utilidades();
                            if(!is_null($ruta_foto)){
                                $extension = ".".strtolower($ruta_foto->getExtensionName());
                                $nombreImagen = $model->id_cliente."_".$model->id_usuario_c."_".$ahora.$extension;
                                $model->ruta_foto=$nombreImagen;
                                $model->save();
                                $ruta_foto->saveAs("images/users/".$nombreImagen);
                                $modUtil->generaThumb("images/users/",$nombreImagen);
                            }
                            if($model->id_perfil==6){
                                //$modUtil->enviarCuenta($model->id_usuario_c, 0);
                            } else {
                            $modUtil->enviarCuenta($model->id_usuario_c, 1);
                            }
                            if(is_null($exam)){
                                $this->redirect(array('admin'));
                            } else {
                                $this->redirect(array('ExamsUser/create&id='.$model->id_usuario_c));
                            }
                        }
		}
                $modGroups = GroupsUsersClients::model()->findAll();
                $modRooms = ClassRooms::model()->findAll();
                if(count($modGroups)<=0){
                    $mensaje = 'There are no User Groups created yet. You have to create at least One User Group to create a TAKER.';
                }
                if(count($modRooms)<=0){
                    $mensaje .= 'There are no Class Rooms created yet. You have to create at least One Class Room to assign an exam to a TAKER';
                }
                if(strlen($mensaje)>0) Yii::app()->user->setFlash('error',$mensaje);
		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionCreate_exam()
	{
            $this->actionCreate("*");
	}
        
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=  UsersC::model()->findByAttributes(array('id_usuario_c'=>$id));

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['UsersC']))
		{
                    $anteClave = $model->clave;
                    $model->attributes=$_POST['UsersC'];
                    $model->clave = $anteClave;
                    $model->clave2 = $anteClave;
                    $ruta_imagen=CUploadedFile::getInstance($model,'ruta_foto');
                    $modUtil = new Utilidades();
                    if($model->save(true,array('nombres', 'apellidos', 'email', 'id_cliente', 'id_perfil', 'tipo_id', 'numero_id', 'estado', 'id_pais', 'id_ciudad'))){
                        if(!is_null($ruta_imagen)){
                            //Si tenia una imagen, eliminarla
                            if(isset($_POST['ruta_imagen_actual'])){
                                $modUtil->eliminarThumbs("images/users/",$_POST['ruta_imagen_actual']);
                            }
                            //Nombre de la nueva imagen
                            $extension = ".".$ruta_imagen->getExtensionName();
                            $ahora = time();
                            $nombreImagen = $model->id_cliente."_".$model->id_usuario_c.$ahora.$extension;
                            $model->ruta_foto=$nombreImagen;
                            $model->save();
                            $ruta_imagen->saveAs("images/users/".$nombreImagen);
                            $modUtil->generaThumb("images/users/",$nombreImagen);
                        } elseif(strlen($_POST['ruta_imagen_actual'])>0){ //Si existia una imagen previa
                            if(isset($_POST['delete_imagen'])){
                                $model->ruta_foto=NULL;
                                $model->save(true,array('ruta_foto'));
                                $modUtil->eliminarThumbs("images/users/",$_POST['ruta_imagen_actual']);
                            }
                        }
                        $this->redirect(array('admin'));
                    }
		}
		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
                $modTmp = UsersC::model()->findByAttributes(array('id_usuario_c'=>$id,'id_cliente'=>Yii::app()->user->getState('cliente')));
                if(!is_null($modTmp)){
                    $modTmp->delete();
                }

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

     	public function actionEmail($id,$login=0)
        {
            $modUtil = new Utilidades();
            $modUtil->actionEnviarRegistro($id,$login);
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('UsersC');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new UsersC('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['UsersC']))
			$model->attributes=$_GET['UsersC'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return UsersC the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=UsersC::model()->findByAttributes(array('id_usuario_c'=>$id));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param UsersC $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='users-c-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

<?php

class DefaultController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}
        
        public function actionRememberPass(){
            $model=new RememberPass;
            if(isset($_POST['RememberPass'])){
                $datos=$_POST['RememberPass'];
               $modUtil = new Utilidades();
               if($modUtil->recoverPass($datos["agent"],$datos["username"],$datos["email"])){
                   $this->redirect(Yii::app()->createUrl('Clients/default/login'));
               }
            }
            $this->render('rememberPass',array('model'=>$model));
        }
        
        public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()){
        			$this->redirect('index.php?r=Clients/default/index');
                        } else{
                            $this->redirect('/examtecs2_0/administrator/index.php?loginFailed=1');
                        }
                                
		}
                if(isset($_GET['hash_ede'])){
                    $datos = base64_decode($_GET['hash_ede']);
                    $arreglo = explode('&',$datos);
                    $model->agent = $arreglo[0];
                    $model->username = $arreglo[1];
                    $model->password = $arreglo[2];
                    $_POST['LoginForm']['agent']= $arreglo[0];
                    $_POST['LoginForm']['username']= $arreglo[1];
                    $_POST['LoginForm']['password']= $arreglo[2];
                    if($model->validate() && $model->login())
                            $this->redirect('index.php?r=Clients/default/index&ede=1');
                    
                }
		// display the login form
		$this->render('login',array('model'=>$model));
	}

        public function actionLogout()
	{
		Yii::app()->user->logout();
                Yii::app()->user->setState('usuario',NULL);
                Yii::app()->user->setState('cliente',NULL);
                Yii::app()->user->setState('nombre_cliente',NULL);
                Yii::app()->user->setState('logo_cliente',NULL);
                Yii::app()->user->setState('nombres',NULL);
                Yii::app()->user->setState('apellidos',NULL);
                Yii::app()->user->setState('id_perfil',NULL);
                Yii::app()->user->setState('nombre_perfil',NULL);
		$this->redirect('/');
	}

}
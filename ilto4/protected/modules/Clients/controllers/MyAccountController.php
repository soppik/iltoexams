<?php

class MyAccountController extends Controller
{
    	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','licenses','statistics','buyLicenses'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

        public function actionBuyLicenses(){
            $model = new BuyLicensesForm();
		if(isset($_POST['BuyLicensesForm']))
		{
                    $model->attributes=$_POST['BuyLicensesForm'];
                    $licenseName = Licenses::model()->findByPk($model->licensetype)->nombre;
                    $mail = new YiiMailer('buyLicenses', array('model' => $model,'header_title'=>'Purchase Licenses',  'description'=>'Buy Licenses Form','licenseName'=>"$licenseName"));
                    $mail->setFrom("buy@iltoexams.com", "Buy Licenses");
                    $mail->setSubject("Licenses Purchase Request");
                    $mail->setTo("info@iltoexams.com");
                    //$mail->setTo("sairgo@gmail.com");
                    if ($mail->send()) {
                            Yii::app()->user->setFlash('success','Your request is being processed. One of our consultants will contact you to complete the purchase.');
                    } else {
                            Yii::app()->user->setFlash('error','Error while sending email: '.$mail->getError());
                    }
                    $this->redirect(array('index'));
		}
            $this->render('buyLicenses',array('model'=>$model));
        }
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionLicenses()
	{
		$this->render('licenses');
	}

	public function actionStatistics()
	{
		$this->render('statistics');
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
<?php

class ExamsUserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('viewDay','printDay','schedule', 'tieneTutor','create','update','admin','delete'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

        public function actionTieneTutor($id){
            $modUtil = new Utilidades();
            echo $modUtil->tieneTutor($id);
        }
        
        public function actionPrintDay($day,$month,$year,$room,$time){
            $nomSalon = ClassRooms::model()->findByPk($room)->nombre;
            $mPDF1 = Yii::app()->ePdf->mpdf();
            $informe='<html><head><style>table.externa{ border-spacing: 10px;width:640px;} td.externa{width:300px;} table.interna{border-width: 3px;border-spacing: 10px;border-style: dashed;border-color: gray;
	border-collapse: collapse;background-color: white; width:300px;} td.interna {border-width: thin; border-style: none;
	border-color: gray; background-color: white; }h2,h3{ text-align:center;}</style></head><body>';
            $informe.= "<h2>USER AND PASSWORD LIST FOR $nomSalon</h2>"; 
            $informe.= "<h3>(MM/DD/AAAA) $month/$day/$year</h3>"; 
            $criteria = new CDbCriteria();
            $criteria->addCondition("month(fecha_presentacion) = $month and year(fecha_presentacion)=$year and day(fecha_presentacion)=$day and salon=$room and hora='$time'");
            $criteria->group = "id_licencia_usuario";
            if(Yii::app()->user->getState('id_perfil')==4){ //Es un tutor, ver solo sus compromisos
                $criteria->addCondition("tutor = '$usuario'");
            }
            $model = ExamsUser::model()->findAll($criteria);            
            $informe .= '<table class="externa">';
            $col=0;
            foreach($model as $registro){
                if($col==0){
                    $informe .= "<tr>";
                }
                $informe .= '<td><table class="interna"><tr><td colspan="2" style="font-size:80%;">'.$registro->idLicenciaUsuario->idUsuario->getNombresApellidos().' please enter <strong>www.iltoexams.com</strong> in your browser, click on the Test Taker button and use the following information:</td></tr><tr><td>Agent:</td><td>'.$registro->idLicenciaUsuario->idUsuario->id_cliente.'</td></tr><tr><td>User:</td><td>'.$registro->idLicenciaUsuario->idUsuario->id_usuario_c.'</td></tr><tr><td>Password:</td><td>'.$registro->idLicenciaUsuario->idUsuario->id_usuario_c.'</td></tr></table></td>';
                $col++;
                if($col==2) {
                    $col=0;
                    $informe .= "</tr>";
                }
            }
            if($col==1) {
                $informe .= "<td></td></tr>";
            }
            $informe.='</table></body></html>';
            $mPDF1->WriteHTML($informe);
            $mPDF1->Output();
        }

        public function actionViewDay($day,$month,$year,$room){
            $usuario = Yii::app()->user->getState('usuario');
            $criteria = new CDbCriteria();
            $criteria->addCondition("month(fecha_presentacion) = $month and year(fecha_presentacion)=$year and day(fecha_presentacion)=$day and salon=$room");
            $criteria->group = "id_licencia_usuario";
            if(Yii::app()->user->getState('id_perfil')==4){ //Es un tutor, ver solo sus compromisos
                $criteria->addCondition("tutor = '$usuario'");
            }
            $model = ExamsUser::model()->findAll($criteria);            
            $nomSalon = ClassRooms::model()->findByPk($room)->nombre;
            $this->renderPartial('viewDay',array('model'=>$model, 'day'=>$day,'month'=>$month,'year'=>$year,'room'=>$room,'salon'=>$nomSalon));
        }
        
        public function actionSchedule($mes = NULL,$anio = NULL){
            if(is_null($mes)){
                $mes = date("m");
            }
            if(is_null($anio)){
                $anio = date("Y");
            }
            $mesPost=$mes+1;
            $mesAnte=$mes-1;
            $anioAnte = $anio;
            $anioPost = $anio;
            if($mes==1){
                $mesAnte = 12;
                $anioAnte = date("Y")-1;
            }
            if($mes==12){
                $mesPost = 1;
                $anioPost = $anio+1;
            }
            $usuario = Yii::app()->user->getState('usuario');
            $criteria = new CDbCriteria();
            $criteria->addCondition("month(fecha_presentacion) = $mes and year(fecha_presentacion)=$anio");
            $criteria->group = "day(fecha_presentacion), salon";
            if(Yii::app()->user->getState('id_perfil')==4){ //Es un tutor, ver solo sus compromisos
                $criteria->addCondition("tutor = '$usuario'");
            }
            $model = ExamsUser::model()->findAll($criteria);
		$this->render('schedule',array('model'=>$model,'mes'=>$mes,'anio'=>$anio,'mesAnte'=>$mesAnte,'anioAnte'=>$anioAnte,'mesPost'=>$mesPost,'anioPost'=>$anioPost));
        }
        
        
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id)
	{
		$model=new ExamsUser;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ExamsUser']))
		{
                    $datos = $_POST['ExamsUser'];
                    //Asociar la licencia al usuario
                    $modLicUsu = new LicensesUser();
                    $modLicUsu->id_licencia_cliente = $datos['id_licencia_usuario'];
                    $modLicUsu->id_usuario=$id;
                    $modLicUsu->fecha_asignacion = new CDbExpression('NOW()');
                    $modLicUsu->fecha_presentacion = $datos['fecha_presentacion'];
                    $modLicUsu->hora = $datos['hora'];
                    $modLicUsu->estado="A";
                    $modLicUsu->save();
                    //Gastar la licencia
                    $modUtil = new Utilidades();
                    $modUtil->gastarLicencia($datos['id_licencia_usuario']);
                    //Asignar los exámenes al usuario
                    $modLicenciasCliente = LicensesClient::model()->findByPk($modLicUsu->id_licencia_cliente);
                    $modExamenes = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>$modLicenciasCliente->id_licencia));
                    foreach($modExamenes as $examen){
                        $modExamenUsuario = new ExamsUser();
                        $modExamenUsuario->id_examen = $examen->id_examen;
                        $modExamenUsuario->id_licencia_usuario = $modLicUsu->id_licencias_usuario;
                        $modExamenUsuario->fecha_presentacion = $datos['fecha_presentacion'];
                        $modExamenUsuario->hora = $datos['hora'];
                        $modExamenUsuario->salon = $datos['salon'];
                        if(strlen($datos['tutor'])>0){
                            $modExamenUsuario->tutor = $datos['tutor']; 
                        }
                        $modExamenUsuario->estado = "A";
                        $modExamenUsuario->save();
                    }
                    if(isset($_POST['doitnow'])){
                        if($_POST['doitnow']=="1"){
                            $this->redirect(array('AnswersExamUser/exam/userId/'.$id.'/license/'.$modLicUsu->id_licencias_usuario.'/now/1'));
                        }
                    }
                    $this->redirect(array('ExamsUser/admin/id/'.$modLicUsu->id_licencias_usuario));
		}

		$this->render('create',array(
			'model'=>$model,'id'=>$id
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ExamsUser']))
		{
			$model->attributes=$_POST['ExamsUser'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('ExamsUser');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin($id)
	{
		$model=new ExamsUser('search');
		$model->unsetAttributes();  // clear any default values
                $model->id_licencia_usuario = $id;
		if(isset($_GET['ExamsUser']))
			$model->attributes=$_GET['ExamsUser'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ExamsUser the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ExamsUser::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ExamsUser $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='exams-user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
?>

<?php

class UsersCController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('areandina', 'index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','citiesCountry','update','email','admin','delete'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        public function actionAreandina(){
            $modUsu = UsersC::model()->findByAttributes(array('email'=>$_POST['email']));
            if(count($modUsu)>0){
                $hash_ede = base64_encode('8605173021&'.$modUsu->id_usuario_c.'&1234');
                $this->redirect('/ilto3/index.php?r=Clients/default/login&hash_ede='.$hash_ede);
            } else {
                //Registro en el area andina
                $modlicencia_cliente = LicensesClient::model()->disponibles()->findByAttributes(array('id_licencia'=>6,'estado'=>'A'));
                //Si hay licencias disponibles continuar
                if($modlicencia_cliente){
                    $modUtil = new Utilidades();
                    $modArea = new EdeAreandina();
                    $modArea->email = $_POST['email'];
                    $modArea->firstName = $_POST['firstName'];
                    $modArea->lastName = $_POST['lastName'];
                    if($modArea->save()){
                        //Registro unicamente en usuarios
                        $modUsu = new UsersC;
                        $modUsu->id_cliente = '8605173021';
                        $modUsu->email = $_POST['email'];
                        $modUsu->nombres = $_POST['firstName'];
                        $modUsu->apellidos = $_POST['lastName'];
                        $modUsu->id_grupo_cliente = $_POST['group'];
                        $modUsu->id_perfil=6;
                        $modUsu->estado='A';
                        $modUsu->numero_id='ede_'.$modArea->id;
                        $modUsu->id_usuario_c ='ede_'.$modArea->id;
                        $modUsu->clave = md5('1234');
                        $modUsu->clave2 = md5('1234');
                        $modUsu->id_pais=3;
                        $modUsu->id_ciudad=2;
                        $modUsu->save();
                        //Asignar licencia y exámenes. Descontar licencia al cliente
                        //Asociar la licencia al usuario
                        Yii::import('application.modules.Clients.models.LicensesUser');
                        Yii::import('application.modules.Clients.models.ExamsUser');
                        $modLicUsu = new LicensesUser();
                        $modLicUsu->id_licencia_cliente = $modlicencia_cliente->id_licencias_cliente;
                        $modLicUsu->id_usuario=$modUsu->id_usuario_c;
                        $modLicUsu->fecha_asignacion = new CDbExpression('NOW()');
                        $modLicUsu->fecha_presentacion = new CDbExpression('NOW()');
                        $modLicUsu->hora =  '10:00';
                        $modLicUsu->estado="A";
                        $modLicUsu->save();
                        //Gastar la licencia
                        $modUtil->gastarLicencia($modLicUsu->id_licencia_cliente);
                        //Asignar los exámenes al usuario
                        $modExamenes = ExamsLicense::model()->findAllByAttributes(array('id_licencia'=>6));
                        foreach($modExamenes as $examen){
                            $modExamenUsuario = new ExamsUser();
                            $modExamenUsuario->id_examen = $examen->id_examen;
                            $modExamenUsuario->id_licencia_usuario = $modLicUsu->id_licencias_usuario;
                            $modExamenUsuario->fecha_presentacion = $modLicUsu->fecha_presentacion;
                            $modExamenUsuario->hora = '10:00';
                            $modExamenUsuario->salon = 16;
                            $modExamenUsuario->estado = "A";
                            $modExamenUsuario->save();
                        }
                        $hash_ede = base64_encode('8605173021&'.'ede_'.$modArea->id.'&1234');
                        $this->redirect('/ilto3/index.php?r=Clients/default/login&hash_ede='.$hash_ede);
                    } else {
                      $this->redirect('/ilto3/ede_areandina.php?err=2'); 
                    }
                } else {
                  $this->redirect('/ilto3/ede_areandina.php?err=2'); 
                }
            }
        }


        /**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new UsersC;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['UsersC']))
		{
                        $modUtil = new Utilidades();
			$model->attributes=$_POST['UsersC'];
                        $ruta_foto=CUploadedFile::getInstance($model,'ruta_foto');
                        $ahora = time();
                        $model->id_usuario_c=$model->numero_id;
                        $clave=$model->numero_id;
                        $model->clave=md5($clave);
                        $model->clave2=md5($clave);
                        $model->estado ="A";
			if($model->save(true,array('numero_id' ,'id_cliente','id_usuario_c','nombres','apellidos','clave','email','id_perfil','tipo_id','numero_id','estado','id_pais','id_ciudad'))){
                            $modUtil = new Utilidades();
                            if(!is_null($ruta_foto)){
                                $extension = ".".$ruta_foto->getExtensionName();
                                $nombreImagen = $model->id_cliente."_".$model->id_usuario_c."_".$ahora.$extension;
                                $model->ruta_foto=$nombreImagen;
                                $model->save();
                                $ruta_foto->saveAs("images/users/".$nombreImagen);
                                $modUtil->generaThumb("images/users/",$nombreImagen);
                            }
                            $modUtil->enviarCuenta($model->id_usuario_c, 1);
                            $this->redirect(array('admin'));
                        }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

        public function actionEmail($id){
            $modUtil = new Utilidades();
            $modUtil->actionEnviarRegistro($id,1,1);
            $this->redirect(array('admin'));
            
        }
        
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=  UsersC::model()->findByAttributes(array('id_usuario_c'=>$id));

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['UsersC']))
		{
                    $anteClave = $model->clave;
                    $model->attributes=$_POST['UsersC'];
                    $model->clave = $anteClave;
                    $model->clave2 = $anteClave;
                    $ruta_imagen=CUploadedFile::getInstance($model,'ruta_foto');
                    $modUtil = new Utilidades();
                    if($model->save(true,array('nombres', 'apellidos', 'email', 'id_cliente', 'id_perfil', 'tipo_id', 'numero_id', 'estado', 'id_pais', 'id_ciudad'))){
                        if(!is_null($ruta_imagen)){
                            //Si tenia una imagen, eliminarla
                            if(isset($_POST['ruta_imagen_actual'])){
                                $modUtil->eliminarThumbs("images/users/",$_POST['ruta_imagen_actual']);
                            }
                            //Nombre de la nueva imagen
                            $extension = ".".$ruta_imagen->getExtensionName();
                            $ahora = time();
                            $nombreImagen = $model->id_cliente."_".$model->id_usuario_c.$ahora.$extension;
                            $model->ruta_foto=$nombreImagen;
                            $model->save();
                            $ruta_imagen->saveAs("images/users/".$nombreImagen);
                            $modUtil->generaThumb("images/users/",$nombreImagen);
                        } elseif(strlen($_POST['ruta_imagen_actual'])>0){ //Si existia una imagen previa
                            if(isset($_POST['delete_imagen'])){
                                $model->ruta_foto=NULL;
                                $model->save(true,array('ruta_foto'));
                                $modUtil->eliminarThumbs("images/users/",$_POST['ruta_imagen_actual']);
                            }
                        }
                        $this->redirect(array('admin'));
                    }
		}
		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id,$client)
	{
            Yii::import('application.modules.Clients.models.LicensesUser');
            Yii::import('application.modules.Clients.models.ExamsUser');
            LicensesUser::model()->deleteAllByAttributes(array('id_usuario'=>$id));
            UsersC::model()->findByAttributes(array('id_usuario_c'=>$id,'id_cliente'=>$client))->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('UsersC');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new UsersC('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['UsersC']))
			$model->attributes=$_GET['UsersC'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return UsersC the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=UsersC::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param UsersC $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='users-c-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        public function actionCitiesCountry($country){
            $retorno = CHtml::listData(Cities::model()->findAllByAttributes(array('id_pais'=>$country)), 'id_ciudad', 'nombre');
            echo json_encode($retorno);
        }
}

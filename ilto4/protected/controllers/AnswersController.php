<?php

class AnswersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','delete'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($question)
	{
		$model=new Answers;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Answers']))
		{
                        $model->attributes=$_POST['Answers'];
                        $ruta_imagen=CUploadedFile::getInstance($model,'ruta_imagen');
                        $ruta_audio=CUploadedFile::getInstance($model,'ruta_audio');
                        $ahora = time();
			if($model->save()){
                            if($model->es_verdadera==true){
                                //Marcar las otras respuestas como falsas
                                $modelRespuestas = Answers::model()->findAllByAttributes(array('id_pregunta'=>$question));
                                foreach($modelRespuestas as $rowRespuesta){
                                    $rowRespuesta->es_verdadera = 0;
                                    $rowRespuesta->save(true,array('es_verdadera'));
                                }
                                $model->es_verdadera=1;
                                $model->save(true,array('es_verdadera'));
                            }
                            $modUtil = new Utilidades();
                            if(!is_null($ruta_imagen)){
                                $extension = ".".$ruta_imagen->getExtensionName();
                                $nombreImagen = $model->id_pregunta."_".$ahora.$extension;
                                $model->ruta_imagen=$nombreImagen;
                                $model->save();
                                $ruta_imagen->saveAs("images/answers/".$nombreImagen);
                                $modUtil->generaThumb("images/answers/",$nombreImagen);
                            }
                            if(!is_null($ruta_audio)){
                                $nombreAudio = 'audio/answers/'.$model->id_pregunta."_".$ahora.".mp3";
                                $model->ruta_audio=$nombreAudio;
                                $model->save();
                                $ruta_audio->saveAs($nombreAudio);
                            }
				$this->redirect(array('admin','question'=>$question));
                        }
		}

		$this->render('create',array(
			'model'=>$model,'question'=>$question
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id,$question)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Answers']))
		{
                        $modAnt = $this->loadModel($id);
			$model->attributes=$_POST['Answers'];
                        $ruta_imagen=CUploadedFile::getInstance($model,'ruta_imagen');
                        $ruta_audio=CUploadedFile::getInstance($model,'ruta_audio');
                        $ahora = time();
			if($model->save(true,array('texto','estado','es_verdadera'))){
                            if($model->es_verdadera==true){
                                //Marcar las otras respuestas como falsas
                                $modelRespuestas = Answers::model()->findAllByAttributes(array('id_pregunta'=>$question));
                                foreach($modelRespuestas as $rowRespuesta){
                                    $rowRespuesta->es_verdadera = 0;
                                    $rowRespuesta->save(true,array('es_verdadera'));
                                }
                                $model->es_verdadera=1;
                                $model->save(true,array('es_verdadera'));
                            }
                            $modUtil = new Utilidades();
                                if(!is_null($ruta_imagen)){
                                    //Si tenia una imagen, eliminarla
                                    if(isset($_POST['ruta_imagen_actual'])){
                                        $modUtil->eliminarThumbs("images/answers/",$_POST['ruta_imagen_actual']);
                                    }
                                    //Nombre de la nueva imagen
                                    $extension = ".".$ruta_imagen->getExtensionName();
                                    $nombreImagen = $model->id_pregunta."_".$ahora.$extension;
                                    $model->ruta_imagen=$nombreImagen;
                                    $model->save();
                                    $ruta_imagen->saveAs("images/answers/".$nombreImagen);
                                    $modUtil->generaThumb("images/answers/",$nombreImagen);
                                } elseif(strlen($_POST['ruta_imagen_actual'])>0){ //Si existia una imagen previa
                                    if(isset($_POST['delete_imagen'])){
                                        $model->ruta_imagen=NULL;
                                        $model->save(true,array('ruta_imagen'));
                                        $modUtil->eliminarThumbs("images/answers/",$_POST['ruta_imagen_actual']);
                                    }
                                }
                                if(!is_null($ruta_audio)){
                                    //Si tenia una imagen, eliminarla
                                    if(isset($_POST['ruta_audio_actual'])){
                                        @unlink($_POST['ruta_audio_actual']);
                                    }
                                    //Nombre de la nueva imagen
                                    $nombreAudio = 'audio/answers/'.$model->id_pregunta."_".$ahora.".mp3";
                                    $model->ruta_audio=$nombreAudio;
                                    $model->save();
                                    $ruta_audio->saveAs($nombreAudio);
                                } elseif(strlen($_POST['ruta_audio_actual'])>0){ //Si existia un archivo previa
                                    if(isset($_POST['delete_audio'])){
                                        @unlink($_POST['ruta_audio_actual']); //Eliminar el audio actual
                                        $model->ruta_audio = NULL;
                                        $model->save(true,array('ruta_audio'));
                                    }
                                }
				$this->redirect(array('admin','question'=>$question));
                        }
		}

		$this->render('update',array(
			'model'=>$model,'question'=>$question
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Answers');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin($question=NULL)
	{
		$model=new Answers('search');
		$model->unsetAttributes();  // clear any default values
                if(!is_null($question)){
                    $model->id_pregunta = $question;
                }
		if(isset($_GET['Answers']))
			$model->attributes=$_GET['Answers'];

		$this->render('admin',array(
			'model'=>$model,'question'=>$question
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Answers the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Answers::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Answers $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='answers-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

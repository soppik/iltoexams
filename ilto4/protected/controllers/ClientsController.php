<?php

class ClientsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','delete'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Clients;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Clients']))
		{
			$model->attributes=$_POST['Clients'];
                        $ruta_logo=CUploadedFile::getInstance($model,'ruta_logo');
                        $ahora = time();
                        $model->estado="A";
                        $model->id_cliente = $model->nit_id_legal;
                        if($_POST['Clients']['id_distributor']=='-1'){
                            $model->id_distributor = NULL;
                        }
			if($model->save()){
                            $modUtil = new Utilidades();
                            if(!is_null($ruta_logo)){
                                $extension = ".".$ruta_logo->getExtensionName();
                                $nombreImagen = $model->id_cliente."_".$ahora.$extension;
                                $model->ruta_logo=$nombreImagen;
                                $model->save();
                                $ruta_logo->saveAs("images/clients/".$nombreImagen);
                                $modUtil->generaThumb("images/clients/",$nombreImagen);
                            }
                            $this->redirect(array('admin'));
                        }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['Clients']))
		{

                        $modAnt = $this->loadModel($id);
			$model->attributes=$_POST['Clients'];
                        $ruta_imagen=CUploadedFile::getInstance($model,'ruta_logo');
                        $ahora = time();
                        if($_POST['Clients']['id_distributor']=='-1'){
                            $model->id_distributor = NULL;
                        }
			if($model->save(true,array('id_cliente','is_distributor','examen_asistido','id_distributor', 'nombre_rsocial','direccion','id_pais','id_ciudad','nit_id_legal','telefono','contacto','email','logo_en_certificado','estado'))){
                            $modUtil = new Utilidades();
                                if(!is_null($ruta_imagen)){
                                    //Si tenia una imagen, eliminarla
                                    if(isset($_POST['ruta_imagen_actual'])){
                                        $modUtil->eliminarThumbs("images/clients/",$_POST['ruta_imagen_actual']);
                                    }
                                    //Nombre de la nueva imagen
                                    $extension = ".".$ruta_imagen->getExtensionName();
                                    $nombreImagen = $model->id_cliente."_".$ahora.$extension;
                                    $model->ruta_logo=$nombreImagen;
                                    $model->save();
                                    $ruta_imagen->saveAs("images/clients/".$nombreImagen);
                                    $modUtil->generaThumb("images/clients/",$nombreImagen);
                                } elseif(strlen($_POST['ruta_imagen_actual'])>0){ //Si existia una imagen previa
                                    if(isset($_POST['delete_imagen'])){
                                        $model->ruta_logo=NULL;
                                        $model->save(true,array('ruta_logo'));
                                        $modUtil->eliminarThumbs("images/clients/",$_POST['ruta_imagen_actual']);
                                    }
                                }
                            $this->redirect(array('admin'));
                        }
                }            
		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Clients');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Clients('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Clients']))
			$model->attributes=$_GET['Clients'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Clients the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Clients::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Clients $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='clients-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

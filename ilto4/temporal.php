<div style="width: 620px;">
	<div class="blanca">
		<p style="text-align: justify;">Ingrese su información y su inscripción quedará registrada. Por favor, diligencie completamente el formulario para garantizar su participación.</p>
		<div id="formform">
			<link rel="stylesheet" type="text/css" href="registro/form_styles.css" />
			<div id="formulario" class="form-style-5">
				<form id="form1" name="form1" method="post" onsubmit="return validar()" action="http://www.jornadaoperadoresdemonoboyas.org/registro/form.php">
					<legend>Información Personal</legend>
					<fieldset id="datos_generales"><label for="documento">* Documento de Identidad</label><select name="tipo_documento" id="tipo_documento"><option value="">...</option><option value="CC">Cédula</option><option value="Pasaporte">Pasaporte</option><option value="CE">Cédula de Extranjería</option></select><input style="background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg=='); background-repeat: no-repeat; background-attachment: scroll; background-position: right center;" name="documento" id="documento" placeholder="Número" type="text" /><br class="clear" />
                                            <label for="nombre">* Primer Nombre</label><input style="background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg=='); background-repeat: no-repeat; background-attachment: scroll; background-position: right center; cursor: auto;" name="nombre" id="nombre" type="text" /><br class="clear" />
                                            <label for="segundo_nombre"> Segundo Nombre</label><input name="segundo_nombre" id="segundo_nombre" type="text" /><br class="clear" />
                                            <label for="apellido"> * Primer Apellido</label><input name="apellido" id="apellido" type="text" /><br class="clear" />
                                            <label for="segundo_apellido"> Segundo Apellido</label><input name="segundo_apellido" id="segundo_apellido" type="text" /><br class="clear" />
                                            <label for="direccion">* Dirección</label><input name="direccion" id="direccion" type="text" /><br class="clear" />
                                            <label for="email">* Email</label><input name="email" id="email" type="text" /><br class="clear" />
                                            <label for="telefono">* Teléfono</label><input name="telefono" id="telefono" type="text" /><br class="clear" />
                                            <label for="celular">Teléfono Móvil</label><input name="celular" id="celular" type="text" /><br class="clear" />
                                            <label for="pais">* País</label><input name="pais" id="pais" type="text" /><br class="clear" />
                                            <label for="estado">* Estado</label><input name="estado" id="estado" type="text" /><br class="clear" />
                                            <label for="ciudad">* Ciudad</label><input name="ciudad" id="ciudad" type="text" /><br class="clear" />
                                            <p>* Para legalizar su inscripción, la factura se debe hacer a </p>
                                            <label for="factura_para">nombre de:</label><select name="factura_para" id="factura_para"><option value="">...</option><option value="Empresa">Empresa</option><option value="Persona">Persona</option></select><br class="clear" />
					</fieldset>
					<fieldset id="company" style="display: none;">
                                            <label for="compania">*Nombre de la Empresa</label><input name="compania" id="compania" type="text" /><br class="clear" />
                                            <label for="nit">*NIT/VAT/ID/RUT</label><input name="nit" id="nit" type="text" /><br class="clear" />
                                            <label for="direccion_factura">*Dirección</label><input name="direccion_factura" id="direccion_factura" type="text" /><br class="clear" />
                                            <label for="encargado">*Persona Contacto</label><input name="encargado" id="encargado" type="text" /><br class="clear" />
                                            <label for="cargo">*Cargo</label><input name="cargo" id="cargo" type="text" /><br class="clear" />
                                            <label for="email_corp">*Email Corporativo</label><input name="email_corp" id="email_corp" type="text" /><br class="clear" />
                                            <label for="pais_factura">*País</label><input name="pais_factura" id="pais_factura" type="text" /><br class="clear" />
                                            <label for="estado_factura">*Estado</label><input name="estado_factura" id="estado_factura" type="text" /><br class="clear" />
                                            <label for="ciudad_factura">*Ciudad</label><input name="ciudad_factura" id="ciudad_factura" type="text" /><br class="clear" />
                                            <label for="telefono_factura">*Teléfono</label><input name="telefono_factura" id="telefono_factura" type="text" /><br class="clear" />
					</fieldset><label for="formapago">*Forma de Pago</label><select name="formapago" id="formapago"><option value="1">Tarjeta de Crédito / Débito</option><option value="2">Efectivo / Transferencia</option></select><br class="clear" />
					<fieldset id="participante"><label for="tipo_participante">* Tipo Participante</label><select name="tipo_participante" id="tipo_participante"><option value="">...</option><option value="1">Acompañante</option><option value="2">Modelo Stand</option><option value="3">Participante General</option><option value="4">Participante General 1 Día</option><option value="5">Conferencista</option></select>
						<fieldset id="conferencista" style="display: none;"><label for="titulo_ponencia">Titulo Ponencia</label><textarea name="titulo_ponencia" id="titulo_ponencia" cols="45" rows="2"></textarea><br class="clear" /><label for="resumen_conferencia">Resumen Conferencia</label><textarea name="resumen_conferencia" id="resumen_conferencia" cols="45" rows="5"></textarea><br class="clear" /><label for="autores_tema">Autores Tema</label><input name="autores_tema" id="autores_tema" type="text" /><br class="clear" />
						</fieldset>
					</fieldset><span id="total">Total a Pagar:</span>
					<fieldset id="fieldsetTotales"></fieldset><input name="btnRegistro" id="btnEnviar" value="Siguiente / Next" type="submit" />
				</form>
			</div>
		</div>
		<div>&nbsp;</div>
	</div>
	<div class="blanca" style="background-color: red; width: 90%; margin: auto; height: 40px; color: #ffffff; text-align: center; font-size: 14px; vertical-align: middle; line-height: 40px;">Contacto:&nbsp;<a href="mailto:operadorlogistico.slom@jornadasoperadoresdemonoboyas.org">operadorlogistico.slom@jornadaoperadoresdemonoboyas.org</a>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
	// <![CDATA[
	var pago1 = ["", "Total a Pagar: $USD 280.00", "Total a Pagar: $USD 280.00", "Total a Pagar: $USD 484.00", "Total a Pagar: $USD 242.00", "Total a Pagar: $USD 0.00"];
	var pago2 = ["", "Total a Pagar: $USD 260.00", "Total a Pagar: $USD 260.00", "Total a Pagar: $USD 450.00", "Total a Pagar: $USD 225.00", "Total a Pagar: $USD 0.00"];
	var valor1 = [0, 280, 280, 484, 242, 0];
	var valor2 = [0, 260, 260, 450, 225, 0];
	function cargaform() {
		jQuery('#formform').show();
	}
	function validar() {
		var retorno = true;
		if (document.form1.nombre.value == "") {
			alert('Debe suminisrar su nombre');
			return false;
		}
		if (document.form1.email.value == "") {
			alert('Debe suminisrar su direccion de Email');
			return false;
		}
		if (document.form1.tipo_participante.value == "") {
			alert('Debe seleccionar un tipo de participante');
			return false;
		}
		return true;
	}
	$('#formapago').change(function() {
		ponePrecios();
	});
	$('#factura_para').change(function() {
		if ($('#factura_para').val() == "Empresa") {
			$('#company').show();
		} else {
			$('#company').hide();
		}
	});
	$('#tipo_participante').change(function() {
		ponePrecios();
	});
	function ponePrecios() {
		$('#conferencista').hide();
		if ($('#formapago').val() === "1") {
			arreglo = pago1;
			arrvalo = valor1;
		} else {
			arreglo = pago2;
			arrvalo = valor2;
		}
		var precio = "Total a pagar:";
		if ($('#tipo_participante').val() === "1") {
			precio = arreglo[1];
			valor = arrvalo[1];
		}
		if ($('#tipo_participante').val() === "2") {
			precio = arreglo[2];
			valor = arrvalo[2];
		}
		if ($('#tipo_participante').val() === "3") {
			precio = arreglo[3];
			valor = arrvalo[3];
		}
		if ($('#tipo_participante').val() === "4") {
			precio = arreglo[4];
			valor = arrvalo[4];
		}
		if ($('#tipo_participante').val() === "5") {
			$('#conferencista').show();
			precio = arreglo[5];
			valor = arrvalo[5];
		}
		$('#total').html(precio);
	}
	// ]]>
	
</script>
<p>
	<style scoped="scoped" type="text/css">
		<!--
		/* unvisited link */
		.blanca a:link {
			color: #FFFFFF !important;
		}
		/* visited link */
		.blanca a:visited {
			color: #FFFFFF !important;
		}
		/* mouse over link */
		.blanca a:hover {
			color: #E0E0E0 !important;
		}
		/* selected link */
		.blanca a:active {
			color: #FFFFFF !important;
		}
		.negra a:link {
			color: #000000 !important;
		}
		/* visited link */
		.negra a:visited {
			color: #000000 !important;
		}
		/* mouse over link */
		.negra a:hover {
			color: #000000 !important;
		}
		/* selected link */
		.negra a:active {
			color: #000000 !important;
		}
		-->
	</style>
</p>